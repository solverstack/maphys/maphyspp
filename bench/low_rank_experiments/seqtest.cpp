#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0

#include <maphys.hpp>
#include <maphys/IO/ReadParam.hpp>
#include <maphys/dist/MPI.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#if defined(MPH_SDS_USE_MUMPS)
#include <maphys/solver/Mumps.hpp>
#endif
#if defined(MPH_SDS_USE_PASTIX)
#include <maphys/solver/Pastix.hpp>
#endif

using namespace maphys;
using Scalar = double;
using Real = double;

using SpMat = SparseMatrixCOO<Scalar>;
using Vect = Vector<Scalar>;

const bool root_verbose = true;

void usage(char ** argv){
  std::cerr << "Usage: " << std::string(argv[0]) << " matrix_file [rhs_file]\n";
}

template<typename DirectSolver>
void bench(const SpMat& A, const Vect& b, const std::string& benchname){
  using Solver = ConjugateGradient<SpMat, Vect, DirectSolver>;

  bool verbose = root_verbose;

  Solver cg(A);

  auto dirsolver_setup = [/*verbose*/](const SpMat& matrix, DirectSolver& precond){
                           precond.set_low_rank(1e-3);
                           precond.setup(matrix);
                           precond.set_low_rank(1e-3);
                           //if(verbose) precond.apply_on_data([](DirectSolver& d){ d.set_verbose(2); });
                         };

  cg.setup(parameters::max_iter{2},
           parameters::tolerance{1e-8},
           parameters::verbose{verbose},
           parameters::setup_pcd<std::function<void(const SpMat&, DirectSolver&)>>{dirsolver_setup});

  std::cout << "##### Bench: " << benchname << '\n';

  Timer<1> t(benchname);
  Vect X = cg * b;
  t.stop();
  t.results(std::cout, false);
  t.reset();

  std::cout << "First 10 values\n";
  for(int k = 0; k < 10; ++k){
    std::cout << X[k] << '\n';
  }
  std::cout << '\n';

  cg.display("CG");
}

int main(int argc, char ** argv) {

  if(argc != 2 && argc != 3){
    usage(argv);
    return 1;
  }

  MMPI::init();{

    const std::string mat_filename = std::string(argv[1]);
    auto get_A = [mat_filename](){
                   SparseMatrixCOO<Scalar> mat;
                   mat.from_matrix_market_file(mat_filename);
                   mat.set_spd(MatrixStorage::lower);
                   return mat;
                 };

    SparseMatrixCOO<Scalar> A(get_A());

    Vector<Scalar> b;
    if(argc == 3){
      std::cout << "Loading RHS file\n";
      const std::string rhs_filename = std::string(argv[2]);
      SparseMatrixCOO<Scalar> Bsp;
      Bsp.from_matrix_market_file(rhs_filename);
      b = Bsp.to_dense();
    }
    else{
      std::cout << "Generation RHS, with solution = (1, ..., 1)^T\n";
      Vector<Scalar> x_gen(n_rows(A));
      for(Size k = 0; k < n_rows(A); ++k) x_gen[k] = Scalar{1};
      b = A * x_gen;
    }

#if defined(MPH_SDS_USE_MUMPS)
    {
      bench<Mumps<SparseMatrixCOO<Scalar>, Vector<Scalar>>>(A, b, "Mumps");
    }
#endif
#if defined(MPH_SDS_USE_PASTIX)
    {
      bench<Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>>(A, b, "Pastix");
    }
#endif
  }
  MMPI::finalize();

  return 0;
}
