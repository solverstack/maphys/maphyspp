#+AUTHOR: HiePACS
#+EMAIL: gilles.marait@inria.fr
#+TITLE: MaPHyS++ weekly benchmark on plafrim

#+begin_src sh :tangle benchscript.sh
#!/bin/bash

########################
### PARAMETERS SETUP ###
########################

guix pull

# matrices folder
MATRIX_DIR=/home/gitlab-compose
# results
RESULT_ROOT_DIR=/home/gitlab-compose/weekly_mpp_results

# openblas options for guix and cmake
PKG_ENV="slurm@22 eigen armadillo scalapack zlib fabulous --with-input=pastix=pastix@6.3.1 --with-input=slurm=slurm@22"
PKG_ENV_MKL="$PKG_ENV --with-input=mumps-openmpi=mumps-mkl-openmpi --with-input=openblas=mkl --with-input=python-numpy=python-numpy-mkl"

CMAKE_DFLAGS="-DCMAKE_BUILD_TYPE=Release -DMAPHYSPP_USE_FABULOUS=ON -DMAPHYSPP_COMPILE_TESTS=OFF -DMAPHYSPP_GCC_WARNINGS=OFF"

RESULT_DIR=$RESULT_ROOT_DIR/plafrim

echo "RESULT_DIR: $RESULT_DIR"

git submodule update --init --recursive
guix describe
rm -rf build_openblas build_mkl
mkdir build_openblas build_mkl

# Create guix manifests # Buggy for the moment
# guix shell --export-manifest -D maphys++ $PKG_ENV > build_openblas/env.scm
# guix shell --export-manifest -D maphys++ $PKG_ENV_MKL > build_mkl/env.scm

# OPENBLAS
cd build_openblas
GX_CMD="guix shell --pure -D maphys++ $PKG_ENV -- cmake .. $CMAKE_DFLAGS" && printf "\n${GX_CMD}\n" && $GX_CMD
GX_CMD="guix shell --pure -D maphys++ $PKG_ENV -- make mpp_driver_cg" && printf "\n${GX_CMD}\n" && $GX_CMD

# MKL
cd ../build_mkl
GX_CMD="guix shell --pure -D maphys++ $PKG_ENV_MKL -- cmake .. $CMAKE_DFLAGS" && printf "\n${GX_CMD}\n" && $GX_CMD
GX_CMD="guix shell --pure -D maphys++ $PKG_ENV_MKL -- make mpp_driver_cg" && printf "\n${GX_CMD}\n" && $GX_CMD  
cd ..

# Copy executable to benchmark directory
rm -rf $RESULT_DIR 2> /dev/null
mkdir -p $RESULT_DIR

guix describe --format=channels > $RESULT_DIR/channels.txt
git log | head -n 1 | cut -d\  -f2 > $RESULT_DIR/commit.txt
cp -r bench/plafrim/* $RESULT_DIR
cp build_openblas/src/driver/C++/mpp_driver_cg $RESULT_DIR/driver_openblas
cp build_openblas/env.scm $RESULT_DIR/driver_openblas
cp build_mkl/src/driver/C++/mpp_driver_cg $RESULT_DIR/driver_mkl
cp build_mkl/env.scm $RESULT_DIR/driver_mkl

# Launch benchmark
cd $RESULT_DIR
chmod +x execmaphyspp.sh
date +"%c" > lastruntime.txt
guix shell jube-with-yaml -- jube run bench.yaml

# Wait for jobs completion
NJOBS=$((`squeue | wc -l` - 1))
while [[ "$NJOBS" > 0 ]]
do
  NJOBS=$((`squeue | wc -l` - 1))
  echo "NJOBS = $NJOBS"
  sleep 30
done

# Post treatment
rm `find . | grep core` # Rm core files in case of failure
chmod +x post-treat.sh
./post-treat.sh

#+end_src

#+begin_src xml :tangle bench.yaml
name: M++_weekly
outpath: maphyspp_results
comment: Weekly bench for maphys++

#Configuration
parameterset:
  - name: execute_param
    parameter:
      - { name: exe, _: "${jube_benchmark_home}/execmaphyspp.sh" }
      - { name: blas, _: "openblas,mkl" }
      - { name: driver, _: "${jube_benchmark_home}/driver_${blas}" }
      - { name: guix_env_openblas, _: "-D maphys++ slurm@22 eigen armadillo scalapack zlib fabulous --with-input=pastix=pastix@6.3.1 --with-input=slurm=slurm@22" }
      - { name: guix_env_mkl, _: "${guix_env_openblas} --with-input=mumps-openmpi=mumps-mkl-openmpi --with-input=openblas=mkl --with-input=python-numpy=python-numpy-mkl" }
      - { name: guix_env, mode: python, _: "{'mkl':'${guix_env_mkl}', 'openblas':'${guix_env_openblas}'}['${blas}']" }

  - name: systemParameter
    parameter:
      - { name: tasks, type: int,          _: "18,36,72,144" } #18,36,72,144
      - { name: corepernode, type: int,    _: 36 }
      - { name: threadspertask, type: int, _: 1 }
      - { name: ndomains, type: int,       _: "${tasks}" }
      - { name: nodes, type: int, mode: python, _: "int(${tasks} / ${corepernode} + 1 if ${tasks} % ${corepernode} else ${tasks} / ${corepernode})" }
      - { name: taskspernode, type: int, mode: python, _: "min(${tasks},${corepernode})" }
      - { name: is_mt, type: int, mode: python, _: "0 if $threadspertask == 1 else 1" }
      - { name: executable, _: "$exe" }
      - { name: args_exe, _: "$driver" }
      - { name: outlogfile, _: job.out }
      - { name: errlogfile, _: job.err }
      - { name: timelimit, _: "00:10:00" }
      - { name: preprocess, _: "" }
      - { name: postprocess, _: "" }

  - name: executeset
    parameter:
      - { name: env, _: "module load mpi/openmpi/4.1.1; export OMPI_MCA_pml='^ucx'" }
      - { name: measurement, _: "" }
      - { name: submit, _: sbatch }
      - { name: submit_script, _: submit.job }
      - { name: starter, _: "guix shell --pure --preserve='^SLURM|^OMPI' ${guix_env} --" }
      - { name: args_starter, _: "" }

  - name: matrix_param
    parameter:
      - { name: preconditioner, _: "0,diag,AS,RR" } #0, diag, AS, NN, RR, ASG+, ASGd, NNGd, RRGd, ASed, RRed
      - { name: use_schur, _: "0,1" }
      - { name: matrix_type, _: mpp } #mpp/Eigen
      - { name: robin_weight, _: 1.0 } # For RR (real) precond
      - { name: sparse_direct_solver, _: Mumps } # Pastix / Mumps /Eigen

fileset:
  name: copy_files
  copy:
    - { _: "${submit_script}.in" } # submit.job.in -> submit.job
    - { _: "/home/gitlab-compose/weekly_mpp_results/plafrim/template.in" }

substituteset:
  - name: set_input
    iofile: { in: template.in, out: input.in }
    sub:
      - {source: "#NDOM#",       dest: "${ndomains}" }
      - {source: "#MAXITER#",    dest: "20000" }
      - {source: "#USE_SCHUR#",  dest: "${use_schur}" }
      - {source: "#PARTITIONS#", dest: "/home/gitlab-compose/baton_30_newformat/" }
      - {source: "#PARTTYPE#",   dest: "baton" }
      - {source: "#PCD#",        dest: "${preconditioner}" }
      - {source: "#SDS#",        dest: "${sparse_direct_solver}" }
      - {source: "#MATTYPE#",    dest: "${matrix_type}" }
      - {source: "#TOL#",        dest: "1.0e-8" }
      - {source: "#JACITER#",    dest: "${jac_iter}"}
      - {source: "#RR_WEIGHT#",  dest: "${robin_weight}"}
  - name: executesub
    iofile: { in: "${submit_script}.in", out: "$submit_script"}
    sub:
      - {source: "#ENV#", dest: "$env" }
      - {source: "#BENCHNAME#", dest: "${jube_benchmark_name}" }
      - {source: "#NODES#", dest: "$nodes" }
      - {source: "#TASKS#", dest: "$tasks" }
      - {source: "#NCPUS#", dest: "$taskspernode" }
      - {source: "#NTHREADS#", dest: "$threadspertask" }
      - {source: "#TIME_LIMIT#", dest: "$timelimit" }
      - {source: "#PREPROCESS#", dest: "$preprocess" }
      - {source: "#POSTPROCESS#", dest: "$postprocess" }
      - {source: "#STARTER#", dest: "$starter" }
      - {source: "#ARGS_STARTER#", dest: "$args_starter" }
      - {source: "#MEASUREMENT#", dest: "$measurement" }
      - {source: "#STDOUTLOGFILE#", dest: "$outlogfile" }
      - {source: "#STDERRLOGFILE#", dest: "$errlogfile" }
      - {source: "#EXECUTABLE#", dest: "$executable" }
      - {source: "#ARGS_EXECUTABLE#", dest: "$args_exe" }
      - {source: "#FLAG#", dest: "touch ready" }

patternset:
  name: pattern
  pattern:
    - { name: NbDomains   , type: int, _: "Number of subdomains: $jube_pat_int" }
    - { name: nIter       , type: int, _: "Niter performed last solve: $jube_pat_int" }
    - { name: GlobMatOrder, type: int, _: "Size of global K: $jube_pat_int" }
    - { name: DirectSolver,            _: 'Direct solver: (\w+)' }
    - { name: CoarseEigenSolve,        _: "Coarse Eigen Solve: $jube_pat_fp" }
    - { name: DirectSolve,             _: "Direct Solve: $jube_pat_fp" }
    - { name: CoarsePcdSetup,          _: "Coarse Pcd Setup: $jube_pat_fp" }
    - { name: IterativeSolve,          _: "Iterative Solve: $jube_pat_fp" }
    - { name: LocalPcdSetup,           _: "Local Pcd Setup: $jube_pat_fp" }
    - { name: RunTime,                 _: "Runtime: $jube_pat_fp" }
    - { name: SchurComputation,        _: "Schur Computation: $jube_pat_fp" }
    - { name: TotalTime,               _: "Total time: $jube_pat_fp" }

step:
  - name: execute
    use:
      - [executeset,systemParameter,execute_param,matrix_param,copy_files,set_input,executesub]
    do: "$submit $submit_script"

  - name: postprocess
    depend: execute
    do: echo "postprocess"

analyser:
  name: analyse
  use: pattern
  analyse:
    step: postprocess
    file: "./execute/${outlogfile}"

result:
  use: analyse
  table:
    name: result
    style: csv
    sort: NbDomains
    column:
      - NbDomains
      - preconditioner
      - use_schur
      - blas
      - DirectSolver
      - nIter
      - CoarseEigenSolve
      - DirectSolve
      - CoarsePcdSetup
      - IterativeSolve
      - LocalPcdSetup
      - SchurComputation
      - TotalTime
      - RunTime
      - GlobMatOrder
      - robin_weight
      - threadspertask

#+end_src

#+begin_src sh :tangle post-treat.sh
#!/bin/bash

echo "######### MaPHyS++ benchmarks - post-treatment start #########"

CURDATE=`date +"%y-%m-%d"`

guix shell jube-with-yaml -- bash -c 'jube continue maphyspp_results && jube analyse maphyspp_results && jube result maphyspp_results > results.csv'
cp results.csv results_${CURDATE}.csv
cat results.csv

guix shell --pure coreutils bash r-minimal r-stringr r-dplyr r-ggplotify r-tidyr -- Rscript post-treat.R

cp results* ..
cp mpp_* ..
cp *.txt ..

echo "######### MaPHyS++ benchmarks - post-treatment end #########"

exit 0
#+end_src

#+begin_src sh :tangle submit.job.in
#!/bin/bash
#SBATCH --job-name=#BENCHNAME#
#SBATCH --nodes=#NODES#
#SBATCH --ntasks=#TASKS#
#SBATCH --ntasks-per-node=#NCPUS#
#SBATCH --cpus-per-task=#NTHREADS#
#SBATCH --time=#TIME_LIMIT#
#SBATCH --output=#STDOUTLOGFILE#
#SBATCH --error=#STDERRLOGFILE#
#SBATCH --exclusive
#SBATCH -p routage
#SBATCH --constraint bora

#ENV#

#PREPROCESS#

#MEASUREMENT# #STARTER# #ARGS_STARTER# #EXECUTABLE# #ARGS_EXECUTABLE#

#POSTPROCESS#

JUBE_ERR_CODE=$?
if [ $JUBE_ERR_CODE -ne 0 ]; then
    exit $JUBE_ERR_CODE
fi

#FLAG#
#+end_src

#+begin_src txt :tangle template.in
n_subdomains: #NDOM#
max_iter: #MAXITER#
use_schur: #USE_SCHUR#
# Path to partitions
partitions: #PARTITIONS#
# Partiton type
partition_type: #PARTTYPE#
# Type of preconditioner
precond: #PCD#
# mpp or Eigen
matrix_type: #MATTYPE#
# Pastix or Mumps
direct_solver: #SDS#
tolerance: #TOL#
robin_weight: #RR_WEIGHT#
#+end_src

#+begin_src R :tangle post-treat.R
library(ggplot2)
library(dplyr)
library(tidyr)
library(stringr)

c_facto = "#1f78b4"
c_CG = "#e31a1c"
c_M1 = "#33a02c"
c_eigen = "#b2FF8a"
c_M0 = "#92DF6a"
c_dsolve = "#ff7f00"
c_walltime= "#AAAAAA"

data = read.csv(file="results.csv")

data <- data %>%
  gather(step, time, SchurComputation, LocalPcdSetup, DirectSolve, IterativeSolve, factor_key=TRUE)
data$step = factor(data$step, levels=c("SchurComputation", "LocalPcdSetup", "DirectSolve", "IterativeSolve"))

data$label = ""
data <- filter(data, nIter > 0)
data$label = data$nIter
data$label = format(format(data$label), width=5, justify="left")

for (fblas in c("openblas", "mkl")){
  for (on_A in c(0, 1)){
    
    # Now filter
    b_data <- filter(data, grepl(fblas, blas, fixed=TRUE))
    a_data <- filter(b_data, grepl(on_A, use_schur, fixed=TRUE))
    
    # Add y_label as the TotalTime + smtg (to put iteration number there)
    A <- "S"
    if(on_A == 0){ A <- "K" }
    cut <- max(a_data$TotalTime) * 1.05

    if(cut == -Inf){ 
      print(paste("No results for BLAS = ", fblas, " ; on ", A))
      next
    }

    a_data <- a_data %>%
      mutate(y_label = TotalTime) %>%
      mutate(y_label=y_label + .05*min(cut, max(y_label)))
    disp_lim <- cut * 0.99
    a_data$y_label[a_data$y_label>disp_lim] = disp_lim
    
    g = ggplot(a_data, aes(x=factor(NbDomains), y=time)) +
      geom_col(aes(fill=step), position = position_stack(reverse = TRUE)) +
      geom_text(angle=90, size=4, aes(label=label, y=y_label)) +
      scale_fill_manual(name="Solver step: ",
                        values=c("SchurComputation"=c_facto,
                                 "LocalPcdSetup"=c_M1,
                                        # "CoarseEigenSolve"=c_eigen,
                                        # "CoarsePcdSetup"=c_M0,
                                 "DirectSolve"=c_dsolve,
                                 "IterativeSolve"=c_CG),
                        labels=c("SchurComputation"="Schur Computation",
                                 "LocalPcdSetup"="Local Pcd Setup",
                                        # "CoarseEigenSolve"="Coarse Eigen Solve",
                                        # "CoarsePcdSetup"="Coarse Pcd Setup",
                                 "DirectSolve"="Direct Solve",
                                 "IterativeSolve"="Iterative Solve")) +
      facet_grid( . ~ preconditioner ) +
      labs(x="Number of domains",
           y="Time (s)") +
      theme_bw() +
      theme(text = element_text(size = 10)) +
      theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
      coord_cartesian(ylim = c(0, cut), expand = TRUE)
    
    filename <- paste("mpp_", A, "_", fblas, ".png", sep="")
    ggsave(filename, width=8,height=6)
  }
}
#+end_src

#+begin_src sh :tangle execmaphyspp.sh
#!/bin/bash

if [ "$#" -lt 1 ]; then
  echo 'Missing argument : result directory' && exit 1
fi

EXEC=$1
export LD_LIBRARY_PATH=$LIBRARY_PATH
export MKL_NUM_THREADS=1
export OPENBLAS_NUM_THREADS=1
export OMP_NUM_THREADS=1
export MPIRUN_OPTIONS="--bind-to core --map-by ppr:1:node:PE=${SLURM_CPUS_PER_TASK} -report-bindings"
mpiexec -n ${SLURM_NPROCS} $EXEC input.in
#+end_src
