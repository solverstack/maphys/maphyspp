export MIN_NRHS=25; \
export MAX_NRHS=25; \  # Number of columns of right hand side
export MATRIX_PATH=/maphyspp-1.0.0/matrices/young1c.mtx; \  #/home/msimonin/pc/maphyspp/matrices/matrix_PW_PBC_PML_P1.mtx; \  # Input matrix (A in Ax = b)
#export GUIX_BASE_ENV='-L /home/matthieu/Repositories/guix-hpc --pure maphys++ --ad-hoc eigen armadillo scalapack zlib fabulous+timer'; \
#export GUIX_BASE_ENV=${GUIX_BASE_ENV}" --preserve='^OMP_NUM_THREADS$' "; \
export CMAKE_BASE_FLAGS=' -DCMAKE_BUILD_TYPE=Release -DMAPHYSPP_COMPILE_EXAMPLES=ON -DMAPHYSPP_COMPILE_TESTS=OFF'; \
export CMAKE_BASE_FLAGS=${CMAKE_BASE_FLAGS}' -DMAPHYSPP_DEV_TANGLE=OFF -DMAPHYSPP_GCC_WARNINGS=OFF -DMAPHYSPP_USE_FABULOUS=ON '; \ 
#export GUIX_MKL_ENV=' --with-input=mumps-openmpi=mumps-mkl-openmpi --with-input=openblas=mkl '; \
#export GUIX_RSB_ENV=' librsb --with-source=librsb=/home/matthieu/Repositories/librsb '; \
export CMAKE_RSB_FLAGS=' -DMAPHYS_USE_RSB_SPBLAS=ON'; \
rm -f ../trace_t.csv && \
rm -f ../trace_p.csv && \
rm -f ../trace_b.csv && \
export OMP_NUM_THREADS=1 ; \
trace(){
cmake .. $CMAKE_FLAGS && \
make fabulous_bench && \
# "blas", "scalar", "solver", "ortho", "nrhs", "n_iter", "name", "level", "tbegin", "tend"
for nrhs in $(seq ${MIN_NRHS} ${MAX_NRHS}); do export OMP_NUM_THREADS=1 && ./src/examples/fabulous_bench $MATRIX_PATH $nrhs -t >> ../trace_t.out ; done # time
# "blas","scalar","solver","ortho","n","nrhs","iter","min","max","time"
for nrhs in $(seq ${MIN_NRHS} ${MAX_NRHS}); do export OMP_NUM_THREADS=1 && ./src/examples/fabulous_bench $MATRIX_PATH $nrhs -p >> ../trace_p.csv ; done # convergence history
# blas, solver, ortho, niter, t_setup, t_solve, re, scalar, rr, nb_col
for nrhs in $(seq ${MIN_NRHS} ${MAX_NRHS}); do export OMP_NUM_THREADS=1 && ./src/examples/fabulous_bench $MATRIX_PATH $nrhs -b >> ../trace_b.csv ; done # summary
}

trace_reset(){
rm -f ../trace_t.out && \
rm -f ../trace_p.csv && \
rm -f ../trace_b.csv && \
#rm -rf *
}

# openblas env
#export GUIX_ENV=$GUIX_BASE_ENV ; \
export CMAKE_FLAGS=$CMAKE_BASE_FLAGS ; \
rm -rf * && \
trace  && \
# openblas + librsb env
#export GUIX_ENV=${GUIX_BASE_ENV}$GUIX_RSB_ENV ; \
export CMAKE_FLAGS=${CMAKE_BASE_FLAGS}$CMAKE_RSB_FLAGS ; \
rm -rf * && \
trace  && \


# mkl env
#export GUIX_ENV=${GUIX_MKL_ENV}${GUIX_BASE_ENV} ; \
export CMAKE_FLAGS=$CMAKE_BASE_FLAGS ; \
rm -rf * && \
trace  && \

# mkl + librsb env
#export GUIX_ENV=${GUIX_MKL_ENV}${GUIX_BASE_ENV}$GUIX_RSB_ENV ; \
export CMAKE_FLAGS=${CMAKE_BASE_FLAGS}$CMAKE_RSB_FLAGS ; \
rm -rf * && \
trace  && \


# add all the results
csplit --digits=2 --quiet --prefix=outfile ../trace_t.out "/op/+1" "{*}" && \
for f in $(ls outfile*); do head -n -1 $f | sed "s@^@$(tail -n 1 $f)@" >> trace_t.csv ; done && \
csplit --digits=2 --quiet --prefix=outfile ../trace.out "/mk/+1" "{*}" && \
for f in $(ls outfile*); do head -n -1 $f | sed "s@^@$(tail -n 1 $f)@" >> trace_t.csv ; done && \
cp trace_t.csv ..
