#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1011
#define TIMER_LEVEL_MIN 0

#include <maphys.hpp>
#include <maphys/solver/IRSolver.hpp>
#include <maphys/solver/GMRES.hpp>
#include <maphys/solver/Mumps.hpp>
#include <maphys/solver/CastOperator.hpp>
#include <maphys/dist/MPI.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>

#include <random>

using namespace maphys;

void usage(char ** argv){
  std::cerr << "Usage: " << std::string(argv[0]) << " matrix_file tolerance zeta max_iter_inner max_iter_outer restart orthogonalization trace_backward_error=0 use_exact_solver=0\n";
  std::cerr << "zeta sets both compression precision and inner solver tolerance (>0)\n";
  std::cerr << "orthogonalization: CGS, CGS2, MGS or MGS2\n";
  std::cerr << "trace_backward_error / use_exact_solver: 0 for false, anything else for true\n";
}

struct test_parameters{
  std::string mat_filename;
  double tol;
  double zeta;
  int max_iter_inner;
  int max_iter_outer;
  int restart;
  Ortho ortho;
  std::string ortho_str;
  bool trace_be;
  bool use_exact_solver;
};

template<typename Solver>
void solve(const test_parameters& params, const PartMatrix<SparseMatrixCSC<double>>& A, const PartVector<Vector<double>>& b){
  //double normA = A.frobenius_norm();
  //double normB = b.norm();

  Solver IR(A);
  auto& gmres = IR.get_inner_solver().inner_op;
  const bool is_root = (MMPI::rank() == 0);

  gmres.setup(parameters::tolerance<float>{static_cast<float>(params.zeta)},
              parameters::copy_A<bool>{true},
              parameters::max_iter<int>{params.max_iter_inner},
              parameters::restart<int>{params.restart},
              parameters::compression_accuracy<double>{params.zeta},
              parameters::orthogonalization<Ortho>{params.ortho},
              parameters::verbose_mem<bool>{true},
              parameters::always_true_residual<bool>{params.trace_be},
              //parameters::alpha<float>{static_cast<float>(normA)},
              parameters::verbose<bool>{is_root});

  auto setup_gmres_be_params = [&gmres](const PartMatrix<SparseMatrixCSC<double>>& m, const PartVector<Vector<double>>& v) {
                                 (void) m;
                                 //gmres.setup(parameters::beta<float>{static_cast<float>(v.norm())});
                               };

  IR.setup(parameters::tolerance<double>{static_cast<double>(params.tol)},
           parameters::max_iter<int>{params.max_iter_outer},
           //parameters::alpha<double>{normA},
           //parameters::beta<double>{normB},
           parameters::setup_inner_solver<std::function<void(const PartMatrix<SparseMatrixCSC<double>>&, const PartVector<Vector<double>>&)>>{setup_gmres_be_params},
           parameters::verbose<bool>{is_root});

  PartVector<Vector<double>> X = IR * b;

  if(is_root){
    IR.display();
    std::cout << "Cumulated iterations: " << IR.get_n_cumul_iter() << '\n';
  }
}

void test(char ** argv){

  test_parameters params;
  params.mat_filename = std::string(argv[1]);
  params.tol = std::stof(std::string(argv[2]));
  params.zeta = std::stof(std::string(argv[3]));
  params.max_iter_inner = std::stoi(std::string(argv[4]));
  params.max_iter_outer = std::stoi(std::string(argv[5]));
  params.restart = std::stoi(std::string(argv[6]));

  params.trace_be = false;
  params.use_exact_solver = false;
  params.trace_be = !(std::string(argv[8]) == std::string("0"));
  params.use_exact_solver = !(std::string(argv[9]) == std::string("0"));
  params.ortho_str = std::string(argv[7]);

  if(params.ortho_str == std::string("CGS")) { params.ortho = Ortho::CGS; }
  else if(params.ortho_str == std::string("CGS2")) { params.ortho = Ortho::CGS2; }
  else if(params.ortho_str == std::string("MGS")) { params.ortho = Ortho::MGS; }
  else if(params.ortho_str == std::string("MGS2")) { params.ortho = Ortho::MGS2; }
  else{
    std::cerr << "Orthogonalization not recognized\n";
    usage(argv);
  }

  int n_subdomains = MMPI::size();
  const bool is_root = (MMPI::rank() == 0);

  if(is_root){
    std::cout << " - Number of subdomains: " << n_subdomains << '\n'
              << " - Matrix file: " << params.mat_filename << '\n'
              << " - Tolerance: " << params.tol << '\n'
              << " - Zeta: " << params.zeta << '\n'
              << " - Max iter (inner solver): " << params.max_iter_inner << '\n'
              << " - Max iter (outer solver): " << params.max_iter_outer << '\n'
              << " - Restart: " << params.restart << '\n'
              << " - Orthogonalization: " << params.ortho_str << '\n'
              << " - Trace backward error: " << std::boolalpha << params.trace_be << '\n'
              << " - Use exact solver: " << params.use_exact_solver << '\n';
  }

  auto p = bind_subdomains(n_subdomains);

  using PMat = PartMatrix<SparseMatrixCSC<double>>;
  using PVect = PartVector<Vector<double>>;

  PMat A(p);
  PVect b(p);
  {
    PartMatrix<SparseMatrixCOO<double>> A_coo(p);
    load_subdomains_and_data(params.mat_filename, n_subdomains, p, A_coo, b);
    for(int sd_id : A_coo.get_sd_ids()){
      auto& loc_A_coo = A_coo.get_local_matrix(sd_id);
      loc_A_coo.fill_tri_to_full();
      loc_A_coo.set_property(MatrixSymmetry::general);
      A.add_subdomain(sd_id, loc_A_coo.to_csc());
    }
  }

  int seed = 1337;
  //std::random_device rd;
  //seed = rd() // for true random
  std::mt19937 gen(seed);
  std::uniform_real_distribution<> dis(double{-1}, double{1});

  PVect x_exp = b;
  for(int sd_id : x_exp.get_sd_ids()){
    auto& x_loc = x_exp.get_local_vector(sd_id);
    auto M = n_rows(x_loc);
    // for(size_t k = 0; k < M; ++k) x_loc[k] = static_cast<double>(dis(gen));
    for(size_t k = 0; k < M; ++k) x_loc[k] = double{1};
  }

  x_exp.disassemble();
  x_exp.assemble();

  b = A * x_exp;

  SZ_compressor_init();

  using LowPMat = PartMatrix<SparseMatrixCSC<float>>;
  using LowPVect = PartVector<Vector<float>>;
  using DirectSolver = Mumps<SparseMatrixCSC<float>, Vector<float>>;
  using PCD = AdditiveSchwarz<LowPMat, LowPVect, DirectSolver>;
  //using PCD = DiagonalPrecond<LowPMat, LowPVect>;

  using LowPrecSolver = GMRES<LowPMat, LowPVect, PCD>;
  using InnerSolver = CastOperator<PMat, PVect, LowPrecSolver>;

  using Solver = IRSolver<PMat, PVect, InnerSolver>;
  solve<Solver>(params, A, b);

  SZ_compressor_finalize();
}

int main(int argc, char ** argv){

  if(argc != 10){
    usage(argv);
    return 1;
  }

  MMPI::init();

  test(argv);

  std::vector<std::string> keys{"GMRES: get_W", "GMRES: orthogonalization", "GMRES: least_squares", "GMRES: update_sol", "GMRES iteration 0", "GMRES iteration", "IR iteration", "Mumps analysis", "Mumps facto", "Mumps solve"};
  //std::vector<std::string> keys{"GMRES: get_W", "GMRES: orthogonalization", "GMRES: least_squares", "GMRES: update_sol", "GMRES iteration 0", "GMRES iteration", "IR iteration"};
  if(MMPI::rank() == 0){
    for(auto k : keys){
      std::cout << k << ": " << Timer<0>::get_event_cumul_time(k) << '\n';
    }
  }

  MMPI::finalize();

  return 0;
}
