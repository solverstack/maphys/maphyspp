#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1011
#define TIMER_LEVEL_MIN 0

#include <maphys.hpp>
#include <maphys/solver/IRSolver.hpp>
#include <maphys/solver/GMRES.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#if defined(MPH_SDS_USE_MUMPS)
#include <maphys/dist/MPI.hpp>
#include <maphys/solver/Mumps.hpp>
#endif

#include <random>

using namespace maphys;

void usage(char ** argv){
  std::cerr << "Usage: " << std::string(argv[0]) << " s/d matrix_file tolerance zeta max_iter_inner max_iter_outer restart orthogonalization trace_backward_error=0 use_exact_solver=0\n";
  std::cerr << "zeta sets both compression precision and inner solver tolerance (>0)\n";
  std::cerr << "orthogonalization: CGS, CGS2, MGS or MGS2\n";
  std::cerr << "trace_backward_error / use_exact_solver: 0 for false, anything else for true\n";
}

struct test_parameters{
  std::string mat_filename;
  double tol;
  double zeta;
  int max_iter_inner;
  int max_iter_outer;
  int restart;
  Ortho ortho;
  std::string ortho_str;
  bool trace_be;
  bool use_exact_solver;
};

template<typename Solver, typename Scalar>
void solve(const test_parameters& params, const SparseMatrixCSC<Scalar>& A, const Vector<Scalar>& b){
  using Real = typename arithmetic_real<Scalar>::type;
  //Real normA = A.frobenius_norm();
  //Real normB = b.norm();

  Solver IR(A);
  auto& gmres = IR.get_inner_solver();

  gmres.setup(parameters::tolerance<Real>{static_cast<Real>(params.zeta)},
              parameters::max_iter<int>{params.max_iter_inner},
              parameters::restart<int>{params.restart},
              parameters::compression_accuracy<double>{params.zeta},
              parameters::orthogonalization<Ortho>{params.ortho},
              parameters::verbose_mem<bool>{true},
              parameters::always_true_residual<bool>{params.trace_be},
              //parameters::alpha<Real>{normA},
              parameters::verbose<bool>{true});

  auto setup_gmres_be_params = [&gmres](const SparseMatrixCSC<Scalar>& m, const Vector<Scalar>& v) {
    (void) m;
    (void) v;
    //gmres.setup(parameters::beta<Real>{v.norm()});
  };

  IR.setup(parameters::tolerance<Real>{static_cast<Real>(params.tol)},
           parameters::max_iter<int>{params.max_iter_outer},
           //parameters::alpha<Real>{normA},
           //parameters::beta<Real>{normB},
           parameters::setup_inner_solver<std::function<void(const SparseMatrixCSC<Scalar>&, const Vector<Scalar>&)>>{setup_gmres_be_params},
           parameters::verbose<bool>{true});

  Vector<Scalar> X = IR * b;

  IR.display();
  std::cout << "Cumulated iterations: " << IR.get_n_cumul_iter() << '\n';
}

template<typename Scalar>
void test(char ** argv){

  test_parameters params;
  params.mat_filename = std::string(argv[2]);
  params.tol = std::stof(std::string(argv[3]));
  params.zeta = std::stof(std::string(argv[4]));
  params.max_iter_inner = std::stoi(std::string(argv[5]));
  params.max_iter_outer = std::stoi(std::string(argv[6]));
  params.restart = std::stoi(std::string(argv[7]));

  params.trace_be = false;
  params.use_exact_solver = false;
  params.trace_be = !(std::string(argv[9]) == std::string("0"));
  params.use_exact_solver = !(std::string(argv[10]) == std::string("0"));
  params.ortho_str = std::string(argv[8]);

  if(params.ortho_str == std::string("CGS")) { params.ortho = Ortho::CGS; }
  else if(params.ortho_str == std::string("CGS2")) { params.ortho = Ortho::CGS2; }
  else if(params.ortho_str == std::string("MGS")) { params.ortho = Ortho::MGS; }
  else if(params.ortho_str == std::string("MGS2")) { params.ortho = Ortho::MGS2; }
  else{
    std::cerr << "Orthogonalization not recognized\n";
    usage(argv);
  }

  std::cout << " - Arithmetic: " << arithmetic_name<Scalar>::name << '\n'
            << " - Matrix file: " << params.mat_filename << '\n'
            << " - Tolerance: " << params.tol << '\n'
            << " - Zeta: " << params.zeta << '\n'
            << " - Max iter (inner solver): " << params.max_iter_inner << '\n'
            << " - Max iter (outer solver): " << params.max_iter_outer << '\n'
            << " - Restart: " << params.restart << '\n'
            << " - Orthogonalization: " << params.ortho_str << '\n'
            << " - Trace backward error: " << std::boolalpha << params.trace_be << '\n'
            << " - Use exact solver: " << params.use_exact_solver << '\n';

  auto get_A = [&params](){
                 SparseMatrixCOO<Scalar> mat;
                 mat.from_matrix_market_file(params.mat_filename);
                 mat.fill_tri_to_full();
                 mat.set_property(MatrixSymmetry::general);
                 return mat;
               };
  const SparseMatrixCSC<Scalar> A(get_A().to_csc());

  int seed = 1337;
  //std::random_device rd;
  //seed = rd() // for true random
  std::mt19937 gen(seed);
  std::uniform_real_distribution<> dis(double{-1}, double{1});
  auto M = n_rows(A);
  Vector<Scalar> x_exp(M);
  for(size_t k = 0; k < M; ++k) x_exp[k] = static_cast<Scalar>(dis(gen));
  Vector<Scalar> b = A * x_exp;

  SZ_compressor_init();

  using InnerSolver = GMRES<SparseMatrixCSC<Scalar>, Vector<Scalar>, DiagonalPrecond<SparseMatrixCSC<Scalar>, Vector<Scalar>>>;

#if defined(MPH_SDS_USE_MUMPS)
  if(params.use_exact_solver){
    using ExactSolver = Mumps<SparseMatrixCSC<Scalar>, Vector<Scalar>>;
    using Solver = IRSolver<SparseMatrixCSC<Scalar>, Vector<Scalar>, InnerSolver, ExactSolver>;
    solve<Solver, Scalar>(params, A, b);
  }
  else{
    using Solver = IRSolver<SparseMatrixCSC<Scalar>, Vector<Scalar>, InnerSolver>;
    solve<Solver, Scalar>(params, A, b);
  }
#else
  using Solver = IRSolver<SparseMatrixCSC<Scalar>, Vector<Scalar>, InnerSolver>;
  solve<Solver, Scalar>(params, A, b);
#endif

  SZ_compressor_finalize();
}

int main(int argc, char ** argv){

  if(argc != 11){
    usage(argv);
    return 1;
  }

#if defined(MPH_SDS_USE_MUMPS)
  MMPI::init();
#endif

  if(std::string(argv[1]) == std::string("s")){
    test<float>(argv);
  }
  else if(std::string(argv[1]) == std::string("d")){
    test<double>(argv);
  }
  else{
    usage(argv);
#if defined(MPH_SDS_USE_MUMPS)
    MMPI::finalize();
#endif
    return 1;
  }

#if defined(MPH_SDS_USE_MUMPS)
  MMPI::finalize();
#endif

  std::vector<std::string> keys{"GMRES: get_W", "GMRES: orthogonalization", "GMRES: least_squares", "GMRES: update_sol", "GMRES iteration 0", "GMRES iteration", "IR iteration"};
  for(auto k : keys){
    std::cout << k << ": " << Timer<0>::get_event_cumul_time(k) << '\n';
  }

  std::ofstream fileout{"iter_time.csv", std::ios::out};
  MAPHYSPP_ASSERT(fileout, "iter_time.csv: could not be opened");

  fileout << "Iteration,Instant\n";
  auto& t0 = timer::timer_t0;

  using duration = std::chrono::duration<double>;

  const auto& ite0_events = Timer<0>::get_events("GMRES iteration 0");
  const auto& ite_events = Timer<0>::get_events("GMRES iteration");

  int tot_ite = 1;
  int k_ite = 0;
  for(const auto& e_ite0 : ite0_events){
    //duration i0_start = e_ite0.start_time - t0;
    duration i0_end = e_ite0.end_time - t0;

    while(k_ite < static_cast<int>(ite_events.size())){
      duration i_start = ite_events[k_ite].start_time - t0;
      //duration i_end = ite_events[k_ite].end_time - t0;
      if(i_start > i0_end) break;
      fileout << tot_ite++ << ',';
      timer::Event::write_instant(ite_events[k_ite].end_time, fileout, "\n");
      ++k_ite;
    }
    fileout << tot_ite++ << ',';
    timer::Event::write_instant(e_ite0.end_time, fileout, "\n");
  }

  return 0;
}
