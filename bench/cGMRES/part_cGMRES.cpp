#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1011
#define TIMER_LEVEL_MIN 0

#include <maphys.hpp>
#include <maphys/solver/GMRES.hpp>
#include <maphys/solver/Mumps.hpp>
#include <maphys/dist/MPI.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>
#include <random>

using namespace maphys;

void usage(char ** argv){
  if(MMPI::rank() != 0) return;
  std::cerr << "Usage: " << std::string(argv[0]) << " s/d matrix_directory tolerance zeta max_iter restart orthogonalization [trace_backward_error=0]\n";
  std::cerr << "Use zeta < 0 for no compression\n";
  std::cerr << "orthogonalization: CGS, CGS2, MGS or MGS2\n";
  std::cerr << "trace_backward_error: 0 for false, anything else for true\n";
}

template<typename Scalar>
void test(int argc, char ** argv){

  using Real = typename arithmetic_real<Scalar>::type;

  int n_subdomains = MMPI::size();
  const bool is_root = (MMPI::rank() == 0);

  const std::string mat_filename = std::string(argv[2]);
  const Real tol = std::stof(std::string(argv[3]));
  const double zeta = std::stof(std::string(argv[4]));
  const int max_iter = std::stoi(std::string(argv[5]));
  const int restart = std::stoi(std::string(argv[6]));
  const std::string ortho_str = std::string(argv[7]);

  bool trace_be = false;
  if(argc == 9) trace_be = !(std::string(argv[8]) == std::string("0"));

  Ortho ortho = Ortho::CGS;
  if(ortho_str == std::string("CGS")) { ortho = Ortho::CGS; }
  else if(ortho_str == std::string("CGS2")) { ortho = Ortho::CGS2; }
  else if(ortho_str == std::string("MGS")) { ortho = Ortho::MGS; }
  else if(ortho_str == std::string("MGS2")) { ortho = Ortho::MGS2; }
  else{
    std::cerr << "Orthogonalization not recognized\n";
    usage(argv);
  }

  if(is_root){
    std::cout << " - Number of subdomains: " << n_subdomains << '\n'
              << " - Arithmetic: " << arithmetic_name<Scalar>::name << '\n'
              << " - Matrix directory: " << mat_filename << '\n'
              << " - Tolerance: " << tol << '\n'
              << " - Zeta: " << zeta << '\n'
              << " - Max iter: " << max_iter << '\n'
              << " - Restart: " << restart << '\n'
              << " - Orthogonalization: " << ortho_str << '\n'
              << " - Trace backward error: " << std::boolalpha << trace_be << '\n';
  }

  auto p = bind_subdomains(n_subdomains);

  using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
  using PVect = PartVector<Vector<Scalar>>;

  PMat A(p);
  PVect b(p);
  {
    PartMatrix<SparseMatrixCOO<Scalar>> A_coo(p);
    load_subdomains_and_data(mat_filename, n_subdomains, p, A_coo, b);
    for(int sd_id : A_coo.get_sd_ids()){
      auto& loc_A_coo = A_coo.get_local_matrix(sd_id);
      loc_A_coo.fill_tri_to_full();
      loc_A_coo.set_property(MatrixSymmetry::general);
      A.add_subdomain(sd_id, loc_A_coo.to_csc());
    }
  }

  // Evaluate A norm
  Real normA = A.frobenius_norm();
  if(is_root) std::cout << "A norm: " << normA << '\n';

  int seed = 1337;
  //std::random_device rd;
  //seed = rd() // for true random
  std::mt19937 gen(seed);
  std::uniform_real_distribution<> dis(double{-1}, double{1});

  PVect x_exp = b;
  for(int sd_id : x_exp.get_sd_ids()){
    auto& x_loc = x_exp.get_local_vector(sd_id);
    auto M = n_rows(x_loc);
    // for(size_t k = 0; k < M; ++k) x_loc[k] = static_cast<Scalar>(dis(gen));
    for(size_t k = 0; k < M; ++k) x_loc[k] = Scalar{1};
  }

  x_exp.disassemble();
  x_exp.assemble();

  b = A * x_exp;

  //Real normB = b.norm();

  SZ_compressor_init();

  using DirectSolver = Mumps<SparseMatrixCSC<Scalar>, Vector<Scalar>>;
  using PCD = AdditiveSchwarz<decltype(A), decltype(b), DirectSolver>;

  GMRES<PMat, PVect, PCD> gmres(A);
  gmres.setup(parameters::tolerance<Real>{tol},
              parameters::max_iter<int>{max_iter},
              parameters::restart<int>{restart},
              parameters::compression_accuracy<double>{zeta},
              parameters::orthogonalization<Ortho>{ortho},
              parameters::verbose_mem<bool>{true},
              parameters::always_true_residual<bool>{trace_be},
              //parameters::alpha<Real>{normA},
              //parameters::beta<Real>{normB},
              parameters::verbose<bool>{is_root});

  PVect X = gmres * b;

  if(is_root){
    gmres.display();
    std::vector<std::string> keys{"GMRES: get_W", "GMRES: orthogonalization", "GMRES: least_squares", "GMRES: update_sol", "GMRES iteration 0", "GMRES iteration", "Iterative solve", "Mumps analysis", "Mumps facto", "Mumps solve"};
    for(auto k : keys){
      std::cout << k << ": " << Timer<0>::get_event_cumul_time(k) << '\n';
    }

    std::ofstream fileout{"iter_time.csv", std::ios::out};
    MAPHYSPP_ASSERT(fileout, "iter_time.csv: could not be opened");

    fileout << "Iteration,Instant\n";

    const auto& ite0_events = Timer<0>::get_events("GMRES iteration 0");
    const auto& ite_events = Timer<0>::get_events("GMRES iteration");

    fileout << "0,";
    timer::Event::write_instant(ite0_events[0].end_time, fileout, "\n");
    int tot_ite = 1;

    for(const auto& e_ite : ite_events){
      fileout << tot_ite++ << ',';
      timer::Event::write_instant(e_ite.end_time, fileout, "\n");
    }
  }

  SZ_compressor_finalize();
}

int main(int argc, char ** argv){

  MMPI::init();

  if(argc != 8 && argc != 9){
    usage(argv);
    MMPI::finalize();
    return 1;
  }

  if(std::string(argv[1]) == std::string("s")){
    test<float>(argc, argv);
  }
  else if(std::string(argv[1]) == std::string("d")){
    test<double>(argc, argv);
  }
  else{
    usage(argv);
    MMPI::finalize();
    return 1;
  }

  MMPI::finalize();
  return 0;
}
