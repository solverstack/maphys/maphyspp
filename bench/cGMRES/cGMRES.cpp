#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1011
#define TIMER_LEVEL_MIN 0

#include <maphys.hpp>
#include <maphys/solver/GMRES.hpp>
#include <random>

using namespace maphys;

void usage(char ** argv){
  std::cerr << "Usage: " << std::string(argv[0]) << " s/d matrix_file tolerance zeta max_iter restart orthogonalization [trace_backward_error=0]\n";
  std::cerr << "Use zeta < 0 for no compression\n";
  std::cerr << "orthogonalization: CGS, CGS2, MGS or MGS2\n";
  std::cerr << "trace_backward_error: 0 for false, anything else for true\n";
}

template<typename Scalar>
void test(int argc, char ** argv){

  using Real = typename arithmetic_real<Scalar>::type;

  const std::string mat_filename = std::string(argv[2]);
  const Real tol = std::stof(std::string(argv[3]));
  const double zeta = std::stof(std::string(argv[4]));
  const int max_iter = std::stoi(std::string(argv[5]));
  const int restart = std::stoi(std::string(argv[6]));
  const std::string ortho_str = std::string(argv[7]);

  bool trace_be = false;
  if(argc == 9) trace_be = !(std::string(argv[8]) == std::string("0"));

  Ortho ortho = Ortho::CGS;
  if(ortho_str == std::string("CGS")) { ortho = Ortho::CGS; }
  else if(ortho_str == std::string("CGS2")) { ortho = Ortho::CGS2; }
  else if(ortho_str == std::string("MGS")) { ortho = Ortho::MGS; }
  else if(ortho_str == std::string("MGS2")) { ortho = Ortho::MGS2; }
  else{
    std::cerr << "Orthogonalization not recognized\n";
    usage(argv);
  }

  std::cout << " - Arithmetic: " << arithmetic_name<Scalar>::name << '\n'
            << " - Matrix file: " << mat_filename << '\n'
            << " - Tolerance: " << tol << '\n'
            << " - Zeta: " << zeta << '\n'
            << " - Max iter: " << max_iter << '\n'
            << " - Restart: " << restart << '\n'
            << " - Orthogonalization: " << ortho_str << '\n'
            << " - Trace backward error: " << std::boolalpha << trace_be << '\n';

  auto get_A = [mat_filename](){
                 SparseMatrixCOO<Scalar> mat;
                 mat.from_matrix_market_file(mat_filename);
                 mat.fill_tri_to_full();
                 mat.set_property(MatrixSymmetry::general);
                 return mat;
               };
  const SparseMatrixCSC<Scalar> A(get_A().to_csc());

  // Evaluate A norm
  Real normA = A.frobenius_norm();
  std::cout << "A norm: " << normA << '\n';

  int seed = 1337;
  //std::random_device rd;
  //seed = rd() // for true random
  std::mt19937 gen(seed);
  std::uniform_real_distribution<> dis(double{-1}, double{1});
  auto M = n_rows(A);
  Vector<Scalar> x_exp(M);
  //for(size_t k = 0; k < M; ++k) x_exp[k] = static_cast<Scalar>(dis(gen));
  for(size_t k = 0; k < M; ++k) x_exp[k] = Scalar{1};
  Vector<Scalar> b = A * x_exp;

  //Real normB = b.norm();

  SZ_compressor_init();

  GMRES<SparseMatrixCSC<Scalar>, Vector<Scalar>> gmres(A);
  gmres.setup(parameters::tolerance<Real>{tol},
              parameters::max_iter<int>{max_iter},
              parameters::restart<int>{restart},
              parameters::compression_accuracy<double>{zeta},
              parameters::orthogonalization<Ortho>{ortho},
              parameters::verbose_mem<bool>{true},
              parameters::verbose_ortho_loss<bool>{true},
              parameters::always_true_residual<bool>{trace_be},
              //parameters::alpha<Real>{normA},
              //parameters::beta<Real>{normB},
              parameters::verbose<bool>{true});

  Vector<Scalar> X = gmres * b;

  gmres.display();

  std::vector<std::string> keys{"GMRES: get_W", "GMRES: orthogonalization", "GMRES: least_squares", "GMRES: update_sol", "GMRES iteration 0", "GMRES iteration", "Iterative solve"};
  for(auto k : keys){
    std::cout << k << ": " << Timer<0>::get_event_cumul_time(k) << '\n';
  }

  SZ_compressor_finalize();
}

int main(int argc, char ** argv){

  if(argc != 8 && argc != 9){
    usage(argv);
    return 1;
  }

  if(std::string(argv[1]) == std::string("s")){
    test<float>(argc, argv);
  }
  else if(std::string(argv[1]) == std::string("d")){
    test<double>(argc, argv);
  }
  else{
    usage(argv);
    return 1;
  }

  return 0;
}
