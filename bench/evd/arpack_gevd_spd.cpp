#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0

#include <iostream>
#include <maphys.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/linalg/EigenArpackSolver.hpp>

using namespace maphys;
using Scalar = double;
using Real = double;

void test(const int M, const int n_v){
  using DenseMat = DenseMatrix<Scalar>;
  using SparseMat = SparseMatrixCOO<Scalar>;
  using SolverSparse = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
  using SolverDense = BlasSolver<DenseMat, Vector<Scalar>>;

  // We want to find U so that A U = LAM B U
  // With U the n_v eigenvectors associated to the smallest eigenvalues
  auto build_mat = [M](){
                     DenseMat mat = test_matrix::random_matrix<Scalar, DenseMat>(3*M, Real{-1}, Real{1}, M, M).matrix;
                     for(int i = 0; i < M; ++i) mat(i,i) += (5 + i);
                     mat *= mat.t();
                     for(int j = 1; j < M; ++j){
                       for(int i = 0; i < j; ++i){
                         mat(i,j) = 0;
                       }
                     }
                     mat.set_spd(MatrixStorage::lower);
                     return mat;
                   };

  const DenseMat A(build_mat());
  const DenseMat B(build_mat());

  std::vector<double> results;

  {
    Timer<1> ttt("expe1"); // LAPACK - dense
    auto [lambda, U] = blas_kernels::gen_eigh_smallest(A, B, n_v);
    ttt.stop();
    results.push_back(ttt.get_event_cumul_time("expe1"));
  }

  //for(const auto& l : lambda) std::cout << l << ',';
  //std::cout << '\n';

  {
    Timer<1> ttt("expe2"); // ARPACK - dense - mode 1
    auto [lambda, U] = EigenArpackSolver<DenseMat,DenseMat,Vector<Scalar>,SolverDense,1>::gen_eigh_smallest(A, B, n_v);
    ttt.stop();
    results.push_back(ttt.get_event_cumul_time("expe2"));
  }

  //for(const auto& l : lambda) std::cout << l << ',';
  //std::cout << '\n';

  {
    Timer<1> ttt("expe3"); // ARPACK - dense - mode 2
    auto [lambda, U] = EigenArpackSolver<DenseMat,DenseMat,Vector<Scalar>,SolverDense, 2>::gen_eigh_smallest(A, B, n_v);
    ttt.stop();
    results.push_back(ttt.get_event_cumul_time("expe3"));
  }

  //for(const auto& l : lambda) std::cout << l << ',';
  //std::cout << '\n';

  const SparseMat A_sp(A);
  const SparseMat B_sp(B);

  {
    Timer<1> ttt("expe4"); // ARPACK - sparse - mode 1
    auto [lambda, U] = EigenArpackSolver<DenseMat,SparseMat,Vector<Scalar>,SolverSparse, 1>::gen_eigh_smallest(A_sp, B_sp, n_v);
    ttt.stop();
    results.push_back(ttt.get_event_cumul_time("expe4"));
  }

  //for(const auto& l : lambda) std::cout << l << ',';
  //std::cout << '\n';

  {
    Timer<1> ttt("expe5"); // ARPACK - sparse - mode 2
    auto [lambda, U] = EigenArpackSolver<DenseMat,SparseMat,Vector<Scalar>,SolverSparse, 2>::gen_eigh_smallest(A_sp, B_sp, n_v);
    ttt.stop();
    results.push_back(ttt.get_event_cumul_time("expe5"));
  }

  //for(const auto& l : lambda) std::cout << l << ',';
  //std::cout << '\n';

  Timer<0>::reset();

  std::cout << M << ',' << n_v << ',';
  for(const auto& time : results) std::cout << time << ',';
  std::cout << '\n';
}

int main() {

  MMPI::init();

  std::cout << "M,n_v,LAPACK_dense,ARPACK_1_dense,ARPACK_2_dense,ARPACK_1_sparse,ARPACK_2_sparse\n";

  test(100, 2);
  test(500, 2);
  test(1000, 2);
  test(2000, 2);
  test(3000, 2);

  test(100, 5);
  test(500, 5);
  test(1000, 5);
  test(2000, 5);
  test(3000, 5);

  test(100, 10);
  test(500, 10);
  test(1000, 10);
  test(2000, 10);
  test(3000, 10);

  MMPI::finalize();

  return 0;
}
