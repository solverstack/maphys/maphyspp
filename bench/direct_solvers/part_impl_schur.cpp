#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0

#include <maphys.hpp>
#include <maphys/IO/ReadParam.hpp>
#include <maphys/dist/MPI.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/solver/PartSchurSolver.hpp>
#include <maphys/solver/BlasSolver.hpp>
#if defined(MPH_SDS_USE_QRMUMPS)
#include <maphys/solver/QrMumps.hpp>
#endif
#if defined(MPH_SDS_USE_MUMPS)
#include <maphys/solver/Mumps.hpp>
#endif
#if defined(MPH_SDS_USE_PASTIX)
#include <maphys/solver/Pastix.hpp>
#endif

using namespace maphys;
using Scalar = double;
using Real = double;

using LocMat = SparseMatrixCOO<Scalar>;
using LocVect = Vector<Scalar>;

using SpMat = PartMatrix<LocMat>;
using Vect = PartVector<LocVect>;

const bool root_verbose = true;

void usage(char ** argv){
  std::cerr << "Usage: " << std::string(argv[0]) << " subdomains_path n_subdomains\n";
}

template<typename DirectSolver>
void bench(const SpMat& A, const Vect& b, const std::string& benchname, std::map<std::string, int>& n_iterations){

  using ImplSchur = ImplicitSchur<LocMat,LocVect,DirectSolver>;
  using Solver_S = ConjugateGradient<PartOperator<ImplSchur,LocMat,LocVect>,Vect>;
  using Solver = PartSchurSolver<SpMat,Vect, DirectSolver,Solver_S>;
  bool verbose = (root_verbose && MMPI::rank() == 0);

  Solver hybrid_solver;
  Solver_S& cg = hybrid_solver.get_solver_S();
  cg.setup(parameters::max_iter{5000},
           parameters::tolerance{1e-5},
           parameters::verbose{verbose});
  hybrid_solver.setup(parameters::A{A},
                      parameters::verbose{verbose});

  if(MMPI::rank() == 0) std::cout << "##### Bench: " << benchname << '\n';

  Timer<1> t(benchname);
  Vect X = hybrid_solver * b;
  if(MMPI::rank() == 0) cg.display("CG");
  n_iterations[benchname] = cg.get_n_iter();
}

int main(int argc, char ** argv) {

  Timer<0> tt("Total time");

  MMPI::init();{
    if(argc != 3){
      usage(argv);
      MMPI::finalize();
      return 1;
    }

    const std::string mat_filename = std::string(argv[1]);
    const int n_subdomains = std::stoi(argv[2]);

    auto p = bind_subdomains(n_subdomains);
    SpMat Acoo(p);

    Vect b(p);
    load_subdomains_and_data(mat_filename, n_subdomains, p, Acoo, b);

    Acoo.apply_on_data([](LocMat& m){
                         m.set_spd(MatrixStorage::lower);
                         m.order_indices();
                       });

    std::map<std::string, int> n_iterations;

#if defined(MPH_SDS_USE_MUMPS)
    bench<Mumps<LocMat,LocVect>>(Acoo, b, "Mumps", n_iterations);
#endif
#if defined(MPH_SDS_USE_PASTIX)
    bench<Pastix<LocMat,LocVect>>(Acoo, b, "Pastix", n_iterations);
#endif
#if defined(MPH_SDS_USE_QRMUMPS)
    bench<QrMumps<LocMat,LocVect>>(Acoo, b, "QrMumps", n_iterations);
#endif

    MMPI::barrier();
    if(MMPI::rank() == 0){
      tt.results(std::cout, /*detailed*/ false);
    }

    tt.stop();

    std::vector<std::string> keys = Timer<1>::get_event_keys();
    std::sort(keys.begin(), keys.end());
    std::map<std::string, double> output_times;
    for(std::string& key : keys){
      double time = Timer<1>::get_event_cumul_time(key);
      double max_time;
      MMPI::reduce(&time, &max_time, 1, MPI_MAX, 0, MPI_COMM_WORLD);
      output_times[key] = max_time;
    }

    if(MMPI::rank() == 0){
      std::cout << "\n-----------------------------\n";
      for(const auto& [key, time] : output_times){
	std::cout << key << ": " << time << '\n';
      }
      std::cout << "\n-----------------------------\n";
      std::cout << "Number of iterations\n";
      for(const auto& [solver, niter] : n_iterations){
	std::cout << "Niter-" << solver << ": " << niter << '\n';
      }
    }

  }
  MMPI::finalize();

  return 0;
}
