#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0

#include <maphys.hpp>
#include <maphys/IO/ReadParam.hpp>
#include <maphys/dist/MPI.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/solver/QrMumps.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>

using namespace maphys;
using Scalar = double;
using Real = double;

void usage(char ** argv){
  std::cerr << "Usage: " << std::string(argv[0]) << " path_to_partitions n_subdomains\n";
}

int main(int argc, char ** argv) {

  int rank;
  
  MMPI::init();
  {
    Timer<0> t_tot("Total time");
    rank = MMPI::rank();
    
    if(argc != 3){
      usage(argv);
      return 1;
    }
    
    const std::string path_to_part = std::string(argv[1]);
    const int n_sd = std::stoi(argv[2]);

    auto p = bind_subdomains(n_sd);

    // Load matrix and RHS
    PartMatrix<SparseMatrixCOO<Scalar>> A(p);
    PartVector<Vector<Scalar>> B(p);
    load_baton_subdomains(path_to_part, n_sd, p, A, B);

    auto set_spd_lower = [](SparseMatrixCOO<Scalar>& loc_mat){
                           loc_mat.set_spd(MatrixStorage::lower);
                           loc_mat.order_indices();
                         };
    A.apply_on_data(set_spd_lower);

    // b must be assembled!
    B.assemble();

    using SparseDirectSolver = QrMumps<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using Pcd = AdditiveSchwarz<decltype(A), decltype(B), SparseDirectSolver>;

    ConjugateGradient<decltype(A), decltype(B), Pcd> cg_as;
    cg_as.setup(parameters::A{A},
                parameters::tolerance{1e-8},
                parameters::max_iter{10000},
                parameters::verbose{rank == 0});

    auto setup_qrm = [](SparseDirectSolver& qrm, const SparseMatrixCOO<Scalar>& locA)
                     {
                       qrm.set_n_cpu_gpu(-1, -1);
                       qrm.setup(locA);
                     };
    cg_as.get_preconditioner().setup(parameters::setup_local_solver<std::function<void(SparseDirectSolver&, const SparseMatrixCOO<Scalar>&)>>{setup_qrm});

    auto X = cg_as * B;

    if(rank == 0) cg_as.display();
  }
  //Timer<0>::summary_results();

  const std::vector<std::string> keys{"CG iteration", "QrMumps LLT-solve", "Iterative solve", "Total time", "CG iteration 0", "Precond setup aS", "QrMumps facto", "Precond apply aS", "QrMumps analysis"};

  if(rank == 0){
    std::cout << "+---------+-----+-----+-----+\n";
    std::cout << "| Measure | max | avg | min |\n";
  }

  for(const std::string& key : keys){
    double time = Timer<1>::get_event_cumul_time(key);
    double max_time, min_time, sum_time;
    MMPI::reduce(&time, &max_time, 1, MPI_MAX, 0, MPI_COMM_WORLD);
    MMPI::reduce(&time, &min_time, 1, MPI_MIN, 0, MPI_COMM_WORLD);
    MMPI::reduce(&time, &sum_time, 1, MPI_SUM, 0, MPI_COMM_WORLD);
    if(rank == 0){
      double avg_time = (sum_time / MMPI::size());
      std::cout << "| " << key << " | " << max_time << " | " << avg_time << " | " << min_time << "|\n";
    }
  }

  MMPI::barrier();
  MMPI::finalize();

  return 0;
}
