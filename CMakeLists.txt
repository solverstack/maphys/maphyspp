cmake_minimum_required(VERSION 3.12)
# Need to support "cxx_std_20" for target_compile_features

project(maphyspp VERSION 1.1.9 LANGUAGES C CXX Fortran)

# to be able to use "PACKAGE"_ROOT env. var.
cmake_policy(SET CMP0074 NEW)
# to be able to use target_link_libraries on targets not defined on the same scope
cmake_policy(SET CMP0079 NEW)
# For fPIC when build static
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# User options
option(MAPHYSPP_USE_ARMADILLO "Compile examples and tests using Armadillo" OFF)
option(MAPHYSPP_USE_CHAMELEON "Compile examples and tests using Chameleon (dense direct solver)" OFF)
option(MAPHYSPP_USE_PASTIX "Compile examples and tests using pastix (sparse direct solver)" ON)
option(MAPHYSPP_USE_MUMPS "Compile examples and tests using mumps (sparse direct solver)" ON)
option(MAPHYSPP_USE_QRMUMPS "Compile examples and tests using qrmumps (sparse direct solver)" OFF)
option(MAPHYSPP_USE_EIGEN "Compile examples and tests using Eigen" OFF)
option(MAPHYSPP_USE_FABULOUS "Compile examples and tests using Fabulous" OFF)
option(MAPHYSPP_USE_SZ_COMPRESSOR "Compile examples and tests using the SZ compressor" OFF)
option(MAPHYSPP_USE_ARPACK "Compile examples and tests using arpack" ON)
option(MAPHYSPP_USE_PADDLE "Compile examples and tests using paddle" OFF)
option(MAPHYSPP_USE_RSB "Compile examples and tests using librsb" OFF)
option(MAPHYSPP_USE_RSB_SPBLAS "Use rsb library as sparse blas kernel" OFF)
option(MAPHYSPP_USE_MULTITHREAD "Use shared-memory parallelism (threads)" OFF)
option(MAPHYSPP_USE_LAPACKPP "Use blaspp/lapackpp library" ON)
option(MAPHYSPP_USE_TLAPACK "Use tlapack (header-only) library" OFF)

# C and Fortran drivers
option(MAPHYSPP_DRIVERS "Compile C++ drivers" ON)
option(MAPHYSPP_C_DRIVER "Compile C basic driver" ON)
option(MAPHYSPP_Fortran_DRIVER "Compile Fortran basic driver" ON)

# What to compile
option(MAPHYSPP_COMPILE_EXAMPLES "Compile examples" ON)
option(MAPHYSPP_COMPILE_TESTS "Compile tests" ON)
option(MAPHYSPP_COMPILE_BENCH "Compile benchmarks" OFF)

# Developer options
option(MAPHYSPP_CPPCHECK "Runs cppcheck" OFF)
option(MAPHYSPP_GCC_WARNINGS "Add many compilation warnings for GCC" OFF)

# Misc options
option(BUILD_SHARED_LIBS "Build shared libraries" ON)

# To expose to the use, will all dependencies
add_library(${CMAKE_PROJECT_NAME} INTERFACE)
target_include_directories(${CMAKE_PROJECT_NAME} INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
  )
target_include_directories(${CMAKE_PROJECT_NAME} INTERFACE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/timer/include>
  $<INSTALL_INTERFACE:include>
  )
target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE HAVE_LAPACK_CONFIG_H LAPACK_COMPLEX_CPP)

# Installation
# ------------
install(DIRECTORY include/ DESTINATION include FILES_MATCHING PATTERN "*.hpp")
install(FILES timer/include/Timer.hpp DESTINATION include)

# For the project, with the fewest dependencies possible
add_library(maphyspp_minimal INTERFACE)
target_include_directories(maphyspp_minimal INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/include ${CMAKE_CURRENT_SOURCE_DIR}/timer/include)
target_compile_definitions(maphyspp_minimal INTERFACE HAVE_LAPACK_CONFIG_H LAPACK_COMPLEX_CPP)

#######################
# Morse CMake modules #
#######################

# Add extra cmake module path and initialize morse cmake modules
# --------------------------------------------------------------
set(MORSE_CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules/morse_cmake/modules/")
list(APPEND CMAKE_MODULE_PATH ${MORSE_CMAKE_MODULE_PATH})
include(MorseInit)

### CPPCHECK

if( MAPHYSPP_CPPCHECK )
  find_program(CPPCHECK cppcheck)
  set(CMAKE_CXX_CPPCHECK "${CPPCHECK}"
    "--language=c++"
    "--platform=unix64"
    "--enable=all"
    "--force"
    "--inline-suppr"
    )
endif()

### Ctest

include("${CMAKE_SOURCE_DIR}/ctests.cmake")

# Need c++20
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

################################################
# REQUIRED DEPENDENCIES:
# - blaspp, lapackpp OR tlapack
# - MPI
# - Catch2 for tests
################################################

###########
# tlapack #
###########

if(MAPHYSPP_USE_TLAPACK)
  include(ExternalProject)

  ExternalProject_Add(tlapack
  PREFIX "${PROJECT_BINARY_DIR}"
  SOURCE_DIR ${CMAKE_SOURCE_DIR}/tlapack
  CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX}
             -DCMAKE_PREFIX_PATH=${CMAKE_INSTALL_PREFIX}
             -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
             -DBUILD_SHARED_LIBS=ON
	     -DBUILD_TESTING=OFF
	     -DBUILD_BLASPP_TESTS=OFF
	     -DBUILD_LAPACKPP_TESTS=OFF
	     -DBUILD_testBLAS_TESTS=OFF
	     -DBUILD_EXAMPLES=OFF
  BUILD_COMMAND make -j 4
  )

  set(TLAPACK_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/tlapack/include")
  target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_TLAPACK)
  target_include_directories(${CMAKE_PROJECT_NAME} SYSTEM INTERFACE
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/tlapack/include>
    $<INSTALL_INTERFACE:include>)
endif(MAPHYSPP_USE_TLAPACK)

if(MAPHYSPP_USE_LAPACKPP)

  ##########
  # blaspp #
  ##########
  if(NOT blaspp_FOUND)
    find_package(blaspp REQUIRED)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE blaspp)
    if(BLAS_LIBRARIES MATCHES "libmkl")
      set(MAPHYSPP_USE_MKL_SPBLAS "ON")
      target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_MKL_SPBLAS)
    endif()
  endif()

  ############
  # lapackpp #
  ############
  if(NOT lapackpp_FOUND)
    find_package(lapackpp REQUIRED)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE lapackpp)
  endif()

  target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_LAPACKPP)

endif(MAPHYSPP_USE_LAPACKPP)

if((NOT MAPHYSPP_USE_TLAPACK) AND (NOT MAPHYSPP_USE_LAPACKPP))
  message(FATAL_ERROR "One of MAPHYSPP_USE_TLAPACK and MAPHYSPP_USE_LAPACKPP must be ON")
endif()

#######
# mpi #
#######
if(NOT MPI_FOUND)
  find_package(MPI)
  if(NOT MPIEXEC_EXECUTABLE)
    set(MPIEXEC ${MPIEXEC_EXECUTABLE})
    if(NOT MPIEXEC_EXECUTABLE)
      set(MPIEXEC_EXECUTABLE "mpirun")
    endif()
  endif()
  target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE MPI::MPI_CXX)
endif()

##########
# Catch2 #
##########

if(MAPHYSPP_COMPILE_TESTS)
  add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/Catch2)
endif(MAPHYSPP_COMPILE_TESTS)

#########################
# OPTIONAL DEPENDENCIES #
#########################

#############
# Chameleon #
#############
if(MAPHYSPP_USE_CHAMELEON)
  if(NOT TARGET CHAMELEON::chameleon)
    message(STATUS "Looking for CHAMELEON")
    find_package(CHAMELEON REQUIRED)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE CHAMELEON::chameleon)
    # Disable Chameleon to be used by default for Blas/Lapack for now
    # Need to add MAPHYSPP_USE_CHAMELEON as definition to the executable explicitly
    # TODO: enable Chameleon by default as soon as its global context handling is properly managed
    #target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_CHAMELEON)
    set(CHAMELEON_FOUND TRUE)
  endif(NOT TARGET CHAMELEON::chameleon)
endif(MAPHYSPP_USE_CHAMELEON)

##########
# Pastix #
##########
if(MAPHYSPP_USE_PASTIX)
  if(NOT TARGET PASTIX::pastix)
    message(STATUS "Looking for PASTIX")
    find_package(PASTIX REQUIRED)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE PASTIX::pastix)
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_PASTIX)
    set(PASTIX_FOUND TRUE)
  endif(NOT TARGET PASTIX::pastix)
endif(MAPHYSPP_USE_PASTIX)

#########
# Mumps #
#########
if(MAPHYSPP_USE_MUMPS)
  if(NOT MUMPS_FOUND)
    message(STATUS "Looking for MUMPS")
    find_package(MUMPS REQUIRED)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE MORSE::MUMPS)
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_MUMPS)
  endif(NOT MUMPS_FOUND)
endif(MAPHYSPP_USE_MUMPS)

###########
# QrMumps #
###########
if(MAPHYSPP_USE_QRMUMPS)
  if(NOT TARGET qrm_common)
    message(STATUS "Looking for QRMUMPS")
    find_package(QRM REQUIRED)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE dqrm qrm_common)
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_QRMUMPS)
  endif(NOT TARGET qrm_common)
endif(MAPHYSPP_USE_QRMUMPS)

#########
# Eigen #
#########
if(MAPHYSPP_USE_EIGEN)
  message(STATUS "Looking for Eigen")
  if(NOT TARGET Eigen3::Eigen)
    find_package (Eigen3 REQUIRED NO_MODULE)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE Eigen3::Eigen)
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_EIGEN)
  endif(NOT TARGET Eigen3::Eigen)
endif(MAPHYSPP_USE_EIGEN)

#############
# Armadillo #
#############
if(MAPHYSPP_USE_ARMADILLO)
  if(NOT ARMADILLO_FOUND)
    message(STATUS "Looking for ARMADILLO")
    find_package(Armadillo REQUIRED)
    target_include_directories(${CMAKE_PROJECT_NAME} SYSTEM INTERFACE ${ARMADILLO_INCLUDE_DIRS})
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE ${ARMADILLO_LIBRARIES})
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_ARMADILLO)
    if(ARMADILLO_EXTRA_DEFINITIONS)
      target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE ${ARMADILLO_EXTRA_DEFINITIONS})
    endif()
  endif(NOT ARMADILLO_FOUND)
endif(MAPHYSPP_USE_ARMADILLO)

############
# Fabulous #
############
if(MAPHYSPP_USE_FABULOUS)
  if(NOT TARGET FABULOUS::fabulous_cpp)
    message(STATUS "Looking for Fabulous")
    find_package(Fabulous REQUIRED)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE FABULOUS::fabulous_cpp)
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_FABULOUS)
  endif()
endif(MAPHYSPP_USE_FABULOUS)

#################
# SZ compressor #
#################
if(MAPHYSPP_USE_SZ_COMPRESSOR)
  if(NOT TARGET SZ)
    message(STATUS "Looking for SZ")
    find_package(SZ REQUIRED)
    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE SZ)
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_SZ_COMPRESSOR)
  endif(NOT TARGET SZ)
endif()

##########
# arpack #
##########

if(MAPHYSPP_USE_ARPACK)
  if(NOT TARGET arpack)
    message(STATUS "Looking for ARPACK")
    message(STATUS "NOTE: if linking with ARPACK does not work, set manually variables ARPACK_LINK_DIRECTORIES and ARPACK_LINK_LIBRARIES")
    find_package(arpack-ng REQUIRED NAMES arpackng arpack-ng)

    set(_user_link_def "OFF")
    if(DEFINED ENV{ARPACK_LINK_DIRECTORIES})
      message(STATUS "Using ARPACK_LINK_DIRECTORIES = $ENV{ARPACK_LINK_DIRECTORIES}")
      set_target_properties(arpack PROPERTIES INTERFACE_LINK_DIRECTORIES $ENV{ARPACK_LINK_DIRECTORIES})
      set(_user_link_def "ON")
    endif()
    if(DEFINED ENV{ARPACK_LINK_LIBRARIES})
      message(STATUS "Using ARPACK_LINK_LIBRARIES = $ENV{ARPACK_LINK_LIBRARIES}")
      set_target_properties(arpack PROPERTIES INTERFACE_LINK_LIBRARIES $ENV{ARPACK_LINK_LIBRARIES})
      set(_user_link_def "ON")
    endif()

    if(NOT _user_link_def)
      # HACK (arpack-ng does not give the link directory properly)
      get_target_property(_arpack_INCLUDES arpack INTERFACE_INCLUDE_DIRECTORIES)
      set(_arpack_LINK_DIR "${_arpack_INCLUDES}/../../lib")
      set_target_properties(arpack PROPERTIES INTERFACE_LINK_DIRECTORIES ${_arpack_LINK_DIR})
    endif()
    unset(_user_link_def)

    get_target_property(_INCLUDES arpack INTERFACE_INCLUDE_DIRECTORIES)
    get_target_property(_DIRECTORIES arpack INTERFACE_LINK_DIRECTORIES)
    get_target_property(_LIBRARIES arpack INTERFACE_LINK_LIBRARIES)

    message(STATUS "arpack INTERFACE_INCLUDE_DIRECTORIES ${_INCLUDES}")
    message(STATUS "arpack INTERFACE_LINK_DIRECTORIES ${_DIRECTORIES}")
    message(STATUS "arpack INTERFACE_LINK_LIBRARIES ${_LIBRARIES}")

    target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE arpack)
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_ARPACK)
  endif()
endif()

##########
# PADDLE #
##########

if(MAPHYSPP_USE_PADDLE)
  if(NOT PADDLE_FOUND)
    if(PKG_CONFIG_FOUND AND PKG_CONFIG_EXECUTABLE)
      message(STATUS "Looking for PADDLE - found using PkgConfig")
      pkg_search_module(PADDLE paddle)
    else()
      find_package(PADDLE REQUIRED)
    endif()
    if(NOT PADDLE_FOUND)
      find_package(PADDLE REQUIRED)
    endif()
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_PADDLE)
    target_include_directories(${CMAKE_PROJECT_NAME} INTERFACE ${PADDLE_INCLUDE_DIRS})
    target_link_libraries     (${CMAKE_PROJECT_NAME} INTERFACE ${PADDLE_LDFLAGS})
  endif()

  find_package(PTSCOTCH REQUIRED)
  target_include_directories(${CMAKE_PROJECT_NAME} INTERFACE ${PTSCOTCH_INCLUDE_DIRS} ${SCOTCH_INCLUDE_DIRS})
  target_link_libraries     (${CMAKE_PROJECT_NAME} INTERFACE ${PTSCOTCH_LIBRARIES} ${SCOTCH_LIBRARIES})
  target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_PADDLE)
endif()

##########
# LIBRSB #
##########

if(MAPHYSPP_USE_RSB_SPBLAS OR MAPHYSPP_USE_RSB)
  find_program(LIBRSB_CONFIG librsb-config)
  execute_process(COMMAND ${LIBRSB_CONFIG} "--ldflags"
    OUTPUT_VARIABLE LIBRSB_LDFLAGS
    OUTPUT_STRIP_TRAILING_WHITESPACE)
  target_link_libraries(${CMAKE_PROJECT_NAME} INTERFACE ${LIBRSB_LDFLAGS})
  if(MAPHYSPP_USE_RSB_SPBLAS) # Force the use of RSB blas when both are activated
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_RSB_SPBLAS)
  endif()
  if(MAPHYSPP_USE_RSB)
    target_compile_definitions(${CMAKE_PROJECT_NAME} INTERFACE MAPHYSPP_USE_RSB)
  endif()
endif()

### Warnings
#
if(MAPHYSPP_GCC_WARNINGS)
  include("${CMAKE_SOURCE_DIR}/GccWarnings.cmake")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${_maphys_gcc_warnings}")
  check_cxx_compiler_flag("-fconcepts-diagnostics-depth=2" fconcepts-diagnostics-avail)
  if(fconcepts-diagnostics-avail)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fconcepts-diagnostics-depth=2")
  endif(fconcepts-diagnostics-avail)
endif(MAPHYSPP_GCC_WARNINGS)

#####################
add_subdirectory(matrices)
set(MAPHYSPP_MATRICES_DIR "${CMAKE_CURRENT_BINARY_DIR}/matrices")
set(MAPHYSPP_MATRIX_PARTITIONS_8 "${CMAKE_CURRENT_BINARY_DIR}/matrices/partitioned")

add_subdirectory(src)
if(MAPHYSPP_COMPILE_BENCH)
  add_subdirectory(bench)
endif()

# export target maphyspp
install(EXPORT maphysppTargets
  NAMESPACE MAPHYSPP::
  DESTINATION lib/cmake/maphyspp
  )

# Installation
# ------------
install(TARGETS ${CMAKE_PROJECT_NAME}
  EXPORT maphysppTargets
  RUNTIME DESTINATION bin
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib)

# see https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html
include(CMakePackageConfigHelpers)

set(BIN_INSTALL_DIR "bin/"     CACHE STRING "where to install executables relative to prefix")
set(INC_INSTALL_DIR "include/" CACHE STRING "where to install headers relative to prefix"    )
set(LIB_INSTALL_DIR "lib/"     CACHE STRING "where to install libraries relative to prefix"  )

configure_package_config_file(cmake_modules/maphysppConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/maphysppConfig.cmake
  INSTALL_DESTINATION lib/cmake/maphyspp
  PATH_VARS BIN_INSTALL_DIR INC_INSTALL_DIR LIB_INSTALL_DIR)
write_basic_package_version_file(maphysppConfigVersion.cmake
  VERSION ${maphyspp_VERSION}
  COMPATIBILITY AnyNewerVersion)

# Install config files
install( FILES
  ${CMAKE_CURRENT_BINARY_DIR}/maphysppConfig.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/maphysppConfigVersion.cmake
  DESTINATION lib/cmake/maphyspp)

install(DIRECTORY DESTINATION ${BIN_INSTALL_DIR}) # Empty directory

# need MORSE Find modules: necessary files must be distributed in the install path
set(morse_dependencies "MUMPS;METIS;SCOTCH;SCALAPACK")
morse_install_finds(morse_dependencies lib/cmake/maphyspp/find)
