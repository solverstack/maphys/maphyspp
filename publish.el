;; publish.el --- Publish org-mode project on Gitlab Pages
;; Initially taken from: Rasmus

;; Author: Gilles Marait

;; This publish script tangles all org files in ./include
;; into the source code of the library.

;; For developers, it is recommended to modify org code
;; and use this publish script to obtain up-to-date source code

;; From a terminal, one can invoke the publication using:
;;emacs --batch --load publish.el --eval '(org-publish "generate-source-code")'

;; To generate HTML pages from the .org, one can use:
;;(cd ./doc/html/ && ./make_doc.sh)
;;emacs --batch --no-init-file --load publish.el --funcall org-publish-all
(require 'package)
(package-initialize)

(require 'org)
(require 'ox-publish)

;; Add minted support for code display
(setq org-latex-packages-alist '())
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("frame" "lines") ("linenos=true") ("mathescape") ("breaklines")))

;; Replace command to generate latex
(setq org-latex-pdf-process
      (mapcar
       (lambda (s)
         (replace-regexp-in-string "%latex " "%latex -shell-escape " s))
       org-latex-pdf-process))

;; Add languages
(add-to-list 'org-src-lang-modes '("c++" . c++))
(setq org-export-with-todo-keywords nil)
(setq org-latex-create-formula-image-program 'imagemagick)

;; Add verbose when errors for pdf generation
(defun org-latex-publish-to-pdf-verbose-on-error (plist filename pub-dir)
  (condition-case nil
      (org-latex-publish-to-pdf
       plist filename pub-dir)
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (append-to-file (point-min) (point-max) "/dev/stdout")))))

;; Fox mathjax latex math expressions -> html
(setq org-confirm-babel-evaluate nil)
(add-to-list 'org-src-lang-modes '("latex-macros" . latex))

(defvar org-babel-default-header-args:latex-macros
  '((:results . "raw")
    (:exports . "results")))

(defun prefix-all-lines (pre body)
  (with-temp-buffer
    (insert body)
    (string-insert-rectangle (point-min) (point-max) pre)
    (buffer-string)))

(defun org-babel-execute:latex-macros (body _params)
  (concat
   (prefix-all-lines "#+LATEX_HEADER: " body)
   "\n#+HTML_HEAD_EXTRA: <div style=\"display: none\"> \\(\n"
   (prefix-all-lines "#+HTML_HEAD_EXTRA: " body)
   "\n#+HTML_HEAD_EXTRA: \\)</div>\n"))

(setq org-html-htmlize-output-type 'css)

;; Use timestamp to avoid publishing
(setq org-publish-use-timestamps-flag t)

;; This fix is necessary to obtain relative path when tangling with comments: link
;; It has been fix for org version 9.3.7 (but it's too)
(if (string< (org-version) "9.3.7")
    (load-file "./ob-tangle-fix.el") ())

(defvar site-attachments
  (regexp-opt '("m" "py" "ipynb" "scm"
                "jpg" "jpeg" "gif" "png" "svg"
                "ico" "cur" "css" "js" "woff" "html" "pdf"))
  "File types that are published as static files.")

(setq org-publish-project-alist
      (list
       (list "generate-source-code"
             :base-directory "./org"
             :base-extension "org"
             :recursive t
             :publishing-function '(org-babel-tangle-publish)
             :publishing-directory "."
             :auto-sitemap nil)
       (list "generate-pdf"
             :base-directory "./doc/fulldoc"
             :base-extension "org"
             :recursive t
             :publishing-function '(org-latex-publish-to-pdf-verbose-on-error)
             :publishing-directory "./doc/fulldoc"
             :auto-sitemap nil)
       (list "site-org"
             :base-directory "./doc/html/public"
             :base-extension "org"
             :recursive t
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory (concat "./public/" (getenv "PUBLISHDIR"))
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>"
             :auto-sitemap nil)
       (list "site-static"
             :base-directory "./doc/html/public"
             :base-extension "png\\|csv\\|txt\\|pdf\\|svg"
             :recursive t
             :publishing-directory (concat "./public/" (getenv "PUBLISHDIR"))
             :publishing-function '(org-publish-attachment)
             :recursive t)
       (list "site-index"
             :base-directory "."
             :base-extension "org"
             :recursive nil
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public/"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>"
             :auto-sitemap nil)
       (list "site" :components '("site-org" "site-static" "site-index"))))

(provide 'publish)
;;; publish.el ends here
