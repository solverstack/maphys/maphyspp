// [[file:../../../org/maphys/graph/Paddle.org::*Header][Header:1]]
#pragma once

namespace maphys {
template<MPH_Scalar Scalar> class Paddle;
}

#include "maphys/loc_data/SparseMatrixCOO.hpp"
#include "maphys/part_data/PartMatrix.hpp"
#include "spdl_c_types.h"
#include "dpdl_c_types.h"
#include "cpdl_c_types.h"
#include "zpdl_c_types.h"

namespace maphys {

template<class Scalar>
struct PaddleWrapper : public std::false_type {};

template<> struct PaddleWrapper<float>: public std::true_type {
  using type = spdl_paddle_t_c;
  static void pdl_driver(spdl_paddle_t_c * s){ spdl_driver_c(s); }
  static void pdl_pdls_sm_nnz(int * nnz)     { spdl_pdls_sm_nnz(nnz); }
  static void pdl_pdls_sm_rhs_nnz(int * nnz) { spdl_pdls_sm_rhs_nnz(nnz); }
  static void pdl_pdls_sm_sol_nnz(int * nnz) { spdl_pdls_sm_sol_nnz(nnz); }

  static void pdl_pdls_sm_ijv    (int * i, int * j, float * v){ spdl_pdls_sm_ijv    (i, j, v); }
  static void pdl_pdls_sm_rhs_ijv(int * i, int * j, float * v){ spdl_pdls_sm_rhs_ijv(i, j, v); }
  static void pdl_pdls_sm_sol_ijv(int * i, int * j, float * v){ spdl_pdls_sm_sol_ijv(i, j, v); }

  static void pdl_get_map_domain_sizes(int * myndof, int * mynbvi, int * mysizeintrf,int * myindexintrf_size){
    spdl_get_map_domain_sizes(myndof, mynbvi, mysizeintrf, myindexintrf_size);
  }
  static void pdl_get_map_domain_arrays(int * myinterface, int * myindexvi, int * myptrindexvi, int * myindexintrf){
    spdl_get_map_domain_arrays(myinterface, myindexvi, myptrindexvi, myindexintrf);
  }
};

template<> struct PaddleWrapper<double>: public std::true_type {
  using type = dpdl_paddle_t_c;
  static void pdl_driver(dpdl_paddle_t_c * s){ dpdl_driver_c(s); }
  static void pdl_pdls_sm_nnz(int * nnz)     { dpdl_pdls_sm_nnz(nnz); }
  static void pdl_pdls_sm_rhs_nnz(int * nnz) { dpdl_pdls_sm_rhs_nnz(nnz); }
  static void pdl_pdls_sm_sol_nnz(int * nnz) { dpdl_pdls_sm_sol_nnz(nnz); }

  static void pdl_pdls_sm_ijv    (int * i, int * j, double * v){ dpdl_pdls_sm_ijv    (i, j, v); }
  static void pdl_pdls_sm_rhs_ijv(int * i, int * j, double * v){ dpdl_pdls_sm_rhs_ijv(i, j, v); }
  static void pdl_pdls_sm_sol_ijv(int * i, int * j, double * v){ dpdl_pdls_sm_sol_ijv(i, j, v); }

  static void pdl_get_map_domain_sizes(int * myndof, int * mynbvi, int * mysizeintrf,int * myindexintrf_size){
    dpdl_get_map_domain_sizes(myndof, mynbvi, mysizeintrf, myindexintrf_size);
  }
  static void pdl_get_map_domain_arrays(int * myinterface, int * myindexvi, int * myptrindexvi, int * myindexintrf){
    dpdl_get_map_domain_arrays(myinterface, myindexvi, myptrindexvi, myindexintrf);
  }
};

template<> struct PaddleWrapper<std::complex<float>>: public std::true_type {
  using type = cpdl_paddle_t_c;
  static void pdl_driver(cpdl_paddle_t_c * s){ cpdl_driver_c(s); }
  static void pdl_pdls_sm_nnz(int * nnz)     { cpdl_pdls_sm_nnz(nnz); }
  static void pdl_pdls_sm_rhs_nnz(int * nnz) { cpdl_pdls_sm_rhs_nnz(nnz); }
  static void pdl_pdls_sm_sol_nnz(int * nnz) { cpdl_pdls_sm_sol_nnz(nnz); }

  static void pdl_pdls_sm_ijv    (int * i, int * j, std::complex<float> * v){ cpdl_pdls_sm_ijv    (i, j, (paddle_complex *) v); }
  static void pdl_pdls_sm_rhs_ijv(int * i, int * j, std::complex<float> * v){ cpdl_pdls_sm_rhs_ijv(i, j, (paddle_complex *) v); }
  static void pdl_pdls_sm_sol_ijv(int * i, int * j, std::complex<float> * v){ cpdl_pdls_sm_sol_ijv(i, j, (paddle_complex *) v); }

  static void pdl_get_map_domain_sizes(int * myndof, int * mynbvi, int * mysizeintrf,int * myindexintrf_size){
    cpdl_get_map_domain_sizes(myndof, mynbvi, mysizeintrf, myindexintrf_size);
  }
  static void pdl_get_map_domain_arrays(int * myinterface, int * myindexvi, int * myptrindexvi, int * myindexintrf){
    cpdl_get_map_domain_arrays(myinterface, myindexvi, myptrindexvi, myindexintrf);
  }
};

template<> struct PaddleWrapper<std::complex<double>>: public std::true_type {
  using type = zpdl_paddle_t_c;
  static void pdl_driver(zpdl_paddle_t_c * s){ zpdl_driver_c(s); }
  static void pdl_pdls_sm_nnz(int * nnz)     { zpdl_pdls_sm_nnz(nnz); }
  static void pdl_pdls_sm_rhs_nnz(int * nnz) { zpdl_pdls_sm_rhs_nnz(nnz); }
  static void pdl_pdls_sm_sol_nnz(int * nnz) { zpdl_pdls_sm_sol_nnz(nnz); }

  static void pdl_pdls_sm_ijv    (int * i, int * j, std::complex<double> * v){ zpdl_pdls_sm_ijv    (i, j, (paddle_complex_double *) v); }
  static void pdl_pdls_sm_rhs_ijv(int * i, int * j, std::complex<double> * v){ zpdl_pdls_sm_rhs_ijv(i, j, (paddle_complex_double *) v); }
  static void pdl_pdls_sm_sol_ijv(int * i, int * j, std::complex<double> * v){ zpdl_pdls_sm_sol_ijv(i, j, (paddle_complex_double *) v); }

  static void pdl_get_map_domain_sizes(int * myndof, int * mynbvi, int * mysizeintrf,int * myindexintrf_size){
    zpdl_get_map_domain_sizes(myndof, mynbvi, mysizeintrf, myindexintrf_size);
  }
  static void pdl_get_map_domain_arrays(int * myinterface, int * myindexvi, int * myptrindexvi, int * myindexintrf){
    zpdl_get_map_domain_arrays(myinterface, myindexvi, myptrindexvi, myindexintrf);
  }
};
// Header:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Attributes][Attributes:1]]
template<MPH_Scalar Scalar> class Paddle{
private:
  using PdlsStruct = typename PaddleWrapper<Scalar>::type;
  using PDLSW = PaddleWrapper<Scalar>;

  PdlsStruct _pdls;
  std::shared_ptr<Process> _proc;
  IndexArray<int> _rhs_i;

  using PaddleScalarPtr = decltype(_pdls.v);
// Attributes:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Call driver][Call driver:1]]
private:
  void _call_driver(int job){
    _pdls.job = job;
    PDLSW::pdl_driver(&_pdls);
  }
// Call driver:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Constructor][Constructor:1]]
public:
  Paddle(MPI_Comm comm = MPI_COMM_WORLD){
    _pdls.comm = comm;
    _call_driver(0); // Initialize
  }
// Constructor:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Destuctor][Destuctor:1]]
~Paddle(){
  _call_driver(-1); // Finalize
}
// Destuctor:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Auxiliary functions][Auxiliary functions:1]]
private:
  bool _is_power_of_two(int n){
    int pw = 1;
    for(int k = 0; k < 30; ++k){
      if(n == pw) return true;
      pw *= 2;
    }
    return false;
  }

  void _set_default_parameters(Size n_dofs, Size nnz){
    _pdls.nrhs = 1;
    _pdls.n    = static_cast<int>(n_dofs);
    _pdls.nnz  = static_cast<int>(nnz);
    _pdls.icntl[ICNTL_PART_LIB]   = PART_LIB_PTSCOTCH;
    _pdls.icntl[ICNTL_CHECKDIST]  = CHECKDIST_YES;
    _pdls.icntl[ICNTL_OUTPUTDIST] = OUTPUTDIST_YES;
    _pdls.icntl[ICNTL_INITGUESS] = NO_INITGUESS;
    _pdls.icntl[ICNTL_RHS] = HAS_RHS;
    _pdls.icntl[ICNTL_SOLVER] = SOLVER_MAPHYS;
  }

  // Copy input matrix to be able to change numbering / storage
  void _paddle_analyse_and_dist(SparseMatrixCOO<Scalar, int> mat_in,
                                 int * rhs_iptr, const Scalar * rhs_vptr){

    if(!mat_in.is_storage_full()){
      mat_in.fill_tri_to_full();
      _pdls.nnz = static_cast<int>(mat_in.get_nnz());
    }

    _pdls.i = mat_in.get_i_ptr();
    _pdls.j = mat_in.get_j_ptr();
    _pdls.v  = (PaddleScalarPtr) mat_in.get_v_ptr();

    // Fortran numbering starting at 1
    for(Size k = 0; k < mat_in.get_nnz(); ++k){
      _pdls.i[k] += 1;
      _pdls.j[k] += 1;
    }

    _pdls.b_i = rhs_iptr;
    _pdls.b_v = (PaddleScalarPtr) rhs_vptr;
    _call_driver(9);
  }

  SparseMatrixCOO<Scalar, int> _get_local_matrix(int myndof, int loc_nnz, char input_mat_prop){
    IndexArray<int> a_i(loc_nnz);
    IndexArray<int> a_j(loc_nnz);
    IndexArray<Scalar> a_v(loc_nnz);

    PDLSW::pdl_pdls_sm_ijv(a_i.data(), a_j.data(), a_v.data());
    a_i -= 1; // C numbering stating at 0
    a_j -= 1;
    SparseMatrixCOO<Scalar, int> out_loc_mat(myndof, myndof, loc_nnz, std::move(a_i), std::move(a_j), std::move(a_v));
    MatrixSymmetry sym = MatrixProperties<Scalar>::properties_deserialize_symmetry(input_mat_prop);
    out_loc_mat.set_property(sym);
    MatrixStorage stor = MatrixProperties<Scalar>::properties_deserialize_storage(input_mat_prop);
    if(stor != MatrixStorage::full) out_loc_mat.to_triangular(stor);
    out_loc_mat.properties_deserialize(input_mat_prop);

    return out_loc_mat;
  }

  template<int NbCol = 1>
  DenseMatrix<Scalar, NbCol> _get_local_rhs(int myndof, int nrhs){
    //3. Get local rhs
    _call_driver(6);
    int loc_nnz_rhs;
    PDLSW::pdl_pdls_sm_rhs_nnz(&loc_nnz_rhs);

    DenseMatrix<Scalar, NbCol> out_loc_rhs(static_cast<Size>(myndof), static_cast<Size>(nrhs));

    IndexArray<int> rhs_i(loc_nnz_rhs);
    IndexArray<int> rhs_j(loc_nnz_rhs);
    IndexArray<Scalar> rhs_v(loc_nnz_rhs);
    PDLSW::pdl_pdls_sm_rhs_ijv(rhs_i.data(), rhs_j.data(), rhs_v.data());
    rhs_i -= 1; // C numbering stating at 0
    rhs_j -= 1;

    for(int k = 0; k < loc_nnz_rhs; ++k){
      out_loc_rhs(rhs_i[k], rhs_j[k]) = rhs_v[k];
    }
    return out_loc_rhs;
  }

  void _create_ddm(int myndof, int mynbvi, int mysizeintrf, int myindexintrf_size, int id){
    IndexArray<int> myinterface (mysizeintrf);
    IndexArray<int> myindexvi   (mynbvi);
    IndexArray<int> myptrindexvi(mynbvi + 1);
    IndexArray<int> myindexintrf(myindexintrf_size);

    PDLSW::pdl_get_map_domain_arrays(myinterface.data(), myindexvi.data(),
                                     myptrindexvi.data(), myindexintrf.data());
    myptrindexvi -= 1;

    std::map<int, IndexArray<int>> nei2dof;
    int k_idxi = 0;
    for(int kn = 0; kn < mynbvi; ++kn){
      int nei = myindexvi[kn];
      nei2dof[nei] = IndexArray<int>(myptrindexvi[kn+1] - myptrindexvi[kn]);
      for(int kdof = 0; kdof < myptrindexvi[kn+1] - myptrindexvi[kn]; ++kdof){
        nei2dof[nei][kdof] = myndof - mysizeintrf + myindexintrf[k_idxi++] - 1;
      }
    }

    std::vector<Subdomain> sdv;
    sdv.emplace_back(id, myndof, std::move(nei2dof), false);

    _proc = bind_subdomains(MMPI::size(_pdls.comm), 1, _pdls.comm);
    _proc->load_subdomains(sdv);
  }
// Auxiliary functions:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Centralized input matrix][Centralized input matrix:1]]
public:
  template<int NbCol = 1>
  std::pair<PartMatrix<SparseMatrixCOO<Scalar, int>>, PartVector<DenseMatrix<Scalar, NbCol>>>
  centralized_input(const SparseMatrixCOO<Scalar, int>& mat_in, const DenseMatrix<Scalar, NbCol>& rhs_in){
    MAPHYSPP_DIM_ASSERT(mat_in.get_n_rows(), mat_in.get_n_cols(), "Paddle::centralized_input: matrix must be square");
    MAPHYSPP_DIM_ASSERT(mat_in.get_n_rows(), rhs_in.get_n_rows(), "Paddle::centralized_input: matrix and rhs muse have same size");
    MAPHYSPP_ASSERT(_is_power_of_two(MMPI::size(_pdls.comm)), "Paddle::centralized_input: MPI size must be a power of 2");

    // Broadcast matrix properties
    char input_mat_prop = mat_in.properties_serialize();
    MMPI::bcast(&input_mat_prop, 1, 0, _pdls.comm);

    // Set paddle parameters
    _set_default_parameters(mat_in.get_n_rows(), mat_in.get_nnz());
    _pdls.icntl[ICNTL_ENTRY_MODE] = ENTRY_CENTRALIZED;

    // Analyse
    IndexArray<int> rhs_indices = arange(1, _pdls.n + 1);
    _paddle_analyse_and_dist(mat_in, rhs_indices.data(), rhs_in.get_ptr());

    // Get sizes
    int loc_nnz, myndof, mynbvi, mysizeintrf, myindexintrf_size;
    PDLSW::pdl_pdls_sm_nnz(&loc_nnz);
    PDLSW::pdl_get_map_domain_sizes(&myndof, &mynbvi, &mysizeintrf, &myindexintrf_size);

    // Get local matrix and rhs
    auto out_loc_mat = _get_local_matrix(myndof, loc_nnz, input_mat_prop);
    auto out_loc_rhs = _get_local_rhs<1>(myndof, 1);

    // Create domain decomposition
    const int id = MMPI::rank(_pdls.comm);
    _create_ddm(myndof, mynbvi, mysizeintrf, myindexintrf_size, id);

    // Create PartMatrix and PartVector on the dd
    PartMatrix<SparseMatrixCOO<Scalar, int>> mat_out(_proc, id, std::move(out_loc_mat));
    PartVector<DenseMatrix<Scalar, NbCol>> vect_out(_proc, id, std::move(out_loc_rhs));
    return std::make_pair(mat_out, vect_out);
  }
// Centralized input matrix:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Distributed input matrix][Distributed input matrix:1]]
template<int NbCol = 1>
std::pair<PartMatrix<SparseMatrixCOO<Scalar, int>>, PartVector<DenseMatrix<Scalar, NbCol>>>
distributed_input(const SparseMatrixCOO<Scalar, int>& mat_in, const IndexArray<int>& rhs_indices, const IndexArray<Scalar>& rhs_values){
  MAPHYSPP_DIM_ASSERT(mat_in.get_n_rows(), mat_in.get_n_cols(), "Paddle::distributed_input: matrix must be square");
  MAPHYSPP_DIM_ASSERT(rhs_indices.size(), rhs_values.size(), "Paddle::distributed_input: rhs_indices and rhs_values must have same size");
  MAPHYSPP_ASSERT(_is_power_of_two(MMPI::size(_pdls.comm)), "Paddle::distributed_input: MPI size must be a power of 2");

  // Broadcast matrix properties
  char input_mat_prop = mat_in.properties_serialize();
  MMPI::bcast(&input_mat_prop, 1, 0, _pdls.comm);

  // Set paddle parameters
  _rhs_i = rhs_indices;
  _set_default_parameters(_rhs_i.size(), mat_in.get_nnz());
  _pdls.icntl[ICNTL_ENTRY_MODE] = ENTRY_DISTRIBUTED;
  _pdls.n_glob = static_cast<int>(mat_in.get_n_rows());

  _rhs_i += 1;
  _paddle_analyse_and_dist(mat_in, _rhs_i.data(), rhs_values.data());

  //1. Get sizes
  int loc_nnz, myndof, mynbvi, mysizeintrf, myindexintrf_size;
  PDLSW::pdl_pdls_sm_nnz(&loc_nnz);
  PDLSW::pdl_get_map_domain_sizes(&myndof, &mynbvi, &mysizeintrf, &myindexintrf_size);

  // Get local matrix and rhs
  auto out_loc_mat = _get_local_matrix(myndof, loc_nnz, input_mat_prop);
  auto out_loc_rhs = _get_local_rhs(myndof, 1);

  // Create domain decomposition
  const int id = MMPI::rank(_pdls.comm);
  _create_ddm(myndof, mynbvi, mysizeintrf, myindexintrf_size, id);

  // We obtain the local matrices without coefficients on all the interfaces
  // Let's fix that by assembling the matrix and dividing each coefficient by
  // its multiplicity (the total number of times it is stored on all processes).
  PartMatrix<SparseMatrixCOO<Scalar, int>> mat_out(_proc, id, std::move(out_loc_mat));
  mat_out.assemble();

  // To get the multiplicity we create a new PartMatrix with the same coefficient
  // positions but with a value of 1, and we assemble it.
  auto divide_by_multiplicity =
    [this, id](SparseMatrixCOO<Scalar, int>& loc_mat){
      std::vector<Scalar> ones(loc_mat.get_nnz(), Scalar{1});
      SparseMatrixCOO<Scalar, int> mat_ones(loc_mat.get_n_rows(), loc_mat.get_n_cols(), loc_mat.get_nnz(),
                                            loc_mat.get_i_ptr(), loc_mat.get_j_ptr(), ones.data());
      ones.clear();

      PartMatrix<SparseMatrixCOO<Scalar, int>> mat_multiplicity(_proc, id, std::move(mat_ones));
      mat_multiplicity.assemble();

      SparseMatrixCOO<Scalar, int>& loc_mat_mult = mat_multiplicity.get_local_matrix(MMPI::rank());
      loc_mat_mult.order_indices();
      IJVIterator<Scalar, int, SparseMatrixCOO<Scalar, int>> it(loc_mat);
      std::vector<Scalar> new_v(loc_mat.get_nnz());
      int cnt = 0;
      for(auto [i, j, v] : it){
        new_v[cnt++] = v / loc_mat_mult(i, j);
      }
      std::memcpy(loc_mat.get_v_ptr(), &new_v[0], new_v.size() * sizeof(Scalar));
    };

  divide_by_multiplicity(mat_out.get_local_matrix(MMPI::rank()));

  PartVector<DenseMatrix<Scalar, NbCol>> vect_out(_proc, id, std::move(out_loc_rhs));
  vect_out.assemble();
  return std::make_pair(mat_out, vect_out);
}
// Distributed input matrix:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Distribute solution back][Distribute solution back:1]]
template<int NbCol = 1>
DenseMatrix<Scalar, NbCol> solution_redistribution(const PartVector<DenseMatrix<Scalar, NbCol>>& dist_sol){

  const DenseMatrix<Scalar, NbCol>& loc_vect = dist_sol.get_local_vector(MMPI::rank());
  const Size nrows = loc_vect.get_n_rows();
  const Size nrhs  = loc_vect.get_n_cols();

  IndexArray<int> rhs_indices = arange<int>(1, static_cast<int>(nrows * nrhs) + 1);
  _pdls.sol_i = rhs_indices.data();
  _pdls.sol_v = (PaddleScalarPtr) loc_vect.get_ptr();

  _call_driver(7);
  int loc_nnz_sol;
  PDLSW::pdl_pdls_sm_sol_nnz(&loc_nnz_sol);

  DenseMatrix<Scalar, NbCol> out_loc_sol(_pdls.n, _pdls.nrhs);
  {
    IndexArray<int> sol_i(loc_nnz_sol);
    IndexArray<int> sol_j(loc_nnz_sol);
    IndexArray<Scalar> sol_v(loc_nnz_sol);
    PDLSW::pdl_pdls_sm_sol_ijv(sol_i.data(), sol_j.data(), sol_v.data());
    sol_i -= 1; // C numbering stating at 0
    sol_j -= 1;
    for(int k = 0; k < loc_nnz_sol; ++k){
      out_loc_sol(k, 0) = sol_v[k];
    }
  } // To free ijv arrays

  return out_loc_sol;
}
// Distribute solution back:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Set new right hand side input vector][Set new right hand side input vector:1]]
template<int NbCol = 1>
PartVector<DenseMatrix<Scalar, NbCol>> setup_next_rhs(const IndexArray<Scalar>& next_rhs_v){
  _pdls.b_i = _rhs_i.data();
  _pdls.b_v = (PaddleScalarPtr) next_rhs_v.data();
  int myndof = _proc->get_n_dofs(MMPI::rank());
  DenseMatrix<Scalar, NbCol> new_rhs = _get_local_rhs(myndof, 1);
  PartVector<DenseMatrix<Scalar, NbCol>> vect_out(_proc, MMPI::rank(), std::move(new_rhs));
  vect_out.assemble();
  return vect_out;
}
// Set new right hand side input vector:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Setters][Setters:1]]
void set_verbose(bool verb = true){
  _pdls.verbose = verb ? 1 : 0;
}
// Setters:1 ends here

// [[file:../../../org/maphys/graph/Paddle.org::*Footer][Footer:1]]
}; // class Paddle
} // namespace maphys
// Footer:1 ends here
