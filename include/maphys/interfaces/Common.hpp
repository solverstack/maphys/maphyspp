// [[file:../../../org/maphys/interfaces/Common.org][No heading:1]]
#pragma once

#include <type_traits>

#ifdef MAPHYSPP_HEADER_ONLY
#define MAPHYSPP_NO_TIMER
#define MAPHYSPP_NO_MPI
#define MAPHYSPP_USE_TLAPACK
#endif

#ifndef MAPHYSPP_NO_TIMER

#ifndef MAPHYSPP_USE_SHARED_TIMER
#include "Timer.hpp"
#else
#include "Timer_api.hpp"
#endif

#else
#include <string>

namespace timer{
template<int L>
struct Timer{
  Timer(const std::string&) {}
  void stop() {}
  static void results() {}
};
}

#endif

using timer::Timer;

namespace maphys{

constexpr const int TIMER_SOLVER = 100;
constexpr const int TIMER_PRECOND = 150;
constexpr const int TIMER_EVD = 500;
constexpr const int TIMER_ITERATION = 1000;
constexpr const int TIMER_KERNEL = 10000;

template<typename T> struct is_distributed : public std::false_type {};
template<typename T> struct is_sparse : public std::false_type {};
template<typename T> struct is_dense : public std::false_type {};

template<typename T> struct scalar_type : public std::false_type {};
template<typename T> struct vector_type : public std::false_type {};
template<typename T> struct matrix_type : public std::false_type {};
template<typename T> struct sparse_type : public std::false_type {};
template<typename T> struct dense_type : public std::false_type {};
template<typename T> struct real_type : public std::false_type {};
template<typename T> struct complex_type : public std::false_type {};
template<typename T> struct diag_type : public std::false_type {};
template<typename T> struct local_type : public std::false_type {};

// If a direct solver provides separately the forward and backward solve
template<typename T> struct has_triangular_solve : public std::false_type {};
//In C++20:
// requires(Solver r_s, Vector r_v1, Vector r_v2){ r_s.triangular_solve(r_v1, r_v2, MatrixStorage::lower, true); }

template<typename T> struct has_init_guess_function : public std::false_type {};
//In C++20:
// requires(Precond p, Vector v1){ p.init_guess(v1); -> Vector }

using Size = std::size_t;
using Blas_int = int64_t;

template<typename Scalar, typename Index, typename SpMat> struct IJVIterator : public std::false_type {};

template<typename F, typename T1>
void apply_on_data(F fct, T1& t1){
  if constexpr(is_distributed<T1>::value){
    for(auto id : t1.get_sd_ids()){
      fct(t1.get_local_data(id));
    }
  }
  else{
    fct(t1);
  }
}

template<typename F, typename T1, typename T2>
void apply_on_data(F fct, T1& t1, T2& t2){
  if constexpr(is_distributed<T1>::value){
    for(auto id : t1.get_sd_ids()){
      fct(t1.get_local_data(id), t2.get_local_data(id));
    }
  }
  else{
    fct(t1, t2);
  }
}

template<typename F, typename T1, typename T2, typename T3>
void apply_on_data(F fct, T1& t1, T2& t2, T3& t3){
  if constexpr(is_distributed<T1>::value){
    for(auto id : t1.get_sd_ids()){
      fct(t1.get_local_data(id), t2.get_local_data(id), t3.get_local_data(id));
    }
  }
  else{
    fct(t1, t2, t3);
  }
}

template<typename F, typename T1, typename T2, typename T3, typename T4>
void apply_on_data(F fct, T1& t1, T2& t2, T3& t3, T4& t4){
  if constexpr(is_distributed<T1>::value){
    for(auto id : t1.get_sd_ids()){
      fct(t1.get_local_data(id), t2.get_local_data(id), t3.get_local_data(id), t4.get_local_data(id));
    }
  }
  else{
    fct(t1, t2, t3, t4);
  }
}

template<typename F, typename T1, typename T2, typename T3, typename T4, typename T5>
void apply_on_data(F fct, T1& t1, T2& t2, T3& t3, T4& t4, T5& t5){
  if constexpr(is_distributed<T1>::value){
    for(auto id : t1.get_sd_ids()){
      fct(t1.get_local_data(id), t2.get_local_data(id), t3.get_local_data(id), t4.get_local_data(id), t5.get_local_data(id));
    }
  }
  else{
    fct(t1, t2, t3, t4, t5);
  }
}

template<typename T>
void assemble(T& t){
  if constexpr(is_distributed<T>::value){
    t.assemble();
  }
  else{
    (void) t;
  }
}

} // namespace maphys
// No heading:1 ends here
