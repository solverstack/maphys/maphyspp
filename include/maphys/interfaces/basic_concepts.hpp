// [[file:../../../org/maphys/interfaces/basic_concepts.org::*Header][Header:1]]
#pragma once

#include "Common.hpp"

#include <type_traits>
#include <complex>
#include "../utils/Arithmetic.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/interfaces/basic_concepts.org::*Basic types][Basic types:1]]
// Integral type
template <class T>
concept MPH_Integral = std::is_integral<T>::value;
// Basic types:1 ends here

// [[file:../../../org/maphys/interfaces/basic_concepts.org::*Basic types][Basic types:2]]
// Scalar type
template<typename S>
concept MPH_Scalar =
  std::is_same_v<S, float> ||
  std::is_same_v<S, double> ||
  std::is_same_v<S, std::complex<float>> ||
  std::is_same_v<S, std::complex<double>>;

// Real type
template<typename S>
concept MPH_Real =
  std::is_same_v<S, float> ||
  std::is_same_v<S, double>;

// Complex type
template<typename S>
concept MPH_Complex =
  std::is_same_v<S, std::complex<float>> ||
  std::is_same_v<S, std::complex<double>>;
// Basic types:2 ends here

// [[file:../../../org/maphys/interfaces/basic_concepts.org::*Linear object][Linear object:1]]
/*
  MPH_LinearObject
  Concept of an object that can be scaled, added substracted
  such as a vector, matrix, tensor...
*/
template<typename object>
concept MPH_LinearObject =
  requires { typename scalar_type<object>::type; } &&
  MPH_Scalar<typename scalar_type<object>::type> &&
  requires(object obj, typename scalar_type<object>::type scal)
  {
   { obj *= scal };
   { obj += obj };
   { obj -= obj };
   { obj * scal }; //-> std::convertible_to<object>;
   { scal * obj }; //-> std::convertible_to<object>;
   { obj + obj }; //-> std::convertible_to<object>;
   { obj - obj }; //-> std::convertible_to<object>;
  };
// Linear object:1 ends here

// [[file:../../../org/maphys/interfaces/basic_concepts.org::*No concept version][No concept version:1]]
} // namespace maphys
// No concept version:1 ends here
