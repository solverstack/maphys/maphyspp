// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Header][Header:1]]
#pragma once

#include "linalg_concepts.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Vector][Vector:1]]
/*
  MPH_Vector
  Concept of a vector, independently of its construction
  or element access
*/
template<typename vector>
concept MPH_Vector = MPH_LinearObject<vector> &&
  requires(vector v1, vector v2)
  {
    { dot(v1, v2) } -> std::convertible_to<typename scalar_type<vector>::type>; // Dot product
    { size(v1)  } -> std::integral;
  };
// Vector:1 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Vector][Vector:2]]
template<typename vector>
concept MPH_Vector_Multiple = MPH_LinearObject<vector> &&
  requires(vector v1, vector v2)
  {
   { n_rows(v1) } -> std::integral;
   { n_cols(v1) } -> std::integral;
   { cwise_dot(v1, v2) } -> std::convertible_to<std::vector<typename scalar_type<vector>::type>>; // Column-wise dot product
  };
// Vector:2 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Vector][Vector:3]]
template<typename vector>
concept MPH_Vector_Single_Or_Multiple = (MPH_Vector<vector> || MPH_Vector_Multiple<vector>);
// Vector:3 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Dense vector][Dense vector:1]]
/*
  MPH_DenseVector
  Describes a vector object, where individual elements
  can be accesses and modified
*/
template<typename vector>
concept MPH_DenseVector = MPH_Vector<vector> &&
  requires(vector v1, vector v2, int i)
  {
   { vector(i) }   -> std::convertible_to<vector>; // Constructor with size only
   { v1[i] }       -> std::convertible_to<typename scalar_type<vector>::type>;    // Element accessor
   { v1(i) }       -> std::convertible_to<typename scalar_type<vector>::type>;    // Element accessor
   { get_ptr(v1) } -> std::convertible_to<typename scalar_type<vector>::type *>;
  };
// Dense vector:1 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Operator][Operator:1]]
template<typename operat, typename vector = typename vector_type<operat>::type>
concept MPH_LinearOperator = (!std::is_same_v<vector, std::false_type>) && // vector_type is undefined
MPH_Vector<vector> &&
requires(operat m, vector v)
{
  { m * v } -> std::convertible_to<vector>;
};
// Operator:1 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Matrix][Matrix:1]]
template<typename matrix>
concept MPH_Matrix = MPH_LinearOperator<matrix> &&
  requires(matrix m)
  {
   { n_rows(m) } -> std::integral;
   { n_cols(m) } -> std::integral;
  };
// Matrix:1 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Dense matrix][Dense matrix:1]]
/*
  MPH_DenseMatrix
  Refinement of a MPH_Matrix for dense storage
*/
template<typename matrix>
concept MPH_DenseMatrix = MPH_Matrix<matrix> &&
  requires(matrix mat, typename scalar_type<matrix>::type scal, int i, int j)
  {
   { get_ptr(mat) } -> std::convertible_to<typename scalar_type<matrix>::type*>;
   { mat(i, j) }    -> std::convertible_to<typename scalar_type<matrix>::type>; // Element accessor
  };

template<typename matrix>
concept MPH_PartDenseMatrix = MPH_Matrix<matrix> &&
  MPH_DenseMatrix<typename matrix::local_type>;
// Dense matrix:1 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Sparse matrix][Sparse matrix:1]]
/*
  MPH_SparseMatrix
  Refinement of a MPH_Matrix for sparse storage
*/
template<typename matrix>
concept MPH_SparseMatrix = MPH_Matrix<matrix> &&
  requires(matrix mat)
  {
   { n_nonzero(mat) } -> std::integral;
  };

template<typename matrix>
concept MPH_PartSparseMatrix = MPH_Matrix<matrix> &&
  MPH_SparseMatrix<typename matrix::local_type>;
// Sparse matrix:1 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*Solver][Solver:1]]
/*
  template<typename solver, typename matrix, typename vector = typename vector_type<matrix>::type>
  concept MPH_Solver = requires(solver s, vector v, matrix mat)
  {
  { s.setup(mat) } -> std::convertible_to<void>;
  { s.solve(v) } ->std::convertible_to<vector>;
  { s * v } -> std::convertible_to<vector>;
  };
*/
// Solver:1 ends here

// [[file:../../../org/maphys/interfaces/linalg_concepts.org::*No concept version][No concept version:1]]
} // namespace maphys
// No concept version:1 ends here
