// [[file:../../../org/maphys/solver/QrMumps.org::*Header][Header:1]]
#pragma once

#include <maphys/interfaces/linalg_concepts.hpp>
namespace maphys {
template<MPH_SparseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
class QrMumps;
}

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/FortranArray.hpp"
#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/loc_data/SparseMatrixCOO.hpp"
#include "maphys/loc_data/SparseMatrixCSC.hpp"
#include "maphys/solver/LinearOperator.hpp"

extern "C" {
#include "qrm_common_c.h"
#include "sqrm_c.h"
#include "dqrm_c.h"
#include "cqrm_c.h"
#include "zqrm_c.h"
}

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Structures depending on arithmetic types][Structures depending on arithmetic types:1]]
// Arithmetic structure for qr_mumps
template<class Scalar>
struct QrMumpsStructDefinition : public std::false_type {};

#define MPH_DEFINE_QRM_STRUCT(_scal, _c_scal, _spfct, _spmat,           \
                              _spmat_init, _spmat_destroy,              \
                              _spfct_init, _spfct_destroy,              \
                              _analyse, _factorize,                     \
                              _solve, _apply)  template<>               \
  struct QrMumpsStructDefinition<_scal> : public std::true_type {       \
    using spfct_type = _spfct;                                          \
    using spmat_type = _spmat;                                          \
    static int  spmat_init(spmat_type *qrm_spmat_c){ return _spmat_init(qrm_spmat_c); } \
    static int  spmat_destroy(spmat_type *qrm_spmat_c){ return _spmat_destroy(qrm_spmat_c); } \
    static int  spfct_init(spfct_type *qrm_spfct_c, spmat_type *qrm_spmat_c){ return _spfct_init(qrm_spfct_c, qrm_spmat_c); } \
    static int  spfct_destroy(spfct_type *qrm_spfct_c){ return _spfct_destroy(qrm_spfct_c); } \
    static int  analyse(spmat_type *qrm_spmat_c,                        \
                        spfct_type *qrm_spfct_c,                        \
                        const char transp){ return _analyse(qrm_spmat_c, qrm_spfct_c, transp); } \
    static int  factorize(spmat_type *qrm_spmat_c,                      \
                          spfct_type *qrm_spfct_c,                      \
                          const char transp){ return _factorize(qrm_spmat_c, qrm_spfct_c, transp); } \
    static int  solve(spfct_type *qrm_spfct_c, const char transp,       \
                      _scal *b, _scal *x, const int nrhs){ return _solve(qrm_spfct_c, transp, (_c_scal *) b, (_c_scal *) x, nrhs); } \
    static int  apply(spfct_type *qrm_spfct_c, const char transp,       \
                      _scal *b, const int nrhs){ return _apply(qrm_spfct_c, transp, (_c_scal *) b, nrhs); } \
  };

MPH_DEFINE_QRM_STRUCT(float, float, sqrm_spfct_type_c, sqrm_spmat_type_c,
                      sqrm_spmat_init_c, sqrm_spmat_destroy_c,
                      sqrm_spfct_init_c, sqrm_spfct_destroy_c,
                      sqrm_analyse_c, sqrm_factorize_c,
                      sqrm_solve_c, sqrm_apply_c)

MPH_DEFINE_QRM_STRUCT(double, double, dqrm_spfct_type_c, dqrm_spmat_type_c,
                      dqrm_spmat_init_c, dqrm_spmat_destroy_c,
                      dqrm_spfct_init_c, dqrm_spfct_destroy_c,
                      dqrm_analyse_c, dqrm_factorize_c,
                      dqrm_solve_c, dqrm_apply_c)

MPH_DEFINE_QRM_STRUCT(std::complex<float>, float _Complex, cqrm_spfct_type_c, cqrm_spmat_type_c,
                      cqrm_spmat_init_c, cqrm_spmat_destroy_c,
                      cqrm_spfct_init_c, cqrm_spfct_destroy_c,
                      cqrm_analyse_c, cqrm_factorize_c,
                      cqrm_solve_c, cqrm_apply_c)

MPH_DEFINE_QRM_STRUCT(std::complex<double>, double _Complex, zqrm_spfct_type_c, zqrm_spmat_type_c,
                      zqrm_spmat_init_c, zqrm_spmat_destroy_c,
                      zqrm_spfct_init_c, zqrm_spfct_destroy_c,
                      zqrm_analyse_c, zqrm_factorize_c,
                      zqrm_solve_c, zqrm_apply_c)
// Structures depending on arithmetic types:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Attributes][Attributes:1]]
template<MPH_SparseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
class QrMumps :
  public LinearOperator<Matrix, Vector> {

private:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;

public:
  using scalar_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;
  using matrix_type = Matrix;
  using vector_type = Vector;

private:
  using Real = real_type;
  using QRM = QrMumpsStructDefinition<Scalar>;

  static const constexpr Size ICNTL_SIZE = 20;
  static const constexpr Size RCNTL_SIZE = 10;
  static const constexpr Size GSTATS_SIZE = 10;

  // Copy of the input matrix, with fortran numbering
  SparseMatrixCOO<Scalar, int> _A;

  typename QRM::spfct_type _spfct;
  typename QRM::spmat_type _spmat;

  FortranArray<int> ICNTL;
  FortranArray<float> RCNTL;
  FortranArray<long long int> GSTATS;

  bool _is_init = false;
  bool _is_spfct_init = false;
  bool _is_matrix_set = false;
  bool _is_facto = false;

  int _n_cpu = 1;
  int _n_gpu = 0;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Constructors][Constructors:1]]
public:
QrMumps(){}

~QrMumps(){
  _finalize() override ;
}

QrMumps(const Matrix& A){
  setup(A);
}

void _finalize() override {
  if(_is_init){
    QRM::spmat_destroy(&_spmat);
    if(_is_spfct_init) QRM::spfct_destroy(&_spfct);
    qrm_finalize_c();
  }
  _is_init = false;
  _is_spfct_init = false;
  _is_matrix_set = false;
  _is_facto = false;
}

// Delete copy
QrMumps(const QrMumps&) = delete;
QrMumps& operator=(const QrMumps&) = delete;

// Allow move
private:
inline void move_qrmumps(QrMumps&& other){
  _A = std::move(other._A);

  _is_init = std::exchange(other._is_init, false);
  _is_spfct_init = std::exchange(other._is_spfct_init, false);
  _is_matrix_set = std::exchange(other._is_matrix_set, false);

  if(_is_matrix_set){
    _spmat.irn = get_i_ptr(_A);
    _spmat.jcn = get_j_ptr(_A);
    _spmat.val = get_v_ptr(_A);
    _spmat.m = n_rows(_A);
    _spmat.n = n_cols(_A);
    _spmat.nz = n_nonzero(_A);
    _spmat.sym = (_A.is_symmetric()) ? 1 : 0;
  }
  other._spmat.m = 0;
  other._spmat.n = 0;
  other._spmat.nz = 0;
  other._spmat.sym = 0;

  std::memcpy(_spfct.icntl, other._spfct.icntl, ICNTL_SIZE * sizeof(int));
  std::memcpy(_spfct.rcntl, other._spfct.rcntl, RCNTL_SIZE * sizeof(double));
  std::memcpy(_spfct.gstats, other._spfct.gstats, GSTATS_SIZE * sizeof(long int));

  _is_facto = false;
  other._is_facto = false;
}

public:

QrMumps(QrMumps&& other)/*: QrMumps()*/{
  move_qrmumps(std::move(other));
}
QrMumps& operator=(QrMumps&& other){
  if(&other == this) return *this;
  move_qrmumps(std::move(other));
  return *this;
}
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Error handling][Error handling:1]]
void errcheck(int info){
  if(info == 0) return;
  std::cerr << "QRMUMPS returned with info > 0; info = " << info << '\n';
  MAPHYSPP_ASSERT(info == 0, "QRMUMPS fatal error");
}
// Error handling:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Setup][Setup:1]]
void setup(const Matrix& A){
  _A = A.template to_coo<int>();

  qrm_init_c(_n_cpu, _n_gpu);
  errcheck(QRM::spmat_init(&_spmat));
  ICNTL.set_data(ICNTL_SIZE, &(_spfct.icntl[0]));
  RCNTL.set_data(RCNTL_SIZE, &(_spfct.rcntl[0]));
  GSTATS.set_data(GSTATS_SIZE, &(_spfct.gstats[0]));

  _is_init = true;
  _is_matrix_set = false;
  _is_facto = false;
}
// Setup:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Set matrix][Set matrix:1]]
void set_matrix(){
  MAPHYSPP_ASSERT(_is_init, "QrMumps: no matrix has been setup");

  // Storage must be triangular if A symmetric
  if(_A.is_symmetric()){
    _A.to_triangular(MatrixStorage::lower);
    _spmat.sym = 1;
  }
  else{
    _A.fill_tri_to_full();
    _spmat.sym = 0;
  }

  _spmat.irn = get_i_ptr(_A);
  _spmat.jcn = get_j_ptr(_A);
  _spmat.val = get_v_ptr(_A);
  _spmat.m = n_rows(_A);
  _spmat.n = n_cols(_A);
  _spmat.nz = n_nonzero(_A);

  if(!_is_matrix_set){ // Make sure we don't add 1 several times
    for(int k = 0; k < _spmat.nz; ++k){ // Fortran numbering
      _spmat.irn[k] += 1;
      _spmat.jcn[k] += 1;
    }
  }

  MAPHYSPP_ASSERT(_spmat.m >= _spmat.n, "Error in QRMUMPS: input matrix has more columns than rows");
  _is_matrix_set = true;
}
// Set matrix:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Factorization][Factorization:1]]
void factorize(){
  if(!_is_matrix_set) set_matrix();

  {
    Timer<TIMER_SOLVER> t_a("QrMumps analysis");
    errcheck(QRM::spfct_init(&_spfct, &_spmat));
    _is_spfct_init = true;
    errcheck(QRM::analyse(&_spmat, &_spfct, qrm_no_transp));
  }
  {
    Timer<TIMER_SOLVER> t_f("QrMumps facto");
    errcheck(QRM::factorize(&_spmat, &_spfct, qrm_no_transp));
  }
  _is_facto = true;
}
// Factorization:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Solve][Solve:1]]
// Copy B, because it's modified by apply/solve
void QRsolve(Vector B, Vector& X){
  if(!_is_facto) factorize();
  Timer<TIMER_SOLVER> t("QrMumps QR-solve");

  if(_A.is_symmetric()){
    _A.set_property(MatrixSymmetry::general); 
    set_matrix();
  }

  Scalar * b = get_ptr(B);
  Scalar * x = get_ptr(X);

  int nrhs = static_cast<int>(n_cols(B));
  constexpr auto transp = is_complex<Scalar>::value ? qrm_conj_transp : qrm_transp;
  errcheck(QRM::apply(&_spfct, transp, b, nrhs)); // b2 <- Q^* b
  errcheck(QRM::solve(&_spfct, qrm_no_transp, b, x, nrhs)); // x <- solve(R x = b2)
}

// Copy B, because it's modified by solve
void LLTsolve(Vector B, Vector& X){
  MAPHYSPP_ASSERT(_A.is_spd(), "QrMumps: calling LLTsolve on non spd");

  if(!_is_facto) factorize();
  Timer<TIMER_SOLVER> t("QrMumps LLT-solve");

  X = B; // Copy the content of B in X
  Scalar * b = get_ptr(B);
  Scalar * x = get_ptr(X);

  const int nrhs = static_cast<int>(n_cols(B));
  MAPHYSPP_DIM_ASSERT(nrhs, n_cols(X), "QrMumps LLT solve: B and X have different number of columns");

  constexpr auto transp = is_complex<Scalar>::value ? qrm_conj_transp : qrm_transp;
  errcheck(QRM::solve(&_spfct, transp, x, b, nrhs));
  errcheck(QRM::solve(&_spfct, qrm_no_transp, b, x, nrhs));
}

void solve(const Vector& B, Vector& X){
  if(!_is_facto) factorize();
  if(_A.is_spd()){
    LLTsolve(B, X);
  }
  else{
    QRsolve(B, X);
  }
}
// Solve:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Apply][Apply:1]]
Vector apply(const Vector& B){
  Vector X(n_cols(_A), n_cols(B));
  solve(B, X);
  return X;
}
void apply(const Vector& B, Vector& X) {
  solve(B, X);
}
Vector solve(const Vector& B){
  return apply(B);
}
// Apply:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Triangular solve][Triangular solve:1]]
void triangular_solve(const Vector& B, Vector& X, MatrixStorage, bool transposed = false){
  MAPHYSPP_ASSERT(_A.is_spd(), "QrMumps: calling LLTsolve on non spd");

  if(!_is_facto) factorize();
  Timer<TIMER_SOLVER> t("QrMumps triangular solve");

  Scalar * b = const_cast<Scalar *>(get_ptr(B));
  Scalar * x = get_ptr(X);

  const int nrhs = static_cast<int>(n_cols(B));
  MAPHYSPP_DIM_ASSERT(nrhs, n_cols(X), "QrMumps triangular solve: B and X have different number of columns");

  if(!transposed){
    constexpr auto transp = is_complex<Scalar>::value ? qrm_conj_transp : qrm_transp;
    errcheck(QRM::solve(&_spfct, transp, b, x, nrhs));
  }
  else{
    errcheck(QRM::solve(&_spfct, qrm_no_transp, b, x, nrhs));
  }
}

Vector triangular_solve(const Vector& B, MatrixStorage uplo, bool transposed = false){
  Vector X = B * Scalar{0}; // Create X same size as B
  triangular_solve(B, X, uplo, transposed);
  return X;
}
// Triangular solve:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Setters][Setters:1]]
template<MPH_Integral Tint>
void set_n_cpu(Tint ncpu){
  _n_cpu = static_cast<int>(ncpu);
  if(_is_init){
    MAPHYSPP_WARNING("QrMumps::set_n_cpu() resetting some values. It is better to call it before setup");
    _finalize() override ;
  }
}

template<MPH_Integral Tint>
void set_n_gpu(Tint ngpu){
  _n_gpu = static_cast<int>(ngpu);
  if(_is_init){
    MAPHYSPP_WARNING("QrMumps::set_n_gpu() resetting some values. It is better to call it before setup");
    _finalize() override ;
  }
}

template<MPH_Integral Tint>
void set_n_cpu_gpu(Tint ncpu, Tint ngpu){
  _n_cpu = static_cast<int>(ncpu);
  _n_gpu = static_cast<int>(ngpu);
  if(_is_init){
    MAPHYSPP_WARNING("QrMumps::set_n_gpu() resetting some values. It is better to call it before setup");
    _finalize() override ;
  }
}
}; //class QrMumps
// Setters:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Traits][Traits:1]]
// Set traits
template<typename M, typename V>
struct is_solver_direct<QrMumps<M, V>> : public std::true_type {};
template<typename M, typename V>
struct is_solver_iterative<QrMumps<M, V>> : public std::false_type {};
template<typename M, typename V>
struct is_matrix_free<QrMumps<M, V>> : public std::false_type {};
template<typename M, typename V>
struct has_triangular_solve<QrMumps<M, V>> : public std::true_type {};

template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct matrix_type<QrMumps<Matrix, Vector>> : public std::true_type {
  using type = typename QrMumps<Matrix, Vector>::matrix_type;
};

template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct vector_type<QrMumps<Matrix, Vector>> : public std::true_type {
  using type = typename QrMumps<Matrix, Vector>::vector_type;
};

template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct scalar_type<QrMumps<Matrix, Vector>> : public std::true_type {
  using type = typename QrMumps<Matrix, Vector>::scalar_type;
};
// Traits:1 ends here

// [[file:../../../org/maphys/solver/QrMumps.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
