// [[file:../../../org/maphys/solver/LinearOperator.org::*LinearOperator][LinearOperator:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <type_traits>

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"

namespace maphys {

namespace parameters{
  CREATE_STRUCT(A)
}

// Declare solver traits to be used by heriting classes
template<class S> struct is_solver_direct : public std::false_type {};
template<class S> struct is_solver_iterative : public std::false_type {};
template<class S> struct is_matrix_free : public std::false_type {};

template<typename Operator, typename Operand>
struct LinearOperator{

  using operator_type = Operator;
  using vector_type = Operand;
  using scalar_type = typename scalar_type<Operand>::type;
  using real_type = typename arithmetic_real<scalar_type>::type;

  virtual ~LinearOperator() = default;

  // Must be implemented by inheriting classes
  virtual void setup(const Operator& A) = 0;
  virtual Operand apply(const Operand& B) = 0;
  //Optional: void apply(const Operand& B, Operand X)
};

template<typename Operator, typename Operand> [[nodiscard]]
Operand operator* (LinearOperator<Operator, Operand>& linop, const Operand& rhs){
  return linop.apply(rhs);
}

template<typename Operator, typename Operand> [[nodiscard]]
Operand operator* (const LinearOperator<Operator, Operand>& linop, const Operand& rhs){
  LinearOperator<Operator, Operand>& castlinop = const_cast<LinearOperator<Operator, Operand>&>(linop);
  return castlinop.apply(rhs);
}

// Try to convert the RHS to a vector if possible
template<typename Operator, typename Operand, typename RHS> [[nodiscard]]
Operand operator* (LinearOperator<Operator, Operand>& linop, const RHS& rhs){
  return linop.apply((Operand) rhs);
}

template<typename Operator, typename Operand, typename RHS> [[nodiscard]]
Operand operator* (const LinearOperator<Operator, Operand>& linop, const RHS& rhs){
  LinearOperator<Operator, Operand>& castlinop = const_cast<LinearOperator<Operator, Operand>&>(linop);
  return castlinop.apply((Operand) rhs);
}
// LinearOperator:1 ends here

// [[file:../../../org/maphys/solver/LinearOperator.org::*Create new linear operators from other linear operators][Create new linear operators from other linear operators:1]]
template<typename Operator, typename Operand>
struct CombinedLinearOperator :
  public LinearOperator<Operator, Operand> {

  using operator_type = Operator;
  using vector_type = Operand;
  using scalar_type = typename scalar_type<Operand>::type;
  using real_type = typename arithmetic_real<scalar_type>::type;

  LinearOperator<Operator, Operand> * op1;
  LinearOperator<Operator, Operand> * op2;
  std::function<Operand(const Operand&, const Operand&)> lin_fct;

  CombinedLinearOperator(LinearOperator<Operator, Operand>& o1,
                         LinearOperator<Operator, Operand>& o2,
                         std::function<Operand(const Operand&, const Operand&)> fct):
  op1{&o1}, op2{&o2}, lin_fct{fct}
  {
  }

  void setup(const Operator& A){
    op1->setup(A);
    op2->setup(A);
  }

  Operand apply(const Operand& B){
    return lin_fct(op1->apply(B), op2->apply(B));
  }
};

template<typename Operator, typename Operand>
CombinedLinearOperator<Operator, Operand> operator+(LinearOperator<Operator, Operand>& lop1, 
                                                    LinearOperator<Operator, Operand>& lop2){
  return CombinedLinearOperator<Operator, Operand>(lop1, lop2, 
         [](const Operand& opd1, const Operand& opd2){ return opd1 + opd2; });
}
// Create new linear operators from other linear operators:1 ends here

// [[file:../../../org/maphys/solver/LinearOperator.org::*Identity][Identity:1]]
template<typename Operator, typename Operand>
struct Identity : public LinearOperator<Operator, Operand> {
  using matrix_type = Operator;
  using vector_type = Operand;
  void setup(const Operator&){};
  Operand apply(const Operand& B){ return B; };
};

template<typename Operator, typename Operand>
struct vector_type<Identity<Operator, Operand>> : public std::true_type {
  using type = typename Identity<Operator, Operand>::vector_type;
};
} //namespace maphys
// Identity:1 ends here
