// [[file:../../../org/maphys/solver/IterativeSolver.org::*Header][Header:1]]
#pragma once

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/solver/LinearOperator.hpp"
#include "maphys/interfaces/linalg_concepts.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Iterative solver parameters][Iterative solver parameters:1]]
namespace parameters{
  CREATE_STRUCT(copy_A)
  CREATE_STRUCT(tolerance)
  CREATE_STRUCT(max_iter)
  CREATE_STRUCT(verbose)
  CREATE_STRUCT(verbose_mem)
  CREATE_STRUCT(verbose_ortho_loss)
  CREATE_STRUCT(algo)
  CREATE_STRUCT(check_true_residual)
  CREATE_STRUCT(always_true_residual)
  CREATE_STRUCT(setup_pcd)
  CREATE_STRUCT(setup_init_guess)
  CREATE_STRUCT(fixed_iter)
  CREATE_STRUCT(fixed_matrix)
  CREATE_STRUCT(restart)
  CREATE_STRUCT(fixed_rhs)
  CREATE_STRUCT(weight)
  CREATE_STRUCT(compression_accuracy)
  CREATE_STRUCT(orthogonalization)
  CREATE_STRUCT(custom_convergence_check)
  CREATE_STRUCT(stopping_crit_denom)
  CREATE_STRUCT(setup_inner_solver) // For IRSolver
} // end namespace parameters
// Iterative solver parameters:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Orthogonalization enumeration][Orthogonalization enumeration:1]]
enum class Ortho : int {CGS, CGS2, MGS, MGS2};
// Orthogonalization enumeration:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Base class][Base class:1]]
template<MPH_LinearOperator Matrix, MPH_Vector Vector, typename Precond = Identity<Matrix, Vector>>
class IterativeSolver :
    public LinearOperator<Matrix, Vector>{
// Base class:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Attributes][Attributes:1]]
protected:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
  using Real = typename LinearOperator<Matrix, Vector>::real_type;

public:
  using scalar_type = Scalar;
  using vector_type = Vector;
  using real_type = Real;
  using precond_type = Precond;
  using matrix_type = Matrix;
  static constexpr const bool use_preconditioner = !std::is_same<Precond, Identity<Matrix, Vector>>::value;

private:
  static const int _default_max_iter = 1000;
protected:
  // Input
  const Matrix * _A = nullptr; // Input matrix
  Matrix _A_copied;
  Precond _M; // Preconditioner
  Vector _R; // Residual (A X - B or its approximation in general)
  const Vector * _B = nullptr;
  Vector * _X = nullptr;
  bool _check_true_residual = true;
  bool _always_true_residual = false;
  int _max_iter = -1; // Unset

  // Output
  int _n_iter = 0;
  Real _residual_sq;

  bool _copy_A = false;
  bool _verbose = false;
  int _fixed_iter = -1; // Unset
  Real _tolerance = Real{1e-5};
  Real _tol_sq = Real{1e-10};
  Real _stop_crit_denom_inv = Real{-1};
  using PcdSetupFct = std::function<void(const Matrix&, Precond&)>;
  std::vector<PcdSetupFct> _setup_pcd;
  using SetupInitGuessFct = std::function<Vector(const Matrix&, const Vector&, Precond&)>;
  SetupInitGuessFct _setup_init_guess;
  using CustomConvFct = std::function<bool(const Matrix&, const Vector&, const Vector&, const Vector&, const Real)>;
  CustomConvFct _custom_convergence_check;

  // Shortcuts
  inline Scalar _dot(const Vector& v1, const Vector& v2) { return dot((Vector) v1, (Vector) v2); }
  inline Real _squared(const Vector& v) { return std::real(dot((Vector) v, (Vector) v)); }
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Iterative algorithm][Iterative algorithm:1]]
virtual int iterative_solve() = 0;
// Iterative algorithm:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Setup parameter functions][Setup parameter functions:1]]
private:
void setup_A(const Matrix * Aptr){
  if(!_copy_A){
    _A = Aptr;
    return;
  }

  if constexpr(std::is_copy_assignable<Matrix>::value){
    _A_copied = *Aptr;
    _A = &_A_copied;
  }
  else{ // Not copy assignable
    MAPHYSPP_WARNING("IterativeSolver::setup_A operator cannot be copied, taking a pointer");
    _A = Aptr;
  }
}

void _setup(const parameters::A<Matrix>&          v) { setup_A(&(v.value)); }
void _setup(const parameters::A<const Matrix>&    v) { setup_A(&(v.value)); }
void _setup(const parameters::copy_A<bool>&       v) { _copy_A  = v.value; }
void _setup(const parameters::tolerance<Real>&    v) { this->set_tolerance(v.value); }
void _setup(const parameters::max_iter<int>&      v) { this->set_max_iter(v.value); }
void _setup(const parameters::fixed_iter<int>&    v) { _fixed_iter = v.value; }
void _setup(const parameters::verbose<bool>&      v) { _verbose    = v.value; }
void _setup(const parameters::check_true_residual<bool>& v)  { _check_true_residual  = v.value; }
void _setup(const parameters::always_true_residual<bool>& v) { _always_true_residual = v.value; }
void _setup(const parameters::setup_pcd<PcdSetupFct>& v)     { _setup_pcd.push_back(v.value); }
void _setup(const parameters::setup_init_guess<SetupInitGuessFct>& v) { _setup_init_guess  = v.value; }
void _setup(const parameters::setup_pcd<CustomConvFct>& v)   { _custom_convergence_check = v.value; }
void _setup(const parameters::stopping_crit_denom<Real>& v)  { _stop_crit_denom_inv = (Real{1} / (v.value * v.value)); }

// Additional options for dervied classes
virtual void _setup(const parameters::fixed_matrix<bool>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(fixed_matrix)");
}
virtual void _setup(const parameters::fixed_rhs<bool>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(fixed_rhs)");
}
virtual void _setup(const parameters::weight<Scalar>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(weight)");
}
virtual void _setup(const parameters::restart<int>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(restart)");
}
virtual void _setup(const parameters::compression_accuracy<double>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(compression_accuracy)");
}
virtual void _setup(const parameters::orthogonalization<Ortho>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(orthogonalization)");
}
virtual void _setup(const parameters::verbose_mem<bool>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(verbose_mem)");
}
virtual void _setup(const parameters::verbose_ortho_loss<bool>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(verbose_ortho_loss)");
}
virtual void _setup(const parameters::setup_inner_solver<std::function<void(const Matrix&, const Vector&)>>&) {
  MAPHYSPP_WARNING("IterativeSolver: does not support setup(setup_inner_solver)");
}

public:
void setup () const {}

void setup(const Matrix& A){
  setup_A(&A);
}

// Variadic template -> each parameter is a call to the _setup function
template <typename... Types>
void setup(const Types&... args) noexcept { 
  ( void(_setup(args)), ...);
}

template<typename... Types>
void setup(const Matrix& A, const Types&... args){
  this->setup(A);
  setup(args...);
}

IterativeSolver(){}

IterativeSolver(const Matrix& A, bool verb=false){
  this->setup(A);
  _verbose = verb;
}
// Setup parameter functions:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Getters and setters][Getters and setters:1]]
// Getters
inline const Matrix * get_input_matrix() const { return _A; }
inline int get_n_iter() const { return _n_iter; }  // Return -1 if no convergence
inline int get_n_iter_performed() const { // Return max_iter if no convergence
  if(_n_iter >= 0) return _n_iter; 
  return _max_iter;
}
inline Real get_residual() const { return std::sqrt(_residual_sq); }
inline Real get_tolerance() const { return _tolerance; }
Precond& get_preconditioner() { return _M; }

// Setters
inline void set_verbose(const bool verbose){ _verbose = verbose; }
void set_max_iter(const int max_iter){
  MAPHYSPP_ASSERT( max_iter > 0, "IterativeSolver: max_iter must be strictly positive.");
  _max_iter = max_iter;
}
void set_tolerance(const Real tolerance){
  MAPHYSPP_ASSERT( tolerance > 0, "IterativeSolver: tolerance must be strictly positive.");
  _tolerance = tolerance;
}
// Getters and setters:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Solve function][Solve function:1]]
void apply(const Vector& B, Vector& X){
  Timer<TIMER_SOLVER> t("Iterative solve");
  MAPHYSPP_ASSERT(_A != nullptr, "IterativeSolver: A is nullptr! Solver cannot solve if a matrix A has not been set with .setup(A) first!");

  _B = &B;
  _X = &X;
  _tol_sq = _tolerance * _tolerance;
  if(_max_iter < 0){
    MAPHYSPP_WARNING("IterativeSolver: max_iter unset, setting to default value");
    _max_iter = _default_max_iter;
  }

  // Setup preconditioner if any
  if constexpr(use_preconditioner){
    if(_setup_pcd.size() == 0){
      if constexpr(std::is_same<typename Precond::matrix_type, Matrix>::value){
          _M.setup(*_A);
      }
    }
    else{
      for(PcdSetupFct setup_fct : _setup_pcd){
        setup_fct(*_A, _M);
      }
    }
  }

  if(_setup_init_guess){
    (*_X) = _setup_init_guess(*_A, *_B, _M);
  }

  _n_iter = iterative_solve();
}

Vector apply(const Vector& B){
  Vector X = Scalar{0.0} * B; // Create X same size as B
  apply(B, X);
  return X;
}

Vector solve(const Vector& B) { return apply(B); }
void solve(const Vector& B, Vector& X) { apply(B, X); }
// Solve function:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Stopping criteria][Stopping criteria:1]]
protected:
   void setup_initial_stop_crit(const std::string& solver_name){
     const Real bsq = _squared(*_B);
     if(_stop_crit_denom_inv == Real{-1}){
       _stop_crit_denom_inv = (bsq == Real{0}) ? Real{1} : (Real{1} / bsq);
     }

     if(_verbose){
       std::cout << "Starting " << solver_name << ". ";
       std::cout << "Max iterations: " << _max_iter << '\n';
       if(_custom_convergence_check){
         std::cout << "Stopping criterion custom\n";
       }
       else{
         std::cout << "Stopping criterion ||R||/||B|| < " << std::sqrt(_tol_sq) << '\n';
         std::cout << "with ||B|| = " << std::sqrt(bsq) << "\n---\n0 -\t";
       }
     }
   }

   void true_residual_sq(){
     _R = (*_B) - (*_A) * (*_X);
     _residual_sq = _squared(_R)  * _stop_crit_denom_inv;
     if(_verbose) std::cout << "||B-AX||/||B||\t" << std::sqrt(_residual_sq) << '\n';
   }
   
   void approx_residual_sq(){
     _residual_sq = _squared(_R)  * _stop_crit_denom_inv;
     if(_verbose) std::cout << "||R||/||B||\t" << std::sqrt(_residual_sq) << '\n';
   }
 
   bool _convergence_achieved(){
     if(_custom_convergence_check){
       return _custom_convergence_check((*_A), (*_X), (*_B), _R, _tolerance);
     }
     if(_always_true_residual){
       // ||B - A X||^2 / || B ||^2 < tol^2
       true_residual_sq();
       return (_residual_sq < _tol_sq);
     }

     // ||R||^2 / || B ||^2 < tol^2
     approx_residual_sq();
     bool approx_res_converged = (_residual_sq < _tol_sq);
     if(approx_res_converged && _check_true_residual){
       if(_verbose) std::cout << '\t';
       true_residual_sq();
       return (_residual_sq < _tol_sq);
     }
     
     return approx_res_converged;
   }
// Stopping criteria:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Display function][Display function:1]]
public:
  void display(const std::string& name="", std::ostream &out = std::cout) const {
    if(!name.empty()) out << "Solver name: " << name << '\n';
    out << std::boolalpha;
    out << "  - On matrix A at: " << _A << '\n';
    out << "  - Using preconditioner: " << use_preconditioner << '\n';
    out << "  - On arithmetic: " << arithmetic_name<Scalar>::name << '\n';
    if( _fixed_iter > 0 ){
      out << "  - Fixed number of iter: " << _fixed_iter << '\n';
    }
    else{
      out << "  - Max iter: " << _max_iter << '\n';
      out << "  - Tolerance: " << _tolerance << '\n';
    }

    out << "  - Check true residual: " << _check_true_residual << '\n';
    out << "  - Niter performed last solve: " << _n_iter << '\n';
    out << "  - With residual : " << this->get_residual() << '\n';
    out << "  - Verbose: " << _verbose << '\n';
  }
// Display function:1 ends here

// [[file:../../../org/maphys/solver/IterativeSolver.org::*Footer][Footer:1]]
}; // class IterativeSolver
} // namespace maphys
// Footer:1 ends here
