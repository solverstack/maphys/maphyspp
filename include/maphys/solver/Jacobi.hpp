// [[file:../../../org/maphys/solver/Jacobi.org::*Header][Header:1]]
#pragma once

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/solver/IterativeSolver.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/Jacobi.org::*Base class][Base class:1]]
template<typename Matrix, MPH_Vector Vector>
class Jacobi :
    public IterativeSolver<Matrix, Vector> {
// Base class:1 ends here

// [[file:../../../org/maphys/solver/Jacobi.org::*Attributes][Attributes:1]]
private:
  using IterativeSolver<Matrix, Vector>::_A;
  using IterativeSolver<Matrix, Vector>::_B;
  using IterativeSolver<Matrix, Vector>::_X;
  using IterativeSolver<Matrix, Vector>::_R;
  using IterativeSolver<Matrix, Vector>::_squared;
  using IterativeSolver<Matrix, Vector>::_tol_sq;
  using IterativeSolver<Matrix, Vector>::_max_iter;
  using IterativeSolver<Matrix, Vector>::_residual_sq;
  using IterativeSolver<Matrix, Vector>::_verbose;
  using IterativeSolver<Matrix, Vector>::_stop_crit_denom_inv;

  using Scalar = typename IterativeSolver<Matrix, Vector>::scalar_type;
  using Real = typename IterativeSolver<Matrix, Vector>::real_type;

  bool _compute_Z = true;
  bool _compute_t = true;
  bool _fixed_Z = false;
  bool _fixed_t = false;

  // weight
  Scalar omega = Scalar{1};
  // D^-1
  Vector D_inv;
  //Z = D^-1(E+F)
  //or Z = I - (omega D^-1 A)
  Matrix Z;
  //t = omega D^-1 * b
  Vector t;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/Jacobi.org::*Computation of auxilary matrix and vector][Computation of auxilary matrix and vector:1]]
// Compute from A the left matrix Z = D^-1(E+F)
void compute_Z(const Matrix& A){
  Z = A;
  if constexpr(is_distributed<Matrix>::value){
    for(int sd_id : Z.get_sd_ids()){
      auto& loc_Z = Z.get_local_matrix(sd_id);
      const auto& loc_diag = D_inv.get_local_vector(sd_id);
      compute_Z_local(loc_Z, loc_diag);
    }
  }
  else{
    compute_Z_local(Z, D_inv);
  }
  if(_fixed_Z) _compute_Z = false;
}

template<MPH_DenseVector DenseV, typename DenseM>
void compute_Z_local(DenseM& Z_loc, const DenseV& D_inv_loc){
  const Size m = n_rows(Z_loc);
  const Size n = n_cols(Z_loc);
  MAPHYSPP_ASSERT( m == n, "Jacobi solver must be on squared matrix");

  const Scalar diag_val = Scalar{1} - omega;

  for(Size i = 0; i < m; ++i){
    for(Size j = 0; j < m; ++j){
      if(i == j){
        Z_loc(i, j) = diag_val;
      }
      else{
        Z_loc(i, j) = -Z_loc(i, j) * D_inv_loc(i) * omega;
      }
    }
  }
}

template<MPH_DenseVector DenseV, MPH_DenseMatrix DenseM>
void compute_Z_local_weighted(DenseM& Z_loc, const DenseV& D_inv_loc){

  const Size m = n_rows(Z_loc);
  const Size n = n_cols(Z_loc);
  MAPHYSPP_ASSERT( m == n, "Jacobi solver must be on squared matrix");

  for(Size i = 0; i < m; ++i){
    for(Size j = 0; j < m; ++j){
      if(i == j){
        Z_loc(i, j) = Scalar{0};
      }
      else{
        Z_loc(i, j) = -Z_loc(i, j) * D_inv_loc(i);
      }
    }
  }
}

void compute_t(const Vector& B){
  t = B;

  auto multiply_terms = [](Scalar& s1, const Scalar& s2){
    s1 *= s2;
  };
  if constexpr (is_distributed<Vector>::value){
    t.apply(multiply_terms, D_inv);
  }
  else{ // Local vector
    for(Size i = 0; i < size(B); ++i){
      multiply_terms(t(i), D_inv(i));
    }
  }

  if(omega != Scalar{1}) t *= omega;

  if(_fixed_t) _compute_t = false;
}
// Computation of auxilary matrix and vector:1 ends here

// [[file:../../../org/maphys/solver/Jacobi.org::*Iterative solve][Iterative solve:1]]
int iterative_solve() override {
  Timer<TIMER_ITERATION> t0("Jacobi iteration 0");
  // No need for IterativeSolver to recompute the residual
  // because here R is already the true residual
  this->_check_true_residual = false;
  const Matrix& A = *_A;
  const Vector& B = *_B;
  Vector& X = *_X;
  Vector& R = _R;

  this->setup_initial_stop_crit("Jacobi");

  auto inverse_diag = [](Scalar& s){
                        if(s == Scalar{0}){
                          s = Scalar{1};
                        }
                        else{
                          s = Scalar{1}/s;
                        }
                      };
  if(_compute_Z){
    D_inv = diagonal_as_vector(A);
    if constexpr (is_distributed<Matrix>::value){
      D_inv.assemble(); // Not sure about that
      D_inv.apply(inverse_diag);
    }
    else{
      for(Size i = 0; i < size(B); ++i){
        inverse_diag(D_inv(i));
      }
    }
    compute_Z(A);
  }
  if(_compute_t){
    compute_t(B);
  }
  R = B - A * X;

  _residual_sq = _squared(R) * _stop_crit_denom_inv;
  t0.stop();

  if(_verbose) std::cout << "||B-AX||/||B||\t" << std::sqrt(_residual_sq) << '\n';
  if(_residual_sq < _tol_sq) return 0;

  for(int iter = 1; iter < _max_iter + 1; ++iter){
    Timer<TIMER_ITERATION> t_iter("Jacobi iteration");
    if(_verbose) std::cout << iter << " -\t";

    // X(k+1) = D^-1 * (E+F) * X(k) + D^-1 * b
    X = Z * X + t;
    R = B - A * X;
    _residual_sq = _squared(R) * _stop_crit_denom_inv;
    if(_verbose) std::cout << "||B-AX||/||B||\t" << std::sqrt(_residual_sq) << '\n';
    if(_residual_sq < _tol_sq) return iter;
  }

  return -1;
}
// Iterative solve:1 ends here

// [[file:../../../org/maphys/solver/Jacobi.org::*Constructors][Constructors:1]]
public:

  Jacobi(): IterativeSolver<Matrix, Vector>() {}

  Jacobi(const Matrix& A, bool verb=false):
    IterativeSolver<Matrix, Vector>(A, verb) {}
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/Jacobi.org::*Setup functions][Setup functions:1]]
void _setup(const parameters::fixed_matrix<bool>& v) override {
  _fixed_Z = v.value;
  if(!_fixed_Z) _compute_Z = true;
}
void _setup(const parameters::fixed_rhs<bool>& v) override {
  _fixed_t = v.value;
  if(!_fixed_t) _compute_t = true;
}
void _setup(const parameters::weight<Scalar>& v) override {
  omega = v.value;
}
}; // class Jacobi
// Setup functions:1 ends here

// [[file:../../../org/maphys/solver/Jacobi.org::*Traits][Traits:1]]
template<typename Matrix, typename Vector>
struct is_solver_direct<Jacobi<Matrix, Vector>> : public std::false_type {};
template<typename Matrix, typename Vector>
struct is_solver_iterative<Jacobi<Matrix, Vector>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/maphys/solver/Jacobi.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
