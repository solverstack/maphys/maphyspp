// [[file:../../../org/maphys/solver/CoarseSolve.org::*Header][Header:1]]
#pragma once

#include <maphys/solver/LinearOperator.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/loc_data/SparseMatrixLIL.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Class definition][Class definition:1]]
template<class LocalMatrix, class LocalVector, class GlobSolver>
class CoarseSolve : public LinearOperator<PartMatrix<LocalMatrix>, PartVector<LocalVector>>{
// Class definition:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Attributes][Attributes:1]]
public:
  using scalar_type = typename LocalMatrix::scalar_type;
  using real_type = typename LocalMatrix::real_type;
  using matrix_type = LocalMatrix;
  using vector_type = LocalVector;
  using local_solver_type = typename GlobSolver::solver_type;

private:
  using Scalar = scalar_type;
  using Real = real_type;
  using Matrix = PartMatrix<LocalMatrix>;
  using Vector = PartVector<LocalVector>;
  using LocDenseMatrix = typename dense_type<LocalMatrix>::type;
  using LocDiag = typename diag_type<LocalMatrix>::type;
  using Diagonal = PartMatrix<LocDiag>;

  const Matrix * _A = nullptr;
  PartMatrix<LocDenseMatrix> _V0i;

  // "Local matrices", not associated to a dd
  std::map<int, LocalMatrix> _V0i_bar;
  //std::map<int, LocalMatrix> _AiV0i_bar;
  std::map<int, LocDenseMatrix> _AiV0i_bar;

  std::shared_ptr<Process> _dd0;
  GlobSolver _A0_inv;
  bool _is_setup = false;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Constructors][Constructors:1]]
public:
  CoarseSolve() {}
  CoarseSolve(const Matrix& A): _A{&A} {}
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Setup][Setup:1]]
void setup(const Matrix& A){
  Timer<TIMER_SOLVER> t("CoarseSolve_setup");
  _A = &A;
  const bool on_interface = A.on_intrf();

  // If no coarse space is user-provided, use partition of unity
  if(_V0i.get_proc() == nullptr){
    auto V0_1 = AbstractSchwarz<Matrix, Vector, GlobSolver, false, PartOfUnityMethod::matrix>::partition_of_unity(A);
    _V0i.initialize(V0_1.get_proc());
    for(const int loc_sd : _V0i.get_proc()->get_sd_ids()){
      _V0i.add_subdomain(loc_sd, LocDenseMatrix(V0_1.get_local_vector(loc_sd)));
    }
  }

  Process& proc = *(_V0i.get_proc());
  const IndexArray<int> local_subdomains = proc.get_sd_ids();

  // Send buffer : [0] -> number of coarse vectors
  // [1] : nb nei
  // [2:] neighbors
  std::map<std::pair<int, int>, std::vector<int>> send_int_buffers;
  // Coarse vectors
  std::map<std::pair<int, int>, std::vector<char>> send_vectors_buffer;

  for(const int loc_sd : local_subdomains){
    const auto& loc_mat = _V0i.get_local_matrix(loc_sd);
    IndexArray<int> neighbors = proc.get_sd_neighbors(loc_sd);

    for(const int nei_sd : neighbors){
      const Size n_vect = n_cols(loc_mat);
      const IndexArray<int> interface = proc.get_interface_between(loc_sd, nei_sd, on_interface);

      // Integer buffer
      std::vector<int> bufi(2 + neighbors.size());
      bufi[0] = static_cast<int>(n_vect); // Number of coarse vectors
      bufi[1] = static_cast<int>(neighbors.size()); // Number of neighbors
      std::memcpy(&bufi[2], &neighbors[0], neighbors.size() * sizeof(int));
      send_int_buffers[std::make_pair(loc_sd, nei_sd)] = std::move(bufi);

      // Vectors buffer
      SparseMatrixLIL<Scalar> smat(n_rows(loc_mat), n_vect);
      for(int j = 0; j < static_cast<int>(n_vect); ++j){
        for(Size i = 0; i < interface.size(); ++i){
          Scalar val = loc_mat(interface[i], j);
          if(val != Scalar{0}) smat.insert(i, j, val);
        }
      }
      send_vectors_buffer[std::make_pair(loc_sd, nei_sd)] = smat.to_coo().serialize();
    }
  }

  std::map<std::pair<int, int>, std::vector<int>> recv_i = proc.exchange_with_neighbors(send_int_buffers);
  send_int_buffers.clear();
  std::map<std::pair<int, int>, std::vector<char>> recv_v = proc.exchange_with_neighbors(send_vectors_buffer);
  send_vectors_buffer.clear();

  // Add local contribution to the received buffers
  for(const int loc_sd : local_subdomains){
    const auto& loc_mat = _V0i.get_local_matrix(loc_sd);
    const Size n_vect = n_cols(loc_mat);
    const Size n_dofs = n_rows(loc_mat);
    const IndexArray<int> neighbors = proc.get_sd_neighbors(loc_sd);

    std::vector<int> bufi(2 + neighbors.size());
    bufi[0] = n_vect;
    bufi[1] = neighbors.size();
    std::memcpy(&bufi[2], &neighbors[0], neighbors.size() * sizeof(int));

    recv_i[std::make_pair(loc_sd, loc_sd)] = bufi;

    SparseMatrixLIL<Scalar> smat(n_dofs, n_vect);
    for(Size j = 0; j < n_vect; ++j){
      for(Size i = 0; i < n_dofs; ++i){
        if(loc_mat(i, j) != Scalar{0}) smat.insert(i, j, loc_mat(i, j));
      }
    }
    recv_v[std::make_pair(loc_sd, loc_sd)] = smat.to_coo().serialize();
  }

  std::map<int, LocalMatrix> A0_loc;
  std::vector<Subdomain> subdomains0;

  for(const int loc_sd : local_subdomains){
    const IndexArray<int> neighbors = proc.get_sd_neighbors(loc_sd);

    IndexArray<int> nei_plus_me(neighbors.size() + 1);
    for(size_t k = 0; k < neighbors.size(); ++k) nei_plus_me[k] = neighbors[k];
    nei_plus_me[neighbors.size()] = loc_sd;
    nei_plus_me.sort();

    Size n_vect_tot = 0;
    for(const int nei_sd : nei_plus_me){
      n_vect_tot += recv_i[std::make_pair(loc_sd, nei_sd)][0];
    }
    const Size n_dofs = n_rows(_V0i.get_local_matrix(loc_sd));

    LocDenseMatrix V0_locmat(n_dofs, n_vect_tot);
    Size counter = 0;
    std::map<int, IndexArray<int>> nei2dof;
    for(const int nei_sd : nei_plus_me){
      IndexArray<int> interface;
      if(nei_sd == loc_sd){
        interface = arange<int>(static_cast<int>(n_dofs));
      }
      else{
        interface = proc.get_interface_between(loc_sd, nei_sd, on_interface);
      }

      const std::vector<int>& received_i = recv_i[std::make_pair(loc_sd, nei_sd)];
      // 1. Number of vectors
      const Size n_vect = static_cast<Size>(received_i[0]);
      if(n_vect == 0) continue;

      // 2. Neighbors
      IndexArray<int> r_nei0(received_i.size() - 2);
      std::memcpy(r_nei0.data(), &received_i[2], r_nei0.size() * sizeof(int));
      r_nei0.push_back(nei_sd);

      for(const int nei : r_nei0){
        if(nei != loc_sd){
          if(nei2dof.count(nei) == 0) nei2dof[nei] = IndexArray<int>();
          for(Size jloc = counter; jloc < (counter + n_vect); ++jloc){
            nei2dof[nei].push_back(jloc);
          }
        }
      }

      // 3. Copy vectors received into the matrix
      SparseMatrixCOO<Scalar> V0_recved;
      V0_recved.deserialize(recv_v[std::make_pair(loc_sd, nei_sd)]);
      IJVIterator<Scalar, int, SparseMatrixCOO<Scalar, int>> it(V0_recved);
      for(const auto& [i, j, v] : it){
        V0_locmat(interface[i], counter + j) += v;
      }
      counter += n_vect;
    }
    const Size n_g = n_cols(V0_locmat);

    const auto& A_loc = A.get_local_matrix(loc_sd);
    _AiV0i_bar[loc_sd] = A_loc * V0_locmat;
    A0_loc[loc_sd] = adjoint(V0_locmat) * _AiV0i_bar[loc_sd];
    _V0i_bar[loc_sd] = std::move(V0_locmat);

    subdomains0.emplace_back(loc_sd, n_g, nei2dof, false);
  }

  _dd0 = bind_subdomains(proc.get_n_sd_total(), 1, proc.init_comm());
  _dd0->load_subdomains_interface(subdomains0);

  Matrix A0(_dd0);
  for(auto& [sd_id, loc_mat] : A0_loc){
    loc_mat.set_property(A.get_local_matrix(sd_id).get_symmetry());
    A0.add_subdomain(sd_id, loc_mat);
  }

  _A0_inv = GlobSolver(A0);
}
// Setup:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Apply / project][Apply / project:1]]
template<bool project = false>
Vector apply_or_project(const Vector& B){
  Timer<TIMER_SOLVER> t("CoarseSolve_apply");
  Vector x = B;

  if constexpr(!project) { x.disassemble(); }

  Vector b0(_dd0);
  for(const int loc_sd : B.get_proc()->get_sd_ids()){
    if constexpr(project){ // P0 b = M0 A b
      b0.get_local_vector(loc_sd) = adjoint(_AiV0i_bar[loc_sd]) * x.get_local_vector(loc_sd);
    }
    else{ // M0 b
      b0.get_local_vector(loc_sd) = adjoint(_V0i_bar[loc_sd]) * x.get_local_vector(loc_sd);
    }
  }
  b0.assemble();
  Vector x0 = _A0_inv * b0;

  for(const int loc_sd : B.get_proc()->get_sd_ids()){
    x.get_local_vector(loc_sd) = _V0i_bar[loc_sd] * x0.get_local_vector(loc_sd);
  }

  return x;
}

Vector apply(const Vector& B){
  return apply_or_project<false>(B);
}

Vector project(const Vector& B){
  return apply_or_project<true>(B);
}
// Apply / project:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Getters][Getters:1]]
GlobSolver& get_glob_solver(){ return _A0_inv; }
// Getters:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Setters][Setters:1]]
void set_coarse_space(const PartMatrix<LocDenseMatrix>& V0i){ _V0i = V0i; }
void set_coarse_space(PartMatrix<LocDenseMatrix>&& V0i){ _V0i = std::move(V0i); }
// Setters:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Display][Display:1]]
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << name << '\n';
  out << "CoarseSolve\n";
  out << std::boolalpha <<
  " - A ptr is nullptr : " << (_A == nullptr) << '\n' <<
  " - dd0 ptr is nullptr : " << (_dd0 == nullptr) << '\n' <<
  " - is setup : " << _is_setup << '\n';
}
// Display:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Traits][Traits:1]]
}; // class CoarseSolve
// Traits:1 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Traits][Traits:2]]
template<class LocalMatrix, class LocalVector, class GlobSolver>
struct vector_type<CoarseSolve<LocalMatrix, LocalVector, GlobSolver>> : public std::true_type {
  using type = typename CoarseSolve<LocalMatrix, LocalVector, GlobSolver>::vector_type;
};

template<class LocalMatrix, class LocalVector, class GlobSolver>
struct scalar_type<CoarseSolve<LocalMatrix, LocalVector, GlobSolver>> : public std::true_type {
  using type = typename CoarseSolve<LocalMatrix, LocalVector, GlobSolver>::scalar_type;
};
// Traits:2 ends here

// [[file:../../../org/maphys/solver/CoarseSolve.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
