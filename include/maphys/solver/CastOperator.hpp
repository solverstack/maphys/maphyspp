// [[file:../../../org/maphys/solver/CastOperator.org::*CastOperator][CastOperator:1]]
#pragma once

  #include "maphys/utils/Error.hpp"
  #include "maphys/solver/LinearOperator.hpp"

  namespace maphys {
  template<typename Operator, typename Operand, typename InnerOperator>
  struct CastOperator: public LinearOperator<Operator, Operand> {

    InnerOperator inner_op;

    using Scalar = typename Operator::scalar_type;
    using operator_type = Operator;
    using matrix_type = Operator;
    using vector_type = Operand;

    using InnerScalar = typename InnerOperator::scalar_type;
    using inner_operator_type = InnerOperator;
    using inner_vector_type = typename InnerOperator::vector_type;
    using inner_scalar_type = InnerScalar;

    void setup(const Operator& A){
      inner_op.setup(A.template cast<InnerScalar>());
    }

    Operand apply(const Operand& B){
      auto out = inner_op.apply(B.template cast<InnerScalar>());
      return out.template cast<Scalar>();
    }

    void apply(const Operand& B, Operand X){
      auto out = inner_op.apply(B.template cast<InnerScalar>());
      X = out.template cast<Scalar>();
    }

    inline int get_n_iter() const { 
      if constexpr(is_solver_iterative<InnerOperator>::value){
        return inner_op.get_n_iter();
      } else { return 1; }
    }

    inline int get_n_iter_performed() const { 
      if constexpr(is_solver_iterative<InnerOperator>::value){
        return inner_op.get_n_iter_performed();
      } else { return 1; }
    }
  }; // struct CastOperator

  template<typename Operator, typename Operand, typename InnerOperator>
  struct is_solver_direct<CastOperator<Operator, Operand, InnerOperator>>{ static constexpr const bool value = is_solver_direct<InnerOperator>::value; };
  template<typename Operator, typename Operand, typename InnerOperator>
  struct is_solver_iterative<CastOperator<Operator, Operand, InnerOperator>>{ static constexpr const bool value = is_solver_iterative<InnerOperator>::value; };
  template<typename Operator, typename Operand, typename InnerOperator>
  struct is_matrix_free<CastOperator<Operator, Operand, InnerOperator>>{ static constexpr const bool value = is_matrix_free<InnerOperator>::value; };

  template<typename Operator, typename Operand, typename InnerOperator>
  struct vector_type<CastOperator<Operator, Operand, InnerOperator>> : public std::true_type {
    using type = typename CastOperator<Operator, Operand, InnerOperator>::vector_type;
  };

  template<typename Operator, typename Operand, typename InnerOperator>
  struct scalar_type<CastOperator<Operator, Operand, InnerOperator>> : public std::true_type {
    using type = typename CastOperator<Operator, Operand, InnerOperator>::scalar_type;
  };
} // namespace maphys
// CastOperator:1 ends here
