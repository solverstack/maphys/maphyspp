// [[file:../../../org/maphys/solver/ConjugateGradient.org::*Header][Header:1]]
#pragma once

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/solver/IterativeSolver.hpp"
#include "maphys/linalg/SpectralExtraction.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/ConjugateGradient.org::*Attributes][Attributes:1]]
template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond = Identity<Matrix, Vector>>
class ConjugateGradient :
    public IterativeSolver<Matrix, Vector, Precond> {

private:
  using Scalar = typename IterativeSolver<Matrix, Vector, Precond>::scalar_type;
  using Real = typename IterativeSolver<Matrix, Vector, Precond>::real_type;
  using IterativeSolver<Matrix, Vector, Precond>::_A;
  using IterativeSolver<Matrix, Vector, Precond>::_B;
  using IterativeSolver<Matrix, Vector, Precond>::_X;
  using IterativeSolver<Matrix, Vector, Precond>::_R;
  using IterativeSolver<Matrix, Vector, Precond>::_M;
  using IterativeSolver<Matrix, Vector, Precond>::_dot;
  using IterativeSolver<Matrix, Vector, Precond>::_squared;
  using IterativeSolver<Matrix, Vector, Precond>::_tol_sq;
  using IterativeSolver<Matrix, Vector, Precond>::_max_iter;
  using IterativeSolver<Matrix, Vector, Precond>::_check_true_residual;
  using IterativeSolver<Matrix, Vector, Precond>::_always_true_residual;
  using IterativeSolver<Matrix, Vector, Precond>::_residual_sq;
  using IterativeSolver<Matrix, Vector, Precond>::_verbose;
  using IterativeSolver<Matrix, Vector, Precond>::_convergence_achieved;

public:
  using scalar_type = Scalar;
  using vector_type = Vector;
  using matrix_type = Matrix;
  using real_type = Real;

private:
  SpectralExtraction<Matrix, Vector> * _spectral_extraction = nullptr;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/ConjugateGradient.org::*C++ code][C++ code:1]]
int iterative_solve() override {
  Timer<TIMER_ITERATION> t0("CG iteration 0");
  if constexpr(has_spectral_extraction<Precond>::value) {
    _spectral_extraction = _M.get_spectral_extraction();
    MAPHYSPP_ASSERT(_spectral_extraction != nullptr, "Preconditionner needs a spectral extraction but none is given");
  }

  const Matrix& A = *_A;
  const Vector& B = *_B;
  Vector& X = *_X;
  Vector& R = _R;

  this->setup_initial_stop_crit("Conjugate Gradient");

  R = B - A * X;

  if(_convergence_achieved()) return 0;

  Vector Z;
  Vector P;

  Vector Ap = X * Scalar{0}; // Initialize Ap with correct size, avoid shallow copy
  Z = _M * R;
  P = Z;

  Scalar rz = _dot(R, P);

  if constexpr(has_spectral_extraction<Precond>::value) { _spectral_extraction->first_iteration(rz); }
  t0.stop();

  Scalar alpha;
  for(int iter = 1; iter < _max_iter + 1; ++iter){
    Timer<TIMER_ITERATION> t("CG iteration");

    if(_verbose) std::cout << iter << " -\t";

    Ap = A * P;
    alpha = rz / _dot(P, Ap);

    X += alpha * P;
    R -= alpha * Ap;
    //if constexpr(has_spectral_extraction<Precond>::value) _spectral_extraction->iteration(Ap, X, R, alpha, rz);

    if(_convergence_achieved()) return iter;

    Scalar rz_old = rz;
    Z = _M * R;
    rz = _dot(R, Z);
    Scalar beta = (rz / rz_old);
    P = Z + beta * P;

    if constexpr(has_spectral_extraction<Precond>::value) _spectral_extraction->iteration_end(Ap, alpha, beta, rz);
  }

  if constexpr (has_spectral_extraction<Precond>::value) _spectral_extraction->finalize();
  return -1;
}
// C++ code:1 ends here

// [[file:../../../org/maphys/solver/ConjugateGradient.org::*CG constructors][CG constructors:1]]
public:

  ConjugateGradient(): IterativeSolver<Matrix, Vector, Precond>() {}

  ConjugateGradient(const Matrix& A, bool verb=false):
    IterativeSolver<Matrix, Vector, Precond>(A, verb) {}
// CG constructors:1 ends here

// [[file:../../../org/maphys/solver/ConjugateGradient.org::*CG constructors][CG constructors:2]]
}; // class ConjugateGradient
// CG constructors:2 ends here

// [[file:../../../org/maphys/solver/ConjugateGradient.org::*Class traits][Class traits:1]]
// Set traits
template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_solver_direct<ConjugateGradient<Matrix, Vector, Precond>> : public std::false_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_solver_iterative<ConjugateGradient<Matrix, Vector, Precond>> : public std::true_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_matrix_free<ConjugateGradient<Matrix, Vector, Precond>> : public std::true_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct vector_type<ConjugateGradient<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename ConjugateGradient<Matrix, Vector, Precond>::vector_type;
};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct scalar_type<ConjugateGradient<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename ConjugateGradient<Matrix, Vector, Precond>::scalar_type;
};
// Class traits:1 ends here

// [[file:../../../org/maphys/solver/ConjugateGradient.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
