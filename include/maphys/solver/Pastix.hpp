// [[file:../../../org/maphys/solver/Pastix.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <type_traits>
#include <algorithm>
#include <numeric>

#include <maphys/interfaces/linalg_concepts.hpp>
namespace maphys {
template<MPH_SparseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector> class Pastix;
}

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/solver/LinearOperator.hpp"

#include <pastix.h>
#include <spm.h>

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*SPM arithmetic types][SPM arithmetic types:1]]
/* Arithmetic structure for pastix */

#define SET_SPM_ARITHMETIC( type_, enumtype_ )                      \
  template<>                                                        \
  struct PastixScalar<type_> : public std::true_type {              \
    static const constexpr pastix_coeftype_t enumtype = enumtype_;  \
  };

template<class Scalar>
struct PastixScalar : public std::false_type {};

SET_SPM_ARITHMETIC(float               , SpmFloat     )
SET_SPM_ARITHMETIC(double              , SpmDouble    )
SET_SPM_ARITHMETIC(std::complex<float> , SpmComplex32 )
SET_SPM_ARITHMETIC(std::complex<double>, SpmComplex64 )

/*---------------------------------*/
// SPM arithmetic types:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Attributes][Attributes:1]]
template<MPH_SparseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
class Pastix :
    public LinearOperator<Matrix, Vector> {

private:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
  using Real = typename LinearOperator<Matrix, Vector>::real_type;

public:
  using matrix_type = Matrix;
  using vector_type = Vector;
  using scalar_type = Scalar;

private:
  static const constexpr pastix_coeftype_t _scalartype = PastixScalar<Scalar>::enumtype;
  pastix_data_t * _pastix_data = nullptr; /*< Pointer to the storage structure required by pastix */
  spmatrix_t *    _spm = nullptr;
  pastix_int_t    _nrhs  = 1;
  pastix_rhs_t    _xp = NULL;

  std::vector<pastix_int_t> _iparm;  // Integer in/out parameters for pastix
  std::vector<double>       _dparm;  // Floating in/out parameters for pastix

  std::vector<int> _bindtab; // Binding for multithreading

  // Schur complement stuff
  pastix_int_t _n_schur = -1;
  Vector _x;

  bool _is_setup = false;
  bool _is_facto = false;

  bool _use_refinement = true;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Pastix errors][Pastix errors:1]]
// Dealing with error
void catch_pastix_error(const int pastix_output) const {

  switch(pastix_output){
  case PASTIX_SUCCESS: // OK!
    break;
  case PASTIX_ERR_UNKNOWN:
    std::cerr << "Pastix return with error PASTIX_ERR_UNKNOWN" << '\n';
    break;
  case PASTIX_ERR_ALLOC:
    std::cerr << "Pastix return with error PASTIX_ERR_ALLOC" << '\n';
    break;
  case PASTIX_ERR_NOTIMPLEMENTED:
    std::cerr << "Pastix return with error PASTIX_ERR_NOTIMPLEMENTED" << '\n';
    break;
  case PASTIX_ERR_OUTOFMEMORY:
    std::cerr << "Pastix return with error PASTIX_ERR_OUTOFMEMORY" << '\n';
    break;
  case PASTIX_ERR_THREAD:
    std::cerr << "Pastix return with error PASTIX_ERR_THREAD" << '\n';
    break;
  case PASTIX_ERR_INTERNAL:
    std::cerr << "Pastix return with error PASTIX_ERR_INTERNAL" << '\n';
    break;
  case PASTIX_ERR_BADPARAMETER:
    std::cerr << "Pastix return with error PASTIX_ERR_BADPARAMETER" << '\n';
    break;
  case PASTIX_ERR_FILE:
    std::cerr << "Pastix return with error PASTIX_ERR_FILE" << '\n';
    break;
  case PASTIX_ERR_INTEGER_TYPE:
    std::cerr << "Pastix return with error PASTIX_ERR_INTEGER_TYPE" << '\n';
    break;
  case PASTIX_ERR_IO:
    std::cerr << "Pastix return with error PASTIX_ERR_IO" << '\n';
    break;
  case PASTIX_ERR_MPI:
    std::cerr << "Pastix return with error PASTIX_ERR_MPI" << '\n';
    break;
  }
}
// Pastix errors:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Set the matrix in SPM format][Set the matrix in SPM format:1]]
void copy_and_set_matrix(const SparseMatrixCSC<Scalar, pastix_int_t>& A){
  const pastix_int_t * iptr = A.get_i_ptr();
  const pastix_int_t * jptr = A.get_j_ptr();
  const Scalar * vptr = A.get_v_ptr();
  _spm->fmttype = SpmCSC;
  _spm->rowptr = (pastix_int_t*) malloc(sizeof(pastix_int_t) * _spm->nnz);
  _spm->colptr = (pastix_int_t*) malloc(sizeof(pastix_int_t) * (_spm->n + 1));
  Scalar * values = (Scalar*) malloc(sizeof(Scalar) * _spm->nnz);
  for(int k = 0; k < _spm->n + 1; ++k) _spm->colptr[k] = jptr[k];
  for(int k = 0; k < _spm->nnz; ++k){
    _spm->rowptr[k] = iptr[k];
    values[k] = vptr[k];
  }
  _spm->values = (void *) values;
}

void reset_spm(){
  if(_spm != nullptr){
    spmExit( _spm );
    free(_spm);
  }
  _spm = (spmatrix_t *) malloc(sizeof( spmatrix_t ));
  spmInitDist(_spm, MPI_COMM_SELF);
  _spm->flttype = _scalartype;
}

void set_matrix(const Matrix& A){
  _is_facto = false;
  if(_is_setup){
    reset_spm();
  }

  if(_iparm[IPARM_THREAD_NBR] == 1){
    pastixInit(&_pastix_data, MPI_COMM_SELF, &_iparm[0], &_dparm[0]);
  }
  else{
    int * btab_ptr = (_bindtab.size() > 0) ? _bindtab.data() : NULL;
    pastixInitWithAffinity(&_pastix_data, MPI_COMM_SELF, &_iparm[0], &_dparm[0], btab_ptr);
  }

  pastixRhsInit( &_xp );

  if constexpr(PASTIX_VERSION_MAJOR == 6 && PASTIX_VERSION_MINOR >= 1){
    _spm->baseval = 0;
  }
  _spm->layout = SpmColMajor;
  _spm->n = n_cols(A);
  _spm->nnz = n_nonzero(A);
  _spm->dof = 1;

  if constexpr(std::is_same<Matrix, SparseMatrixCSC<Scalar, pastix_int_t>>::value){
    copy_and_set_matrix(A);
  }
  if constexpr(std::is_same<Matrix, SparseMatrixCOO<Scalar, pastix_int_t>>::value){
    copy_and_set_matrix(A.to_csc());
  }
  else{
    copy_and_set_matrix(A.template to_coo<pastix_int_t>().to_csc());
  }

  // Symmetry and definite positiveness
  if( A.is_storage_full() ){
    _spm->mtxtype = SpmGeneral;
    _iparm[IPARM_FACTORIZATION] = PastixFactLU;
    if(A.is_symmetric()){
      MAPHYSPP_WARNING("Pastix: matrix given is symmetric but using full storage\nHalf storage must be used to take advantage of the symmetry.");
    }
  }
  else if ( A.is_hermitian() ){
    _spm->mtxtype = SpmHermitian;
    _iparm[IPARM_FACTORIZATION] = PastixFactLDLH;
  }
  else { // A is symmetric
    _spm->mtxtype = SpmSymmetric;
    _iparm[IPARM_FACTORIZATION] = PastixFactLDLT;
  }

  if( A.is_spd() ) {
    _iparm[IPARM_FACTORIZATION] = PastixFactLLT;
  }

  spmUpdateComputedFields(_spm);

  if(_iparm[IPARM_VERBOSE] == PastixVerboseYes){
    spmPrintInfo( _spm, stdout );
  }
  spmatrix_t spm2;
  int correction = spmCheckAndCorrect( _spm, &spm2 );
  // Returns csc matrix (lower storage if symmetric)

  if (correction != 0){
    spmExit( _spm );
    (*_spm) = spm2;
  }

  _x = Vector();
  _is_setup = true;
}
// Set the matrix in SPM format:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Helper functions][Helper functions:1]]
inline void * _ptr(Vector& v) const { return (void *) const_cast<Scalar *>(get_ptr(v)); }
inline void * _ptr(const Vector& v) const { return (void *) const_cast<Scalar *>(get_ptr(v)); }
// Helper functions:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Constructors][Constructors:1]]
public:

  Pastix(){
    _iparm = std::vector<pastix_int_t>(IPARM_SIZE, 0);
    _dparm = std::vector<double>(DPARM_SIZE, 0);
    pastixInitParam( &_iparm[0], &_dparm[0] );

    _iparm[IPARM_THREAD_NBR] = 1;
    _iparm[IPARM_VERBOSE] = PastixVerboseNot;

    // Good old malloc for C compatibility
    _spm = (spmatrix_t *) malloc(sizeof( spmatrix_t ));
    spmInitDist(_spm, MPI_COMM_SELF);

    _spm->flttype = _scalartype;
  }

  Pastix(const Matrix& A): Pastix()
  {
    set_matrix(A);
  }

  ~Pastix(){
    if(_spm != nullptr){
      spmExit( _spm );
      free(_spm);
    }
    if(_xp != NULL){
      pastixRhsFinalize( _xp );
    }
    if(_pastix_data != nullptr){
      pastixFinalize( &_pastix_data );
    }
  }

  // Delete copy (because of raw pointers)
  Pastix(const Pastix&) = delete;
  Pastix& operator=(const Pastix&) = delete;

  // Allow move
private:
  inline void move_pastix(Pastix&& other){
    _pastix_data = std::exchange(other._pastix_data, nullptr);
    _spm = std::exchange(other._spm, nullptr);
    _nrhs = other._nrhs;
    _iparm = std::move(other._iparm);
    _dparm = std::move(other._dparm);
    _n_schur = std::exchange(other._n_schur, -1);
    _x = std::move(_x);
    _is_setup = std::exchange(other._is_setup, false);
    _is_facto = std::exchange(other._is_facto, false);
  }

public:
  Pastix(Pastix&& other){ move_pastix(std::move(other)); }

  Pastix& operator=(Pastix&& other){
    move_pastix(std::move(other));
    return *this;
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Setup function][Setup function:1]]
void setup(const Matrix& A){
  set_matrix(A);
}
// Setup function:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Factorization][Factorization:1]]
void factorize(){
  MAPHYSPP_ASSERT( _is_setup, "Trying to call factorize before setup !");
  // Perform analysis and factorization

  {
    Timer<TIMER_SOLVER> t_f("Pastix analysis");
    catch_pastix_error( pastix_task_analyze( _pastix_data, _spm ) );
  }{
    Timer<TIMER_SOLVER> t_f("Pastix facto");
    catch_pastix_error( pastix_task_numfact( _pastix_data, _spm ) );
  }
  _is_facto = true;
}
// Factorization:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Solve function][Solve function:1]]
// X and B must have the same values
void apply(const Vector& B, Vector& X) {
  if(!_is_facto){
    factorize();
  }
  Timer<TIMER_SOLVER> t("Pastix solve");

  MAPHYSPP_ASSERT(_spm->n == static_cast<pastix_int_t>(n_rows(B)), "RHS is not the same size as A matrix" );
  _nrhs = static_cast<pastix_int_t>(n_cols(B));

  X = B; // Copy the values

  catch_pastix_error( pastix_task_solve( _pastix_data, _spm->n, _nrhs, _ptr(X), _spm->n ) );

  if(_use_refinement){
    catch_pastix_error( pastix_task_refine( _pastix_data, _spm->n, _nrhs, _ptr(B), _spm->n, _ptr(X), _spm->n ) );
  }

  // NaN checker
  if(_spm->n > 0 && !is_normal_or_zero(get_ptr(X)[0])){
    // Check if right-hand side is not null (Pastix doesn't like it)
    if(B.norm() == 0){
      X = B;
    }
    else{
      MAPHYSPP_WARNING("Pastix solution returned with a NaN value. You can try to turn off iterative refinement (use_refinement(false) method)");
    }
  }
}

void solve(const Vector& B, Vector& X) {
  X = B;
  apply(B, X);
}

Vector apply(const Vector& B){
  Vector X = B; // Create X same size as B
  apply(B, X);
  return X;
}

Vector solve(const Vector& B) { return apply(B); }
// Solve function:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Triangular solve][Triangular solve:1]]
void triangular_solve(const Vector& B, Vector& X, MatrixStorage uplo, bool transposed = false){
  if(!_is_facto){
   factorize();
  }

  Timer<TIMER_SOLVER> t("Pastix triangular solve");
  pastix_uplo_t p_uplo = (uplo == MatrixStorage::lower) ? PastixLower : PastixUpper;
  pastix_trans_t p_trans = PastixNoTrans;
  if(transposed){
    if(is_complex<Scalar>::value) { p_trans = PastixConjTrans; }
    else{ p_trans = PastixTrans; }
  }
  pastix_diag_t diag = PastixNonUnit;
  if(_iparm[IPARM_FACTORIZATION] == PastixFactLU && uplo == MatrixStorage::lower) {
    diag = PastixUnit;
  }
  const pastix_int_t nrhs = static_cast<pastix_int_t>(n_cols(B));

  X = B;
  const pastix_int_t ldb = static_cast<pastix_int_t>(get_leading_dim(X));

  catch_pastix_error(pastix_subtask_applyorder(_pastix_data, PastixDirForward,
					       _spm->n, nrhs, _ptr(X), ldb, _xp));

  catch_pastix_error(pastix_subtask_trsm(_pastix_data,
					 PastixLeft, p_uplo, p_trans, diag,
					 _xp));

  catch_pastix_error( pastix_subtask_applyorder(_pastix_data, PastixDirBackward,
						_spm->n, nrhs, _ptr(X), ldb, _xp));

}

Vector triangular_solve(const Vector& B, MatrixStorage uplo, bool transposed = false){
  Vector X = B; // Create X same size as B
  triangular_solve(B, X, uplo, transposed);
  return X;
}
// Triangular solve:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*SPM info][SPM info:1]]
void spm_info() const {
  spmPrintInfo( _spm, stdout );
}

void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << "Pastix solver name: " << name << '\n';
  (void) out;
  spm_info();
}
// SPM info:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*SPM save][SPM save:1]]
void spm_save(const char * filename) const {
  spmSave(_spm, filename);
}
// SPM save:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Get Schur complement][Get Schur complement:1]]
public:
  // Compute the schur complement
  // NB: the storage type is the same as for matrix A
  template<MPH_Integral Index>
  IndexArray<Scalar> get_schur_as_array(const IndexArray<Index>& schurlist_in){

    MAPHYSPP_ASSERT( _is_setup, "Trying to compute schur complement before setup !");
    _n_schur = static_cast<pastix_int_t>(schurlist_in.size());

    if(_n_schur == 0) return IndexArray<Scalar>();

    // Make a copy if the integer format is not the same
    IndexArray<pastix_int_t> * schurlist_ptr;
    IndexArray<pastix_int_t> cpy_schurlist;
    if constexpr(std::is_same<Index, pastix_int_t>::value){
      schurlist_ptr = const_cast<IndexArray<pastix_int_t> *>(&schurlist_in);
    }
    else{
      cpy_schurlist = IndexArray<pastix_int_t>(_n_schur);
      for(pastix_int_t k = 0; k < _n_schur; ++k) cpy_schurlist[k] = static_cast<pastix_int_t>(schurlist_in[k]);
      schurlist_ptr = &cpy_schurlist;
    }
    IndexArray<pastix_int_t>& schurlist = *schurlist_ptr;

    // Checking boundaries
    for(pastix_int_t k = 0; k < _n_schur; ++k){
      MAPHYSPP_ASSERT_POSITIVE(schurlist[k], "Schurlist given with a negative value");
      MAPHYSPP_ASSERT(schurlist[k] < _spm->n, "Schurlist given with a value >= matrix order");
    }

    _iparm[IPARM_SCHUR_FACT_MODE] = PastixFactModeLocal;
    _iparm[IPARM_SCHUR_SOLV_MODE] = PastixSolvModeInterface;

    pastixSetSchurUnknownList(_pastix_data, _n_schur, schurlist.data());

    // Now perform analysis and factorization
    this->factorize();

    IndexArray<Scalar> schur(_n_schur * _n_schur);
    pastix_int_t ld_schur = _n_schur;

    catch_pastix_error( pastixGetSchur( _pastix_data, (void *) &schur[0], ld_schur) );

    // Deal with symmetry?
    // Full storage for now
    // NB: Pastix returns Column Major dense Schur
    if(_iparm[IPARM_FACTORIZATION] != PastixFactLU){
      for(pastix_int_t i = 0; i < _n_schur; ++i){
	for(pastix_int_t j = i+1; j < _n_schur; ++j){
	  schur[j * _n_schur + i] = conj<Scalar>(schur[i * _n_schur + j]);
	}
      }
    }

    return schur;
  }
// Get Schur complement:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Get Schur complement][Get Schur complement:2]]
template<MPH_Integral Index>
decltype(auto) get_schur(const IndexArray<Index>& schurlist){
  using DenseMat = typename dense_type<Matrix>::type;
  int n_schur = static_cast<int>(schurlist.size());
  DenseMat schur_mat;
  auto schur = this->get_schur_as_array(schurlist);
  build_dense_matrix(schur_mat, n_schur, n_schur, &schur[0]);
  if(_iparm[IPARM_FACTORIZATION] != PastixFactLU){
    schur_mat.set_property(MatrixSymmetry::symmetric);
  }
  return schur_mat;
}
// Get Schur complement:2 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*b2f][b2f:1]]
// Compute f = Bg - Agi.Aii^-1.Bi
// Assume 1 rhs at the moment
Vector b2f(const Vector& B){
  MAPHYSPP_ASSERT( _is_setup, "Trying to compute b2f before setup !");
  MAPHYSPP_ASSERT( _n_schur >= 0, "Calling b2f before get_schur !");
  Timer<TIMER_SOLVER> t("Pastix b2f");

  const pastix_int_t nrhs = 1;
  _x = B;
  const pastix_int_t ldb = n_rows(_x);

  catch_pastix_error( pastix_subtask_applyorder( _pastix_data, PastixDirForward,
						 _spm->n, nrhs, _ptr(_x), ldb, _xp ) );

  pastix_diag_t diag = PastixNonUnit;
  if(_iparm[IPARM_FACTORIZATION] == PastixFactLU) {
    diag = PastixUnit;
  }
  catch_pastix_error( pastix_subtask_trsm( _pastix_data,
					   PastixLeft, PastixLower, PastixNoTrans, diag,
					   _xp ));

  // Copy the last _n_schur entries of x
  Vector f(static_cast<int>(_n_schur));
  const pastix_int_t x_size = n_rows(_x);
  int i = 0;
  for(int k = x_size - _n_schur; k < x_size; ++k){
    f(i++, 0) = _x(k, 0);
    //f[i++] = _x[k];
  }
  //pastixRhsSchurGet( _pastix_data, _n_schur, nrhs, _xp, _ptr(f), _n_schur );

  return f;
}
// b2f:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*y2x][y2x:1]]
// Compute x_i = Kii^-1( Bi - Kig.x_g)
// And return x = (x_i x_g)^T
Vector y2x(const Vector& x_g){
  MAPHYSPP_ASSERT( n_rows(_x) >= n_rows(x_g) , "Wrong size for x. Calling y2x before b2f?");
  Timer<TIMER_SOLVER> t("Pastix y2x");

  const pastix_int_t nrhs = 1;
  Vector xo =_x; // Copy original vector
  const pastix_int_t x_size = n_rows(_x);
  const pastix_int_t ldx = x_size;

  int i = 0;
  for(int k = x_size - _n_schur; k < x_size; ++k){
    _x(k, 0) = x_g(i++, 0);
    //x[k] = x_g[i++];
  }
  //pastixRhsSchurSet( _pastix_data, _n_schur, nrhs, _ptr(x_g), _n_schur, _xp );

  // LU by default
  pastix_uplo_t uplo = PastixUpper;
  pastix_trans_t trans = PastixNoTrans;
  pastix_diag_t diag = PastixNonUnit;
  // LDLT
  if( _iparm[IPARM_FACTORIZATION] == PastixFactLDLT ) {
    uplo = PastixLower;
    trans = PastixTrans;
    diag = PastixUnit;
  }
  //LLT
  else if( _iparm[IPARM_FACTORIZATION] == PastixFactLLT ) {
    uplo = PastixLower;
    trans = PastixTrans;
  }

  catch_pastix_error( pastix_subtask_trsm( _pastix_data,
					   PastixLeft, uplo, trans, diag,
					   _xp ));

  catch_pastix_error( pastix_subtask_applyorder( _pastix_data, PastixDirBackward,
						 _spm->n, nrhs, _ptr(_x), ldx, _xp ) );

  Vector x = _x; // Copy result into the user's vector
  _x = xo; // Copy back original vector
  return x;
}
// y2x:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Set number of threads][Set number of threads:1]]
template<MPH_Integral Tint>
void set_n_threads(Tint n_threads){
  _iparm[IPARM_THREAD_NBR] = static_cast<pastix_int_t>(n_threads);

  if(_is_setup){
    MAPHYSPP_WARNING("Pastix::set_n_threads() resetting some values. It is better to call it before setup");
    pastixRhsFinalize( _xp );
    pastixFinalize(&_pastix_data);
    int * btab_ptr = (_bindtab.size() > 0) ? _bindtab.data() : NULL;
    pastixInitWithAffinity(&_pastix_data, MPI_COMM_SELF, &_iparm[0], &_dparm[0], btab_ptr);
    pastixRhsInit( &_xp );
  }
}

void set_n_threads(const std::vector<int> bindtab){
  pastix_int_t n_threads = static_cast<pastix_int_t>(bindtab.size());
  _bindtab = bindtab;
  set_n_threads(n_threads);
}
// Set number of threads:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Low-rank options][Low-rank options:1]]
void set_low_rank(double lr_tolerance, pastix_int_t min_width = 128, pastix_int_t min_height = 25){
  _iparm[IPARM_COMPRESS_WHEN]       = PastixCompressWhenEnd;
  _iparm[IPARM_COMPRESS_METHOD]     = PastixCompressMethodPQRCP;
  _iparm[IPARM_COMPRESS_MIN_WIDTH]  = min_width;
  _iparm[IPARM_COMPRESS_MIN_HEIGHT] = min_height;
  _dparm[DPARM_COMPRESS_TOLERANCE]  = lr_tolerance;
}

void set_low_rank_jit(double lr_tolerance, pastix_int_t min_width = 128, pastix_int_t min_height = 25){
  set_low_rank(lr_tolerance, min_width, min_height);
}

void set_low_rank_minmem(double lr_tolerance, pastix_int_t min_width = 128, pastix_int_t min_height = 25){
  set_low_rank(lr_tolerance, min_width, min_height);
  _iparm[IPARM_COMPRESS_WHEN] = PastixCompressWhenBegin;
}
// Low-rank options:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Setters][Setters:1]]
// Some setters
void set_verbose(int verbose){ // 0, 1 or 2
  switch (verbose){
  case 0:
    _iparm[IPARM_VERBOSE] = PastixVerboseNot;
    break;
  case 1:
    _iparm[IPARM_VERBOSE] = PastixVerboseNo;
    break;
  case 2:
    _iparm[IPARM_VERBOSE] = PastixVerboseYes;
    break;
  default:
    MAPHYSPP_WARNING("Pastix.set_verbose(i): i must be 0, 1 or 2");
    _iparm[IPARM_VERBOSE] = PastixVerboseNo;
    break;
  }
}

void use_refinement(bool ur){ _use_refinement = ur; }

}; // class Pastix
// Setters:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Traits][Traits:1]]
// Set traits
template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct is_solver_direct<Pastix<Matrix, Vector>> : public std::true_type {};
template<typename Matrix, typename Vector>
struct is_solver_iterative<Pastix<Matrix, Vector>> : public std::false_type {};
template<typename Matrix, typename Vector>
struct is_matrix_free<Pastix<Matrix, Vector>> : public std::false_type {};

template<typename Matrix, typename Vector>
struct has_triangular_solve<Pastix<Matrix, Vector>> : public std::true_type {};

template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct matrix_type<Pastix<Matrix, Vector>> : public std::true_type {
  using type = typename Pastix<Matrix, Vector>::matrix_type;
};

template<typename Matrix, typename Vector>
struct vector_type<Pastix<Matrix, Vector>> : public std::true_type {
  using type = typename Pastix<Matrix, Vector>::vector_type;
};

template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct scalar_type<Pastix<Matrix, Vector>> : public std::true_type {
  using type = typename Pastix<Matrix, Vector>::scalar_type;
};
// Traits:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
