// [[file:../../../org/maphys/solver/TlapackSolver.org::*Header][Header:1]]
#pragma once

#include "maphys/interfaces/linalg_concepts.hpp"
namespace maphys {
  template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector> class TlapackSolver;
}

#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/solver/LinearOperator.hpp"
#include "maphys/kernel/TlapackKernels.hpp"
#include <tlapack/lapack/geqr2.hpp>
#include <tlapack/plugins/legacyArray.hpp>

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Class][Class:1]]
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
class TlapackSolver :
  public LinearOperator<Matrix, Vector> {
// Class:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Attributes][Attributes:1]]
private:
using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
using Real = typename LinearOperator<Matrix, Vector>::real_type;
using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar, std::complex<Real>>::type;

enum class FactoType { LU, LDLT, LLT, QR, LQ };

public:
using matrix_type = Matrix;
using vector_type = Vector;
using scalar_type = Scalar;

private:
Matrix _A;
FactoType _facto_done;
tlapack::Uplo _facto_uplo;
bool _is_setup = false;
bool _is_facto = false;
std::vector<int> _piv;
std::vector<Scalar> _tau;
std::vector<Scalar> _Q;
std::vector<Scalar> _RorL;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Constructors][Constructors:1]]
public:
TlapackSolver(){}
TlapackSolver(const Matrix& A){
  _A = A;
  _is_setup = true;
}
TlapackSolver(Matrix&& A){
  _A = std::move(A);
  _is_setup = true;
}
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Move operations][Move operations:1]]
public:
TlapackSolver(const TlapackSolver&) = delete;
TlapackSolver& operator=(const TlapackSolver&) = delete;

TlapackSolver(TlapackSolver&&) = default;
TlapackSolver& operator=(TlapackSolver&&) = default;
// Move operations:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Setup][Setup:1]]
void setup(const Matrix& A){
  _A = A;
  _is_setup = true;
  _is_facto = false;
}
void setup(Matrix&& A){
  _A = std::move(A);
  _is_setup = true;
  _is_facto = false;
}
// Setup:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Factorization][Factorization:1]]
private:
void _fill_symmetry(){
  if(_A.is_storage_upper()){
    for(size_t j = 0; j < tlapack::ncols(_A); ++j){
      for(size_t i = j + 1; i < tlapack::nrows(_A); ++i){
	_A(i, j) = _A(i, j);
      }
    }
  } else if(_A.is_storage_lower()){
    for(size_t j = 0; j < tlapack::ncols(_A); ++j){
      for(size_t i = 0; i < j; ++i){
	_A(i, j) = _A(i, j);
      }
    }
  }
  _A.set_property(MatrixStorage::full);
}

public:
void factorize(){
  MAPHYSPP_ASSERT(_is_setup == true, "TlapackSolver::factorize: calling factorize before setup(A)");
  Timer<TIMER_SOLVER> t("Tlapack dense facto");

  Size M = n_rows(_A);
  Size N = n_cols(_A);
  Size K = std::min(M, N);

  if(M == 0 || N == 0) return;

  int err = 0;

  if(M > N){ // Rectangular -> least squares -> QR
    _tau = std::vector<Scalar>(K);
    std::span<Scalar> sptau(&_tau[0], K);

    tlapack::Create<tlapack::LegacyMatrix<Scalar>> new_matrix;
    auto Q = new_matrix(_Q, M , N);
    tlapack::lacpy(tlapack::GENERAL, _A, Q);
    auto R = new_matrix(_RorL, N , N);

    err = tlapack::geqr2(Q, sptau);

    // Save the R matrix
    tlapack::lacpy(tlapack::UPPER_TRIANGLE, Q, R);

    // Generates Q = H_1 H_2 ... H_n
    tlapack::ung2r(Q, sptau);

    //err = tlapack::geqrf<Matrix, std::span<Scalar>>(_A, sptau);

    _facto_done = FactoType::QR;
  }

  else if(M < N){ // Rectangular -> minimum norm -> LQ
    _tau = std::vector<Scalar>(K);
    std::span<Scalar> sptau(&_tau[0], K);

    tlapack::Create<tlapack::LegacyMatrix<Scalar>> new_matrix;
    auto Q = new_matrix(_Q, M , N);
    tlapack::lacpy(tlapack::GENERAL, _A, Q);
    auto L = new_matrix(_RorL, M , M);

    err = tlapack::gelq2(Q, sptau);

    // Save the R matrix
    tlapack::lacpy(tlapack::LOWER_TRIANGLE, Q, L);

    // Generates Q = H_1 H_2 ... H_n
    tlapack::ungl2(Q, sptau);

    //err = tlapack::gelqf<Matrix, std::span<Scalar>> (_A, sptau);
    _facto_done = FactoType::LQ;
  }

  else if(_A.is_general()){ // Square general -> LU
    _piv = std::vector<int>(K);
    std::span<int> sppiv(&_piv[0], K);
    err = tlapack::getrf<Matrix, std::span<int>> (_A, sppiv);
    _facto_done = FactoType::LU;
  }

  else{
    _facto_uplo = _A.is_storage_upper() ? tlapack::Uplo::Upper : tlapack::Uplo::Lower;

    if(_A.is_spd_or_hpd()){ // Square SPD -> LLT
      err = tlapack::potrf(_facto_uplo, _A);
      _facto_done = FactoType::LLT;
    }

    else{ // Square symmetric -> LDLT
      // No function sytrf :( -> go back to LU
      _fill_symmetry();
      _piv = std::vector<int>(K);
      std::span<int> sppiv(&_piv[0], K);
      err = tlapack::getrf<Matrix, std::span<int>> (_A, sppiv);
      _facto_done = FactoType::LU;
      //_facto_done = FactoType::LDLT;
    }
  }

  MAPHYSPP_ASSERT(err == 0, "TlapackSolver::factorize return with err != 0");
  _is_facto = true;
}
// Factorization:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Triangular solve operations][Triangular solve operations:1]]
void apply_facto_permutation(Vector& X) const {
  MAPHYSPP_ASSERT(_is_facto, "TlapackSolver::apply_facto_permutation: applying permuation before factorization");
  // Permutation (should be laswp routine)
  if(!_piv.empty()){
    Timer<TIMER_SOLVER> t("tlapack permutation application");
    for(int k = 0; k < static_cast<int>(maphys::n_rows(X)); ++k){ std::swap(X[k], X[_piv[k]]); }
  }
}

[[nodiscard]] Vector apply_facto_permutation(const Vector& B) const {
  Vector X = B;
  apply_facto_permutation(X);
  return X;
}

private:
// Triangular solve where X is RHS on input and solution in output
void _triangular_solve(Vector& X, tlapack::Uplo uplo, tlapack::Diag diag, tlapack::Op trans) const {
  MAPHYSPP_ASSERT(_is_facto, "TlapackSolver::triangular_solve: applying triangular solve before factorization");
  Timer<TIMER_SOLVER> t("tlapack Dense triangular solve");

  if(maphys::n_cols(_A) == 1){
    tlapack::trsv(uplo, trans, diag, _A, X);
  } else {
    tlapack::trsm(tlapack::Side::Left, uplo, trans, diag, Scalar{1}, _A, X);
  }
}

public:
void triangular_solve(Vector& X, MatrixStorage uplo, bool transposed = false) const {
  tlapack::Uplo tuplo = (uplo == MatrixStorage::upper) ? tlapack::Uplo::Upper   : tlapack::Uplo::Lower;
  tlapack::Diag tdiag = (uplo == MatrixStorage::upper) ? tlapack::Diag::NonUnit : tlapack::Diag::Unit;
  tlapack::Op   top   = transposed ? tlapack::Op::Trans : tlapack::Op::NoTrans;
  _triangular_solve(X, tuplo, tdiag, top);
}

void triangular_solve(const Vector& B, Vector& X, MatrixStorage uplo, bool transposed = false) const {
  X = B;
  triangular_solve(X, uplo, transposed);
}

[[nodiscard]] Vector triangular_solve(const Vector& B, MatrixStorage uplo, bool transposed = false) const {
  Vector X = B;
  triangular_solve(X, uplo, transposed);
  return X;
}
// Triangular solve operations:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Solve][Solve:1]]
void apply(const Vector& B, Vector& X){
  if(!_is_facto){
    factorize();
  }
  Timer<TIMER_SOLVER> t("Tlapack dense solve");

  switch(_facto_done){
  case FactoType::LU:
    X = B;
    apply_facto_permutation(X);
    _triangular_solve(X, tlapack::Uplo::Lower, tlapack::Diag::Unit, tlapack::Op::NoTrans);
    _triangular_solve(X, tlapack::Uplo::Upper, tlapack::Diag::NonUnit, tlapack::Op::NoTrans);
    break;

  case FactoType::LLT:
    X = B;
    tlapack::potrs(_facto_uplo, _A, X);
    break;

  case FactoType::LDLT:
    break;

  case FactoType::QR:
    {
      Size M = n_rows(_A);
      Size N = n_cols(_A);

      // Get Q and R back
      tlapack::Create<tlapack::LegacyMatrix<Scalar>> new_matrix;
      auto Q = new_matrix(_Q, M, N);
      auto R = new_matrix(_RorL, N, N);
      if(size(X) != N) X = Vector(N);

      // y = Q*b
      tlapack::gemv(tlapack::Op::ConjTrans, Scalar{1}, Q, B, Scalar{0}, X);
      // Rx = y
      tlapack::trsv(tlapack::Uplo::Upper, tlapack::Op::NoTrans, tlapack::Diag::NonUnit, R, X);
    }
    break;

  case FactoType::LQ:
    {
      Size M = n_rows(_A);
      Size N = n_cols(_A);

      // Get Q and R back
      tlapack::Create<tlapack::LegacyMatrix<Scalar>> new_matrix;
      auto Q = new_matrix(_Q, M, N);
      auto L = new_matrix(_RorL, M, M);
      if(size(X) != N) X = Vector(N);

      // Ly = b
      Vector Y = B;
      tlapack::trsv(tlapack::Uplo::Lower, tlapack::Op::NoTrans, tlapack::Diag::NonUnit, L, Y);

      // x = Q*y
      tlapack::gemv(tlapack::Op::ConjTrans, Scalar{1}, Q, Y, Scalar{0}, X);
    }
    break;
  }
}

void solve(const Vector& B, Vector& X) {
  X = B;
  apply(B, X);
}

Vector apply(const Vector& B){
  Vector X = B; // Create X same size as B
  apply(B, X);
  return X;
}

Vector solve(const Vector& B) { return apply(B); }
// Solve:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Traits][Traits:1]]
}; //class TlapackSolver
// Traits:1 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Traits][Traits:2]]
// Set traits
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_solver_direct<TlapackSolver<Matrix, Vector>> : public std::true_type {};
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_solver_iterative<TlapackSolver<Matrix, Vector>> : public std::false_type {};
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_matrix_free<TlapackSolver<Matrix, Vector>> : public std::false_type {};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct has_triangular_solve<TlapackSolver<Matrix, Vector>> : public std::true_type {};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct vector_type<TlapackSolver<Matrix, Vector>> : public std::true_type {
  using type = typename TlapackSolver<Matrix, Vector>::vector_type;
};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct scalar_type<TlapackSolver<Matrix, Vector>> : public std::true_type {
  using type = typename TlapackSolver<Matrix, Vector>::scalar_type;
};
// Traits:2 ends here

// [[file:../../../org/maphys/solver/TlapackSolver.org::*Footer][Footer:1]]
} //namespace maphys
// Footer:1 ends here
