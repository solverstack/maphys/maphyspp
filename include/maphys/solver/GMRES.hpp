// [[file:../../../org/maphys/solver/GMRES.org::*Header][Header:1]]
#pragma once

#include <lapack.hh>
#include <map>
#include <limits>

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/solver/IterativeSolver.hpp"
#include "maphys/kernel/BlasKernels.hpp"
#ifdef MAPHYSPP_USE_SZ_COMPRESSOR
#include "maphys/utils/SZ_compressor.hpp"
#include "maphys/loc_data/CompressedBasis.hpp"
#endif
#ifndef MAPHYSPP_NO_MPI
#include "maphys/part_data/PartOperator.hpp"
#endif

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Base class][Base class:1]]
template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond = Identity<Matrix, Vector>>
class GMRES :
    public IterativeSolver<Matrix, Vector, Precond> {
// Base class:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Attributes][Attributes:1]]
private:
using Scalar = typename IterativeSolver<Matrix, Vector, Precond>::scalar_type;
using Real = typename IterativeSolver<Matrix, Vector, Precond>::real_type;

using IterativeSolver<Matrix, Vector, Precond>::_A;
using IterativeSolver<Matrix, Vector, Precond>::_B;
using IterativeSolver<Matrix, Vector, Precond>::_X;
using IterativeSolver<Matrix, Vector, Precond>::_R;
using IterativeSolver<Matrix, Vector, Precond>::_M;
//using IterativeSolver<Matrix, Vector, Precond>::_dot;
using IterativeSolver<Matrix, Vector, Precond>::_squared;
using IterativeSolver<Matrix, Vector, Precond>::_max_iter;
using IterativeSolver<Matrix, Vector, Precond>::_tolerance;
using IterativeSolver<Matrix, Vector, Precond>::_tol_sq;
using IterativeSolver<Matrix, Vector, Precond>::_check_true_residual;
using IterativeSolver<Matrix, Vector, Precond>::_always_true_residual;
using IterativeSolver<Matrix, Vector, Precond>::_residual_sq;
using IterativeSolver<Matrix, Vector, Precond>::_verbose;
using IterativeSolver<Matrix, Vector, Precond>::_custom_convergence_check;
using IterativeSolver<Matrix, Vector, Precond>::_stop_crit_denom_inv;

public:
using scalar_type = Scalar;
using vector_type = Vector;
using matrix_type = Matrix;
using real_type = Real;

private:
using LocWorkMat = DenseMatrix<Scalar>;
using LocVect = DenseMatrix<Scalar, 1>;

#ifndef MAPHYSPP_NO_MPI
using WorkMatrix = std::conditional<is_distributed<Matrix>::value, PartMatrix<DenseMatrix<Scalar>>, DenseMatrix<Scalar>>::type;
using WorkVector = std::conditional<is_distributed<Vector>::value, PartVector<DenseMatrix<Scalar, 1>>, DenseMatrix<Scalar, 1>>::type;
#else
using WorkMatrix = LocWorkMat;
using WorkVector = LocVect;
#endif // MAPHYSPP_NO_MPI

int _k = 0; // Iteration index
Vector _X0_ext;
WorkVector _Z;
WorkVector _W;
Scalar _beta = Scalar{0};
WorkMatrix _V;
LocWorkMat _H; // Replicated Hessenberg matrix
LocVect _Y; // Recplicated least squares solution vector
Real _R_lsq_sq; // Least square residual squared
bool _solution_up_to_date = false;

Ortho _ortho = Ortho::MGS;
int _restart = 100;

#ifdef MAPHYSPP_USE_SZ_COMPRESSOR

#ifndef MAPHYSPP_NO_MPI
using ComprBase = typename std::conditional<is_distributed<Matrix>::value, PartMatrix<CompressedBasis<Scalar>>, CompressedBasis<Scalar>>::type;
#else
using ComprBase = CompressedBasis<Scalar>;
#endif

ComprBase _Vtilde;
bool _use_compression = false;
double _zeta = -1.0;
#else

WorkMatrix _Vtilde;
static constexpr const bool _use_compression = false;
#endif

bool _verbose_mem = false;
bool _verbose_ortho = false;
long int _mem_V_th = 0;
long int _mem_V_stored = 0;

template<typename Vec>
Scalar _norm2(const Vec &v) const {
  Real vv = std::real(dot(v, v));
  return static_cast<Scalar>(std::sqrt(vv));
}
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Compute loss of orthogonality][Compute loss of orthogonality:1]]
Real _loss_ortho(bool use_svd = false){
  if(_k == 0) return Real{0};

  LocWorkMat VtKVk(_k, _k);
  for(int i = 0; i < _k; ++i) VtKVk(i, i) = Scalar{1};

  if(_use_compression){
    for(int i = 0; i < _k; ++i){
      auto Vi = get_vect_view(_Vtilde, i);
      for(int j = 0; j < _k; ++j){
	auto Vj = get_vect_view(_Vtilde, j);
	VtKVk(i, j) -= dot(Vi, Vj);
      }
    }
  }
  else{
    for(int i = 0; i < _k; ++i){
      auto Vi = get_vect_view(_V, i);
      for(int j = 0; j < _k; ++j){
	auto Vj = get_vect_view(_V, j);
	VtKVk(i, j) -= dot(Vi, Vj);
      }
    }
  }

  if(!use_svd) return VtKVk.norm();

  auto sing_vals = blas_kernels::svdvals(VtKVk);
  return sing_vals[0];
}
// Compute loss of orthogonality:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Handle compression][Handle compression:1]]
WorkVector get_V_vector(const int j){
    if(_use_compression){
      return get_vect_view(_Vtilde, j);
    }
    return get_vect_view(_V, j);
  }

  void set_V_vector_no_compression(const int j, WorkVector&& v_in){
    auto V_j = get_V_vector(j);
    V_j = std::move(v_in);
  }

#ifdef MAPHYSPP_USE_SZ_COMPRESSOR
  void set_V_vector_compression(const int j, WorkVector&& v_in){

    auto set_vect = [this, j](CompressedBasis<Scalar>& v_cpr, LocVect& v_full){
      v_cpr.set_vector(v_full, j);
      if(_verbose_mem) _mem_V_stored += v_cpr.get_compressed_size(j);
    };

    if constexpr(is_distributed<Matrix>::value){
      _Vtilde.template apply_on_data<LocVect>(v_in, set_vect);
    }
    else{
      set_vect(_Vtilde, v_in);
    }

    v_in = Vector();
  }
#endif

  void set_V_vector(const int j, WorkVector&& v_in){
    if(_verbose_mem){
      if constexpr(is_distributed<Matrix>::value){
        for(const int sd_id : v_in.get_sd_ids()){ _mem_V_th += get_M(sd_id) * sizeof(Scalar); }
      }
      else{
        _mem_V_th += n_rows(v_in) * sizeof(Scalar);
      }
    }

 #ifdef MAPHYSPP_USE_SZ_COMPRESSOR
    if(_use_compression){
      set_V_vector_compression(j, std::move(v_in));
    }
 #endif
    set_V_vector_no_compression(j, std::move(v_in));
  }
// Handle compression:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Handle compression][Handle compression:2]]
Size get_M(int sd_id = 0){
    if constexpr(is_distributed<Vector>::value){
      return n_rows(_B->get_local_vector(sd_id));
    } else { (void) sd_id; }
    return n_rows(*_B);
  }

  void init_matrix(WorkMatrix& mat, int dim_1, int dim_2){
    auto initm = [&](const int sd_id, LocWorkMat& locm){
                             if(dim_1 == -1) dim_1 = get_M(sd_id);
                             locm = LocWorkMat(dim_1, dim_2);
                             if(_verbose_mem){ _mem_V_stored += dim_1 * dim_2 * sizeof(Scalar); }
                           };

    if constexpr(is_distributed<Vector>::value){
      mat.initialize(_B->get_proc());
      mat.set_on_intrf(_B->on_intrf());
      mat.apply_on_data_id(initm);
    }
    else{
      initm(0, mat);
    }
  }

#ifdef MAPHYSPP_USE_SZ_COMPRESSOR
  void init_compressed_base(){
    _mem_V_stored = 0;
    auto init_base = [this](CompressedBasis<Scalar>& cprv){
      cprv = CompressedBasis<Scalar>(_restart + 1, _zeta);
    };

    if constexpr(is_distributed<Vector>::value){
      _Vtilde.initialize(_B->get_proc());
      _Vtilde.apply_on_data(init_base);
    }
    else{
      init_base(_Vtilde);
    }
  }
#endif
// Handle compression:2 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Convergence checking][Convergence checking:1]]
void true_residual_sq(){
  update_solution();
  _R = (*_B) - (*_A) * (*_X);
  _residual_sq = _squared(_R)  * _stop_crit_denom_inv;
  if(_verbose) std::cout << "||B-AX||/||B||\t" << std::sqrt(_residual_sq) << '\n';
}

void approx_residual_sq(){
  _residual_sq = _R_lsq_sq * _stop_crit_denom_inv;
  if(_verbose) std::cout << "lsq_res/||B||\t" << std::sqrt(_residual_sq) << '\n';
}

bool convergence_achieved(){
  if(_custom_convergence_check){
    return _custom_convergence_check((*_A), (*_X), (*_B), _R, _tolerance);
  }
  if(_always_true_residual){
    // ||B - A X||^2 / || B ||^2 < tol^2
    true_residual_sq();
    return (_residual_sq < _tol_sq);
  }

  // ||R||^2 / || B ||^2 < tol^2
  approx_residual_sq();
  bool approx_res_converged = (_residual_sq < _tol_sq);
  if(approx_res_converged && _check_true_residual){
    if(_verbose) std::cout << '\t';
    true_residual_sq();
    return (_residual_sq < _tol_sq);
  }

  return approx_res_converged;
}
// Convergence checking:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Iterative algorithm][Iterative algorithm:1]]
int iterative_solve() override {
    Timer<TIMER_ITERATION> t0("GMRES iteration 0");
    const Matrix& A_ext = *_A;
    const Vector& B_ext = *_B;
    auto& M_ext = _M;
    Vector& X_ext = *_X;
    Vector& R_ext = _R;

    this->setup_initial_stop_crit("GMRES");

#ifdef MAPHYSPP_USE_SZ_COMPRESSOR
    _use_compression = (_zeta > 0);
#endif

    _restart = std::min(_restart, _max_iter);
    _Y = LocVect(_restart + 2);
    _H = LocWorkMat(_restart + 1, _restart);

    if(_use_compression){
#ifdef MAPHYSPP_USE_SZ_COMPRESSOR
      init_compressed_base();
#endif
    }
    else{
      init_matrix(_V, -1, _restart + 1);
    }

    _k = 0;
    _X0_ext = Vector(X_ext);
    R_ext = B_ext - A_ext * X_ext;
    _R_lsq_sq = std::numeric_limits<Real>::infinity();

    Vector Vk_ext(X_ext);
    Vector Z_ext(X_ext);
    Vector W_ext(X_ext);

    WorkVector Vk = WorkVector::view(Vk_ext);
    _Z = WorkVector::view(Z_ext);
    _W = WorkVector::view(W_ext);

    WorkVector X = WorkVector::view(X_ext);
    WorkVector R = WorkVector::view(R_ext);

    if(convergence_achieved()) return 0;

    _beta = _norm2(R);
    _W = R;
    _Z = R;

    t0.stop();

    for(int iter = 1; iter < _max_iter + 1; ++iter){
      Timer<TIMER_ITERATION> t("GMRES iteration");
      if(_verbose) std::cout << iter << " -\t";

      _solution_up_to_date = false;

      Timer<TIMER_ITERATION + 10> t_w("GMRES: get_W");
      // Restarting
      if(_k >= _restart){
	update_solution();
	_R_lsq_sq = std::numeric_limits<Real>::infinity();
	_k = 0;
	_X0_ext = Vector(X_ext);
	R_ext = B_ext - A_ext * X_ext;
	_beta = _norm2(R);
	_mem_V_th = 0;
	if(_use_compression){
  #ifdef MAPHYSPP_USE_SZ_COMPRESSOR
	init_compressed_base();
  #endif
	}
      }

      if(_k == 0){
	set_V_vector(0, R / _beta);
      }

      Vk = get_V_vector(_k);
      Z_ext = M_ext * Vk_ext;
      W_ext = A_ext * Z_ext;

      // Update views
      _Z .update_view(Z_ext);
      _W.update_view(W_ext);

      t_w.stop();

      orthogonalization();
      least_squares(_Y);

      _k++;

      if(_verbose_mem){
	if constexpr(is_distributed<Matrix>::value){
	#ifndef MAPHYSPP_NO_MPI
	  long int tmp1 = _mem_V_th;
	  long int tmp2;
	  MMPI::reduce(&tmp1, &tmp2, 1, MPI_SUM, 0, _B->get_proc()->master_comm());
	  double rho = static_cast<double>(tmp2);
	  tmp1 = static_cast<long int>(_mem_V_stored);
	  MMPI::reduce(&tmp1, &tmp2, 1, MPI_SUM, 0, _B->get_proc()->master_comm());
	  rho /= static_cast<double>(tmp2);
	  if(MMPI::rank() == 0) std::cout << "Compression ratio: " << rho << '\n';
	#endif
	}
	else{
	  double rho = static_cast<double>(_mem_V_th) / static_cast<double>(_mem_V_stored);
	  std::cout << "Compression ratio: " << rho << '\n';
	}
      }

      if(convergence_achieved()) return iter;
    }

    finalize();
    return -1;
  }
// Iterative algorithm:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Least squares][Least squares:1]]
void least_squares(LocVect& yy){
  Timer<TIMER_ITERATION + 10> t_lsq("GMRES: least_squares");

  if(_beta == Scalar{0}){ // Necessary, especially when distributed
    _R_lsq_sq = Real{0};
    return;
  }

  // y <- argmin ||beta e1 - H y||
  const Size M = static_cast<Size>(_k + 2);
  const Size N = static_cast<Size>(_k + 1);
  const LocVect beta_e1 = [M](Scalar beta){ LocVect u(M); u(0) = beta; return u; }(_beta);

#ifdef MAPHYSPP_USE_LAPACKPP // Lapackpp gels version

  yy = beta_e1;

  LocWorkMat Htmp(_H.get_block_copy(0, 0, M, N));

  // gels: y = argmin || b - A y ||_2
  lapack::gels(lapack::Op::NoTrans, static_cast<Blas_int>(M), //Op, M
	       static_cast<Blas_int>(N), 1, //N, NRHS
	       get_ptr(Htmp), //A (modified on exit)
	       get_leading_dim(Htmp), //lda
	       get_ptr(yy), //b (y on exit)
	       get_leading_dim(yy) // ldb
	       );

  //b: "the residual sum of squares for the solution in each column is
  //given by the sum of squares of modulus of elements n+1 to m in that
  //column"
  const Real yykp1 = std::abs(yy(_k+1));
  _R_lsq_sq = yykp1 * yykp1;

#else // generic version

  const LocWorkMat Hbar = _H.get_block_view(0, 0, M, N);
  LocVect Y = get_vect_view(yy, 0, 0, N);

  Y = Hbar % beta_e1;

  _R_lsq_sq = blas_kernels::norm2_squared(LocVect(beta_e1 - Hbar * Y));

#endif // MAPHYSPP_USE_LAPACKPP
}
// Least squares:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Orthogonalization][Orthogonalization:1]]
void orthogonalization(){
   Timer<TIMER_ITERATION + 10> t_o("GMRES: orthogonalization");

   // Orthogonalizations step
   switch(_ortho){
     case Ortho::MGS:
       for(int i = 0; i < _k + 1; ++i){
	 WorkVector Vi = get_V_vector(i);
	 _H(i, _k) = dot(Vi, _W);
	 _W -= _H(i, _k) * Vi;
       }
     break;

     case Ortho::MGS2:
       get_vect_view(_H, 0, _k, _k + 1) *= 0;
       for(int dum = 0; dum < 2; ++dum){
	 std::vector<Scalar> Hk(_k + 1);
	 for(int i = 0; i < _k + 1; ++i){
	   WorkVector Vi = get_V_vector(i);
	   Hk[i] = dot(Vi, _W);
	   _W -= Hk[i] * Vi;
	 }
	 for(int i = 0; i < _k + 1; ++i){
	   _H(i, _k) += Hk[i];
	 }
       }
     break;

     case Ortho::CGS:
       if(_use_compression || is_distributed<WorkMatrix>::value){
	 for(int i = 0; i < _k + 1; ++i){
	   WorkVector Vi = get_V_vector(i);
	   _H(i, _k) = dot(Vi, _W);
	 }
	 for(int i = 0; i < _k + 1; ++i){
	   WorkVector Vi = get_V_vector(i);
	   _W -= _H(i, _k) * Vi;
	 }
       }
       else{
	 auto Vk = get_columns_view(_V, 0, _k + 1);
	 auto Hk = get_vect_view(_H, 0, _k, _k + 1);

	 if constexpr(! is_distributed<WorkMatrix>::value){
	   gemv(Vk, _W, Hk, Scalar{1}, Scalar{0}, 'C'); // Hk <- Vk^* W
	   gemv(Vk, Hk, _W, Scalar{-1}, Scalar{1}, 'N'); // W <- W - Vk Hk
	 }
       }
     break;

     case Ortho::CGS2:
       if(_use_compression || is_distributed<WorkMatrix>::value){
	 get_vect_view(_H, 0, _k, _k + 1) *= 0;
	 for(int dum = 0; dum < 2; ++dum){
	   std::vector<Scalar> Hk(_k + 1);
	   for(int i = 0; i < _k + 1; ++i){
	     WorkVector Vi = get_V_vector(i);
	     Hk[i] = dot(Vi, _W);
	   }
	   for(int i = 0; i < _k + 1; ++i){
	     auto Vi = get_V_vector(i);
	     _W -= Hk[i] * Vi;
	   }
	   for(int i = 0; i < _k +1; ++i){
	     _H(i, _k) += Hk[i];
	   }
	 }
       }
       else{
	 WorkMatrix Vk = get_columns_view(_V, 0, _k + 1);
	 auto Hk = get_vect_view(_H, 0, _k, _k + 1);
	 Hk *= 0;
	 for(int dum = 0; dum < 2; ++dum){
	   auto tmpHk(Hk);

	   if constexpr(! is_distributed<WorkMatrix>::value){
	     gemv(Vk, _W, tmpHk, Scalar{1}, Scalar{0}, 'C'); // tmpHk <- Vk^* W
	     gemv(Vk, tmpHk, _W, Scalar{-1}, Scalar{1}, 'N'); // W <- W - Vk tmpHk
	   }

	   Hk += tmpHk;
	 }
       }
     break;
   }

   _H(_k + 1, _k) = _norm2(_W);
   set_V_vector(_k + 1, _W / _H(_k + 1, _k));

   if(_verbose_ortho){
     std::cout << "Orthgonality loss (norm 2): " << _loss_ortho(true) << '\n';
   }
 }
// Orthogonalization:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Update solution][Update solution:1]]
void update_solution() {
  if(_solution_up_to_date) return;

  Timer<TIMER_ITERATION + 10> t("GMRES: update_sol");
  Vector& X_ext = *_X;

  Vector VkYk_ext = X_ext * Scalar{0};

  if(_k > 0){
    WorkVector VkYk = WorkVector::view(VkYk_ext);
    if(_use_compression){ VkYk = get_columns_view(_Vtilde, 0, _k) * get_vect_view(_Y, 0, 0, _k); }
    else{                 VkYk = get_columns_view(_V     , 0, _k) * get_vect_view(_Y, 0, 0, _k); }
  }

  X_ext = _X0_ext + _M * VkYk_ext;

  _solution_up_to_date = true;
}
// Update solution:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Finalize][Finalize:1]]
void finalize() { _mem_V_th = 0; }
// Finalize:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Constructors][Constructors:1]]
public:

  GMRES(): IterativeSolver<Matrix, Vector, Precond>() {}

  GMRES(const Matrix& A, bool verb=false):
    IterativeSolver<Matrix, Vector, Precond>(A, verb) {}
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Setup functions][Setup functions:1]]
#ifdef MAPHYSPP_USE_SZ_COMPRESSOR
  void _setup(const parameters::compression_accuracy<double>& v) override {
    _zeta = v.value;
  }
#endif

  void _setup(const parameters::restart<int>& v) override {
    _restart = v.value;
  }

  void _setup(const parameters::orthogonalization<Ortho>& v) override {
    _ortho = v.value;
  }

  void _setup(const parameters::verbose_mem<bool>& v) override {
    _verbose_mem = v.value;
  }

  void _setup(const parameters::verbose_ortho_loss<bool>& v) override {
    _verbose_ortho = v.value;
  }

}; // class GMRES
// Setup functions:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Traits][Traits:1]]
// Set traits
template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_solver_direct<GMRES<Matrix, Vector, Precond>> : public std::false_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_solver_iterative<GMRES<Matrix, Vector, Precond>> : public std::true_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_matrix_free<GMRES<Matrix, Vector, Precond>> : public std::true_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct vector_type<GMRES<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename GMRES<Matrix, Vector, Precond>::vector_type;
};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct scalar_type<GMRES<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename GMRES<Matrix, Vector, Precond>::scalar_type;
};
// Traits:1 ends here

// [[file:../../../org/maphys/solver/GMRES.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
