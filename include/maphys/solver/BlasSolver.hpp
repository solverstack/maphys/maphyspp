// [[file:../../../org/maphys/solver/BlasSolver.org::*Header][Header:1]]
#pragma once

#include <lapack.hh>
#include <blas.hh>

#include "maphys/interfaces/linalg_concepts.hpp"
namespace maphys {
  template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector> class BlasSolver;
}

#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/solver/LinearOperator.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Class][Class:1]]
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
class BlasSolver :
  public LinearOperator<Matrix, Vector> {
// Class:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Attributes][Attributes:1]]
private:
using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
using Real = typename LinearOperator<Matrix, Vector>::real_type;
using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar, std::complex<Real>>::type;

public:
using matrix_type = Matrix;
using vector_type = Vector;
using scalar_type = Scalar;

private:
Matrix _A;
IndexArray<Blas_int> _ipiv;
IndexArray<Scalar> _tau;
bool _is_setup = false;
bool _is_facto = false;
lapack::Uplo _facto_uplo = lapack::Uplo::Lower;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Constructors][Constructors:1]]
public:
BlasSolver(){}
BlasSolver(const Matrix& A){
  _A = A;
  _is_setup = true;
}
BlasSolver(Matrix&& A){
  _A = std::move(A);
  _is_setup = true;
}
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Move operations][Move operations:1]]
public:
BlasSolver(const BlasSolver&) = delete;
BlasSolver& operator=(const BlasSolver&) = delete;

BlasSolver(BlasSolver&&) = default;
BlasSolver& operator=(BlasSolver&&) = default;
// Move operations:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Setup][Setup:1]]
void setup(const Matrix& A){
  _A = A;
  _is_setup = true;
  _is_facto = false;
}
void setup(Matrix&& A){
  _A = std::move(A);
  _is_setup = true;
  _is_facto = false;
}
// Setup:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Factorization][Factorization:1]]
void factorize(){
  MAPHYSPP_ASSERT(_is_setup == true, "BlasSolver::factorize: calling factorize before setup(A)");
  Timer<TIMER_SOLVER> t("Dense facto");
  Blas_int M = static_cast<Blas_int>(n_rows(_A));
  Blas_int N = static_cast<Blas_int>(n_cols(_A));
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(_A));

  if(M == 0 || N == 0) return;

  Blas_int err = 0;

  if(M > N){ // Rectangular -> least squares -> QR
    _tau = IndexArray<Scalar>(std::min(M, N));
    err = lapack::geqrf(M, N, get_ptr(_A), lda, &_tau[0]);
  }
  else if(M < N){ // Rectangular -> minimum norm -> LQ
    _tau = IndexArray<Scalar>(std::min(M, N));
    err = lapack::gelqf(M, N, get_ptr(_A), lda, &_tau[0]);
  }
  else if(is_general(_A)){ // Square general -> LU
    _ipiv = IndexArray<Blas_int>(std::min(M, N));
    err = lapack::getrf(M, N, get_ptr(_A), lda, &_ipiv[0]);
  }
  else{
    _facto_uplo = is_storage_upper(_A) ? lapack::Uplo::Upper : lapack::Uplo::Lower;
    if(is_spd(_A)){ // Square SPD -> LLT
      err = lapack::potrf(_facto_uplo, N, get_ptr(_A), lda);
    }
    else{ // Square symmetric -> LDLT
      _ipiv = IndexArray<Blas_int>(N);
      err = lapack::sytrf(_facto_uplo, N, get_ptr(_A), lda, &_ipiv[0]);
    }
  }

  if(err != 0){
    std::string message("Error: lapack ");
    if(M > N){ message += std::string("geqrf"); }
    else if(M < N){ message += std::string("gelqf"); }
    else if(is_general(_A)) { message += std::string("getrf"); }
    else if(is_spd(_A)) { message += std::string("potrf"); }
    else { message += std::string("sytrf"); }
    message += std::string(" returned with value: ") + std::to_string(err);
    MAPHYSPP_ASSERT(err == 0, message);
  }

  _is_facto = true;
}
// Factorization:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Solve][Solve:1]]
void apply(const Vector& B, Vector& X) {
  if(!_is_facto){
    factorize();
  }

  Timer<TIMER_SOLVER> t("Dense solve");

  Blas_int M = static_cast<Blas_int>(n_rows(_A));
  Blas_int N = static_cast<Blas_int>(n_cols(_A));
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(_A));
  Blas_int nrhs = static_cast<Blas_int>(n_cols(B));
  Blas_int ldb = static_cast<Blas_int>(n_rows(B));

  if(M == 0 or N == 0){
    X = Vector();
    return;
  }

  Blas_int err = 0;

  auto err_occured = [](const std::string& fct_name, Blas_int ierr){
    if(ierr != 0){
      std::cerr << "Error: blas/lapack " << fct_name << " returned with value: " << ierr  << '\n';
      return true;
    }
    return false;
  };

  if(M > N){
    Vector QB = B;
    Blas_int ldqb = static_cast<Blas_int>(get_leading_dim(QB));
    Blas_int K = std::min(M, N);

    if constexpr(is_complex<Scalar>::value){
      // Compute Q^*b
      err = lapack::unmqr(lapack::Side::Left, lapack::Op::ConjTrans,
			  M, 1, K,
			  get_ptr(_A), lda,
			  &_tau[0],
			  get_ptr(QB), ldqb);

      if(err_occured("unmqr", err)) return;
    }
    else{
      // Compute Q^T b
      err = lapack::ormqr(lapack::Side::Left, lapack::Op::Trans,
			  M, 1, K,
			  get_ptr(_A), lda,
			  &_tau[0],
			  get_ptr(QB), ldqb);

      if(err_occured("ormqr", err)) return;
    }
    X = QB.get_block_copy(0, 0, N, 1);
    auto ldx = N;

    // TRSM: solve R x = (Q^*b)
    Scalar alpha{1};
    blas::trsm(blas::Layout::ColMajor, blas::Side::Left, blas::Uplo::Upper,
	       blas::Op::NoTrans, blas::Diag::NonUnit, N, 1, alpha, get_ptr(_A),
	       lda, get_ptr(X), ldx);
  }
  else if(M < N){
    // TRSM: solve L y = b
    X = Vector(N);
    X.get_block_view(0, 0, M, 1) = B;
    Scalar * Y_ptr = get_ptr(X);
    const auto ldy = M;
    Scalar alpha{1};
    blas::trsm(blas::Layout::ColMajor, blas::Side::Left, blas::Uplo::Lower,
	       blas::Op::NoTrans, blas::Diag::NonUnit, M, 1, alpha, get_ptr(_A),
	       lda, Y_ptr, ldy);

    const auto ldx = N;
    if constexpr(is_complex<Scalar>::value){
      // Compute X = Q^* Y
      err = lapack::unmlq(lapack::Side::Left, lapack::Op::ConjTrans,
			  N, 1, M,
			  get_ptr(_A), lda,
			  &_tau[0],
			  get_ptr(X), ldx);

      err_occured("unmlq", err);
    }
    else{
      // Compute X = Q^T Y
      err = lapack::ormlq(lapack::Side::Left, lapack::Op::Trans,
			  N, 1, M,
			  get_ptr(_A), lda,
			  &_tau[0],
			  get_ptr(X), ldx);

      err_occured("ormlq", err);
    }
  }
  else{ // A is square
    X = B;
    if(is_general(_A)){
      lapack::Op trans = lapack::Op::NoTrans;
      err = lapack::getrs(trans, N, nrhs, get_ptr(_A), lda, &_ipiv[0], get_ptr(X), ldb);
      err_occured("getrs", err);
    }
    else{
      if(is_spd(_A)){
	err = lapack::potrs(_facto_uplo, N, nrhs, get_ptr(_A), lda, get_ptr(X), ldb);
	err_occured("potrs", err);
      }
      else{
	err = lapack::sytrs(_facto_uplo, N, nrhs, get_ptr(_A), lda, &_ipiv[0], get_ptr(X), ldb);
	err_occured("sytrs", err);
      }
    }
  }
}

void solve(const Vector& B, Vector& X) {
  X = B;
  apply(B, X);
}

Vector apply(const Vector& B){
  Vector X = B; // Create X same size as B
  apply(B, X);
  return X;
}

Vector solve(const Vector& B) { return apply(B); }
// Solve:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Triangular solve][Triangular solve:1]]
void triangular_solve(const Vector& B, Vector& X, MatrixStorage uplo, bool transposed = false){
  if(!_is_facto){
    factorize();
  }
  Timer<TIMER_SOLVER> t("Dense triangular solve");

  const auto nrhs = n_cols(B);
  blas::Uplo b_uplo = (uplo == MatrixStorage::upper) ? blas::Uplo::Upper : blas::Uplo::Lower;
  if(is_sym_or_herm(_A)){
    if(_facto_uplo == lapack::Uplo::Lower){
      if(b_uplo == blas::Uplo::Upper) MAPHYSPP_WARNING("BlasSolver::triangular_solve asked for upper part on sym/herm matrix factorized lower");
      b_uplo = blas::Uplo::Lower;
    }
    else{
      if(b_uplo == blas::Uplo::Lower) MAPHYSPP_WARNING("BlasSolver::triangular_solve asked for lower part on sym/herm matrix factorized upper");
      b_uplo = blas::Uplo::Upper;
    }
  }

  blas::Op b_trans = transposed ? blas::Op::Trans : blas::Op::NoTrans;
  if(transposed and is_complex<Scalar>::value) b_trans = blas::Op::ConjTrans;
  const blas::Diag b_diag = blas::Diag::NonUnit;
  Blas_int N = static_cast<Blas_int>(n_rows(_A));
  const Scalar * A = get_ptr(_A);
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(_A));
  Blas_int incx = 1;

  X = B;
  Scalar * x = get_ptr(X);

  if(nrhs == 1){
    blas::trsv(blas::Layout::ColMajor, b_uplo, b_trans, b_diag, N, A, lda, x, incx);
  }
  else{
    blas::Side b_side = blas::Side::Left;
    Blas_int M = N;
    N = static_cast<Blas_int>(nrhs);
    Scalar alpha{1};
    Blas_int ldb = static_cast<Blas_int>(get_leading_dim(X));
    blas::trsm(blas::Layout::ColMajor, b_side, b_uplo, b_trans, b_diag, M, N, alpha, A, lda, x, ldb);
  }
}

[[nodiscard]] Vector triangular_solve(const Vector& B, MatrixStorage uplo, bool transposed = false){
  Vector X = B; // Create X same size as B
  triangular_solve(B, X, uplo, transposed);
  return X;
}
// Triangular solve:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Display][Display:1]]
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << "Blas solver name: " << name << '\n';
  out << "On matrix: \n";
  _A.display("", out);
  out << "_is_setup: " << _is_setup << '\n';
  out << "_is_facto: " << _is_facto << '\n';
}
// Display:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Traits][Traits:1]]
}; //class BlasSolver
// Traits:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Traits][Traits:2]]
// Set traits
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_solver_direct<BlasSolver<Matrix, Vector>> : public std::true_type {};
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_solver_iterative<BlasSolver<Matrix, Vector>> : public std::false_type {};
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_matrix_free<BlasSolver<Matrix, Vector>> : public std::false_type {};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct has_triangular_solve<BlasSolver<Matrix, Vector>> : public std::true_type {};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct vector_type<BlasSolver<Matrix, Vector>> : public std::true_type {
  using type = typename BlasSolver<Matrix, Vector>::vector_type;
};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct scalar_type<BlasSolver<Matrix, Vector>> : public std::true_type {
  using type = typename BlasSolver<Matrix, Vector>::scalar_type;
};
// Traits:2 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
