// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <type_traits>
#include <cmath>

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/loc_data/SparseMatrixLIL.hpp"
#include "maphys/solver/LinearOperator.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Attributes][Attributes:1]]
template<MPH_Matrix Matrix, MPH_Vector Vector, typename SolverK>
class ImplicitSchur : public LinearOperator<Matrix, Vector>{
private:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
  using Real = typename arithmetic_real<Scalar>::type;

public:
  using matrix_type = Matrix;
  using scalar_type = Scalar;
  using vector_type = Vector;
  using real_type = Real;

private:
  IndexArray<int> _schurlist;

  const Matrix * _K = nullptr;
  SolverK _Kii_inv;
  Matrix _Kii;
  Matrix _Kig;
  Matrix _Kgi;
  Matrix _Kgg;
  Vector _Fi;

  IndexArray<int> _perm;
  IndexArray<int> _invperm;
  Size _n_i;
  Size _n_g;

  bool _splitted = false;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Constructors][Constructors:1]]
public:
ImplicitSchur(){}
ImplicitSchur(const Matrix& K, const IndexArray<int>& schurlist): _K{&K}, _schurlist{schurlist} {}

ImplicitSchur(const ImplicitSchur&) = default;
ImplicitSchur& operator=(const ImplicitSchur&) = default;
ImplicitSchur(ImplicitSchur&&) = default;
ImplicitSchur& operator=(ImplicitSchur&&) = default;
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Split matrix][Split matrix:1]]
public:
void split_matrix(){
  MAPHYSPP_ASSERT(_K != nullptr, "ImplicitSchur: K is null pointer");
  MAPHYSPP_ASSERT(_schurlist.size() > 0, "ImplicitSchur: size of the schurlist must be not null");

  const Size n_tot = n_rows(*_K);

  compute_permutations();
  Matrix K_ord = *_K;

  reindex<Scalar, int>(K_ord, _perm);
  MAPHYSPP_ASSERT(_n_g < n_tot, "ImplicitSchur: size of the schurlist is larget than matrix size");
  const int ni = static_cast<int>(_n_i);
  const int ng = static_cast<int>(_n_g);

  SparseMatrixLIL<Scalar> Kii(static_cast<Size>(ni), static_cast<Size>(ni));
  SparseMatrixLIL<Scalar> Kig(static_cast<Size>(ni), static_cast<Size>(ng));
  SparseMatrixLIL<Scalar> Kgi(static_cast<Size>(ng), static_cast<Size>(ni));
  SparseMatrixLIL<Scalar> Kgg(static_cast<Size>(ng), static_cast<Size>(ng));

  Kii.copy_properties(*_K);
  Kgg.copy_properties(*_K);

  IJVIterator<Scalar, int, Matrix> it_K(K_ord);
  for(auto [i, j, v] : it_K){
    if(i >= ni){
      if(j >= ni){
        Kgg.insert(i - ni, j - ni, v);
      }
      else{
        Kgi.insert(i - ni, j, v);
      }
    }
    else{
      if(j >= ni){
        Kig.insert(i, j - ni, v);
      }
      else{
        Kii.insert(i, j, v);
      }
    }
  }

  auto convert_to_matrix = [](SparseMatrixLIL<Scalar>& matrix_in, Matrix& matrix_out){
                             SparseMatrixCOO<Scalar> coomat(matrix_in.to_coo());
                             matrix_in = SparseMatrixLIL<Scalar>(0, 0);
                             build_matrix(matrix_out, coomat.get_n_rows(), coomat.get_n_cols(), coomat.get_nnz(), coomat.get_i_ptr(), coomat.get_j_ptr(), coomat.get_v_ptr());
                             matrix_out.copy_properties(coomat);
                           };

  convert_to_matrix(Kii, _Kii);

  _Kii_inv.setup(_Kii);

  convert_to_matrix(Kig, _Kig);
  convert_to_matrix(Kgi, _Kgi);
  convert_to_matrix(Kgg, _Kgg);

  // Dealing with half storage issues
  if(_K->is_storage_lower()){
    _Kig = adjoint(_Kgi);
  }
  else if(_K->is_storage_upper()){
    _Kgi = adjoint(_Kig);
  }

  _splitted = true;
}
// Split matrix:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Permutations][Permutations:1]]
private:
  // Compute perm and invperm to set schur unknowns at the end
  void compute_permutations(){
    const auto M = n_rows(*_K);
    _perm = IndexArray<int>(M);
    _invperm = IndexArray<int>(M);

    _schurlist.sort();

    _n_g = _schurlist.size();
    _n_i = M - _n_g;

    Size idx_g = 0;
    Size idx_i = 0;

    // ex: M = 4; schurlist = [0, 2]
    // perm = [2, 0, 3, 1] (B -> (Fi, Fg)^T)
    // invperm = [1, 3, 0, 2] ((Ui, Ug)^T -> U)
    for(int k = 0; k < static_cast<int>(M); ++k){
      if((idx_g >= _n_g) || (_schurlist[idx_g] != k)){
        _perm[k] = idx_i;
        _invperm[idx_i] = k;
        idx_i++;
      }
      else{
        _perm[k] = _n_i + idx_g;
        _invperm[_n_i + idx_g] = k;
        idx_g++;
      }
    }
  }
// Permutations:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Setup][Setup:1]]
public:
  void setup(const Matrix& K){
    _K = &K;
  }

  void setup_schurlist(const IndexArray<int>& schurlist){
    _schurlist = schurlist;
  }
// Setup:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Apply][Apply:1]]
Vector apply(const Vector& x){
  if(!_splitted){ 
    split_matrix(); 
  }

  Vector y = _Kig * x;
  y = _Kii_inv * y;
  y = _Kgi * y;
  return _Kgg * x - y;
}
// Apply:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Get Schur system RHS][Get Schur system RHS:1]]
Vector compute_rhs(const Vector& B){
  if(!_splitted){
    split_matrix(); 
  }
  _Fi = Vector(_n_i);
  Vector Fg(_n_g);

  for(Size k = 0; k < (_n_i + _n_g); ++k){
    if(_perm[k] >= static_cast<int>(_n_i)){
      Fg[_perm[k] - _n_i] = B[k];
    }
    else{
      _Fi[_perm[k]] = B[k];
    }
  }

  return Fg - _Kgi * (_Kii_inv * _Fi);
}
// Get Schur system RHS:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Get solution][Get solution:1]]
void get_solution(const Vector& Ug, Vector& U) const {
  Vector Ui = _Kii_inv * (_Fi - _Kig * Ug);

  for(Size k = 0; k < _n_i; ++k){
    U[_invperm[k]] = Ui[k];
  }
  for(Size k = 0; k < _n_g; ++k){
    U[_invperm[_n_i + k]] = Ug[k];
  }
}

Vector get_solution(const Vector& Ug) const {
  Vector U(_n_i + _n_g);
  get_solution(Ug, U);
  return U;
}
// Get solution:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Compute explicit Schur complement][Compute explicit Schur complement:1]]
Matrix get_schur(){
  return _Kgg - _Kgi * (_Kii_inv * _Kig);
}

using DMat = typename dense_type<Matrix>::type;

DMat get_dense_schur(){
  if constexpr(is_solver_direct<SolverK>::value){
    SolverK schur_solver(*_K);
    return schur_solver.get_schur(_schurlist);
  }
  if(!_splitted){
    split_matrix();
  }
  DMat dense_Kig;
  _Kig.convert(dense_Kig);
  DMat S;
  _Kgg.convert(S);
  for(Size k = 0; k < _n_g; ++k){
    auto S_col_k = S.get_vect_view(k);
    auto Kig_col_k = dense_Kig.get_vect_view(k);
    S_col_k -= _Kgi * (_Kii_inv * Kig_col_k);
  }
  return S;
}
// Compute explicit Schur complement:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Getters][Getters:1]]
[[nodiscard]] SolverK& get_solver_K() { return _Kii_inv; }
}; // class ImplicitSchur
// Getters:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Traits][Traits:1]]
template<MPH_Matrix Matrix, MPH_Vector Vector, typename SolverK>
struct vector_type<ImplicitSchur<Matrix, Vector, SolverK>> : public std::true_type { using type = Vector; };

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SolverK>
struct scalar_type<ImplicitSchur<Matrix, Vector, SolverK>> : public std::true_type {
  using type = typename ImplicitSchur<Matrix, Vector, SolverK>::scalar_type;
};
// Traits:1 ends here

// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Footer][Footer:1]]
} //namespace maphys
// Footer:1 ends here
