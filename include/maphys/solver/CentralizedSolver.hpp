// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Header][Header:1]]
#pragma once

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/solver/LinearOperator.hpp"
#include "maphys/solver/IterativeSolver.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Parameters][Parameters:1]]
namespace parameters{
CREATE_STRUCT(root)
} // end namespace parameters
// Parameters:1 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Attributes][Attributes:1]]
template<MPH_Matrix Matrix, MPH_Vector Vector, typename Solver>
class CentralizedSolver :
    public LinearOperator<Matrix, Vector> {

public:
  using scalar_type = typename Vector::scalar_type;
  using real_type = typename Vector::real_type;
  using matrix_type = Matrix;
  using vector_type = Vector;
  using solver_type = Solver;

private:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
  using Real = typename LinearOperator<Matrix, Vector>::real_type;

  using GlobMatrix = typename Matrix::local_type;
  using GlobVector = typename Vector::local_type;

  Solver _loc_solver;
  std::unique_ptr<Matrix> _A;
  int _root = 0;

  bool _is_setup = false;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Constructors][Constructors:1]]
public:
  CentralizedSolver(){}
  CentralizedSolver(const Matrix& A) { setup(A); }
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Setup][Setup:1]]
private:
  void _setup(const parameters::A<Matrix>&       v) { _is_setup = false; setup(v.value); }
  void _setup(const parameters::A<const Matrix>& v) { _is_setup = false; setup(v.value); }
  void _setup(const parameters::root<int>&       v) { _is_setup = false; _root  = v.value; }

public:
  void setup(const Matrix& A){
    _A = std::make_unique<Matrix>(A);
  }

  // Variadic template -> each parameter is a call to the _setup function
  template <typename... Types>
  void setup(const Types&... args) noexcept {
    ( void(_setup(args)), ...);
  }

  template<typename... Types>
  void setup(const Matrix& A, const Types&... args){
    _setup(parameters::A{A});
    setup(args...);
  }
// Setup:1 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Solve][Solve:1]]
Vector solve(const Vector& B){
  MAPHYSPP_ASSERT(_A != nullptr, "CentralizedSolver: calling solve() before setting up matrix");
  MPI_Comm comm = _A->get_proc()->master_comm();
  std::shared_ptr<Process> p = B.get_proc();
  MAPHYSPP_ASSERT(p == _A->get_proc(), "CentralizedSolver::solve b and A not on the same Process");

  // TOFIX ? negative root for bcast
  MAPHYSPP_ASSERT_POSITIVE(_root, "CentralizedSolver::solve root");

  const bool is_master = p->is_master();
  if(!is_master) return Vector(p);

  const auto rank = MMPI::rank(comm);

  if(!_is_setup){
    GlobMatrix A_centr = _A->centralize(_root);
    if((_root) < 0 || (_root == rank)){
      _loc_solver.setup(std::move(A_centr));
    }
    _is_setup = true;
  }

  GlobVector glob_rhs = B.centralize(_root);
  Vector X_dist = B;

  if(_root < 0){
    GlobVector X_loc = _loc_solver * glob_rhs;
    X_dist.from_global_vector(X_loc);
  }
  else{
    if(_root == rank){
      GlobVector X_loc = _loc_solver * glob_rhs;
      MMPI::bcast(get_ptr(X_loc), size(X_loc), _root, comm);
      X_dist.from_global_vector(X_loc);
    }
    else{
      GlobVector X_loc(B.get_size());
      MMPI::bcast(get_ptr(X_loc), size(X_loc), _root, comm);
      X_dist.from_global_vector(X_loc);
    }
  }

  return X_dist;
}

Vector apply(const Vector& B) { return solve(B); }
// Solve:1 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Getters][Getters:1]]
Solver& get_local_solver(){ return _loc_solver; }
// Getters:1 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Traits][Traits:1]]
}; // class CentralizedSolver
// Traits:1 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Traits][Traits:2]]
template<MPH_Matrix Matrix, MPH_Vector Vector, typename Solver>
struct vector_type<CentralizedSolver<Matrix, Vector, Solver>> : public std::true_type {
  using type = typename CentralizedSolver<Matrix, Vector, Solver>::vector_type;
};

template<MPH_Matrix Matrix, MPH_Vector Vector, typename Solver>
struct scalar_type<CentralizedSolver<Matrix, Vector, Solver>> : public std::true_type {
  using type = typename CentralizedSolver<Matrix, Vector, Solver>::scalar_type;
};
// Traits:2 ends here

// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
