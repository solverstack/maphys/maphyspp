// [[file:../../../org/maphys/solver/IRSolver.org::*Header][Header:1]]
#pragma once

#include "maphys/solver/IterativeSolver.hpp"
#ifndef MAPHYSPP_NO_MPI
#include "maphys/part_data/PartMatrix.hpp"
#endif

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/IRSolver.org::*Base class][Base class:1]]
struct IR_NoSolver{};

template<MPH_Matrix Matrix, MPH_Vector Vector, class InnerSolver, class ExactSolver = IR_NoSolver>
class IRSolver :
    public IterativeSolver<Matrix, Vector> {
// Base class:1 ends here

// [[file:../../../org/maphys/solver/IRSolver.org::*Attributes][Attributes:1]]
using Scalar = typename IterativeSolver<Matrix, Vector>::scalar_type;
 using Real = typename IterativeSolver<Matrix, Vector>::real_type;

 using IterativeSolver<Matrix, Vector>::_A;
 using IterativeSolver<Matrix, Vector>::_B;
 using IterativeSolver<Matrix, Vector>::_X;
 using IterativeSolver<Matrix, Vector>::_R;
 using IterativeSolver<Matrix, Vector>::_squared;
 using IterativeSolver<Matrix, Vector>::_tol_sq;
 using IterativeSolver<Matrix, Vector>::_max_iter;
 using IterativeSolver<Matrix, Vector>::_verbose;
 using IterativeSolver<Matrix, Vector>::_residual_sq;
 using IterativeSolver<Matrix, Vector>::_custom_convergence_check;
 using IterativeSolver<Matrix, Vector>::_stop_crit_denom_inv;

 InnerSolver _inner_solver;
 ExactSolver _exact_solver; // Reference for experiments

 Vector D;
 Vector D_exact;
 std::function<void(const Matrix&, const Vector&)> _setup_inner_solver;

 int _cumul_iter = 0;

 public:

public:

  IRSolver(): IterativeSolver<Matrix, Vector>() {}

  IRSolver(const Matrix& A, bool verb=false):
    IterativeSolver<Matrix, Vector>(A, verb) {}
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/IRSolver.org::*C++ code][C++ code:1]]
int iterative_solve() override {
  Timer<TIMER_ITERATION> t0("IR iteration 0");

  const Matrix& A = *_A;
  const Vector& B = *_B;
  Vector& X = *_X;
  Vector& R = _R;

  this->setup_initial_stop_crit("Iterative Refinement solver");

  this->_check_true_residual = false;
  D = X * Scalar{0};
  _inner_solver.setup(*_A);
  _cumul_iter = 0;

  if constexpr(!std::is_same_v<ExactSolver,IR_NoSolver>){
    D_exact = D;
    _exact_solver.setup(*_A);
  }
  else{ (void) D_exact; (void) _exact_solver; }

  R = B - A * X;
  t0.stop();
  _residual_sq = _squared(R) * _stop_crit_denom_inv;
  if(_verbose) std::cout << "IR - 0 -\t||B-AX||/||B||\t" << std::sqrt(_residual_sq) << '\n';
  if(_residual_sq < _tol_sq) return 0;

  for(int iter = 1; iter < _max_iter + 1; ++iter){
    Timer<TIMER_ITERATION> t("IR iteration");

    if(_setup_inner_solver){ _setup_inner_solver(A, R); }

    D = _inner_solver * R;

    if constexpr(is_solver_iterative<InnerSolver>::value){
      _cumul_iter += _inner_solver.get_n_iter_performed();
    }

    if constexpr(!std::is_same_v<ExactSolver,IR_NoSolver>){

      auto inf_norm = [](const Vector& v){
        Scalar i_norm = 0;
        for(Size k = 0; k < n_rows(v); ++k){
          i_norm = std::max(i_norm, std::abs(v[k]));
        }
        return i_norm;
      };

      D_exact = _exact_solver * R;
      Real delta = inf_norm(D_exact - D);
      delta /= inf_norm(D_exact);
      std::cout << "delta = " << delta << '\n';
    }

    X += D;
    R = B - A * X;
    _residual_sq = _squared(R) * _stop_crit_denom_inv; 

    if(_verbose) std::cout << "IR - " << iter << " -\t||B-AX||/||B||\t" << std::sqrt(_residual_sq) << '\n';
    if(_residual_sq < _tol_sq) return iter;
  }

  return -1;
}
// C++ code:1 ends here

// [[file:../../../org/maphys/solver/IRSolver.org::*Setup parameters][Setup parameters:1]]
void _setup(const parameters::setup_inner_solver<std::function<void(const Matrix&, const Vector&)>>& v) override { _setup_inner_solver  = v.value; }
// Setup parameters:1 ends here

// [[file:../../../org/maphys/solver/IRSolver.org::*Getters][Getters:1]]
InnerSolver& get_inner_solver(){ return _inner_solver; }
ExactSolver& get_exact_solver(){ return _exact_solver; }
int get_n_cumul_iter(){ return _cumul_iter; }
// Getters:1 ends here

// [[file:../../../org/maphys/solver/IRSolver.org::*Footer][Footer:1]]
}; // class IRSolver
} //namespace maphys
// Footer:1 ends here
