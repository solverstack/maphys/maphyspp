// [[file:../../../org/maphys/solver/Mumps.org::*Header][Header:1]]
#pragma once

#include <mpi.h>
#include <maphys/interfaces/linalg_concepts.hpp>

namespace maphys {
  template<typename  Matrix, MPH_Vector_Single_Or_Multiple Vector>
  requires (MPH_SparseMatrix<Matrix> || MPH_PartSparseMatrix<Matrix>)

  class Mumps;
}

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/FortranArray.hpp"
#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/loc_data/SparseMatrixCOO.hpp"
#include "maphys/loc_data/SparseMatrixCSC.hpp"
#include "maphys/part_data/PartMatrix.hpp"
#include "maphys/solver/LinearOperator.hpp"

extern "C" {
#include "cmumps_c.h"
#include "dmumps_c.h"
#include "smumps_c.h"
#include "zmumps_c.h"
}

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Structures depending on arithmetic types][Structures depending on arithmetic types:1]]
/* Arithmetic structure for mumps */

#define MUMPS_STRUCT_ARITHMETIC( type_, structype_, real_, complex_, fct_ ) \
  template<>                                                            \
  struct MumpsStructDefinition<type_> : public std::true_type {         \
  using type = structype_;                                              \
  using real_type = real_;                                              \
  using complex_type = complex_;                                        \
  static void driver_mumps(structype_& s){ fct_(&s); }                  \
  };

template<class Scalar>
struct MumpsStructDefinition : public std::false_type {};

MUMPS_STRUCT_ARITHMETIC(float               , SMUMPS_STRUC_C, SMUMPS_REAL, SMUMPS_COMPLEX, smumps_c )
MUMPS_STRUCT_ARITHMETIC(double              , DMUMPS_STRUC_C, DMUMPS_REAL, DMUMPS_COMPLEX, dmumps_c )
MUMPS_STRUCT_ARITHMETIC(std::complex<float> , CMUMPS_STRUC_C, CMUMPS_REAL, CMUMPS_COMPLEX, cmumps_c )
MUMPS_STRUCT_ARITHMETIC(std::complex<double>, ZMUMPS_STRUC_C, ZMUMPS_REAL, ZMUMPS_COMPLEX, zmumps_c )
/*---------------------------------*/
// Structures depending on arithmetic types:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Attributes][Attributes:1]]
template<typename  Matrix, MPH_Vector_Single_Or_Multiple Vector>
requires (MPH_SparseMatrix<Matrix> || MPH_PartSparseMatrix<Matrix>)
class Mumps :
  public LinearOperator<Matrix, Vector> {

private:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
  using Real = typename arithmetic_real<Scalar>::type;

public:
  using scalar_type = Scalar;
  using real_type = Real;
  using matrix_type = Matrix;
  using vector_type = Vector;

private:    
  using MumpsStruct = typename MumpsStructDefinition<Scalar>::type;
  using MumpsReal = typename MumpsStructDefinition<Scalar>::real_type;
  using MumpsScalar = typename MumpsStructDefinition<Scalar>::complex_type;

  static const constexpr Size ICNTL_SIZE = 60;
  static const constexpr Size CNTL_SIZE = 15;
  static const constexpr Size INFO_SIZE = 80;
  static const constexpr Size INFOG_SIZE = 80;
  static const constexpr Size RINFO_SIZE = 40;
  static const constexpr Size RINFOG_SIZE = 40;
  inline static std::mutex _mumps_mutex;
  inline static int _counter_warning;

  void (*_driver_mumps)(MumpsStruct&) = MumpsStructDefinition<Scalar>::driver_mumps;

  int _rank;
  int _host;
  static constexpr const bool _is_distributed = is_distributed<Matrix>::value;
  static_assert((_is_distributed && is_distributed<Vector>::value) || !(_is_distributed || is_distributed<Vector>::value));

  // Copy of the input matrix, with fortran numbering
  SparseMatrixCOO<Scalar, MUMPS_INT> _A;

  MumpsStruct _id_mumps;
  FortranArray<MUMPS_INT> ICNTL, INFO_MUMPS, INFOG;
  FortranArray<Real> CNTL, RINFO, RINFOG;
  MPI_Comm _comm;

  // To set parameters before initializing mumps
  std::map<int, MUMPS_INT> _icntl_params;
  std::map<int, Real> _cntl_params;

  // Keep data to have valid pointers
  Vector _rhs;
  MUMPS_INT _n_schur = -1;
  IndexArray<MUMPS_INT> _schurlist;
  IndexArray<Scalar> _rhs_centr; // For distributed input
  IndexArray<Scalar> _schur;
  IndexArray<Scalar> _sol_loc;
  IndexArray<MUMPS_INT> _isol_loc;

  bool _is_init = false;
  bool _is_setup = false;
  bool _is_facto = false;
  bool _debug = false;
  bool _is_master = true;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Check mumps status][Check mumps status:1]]
void check_status(){
  if(INFOG(1) == 0) return;
  std::cerr << "MUMPS returned with INFOG(1),INFO(1),INFO(2) = " << INFOG(1);
  std::cerr << ',' << INFO_MUMPS(1) << ',' << INFO_MUMPS(2) << '\n';
  MAPHYSPP_ASSERT(INFOG(1) >= 0, "MUMPS fatal error");
}
// Check mumps status:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Calling mumps][Calling mumps:1]]
void xmumps(const int job){
  {
    std::lock_guard<std::mutex> lock_mumps(_mumps_mutex);
    if(_debug) std::cerr << "DEBUG: Before MUMPS job " << job << '\n';
    if(!_is_master) return;
    _id_mumps.job = job;
    _driver_mumps(_id_mumps);
    if(_debug) std::cerr << "DEBUG: After MUMPS job " << job << '\n';
    check_status();
  }
}
// Calling mumps:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Initialize the structure][Initialize the structure:1]]
void initialize_struct(){
  _id_mumps.irn = NULL;
  _id_mumps.jcn = NULL;
  _id_mumps.a = NULL;
  _id_mumps.irn_loc = NULL;
  _id_mumps.jcn_loc = NULL;
  _id_mumps.a_loc   = NULL;
  _id_mumps.listvar_schur = NULL;
  _id_mumps.schur = NULL;
  _id_mumps.rhs = NULL;
  _id_mumps.rhs_sparse = NULL;
  _id_mumps.irhs_sparse = NULL;
  _id_mumps.irhs_ptr = NULL;

  ICNTL.set_data(ICNTL_SIZE, &(_id_mumps.icntl[0]));
  CNTL.set_data(CNTL_SIZE, &(_id_mumps.cntl[0]));
  INFO_MUMPS.set_data(INFO_SIZE, &(_id_mumps.info[0]));
  INFOG.set_data(INFOG_SIZE, &(_id_mumps.infog[0]));
  RINFO.set_data(RINFO_SIZE, &(_id_mumps.rinfo[0]));
  RINFOG.set_data(RINFOG_SIZE, &(_id_mumps.rinfog[0]));
  _counter_warning = 0;
}
// Initialize the structure:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Set the matrix][Set the matrix:1]]
template<typename LocMat = Matrix>
void set_sym(const LocMat& A){
  if(A.is_storage_full()){
    _id_mumps.sym = 0;
  }
  else if(A.is_spd()){
    _id_mumps.sym = 1;
  }
  else{
    _id_mumps.sym = 2;
  }
}

void set_matrix(const Matrix& A){
  _is_facto = false;
  if constexpr(_is_distributed){
    if(A.get_sd_ids().size() > 0){
      set_sym(A.get_local_matrix(A.get_sd_ids()[0]));
    }
  }
  else{
    set_sym(A);
  }

  if constexpr(_is_distributed){
    std::shared_ptr<Process> p = A.get_proc();
    _is_master = p->is_master();
    if(_is_master){
      set_mpi_comm(p->master_comm(), 0);
    }
    else{
      set_mpi_comm();
    }

    // We only want one global matrix
    Matrix A_cpy = A;
    A_cpy.local_to_global();

    IndexArray<int> sd_ids = A_cpy.get_sd_ids();

    _A = SparseMatrixCOO<Scalar, MUMPS_INT>(A_cpy.get_n_rows(), A_cpy.get_n_rows());

    // So we sum the local matrices into the global one
    for(int sd_id : sd_ids){
      SparseMatrixCOO<Scalar, MUMPS_INT> A_tmp;
      auto& A_loc = A_cpy.get_local_matrix(sd_id);
      A_loc.convert(A_tmp);
      _A += A_tmp;
    }
  }
  else{
    set_mpi_comm();
    _A = A.template to_coo<MUMPS_INT>();
  }

  xmumps(-1);
  _is_init = true;

  // Put preset values
  for(auto& [id, value] : _icntl_params) { ICNTL(id) = value; }
  _icntl_params.clear();
  for(auto& [id, value] : _cntl_params) { CNTL(id) = value; }
  _cntl_params.clear();

  // Offset i and j by one for fortran numbering
  auto M = n_rows(_A);
  auto N = n_cols(_A);
  MAPHYSPP_ASSERT(M == N, "Trying to use Mumps solver with non square matrix");

  auto nnz = n_nonzero(_A);
  auto * i_ptr = get_i_ptr(_A);
  auto * j_ptr = get_j_ptr(_A);
  auto * v_ptr = get_v_ptr(_A);

  for(Size k = 0; k < nnz; ++k){
    i_ptr[k] += 1;
    j_ptr[k] += 1;
  }

  // Verbose by default
  ICNTL(1) = 6;
  ICNTL(2) = 0;
  ICNTL(3) = 0;
  ICNTL(4) = 1;

  ICNTL(5) = 0; // the matrix is given is assembled format
  if constexpr(_is_distributed){
    ICNTL(18) = 3; // the input matrix is distributed
  }
  else{
    ICNTL(18) = 0; // the input matrix is centralised
  }

  ICNTL(14) = 100;  // set the estimated memory increase
  CNTL(1) = 0.0; // deactivate pivoting

  ICNTL(24) = 1; // Null pivot detection

  if(_is_distributed){
    _id_mumps.n = static_cast<MUMPS_INT>(M);
    _id_mumps.nz_loc = static_cast<MUMPS_INT8>(nnz);
    _id_mumps.irn_loc = i_ptr;
    _id_mumps.jcn_loc = j_ptr;
    _id_mumps.a_loc = (MumpsScalar*) v_ptr;
  }
  else if(_rank == _host){
    _id_mumps.n = static_cast<MUMPS_INT>(M);
    _id_mumps.nz = static_cast<MUMPS_INT8>(nnz);
    _id_mumps.irn = i_ptr;
    _id_mumps.jcn = j_ptr;
    _id_mumps.a = (MumpsScalar*) v_ptr;
  }

  _is_setup = true;
}
// Set the matrix:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Set the communicator][Set the communicator:1]]
void set_mpi_comm(MPI_Comm comm = MPI_COMM_SELF, const int host = 0){
  _comm = MMPI::comm_dup(comm);
  _id_mumps.comm_fortran = (MUMPS_INT) MPI_Comm_c2f(_comm);
  _host = host;
  _rank = MMPI::rank(_comm);
  _id_mumps.par = 1;
}
// Set the communicator:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Constructors][Constructors:1]]
public:

  Mumps(){
    initialize_struct();
  }

  Mumps(const Matrix& A){
    initialize_struct();
    set_matrix(A);
  }

  ~Mumps(){
    if(_is_init){
      xmumps(-2);
    }
  }

  // Delete copy
  Mumps(const Mumps&) = delete;
  Mumps& operator=(const Mumps&) = delete;

  // Allow move
private:
  inline void move_mumps(Mumps&& other){
    if(&other == this) return;
    this->initialize_struct();
    _rank = std::exchange(other._rank, 0);
    _host = std::exchange(other._host, 0);
    _A = std::move(other._A);
    _id_mumps = std::move(other._id_mumps);
    other.initialize_struct();

    _rhs = std::move(other._rhs);
    _n_schur = std::exchange(other._n_schur, -1);
    _schurlist = std::move(other._schurlist);
    _schur = std::move(other._schur);
    _rhs_centr = std::move(other._rhs_centr);
    _sol_loc = std::move(other._sol_loc);
    _isol_loc = std::move(other._isol_loc);

    _is_init = std::exchange(other._is_init, false);
    _is_setup = std::exchange(other._is_setup, false);
    _is_facto = std::exchange(other._is_facto, false);
    _debug = std::exchange(other._debug, false);
    _is_master = std::exchange(other._is_master, true);
  }

public:
  Mumps(Mumps&& other){ move_mumps(std::move(other)); }

  Mumps& operator=(Mumps&& other){
    move_mumps(std::move(other));
    return *this;
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Setup function][Setup function:1]]
void setup(const Matrix& A){
  set_matrix(A);
}
// Setup function:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Factorization][Factorization:1]]
void factorize(){
  if(!_is_master) return;
  MAPHYSPP_ASSERT( _is_setup, "Trying to call factorize before setup !");
  if (_counter_warning++ == 1) {
      MAPHYSPP_WARNING("Mumps direct solver does not support the multithreading and is sequentially running.\n");
  }
  // Perform analysis and factorization
  {
    Timer<TIMER_SOLVER> t_a("Mumps analysis");
    xmumps(1); // Analysis
  }{
    Timer<TIMER_SOLVER> t_f("Mumps facto");
    xmumps(2); // Facto
  }
  _is_facto = true;
}
// Factorization:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Set RHS][Set RHS:1]]
void set_rhs(Vector B){ // Move if rvalue
  if(!_is_master) return;
  _rhs = std::move(B);

  MUMPS_INT nrhs = static_cast<MUMPS_INT>(n_cols(_rhs));
  MUMPS_INT lrhs = static_cast<MUMPS_INT>(n_rows(_rhs));

  ICNTL(20) = 0; // Dense RHS
  _id_mumps.nrhs = nrhs;
  _id_mumps.lrhs = lrhs;
  if constexpr(_is_distributed){
    ICNTL(21) = 1; // Solution is distributed
    _id_mumps.lsol_loc = INFO_MUMPS(23);
    _sol_loc = IndexArray<Scalar>(_id_mumps.lsol_loc * nrhs);
    _isol_loc = IndexArray<MUMPS_INT>(_id_mumps.lsol_loc);
    _id_mumps.sol_loc = &_sol_loc[0];
    _id_mumps.isol_loc = &_isol_loc[0];
    auto ctr_vect = _rhs.centralize(_host);
    if(_rank == _host){
      _rhs_centr = IndexArray<Scalar>(&ctr_vect[0], n_rows(ctr_vect));
      _id_mumps.rhs = &_rhs_centr[0];
    }
  }
  else{
    ICNTL(21) = 0; // Solution is centralized
    if(_rank == _host){
      _id_mumps.rhs = (MumpsScalar*) get_ptr(_rhs);
    }
  }
}
// Set RHS:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Get solution][Get solution:1]]
Vector get_solution(){
  if constexpr(!_is_distributed){
    return Vector(_rhs);
  }
  else{
    if(!_is_master) return Vector();
    using LocVect = typename Vector::local_type;
    //TODO: nrhs > 1 ?
    LocVect glob_v(n_rows(_A));
    int k = 0;
    for(int idx : _isol_loc){
      glob_v[idx - 1] = _sol_loc[k++];
    }

    LocVect glob_v_out = glob_v;

    MMPI::allreduce(&glob_v[0], &glob_v_out[0], static_cast<int>(n_rows(glob_v)), MPI_SUM, _comm);

    Vector out_v(_rhs);
    out_v.from_global_vector(glob_v_out);

    return out_v;
  }
}
// Get solution:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Solve function][Solve function:1]]
template<typename VectorURef>
Vector solve(VectorURef&& B){
  if(!_is_facto){
    factorize();
  }
  Timer<TIMER_SOLVER> t("Mumps solve");

  this->set_rhs(std::forward<VectorURef>(B));
  xmumps(3); // Solve

  return get_solution();
}

Vector apply(const Vector& B) { return solve(B); }
Vector apply(Vector&& B) { return solve(std::move(B)); }

void apply(const Vector& B, Vector& X) { 
  X = apply(B);
}

void solve(const Vector& B, Vector& X) { 
  apply(B, X); 
}
// Solve function:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Display info][Display info:1]]
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << "Mumps solver name: " << name << '\n';
  out << " - On arithmetic: " << arithmetic_name<Scalar>::name << '\n';
  out << std::boolalpha << " - Is init / setup / facto: " << _is_init << " / "<< _is_setup << " / "<< _is_facto << '\n';
  if(_is_setup){
    out << " - On matrix A of size: " << n_rows(_A) << '\n';
  }
  if(_n_schur >= 0){
    out << " - Schur computed, of size: " << _n_schur << '\n';
  }
  out << '\n';
}
// Display info:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Get Schur complement][Get Schur complement:1]]
template <MPH_Integral Index>
IndexArray<Scalar> get_schur_as_array(const IndexArray<Index>& schurlist){
  ICNTL(19) = 1; // Schur on the host
  _n_schur = static_cast<MUMPS_INT>(schurlist.size());

  // Copy to get Fortran numbering (and correct int type)
  _schurlist = IndexArray<MUMPS_INT>(schurlist.size());
  for(Size k = 0; k < schurlist.size(); ++k){
    _schurlist[k] = static_cast<MUMPS_INT>(schurlist[k]) + 1;
  }

  _id_mumps.size_schur = _n_schur;
  _id_mumps.listvar_schur = &_schurlist[0];

  _schur = IndexArray<Scalar>(_n_schur * _n_schur);
  _id_mumps.schur = (MumpsScalar *) &_schur[0];

  this->factorize();

  // Deal with symmetry?
  // Full storage for now
  // NB: returns Row Major dense Schur (in centralized)
  if(_id_mumps.sym != 0){
    for(MUMPS_INT i = 0; i < _n_schur; ++i){
      for(MUMPS_INT j = i+1; j < _n_schur; ++j){
        _schur[i * _n_schur + j] = conj<Scalar>(_schur[j * _n_schur + i]);
      }
    }
  }

  return _schur;
}
// Get Schur complement:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Get Schur complement][Get Schur complement:2]]
template <MPH_Integral Index>
decltype(auto) get_schur(const IndexArray<Index>& schurlist){
  using DenseMat = typename dense_type<Matrix>::type;
  int n_schur = static_cast<int>(schurlist.size());
  DenseMat schur_mat;
  auto schur = this->get_schur_as_array(schurlist);
  build_dense_matrix(schur_mat, n_schur, n_schur, &schur[0]);
  if(_id_mumps.sym != 0){
    schur_mat.set_property(MatrixSymmetry::symmetric);
  }
  return schur_mat;
}
// Get Schur complement:2 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*b2f][b2f:1]]
template<typename VectorURef>
Vector b2f(VectorURef&& B){
  MAPHYSPP_ASSERT(_n_schur >= 0, "Schur complement not computed. Call a Mumps.get_schur function before b2f");
  Timer<TIMER_SOLVER> t("Mumps b2f");
  this->set_rhs(std::forward<VectorURef>(B));
  ICNTL(26) = 1; // Forward elimination with the Schur

  Vector f(static_cast<Size>(_n_schur));

  _id_mumps.redrhs = (MumpsScalar *) get_ptr(f);
  if(_id_mumps.nrhs > 1){
    _id_mumps.lredrhs = static_cast<MUMPS_INT>(n_rows(B));
  }
  xmumps(3); // Solve

  return f;
}
// b2f:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*y2x][y2x:1]]
Vector y2x(const Vector& x_g){
  MAPHYSPP_ASSERT(_n_schur >= 0, "Schur complement not computed. Call a Mumps.get_schur and b2f before y2x");
  Timer<TIMER_SOLVER> t("Mumps y2x");
  ICNTL(26) = 2; // Backward elimination with the Schur

  _id_mumps.redrhs = (MumpsScalar *) get_ptr(x_g);
  if(_id_mumps.nrhs > 1){
    _id_mumps.lredrhs = static_cast<MUMPS_INT>(n_rows(x_g));
  }

  xmumps(3); // Solve

  return get_solution();
}
// y2x:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Set number of threads][Set number of threads:1]]
template<MPH_Integral Tint>
void set_n_threads(Tint n_threads){
  set_icntl(16, n_threads);
}
// Set number of threads:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Low-rank options][Low-rank options:1]]
void set_low_rank(double lr_tolerance){
  set_icntl(35, 1); // automatic BLR option
  set_cntl(7, lr_tolerance);
}
// Low-rank options:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Setters][Setters:1]]
// Some setters
  void set_verbose(int verbose){ // 0, 1 or 2
    switch (verbose){
    case 0:
      set_icntl(1, 0);
      set_icntl(2, 0);
      set_icntl(3, 0);
      set_icntl(4, 0);
      break;
    case 1:
      set_icntl(1, 6);
      set_icntl(2, 0);
      set_icntl(3, 0);
      set_icntl(4, 1);
      break;
    case 2:
      set_icntl(1, 6);
      set_icntl(2, 0);
      set_icntl(3, 6);
      set_icntl(4, 3);
      set_icntl(11, 2);
      break;
    default:
      MAPHYSPP_WARNING("Mumps.set_verbose(i): i must be 0, 1 or 2");
      set_icntl(1, 0);
      set_icntl(2, 0);
      set_icntl(3, 0);
      set_icntl(4, 0);
      break;
    }
  }

  void set_debug(bool b = true){
    _debug = b;
  }

  void set_icntl(int id, MUMPS_INT value){
    if(_is_init){
      ICNTL(id) = value;
    }
    else{
      _icntl_params[id] = value;
    }
  }

  void set_cntl(int id, Real value){
    if(_is_init){
      CNTL(id) = value;
    }
    else{
      _cntl_params[id] = value;
    }
  }

}; // class Mumps
// Setters:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Traits][Traits:1]]
// Set traits
template<typename M, typename V>
struct is_solver_direct<Mumps<M, V>> : public std::true_type {};
template<typename M, typename V>
struct is_solver_iterative<Mumps<M, V>> : public std::false_type {};
template<typename M, typename V>
struct is_matrix_free<Mumps<M, V>> : public std::false_type {};

template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct matrix_type<Mumps<Matrix, Vector>> : public std::true_type {
  using type = typename Mumps<Matrix, Vector>::matrix_type;
};

template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct vector_type<Mumps<Matrix, Vector>> : public std::true_type {
  using type = typename Mumps<Matrix, Vector>::vector_type;
};

template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct scalar_type<Mumps<Matrix, Vector>> : public std::true_type {
  using type = typename Mumps<Matrix, Vector>::scalar_type;
};
// Traits:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
