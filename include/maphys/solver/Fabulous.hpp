// [[file:../../../org/maphys/solver/Fabulous.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <type_traits>

#include <lapack.hh>
#include <fabulous.hpp>
#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/part_data/PartMatrix.hpp"
#include "maphys/solver/LinearOperator.hpp"

namespace maphys {
template<MPH_Matrix Matrix, MPH_Vector_Single_Or_Multiple Vector, class Precond = Identity<Matrix, Vector>,
         typename FabAlgo = fabulous::bgmres::ARNOLDI_IBDR,
         typename FabDefRestart = fabulous::ClassicRestart>
class Fabulous;
}

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Parameters][Parameters:1]]
namespace fabulous_params{

CREATE_STRUCT(A)
CREATE_STRUCT(tolerances)
CREATE_STRUCT(max_iter)
CREATE_STRUCT(verbose)
CREATE_STRUCT(max_krylov_space)
CREATE_STRUCT(ortho_type)
CREATE_STRUCT(ortho_scheme)
CREATE_STRUCT(algo)
CREATE_STRUCT(deflated_restart)
CREATE_STRUCT(max_kept_direction)
CREATE_STRUCT(Uk)
CREATE_STRUCT(Uk_const)

} // end namespace fabulous
// Parameters:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Conversion][Conversion:1]]
// Convert without copying
template<MPH_Scalar Scalar, int NbCol = -1>
fabulous::Block<Scalar> convert_to_fabulous(DenseMatrix<Scalar, NbCol>& A){
  return fabulous::Block<Scalar>{static_cast<int>(A.get_n_rows()), static_cast<int>(A.get_n_cols()),
	  static_cast<int>(A.get_leading_dim()), A.get_ptr()};
}
template<MPH_Scalar Scalar, int NbCol = -1>
const fabulous::Block<Scalar> convert_to_fabulous(const DenseMatrix<Scalar, NbCol>& A){
  return fabulous::Block<Scalar>(static_cast<int>(A.get_n_rows()), static_cast<int>(A.get_n_cols()),
	  static_cast<int>(A.get_leading_dim()), (Scalar*)A.get_ptr());
}
template<MPH_Scalar Scalar, int NbCol = -1>
DenseMatrix<Scalar, NbCol> convert_to_mpp(fabulous::Block<Scalar>& A){
  return DenseMatrix<Scalar, NbCol>(static_cast<Size>(A.get_nb_row()), static_cast<Size>(A.get_nb_col()), const_cast<Scalar*>(A.get_ptr()), true);
}
template<MPH_Scalar Scalar, int NbCol = -1>
const DenseMatrix<Scalar, NbCol> convert_to_mpp(const fabulous::Block<Scalar>& A){
  return DenseMatrix<Scalar, NbCol>(static_cast<Size>(A.get_nb_row()), static_cast<Size>(A.get_nb_col()), const_cast<Scalar*>(A.get_ptr()), true);
}
// Conversion:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Product operator][Product operator:1]]
// Matrix multiplication
template<MPH_Scalar Scalar, int NbCol, int NbCol2>
inline DenseMatrix<Scalar, NbCol2> operator* (DenseMatrix<Scalar>& A, const fabulous::Block<Scalar>& B){
  return A * convert_to_mpp(B);
}
template<MPH_Scalar Scalar, int NbCol, int NbCol2>
inline DenseMatrix<Scalar, NbCol2> operator* (const DenseMatrix<Scalar, NbCol>& A, const fabulous::Block<Scalar>& B){
  return A * convert_to_mpp(B);
}
// Product operator:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Attributes][Attributes:1]]
template<typename Vect>
  class FabulousPartVector: public PartVector<Vect> {

  private:
    using Vector = PartVector<Vect>;
    using P = typename Vect::real_type;
    using S = typename Vect::scalar_type;
  public:
    using value_type = typename Vect::scalar_type;
    using primary_type = typename Vect::real_type;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Constructors][Constructors:1]]
// Fabulous needed constructor
explicit FabulousPartVector(): Vector() {}
explicit FabulousPartVector(FabulousPartVector &ref, const int n): Vector(ref, static_cast<Size>(n)) {}
explicit FabulousPartVector(const FabulousPartVector &ref, const int n): Vector(ref, static_cast<Size>(n)) {}

// Copy constructor
explicit FabulousPartVector(Vector &ref): Vector(ref) {}
explicit FabulousPartVector(const Vector &ref): Vector(ref) {}

// move constructor
explicit FabulousPartVector(Vector &&ref): Vector(std::move(ref)) {}
explicit FabulousPartVector(const Vector &&ref): Vector(std::move(ref)) {}
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Fonctions and operators][Fonctions and operators:1]]
FabulousPartVector& operator=(const Vector& ref){
  Vector * th = this;
  (*th) = ref;
  return *this;
}

/*bool operator==(const FabulousPartVector& ref) const {
  const Vector* V = this;
  const Vector* refV = &ref; // upcast
  return (*V == *refV);
  }*/

void copy(const FabulousPartVector &ref){
  (*this) = ref;
}

FabulousPartVector copy(void) const {
  FabulousPartVector cp{};
  cp.copy(*this);
  return cp;
}
void zero(void){
  (*this) *= 0;
}
void cwise_axpy(FabulousPartVector &X, S a = S{1.0}){
  (*this) += a * X;
}
void axpy(const FabulousPartVector &X, const fabulous::Block<S> &Y, S a = S{1.0}){
  if(Y.get_nb_row() == 0) return; // FIXME
  (*this) += a * (X * Y);
}
void axpy(const FabulousPartVector &X, const S &Y, S a = S{1.0}){
  (*this) += (a * Y) * X;
}

void dot(const FabulousPartVector &X, fabulous::Block<S> &Y) const {
  if(Y.get_nb_row() == 0) return; // FIXME
  MAPHYSPP_DIM_ASSERT( this->get_n_rows(), X.get_n_rows(), "Matrix dot() different row numbers");
  const Vector *Xv = &X; // upcast
  convert_to_mpp(Y) = this->distdot_block(*Xv);
}
void dot(const FabulousPartVector &X, S &Y){
  MAPHYSPP_DIM_ASSERT( this->get_n_cols(), 1, "Scalar dot col numbers != 1");
  MAPHYSPP_DIM_ASSERT( X.get_n_cols(), 1, "Scalar dot col numbers != 1");
  const Vector *Xv = &X; // upcast
  Y = this->distdot(*Xv);
}
void qr(FabulousPartVector &q, fabulous::Block<S> &r) {
  if (q != *this) {
    q.copy(*this);
  }
  Vector q0 = this->get_vect_view(0);
  auto norm = q0.cwise_norm();
  r(0, 0) = norm[0];// q.display();
  q0 /= r(0,0);
  for(int j = 1; j < r.get_nb_col(); j++){
    Vector qj = this->get_vect_view(j);
    Vector aj = this->get_vect(j);
    Vector Qj = this->get_vect_view(0, j);
    Vect Cj = Qj.distdot_block(aj);
    fabulous::Block<S> Rj = r.sub_block(0, j, j, 1);
    Rj.copy(convert_to_fabulous(Cj));
    for(int i = 0; i < Rj.get_nb_row(); i++) {
      qj -= Rj(i, 0) * this->get_vect_view(i);
    }
    auto normqj = qj.cwise_norm();
    r(j, j) = normqj[0];
    qj /= r(j, j);
  }
}
void trsm(FabulousPartVector &q, fabulous::Block<S> &r) {
  if (q != *this) {
    q.copy(*this);
  }
  auto trsm_data = [&] (Vect& data) { fabulous::Block<S> qb = convert_to_fabulous(data); qb.trsm(qb, r); };
  this->apply_on_data(trsm_data);
}
FabulousPartVector sub_vector(const int offset, const int nb_vect) const {
  return (FabulousPartVector)this->get_vect_view(offset, nb_vect);
}
FabulousPartVector get_bvect(const int offset) const {
  return (FabulousPartVector)this->get_vect_view(offset, 1);
}
int get_nb_local_row() const{
  int nb_row = 0;
  for(auto& my_id : this->get_sd_ids()){
    nb_row += this->get_local_vector(my_id).get_n_rows();
  }
  return nb_row;
}
}; // class FabulousPartVector
// Fonctions and operators:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Out of class functions][Out of class functions:1]]
template<typename Vect, typename P = typename Vect::real_type>
P snorm(const FabulousPartVector<Vect> &ref){
  MAPHYSPP_ASSERT( ref.get_n_cols() == 1 , "snorm() more than 1 col");
  auto norm = ref.cwise_norm();
  return norm[0];
}
template<typename Vect, typename S = typename Vect::scalar_type>
void sscale(FabulousPartVector<Vect> &ref, S a){
  ref *= a;
}
// Convert into a copy
template<typename Vect>
FabulousPartVector<Vect> convert_to_fabulous(PartVector<Vect>& A){
  return FabulousPartVector<Vect>(A);
}
template<typename Vect>
const FabulousPartVector<Vect> convert_to_fabulous(const PartVector<Vect>& A){
  return FabulousPartVector<Vect>(A);
}
template<typename Vect>
PartVector<Vect> convert_to_mpp(FabulousPartVector<Vect>& A){
  PartVector<Vect>* V = &A; // upcast
  return PartVector<Vect>(*V);
}
template<typename Vect>
PartVector<Vect> convert_to_mpp(const FabulousPartVector<Vect>& A){
  const PartVector<Vect>* V = &A; // upcast
  return PartVector<Vect>(*V);
}
// Out of class functions:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Out of class operators][Out of class operators:1]]
// Matrix multiplication
template<typename Vect, MPH_Scalar Scalar = typename Vect::scalar_type>
PartVector<Vect> operator* (const PartVector<Vect>& A, const fabulous::Block<Scalar>& B){
  return A * convert_to_mpp(B);
}
template<typename Vect, typename Matrix>
FabulousPartVector<Vect> operator* (const PartMatrix<Matrix>& A, const FabulousPartVector<Vect>& B){
  const PartVector<Vect> *B_mpp = &B; // upcast
  return FabulousPartVector<Vect>(A * (*B_mpp));
}
// Out of class operators:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Attributes][Attributes:1]]
template<MPH_Matrix Matrix, MPH_Vector_Single_Or_Multiple Vector, class Precond,
	   typename FabAlgo /*= fabulous::bgmres::ARNOLDI_IBDR*/,
	   typename FabDefRestart /*= fabulous::ClassicRestart*/>
  class Fabulous :
      public LinearOperator<Matrix, Vector>{

  private:
    using Scalar = typename Vector::scalar_type;
    using Real = typename arithmetic_real<Scalar>::type;
    using Block = fabulous::Block<Scalar>;
    using FVector = typename std::conditional<is_distributed<Vector>::value, FabulousPartVector<typename Vector::local_type>, Block>::type;
    static_assert(!is_distributed<Vector>::value || !std::is_same<FabAlgo, fabulous::bgcr::ARNOLDI>::value,
		  "Fabulous::BGCR can not be called with custom C++ vectors");
    static_assert(!is_distributed<Vector>::value || !std::is_same<FabAlgo, fabulous::bcg::BCG_ITER2>::value,
		  "Fabulous::BCG::v2 can not be called with custom C++ vectors");

  public:
    using value_type = Scalar;
    using real_type = Real;
    static constexpr const bool use_preconditioner = !std::is_same_v<Precond, Identity<Matrix, Vector>>;

  private:
    static const int _default_max_iter = 1000;
    static const int _default_max_space = 100;

    // Input
    const Matrix * _A = nullptr; // Input matrix
    Precond _M; // Preconditioner
    bool _is_precond_setup = false;
    bool _verbose = false;
    int _max_iter = -1; // Unset
    int _max_krylov_space = -1;
    int _max_kept_direction = 0; // Cumputational blocking
    std::vector<Real> _tolerances;
    fabulous::OrthoType _ortho_type = fabulous::OrthoType::RUHE;
    fabulous::OrthoScheme _ortho_scheme = fabulous::OrthoScheme::MGS;
    fabulous::Parameters _parameters;
    // Need a pointer because FabDefRestart may not be default constructible
    std::unique_ptr<FabDefRestart> _fab_restart_ptr;
    FabAlgo _fab_algo;
    // Saving last solve info
    fabulous::Logger _last_log;
    FVector _Uk{}; // Deflation space of GCRO
    bool _Uk_const = false; // can _Uk be considered constant

    // Output
    int _n_iter = 0;
    std::vector<Real> _residuals;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Nested classes][Nested classes:1]]
struct FabMatrix
{
  const Matrix * _fab_A;

  using value_type = Scalar;
  using primary_type = Real;

  explicit FabMatrix(const Matrix& A): _fab_A{&A} {}

  /* --------------- methods for fabulous core: --------------------- */

  int size() const { return static_cast<int>(n_rows(*_fab_A)); }

  // B := alpha * A * X + beta * B
  int64_t operator()(const FVector &X, FVector &B, Scalar alpha=Scalar{1.0}, Scalar beta=Scalar{0.0}) const{
    int M = B.get_nb_row();
    int N = B.get_nb_col();
    int K = X.get_nb_row();

    B *= beta;
    if constexpr(std::is_same<FVector, Block>::value){
	    Vector AX = (*_fab_A) * convert_to_mpp(X);
	    convert_to_mpp(B) += alpha * AX;
    } else {
	    FVector AX = (*_fab_A) * X;
	    AX *= alpha;
	    B += AX;
    }

    return fabulous::lapacke::flops::gemm<Scalar>(M, N, K);
  }
}; // end class FabMatrix
// Nested classes:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Nested classes][Nested classes:2]]
struct FabPrecond
{

  using value_type = Scalar;
  using primary_type = Real;

  Precond& _fab_M;

  FabPrecond(Precond& M): _fab_M{M} {} // M Not setup preconditionner

  template<typename Mat> void setup(const Mat &A){ _fab_M.setup(A); }

  operator bool() const { return true; }

  int64_t operator()(const FVector &B, FVector &X) const{

    if constexpr(use_preconditioner){
	    if constexpr(std::is_same_v<FVector, Block>){
	      convert_to_mpp(X) = _fab_M.apply(convert_to_mpp(B));
      } else {
        X = _fab_M.apply(B);
      }
    }

    return 0;
  }
}; // end class FabPrecond
// Nested classes:2 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Fabulous solve][Fabulous solve:1]]
template<class Equation, class Algo, class Parameters, class OrthoParam, class RestartParam>
fabulous::Logger fabulous_get_solve(Equation &eq, Algo algo, Parameters param, OrthoParam ortho, RestartParam restart_param){

  if constexpr (std::is_same<Algo, fabulous::bcg::BCG_ITER>::value
                || std::is_same<Algo, fabulous::bcg::BCG_ITER2>::value){/* BCG */
    return fabulous::bcg::solve(eq, algo, param);
  }else if constexpr (std::is_same<Algo, fabulous::bgcr::ARNOLDI>::value){/* BGCR */
    return fabulous::bgcr::solve(eq, algo, param, ortho);
  } else if constexpr (std::is_same<Algo, fabulous::bgcro::ARNOLDI>::value){ /* BGCRO */
    return fabulous::bgcro::solve(eq, algo, param, _Uk, ortho, restart_param, _Uk_const);
  } else { /* BGMRES */
    return fabulous::bgmres::solve(eq, algo, param, ortho, restart_param);
  }
}

int fabulous_solve(const Vector& B, Vector& X){
  MAPHYSPP_ASSERT( B.get_n_rows() == X.get_n_rows() && B.get_n_cols() == X.get_n_cols(), "maphys::Fabulous X and B wrong dimentions");
  MAPHYSPP_ASSERT( B.get_n_cols() == 1 || !is_distributed<Vector>::value, "maphys::Fabulous does not not support parallel vector with multiple right-hand-side yet");
  int nrhs = B.get_n_cols();
  int n_tolerances = static_cast<int>(_tolerances.size());
  MAPHYSPP_ASSERT( n_tolerances == 1 || n_tolerances == nrhs, "maphys::Fabulous Wrong number of tolerances");

  if(_max_iter <= 0){
    MAPHYSPP_WARNING(std::string("maphys::Fabulous Max iter unset, setting to default value ") + std::to_string(_default_max_iter));
    _max_iter = _default_max_iter;
  }
  if(_max_krylov_space <= 0){
    _max_krylov_space = _default_max_space;
  }

  _parameters.max_mvp = _max_iter;
  _parameters.max_kept_direction = _max_kept_direction;
  _parameters.max_space = _max_krylov_space;
  _parameters.quiet = !_verbose;
  auto ortho = _ortho_type + _ortho_scheme;

  // Conversion to fabulous compatible formats
  FVector fab_B = convert_to_fabulous(B);
  FVector fab_X = convert_to_fabulous(X);
  const FabMatrix fab_A(*_A);

  fabulous::noop_placeholder _;
  // No precond
  if(_fab_restart_ptr == nullptr){
    if constexpr(std::is_default_constructible<FabDefRestart>::value){
      _fab_restart_ptr = std::make_unique<FabDefRestart>();
    }
    else{
      MAPHYSPP_ASSERT(_fab_restart_ptr != nullptr,
                      "FabDefRestart is not default constructible and must be set with.setup(fabulous_params::deflated_restart{...})");
    }
  }
  if(_tolerances.size() == 0) _tolerances.push_back(Real{1e-6});
  if constexpr(!use_preconditioner){
    auto eq = fabulous::equation(fab_A, _, _, fab_X, fab_B, _tolerances);
    _last_log = fabulous_get_solve(eq, _fab_algo, _parameters, ortho, *_fab_restart_ptr);
  }
  else { //Precond
    FabPrecond fab_M(_M);
    if(!_is_precond_setup){
      fab_M.setup(*_A);
      _is_precond_setup = true;
    }
    auto eq = fabulous::equation(fab_A, fab_M, _, fab_X, fab_B, _tolerances);
    _last_log = fabulous_get_solve(eq, _fab_algo, _parameters, ortho, *_fab_restart_ptr);
  }
  X = convert_to_mpp(fab_X);
  return _last_log.get_nb_mvp();
}
// Fabulous solve:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Setup][Setup:1]]
public:

  Fabulous(void) {}
  Fabulous(const Matrix& A, bool verb=false): _A{&A}, _verbose{verb} {}
  Fabulous(const Matrix& A, const Matrix& M, bool verb=false): _A{&A}, _M{M}, _verbose{verb} {}

  void setup(const Matrix& A){ _A = &A; }

private:
  void _setup(const fabulous_params::A<Matrix>&                     v) { _A  = &(v.value); }
  void _setup(const fabulous_params::A<const Matrix>&               v) { _A  = &(v.value); }

  void _setup(const fabulous_params::max_iter<int>&           v) { _max_iter   = v.value; }
  void _setup(const fabulous_params::verbose<bool>&           v) { _verbose    = v.value; }
  void _setup(const fabulous_params::max_krylov_space<int>&   v) { _max_krylov_space    = v.value; }
  void _setup(const fabulous_params::max_kept_direction<int>& v) { _max_kept_direction  = v.value; }

  void _setup(const fabulous_params::ortho_type<fabulous::OrthoType>&     v) { _ortho_type    = v.value; }
  void _setup(const fabulous_params::ortho_scheme<fabulous::OrthoScheme>& v) { _ortho_scheme  = v.value; }
  void _setup(const fabulous_params::algo<FabAlgo>&                       v) { _fab_algo      = v.value; }
  void _setup(const fabulous_params::tolerances<std::vector<Real>>&       v) { set_tolerance(v.value); }
  void _setup(const fabulous_params::deflated_restart<FabDefRestart>&     v) { _fab_restart_ptr = std::make_unique<FabDefRestart>(v.value); }

  void _setup(const fabulous_params::Uk<Vector>&     v) { _Uk = FVector(v.value); }
  void _setup(const fabulous_params::Uk_const<bool>& v) { _Uk_const = v.value; }

  void setup () {}
public:

  // Variadic template -> each parameter is a call to the _setup function
  template <typename... Types>
  void setup(const Types&... args) noexcept {
    ( void(_setup(args)), ...);
  }

  template<typename... Types>
  void setup(const Matrix& A, const Types&... args){
    _A = &A;
    setup(args...);
  }
// Setup:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Setters and getters][Setters and getters:1]]
// Getters
inline int get_n_iter() const { return _n_iter; }
inline fabulous::Logger get_last_log(void) const { return _last_log; }
//inline int get_residuals() { return _residuals; }

// Setters
inline void set_verbose(const bool verbose){ _verbose = verbose; }
void set_max_iter(const int max_iter){
  MAPHYSPP_ASSERT( max_iter > 0, "CG's max_iter must be strictly positive.");
  _max_iter = max_iter;
}
void set_tolerance(const std::vector<Real>& tolerances){
  auto n_tolerances = static_cast<int>(tolerances.size());
  MAPHYSPP_ASSERT( n_tolerances > 0, "No tolerances given");
  for (auto& t : tolerances) {
    MAPHYSPP_ASSERT( t > 0, "Fabulous's tolerance must be strictly positive.");
  }
  _tolerances = tolerances;
}
void set_ortho_type(const fabulous::OrthoType& O){
  _ortho_type = O;
}
void set_ortho_scheme(const fabulous::OrthoScheme& O){
  _ortho_scheme = O;
}
Vector get_Uk(void) const {
  return convert_to_mpp(_Uk).copy();
}
// Setters and getters:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Apply][Apply:1]]
void apply(const Vector& B, Vector& X) {
  MAPHYSPP_ASSERT( _A != nullptr, "maphys::Fabulous A is nullptr! Solver cannot solve if a matrix A has not been set with .setup(A) first!");
  _n_iter = fabulous_solve(B, X);
}

Vector apply(const Vector& B){
  if constexpr(std::is_same<FVector, Block>::value){
    Vector X(B.get_n_rows(), B.get_n_cols()); // Create X same size as B
    apply(B, X);
    return X;
  } else {
    Vector X(B, B.get_n_cols()); // Create X same size as B
    apply(B, X);
    return X;
  }
}
void solve(const Vector& B, Vector& X){ apply(B, X); }
Vector solve(const Vector& B){ return apply(B); }
// Apply:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Display][Display:1]]
void display(const std::string name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << "Solver name: " << name << '\n';
  out << "Conjugate gradient solver:" << '\n';
  out << "  - On matrix A at : " << _A << '\n';
  //out << "  - On arithmetic: " << arithmetic_name<Scalar>::name << '\n';
  out << "  - Max iter: " << _max_iter << '\n';
  out << "  - Tolerances: ";
  for (auto& t : _tolerances) out << t << ", ";
  out << '\n';
  out << "  - Niter performed last solve: " << _n_iter << '\n';
  out << std::boolalpha;
  out << "  - Verbose: " << _verbose << '\n';
  out << "  - Use preconditioner: " << use_preconditioner << '\n' << '\n';
}
// Display:1 ends here

// [[file:../../../org/maphys/solver/Fabulous.org::*Footer][Footer:1]]
}; //class Fabulous
} //namespace maphys
// Footer:1 ends here
