// [[file:../../../org/maphys/solver/PartSchurSolver.org::*Class][Class:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <string>
#include <type_traits>
#include <cmath>
#include <map>

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/part_data/PartOperator.hpp"
#include "maphys/solver/SchurSolver.hpp"

namespace maphys {

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SolverK, typename SolverS>
class PartSchurSolver:
    public SchurSolverBase<Matrix, Vector, SolverK, SolverS>{

private:
  using BaseT = SchurSolverBase<Matrix, Vector, SolverK, SolverS>;
  using SchurOperator = typename BaseT::schur_type;
  using BaseT::_S;
  using BaseT::_setup_solver_K;

  using SchurOperatorLoc = typename SchurOperator::local_type;
  using MatrixLoc = typename Matrix::local_type;
  using VectorLoc = typename Vector::local_type;

  constexpr const static bool is_implicit = std::is_same<ImplicitSchur<MatrixLoc,VectorLoc,SolverK>,SchurOperatorLoc>::value;

  PartOperator<SolverK,MatrixLoc,VectorLoc> _solver_K;
  // Keep a valid pointer to the explicit Schur if needed for Preconditioner

  std::enable_if_t<is_solver_iterative<SolverS>::value>
  setup_precond(){
    using Precond = typename SolverS::precond_type;
    using SchurPcdType = typename Precond::matrix_type;
    using SchurPcdVectType = typename Precond::vector_type;
    if constexpr(!std::is_same<Precond, Identity<SchurPcdType, SchurPcdVectType>>::value){
      using SchurPcdTypeLoc = typename SchurPcdType::local_type;

      auto fct_setup_pcd = [&](const SchurOperator& Simpl, Precond& M){
                             SchurPcdType pcd_S(_S->get_proc(), /*on_interface*/true);

                             auto get_expl_schur = [](SchurPcdTypeLoc& out_S, const SchurOperatorLoc& loc_impl_S){
                                                     out_S = const_cast<SchurOperatorLoc&>(loc_impl_S).get_dense_schur();
                                                   };

                             pcd_S.template apply_on_data<SchurOperatorLoc>(Simpl, get_expl_schur);
                             M.setup(pcd_S);
                           };
      BaseT::_solver_S.setup(parameters::setup_pcd<std::function<void(const SchurOperator&, Precond&)>>{fct_setup_pcd});
    }
  }

  void get_schur_complement(const Matrix& K){
    Timer<TIMER_SOLVER> t("Compute Schur");

    std::shared_ptr<Process> proc = K.get_proc();
    _S = std::make_unique<SchurOperator>(proc, /*on_interface*/ true);

    if constexpr(is_implicit){
        // Set matrix
        _S->setup(K);
        // Set schurlist
        std::map<int, IndexArray<int>> interfaces = proc->get_interface();
        auto fct_schur_impl = [&](int sd_id, SchurOperatorLoc& is){
                                is.setup_schurlist(interfaces[sd_id]);
                                is.split_matrix();
                                _setup_solver_K(is.get_solver_K());
                              };
        _S->apply_on_data_id(fct_schur_impl);
        if constexpr(is_solver_iterative<SolverS>::value) setup_precond();
      }
    else{
      _solver_K.initialize(proc);
      _solver_K.setup(K);
      auto fct_schur = [&](const int sd_id, SchurOperatorLoc& Sloc, SolverK& solver){
                         _setup_solver_K(solver);
                         IndexArray<int> intrf = proc->get_interface(sd_id);
                         Sloc = solver.get_schur(intrf);
                       };
      _S->template apply_on_data_id<SolverK>(_solver_K, fct_schur);
    }

  //_S-> display("Schur");
  }

  Vector get_schur_rhs(const Vector& B){
    Timer<TIMER_SOLVER> t("PartSchurSolver b2f");
    Vector F(B.get_proc(), /*on_interface*/ true);
    Vector B_dis = B;
    B_dis.disassemble();

    if constexpr(is_implicit){
      auto fct_rhs_impl = [](SchurOperatorLoc& Sloc, const VectorLoc& Bloc){ return Sloc.compute_rhs(Bloc); };
      _S->template apply_on_data<VectorLoc,VectorLoc>(B_dis, F, fct_rhs_impl);
    }
    else{
      auto fct_rhs = [](SolverK& solver, const VectorLoc& Bloc){ return solver.b2f(Bloc); };
      _solver_K.template apply_on_data<VectorLoc,VectorLoc>(B_dis, F, fct_rhs);
    }
    F.assemble();
    return F;
  }

  void get_schur_solution(const Vector& Ug, Vector& U){
    Timer<TIMER_SOLVER> t("PartSchurSolver y2x");

    if constexpr(is_implicit){
        auto fct_sol_impl = [](SchurOperatorLoc& Sloc, const VectorLoc& UgLoc){ return Sloc.get_solution(UgLoc); };
        _S->template apply_on_data<VectorLoc,VectorLoc>(Ug, U, fct_sol_impl);
      }
    else{
      auto fct_sol = [](SolverK& solver, const VectorLoc& Ugloc){ return solver.y2x(Ugloc); };
      _solver_K.template apply_on_data<VectorLoc,VectorLoc>(Ug, U, fct_sol);
    }
    U.set_on_intrf(false);
  }

public:

  [[nodiscard]] PartOperator<SolverK,MatrixLoc,VectorLoc>& get_solver_K() { return _solver_K; }

}; // class PartSchurSolver

template<typename Matrix, typename Vector, typename SolverK, typename SolverS>
struct is_matrix_free<PartSchurSolver<Matrix, Vector, SolverK, SolverS>> : public std::false_type {};

} // namespace maphys
// Class:1 ends here
