// [[file:../../../org/maphys/solver/BiCGSTAB.org::*Header][Header:1]]
#pragma once

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/solver/IterativeSolver.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/solver/BiCGSTAB.org::*Base class][Base class:1]]
template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond = Identity<Matrix, Vector>>
class BiCGSTAB :
    public IterativeSolver<Matrix, Vector, Precond> {
// Base class:1 ends here

// [[file:../../../org/maphys/solver/BiCGSTAB.org::*Attributes][Attributes:1]]
private:
  using Scalar = typename IterativeSolver<Matrix, Vector, Precond>::scalar_type;
  using Real = typename IterativeSolver<Matrix, Vector, Precond>::real_type;
  using IterativeSolver<Matrix, Vector, Precond>::_A;
  using IterativeSolver<Matrix, Vector, Precond>::_B;
  using IterativeSolver<Matrix, Vector, Precond>::_X;
  using IterativeSolver<Matrix, Vector, Precond>::_R;
  using IterativeSolver<Matrix, Vector, Precond>::_M;
  using IterativeSolver<Matrix, Vector, Precond>::_dot;
  using IterativeSolver<Matrix, Vector, Precond>::_squared;
  using IterativeSolver<Matrix, Vector, Precond>::_tol_sq;
  using IterativeSolver<Matrix, Vector, Precond>::_max_iter;
  using IterativeSolver<Matrix, Vector, Precond>::_check_true_residual;
  using IterativeSolver<Matrix, Vector, Precond>::_always_true_residual;
  using IterativeSolver<Matrix, Vector, Precond>::_residual_sq;
  using IterativeSolver<Matrix, Vector, Precond>::_verbose;
  using IterativeSolver<Matrix, Vector, Precond>::_convergence_achieved;

public:
  using scalar_type = Scalar;
  using vector_type = Vector;
  using matrix_type = Matrix;
  using real_type = Real;
// Attributes:1 ends here

// [[file:../../../org/maphys/solver/BiCGSTAB.org::*C++ code][C++ code:1]]
int iterative_solve() override {

  Timer<TIMER_ITERATION> t0("BiCGSTAB iteration 0");
  const Matrix& A = *_A;
  const Vector& B = *_B;
  Vector& X = *_X;
  Vector& R = _R;

  this->setup_initial_stop_crit("BiCGSTAB");

  R = B - A * X;
  if(_convergence_achieved()) return 0;

  Vector R_hat = R;

  Vector P  = R * Scalar{0};
  Vector NU = P;
  Vector H  = P;
  Vector S  = P;
  Vector T  = P;
  Vector Z  = P;
  Vector MT = P;
  Vector Y  = P;

  Scalar rho = 1;
  Scalar alpha = 1;
  Scalar omega = 1;

  t0.stop();

  for(int iter = 1; iter < _max_iter + 1; ++iter){
    Timer<TIMER_ITERATION> t("BiCGSTAB iteration");
    if(_verbose) std::cout << iter << " -\t";

    Scalar rho_old = rho;
    rho = _dot(R_hat, R);

    Scalar beta = (rho * alpha) / ( rho_old * omega);
    P = R + beta * (P - omega * NU);
    Y = _M * P;
    NU = A * Y;
    alpha = rho / _dot(R_hat, NU);
    H = X + alpha * Y;
    S = R - alpha * NU;
    Z = _M * S;
    T = A * Z;
    MT = _M * T;
    omega = _dot( MT, _M * S)/ _squared(MT);
    X = H + omega * Z;
    R = S - omega * T;

    if(_convergence_achieved()) return iter;
  }

  return -1;
}
// C++ code:1 ends here

// [[file:../../../org/maphys/solver/BiCGSTAB.org::*Constructors][Constructors:1]]
public:

  BiCGSTAB(): IterativeSolver<Matrix, Vector, Precond>() {}

  BiCGSTAB(const Matrix& A, bool verb=false):
    IterativeSolver<Matrix, Vector, Precond>(A, verb) {}
}; //class BiCGSTAB
// Constructors:1 ends here

// [[file:../../../org/maphys/solver/BiCGSTAB.org::*Traits][Traits:1]]
// Set traits
template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_solver_direct<BiCGSTAB<Matrix, Vector, Precond>> : public std::false_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_solver_iterative<BiCGSTAB<Matrix, Vector, Precond>> : public std::true_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_matrix_free<BiCGSTAB<Matrix, Vector, Precond>> : public std::true_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct vector_type<BiCGSTAB<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename BiCGSTAB<Matrix, Vector, Precond>::vector_type;
};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct scalar_type<BiCGSTAB<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename BiCGSTAB<Matrix, Vector, Precond>::scalar_type;
};
// Traits:1 ends here

// [[file:../../../org/maphys/solver/BiCGSTAB.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
