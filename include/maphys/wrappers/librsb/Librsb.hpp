// [[file:../../../../org/maphys/wrappers/librsb/Librsb.org::*Alias][Alias:1]]
#pragma once


#include <rsb.hpp>
#include <maphys.hpp>
#include <maphys/loc_data/SparseMatrixCOO.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>

namespace maphys {
// Alias:1 ends here

// [[file:../../../../org/maphys/wrappers/librsb/Librsb.org::*Constructor / FIXME][Constructor // FIXME:1]]
template<MPH_Scalar Scalar>
struct RsbSparseMatrix : public rsb::RsbMatrix<Scalar>, public MatrixProperties<Scalar>{
  using RsbMat = rsb::RsbMatrix<Scalar>;
public:
  using index_type = int;

public:

  RsbSparseMatrix(): RsbMat(nullptr,nullptr,nullptr,0) {}

  explicit RsbSparseMatrix(const Size m, const Size n, const Size /*nnz*/ /*, const rsb::RsbSym sym = IsGen*/): RsbMat(m, n/*, sym*/) {}

  template<MPH_Integral Index>
  explicit RsbSparseMatrix(const Size, const Size, const Size nnz, const IndexArray<Index>& i, const IndexArray<Index>& j, const IndexArray<Scalar>& v, rsb_flags_t flags = RSB_FLAG_NOFLAGS):
    RsbMat(i.get_data_ptr(), j.get_data_ptr(), v.get_data_ptr(), nnz, flags) {}

  template<MPH_Integral Index>
  explicit RsbSparseMatrix(const Index* i, const Index* j, const Scalar* v, const Size nnz, rsb_flags_t flags = RSB_FLAG_NOFLAGS):
    RsbMat(i, j, v, nnz, flags) {}


  template<MPH_Integral Index>
  explicit RsbSparseMatrix(std::initializer_list<Index> i, std::initializer_list<Index> j,
			   std::initializer_list<Scalar> v, const Size, const Size):
    RsbMat(i.begin(), j.begin(), v.begin(), v.size(), RSB_FLAG_NOFLAGS){}

  // Possible ?
  //RsbSparseMatrix(const Size m, const Size n, const Size nnz, Idx_arr&& i, Idx_arr&& j, Scal_arr&& v):
  //  _m{m}, _n{n}, _nnz{nnz}, _i{std::move(i)}, _j{std::move(j)}, _v{std::move(v)} {}

  // Copy constructor
  RsbSparseMatrix(const RsbSparseMatrix& spmat): RsbMat(spmat) {}
  // nothing more to do if already exist for RsbMatrix class

  // Move constructor
  RsbSparseMatrix(RsbSparseMatrix&& spmat): RsbMat(spmat) {}
  // nothing more to do if already exist for RsbMatrix class

  // Copy
  //RsbSparseMatrix& operator= (const RsbSparseMatrix& copy)
  // nothing to do if already exist for RsbMatrix class

  // Move assignment operator
  RsbSparseMatrix& operator= (RsbSparseMatrix&& copy) = default;

  // load from a file
  //RsbSparseMatrix(const rsb_char_t *filename, const RsbSym sym = IsGen): rsb::RsbMatrix(filename, sym) {}

  // convert functions
  RsbSparseMatrix(const RsbMat &A_Rsb, bool do_trans = false, rsb_flags_t flagsA = RSB_FLAG_NOFLAGS): RsbMat(A_Rsb, do_trans, flagsA) {}

  RsbSparseMatrix(RsbMat &&other): RsbMat(other) {}
// Constructor // FIXME:1 ends here

// [[file:../../../../org/maphys/wrappers/librsb/Librsb.org::*Element access][Element access:1]]
Scalar& operator()(const int, const int){
  Scalar& v = Scalar{0.0};
  MAPHYSPP_ASSERT(false, "Operation not supported");
  return v;
}

Scalar operator()(const int i, const int j) const {
  return this->get_val(i, j);
}
// Element access:1 ends here

// [[file:../../../../org/maphys/wrappers/librsb/Librsb.org::*Getters][Getters:1]]
//void convert(DenseMatrix<Scalar>& dmat) const {
  //  dmat = EigenDenseMatrix<Scalar>(*this);
  //}

  Size get_nb_row() const { return static_cast<Size>(this->rows()); }
  Size get_nb_col() const { return static_cast<Size>(this->cols()); }
private:
  template<typename SpMat, typename Index>
  SpMat _to_other() const {
    const Index nnz = this->nnz();
    std::vector<Index> I(nnz), J(nnz);
    std::vector<Scalar> V(nnz);
    this->get_coo(RSB_TRANSPOSITION_N, V.data(), I.data(), J.data(), this->is_symmetric() ? RSB_FLAG_SYMMETRIC : RSB_FLAG_NOFLAGS);
    IndexArray<Index> iarr(I);
    IndexArray<Index> jarr(J);
    IndexArray<Scalar> varr(V);
    SpMat res (this->rows(), this->cols(), nnz, std::move(iarr), std::move(jarr), std::move(varr));
    res.copy_properties(*this);
    return res;
  }
public:
  template<typename Index = int>
  SparseMatrixCOO<Scalar, Index> to_coo() const {
    return _to_other<SparseMatrixCOO<Scalar, Index>, Index>();
  }

  template<typename Index = int>
  SparseMatrixCSC<Scalar, Index> to_csc() const {
    return _to_other<SparseMatrixCSC<Scalar, Index>, Index>();
  }
}; //struct RsbSparseMatrix

template<MPH_Scalar Scalar>
inline size_t n_nonzero(const RsbSparseMatrix<Scalar>& m){
  return static_cast<size_t>(m.get_info_nnz_t(RSB_MIF_MATRIX_NNZ__TO__RSB_NNZ_INDEX_T));
}

template<MPH_Scalar Scalar>
inline size_t n_rows(const RsbSparseMatrix<Scalar>& m){
  return static_cast<size_t>(m.rows());
}

template<MPH_Scalar Scalar>
inline size_t n_cols(const RsbSparseMatrix<Scalar>& m){
  return static_cast<size_t>(m.cols());
}
// Getters:1 ends here

// [[file:../../../../org/maphys/wrappers/librsb/Librsb.org::*Setters][Setters:1]]
/*void set_spd(MatrixStorage s){
  this->set_spd(s);
  if(this->is_symmetric()){

  }
}*/
// Setters:1 ends here

// [[file:../../../../org/maphys/wrappers/librsb/Librsb.org::*Operator][Operator:1]]
// Not important
//template<MPH_Scalar Scalar>
//inline EigenSparseMatrix<Scalar> adjoint(const EigenSparseMatrix<Scalar>& m){
//  return EigenSparseMatrix<Scalar>(m.adjoint());
//}

// Not important
//template<MPH_Scalar Scalar>
//void display(const EigenSparseMatrix<Scalar>& m, const std::string& name="", std::ostream &out = std::cout){
//  if(!name.empty()) out << name << '\n';
//  out << m;
//  out << '\n';
//}

template<MPH_Scalar Scalar, typename M>
M operator*(const RsbSparseMatrix<Scalar>& spmat, M v_rhs){
  M out(spmat.rows(), n_cols(v_rhs));
  spmat.spmm(RSB_TRANSPOSITION_N, Scalar{1.0}, n_cols(v_rhs),
             RSB_FLAG_WANT_COLUMN_MAJOR_ORDER, v_rhs.get_ptr(), v_rhs.get_leading_dim(),
    	     Scalar{0.0}, out.get_ptr(), v_rhs.get_leading_dim());
  return out;
}
// Operator:1 ends here

// [[file:../../../../org/maphys/wrappers/librsb/Librsb.org::*Conversion][Conversion:1]]
template<MPH_Scalar Scalar, typename Index = int>
RsbSparseMatrix<Scalar> convert(const SparseMatrixCOO<Scalar, Index> m){
  return RsbSparseMatrix(m.get_i_ptr(), m.get_j_ptr(), m.get_v_ptr(), m.get_nnz(), m.is_symmetric() ? RSB_FLAG_SYMMETRIC : RSB_FLAG_NOFLAGS);
}

template<MPH_Scalar Scalar, typename Index = int>
RsbSparseMatrix<Scalar> convert(const SparseMatrixCSC<Scalar, Index> m){
  return RsbSparseMatrix(m.get_i_ptr(), m.get_j_ptr(), m.get_v_ptr(), m.get_nnz(), m.is_symmetric() ? RSB_FLAG_SYMMETRIC : RSB_FLAG_NOFLAGS);
}
// Conversion:1 ends here

// [[file:../../../../org/maphys/wrappers/librsb/Librsb.org::*Traits][Traits:1]]
template<MPH_Scalar Scalar> struct is_sparse<RsbSparseMatrix<Scalar>> : public std::true_type {};

  // Scalar types
  template<MPH_Scalar Scalar> struct scalar_type<RsbSparseMatrix<Scalar>> : public std::true_type {
    using type = Scalar;
  };

  template<MPH_Scalar Scalar> struct vector_type<RsbSparseMatrix<Scalar>> : public std::true_type {
    using type = Vector<Scalar>;
  };
} // maphys
// Traits:1 ends here
