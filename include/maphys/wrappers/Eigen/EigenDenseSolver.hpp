// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Header][Header:1]]
#pragma once

#include "maphys/wrappers/Eigen/Eigen_header.hpp"
#include "Eigen.hpp"

namespace maphys{
  template<typename Matrix, typename Vector>
  class EigenDenseSolver;
}

#include "../../solver/LinearOperator.hpp"
#include <Eigen/Dense>

namespace maphys{
// Header:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Class][Class:1]]
template<typename Matrix, typename Vector>
class EigenDenseSolver :
    public LinearOperator<Matrix, Vector> {
// Class:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Attributes][Attributes:1]]
public:
using matrix_type = Matrix;
using vector_type = Vector;

private:
using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
using Real = typename LinearOperator<Matrix, Vector>::real_type;
using E_DenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

E_DenseMatrix _A;
MatrixProperties<Scalar> _properties;

std::unique_ptr<Eigen::LLT<E_DenseMatrix>> _llt;
std::unique_ptr<Eigen::LDLT<E_DenseMatrix>> _ldlt;
std::unique_ptr<Eigen::PartialPivLU<E_DenseMatrix>> _lu;

bool _is_setup = false;
bool _is_facto = false;
// Attributes:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Constructors][Constructors:1]]
public:
  EigenDenseSolver(){}
  EigenDenseSolver(const Matrix& A){ setup(A); }
  EigenDenseSolver(const Matrix& A, const MatrixProperties<Scalar>& A_properties){ setup(A, A_properties); }
  EigenDenseSolver(Matrix&& A){ setup(std::move(A)); }
  EigenDenseSolver(Matrix&& A, const MatrixProperties<Scalar>& A_properties){ setup(std::move(A, A_properties)); }
// Constructors:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Move operations][Move operations:1]]
EigenDenseSolver(const EigenDenseSolver&) = delete;
EigenDenseSolver& operator=(const EigenDenseSolver&) = delete;

EigenDenseSolver(EigenDenseSolver&&) = default;
EigenDenseSolver& operator=(EigenDenseSolver&&) = default;
// Move operations:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Setup][Setup:1]]
void setup(const Matrix& A){
  _A = A;
  _is_setup = true;
}
void setup(const Matrix& A, const MatrixProperties<Scalar>& A_properties){
  _A = A;
  _is_setup = true;
  _properties = A_properties;
}
void setup(Matrix&& A){
  _A = std::move(A);
  _is_setup = true;
}
void setup(Matrix&& A, const MatrixProperties<Scalar>& A_properties){
  _A = std::move(A);
  _is_setup = true;
  _properties = A_properties;
}
// Setup:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Factorization][Factorization:1]]
void factorize(){
  MAPHYSPP_ASSERT(_is_setup == true, "EigenDenseSolver::factorize: calling factorize before setup(A)");
  Timer<TIMER_SOLVER> t("Dense facto");

  if(_properties.is_spd()){
    _llt = std::make_unique<Eigen::LLT<E_DenseMatrix>>(_A);
    _llt->compute(_A);
  }
  else if(_properties.is_symmetric()){
    _ldlt = std::make_unique<Eigen::LDLT<E_DenseMatrix>>(_A);
    _ldlt->compute(_A); //FIXME
  }
  else{
    _lu = std::make_unique<Eigen::PartialPivLU<E_DenseMatrix>>(_A);
    _lu->compute(_A);
  }
  _is_facto = true;
}
// Factorization:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Solve][Solve:1]]
Vector apply(const Vector& B){
  if(!_is_facto){
    factorize();
  }
  Timer<TIMER_SOLVER> t("Dense solve");
  if(_properties.is_spd()){
    return Vector(_llt->solve(B));
  }
  else if(_properties.is_symmetric()){
    return Vector(_ldlt->solve(B));
  }
  else{
    return Vector(_lu->solve(B));
  }
}
// Solve:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Display][Display:1]]
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << "Dense Eigen lib solver name: " << name << '\n';
  out << "Matrix properties: " << _properties.properties_str() << '\n';
  out << "_is_setup: " << _is_setup << '\n';
  out << "_is_facto: " << _is_facto << '\n';
}
// Display:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Footer][Footer:1]]
}; //class EigenDenseSolver.org
} // namespace maphys
// Footer:1 ends here
