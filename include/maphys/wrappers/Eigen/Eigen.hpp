// [[file:../../../../org/maphys/wrappers/Eigen/Eigen.org::*Header][Header:1]]
#pragma once

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <maphys/interfaces/basic_concepts.hpp>

#include <maphys.hpp>
#include <maphys/loc_data/SparseMatrixCOO.hpp>
#include <maphys/interfaces/linalg_concepts.hpp>

namespace maphys {

  // Some aliasing
  template<MPH_Scalar Scalar>
  using E_Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;

  template<MPH_Scalar Scalar>
  using E_DenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

  template<MPH_Scalar Scalar>
  using E_DiagMatrix = Eigen::DiagonalMatrix<Scalar, Eigen::Dynamic>;

  template<MPH_Scalar Scalar>
  using E_SparseMatrix = Eigen::SparseMatrix<Scalar>;
// Header:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/Eigen.org::*Vector][Vector:1]]
template<MPH_Scalar Scalar>
[[nodiscard]] Scalar dot(const E_Vector<Scalar>& v1, const E_Vector<Scalar>& v2){
  return v1.dot(v2);
}

template<MPH_Scalar Scalar>
[[nodiscard]] size_t size(const E_Vector<Scalar>& v){
  return static_cast<size_t>(v.size());
}

template<MPH_Scalar Scalar>
[[nodiscard]] size_t n_rows(const E_Vector<Scalar>& v){
  return static_cast<size_t>(v.size());
}

template<MPH_Scalar Scalar>
[[nodiscard]] size_t get_leading_dim(const E_Vector<Scalar>& v){
  return static_cast<size_t>(v.size());
}

template<MPH_Scalar Scalar>
[[nodiscard]] Scalar * get_ptr(E_Vector<Scalar>& v){
  return &v[0];
}

template<MPH_Scalar Scalar>
[[nodiscard]] const Scalar * get_ptr(const E_Vector<Scalar>& v){
  return &v[0];
}

template<MPH_Scalar Scalar, int NbCol>
void display(const Eigen::Matrix<Scalar, Eigen::Dynamic, NbCol>& v, const std::string& name="", std::ostream &out = std::cout){
  if(!name.empty()) out << name << '\n';
  out << " (Eigen::Matrix)\n\n" << v;
  out << "\n\n";
}
// Vector:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/Eigen.org::*Diagonal matrix][Diagonal matrix:1]]
template<MPH_Scalar Scalar>
[[nodiscard]] size_t n_rows(const E_DiagMatrix<Scalar>& dm){ return dm.rows(); }

template<MPH_Scalar Scalar>
[[nodiscard]] size_t n_cols(const E_DiagMatrix<Scalar>& dm){ return dm.cols(); }
// Diagonal matrix:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/Eigen.org::*Dense matrix][Dense matrix:1]]
template<MPH_Scalar Scalar>
[[nodiscard]] size_t n_rows(const E_DenseMatrix<Scalar>& m){ return m.rows(); }

template<MPH_Scalar Scalar>
[[nodiscard]] size_t n_cols(const E_DenseMatrix<Scalar>& m){ return m.cols(); }

template<MPH_Scalar Scalar>
[[nodiscard]] size_t get_leading_dim(const E_DenseMatrix<Scalar>& m){ return m.rows(); }

template<MPH_Scalar Scalar>
[[nodiscard]] E_Vector<Scalar> diagonal_as_vector(const E_DenseMatrix<Scalar>& m){
  return m.diagonal();
}
// Dense matrix:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/Eigen.org::*Sparse matrix][Sparse matrix:1]]
template<MPH_Scalar Scalar>
void build_matrix(E_SparseMatrix<Scalar>& mat, const size_t M, const size_t N, const size_t nnz,
		  const int * i, const int * j, const Scalar * v,
		  const bool fill_symmetry =  false){
  mat = Eigen::SparseMatrix<Scalar>(M, N);

  int true_nnz = 0;
  if(fill_symmetry){
    for(size_t k = 0; k < nnz; ++k){
      true_nnz++;
      if(i[k] != j[k]) true_nnz++;
    }
  }
  else{
    true_nnz = nnz;
  }

  std::vector< Eigen::Triplet<Scalar> > tripletList;
  tripletList.reserve(true_nnz);

  for(size_t k = 0; k < nnz; ++k){
    tripletList.push_back( Eigen::Triplet<Scalar>(i[k], j[k], v[k]) );
    if(i[k] != j[k] && fill_symmetry){
      tripletList.push_back( Eigen::Triplet<Scalar>(j[k], i[k], v[k]) );
    }
  }

  mat.setFromTriplets(tripletList.begin(), tripletList.end());
}

// Sparse matrix
template<MPH_Scalar Scalar>
[[nodiscard]] size_t n_rows(const E_SparseMatrix<Scalar>& m){ return m.rows(); }

template<MPH_Scalar Scalar>
[[nodiscard]] size_t n_cols(const E_SparseMatrix<Scalar>& m){ return m.cols(); }

template<MPH_Scalar Scalar>
[[nodiscard]] E_Vector<Scalar> diagonal_as_vector(const E_SparseMatrix<Scalar>& m){
  return m.diagonal();
}
// Sparse matrix:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/Eigen.org::*Traits][Traits:1]]
template<MPH_Scalar Scalar> struct scalar_type<E_Vector<Scalar>> : public std::true_type {
  using type = Scalar;
};

template<MPH_Scalar Scalar> struct vector_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using type = E_Vector<Scalar>;
};

template<MPH_Scalar Scalar> struct sparse_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using type = E_SparseMatrix<Scalar>;
};

template<MPH_Scalar Scalar> struct dense_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using type = E_DenseMatrix<Scalar>;
};

template<MPH_Scalar Scalar> struct diag_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using type = E_DiagMatrix<Scalar>;
};

template<MPH_Scalar Scalar> struct scalar_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using type = Scalar;
};

template<MPH_Scalar Scalar> struct is_dense<E_DenseMatrix<Scalar>> : public std::true_type {};

template<MPH_Scalar Scalar> struct real_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using Real = typename arithmetic_real<Scalar>::type;
  using type = E_DenseMatrix<Real>;
};

template<MPH_Scalar Scalar> struct complex_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using Complex = typename arithmetic_complex<Scalar>::type;
  using type = E_DenseMatrix<Complex>;
};

// Sparse
template<MPH_Scalar Scalar> struct vector_type<E_SparseMatrix<Scalar>> : public std::true_type {
  using type = E_Vector<Scalar>;
};

template<MPH_Scalar Scalar> struct sparse_type<E_SparseMatrix<Scalar>> : public std::true_type {
  using type = E_SparseMatrix<Scalar>;
};

template<MPH_Scalar Scalar> struct dense_type<E_SparseMatrix<Scalar>> : public std::true_type {
  using type = E_DenseMatrix<Scalar>;
};

template<MPH_Scalar Scalar> struct diag_type<E_SparseMatrix<Scalar>> : public std::true_type {
  using type = E_DiagMatrix<Scalar>;
};

template<MPH_Scalar Scalar> struct scalar_type<E_SparseMatrix<Scalar>> : public std::true_type {
  using type = Scalar;
};

template<MPH_Scalar Scalar> struct is_dense<E_SparseMatrix<Scalar>> : public std::false_type {};
// Traits:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/Eigen.org::*Footer][Footer:1]]
} // maphys
// Footer:1 ends here
