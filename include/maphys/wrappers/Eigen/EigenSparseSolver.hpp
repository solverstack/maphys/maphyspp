// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Header][Header:1]]
#pragma once

#include "maphys/wrappers/Eigen/Eigen_header.hpp"
#include "maphys/wrappers/Eigen/Eigen.hpp"
#include "maphys/solver/LinearOperator.hpp"
#include <Eigen/Sparse>
#include <variant>

namespace maphys{
// Header:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Class][Class:1]]
template<typename Matrix, typename Vector>
class EigenSparseSolver :
    public LinearOperator<Matrix, Vector> {
// Class:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Attributes][Attributes:1]]
public:
using matrix_type = Matrix;
using vector_type = Vector;

private:
using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
using Real = typename LinearOperator<Matrix, Vector>::real_type;
using E_SparseMatrix = Eigen::SparseMatrix<Scalar>;
using E_DenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

E_SparseMatrix _A;
MatrixProperties<Scalar> _properties;

using SolverLLT  = Eigen::SimplicialLLT<E_SparseMatrix>;
using SolverLDLT = Eigen::SimplicialLDLT<E_SparseMatrix>;
using SolverLU   = Eigen::SparseLU<E_SparseMatrix>;

std::variant<SolverLLT, SolverLDLT, SolverLU> _solver;

bool _is_setup = false;
bool _is_facto = false;

// Schur complement computation
E_SparseMatrix _A_ig;
E_SparseMatrix _A_gi;

Vector _B_i;
Size _n_schur;
Eigen::PermutationMatrix<Eigen::Dynamic> _permut_schur;

bool _is_schur_setup = false;
bool _is_schur_b2f = false;
// Attributes:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Constructors][Constructors:1]]
public:
EigenSparseSolver(){}
EigenSparseSolver(const Matrix& A){ setup(A); }
EigenSparseSolver(Matrix&& A){ setup(std::move(A)); }
EigenSparseSolver(const Matrix& A, const MatrixProperties<Scalar>& p){ setup(A, p); }
EigenSparseSolver(Matrix&& A, const MatrixProperties<Scalar>& p){ setup(std::move(A), p); }
// Constructors:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Move operations][Move operations:1]]
EigenSparseSolver(const EigenSparseSolver&) = delete;
EigenSparseSolver& operator=(const EigenSparseSolver&) = delete;

EigenSparseSolver(EigenSparseSolver&&) = default;
EigenSparseSolver& operator=(EigenSparseSolver&&) = default;
// Move operations:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Setup][Setup:1]]
void setup(const Matrix& A){
  _A = A;
  _is_setup = true;
}
void setup(const Matrix& A, const MatrixProperties<Scalar>& p){
  _A = A;
  _is_setup = true;
  _properties = p;
}
void setup(Matrix&& A){
  _A = std::move(A);
  _is_setup = true;
}
void setup(Matrix&& A, const MatrixProperties<Scalar>& p){
  _A = std::move(A);
  _is_setup = true;
  _properties = p;
}
// Setup:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Error management][Error management:1]]
void catch_eigen_error(Eigen::ComputationInfo& info, const std::string fct){
  if(info == Eigen::Success) return;
  std::cerr << "EigenSparseSolver::" << fct << " returned with info: ";
  if(info == Eigen::NumericalIssue) std::cerr << "NumericalIssue\n";
  if(info == Eigen::NoConvergence)  std::cerr << "NoConvergence\n";
  if(info == Eigen::InvalidInput)   std::cerr << "InvalidInput\n";
  MAPHYSPP_ASSERT(false, "EigenSparseSolver fatal error");
}
// Error management:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Internal solver operations][Internal solver operations:1]]
void solver_init(E_SparseMatrix& mat){
  // Lower storage only
  if(_properties.is_storage_upper()){
    mat = mat.transpose();
  }
  if(_properties.is_spd()){
    _solver.template emplace<SolverLLT>(mat);
  }
  else if(_properties.is_symmetric()){
    MAPHYSPP_ASSERT(false, "EigenSparseSolver:: LDLT facto is not working");
    _solver.template emplace<SolverLDLT>(mat);
  }
  else{
    _solver.template emplace<SolverLU>(mat);
  }
}

void solver_facto(const E_SparseMatrix& mat){
  auto info = Eigen::Success;
  std::visit([&mat, &info](auto&& solver){
    solver.compute(mat);
    info = solver.info();
  }, _solver);
  catch_eigen_error(info, "solver_facto");
}

template<typename RhsType = Vector, typename OutType = Vector>
OutType solver_solve(const RhsType& B){
  auto info = Eigen::Success;
  OutType X;
  std::visit([&X, &B, &info](auto&& solver){
    X = solver.solve(B);
    info = solver.info();
  }, _solver);
  catch_eigen_error(info, "solver_solve");
  return X;
}
// Internal solver operations:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Factorization][Factorization:1]]
void factorize(){
  MAPHYSPP_ASSERT(_is_setup == true, "EigenSparseSolver::factorize: calling factorize before setup(A)");
  MAPHYSPP_ASSERT(_is_schur_setup == false, "EigenSparseSolver::factorize: calling factorize after get_schur is not allowed");

  {
    Timer<TIMER_SOLVER> t("Eigen Sparse analysis");
    solver_init(_A);
  }{
    Timer<TIMER_SOLVER> t_f("Eigen Sparse facto");
    solver_facto(_A);
  }
  _is_facto = true;
}
// Factorization:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Solve][Solve:1]]
Vector apply(const Vector& B){
  if(!_is_facto){
    factorize();
  }
  Timer<TIMER_SOLVER> t("Sparse solve");
  return solver_solve(B);
}
// Solve:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Display][Display:1]]
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << "Sparse Eigen lib solver name: " << name << '\n';
  out << " - On arithmetic: " << arithmetic_name<Scalar>::name << '\n';
  out << " - is_setup: " << _is_setup << '\n';
  out << " - is_facto: " << _is_facto << '\n';
  if(_is_setup){
    out << " - On matrix A of size: " << _A.rows() << '\n';
  }
}
// Display:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Get Schur complement][Get Schur complement:1]]
template <MPH_Integral Index>
IndexArray<Scalar> get_schur_as_array(const IndexArray<Index>& schurlist){
  E_DenseMatrix e_schur = this->get_dense_schur(schurlist);
  return IndexArray<Scalar>(&e_schur, n_rows(e_schur));
}
// Get Schur complement:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Get Schur complement][Get Schur complement:2]]
template <MPH_Integral Index>
E_DenseMatrix get_schur(const IndexArray<Index>& schurlist){
  MAPHYSPP_ASSERT(_is_setup, "EigenSparseSolver: calling get_dense_schur before setup");
  Timer<TIMER_SOLVER> t("EigenSparseSolver Schur computation");

  auto M = _A.rows();
  _n_schur = schurlist.size();

  const int n_i  = static_cast<int>(M - _n_schur);
  const int n_g  = static_cast<int>(_n_schur);

  //1. Reorder unknowns to put schur indices at the end
  _permut_schur = Eigen::PermutationMatrix<Eigen::Dynamic>(M);
  auto& perm = _permut_schur.indices();
  IndexArray<Index> sorted_schurlist = schurlist;
  sorted_schurlist.sort();

  int idx_g = 0;
  int idx_i = 0;
  for(Index k = 0; k < static_cast<Index>(M); ++k){
    if((idx_g >= n_g) || (sorted_schurlist[idx_g] != k)){
      perm[idx_i] = k;
      idx_i++;
    }
    else{
      perm[n_i + idx_g] = k;
      idx_g++;
    }
  }

  if(_properties.is_storage_lower()){
    _A = _A.template selfadjointView<Eigen::Lower>().twistedBy(_permut_schur); // Permutation of rows and columns
  }
  else if(_properties.is_storage_upper()){
    _A = _A.template selfadjointView<Eigen::Upper>().twistedBy(_permut_schur); // Permutation of rows and columns
  }
  else{
    _A = _A.twistedBy(_permut_schur); // Permutation of rows and columns
  }

  //2. get blocks
  E_SparseMatrix A_ii = _A.topLeftCorner(n_i, n_i);
  auto A_ig = _A.topRightCorner(n_i, n_g);
  auto A_gi = _A.bottomLeftCorner(n_g, n_i);
  auto A_gg = _A.bottomRightCorner(n_g, n_g);

  // 3. Compute A_ii^-1 * A_gi
  // Solve A_ii X = A_ig
  // NB: the solver setup for A_ii will be useful for b2f later
  E_DenseMatrix A_ii_inv_A_ig(n_i, n_g);
  solver_init(A_ii);
  solver_facto(A_ii);
  A_ii = E_SparseMatrix(); // Free some memory
  for(int k = 0; k < n_g; ++k){
    A_ii_inv_A_ig.col(k) = solver_solve(A_ig.innerVector(k));
  }
  // Somehow blocking is slower...
  //A_ii_inv_A_ig = solver_solve<decltype(A_ig),decltype(A_ii_inv_A_ig)>(A_ig);

  // 4. Compute the schur: S = A_gg - Agi A_ii^-1 A_ig
  E_DenseMatrix schur = A_gg - A_gi * A_ii_inv_A_ig;

  _is_schur_setup = true;
  return E_DenseMatrix(std::move(schur));
}
// Get Schur complement:2 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*b2f][b2f:1]]
Vector b2f(const Vector& B){
  MAPHYSPP_ASSERT(_is_schur_setup, "EigenSparseSolver: (using Schur complement) calling b2f before get_schur");
  Timer<TIMER_SOLVER> t("EigenSparseSolver b2f");

  Vector B_permut = _permut_schur * B;
  auto n_g = _n_schur;
  auto n_i = _A.rows() - n_g;
  _B_i = Vector(B_permut.head(n_i));
  auto B_g = B_permut.tail(n_g);
  auto A_gi = _A.bottomLeftCorner(n_g, n_i);
  //E_SparseMatrix& A_gi = _A_gi;

  Vector A_ii_inv_b_i = solver_solve(_B_i);
  _is_schur_b2f = true;
  return Vector(B_g - A_gi * A_ii_inv_b_i);
}
// b2f:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*y2x][y2x:1]]
Vector y2x(const Vector& x_g){
  MAPHYSPP_ASSERT(_is_schur_b2f, "EigenSparseSolver: (using Schur complement) calling y2x before b2f");
  Timer<TIMER_SOLVER> t("EigenSparseSolver y2x");

  Vector x(_A.rows());
  auto n_g = _n_schur;
  auto n_i = _A.rows() - n_g;
  auto A_ig = _A.topRightCorner(n_i, n_g);
  auto x_i = x.head(n_i);
  x.tail(n_g) = x_g;

  auto rhs = _B_i - A_ig * x_g;
  x_i = solver_solve(rhs);
  x = _permut_schur.inverse() * x;
  return x;
}
// y2x:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Traits][Traits:1]]
}; //class EigenSparseSolver.org
// Traits:1 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Traits][Traits:2]]
// Set traits
template<MPH_SparseMatrix Matrix, MPH_Vector Vector>
struct is_solver_direct<EigenSparseSolver<Matrix, Vector>> : public std::true_type {};
template<typename Matrix, typename Vector>
struct is_solver_iterative<EigenSparseSolver<Matrix, Vector>> : public std::false_type {};
// Traits:2 ends here

// [[file:../../../../org/maphys/wrappers/Eigen/EigenSparseSolver.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
