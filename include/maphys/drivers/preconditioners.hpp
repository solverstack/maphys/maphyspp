#pragma once

#include <maphys.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>
#include <maphys/precond/TwoLevelAbstractSchwarz.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#if defined(MAPHYSPP_USE_FABULOUS)
#include <maphys/precond/EigenDeflatedPcd.hpp>
#endif
#include <type_traits>

namespace maphys {

/*
 * Jan - 22: for some reason the conditional does not work with c++20 concepts...
 */

template<MPH_Matrix LocMatrix, MPH_Vector LocVector, typename SparseSolver>
auto mph_get_solver_type(){
#ifdef __cpp_concepts
  if constexpr(is_dense<LocMatrix>::value){ return BlasSolver<LocMatrix, LocVector>(); }
  else { return SparseSolver(); }
#else
  using Solver = typename std::conditional_t<is_dense<LocMatrix>::value,
                                             BlasSolver<LocMatrix, LocVector>,
                                             SparseSolver>;
  return Solver();
#endif //__cpp_concepts
}

#define MPH_ONE_LEVEL_AS(CLASS_NAME)                                    \
  using LocMatrix = typename Matrix::local_data;                        \
  using LocVector = typename Vector::local_data;                        \
  using Solver = decltype(mph_get_solver_type<LocMatrix, LocVector, SparseSolver>()); \
  using Precond = CLASS_NAME <Matrix, Vector, Solver>;                  \
  return Precond();

#define MPH_TWO_LEVEL_AS_GENEO(CLASS_NAME)                              \
  using LocMatrix = typename Matrix::local_data;                        \
  using LocVector = typename Vector::local_data;                        \
  using Solver = decltype(mph_get_solver_type<LocMatrix, LocVector, SparseSolver>()); \
  using GlobSolver = CentralizedSolver<Matrix, Vector, Solver>;         \
  using M0_type = CoarseSolve<LocMatrix, LocVector, GlobSolver>;        \
  using M1_type = CLASS_NAME <Matrix, Vector, Solver>;                  \
  return TwoLevelAbstractSchwarz<Matrix, Vector, M0_type, M1_type, deflated>();

#define MPH_EIG_DEFL_AS(CLASS_NAME)                                     \
  using LocMatrix = typename Matrix::local_data;                        \
  using LocVector = typename Vector::local_data;                        \
  using Solver = decltype(mph_get_solver_type<LocMatrix, LocVector, SparseSolver>()); \
  using M0_type = EigenDeflatedPcd<Matrix, Vector>;                     \
  using M1_type = CLASS_NAME <Matrix, Vector, Solver>;                  \
  return TwoLevelAbstractSchwarz<Matrix, Vector, M0_type, M1_type, true>();

template<MPH_Matrix Matrix, MPH_Vector Vector>
auto precond_diagonal(){
  return DiagonalPrecond<Matrix, Vector>();
}

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver>
auto precond_additive_schwarz (){ MPH_ONE_LEVEL_AS(AdditiveSchwarz) }

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver>
auto precond_neumann_neumann (){ MPH_ONE_LEVEL_AS(NeumannNeumann) }

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver>
auto precond_robin_robin (){ MPH_ONE_LEVEL_AS(RobinRobin) }

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver, bool deflated = true>
auto precond_geneo_AS(){ MPH_TWO_LEVEL_AS_GENEO(AdditiveSchwarz) }

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver, bool deflated = true>
auto precond_geneo_NN(){ MPH_TWO_LEVEL_AS_GENEO(NeumannNeumann) }

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver, bool deflated = true>
auto precond_geneo_RR(){ MPH_TWO_LEVEL_AS_GENEO(RobinRobin) }

#if defined(MAPHYSPP_USE_FABULOUS)
template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver>
auto precond_defl_AS(){ MPH_EIG_DEFL_AS(AdditiveSchwarz) }

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver>
auto precond_defl_NN(){ MPH_EIG_DEFL_AS(NeumannNeumann) }

template<MPH_Matrix Matrix, MPH_Vector Vector, typename SparseSolver>
auto precond_defl_RR(){ MPH_EIG_DEFL_AS(RobinRobin) }
#endif

} // namespace maphys
