// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*Header][Header:1]]
#pragma once

#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <iostream>
#include <fstream>
#include <sstream>
#include <type_traits>
#include <complex>
#include <vector>
#include <algorithm>

#include "maphys/utils/Error.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*Split string][Split string:1]]
inline std::vector<std::string> split(const std::string &line, const char sep = ' ')
{
  std::stringstream ss{line};
  std::string token;
  std::vector<std::string> res;

  while (std::getline(ss, token, sep)) {
    // empty token each time there is several separators following each other :
    if (token != "") {
      res.emplace_back(token);
    }
  }
  return res;
}
// Split string:1 ends here

// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*Find a sub-string][Find a sub-string:1]]
inline bool header_match(const std::vector<std::string> &header, std::string token)
{
  return ( std::find(header.begin(), header.end(), token) != header.end() );
}
// Find a sub-string:1 ends here

// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*String to real conversion][String to real conversion:1]]
inline void str_to_real(const std::string& s, float& f) { f = std::stof(s); }
inline void str_to_real(const std::string& s, double& d) { d = std::stod(s); }
// String to real conversion:1 ends here

// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*Matrix market namespace][Matrix market namespace:1]]
namespace matrix_market {
// Matrix market namespace:1 ends here

// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*Load a matrix market line][Load a matrix market line:1]]
template<typename Scalar>
inline Scalar read_scalar(const std::vector<std::string>& line, int index){
  if constexpr(is_real<Scalar>::value){
    Scalar v;
    str_to_real(line[index], v);
    return v;
  }
  else{
    using Real = typename arithmetic_real<Scalar>::type;
    Real real, imag;
    str_to_real(line[index], real);
    str_to_real(line[index+1], imag);
    return Scalar{real, imag};
  }
}

template<typename Scalar>
inline void read_ijv(const std::vector<std::string> &line, int& i, int& j, Scalar& v){
  i = std::stoi(line[0]) - 1;
  j = std::stoi(line[1]) - 1;
  v = read_scalar<Scalar>(line, 2);
}
// Load a matrix market line:1 ends here

// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*Read header][Read header:1]]
inline bool is_real_matrix(const std::string& filename){

  std::ifstream file{filename, std::ios::in};
  MAPHYSPP_ASSERT(file, filename + ": could not be opened");

  std::string line;

  getline(file, line);
  auto header = split(line);

  // Check MatrixMarket
  MAPHYSPP_ASSERT( (header[0] == "%%MatrixMarket"),
                 filename + ": unrecognized file format.");

  if(header_match(header, "complex")){
    return false;
  }
  if(header_match(header, "real")){
    return true;
  }
  MAPHYSPP_ASSERT(false, filename + ": unreconized scalar");
  return false;
}

template<class Scalar>
inline void read_header(std::ifstream& file, const std::string& filename, int& M, int& N, int& NNZ, MatrixSymmetry& symmetry, bool& is_format_array){

  if ( !file.is_open() ) {
    MAPHYSPP_ASSERT(file, filename + ": file not opened");
  }

  bool commentLine;
  std::string line;
  symmetry = MatrixSymmetry::general;

  getline(file, line);
  auto header = split(line);

  // Check MatrixMarket
  MAPHYSPP_ASSERT( (header[0] == "%%MatrixMarket"),
                 filename + ": unrecognized file format.");

  // Array / coordinate format
  is_format_array = header_match(header, "array");

  // Get symmetry
  if(header_match(header, "symmetric")){
    symmetry = MatrixSymmetry::symmetric;
  }
  else if(header_match(header, "hermitian")){
    symmetry = MatrixSymmetry::hermitian;
  }

  // Check type is corresponding
  if (is_complex<Scalar>::value && !header_match(header, "complex")) {

      MAPHYSPP_ASSERT( header_match(header, "complex"),
                     "This is a complex parser but file "
                     "header does not have \"complex\" tag");

    } else if (!is_complex<Scalar>::value){
    MAPHYSPP_ASSERT( header_match(header, "real"),
                   "This is a real parser but file "
                   "header does not have \"real\" tag");
  }

  do{
    getline(file,line);
    //std::cerr << line << "\n";
    commentLine = ( line.length() == 0 || line[0] == '%' );
  } while (commentLine);
  std::cerr << std::flush;

  auto size = split(line);
  M = std::stoi(size[0]);
  N = std::stoi(size[1]);
  NNZ = is_format_array ? M * N : std::stoi(size[2]);
}
// Read header:1 ends here

// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*Loading the file][Loading the file:1]]
template<class Scalar, class IntArray, class ScalArray>
inline void load(const std::string &filename,
          IntArray& i,
          IntArray& j,
          ScalArray& values,
          int& m,
          int& n,
          int& nnz,
          MatrixSymmetry& symmetry,
          MatrixStorage& storage){

  std::ifstream file{filename, std::ios::in};
  MAPHYSPP_ASSERT(file, filename + ": could not be opened");

  bool is_format_array;
  read_header<Scalar>(file, filename, m, n, nnz, symmetry, is_format_array);
  std::string line;

  i = IntArray(nnz);
  j = IntArray(nnz);
  values = ScalArray(nnz);

  if(is_format_array){
    int k = 0;
    for(int jj = 0; jj < n; ++jj){
      for(int ii = 0; ii < m; ++ii){
        getline(file, line);
        auto lv = split(line);
        i[k] = ii;
        j[k] = jj;
        values[k] = read_scalar<Scalar>(lv, 0);
        k++;
      }
    }
    storage = MatrixStorage::full;
  }

  else{ // Format coordinate
    for (int k = 0; k < nnz; ++k) {
      getline(file, line);
      auto lv = split(line);
      read_ijv<Scalar>(lv, i[k], j[k], values[k]);
    }

    // When symmetric, trying to find storage
    storage = MatrixStorage::full;
    if(symmetry != MatrixSymmetry::general){
      bool upper = false;
      bool lower = false;
      for(int k = 0; k < nnz; ++k){
        if(i[k] > j[k]){
          lower = true;
        }
        else if(i[k] < j[k]){
          upper = true;
        }
        if(lower && upper) return;
      }
      storage = lower ? MatrixStorage::lower : MatrixStorage::upper;
    }
  }
}
// Loading the file:1 ends here

// [[file:../../../org/maphys/IO/MatrixMarketLoader.org::*Footer][Footer:1]]
} // end namespace matrix_market
} // end namespace maphys
// Footer:1 ends here
