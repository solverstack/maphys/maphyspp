#pragma once

#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <iostream>
#include <fstream>
#include <sstream>
#include <type_traits>
#include <complex>
#include <vector>
#include <algorithm>
#include <armadillo>

#include "maphys/utils/Error.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/IO/MatrixMarketLoader.hpp"

namespace maphys {

// Load a sparse matrix
template<typename Scalar>
arma::SpMat<Scalar> load_armadillo_matrix(const std::string &filename, MatrixSymmetry& sym){

  std::vector<int> i, j;
  std::vector<Scalar> v;
  int m, n, nnz;
  matrix_market::load<Scalar, std::vector<int>, std::vector<Scalar>>(filename, i, j, v, m, n, nnz, sym);

  arma::SpMat<Scalar> out_matrix;
  const bool fill_symmetry = (sym == MatrixSymmetry::symmetric);
  build_matrix(out_matrix, m, n, nnz, i.data(), j.data(), v.data(), fill_symmetry);

  return out_matrix;
}

// Load a dense vector
template<typename Scalar>
arma::Col<Scalar> load_armadillo_vector(const std::string &filename){

  std::vector<int> i, j;
  std::vector<Scalar> v;
  int m, n, nnz;
  MatrixSymmetry sym;
  matrix_market::load<Scalar, std::vector<int>, std::vector<Scalar>>(filename, i, j, v, m, n, nnz, sym);
  MAPHYSPP_ASSERT( n == 1, "Trying to load an armadillo vector from a matrix file with nb of col != 1");

  arma::Col<Scalar> out_vector(m);

  for(int k = 0; k < nnz; ++k){
    out_vector[i[k]] = v[k];
  }

  return out_vector;
}
} // end namespace maphys
