// [[file:../../../org/maphys/IO/ReadParam.org::*Read parameters from file][Read parameters from file:1]]
#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

#include "maphys/utils/Error.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "./MatrixMarketLoader.hpp"

namespace maphys {

inline void remove_spaces(std::string& line){
  std::string newline;
  for(auto c : line){
    if(c != ' ') newline += c;
  }
  line = newline;
}

inline std::map<std::string, std::string> read_param_file(const std::string& filename){

  std::map<std::string, std::string> key_values_map;
  std::ifstream file{filename, std::ios::in};
  MAPHYSPP_ASSERT(file, filename + ": could not be opened");

  const char sep = ':';
  const char comment_char = '#';

  for(std::string line; std::getline(file, line); ) {

    std::vector<std::string> line_no_comment;
    std::vector<std::string> line_split;

    // Remove spaces
    remove_spaces(line);
    if(line.size() > 0 && line[0] == comment_char) continue;

    // Remove comments
    line_no_comment = split(line, comment_char);
    if(line.size() == 0) continue;

    line = line_no_comment[0];

    line_split = split(line, sep);

    key_values_map[line_split[0]] = line_split[1];
  }

  return key_values_map;
}
} // namespace maphys
// Read parameters from file:1 ends here
