// [[file:../../../org/maphys/part_data/PartBase.org::*Header][Header:1]]
#pragma once

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <mutex>
namespace maphys {
  template<class Data> class PartBase;
}

#include "maphys/utils/Error.hpp"
#include "maphys/dist/Process.hpp"
#include "maphys/interfaces/linalg_concepts.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Attributes][Attributes:1]]
template<class Data>
class PartBase {

public:
  using local_data = Data;

protected:
  std::shared_ptr<Process> _proc;
  // Map SD ID -> local data
  std::map<int, Data> _loc_data;
  mutable std::mutex _map_mutex;
  bool _on_interface = false;
// Attributes:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Constructors][Constructors:1]]
protected:
bool check_process_owns_subdomain(){
  for(auto& [sd_id, sd_data] : _loc_data){
    (void) sd_data;
    if(!_proc->owns_subdomain(sd_id)) return false;
  }
  return true;
}
void check_subdomain_data() const {
  int nrhs = -1;
  for(auto& [sd_id, sd_data] : _loc_data){
    (void) sd_id;
    int sd_nrhs = sd_data.get_n_cols();
    if(nrhs == -1) nrhs = sd_nrhs;
    if(MMPI::rank() == 0) std::cerr << "c:" << sd_data.get_n_rows() << ',' << sd_data.get_n_cols() <<'\n';
    MAPHYSPP_ASSERT(nrhs == sd_nrhs, "Some data on the process deos not have the same number of columns");
  }
}

public:
  // Minimal constructor
  PartBase(){}

  PartBase(std::shared_ptr<Process> proc, bool on_interface = false):
    _proc{proc},
    _on_interface{on_interface}
  {
    this->initialize(proc);
  }

  // One subdomain constructors
  PartBase(std::shared_ptr<Process> proc, const int sd_id, const Data& loc_op, bool on_interface = false):
    PartBase(proc, on_interface)
  {
    _loc_data = std::map<int, Data>{{sd_id, loc_op}};
    MAPHYSPP_ASSERT(check_process_owns_subdomain(), "Error in building PartBase: process does not own subdomain");
  }

  PartBase(std::shared_ptr<Process> proc, const int sd_id, Data&& loc_op, bool on_interface = false):
    PartBase(proc, on_interface)
  {
    _loc_data[sd_id] = std::move(loc_op);
    MAPHYSPP_ASSERT(check_process_owns_subdomain(), "Error in building PartBase: process does not own subdomain");
  }

  // Multiple subdomain constructors
  PartBase(std::shared_ptr<Process> proc, const std::map<int, Data>& loc_data, bool on_interface = false):
    PartBase(proc, on_interface)
  {
    _loc_data = loc_data;
    MAPHYSPP_ASSERT(check_process_owns_subdomain(), "Error in building PartBase: process does not own subdomain");
  }

  PartBase(std::shared_ptr<Process> proc, std::map<int, Data>&& loc_data, bool on_interface = false):
    PartBase(proc, on_interface)
  {
    _loc_data = std::move(loc_data);
    MAPHYSPP_ASSERT(check_process_owns_subdomain(), "Error in building PartBase: process does not own subdomain");
  }

  // Same distribution constructor
  PartBase(const PartBase& other, Size nrhs):
    _proc{other._proc}, _on_interface{other._on_interface}
  {
    for(auto& id : _proc->get_sd_ids()){
      this->_loc_data[id] = Data(n_rows(other.get_local_data(id)), nrhs);
    }
  }

  // Copy constructor
  PartBase(const PartBase& other):
    _proc{other._proc}, _loc_data{other._loc_data}, _on_interface{other._on_interface}
  {
  }

  // Move constructor
  PartBase(PartBase&& other):
    _proc{std::exchange(other._proc, nullptr)},
    _loc_data{std::move(other._loc_data)},
    _on_interface{std::exchange(other._on_interface, false)}
  {
  }

  // Copy from other type of data
  template<class OtherData>
  PartBase(const PartBase<OtherData>& other):
    _proc{other.get_proc()}, _on_interface{other.on_intrf()}
  {
    if(_proc == nullptr) return;
    for(const auto& sd_id : _proc->get_sd_ids()){
      _loc_data.insert_or_assign(sd_id, Data(other.get_local_data(sd_id)));
    }
  }

  // Copy assignment operator
  PartBase& operator=(const PartBase& other){
    if(_proc != other._proc){
      _loc_data.clear();
      _proc = other._proc;
    }
    if(_proc == nullptr) return *this;
    for(const auto& sd_id : _proc->get_sd_ids()){
      _loc_data[sd_id] = other.get_local_data(sd_id);
    }
    _on_interface = other._on_interface;
    return *this;
  }

  // Move assignment operator
  PartBase& operator=(PartBase&& other){
    if(_proc != other._proc){
      _loc_data.clear();
    }
    _proc = std::exchange(other._proc, nullptr);
    if(_proc == nullptr) return *this;
    for(const auto& sd_id : _proc->get_sd_ids()){
      _loc_data[sd_id] = std::move(other._loc_data[sd_id]);
    }
    _on_interface = std::exchange(other._on_interface, false);
    return *this;
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Default initialization][Default initialization:1]]
void initialize(std::shared_ptr<Process> proc){
  _proc = proc;
  for(const auto& sd_id : _proc->get_sd_ids()){
    _loc_data.emplace(sd_id, Data());
  }
}
// Default initialization:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Add a subdomain][Add a subdomain:1]]
void add_subdomain(const int i, Data&& v){
  const std::lock_guard<std::mutex> lock_map(_map_mutex);
  _loc_data[i] = std::move(v);
}
void add_subdomain(const int i, const Data& v){
  const std::lock_guard<std::mutex> lock_map(_map_mutex);
  _loc_data[i] = v;
}
// Add a subdomain:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Unary operator][Unary operator:1]]
#if defined(MAPHYSPP_USE_MULTITHREAD)
  void apply_on_data(std::function<void(Data&)> fct) {
    _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
      for (const auto& sd_id : sds)
        fct(_loc_data[sd_id]);
    });

  }
  #else
  void apply_on_data(std::function<void(Data&)> fct) {
    for(auto& m : _loc_data){
      fct(m.second);
    }
  }
  #endif

  #if defined(MAPHYSPP_USE_MULTITHREAD)
  void apply_on_data_id(std::function<void(const int, Data&)> fct) {
    _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
      for (const auto& sd_id : sds)
        fct(sd_id, _loc_data[sd_id]);
    });
  }
  #else
  void apply_on_data_id(std::function<void(const int, Data&)> fct) {
    for(auto& m : _loc_data){
      fct(m.first, m.second);
    }
  }
#endif
// Unary operator:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Binary operator][Binary operator:1]]
#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherData = Data>
void apply_on_data(const PartBase<OtherData>& other, std::function<void(Data&, const OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for (auto& sd_id : sds) {
      const auto& their_op = other.get_local_data(sd_id);
      fct(_loc_data[sd_id], their_op);
    }
  });
}
#else
template<class OtherData = Data>
void apply_on_data(const PartBase<OtherData>& other, std::function<void(Data&, const OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  for(auto& [my_id, my_op] : _loc_data){
    const auto& their_op = other.get_local_data(my_id); // Same ID
    fct(my_op, their_op);
  }
}
#endif

#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherData = Data>
void apply_on_data(PartBase<OtherData>& other, std::function<void(Data&, OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for (const auto& sd_id : sds) {
      auto& their_op = other.get_local_data(sd_id);
      fct(_loc_data[sd_id], their_op);
    }
  });
 }
#else
template<class OtherData = Data>
void apply_on_data(PartBase<OtherData>& other, std::function<void(Data&, OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  for(auto& [my_id, my_op] : _loc_data){
    auto& their_op = other.get_local_data(my_id); // Same ID
    fct(my_op, their_op);
  }
}
#endif
#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherData = Data>
void apply_on_data_id(const PartBase<OtherData>& other, std::function<void(const int, Data&, const OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for (const auto& sd_id : sds) {
      const auto& their_op = other.get_local_data(sd_id);
      fct(sd_id, _loc_data[sd_id], their_op);
    }
  });
}
#else
template<class OtherData = Data>
void apply_on_data_id(const PartBase<OtherData>& other, std::function<void(const int, Data&, const OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  for(auto& [my_id, my_op] : _loc_data){
    const auto& their_op = other.get_local_data(my_id); // Same ID
    fct(my_id, my_op, their_op);
  }
}
#endif
#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherData = Data>
void apply_on_data_id(PartBase<OtherData>& other, std::function<void(const int, Data&, OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for (const auto& sd_id : sds) {
      auto& their_op = other.get_local_data(sd_id);
      fct(sd_id, _loc_data[sd_id], their_op);
    }
  });
}
#else
template<class OtherData = Data>
void apply_on_data_id(PartBase<OtherData>& other, std::function<void(const int, Data&, OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  for(auto& [my_id, my_op] : _loc_data){
    auto& their_op = other.get_local_data(my_id); // Same ID
    fct(my_id, my_op, their_op);
  }
}
#endif
// Binary operator:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Ternary operator][Ternary operator:1]]
#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherData = Data, class NewData = Data>
void apply_on_data(const PartBase<OtherData>& other, PartBase<NewData>& out, std::function<NewData (Data&, const OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  std::map<int, NewData> new_ops;
  for (const auto& [sd_id, sd] : _loc_data) {
    new_ops.emplace(std::make_pair(sd_id, NewData()));
  }
  _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for (const auto& sd_id : sds) {
      const auto& their_op = other.get_local_data(sd_id);
      auto& my_op = _loc_data[sd_id];
      new_ops[sd_id] = fct(my_op, their_op);
    }
  });
  out = PartBase<NewData>(_proc, std::move(new_ops), out.on_intrf());
}
#else
template<class OtherData = Data, class NewData = Data>
void apply_on_data(const PartBase<OtherData>& other, PartBase<NewData>& out, std::function<NewData (Data&, const OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  std::map<int, NewData> new_ops;

  for(auto& [my_id, my_op] : _loc_data){
    const auto& their_op = other.get_local_data(my_id); // Same ID
    new_ops[my_id] = fct(my_op, their_op);
  }

  out = PartBase<NewData>(_proc, std::move(new_ops), out.on_intrf());
}
#endif
#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherData = Data, class NewData = Data>
void apply_on_data_id(const PartBase<OtherData>& other, PartBase<NewData>& out, std::function<NewData (const int, Data&, const OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  std::map<int, NewData> new_ops;
  for (const auto& [sd_id, sd] : _loc_data) {
    new_ops.emplace(std::make_pair(sd_id, NewData()));
  }
  _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for (const auto& sd_id : sds) {
      const auto& their_op = other.get_local_data(sd_id);
      new_ops[sd_id] = fct(sd_id, _loc_data[sd_id], their_op);
    }
  });

  out = PartBase<NewData>(*_proc, std::move(new_ops), out.on_intrf());
}
#else
template<class OtherData = Data, class NewData = Data>
void apply_on_data_id(const PartBase<OtherData>& other, PartBase<NewData>& out, std::function<NewData (const int, Data&, const OtherData&)> fct) {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with operator on different procs");
  std::map<int, NewData> new_ops;

  for(auto& [my_id, my_op] : _loc_data){
    const auto& their_op = other.get_local_data(my_id); // Same ID
    new_ops[my_id] = fct(my_id, my_op, their_op);
  }

  out = PartBase<NewData>(*_proc, std::move(new_ops), out.on_intrf());
}
#endif
// Ternary operator:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Ternary operator][Ternary operator:2]]
#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherData = Data, class NewData = Data>
void apply_on_data(const PartBase<OtherData>& other, PartBase<NewData>& out, std::function<NewData (const Data&, const OtherData&)> fct) const {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with data on different procs");

  std::map<int, NewData> new_ops;
  for (const auto& [sd_id, sd] : _loc_data) {
    new_ops.emplace(std::make_pair(sd_id, NewData()));
  }
  _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for (const auto& sd_id : sds) {
      const auto& their_op = other.get_local_data(sd_id);
      new_ops[sd_id] = fct(get_local_data(sd_id), their_op);

    }
  });

  out = PartBase<NewData>(*_proc, std::move(new_ops), out.on_intrf());
}
#else
template<class OtherData = Data, class NewData = Data>
void apply_on_data(const PartBase<OtherData>& other, PartBase<NewData>& out, std::function<NewData (const Data&, const OtherData&)> fct) const {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with data on different procs");
  std::map<int, NewData> new_ops;

  for(const auto& [my_id, my_op] : _loc_data){
    const auto& their_op = other.get_local_data(my_id); // Same ID
    new_ops[my_id] = fct(my_op, their_op);
  }

  out = PartBase<NewData>(*_proc, std::move(new_ops), out.on_intrf());
}
#endif
#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherData = Data, class NewData = Data>
void apply_on_data_id(const PartBase<OtherData>& other, PartBase<NewData>& out, std::function<NewData (const int, const Data&, const OtherData&)> fct) const {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with data on different procs");
  std::map<int, NewData> new_ops;
  for (const auto& [sd_id, sd] : _loc_data) {
    new_ops.emplace(std::make_pair(sd_id, NewData()));
  }
  _proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for (const auto& sd_id : sds) {
      const auto& their_op = other.get_local_data(sd_id);
      new_ops[sd_id] = fct(sd_id, get_local_data(sd_id), their_op);
    }
  });

  out = PartBase<NewData>(*_proc, std::move(new_ops), out.on_intrf());
}
#else
template<class OtherData = Data, class NewData = Data>
void apply_on_data_id(const PartBase<OtherData>& other, PartBase<NewData>& out, std::function<NewData (const int, const Data&, const OtherData&)> fct) const {
  MAPHYSPP_ASSERT(other.is_on_proc(*_proc), "PartBase: apply with data on different procs");
  std::map<int, NewData> new_ops;

  for(const auto& [my_id, my_op] : _loc_data){
    const auto& their_op = other.get_local_data(my_id); // Same ID
    new_ops[my_id] = fct(my_id, my_op, their_op);
  }

  out = PartBase<NewData>(*_proc, std::move(new_ops), out.on_intrf());
}
#endif
// Ternary operator:2 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Getters][Getters:1]]
[[nodiscard]] Data& get_local_data(const int sd_id){
  return _loc_data.at(sd_id);
}

[[nodiscard]] const Data& get_local_data(const int sd_id) const {
  return _loc_data.at(sd_id);
}

[[nodiscard]] inline bool on_intrf() const {
  return _on_interface;
}

[[nodiscard]] inline IndexArray<int> get_sd_ids() const {
  return this->_proc->get_sd_ids();
}

[[nodiscard]] std::shared_ptr<Process> get_proc() const {
  return _proc;
}

[[nodiscard]] std::shared_ptr<Process>& get_proc_ref(){
  return _proc;
}

[[nodiscard]] Size get_buffer_size(const int sd_id) const {
  MAPHYSPP_ASSERT(_proc != nullptr, "PartBase::_proc is nullptr");
  return _proc->get_n_dofs(sd_id, this->_on_interface);
}

[[nodiscard]] std::map<int, Data>& get_data_map(){
  return _loc_data;
}
// Getters:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Setters][Setters:1]]
inline void set_on_intrf(bool on_intrf = true){
  _on_interface = on_intrf;
}
// Setters:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Comparison][Comparison:1]]
bool operator==(const PartBase& other) const {
  if( _proc.get() != other._proc.get()) return false;
  if( _on_interface != other._on_interface) return false;
  return (_loc_data == other._loc_data);
}

bool operator!=(const PartBase& other) const { return !(*this == other); }
// Comparison:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Checking functions][Checking functions:1]]
bool is_on_proc(const Process& p) const { return &p == _proc.get(); }
// Checking functions:1 ends here

// [[file:../../../org/maphys/part_data/PartBase.org::*Footer][Footer:1]]
}; // class PartBase
} // namespace maphys
// Footer:1 ends here
