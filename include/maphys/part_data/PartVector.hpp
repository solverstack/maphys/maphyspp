// [[file:../../../org/maphys/part_data/PartVector.org::*Header][Header:1]]
#pragma once

#include <type_traits>
#include <string>
#include <vector>
#include <map>

namespace maphys {
  template<MPH_Vector Vect> class PartVector;

  template<class Vect, class Vect2, MPH_Scalar Scalar = typename scalar_type<Vect>::type>
  Scalar dot(const PartVector<Vect> A, const PartVector<Vect2>& B);
} // namespace maphys

#include "maphys/utils/Error.hpp"
#include "maphys/utils/IndexArray.hpp"
#include "maphys/dist/Process.hpp"
#include "maphys/part_data/PartBase.hpp"
#include "maphys/interfaces/linalg_concepts.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Attributes][Attributes:1]]
template<MPH_Vector Vect>
class PartVector : public PartBase<Vect> {

private:
  using Scalar = typename scalar_type<Vect>::type;
  using Real = typename arithmetic_real<Scalar>::type;
  using Base = PartBase<Vect>;

public:
  using scalar_type = Scalar;
  using real_type = Real;
  using local_type = Vect;

private:
  bool _compatible = true;
// Attributes:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Constructors][Constructors:1]]
public:
  // Minimal constructor
  explicit PartVector(){}

  explicit PartVector(std::shared_ptr<Process> proc, bool on_interface = false, bool compatible = true):
    Base(proc, on_interface), _compatible{compatible} {}

  // One subdomain constructors
  explicit PartVector(std::shared_ptr<Process> proc, const int sd_id, const Vect& loc_v, bool on_interface = false, bool compatible = true):
    Base(proc, sd_id, loc_v, on_interface), _compatible{compatible} {}

  explicit PartVector(std::shared_ptr<Process> proc, const int sd_id, Vect&& loc_v, bool on_interface = false, bool compatible = true):
    Base(proc, sd_id, std::move(loc_v), on_interface), _compatible{compatible} {}

  // Multiple subdomain constructors
  explicit PartVector(std::shared_ptr<Process> proc, const std::map<int, Vect>& loc_vectors, bool on_interface = false, bool compatible = true):
    Base(proc, loc_vectors, on_interface), _compatible{compatible} {}

  explicit PartVector(std::shared_ptr<Process> proc, std::map<int, Vect>&& loc_vectors, bool on_interface = false, bool compatible = true):
    Base(proc, loc_vectors, on_interface), _compatible{compatible} {}

  // Copy constructor
  PartVector(const PartVector& dv):
     Base(dv), _compatible{dv._compatible} {}

  // Other local type constructor
  template<typename OtherVect>
  PartVector(const PartVector<OtherVect>& other): Base(other), _compatible{other.compatible()} {}

  // Same distribution constructor
  PartVector(const PartVector& dv, const Size nrhs): Base(dv, nrhs), _compatible{dv._compatible} {}

  // Move constructor
  PartVector(PartVector&& dv):
    Base(std::move(dv)), _compatible{std::exchange(dv._compatible, true)} {}

  PartVector& operator=(const PartVector& dv) = default;
  PartVector& operator=(PartVector&& dv) = default;

  template<typename Scalar, int NbCol, class = typename std::enable_if<std::is_same<Vect,DenseMatrix<Scalar, NbCol>>::value>::value>
  PartVector<DenseMatrix<Scalar, -1>>& operator= (const PartVector<DenseMatrix<Scalar, NbCol>>& mat) {
    PartVector<DenseMatrix<Scalar, -1>> res{mat, mat.get_n_cols()};
    std::function<void(Vect&, const Vect&)> cast_func = [&] (DenseMatrix<Scalar, -1>& data, const DenseMatrix<Scalar, NbCol>& data2){ data = data2; };
    res.apply_on_data(mat, cast_func);
    return res;
  }

  template<typename Scalar, int NbCol, class = typename std::enable_if<std::is_same<Vect,DenseMatrix<Scalar, -1>>::value>::value>
  PartVector<DenseMatrix<Scalar, NbCol>>& operator= (const PartVector<DenseMatrix<Scalar, -1>>& mat) {
    MAPHYSPP_ASSERT(NbCol == mat.get_n_cols(), "PartVector: cast DenseMatrix<Scalar> into DenseMatrix<Scalar, Nbcol>, mismatch of column number");
    PartVector<DenseMatrix<Scalar, NbCol>> res{mat, mat.get_n_cols()};
    std::function<void(Vect&, const Vect&)> cast_func = [&] (DenseMatrix<Scalar, NbCol>& data, const DenseMatrix<Scalar, -1>& data2){ data = data2; };
    res.apply_on_data(mat, cast_func);
    return res;
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Checking process is valid][Checking process is valid:1]]
inline std::shared_ptr<Process> check_proc_ptr() const {
  MAPHYSPP_ASSERT(this->_proc != nullptr, "PartVector::_proc is nullptr");
  return this->_proc;
}
// Checking process is valid:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*PartVector initialization][PartVector initialization:1]]
void initialize(std::shared_ptr<Process> proc){
  this->_proc = proc;
  for(const auto& sd_id : this->_proc->get_sd_ids()){
    this->_loc_data.emplace(sd_id, Vect(this->_proc->get_n_dofs(sd_id, this->_on_interface), 0));
  }
}
void initialize(std::shared_ptr<Process> proc, bool on_intrf){
  this->_on_interface = on_intrf;
  this->_proc = proc;
  for(const auto& sd_id : this->_proc->get_sd_ids()){
    this->_loc_data.emplace(sd_id, Vect(this->_proc->get_n_dofs(sd_id, this->_on_interface), 0));
  }
}
// PartVector initialization:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Load values from a file][Load values from a file:1]]
void load_from_file(int id, const std::string& filename){
  std::ifstream f{filename, std::ios::in};
  std::string line;

  if ( !f.is_open() ) {
    MAPHYSPP_ASSERT(f, filename + ": file not opened");
  }

  IndexArray<int> i, j;
  IndexArray<Scalar> values;
  int m, n, nnz;
  MatrixSymmetry sym;
  MatrixStorage stor;

  matrix_market::load<Scalar,IndexArray<int>,IndexArray<Scalar>>(filename, i, j, values, m, n, nnz, sym, stor);

  //Check we loaded a vector
  MAPHYSPP_ASSERT( n == 1, "PartVector: not loading a vector (n != 1) from " + filename);

  this->_loc_data[id] = Vect(m);
  this->_loc_data[id] *= 0;
  for(int k = 0; k < nnz; ++k){
    this->_loc_data[id](i[k], 0) = values[k];
  }
}
// Load values from a file:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Set from a global vector][Set from a global vector:1]]
void from_global_vector(const Vect& glob_v){
  std::shared_ptr<Process> p = check_proc_ptr();
  this->initialize(p);
  for(auto& v : this->_loc_data){
    const int sd_id = v.first;
    IndexArray<int> glob_idx = p->get_global_indices(sd_id, this->_on_interface);
    Vect& local_vector = v.second;
    local_vector = Vect(glob_idx.size());
    int k = 0;
    for(int i : glob_idx){
      local_vector[k++] = glob_v[i];
    }
  }
}
// Set from a global vector:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Vector assembly][Vector assembly:1]]
void assemble(Reduction reduction = Reduction::twoways_sum){
  std::shared_ptr<Process> p = check_proc_ptr();
  auto& loc_values(this->_loc_data);
  p->subdomain_assemble<Scalar, Vect, true/*has_vector_comm_pattern*/>(loc_values, this->_on_interface, reduction);
}

void disassemble(){
  std::shared_ptr<Process> p = check_proc_ptr();
  p->subdomain_disassemble<Scalar,Vect>(this->_loc_data, this->_on_interface);
}
// Vector assembly:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Operators][Operators:1]]
// Scalar multiplication
  #if defined(MAPHYSPP_USE_MULTITHREAD)
  PartVector& operator*= (const Scalar& scal){
    this->_proc->parallel_run([&, this] (int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        this->_loc_data[sd_id] *= scal;
      }
    });
    return *this;
  }
  #else
  PartVector& operator*= (const Scalar& scal){
      for(auto& [id, data] : this->_loc_data){
        (void) id;
        data *= scal;
      }
      return *this;
    }
  #endif
  #if defined(MAPHYSPP_USE_MULTITHREAD)
  PartVector& operator/= (const Scalar& scal){
    this->_proc->parallel_run([&, this] (int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        this->_loc_data[sd_id] *= (Scalar{1.0} / scal);
      }
    });
    return *this;
  }
  #else
  PartVector& operator/= (const Scalar& scal){
      for(auto& v : this->_loc_data){
        v.second *= (Scalar{1.0}/scal);
      }
      return *this;
    }
  #endif
  #if defined(MAPHYSPP_USE_MULTITHREAD)
  PartVector& operator+=(const PartVector& vect){
    this->_proc->parallel_run([&, this] (int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const auto& their_vect = vect.get_local_vector(sd_id);
        this->_loc_data[sd_id] += their_vect;
      }
    });
    return *this;
  }
  #else
  PartVector& operator+=(const PartVector& vect){
    for(auto& my_vects : this->_loc_data){
	    const int& my_id = my_vects.first;
	    const auto& their_vect = vect._loc_data.at(my_id); // Same vector ID
	    my_vects.second += their_vect;
    }
    return *this;
  }
  #endif
  #if defined(MAPHYSPP_USE_MULTITHREAD)
  PartVector& operator-=(const PartVector& vect){
    this->_proc->parallel_run([&, this] (int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const auto& their_vect = vect.get_local_vector(sd_id);
        this->_loc_data[sd_id] -= their_vect;
      }
    });
    return *this;
  }
  #else
  PartVector& operator-=(const PartVector& vect){
    for(auto& [sd_id, my_vect] : this->_loc_data){
	    const auto& their_vect = vect._loc_data.at(sd_id); // Same vector ID
	    my_vect -= their_vect;
    }

    return *this;
  }
  #endif
  void cwise_scale(const std::vector<Real> &scaling){
    if(this->get_n_cols() == 0) return;
    MAPHYSPP_ASSERT(this->get_n_cols() == scaling.size(), "cwise_scaling: number of columns and scaling have diffrent size");
    for(Size i = 0; i < this->get_n_cols(); i++){
      this->get_vect_view(i) *= scaling[i];
    }
  }

private:
  #if defined(MAPHYSPP_USE_MULTITHREAD)
  Scalar local_dot(const PartVector<Vect>& vect) const {

    std::shared_ptr<Process> p = check_proc_ptr();
    std::vector<Scalar> sendbufs;
    this->_proc->parallel_run([&, this] (int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const Vect& their_vect = vect.get_local_vector(sd_id);  // Same vector ID
        const auto& my_vect = this->get_local_vector(sd_id);
        Scalar s = dot(my_vect, their_vect);
        {
          const std::lock_guard<std::mutex> lock_map(this->_map_mutex);
          sendbufs.push_back(s);
        }
      }
    });
    return p->subdomain_allreduce(sendbufs);
  }
  #else
  Scalar local_dot(const PartVector<Vect>& vect) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    std::vector<Scalar> sendbufs;
    for(auto& my_vects : this->_loc_data){
	    const int& my_id = my_vects.first;
	    const Vect& their_vect = vect._loc_data.at(my_id); // Same vector ID
	    Scalar s = dot(my_vects.second, their_vect);
	    sendbufs.push_back(s);
    }

    return p->subdomain_allreduce(sendbufs);
  }
  #endif
  #if defined(MAPHYSPP_USE_MULTITHREAD)
  Vect local_dot_block(const PartVector<Vect>& vect) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    std::vector<Vect> sendbufs;
    Vect norm2{};
    std::vector<Vect> vec_norm2;
    // bool is_first = true;
    this->_proc->parallel_run([&, this] (int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const Vect& their_vect = vect.get_local_vector(sd_id);
        Vect res_vect = this->get_local_vector(sd_id).dot_block(their_vect);
        {
          const std::lock_guard<std::mutex> lock_map(this->_map_mutex);
          vec_norm2.push_back(res_vect);
        }

      }
    });
    for (Size i = 0; i < vec_norm2.size(); i++) {
      if (i == 0)
        norm2 = vec_norm2[i];
      else
        norm2 += vec_norm2[i];
    }

    // No thread version (to keep?) TODO
    /*
    for(auto& my_vects : this->_loc_data){
      const int& my_id = my_vects.first;
      const Vect& their_vect = vect._loc_data.at(my_id); // Same vector ID
      if(is_first){ // Allocate the first block
        norm2 = my_vects.second.dot_block(their_vect);
        is_first = false;
      }
      else{
        norm2 += my_vects.second.dot_block(their_vect);
      }
    }
    */
    sendbufs.push_back(norm2);
    return p->subdomain_allreduce<Vect, true/*has_vector_comm_pattern*/>(sendbufs, MPI_SUM);
  }
  #else
  Vect local_dot_block(const PartVector<Vect>& vect) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    std::vector<Vect> sendbufs;
    Vect norm2{};
    bool is_first = true;
    for(auto& my_vects : this->_loc_data){
	    const int& my_id = my_vects.first;
	    const Vect& their_vect = vect._loc_data.at(my_id); // Same vector ID
	    if(is_first){ // Allocate the first block
	      norm2 = my_vects.second.dot_block(their_vect);
	      is_first = false;
	    }
      else{
	      norm2 += my_vects.second.dot_block(their_vect);
	    }
    }
    sendbufs.push_back(norm2);
    return p->subdomain_allreduce<Vect, true/*has_vector_comm_pattern*/>(sendbufs, MPI_SUM);
  }
  #endif

public:
  Scalar distdot(const PartVector<Vect>& vect) const{
    if(_compatible){
      PartVector vect_cpy = vect;
      vect_cpy.disassemble();
      return local_dot(vect_cpy);
    }
    return local_dot(vect);
  }

  Vect distdot_block(const PartVector<Vect>& vect) const{
    if(_compatible){
      PartVector vect_cpy = vect;
      vect_cpy.disassemble();
      return local_dot_block(vect_cpy);
    }
    return local_dot_block(vect);
  }
// Operators:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Norms][Norms:1]]
inline Real norm2_sq() const { return std::real(this->distdot(*this)); }
Real norm2() const{ return std::sqrt( this->norm2_sq() ); }
Real norm() const { return this->norm2(); }
std::vector<Real> cwise_norm() const {
  if(this->get_n_cols() == 0) return std::vector<Real>{};
  DenseMatrix<Scalar> norm2 = this->distdot_block(*this);
  const int nrhs = n_rows(norm2);
  std::vector<Real> res(nrhs);
  for(auto i = 0; i < nrhs; i++){
    res[i] = std::sqrt(std::real(norm2(i, i)));
  }
  return res;
}
// Norms:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Centralization][Centralization:1]]
Vect centralize(const int root = -1) const {
  std::shared_ptr<Process> p = check_proc_ptr();
  std::map<int, DenseMatrix<Scalar>> send_buffers;
  for(auto& [sd_id, loc_v] : this->_loc_data){
    send_buffers[sd_id] = loc_v;
  }
  p->subdomain_disassemble<DenseMatrix<Scalar>>(send_buffers, this->_on_interface);

  return p->subdomain_centralize(send_buffers, root, this->_on_interface);
}
// Centralization:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Apply function to values][Apply function to values:1]]
#if defined(MAPHYSPP_USE_MULTITHREAD)
void apply(std::function<void(Scalar&)> fct){
  this->_proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for(const auto& sd_id : sds) {
      Scalar* values = get_ptr(this->_loc_data[sd_id]);
      const Size sz = n_rows(this->_loc_data[sd_id]) * n_cols(this->_loc_data[sd_id]);
      for (Size k = 0; k < sz; k++)
        fct(values[k]);
    }
  });
}
#else
void apply(std::function<void(Scalar&)> fct){
  for(auto& v : this->_loc_data){
    Scalar * values = get_ptr(v.second);
    const Size sz = n_rows(v.second) * n_cols(v.second);
    for(Size k = 0; k < sz; ++k){
      fct(values[k]);
    }
  }
}
#endif
// Apply function to values:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Apply function to values][Apply function to values:2]]
#if defined(MAPHYSPP_USE_MULTITHREAD)
template<class OtherV = Vect>
void apply(std::function<void(Scalar&, const Scalar&)> fct, const PartVector<OtherV>& other){
  std::shared_ptr<Process> p = check_proc_ptr();
  MAPHYSPP_ASSERT(other.is_on_proc(*p), "Error: PartVector apply with vectors on different procs.");
  this->_proc->parallel_run([&, this] (int, std::vector<int>& sds) {
    for(const auto& sd_id : sds) {
      const Vect& other_loc = other.get_local_vector(sd_id);
      Vect& my_loc = this->_loc_data[sd_id];
      MAPHYSPP_DIM_ASSERT(n_rows(my_loc), n_rows(other_loc), "PartVector::apply: different row number");
      MAPHYSPP_DIM_ASSERT(n_cols(my_loc), n_cols(other_loc),
                          "PartVector::apply: different col number");
      for (Size j = 0; j < n_cols(other_loc); ++j) {
        for (Size i = 0; i < n_rows(other_loc); ++i) {
          fct(my_loc(i, j), other_loc(i, j));
        }
      }
    }
  });
}
#else
template<class OtherV = Vect>
void apply(std::function<void(Scalar&, const Scalar&)> fct, const PartVector<OtherV>& other){
  std::shared_ptr<Process> p = check_proc_ptr();
  MAPHYSPP_ASSERT(other.is_on_proc(*p), "Error: PartVector apply with vectors on different procs.");
  for(auto& v : this->_loc_data){
    const int sd_id = v.first;

    const Vect& other_loc = other._loc_data.at(sd_id);
    Vect& my_loc = v.second;

    MAPHYSPP_DIM_ASSERT(n_rows(my_loc), n_rows(other_loc), "PartVector::apply: different row number");
    MAPHYSPP_DIM_ASSERT(n_cols(my_loc), n_cols(other_loc), "PartVector::apply: different col number");
    for(Size j = 0; j < n_cols(other_loc); ++j){
      for(Size i = 0; i < n_rows(other_loc); ++i){
        fct(my_loc(i, j), other_loc(i, j));
      }
    }
  }
}
#endif
// Apply function to values:2 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Cast][Cast:1]]
template<MPH_Scalar OtherScalar>
[[nodiscard]] auto cast() const {
  using CastedLocVector = decltype(Vect().template cast<OtherScalar>());
  using CastedPartVector = PartVector<CastedLocVector>;
  CastedPartVector v_out;
  v_out.initialize(this->_proc);

  v_out.template apply_on_data<Vect>(*this, [](CastedLocVector& out, const Vect& loc){
    out = loc.template cast<OtherScalar>();
  });

  return v_out;
}
// Cast:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Local vector type conversion][Local vector type conversion:1]]
template<class OtherV>
[[nodiscard]] PartVector<OtherV> convert(std::function<OtherV(const Vect&)> convert_fct) const {
  check_proc_ptr();
  PartVector<OtherV> out_distvect(this->_proc, this->_on_interface);
  for(const auto& my_vects : this->_loc_data){
    const int sd_id = my_vects.first;
    const Vect& v = my_vects.second;
    out_distvect.add_subdomain(sd_id, convert_fct(v));
  }
  return out_distvect;
}

// Not really a move...
template<class OtherV>
[[nodiscard]] PartVector<OtherV> move_convert(std::function<OtherV(const Vect&)> convert_fct) {
  PartVector<OtherV> out_distvect = this->convert(convert_fct);
  this->_loc_data.clear();
  this->_on_interface = false;
  return out_distvect;
}
// Local vector type conversion:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Display functions][Display functions:1]]
void display_centralized(const std::string& name="", std::ostream &out = std::cout, const int root = 0) const {
  std::shared_ptr<Process> p = check_proc_ptr();
  Vect v_centr = centralize(root);
  if( p->rank() != root) return;

  if(!name.empty()) out << name;
  if(this->_on_interface) out << " (on interface)";
  if(!_compatible) out << " (not compatible)";
  out << '\n';
  maphys::display(v_centr, "", out);
}
// Display functions:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Display functions][Display functions:2]]
void display_local(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << name;
  if(this->_on_interface) out << " (on interface)";
  if(!_compatible) out << " (not compatible)";
  out << '\n';
  out << "Number of local vectors: " << (this->_loc_data).size() << '\n';
  for(const auto& [sd_id, loc_vect] : this->_loc_data){
    std::cerr << "ptr: " << &loc_vect[0] << '\n';
    out << "Local vector on subdomain " << sd_id << '\n';
    maphys::display(loc_vect, "", out);
  }
}

void display(const std::string& name="", std::ostream &out = std::cout) const { display_local(name, out); }
friend void display(const PartVector& v, const std::string& name="", std::ostream &out = std::cout){ v.display_local(name, out); }
// Display functions:2 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Getters][Getters:1]]
[[nodiscard]] Vect& get_local_vector(const int sd_id){
  return this->_loc_data.at(sd_id);
}

[[nodiscard]] const Vect& get_local_vector(const int sd_id) const {
  return this->_loc_data.at(sd_id);
}

[[nodiscard]] Size get_size() const {
  return this->_proc->get_n_glob();
}

[[nodiscard]] Size get_n_rows() const {
  //if(this->_loc_data.size() == 0) return 0;
  return this->_proc->get_n_glob();
}

[[nodiscard]] Size get_n_cols() const {
  if(this->_loc_data.size() == 0) return 0;
  return n_cols(this->_loc_data.begin()->second);
}

[[nodiscard]] Size get_nb_row() const { return get_n_rows(); }
[[nodiscard]] Size get_nb_col() const { return get_n_cols(); }

[[nodiscard]] inline bool compatible() const {
  return _compatible;
}

[[nodiscard]] inline bool is_view() const {
  return this->_loc_data.begin()->second.is_ptr_fixed();
}

private:
template<typename OtherVect = Vect>
[[nodiscard]] PartVector<OtherVect> _get_vect(const int j, const int nb_col, const bool is_view) const {
  std::shared_ptr<Process> p = check_proc_ptr();
  PartVector<OtherVect> res(p, this->_on_interface, _compatible);
  if(this->get_n_cols() == 0) return res;
  MAPHYSPP_ASSERT(nb_col > 0, "PartVector: can't get subvector of negative size");
  MAPHYSPP_ASSERT(j >= 0, "PartVector: can't get subvector of negative index");
  MAPHYSPP_ASSERT(nb_col + j <= static_cast<int>(this->get_n_cols()), "PartVector: can't get subvector with offset and size too big");

  for(auto& [id, data] : this->_loc_data){
    if(is_view){
      res.add_subdomain(id, data.get_columns_view(j, j + nb_col));
    } else {
      res.add_subdomain(id, data.get_block_copy(0, j, n_rows(data), nb_col));
    }
  }
  return res;
}

public:
template<MPH_Integral Tint, typename OtherVect = Vect>
[[nodiscard]] PartVector<OtherVect> get_vect_view(const Tint j, const Tint nb_col = 1) const {
  return _get_vect<OtherVect>(j, nb_col, true);
}

template<MPH_Integral Tint, typename OtherVect = Vect>
[[nodiscard]] PartVector<OtherVect> get_vect_view(const Tint j, const Tint nb_col = 1){
  return _get_vect<OtherVect>(j, nb_col, true);
}

template<MPH_Integral Tint, typename OtherVect = Vect>
[[nodiscard]] friend PartVector<Vect> get_vect_view(PartVector<OtherVect>& v, const Tint j, const Tint nb_col = 1){
  return v.get_vect_view(j, nb_col);
}

template<MPH_Integral Tint, typename OtherVect = Vect>
[[nodiscard]] PartVector<OtherVect> get_vect(const Tint j, const Tint nb_col = 1) const {
  return _get_vect<OtherVect>(j, nb_col, false);
}
// Getters:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Setters][Setters:1]]
inline void set_compatible(bool compatible = true){
  _compatible = compatible;
}

inline void set_on_intrf(bool on_intrf = true){
  if(this->_on_interface != on_intrf) { // need_distrution_change
    for(const auto& sd_id : this->_proc->get_sd_ids()){
      auto &sd_data = this->_loc_data[sd_id];
      auto sd_m = this->_proc->get_n_dofs(sd_id, on_intrf);
      sd_data = Vect(sd_m, 0);
    }
  }
  this->_on_interface = on_intrf;
}
// Setters:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Generation][Generation:1]]
void set_to_ones(){
  std::shared_ptr<Process> p = check_proc_ptr();
  IndexArray<int> sd_ids = p->get_sd_ids();
  for(int id : sd_ids){
    const Size size = this->get_buffer_size(id);
    Vect v(size);
    for(Size k = 0; k < size; ++k){
      v(k) = Scalar{1.0};
    }
    this->add_subdomain(id, std::move(v));
  }
}
// Generation:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Generation][Generation:2]]
void set_to_arange(Scalar start = 0, Scalar step = 1){
  std::shared_ptr<Process> p = check_proc_ptr();
  this->_loc_data = std::map<int, Vect>();
  IndexArray<int> sd_ids = p->get_sd_ids();
  for(const int id : sd_ids){
    const int size = p->get_n_dofs(id, this->_on_interface);
    std::vector<int> range(size);
    for(int k = 0; k < size; ++k){
      range[k] = k;
    }
    p->local_to_global(id, range, this->_on_interface);

    Vect rangeval(size);
    for(int k = 0; k < size; ++k){
      rangeval[k] = step * range[k] + start;
    }
    this->add_subdomain(id, std::move(rangeval));
  }
}
// Generation:2 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*View][View:1]]
template<typename OtherVect>
static PartVector<Vect> view(PartVector<OtherVect>& other){
  PartVector<Vect> out(other.get_proc());
  for(auto& [id, data] : other.get_data_map()){
    out._loc_data[id] = Vect::view(data);
  }
  return out;
}

template<typename OtherVect>
void update_view(PartVector<OtherVect>& other){
  std::shared_ptr<Process> p = check_proc_ptr();
  MAPHYSPP_ASSERT(other.is_on_proc(*p), "Error: PartVector update_view with vectors on different procs.");
  this->_loc_data.clear();
  for(auto& [id, data] : other.get_data_map()){
    this->_loc_data[id] = Vect::view(data);
  }
}
// View:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Comparison][Comparison:1]]
bool operator==(const PartVector& other) const {
  if( this->_proc != other._proc) return false;
  if( this->_on_interface != other._on_interface) return false;
  if( _compatible != other._compatible) return false;
  return (this->_loc_data == other._loc_data);
}

bool operator!=(const PartVector& other) const { return !(*this == other); }
// Comparison:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Out of class operations][Out of class operations:1]]
}; // class PartVector
// Out of class operations:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Out of class operations][Out of class operations:2]]
// Scalar multiplication
template<class Vect, class Scalar, typename = std::enable_if_t<std::is_same<Scalar, typename scalar_type<Vect>::type>::value>> [[nodiscard]]
inline PartVector<Vect> operator* (PartVector<Vect> vect, const Scalar& scal){
  vect *= scal;
  return vect;
}

template<class Vect, class Scalar, typename = std::enable_if_t<std::is_same<Scalar, typename scalar_type<Vect>::type>::value>> [[nodiscard]]
inline PartVector<Vect> operator* (Scalar scal, const PartVector<Vect>& vect){
  return vect * scal;
}

// Matrix multiplication
template<typename Vect, int NbCol, typename Scalar = typename Vect::scalar_type>
PartVector<Vect> operator* (const PartVector<Vect>& A, const DenseMatrix<Scalar, NbCol>& B){
  PartVector<Vect> res{A, B.get_n_cols()};
  std::function<void(Vect&, const Vect&)> product_B = [&] (Vect& data, const Vect& data2){ data = data2 * B; };
  res.apply_on_data(A, product_B);
  return res;
}
template<class Vect, class Scalar, typename = std::enable_if_t<std::is_same<Scalar, typename scalar_type<Vect>::type>::value>> [[nodiscard]]
inline PartVector<Vect> operator/ (PartVector<Vect> vect, const Scalar& scal){
  vect *= (Scalar{1.0}/scal);
  return vect;
}

// PartVector addition
template<class Vect> [[nodiscard]]
inline PartVector<Vect> operator+ (PartVector<Vect> A, const PartVector<Vect>& B){
  A += B;
  return A;
}

// PartVector substraction
template<class Vect> [[nodiscard]]
inline PartVector<Vect> operator- (PartVector<Vect> A, const PartVector<Vect>& B){
  A -= B;
  return A;
}

// PartVector dot product
template<class Vect, class Vect2, MPH_Scalar Scalar> [[nodiscard]]
inline Scalar dot(const PartVector<Vect> A, const PartVector<Vect2>& B){
  return A.distdot(B);
}

// PartVector dot product
template<class Vect, class Vect2> [[nodiscard]]
inline auto dot_block(const PartVector<Vect> A, const PartVector<Vect2>& B){
  return A.distdot_block(B);
}

// Size is assumed to be the global size
template<class Vect>
inline int size(const PartVector<Vect> V){
  return V.get_size();
}
// Out of class operations:2 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Interface functions][Interface functions:1]]
template<typename Vector>
[[nodiscard]] inline int n_rows(const PartVector<Vector>& vect) { return vect.get_n_rows(); }

template<typename Vector>
[[nodiscard]] inline int n_cols(const PartVector<Vector>& vect) { return vect.get_n_cols(); }
// Interface functions:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Traits][Traits:1]]
template<typename Vect> struct is_distributed<PartVector<Vect>> : public std::true_type {};

template<typename Vect> struct scalar_type<PartVector<Vect>> : public std::true_type {
  using type = typename scalar_type<Vect>::type;
};

template<typename Vect> struct local_type<PartVector<Vect>> : public std::true_type {
  using type = Vect;
};

template<typename Vect> struct dense_type<PartVector<Vect>> : public std::true_type {
  using type = PartVector<typename dense_type<Vect>::type>;
};
// Traits:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
