// [[file:../../../org/maphys/testing/TestMatrix.org::*Header][Header:1]]
#pragma once
#ifdef MAPHYSPP_USE_EIGEN
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <maphys/wrappers/Eigen/Eigen.hpp>
#endif

#include <random>

#ifdef MAPHYSPP_USE_ARMA
#include <armadillo>
#include <maphys/wrappers/armadillo/Armadillo.hpp>
#endif

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/loc_data/SparseMatrixCOO.hpp"
#include "maphys/loc_data/SparseMatrixCSC.hpp"
#include "maphys/loc_data/Laplacian.hpp"

#ifndef MAPHYSPP_NO_MPI
#include "maphys/part_data/PartMatrix.hpp"
#endif

namespace maphys {
namespace test_matrix {

using S_complex = std::complex<float>;
using Z_complex = std::complex<double>;
// Header:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Dense][Dense:2]]
template<class Scalar, class Matrix>
struct general_matrix : public std::false_type {
  Matrix matrix;
  general_matrix(){ MAPHYSPP_ASSERT(false, "general_matrix created with wrong matrix type"); }
}; // general_matrix

template<class Scalar>
struct general_matrix<Scalar, DenseMatrix<Scalar>> : public std::true_type {
  DenseMatrix<Scalar> matrix;
  general_matrix(){
    matrix = DenseMatrix<Scalar>({3, 2, 0, 1,
                                  1, 5, 0, 1,
                                  2, 2, 6, 1,
                                  1, 1, 1, 8}, 4, 4, true);
    if constexpr(is_complex<Scalar>::value){
      DenseMatrix<Scalar> m_img({1, 1, 0, 0,
                                 1, 1, 2, 0,
                                 0, 1, 0, 0,
                                 1, 1, 2, 2}, 4, 4, true);
      matrix += (Scalar{0, 1} * m_img);
    }
  }
}; // genral_matrix
// Dense:2 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Sparse COO][Sparse COO:1]]
template<class Scalar>
struct general_matrix<Scalar, SparseMatrixCOO<Scalar>> : public std::true_type {
  SparseMatrixCOO<Scalar> matrix;
  general_matrix(){
    DenseMatrix<Scalar> m_dense = general_matrix<Scalar, DenseMatrix<Scalar>>().matrix;
    matrix = SparseMatrixCOO<Scalar>(m_dense);
  }
}; // genral_matrix
// Sparse COO:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Sparse CSC][Sparse CSC:1]]
template<class Scalar>
struct general_matrix<Scalar, SparseMatrixCSC<Scalar>> : public std::true_type {
  SparseMatrixCSC<Scalar> matrix;
  general_matrix(){
    DenseMatrix<Scalar> m_dense = general_matrix<Scalar, DenseMatrix<Scalar>>().matrix;
    matrix = SparseMatrixCSC<Scalar>(m_dense);
  }
}; // genral_matrix
// Sparse CSC:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Dense Eigen][Dense Eigen:1]]
#ifdef MAPHYSPP_USE_EIGEN
template<class Scalar>
struct general_matrix<Scalar, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> : public std::true_type {
  Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> matrix;
  general_matrix(){
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m(4, 4);
    m << 3, 2, 0, 1,
	 1, 5, 0, 1,
	 2, 2, 6, 1,
	 1, 1, 1, 8;
    matrix = m;
    if constexpr(is_complex<Scalar>::value){
      Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m_img(4, 4);
      m_img << 1, 1, 0, 0,
	       1, 1, 2, 0,
	       0, 1, 0, 0,
	       1, 1, 2, 2;
      matrix += (Scalar{0, 1} * m_img);
    }
  }
}; // genral_matrix
// Dense Eigen:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Sparse Eigen][Sparse Eigen:1]]
template<class Scalar>
struct general_matrix<Scalar, Eigen::SparseMatrix<Scalar>> : public std::true_type {
  Eigen::SparseMatrix<Scalar> matrix;
  general_matrix(){
    std::vector<int> i{0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 3, 0, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{3, 2, 1, 1, 5, 1, 2, 2, 6, 1, 1, 1, 1, 8};
    build_matrix(matrix, 4, 4, static_cast<int>(i.size()), i.data(), j.data(), v.data());
    if constexpr(is_complex<Scalar>::value){
      Eigen::SparseMatrix<Scalar> m_img;
      std::vector<int> i2{0, 0, 1, 1, 1, 2, 3, 3, 3, 3};
      std::vector<int> j2{0, 1, 0, 1, 2, 1, 0, 1, 2, 3};
      std::vector<Scalar> v2{1, 1, 1, 1, 2, 1, 1, 1, 2, 2};
      build_matrix(m_img, 4, 4, static_cast<int>(i2.size()), i2.data(), j2.data(), v2.data());
      matrix += (Scalar{0, 1} * m_img);
    }
  }
}; // genral_matrix
#endif // MAPHYSPP_USE_EIGEN
// Sparse Eigen:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Dense Armadillo][Dense Armadillo:1]]
#ifdef MAPHYSPP_USE_ARMA
template<class Scalar>
struct general_matrix<Scalar, arma::Mat<Scalar>> : public std::true_type {
  arma::Mat<Scalar> matrix;
  general_matrix(){
    matrix = {{3, 2, 0, 1,
               1, 5, 0, 1,
               2, 2, 6, 1,
               1, 1, 1, 8}};
    matrix.reshape(4, 4);
    if constexpr(is_complex<Scalar>::value){
      arma::Mat<Scalar> m_img = {{1, 1, 0, 0,
                                  1, 1, 2, 0,
                                  0, 1, 0, 0,
                                  1, 1, 2, 2}};
      m_img.reshape(4, 4);
      matrix += (Scalar{0, 1} * m_img);
    }
  }
}; // genral_matrix
// Dense Armadillo:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Sparse Armadillo][Sparse Armadillo:1]]
template<class Scalar>
struct general_matrix<Scalar, arma::SpMat<Scalar>> : public std::true_type {
  arma::SpMat<Scalar> matrix;
  general_matrix(){
    std::vector<int> i{0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 3, 0, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{3, 2, 1, 1, 5, 1, 2, 2, 6, 1, 1, 1, 1, 8};
    build_matrix(matrix, 4, 4, static_cast<int>(i.size()), i.data(), j.data(), v.data());
    if constexpr(is_complex<Scalar>::value){
      arma::SpMat<Scalar> m_img;
      std::vector<int> i2{0, 0, 1, 1, 1, 2, 3, 3, 3, 3};
      std::vector<int> j2{0, 1, 0, 1, 2, 1, 0, 1, 2, 3};
      std::vector<Scalar> v2{1, 1, 1, 1, 2, 1, 1, 1, 2, 2};
      build_matrix(m_img, 4, 4, static_cast<int>(i2.size()), i2.data(), j2.data(), v2.data());
      matrix += (Scalar{0, 1} * m_img);
    }
  }
}; // genral_matrix
#endif // MAPHYSPP_USE_ARMA
// Sparse Armadillo:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*SPD dense matrix][SPD dense matrix:1]]
template<class Scalar, class Matrix>
Matrix spd_matrix() {}

#define TEST_MATRIX_DM_SPD( _type ) template<>                          \
  DenseMatrix<_type> spd_matrix<_type, DenseMatrix<_type>>(){           \
    DenseMatrix<_type> out = general_matrix<_type, DenseMatrix<_type> >().matrix; \
    out = adjoint(out) * out;                                           \
    out.set_spd(MatrixStorage::full);                                   \
    return out;                                                         \
  }

TEST_MATRIX_DM_SPD(float)
TEST_MATRIX_DM_SPD(double)
TEST_MATRIX_DM_SPD(S_complex )
TEST_MATRIX_DM_SPD(Z_complex )
// SPD dense matrix:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*COO spd matrix][COO spd matrix:1]]
#define TEST_MATRIX_COO_SPD( _type ) template<> \
SparseMatrixCOO<_type> spd_matrix<_type, SparseMatrixCOO<_type> >(){ \
  return SparseMatrixCOO<_type>(spd_matrix<_type, DenseMatrix<_type> >()); \
}

TEST_MATRIX_COO_SPD(double)
TEST_MATRIX_COO_SPD(float)
TEST_MATRIX_COO_SPD(S_complex )
TEST_MATRIX_COO_SPD(Z_complex )
// COO spd matrix:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*1D-laplacian][1D-laplacian:1]]
template<class Scalar, class Matrix>
struct laplacian_1D_matrix : public std::false_type {
  Matrix matrix;
  laplacian_1D_matrix(int m){ (void) m; MAPHYSPP_ASSERT(false, "laplacian_1D_matrix created with wrong Matrix type"); }
}; // laplacian_1D_matrix

template<class Scalar>
struct laplacian_1D_matrix<Scalar, Laplacian<Scalar>> : public std::true_type {
  Laplacian<Scalar> matrix;
  laplacian_1D_matrix(int m){
    (void) m;
  }
}; // laplacian_1D_matrix

template<class Scalar>
struct laplacian_1D_matrix<Scalar, DenseMatrix<Scalar>> : public std::true_type {
  DenseMatrix<Scalar> matrix;
  laplacian_1D_matrix(int m){
    matrix = DenseMatrix<Scalar>(m, m);
    Scalar * vals = matrix.get_ptr();
    for(auto i = 0; i < m-1; ++i){
      vals[i*m+i] = 2.0;
      vals[i*m+i+1] = -1.0;
      vals[(i+1)*m+i] = -1.0;
    }
    vals[m*m-1] = 2.0;
    matrix.set_spd(MatrixStorage::full);
  }
}; // laplacian_1D_matrix

template<class Scalar>
struct laplacian_1D_matrix<Scalar, SparseMatrixCOO<Scalar>> : public std::true_type {
  SparseMatrixCOO<Scalar> matrix;
  laplacian_1D_matrix(int m){
    matrix = SparseMatrixCOO<Scalar>(laplacian_1D_matrix<Scalar, DenseMatrix<Scalar>>(m).matrix);
  }
}; // laplacian_1D_matrix

template<class Scalar>
struct laplacian_1D_matrix<Scalar, SparseMatrixCSC<Scalar>> : public std::true_type {
  SparseMatrixCSC<Scalar> matrix;
  laplacian_1D_matrix(int m){
    matrix = SparseMatrixCSC<Scalar>(laplacian_1D_matrix<Scalar, DenseMatrix<Scalar>>(m).matrix);
  }
}; // laplacian_1D_matrix

#ifdef MAPHYSPP_USE_EIGEN
template<class Scalar>
struct laplacian_1D_matrix<Scalar, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> : public std::true_type {
  Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> matrix;
  laplacian_1D_matrix(int m){
    matrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>(m, m);
    matrix *= Scalar{0};
    for(auto i = 0; i < m-1; ++i){
      matrix(i, i) = 2.0;
      matrix(i, i+1) = -1.0;
      matrix(i+1, i) = -1.0;
    }
    matrix(m-1, m-1) = 2.0;
    //matrix.set_spd(MatrixStorage::full);
  }
}; // laplacian_1D_matrix

template<class Scalar>
struct laplacian_1D_matrix<Scalar, Eigen::SparseMatrix<Scalar>> : public std::true_type {
  Eigen::SparseMatrix<Scalar> matrix;
  laplacian_1D_matrix(int m){
    const int nnz = m + 2*(m-1);
    std::vector<int> i(nnz);
    std::vector<int> j(nnz);
    std::vector<Scalar> v(nnz);
    int k = 0;
    for(auto c = 0; c < m-1; ++c){
      i[k] = c;   j[k] = c;   v[k] = 2.0; k++;
      i[k] = c+1; j[k] = c;   v[k] = -1.0; k++;
      i[k] = c;   j[k] = c+1; v[k] = -1.0; k++;
    }
    i[k] = m-1; j[k] = m-1; v[k] = 2.0;
    build_matrix(matrix, 4, 4, static_cast<int>(i.size()), i.data(), j.data(), v.data());
    //matrix.set_spd(MatrixStorage::full);
  }
}; // laplacian_1D_matrix
#endif // MAPHYSPP_USE_EIGEN

#ifdef MAPHYSPP_USE_ARMA
template<class Scalar>
struct laplacian_1D_matrix<Scalar, arma::Mat<Scalar>> : public std::true_type {
  arma::Mat<Scalar> matrix;
  laplacian_1D_matrix(int m){
    matrix = arma::Mat<Scalar>(m, m, arma::fill::zeros);
    for(auto i = 0; i < m-1; ++i){
      matrix(i, i) = 2.0;
      matrix(i, i+1) = -1.0;
      matrix(i+1, i) = -1.0;
    }
    matrix(m-1, m-1) = 2.0;
    //matrix.set_spd(MatrixStorage::full);
  }
}; // laplacian_1D_matrix

template<class Scalar>
struct laplacian_1D_matrix<Scalar, arma::SpMat<Scalar>> : public std::true_type {
  arma::SpMat<Scalar> matrix;
  laplacian_1D_matrix(int m){
    const int nnz = m + 2*(m-1);
    std::vector<int> i(nnz);
    std::vector<int> j(nnz);
    std::vector<Scalar> v(nnz);
    int k = 0;
    for(auto c = 0; c < m-1; ++c){
      i[k] = c;   j[k] = c;   v[k] = 2.0; k++;
      i[k] = c+1; j[k] = c;   v[k] = -1.0; k++;
      i[k] = c;   j[k] = c+1; v[k] = -1.0; k++;
    }
    i[k] = m-1; j[k] = m-1; v[k] = 2.0;
    build_matrix(matrix, 4, 4, static_cast<int>(i.size()), i.data(), j.data(), v.data());
    //matrix.set_spd(MatrixStorage::full);
  }
}; // laplacian_1D_matrix
#endif // MAPHYSPP_USE_ARMA
// 1D-laplacian:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Distributed topology][Distributed topology:1]]
#ifndef MAPHYSPP_NO_MPI
// Distributed topology:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Distributed topology][Distributed topology:2]]
std::shared_ptr<Process> get_distr_process(){
  using Nei_map = std::map<int, IndexArray<int>>;
  const int n_dofs = 4;

  // SD 1
  Nei_map NM0 {{1, {2, 3}},
               {2, {0, 3}}};

  // SD 2
  Nei_map NM1 {{0, {1, 0}},
               {2, {0, 3}}};

  // SD 3
  Nei_map NM2 {{0, {2, 3}},
               {1, {3, 0}}};


  std::vector<Subdomain> sd;
  sd.emplace_back(0, n_dofs, std::move(NM0), false);
  sd.emplace_back(1, n_dofs, std::move(NM1), false);
  sd.emplace_back(2, n_dofs, std::move(NM2), false);

  std::shared_ptr<Process> p = bind_subdomains(static_cast<int>(sd.size()));
  p->load_subdomains(sd);

  return p;
}
// Distributed topology:2 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Distributed general matrix][Distributed general matrix:1]]
template<class Scalar, class LocMatrix>
struct dist_general_matrix : public std::false_type {
  PartMatrix<LocMatrix> matrix;
  dist_general_matrix(std::shared_ptr<Process>){ MAPHYSPP_ASSERT(false, "dist_general_matrix created with wrong LocMatrix type"); }
}; // dist_general_matrix
// Distributed general matrix:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Global matrix][Global matrix:1]]
template<class Scalar, class Matrix>
Matrix dist_general_matrix_global(){}

template<>
DenseMatrix<double> dist_general_matrix_global<double, DenseMatrix<double>>(){
  return DenseMatrix<double>({ 15. ,-1.,  0., -2.,  0.,  0., -1.,
                               -2. , 6., -2., -1.,  0.,  0.,  0.,
                                0. ,-1., 14., -3., -2., -1.,  0.,
                               -4. ,-1., -4., 21.,  0., -2., -1.,
                                0. , 0., -1.,  0.,  8., -1.,  0.,
                                0. , 0., -1., -2., -3., 14., -1.,
                               -2. , 0.,  0., -1.,  0., -2.,  6.}, 7, 7, true);
}

template<>
DenseMatrix<float> dist_general_matrix_global<float, DenseMatrix<float>>(){
  return dist_general_matrix_global<double, DenseMatrix<double> >().cast<float>();
}

template<>
DenseMatrix<Z_complex> dist_general_matrix_global<Z_complex, DenseMatrix<Z_complex>>(){
  return DenseMatrix<Z_complex>({
  {15,0}, {-2,1}, {0,0}, {-4,3}, {0,0}, {0,0}, {-2,3},
  {-1,-1}, {6,1}, {-1,0}, {-1,0}, {0,0}, {0,0}, {0,0},
  {0,0}, {-2,3}, {14,1}, {-4,0}, {-1,0}, {-1,0}, {0,0},
  {-2,0}, {-1,1}, {-3,1}, {21,-2}, {0,0}, {-2,2}, {-1,1},
  {0,0}, {0,0}, {-2,3}, {0,0}, {8,0}, {-3,1}, {0,0},
  {0,0}, {0,0}, {-1,1}, {-2,2}, {-1,0},{14,-1}, {-2,1},
  {-1,0}, {0,0}, {0,0}, {-1,0}, {0,0}, {-1,-1}, {6,1} }, 7, 7);
}

template<>
DenseMatrix<S_complex> dist_general_matrix_global<S_complex, DenseMatrix<S_complex>>(){
  return dist_general_matrix_global<Z_complex, DenseMatrix<Z_complex> >().cast<S_complex>();
}

// Sparse matrices
#define TEST_MATRIX_DIST_GENRAL_GLOB( _matrixtype, _type ) template<> \
_matrixtype<_type> dist_general_matrix_global<_type, _matrixtype<_type>>(){ \
  return _matrixtype<_type>(dist_general_matrix_global<_type, DenseMatrix<_type> >()); \
}
TEST_MATRIX_DIST_GENRAL_GLOB( SparseMatrixCOO, float )
TEST_MATRIX_DIST_GENRAL_GLOB( SparseMatrixCOO, double )
TEST_MATRIX_DIST_GENRAL_GLOB( SparseMatrixCOO, S_complex )
TEST_MATRIX_DIST_GENRAL_GLOB( SparseMatrixCOO, Z_complex )
TEST_MATRIX_DIST_GENRAL_GLOB( SparseMatrixCSC, float )
TEST_MATRIX_DIST_GENRAL_GLOB( SparseMatrixCSC, double )
TEST_MATRIX_DIST_GENRAL_GLOB( SparseMatrixCSC, S_complex )
TEST_MATRIX_DIST_GENRAL_GLOB( SparseMatrixCSC, Z_complex )
// Global matrix:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*With local dense matrix][With local dense matrix:1]]
template<class Scalar>
struct dist_general_matrix<Scalar, DenseMatrix<Scalar>>{

  PartMatrix<DenseMatrix<Scalar>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p){
    //NB column-major filling
    DenseMatrix<Scalar> m({ 7, -2,  0, -1,
                           -1,  6, -1, -1,
                            0, -2,  8, -3,
                           -1, -1, -1,  7}, 4, 4);

    // Add imaginary part for complex
    if constexpr(is_complex<Scalar>::value){
      DenseMatrix<Scalar> m_img({ 0,  1,  0,  2,
                                 -1,  1,  0,  0,
                                  0,  3,  0,  1,
                                  0,  1,  0, -1}, 4, 4);
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, DenseMatrix<Scalar>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<DenseMatrix<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
// With local dense matrix:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*With local sparse matrix COO][With local sparse matrix COO:1]]
template<class Scalar>
struct dist_general_matrix<Scalar, SparseMatrixCOO<Scalar>>{
  using Mat = SparseMatrixCOO<Scalar>;
  PartMatrix<Mat> matrix;

  dist_general_matrix(std::shared_ptr<Process> p){
    PartMatrix<DenseMatrix<Scalar>> m_dense = dist_general_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
    auto convert_fct = [](const DenseMatrix<Scalar>& densemat){ return Mat(densemat); };
    matrix = m_dense.template convert<Mat>(convert_fct);
  }
}; // dist_general_matrix
// With local sparse matrix COO:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*With local sparse matrix CSC][With local sparse matrix CSC:1]]
template<class Scalar>
struct dist_general_matrix<Scalar, SparseMatrixCSC<Scalar>>{
  using Mat = SparseMatrixCSC<Scalar>;
  PartMatrix<Mat> matrix;

  dist_general_matrix(std::shared_ptr<Process> p){
    PartMatrix<SparseMatrixCOO<Scalar>> m_coo = dist_general_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
    auto convert_fct = [](const SparseMatrixCOO<Scalar>& coomat){ return coomat.to_csc(); };
    matrix = m_coo.template convert<Mat>(convert_fct);
  }
}; // dist_general_matrix
// With local sparse matrix CSC:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*With local dense matrix Eigen][With local dense matrix Eigen:1]]
#ifdef MAPHYSPP_USE_EIGEN
template<class Scalar>
struct dist_general_matrix<Scalar, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>>{

  PartMatrix<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p){
    //NB column-major filling
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m(Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>(4, 4));
    m << 7, -2,  0, -1,
        -1,  6, -1, -1,
         0, -2,  8, -3,
        -1, -1, -1,  7;

    // Add imaginary part for complex
    if constexpr(is_complex<Scalar>::value){
      Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m_img(Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>(4, 4));
      m_img << 0,  1,  0,  2,
              -1,  1,  0,  0,
               0,  3,  0,  1,
               0,  1,  0, -1;
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>>(p, std::move(map));
  }
}; // dist_general_matrix
// With local dense matrix Eigen:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*With local sparse matrix Eigen][With local sparse matrix Eigen:1]]
template<class Scalar>
struct dist_general_matrix<Scalar, Eigen::SparseMatrix<Scalar>>{

  PartMatrix<Eigen::SparseMatrix<Scalar>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p){
    std::vector<int> i{0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{7, -2, -1, -1, 6, -1, -1, -2, 8, -3, -1, -1, -1, 7};
    Eigen::SparseMatrix<Scalar> m;
    build_matrix(m, 4, 4, static_cast<int>(i.size()), i.data(), j.data(), v.data());

    // Add imaginary part for complex
    if constexpr(is_complex<Scalar>::value){
      std::vector<int> i2{0, 0, 1, 1, 2, 2, 3, 3};
      std::vector<int> j2{1, 3, 0, 1, 1, 3, 1, 3};
      std::vector<Scalar> v2{1, 2, -1, 1, 3, 1, 1, -1};
      Eigen::SparseMatrix<Scalar> m_img;
      build_matrix(m_img, 4, 4, static_cast<int>(i2.size()), i2.data(), j2.data(), v2.data());
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, Eigen::SparseMatrix<Scalar>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<Eigen::SparseMatrix<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
#endif
// With local sparse matrix Eigen:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*With local dense matrix Armadillo][With local dense matrix Armadillo:1]]
#ifdef MAPHYSPP_USE_ARMA
template<class Scalar>
struct dist_general_matrix<Scalar, arma::Mat<Scalar>>{

  PartMatrix<arma::Mat<Scalar>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p){
    //NB column-major filling
    arma::Mat<Scalar> m = {{ 7, -2,  0, -1,
                            -1,  6, -1, -1,
                             0, -2,  8, -3,
                            -1, -1, -1,  7}};
    m.reshape(4, 4);
    // Add imaginary part for complex
    if constexpr(is_complex<Scalar>::value){
      arma::Mat<Scalar> m_img = {{ 0,  1,  0,  2,
                                  -1,  1,  0,  0,
                                   0,  3,  0,  1,
                                   0,  1,  0, -1}};
      m_img.reshape(4, 4);
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, arma::Mat<Scalar>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<arma::Mat<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
// With local dense matrix Armadillo:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*With local sparse matrix Armadillo][With local sparse matrix Armadillo:1]]
template<class Scalar>
struct dist_general_matrix<Scalar, arma::SpMat<Scalar>>{

  PartMatrix<arma::SpMat<Scalar>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p){
    std::vector<int> i{0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{7, -2, -1, -1, 6, -1, -1, -2, 8, -3, -1, -1, -1, 7};
    arma::SpMat<Scalar> m;
    build_matrix(m, 4, 4, static_cast<int>(i.size()), i.data(), j.data(), v.data());

    // Add imaginary part for complex
    if constexpr(is_complex<Scalar>::value){
      std::vector<int> i2{0, 0, 1, 1, 2, 2, 3, 3};
      std::vector<int> j2{1, 3, 0, 1, 1, 3, 1, 3};
      std::vector<Scalar> v2{1, 2, -1, 1, 3, 1, 1, -1};
      arma::SpMat<Scalar> m_img;
      build_matrix(m_img, 4, 4, static_cast<int>(i2.size()), i2.data(), j2.data(), v2.data());
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, arma::SpMat<Scalar>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<arma::SpMat<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
#endif
// With local sparse matrix Armadillo:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Dense][Dense:1]]
template<class Scalar, class LocMatrix>
struct dist_spd_matrix : public std::false_type {
  PartMatrix<LocMatrix> matrix;
  dist_spd_matrix(std::shared_ptr<Process>){ MAPHYSPP_ASSERT(false, "dist_spd_matrix created with wrong LocMatrix type"); }
}; // dist_spd_matrix

template<class Scalar>
struct dist_spd_matrix<Scalar, DenseMatrix<Scalar>>{

  PartMatrix<DenseMatrix<Scalar>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p){
    DenseMatrix<Scalar> m({ 3, -1,  0, -1,
                           -1,  4, -1, -1,
                            0, -1,  3, -1,
                           -1, -1, -1,  4}, 4, 4);
    m.set_spd(MatrixStorage::full);

    std::map<int, DenseMatrix<Scalar>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<DenseMatrix<Scalar>>(p, std::move(map));
  }
}; // dist_spd_matrix
// Dense:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Sparse COO][Sparse COO:1]]
template<class Scalar>
struct dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>{
  using Mat = SparseMatrixCOO<Scalar>;
  PartMatrix<Mat> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p){
    PartMatrix<DenseMatrix<Scalar>> m_dense = dist_spd_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
    auto convert_fct = [](const DenseMatrix<Scalar>& densemat){ return Mat(densemat); };
    matrix = m_dense.template convert<Mat>(convert_fct);
  }
}; // dist_spd_matrix
// Sparse COO:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Sparse CSC][Sparse CSC:1]]
template<class Scalar>
struct dist_spd_matrix<Scalar, SparseMatrixCSC<Scalar>>{
  using Mat = SparseMatrixCSC<Scalar>;
  PartMatrix<Mat> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p){
    PartMatrix<DenseMatrix<Scalar>> m_dense = dist_spd_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
    auto convert_fct = [](const DenseMatrix<Scalar>& densemat){ return Mat(densemat); };
    matrix = m_dense.template convert<Mat>(convert_fct);
  }
}; // dist_spd_matrix
// Sparse CSC:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Dense Eigen][Dense Eigen:1]]
#ifdef MAPHYSPP_USE_EIGEN
template<class Scalar>
struct dist_spd_matrix<Scalar, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>>{

  PartMatrix<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p){
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m(Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>(4, 4));
    m << 3, -1,  0, -1,
        -1,  4, -1, -1,
         0, -1,  3, -1,
        -1, -1, -1,  4;

    std::map<int, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>>(p, std::move(map));
  }
}; // dist_spd_matrix
// Dense Eigen:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Sparse Eigen][Sparse Eigen:1]]
template<class Scalar>
struct dist_spd_matrix<Scalar, Eigen::SparseMatrix<Scalar>>{

  PartMatrix<Eigen::SparseMatrix<Scalar>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p){
    std::vector<int> i{0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{3, -1, -1, -1, 4, -1, -1, -1, 3, -1, -1, -1, -1, 4};
    Eigen::SparseMatrix<Scalar> m;
    build_matrix(m, 4, 4, static_cast<int>(i.size()), i.data(), j.data(), v.data());

    std::map<int, Eigen::SparseMatrix<Scalar>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<Eigen::SparseMatrix<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
#endif
// Sparse Eigen:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Dense Armadillo][Dense Armadillo:1]]
#ifdef MAPHYSPP_USE_ARMA
template<class Scalar>
struct dist_spd_matrix<Scalar, arma::Mat<Scalar>>{

  PartMatrix<arma::Mat<Scalar>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p){
    //NB column-major filling
    arma::Mat<Scalar> m = {{ 3, -1,  0, -1,
                            -1,  4, -1, -1,
                             0, -1,  3, -1,
                            -1, -1, -1,  4}};
    m.reshape(4, 4);
    std::map<int, arma::Mat<Scalar>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<arma::Mat<Scalar>>(p, std::move(map));
  }
}; // dist_spd_matrix
// Dense Armadillo:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Sparse Armadillo][Sparse Armadillo:1]]
template<class Scalar>
struct dist_spd_matrix<Scalar, arma::SpMat<Scalar>>{

  PartMatrix<arma::SpMat<Scalar>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p){
    std::vector<int> i{0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{3, -1, -1, -1, 4, -1, -1, -1, 3, -1, -1, -1, -1, 4};
    arma::SpMat<Scalar> m;
    build_matrix(m, 4, 4, static_cast<int>(i.size()), i.data(), j.data(), v.data());

    std::map<int, arma::SpMat<Scalar>> map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) map.emplace(k, m);

    matrix = PartMatrix<arma::SpMat<Scalar>>(p, std::move(map));
  }
}; // dist_spd_matrix
#endif
// Sparse Armadillo:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Distributed vector][Distributed vector:1]]
template<class Scalar, class LocVector>
struct dist_vector : public std::false_type {
  PartMatrix<LocVector> vector;
  dist_vector(std::shared_ptr<Process>){ MAPHYSPP_ASSERT(false, "dist_vector created with wrong LocVector type"); }
}; // dist_general_matrix
// Distributed vector:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*MaPHyS vector][MaPHyS vector:1]]
template<class Scalar>
struct dist_vector<Scalar, Vector<Scalar>>{

  PartVector<Vector<Scalar>> vector;

  dist_vector(std::shared_ptr<Process> p){

    std::map<int, Vector<Scalar>> map;
    if(p->owns_subdomain(0)) { map.emplace(0, Vector<Scalar>{{0.0, 1.0, 2.0, 3.0}, 4, 1}); }
    if(p->owns_subdomain(1)) { map.emplace(1, Vector<Scalar>{{3.0, 2.0, 4.0, 5.0}, 4, 1}); }
    if(p->owns_subdomain(2)) { map.emplace(2, Vector<Scalar>{{5.0, 6.0, 0.0, 3.0}, 4, 1}); }

    vector = PartVector<Vector<Scalar>>(p, std::move(map));
  }
}; // struct dist_vector
template<class Scalar>
struct dist_vector<Scalar, DenseMatrix<Scalar,-1>>{

  PartVector<DenseMatrix<Scalar,-1>> vector;

  dist_vector(std::shared_ptr<Process> p){

    std::map<int, DenseMatrix<Scalar,-1>> map;
    if(p->owns_subdomain(0)) { map.emplace(0, DenseMatrix<Scalar,-1>{{0.0, 1.0, 2.0, 3.0}, 4, 1}); }
    if(p->owns_subdomain(1)) { map.emplace(1, DenseMatrix<Scalar,-1>{{3.0, 2.0, 4.0, 5.0}, 4, 1}); }
    if(p->owns_subdomain(2)) { map.emplace(2, DenseMatrix<Scalar,-1>{{5.0, 6.0, 0.0, 3.0}, 4, 1}); }

    vector = PartVector<DenseMatrix<Scalar,-1>>(p, std::move(map));
  }
}; // struct dist_vector
// MaPHyS vector:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Eigen vector][Eigen vector:1]]
#ifdef MAPHYSPP_USE_EIGEN
template<class Scalar>
struct dist_vector<Scalar, Eigen::Matrix<Scalar, Eigen::Dynamic, 1>>{

  PartVector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> vector;
  dist_vector(std::shared_ptr<Process> p){

    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> v0(4); v0 << 0.0, 1.0, 2.0, 3.0;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> v1(4); v1 << 3.0, 2.0, 4.0, 5.0;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> v2(4); v2 << 5.0, 6.0, 0.0, 3.0;

    std::map<int, Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> map;
    if(p->owns_subdomain(0)) map.emplace(0, std::move(v0));
    if(p->owns_subdomain(1)) map.emplace(1, std::move(v1));
    if(p->owns_subdomain(2)) map.emplace(2, std::move(v2));

    vector = PartVector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1>>(p, std::move(map));
  }
}; // struct dist_vector
#endif // MAPHYSPP_USE_EIGEN
// Eigen vector:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Armadillo vector][Armadillo vector:1]]
#ifdef MAPHYSPP_USE_ARMA
template<class Scalar>
struct dist_vector<Scalar, arma::Col<Scalar>>{

  PartVector<arma::Col<Scalar>> vector;
  dist_vector(std::shared_ptr<Process> p){

    std::map<int, arma::Col<Scalar>> map;
    if(p->owns_subdomain(0)) { map.emplace(0, arma::Col<Scalar>{0.0, 1.0, 2.0, 3.0}); }
    if(p->owns_subdomain(1)) { map.emplace(1, arma::Col<Scalar>{3.0, 2.0, 4.0, 5.0}); }
    if(p->owns_subdomain(2)) { map.emplace(2, arma::Col<Scalar>{5.0, 6.0, 0.0, 3.0}); }

    vector = PartVector<arma::Col<Scalar>>(p, std::move(map));
  }
}; // struct dist_vector
#endif // MAPHYSPP_USE_ARMA
// Armadillo vector:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Armadillo vector][Armadillo vector:2]]
#endif // MAPHYSPP_NO_MPI
// Armadillo vector:2 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Simple range vector size 4][Simple range vector size 4:1]]
template<class Scalar, class Vect>
struct simple_vector : public std::false_type {
  Vect vector;
  simple_vector(){ MAPHYSPP_ASSERT(false, "simple_vector created with wrong vector type"); }
}; // dist_general_matrix

// (1, 2, 3, 4)^T for real
// ({1,1}, {2,2}, {3,-3}, {2,-1})^T for complex
template<class Scalar>
struct simple_vector<Scalar, Vector<Scalar>> : public std::true_type {
  Vector<Scalar> vector;
  simple_vector(){
    vector = Vector<Scalar>({1, 2, 3, 4});
    if constexpr(is_complex<Scalar>::value){
      Vector<Scalar> v_img({1, 2, -3, -1});
      vector += (v_img * Scalar{0, 1});
    }
  }
}; // simple_vector

#ifdef MAPHYSPP_USE_EIGEN
template<class Scalar>
struct simple_vector<Scalar, Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> : public std::true_type {
  Eigen::Matrix<Scalar, Eigen::Dynamic, 1> vector;
  simple_vector(){
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> v(4);
    v << 1.0, 2.0, 3.0, 4.0;
    vector = v;
    if constexpr(is_complex<Scalar>::value){
      Eigen::Matrix<Scalar, Eigen::Dynamic, 1> v_img(4);
      v_img << 1, 2, -3, -1;
      vector += (v_img * Scalar{0, 1});
    }
  }
}; // simple_vector
#endif // MAPHYSPP_USE_EIGEN

#ifdef MAPHYSPP_USE_ARMA
template<class Scalar>
struct simple_vector<Scalar, arma::Col<Scalar>> : public std::true_type {
  arma::Col<Scalar> vector;
  simple_vector(){
    vector = arma::Col<Scalar>({1, 2, 3, 4});
    if constexpr(is_complex<Scalar>::value){
      arma::Col<Scalar> v_img({1, 2, -3, -1});
      vector += (v_img * Scalar{0, 1});
    }
  }
}; // simple_vector
#endif // MAPHYSPP_USE_ARMA
// Simple range vector size 4:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Random matrices][Random matrices:1]]
template<class Scalar, class Matrix>
struct random_matrix : public std::false_type {
  Matrix matrix;
  random_matrix(){ MAPHYSPP_ASSERT(false, "random_matrix created with wrong matrix type"); }
}; // random_matrix

template<class Scalar>
struct random_matrix<Scalar, DenseMatrix<Scalar>> : public std::true_type {
  DenseMatrix<Scalar> matrix;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m, int n, bool upper = false){
    matrix = DenseMatrix<Scalar>(m, n);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(min, max);
    std::uniform_real_distribution<> dis_i(Real{0}, static_cast<Real>(m));
    std::uniform_real_distribution<> dis_j(Real{0}, static_cast<Real>(n));

    for(int k = 0; k < nnz_max; ++k){
      int i = static_cast<int>(std::ceil(dis_i(gen) - 1));
      int j = static_cast<int>(std::ceil(dis_j(gen) - 1));
      Scalar val;
      if constexpr(is_complex<Scalar>::value){
        Real re = dis(gen);
        Real im = dis(gen);
        val = Scalar{re, im};
      }
      else{
        val = dis(gen);
      }
      if(upper){
        matrix(std::max(i, j), std::min(i, j)) += val;
      }
      else{
        matrix(i, j) += val;
      }
    }
    if(upper) matrix.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
  }
}; // random_matrix

template<class Scalar>
struct random_matrix<Scalar, SparseMatrixCOO<Scalar>> : public std::true_type {
  SparseMatrixCOO<Scalar> matrix;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m, int n, bool upper = false){
    DenseMatrix<Scalar> dmat = random_matrix<Scalar, DenseMatrix<Scalar>>(nnz_max, min, max, m, n, upper).matrix;
    dmat.convert(matrix);
  }
}; // random_matrix

template<class Scalar>
struct random_matrix<Scalar, SparseMatrixCSC<Scalar>> : public std::true_type {
  SparseMatrixCSC<Scalar> matrix;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m, int n, bool upper = false){
    DenseMatrix<Scalar> dmat = random_matrix<Scalar, DenseMatrix<Scalar>>(nnz_max, min, max, m, n, upper).matrix;
    dmat.convert(matrix);
  }
}; // random_matrix

template<class Scalar>
struct random_matrix<Scalar, Vector<Scalar>> : public std::true_type {
  Vector<Scalar> vector;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m){
    DenseMatrix<Scalar> dmat = random_matrix<Scalar, DenseMatrix<Scalar>>(nnz_max, min, max, m, 1).matrix;
    vector = Vector<Scalar>(dmat);
  }
}; // random_matrix
// Random matrices:1 ends here

// [[file:../../../org/maphys/testing/TestMatrix.org::*Footer][Footer:1]]
} // namespace test_matrix
} // namespace maphys
// Footer:1 ends here
