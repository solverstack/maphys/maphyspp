// [[file:../../../org/maphys/testing/Catch2DenseMatrixMatchers.org::*Test matrices][Test matrices:1]]
#pragma once

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_templated.hpp>
#include "maphys/utils/Arithmetic.hpp"

namespace maphys {

  namespace debug_utils{

    template<typename Vect>
    bool check_vector_dims(const Vect& v1, const Vect& v2, std::string& error_msg){
      std::size_t size_v1 = static_cast<std::size_t>(size(v1));
      std::size_t size_v2 = static_cast<std::size_t>(size(v2));
      if(size_v1 != size_v2){
	error_msg = "\nDifferent sizes: < " + std::to_string(size_v1) + " > != < " + std::to_string(size_v2) + " >\n";
	return false;
      }
      return true;
    }  

    template<typename Matrix>
    bool check_matrix_dims(const Matrix& m1, const Matrix& m2, std::string& error_msg){
      std::size_t nrows_m1 = static_cast<std::size_t>(n_rows(m1));
      std::size_t ncols_m1 = static_cast<std::size_t>(n_cols(m1));
      std::size_t nrows_m2 = static_cast<std::size_t>(n_rows(m2));
      std::size_t ncols_m2 = static_cast<std::size_t>(n_cols(m2));

      if((nrows_m1 != nrows_m2) or (ncols_m1 != ncols_m2)){
	std::ostringstream ss;
	ss << "\nDifferent matrix dimensions: ( " << nrows_m1 << ',' << ncols_m1 << ')'
	   << " != (" << nrows_m2 << ',' << ncols_m2 << ")\n";
	error_msg = ss.str();
	return false;
      }
      return true;
    }

    template<typename T>
    double relative_diff(const T t1, const T t2){ return double{std::abs(t1 - t2) / std::abs(t1)}; }

    template<typename Vect>
    std::string display_vector_difference(const Vect& v1, const Vect& v2, const double tol){
      std::ostringstream ss;
      ss << '\n';
      for(std::size_t i = 0; i < n_rows(v1); ++i){
	if(relative_diff(v1[i], v2[i]) > tol){
	  ss << v1[i] << "\t<X>\t" << v2[i] << '\n';
	}
	else{
	  ss << v1[i] << "\t   \t" << v2[i] << '\n';
	}
      }
      return ss.str();
    }

    template<typename Matrix>
    std::string display_matrix_difference(const Matrix& m1, const Matrix& m2, const double tol){
      std::ostringstream ss;
      ss << '\n';

      for(std::size_t i = 0; i < n_rows(m1); ++i){
	for(std::size_t j = 0; j < n_cols(m1); ++j){
	  if(debug_utils::relative_diff(m1(i, j), m2(i, j)) > tol){
	    ss << "(i=" << i << ",j=" << j << "): " << m1(i, j) << " != " << m2(i, j) << '\n';
	  }
	}
      }

      return ss.str();
    }
  } // namespace debug_utils

  template<typename Vect, bool check_normwise>
  struct EqualsVector : Catch::Matchers::MatcherGenericBase {
    EqualsVector(const Vect& reference_, double tolerance_ = double{-1}):
      reference{ reference_ }
    {
      if(tolerance_ > 0) tolerance = tolerance_;
    }

    bool match(const Vect& other) const {
      std::string * msg_error = const_cast<std::string *>(&error_str);
      if(!debug_utils::check_vector_dims(reference, other, *msg_error)) return false;

      if constexpr(check_normwise){
	const double re = Vect{reference - other}.norm() / reference.norm();
	if(re > tolerance){
	  (*msg_error) = "\n Relative error " + std::to_string(re) + " > tolerance " + std::to_string(tolerance) + '\n';
	  return false;
	}
      }
      else{ // check pointwise
	std::size_t size_other = static_cast<std::size_t>(size(other));
	for(std::size_t i = 0; i < size_other; ++i){
	  if(debug_utils::relative_diff(reference[i], other[i]) > tolerance){
	    (*msg_error) = debug_utils::display_vector_difference(reference, other, tolerance);
	    return false;
	  }
	}
      }

      return true;
    }

    std::string describe() const override {
      return error_str;
    }

  private:
    const Vect& reference;
    double tolerance = maphys::arithmetic_tolerance<typename Vect::scalar_type>::value;
    std::string error_str;
  };

  template<typename Matrix, bool check_normwise>
  struct EqualsMatrix : Catch::Matchers::MatcherGenericBase {
    EqualsMatrix(const Matrix& reference_, double tolerance_ = double{-1}):
      reference{ reference_ }
    {
      if(tolerance_ > 0) tolerance = tolerance_;
    }

    virtual bool match(const Matrix& other) const {
      std::string * msg_error = const_cast<std::string *>(&error_str);
      if(!debug_utils::check_vector_dims(reference, other, *msg_error)) return false;

      if constexpr(check_normwise){
	const double re = Matrix{reference - other}.norm() / reference.norm();
	if(re > tolerance){
	  (*msg_error) = "\n Relative error " + std::to_string(re) + " > tolerance " + std::to_string(tolerance) + '\n';
	  return false;
	}
      }
      else{ // check pointwise
	std::size_t nrows_other = static_cast<std::size_t>(n_rows(other));
	std::size_t ncols_other = static_cast<std::size_t>(n_cols(other));
	for(std::size_t i = 0; i < nrows_other; ++i){
	  for(std::size_t j = 0; j < ncols_other; ++j){
	    if(debug_utils::relative_diff(reference(i, j), other(i, j)) > tolerance){
	      (*msg_error) = debug_utils::display_matrix_difference(reference, other, tolerance);
	      return false;
	    }
	  }
	}
      }

      return true;
    }

    std::string describe() const override {
      return error_str;
    }

  private:
    const Matrix& reference;
    double tolerance = maphys::arithmetic_tolerance<typename Matrix::scalar_type>::value;
    std::string error_str;
  };

  template<typename T>
  using EqualsVectorPW = EqualsVector<T, false>;
  template<typename T>
  using EqualsVectorNW = EqualsVector<T, true>;

  template<typename T>
  using EqualsMatrixPW = EqualsMatrix<T, false>;
  template<typename T>
  using EqualsMatrixNW = EqualsMatrix<T, true>;    
} //namespace maphys
// Test matrices:1 ends here
