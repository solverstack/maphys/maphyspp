// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Header][Header:1]]
#pragma once

#include <forward_list>

namespace maphys {
template<class, class = int> class SparseMatrixLIL;
}

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/IndexArray.hpp"
#include "maphys/loc_data/SparseMatrixCOO.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Attributes][Attributes:1]]
template<class Scalar, class Index>
class SparseMatrixLIL  : public MatrixProperties<Scalar>
{

public:
  using local_type = SparseMatrixLIL<Scalar, Index>;
  using scalar_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;

private:
  using Real = real_type;

  Size _m = 0;
  Size _n = 0;
  Size _nnz = 0;

  using Pair = std::pair<Index, Scalar>;
  using List = std::forward_list<Pair>;
  // Array of N lists of pairs (row index, value)
  std::vector<List> _data;
// Attributes:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Constructors][Constructors:1]]
// Check that n and are positive
  void check_dims_positive(){
    MAPHYSPP_ASSERT_POSITIVE( _m, "M, in initialization SparseMatrixLIL" );
    MAPHYSPP_ASSERT_POSITIVE( _n, "N, in initialization SparseMatrixLIL" );
  }
public:

  explicit SparseMatrixLIL(const Size m, const Size n):
    _m{m}, _n{n}
  {
    check_dims_positive();
    _data = std::vector<List>(n);
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Insertion of an element][Insertion of an element:1]]
private:
  static void sum_duplicates(Scalar& v1, const Scalar& v2) {
    v1 += v2;
  }

public:
  void insert(Index i, Index j, const Scalar v, void (*treat_duplicates)(Scalar&, const Scalar&) = sum_duplicates){
    List& l = _data[j];

    // Parse list to add element in a position
    // To keep a sorted list
    // Deal with duplicates with the function treat_duplicates

    auto it = l.begin();
    // Empty list case
    if(it == l.end()){
      l.emplace_front(Pair(i, v));
      _nnz++;
      return;
    }
    // Iterator to next element
    auto nextit = std::next(it);

    if(this->is_storage_lower() && (i < j)){
      std::swap(i, j);
    }
    else if(this->is_storage_upper() && (i > j)){
      std::swap(i, j);
    }

    // First element
    Pair& head = *it;
    if(head.first == i){
      treat_duplicates(head.second, v);
      return;
    }

    // One element case
    if(nextit == l.end()){
      if(head.first > i){
        l.emplace_front(Pair(i, v));
      }
      else{
        l.emplace_after(it, Pair(i, v));
      }
      _nnz++;
      return;
    }

    // 2 or more element case
    while(nextit != l.end()){
      Pair& next = *nextit;
      if(next.first == i){
        treat_duplicates(next.second, v);
        return;
      }
      else if(next.first > i){
        break;
      }
      it++;
      nextit++;
    }
    l.emplace_after(it, Pair(i, v));
    _nnz++;
  }
// Insertion of an element:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Grouped insertion of a COO sparse matrix][Grouped insertion of a COO sparse matrix:1]]
void insert(const SparseMatrixCOO<Scalar>& coomat, void (*treat_duplicates)(Scalar&, const Scalar&) = sum_duplicates){
  const Size coo_nnz = coomat.get_nnz();
  const Index * i_ptr = coomat.get_i_ptr();
  const Index * j_ptr = coomat.get_j_ptr();
  const Scalar * v_ptr = coomat.get_v_ptr();

  for(Size k = 0; k < coo_nnz; ++k){
    this->insert(i_ptr[k], j_ptr[k], v_ptr[k], treat_duplicates);
  }
}
// Grouped insertion of a COO sparse matrix:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Grouped insertion of a CSC sparse matrix][Grouped insertion of a CSC sparse matrix:1]]
void insert(const SparseMatrixCSC<Scalar>& cscmat, void (*treat_duplicates)(Scalar&, const Scalar&) = sum_duplicates){
  const Index * i_ptr = cscmat.get_i_ptr();
  const Index * j_ptr = cscmat.get_j_ptr();
  const Scalar * v_ptr = cscmat.get_v_ptr();

  for(Size j = 0; j < n_cols(cscmat); ++j){
    for(Index k = j_ptr[j]; k < j_ptr[j+1]; ++k){
      this->insert(i_ptr[k], j, v_ptr[k], treat_duplicates);
    }
  }
}
// Grouped insertion of a CSC sparse matrix:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Grouped insertion of a diagonal matrix][Grouped insertion of a diagonal matrix:1]]
void insert(const DiagonalMatrix<Scalar>& diagmat, void (*treat_duplicates)(Scalar&, const Scalar&) = sum_duplicates){
  const Scalar * diag = diagmat.get_ptr();
  const Size dim = std::min(n_rows(diagmat), n_cols(diagmat));
  for(Size k = 0; k < dim; ++k){
    this->insert(k, k, diag[k], treat_duplicates);
  }
}
// Grouped insertion of a diagonal matrix:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Drop zero elements][Drop zero elements:1]]
void drop(Real drop){

  auto rm_fct = [this, drop](const Pair& p){
    if(std::abs(p.second) < drop){
      _nnz--;
      return true;
    }
    return false;
  };

  for(Index j = 0; j < static_cast<Index>(_n); ++j){
    _data[j].remove_if(rm_fct);
  }
}

void drop(){

  auto rm_fct = [this](const Pair& p){
    if(p.second == Scalar{0}){
      _nnz--;
      return true;
    }
    return false;
  };

  for(Index j = 0; j < static_cast<Index>(_n); ++j){
    _data[j].remove_if(rm_fct);
  }
}
// Drop zero elements:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Conversion functions][Conversion functions:1]]
template<typename OutIndex = Index>
SparseMatrixCOO<Scalar, OutIndex> to_coo() const {
  IndexArray<OutIndex> i_arr(_nnz);
  IndexArray<OutIndex> j_arr(_nnz);
  IndexArray<Scalar> v_arr(_nnz);

  Index k = 0;
  for(Index j = 0; j < static_cast<Index>(_n); ++j){
    for(const Pair& p : _data[j]){
      i_arr[k] = static_cast<OutIndex>(p.first);
      j_arr[k] = static_cast<OutIndex>(j);
      v_arr[k] = p.second;
      k++;
    }
  }

  SparseMatrixCOO<Scalar, OutIndex> coo(_m, _n, _nnz, std::move(i_arr), std::move(j_arr), std::move(v_arr));
  coo.copy_properties(*this);
  return coo;
}

template<typename OutIndex = Index>
SparseMatrixCSC<Scalar, OutIndex> to_csc() const {
  IndexArray<OutIndex> i_a(_nnz);
  IndexArray<OutIndex> j_a(_n + 1);
  IndexArray<Scalar> v_a(_nnz);

  j_a[0] = 0;
  OutIndex k = 0;
  for(OutIndex j = 0; j < static_cast<OutIndex>(_n); ++j){
    for(const Pair& iv : _data[j]){
      i_a[k] = static_cast<OutIndex>(iv.first);
      v_a[k] = iv.second;
      k++;
    }
    j_a[j+1] = k;
  }

  SparseMatrixCSC<Scalar, OutIndex> csc(_m, _n, _nnz, std::move(i_a), std::move(j_a), std::move(v_a));
  csc.copy_properties(*this);
  return csc;
}

DenseMatrix<Scalar> to_dense() const {
  DenseMatrix<Scalar> dm(_m, _n);
  for(Index j = 0; j < static_cast<Index>(_n); ++j){
    const List& l = _data[j];
    for(const Pair& iv : l){
      dm(iv.first, j) = iv.second;
    }
  }
  dm.copy_properties(*this);
  return dm;
}

template<typename OutIndex = Index>
void convert(SparseMatrixCOO<Scalar>& out_mat){ out_mat = this->to_coo<OutIndex>(); }
template<typename OutIndex = Index>
void convert(SparseMatrixCSC<Scalar>& out_mat){ out_mat = this->to_csc<OutIndex>(); }
void convert(DenseMatrix<Scalar>& out_mat){ out_mat = this->to_dense(); }
// Conversion functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Comparison operators][Comparison operators:1]]
bool operator==(const SparseMatrixLIL<Scalar>& other) const {
  if( _m != other._m) return false;
  if( _n != other._n) return false;
  if( _nnz != other._nnz) return false;

  for(Index j = 0; j < static_cast<Index>(_n); ++j){
    const List& l1 = _data[j];
    const List& l2 = other._data[j];
    if(l1 != l2) return false;
  }
  return true;
}

bool operator!=(const SparseMatrixLIL<Scalar>& other) const {
  return !(*this == other);
}
// Comparison operators:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Getters][Getters:1]]
// Getters
[[nodiscard]] inline Size get_n_rows() const { return _m; }
[[nodiscard]] inline Size get_n_cols() const { return _n; }
[[nodiscard]] inline Size get_nnz() const { return _nnz; }
// Getters:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Element accessor][Element accessor:1]]
// Element accessor (access is linear in m)
template<MPH_Integral Tint>
Scalar& operator()(const Tint i, const Tint j){
  Index i_t = static_cast<Index>(i);
  Index j_t = static_cast<Index>(j);
  List& l = _data[j_t];
  for(Pair& iv : l){
    if(iv.first == i_t) return iv.second;
  }
  MAPHYSPP_ASSERT(false, "const SparseMatrixLIL(i,j) does not point to a non zero value");
  return l.front().second;
}

template<MPH_Integral Tint>
const Scalar& operator()(const Tint i, const Tint j) const {
  Index i_t = static_cast<Index>(i);
  Index j_t = static_cast<Index>(j);
  const List& l = _data[j_t];
  for(const Pair& iv : l){
    if(iv.first == i_t) return iv.second;
  }
  MAPHYSPP_ASSERT(false, "const SparseMatrixLIL(i,j) does not point to a non zero value");
  return l.front().second;
}

[[nodiscard]] Scalar coeff(Index i, Index j) const {
  Index i_t = static_cast<Index>(i);
  Index j_t = static_cast<Index>(j);
  const List& l = _data[j_t];
  for(const Pair& iv : l){
    if(iv.first == i_t) return iv.second;
  }
  return Scalar{0};;
}

[[nodiscard]] const Scalar& coeffRef(Index i, Index j) const {
  return (*this)(i, j);
}

[[nodiscard]] Scalar& coeffRef(Index i, Index j){
  return (*this)(i, j);
}
// Element accessor:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Operators][Operators:1]]
// Addition and substraction
SparseMatrixLIL& operator*= (const Scalar scal){
  for(Index j = 0; j < static_cast<Index>(_n); ++j){
    for(Pair& p : _data[j]){
      p.second *= scal;
    }
  }
  return *this;
}

SparseMatrixLIL& operator+=(const SparseMatrixLIL& mat){
  for(Index j = 0; j < static_cast<Index>(_n); ++j){
    const List& l = mat._data[j];
    for(const auto& [i, v] : l){
      this->insert(i, j, v, sum_duplicates);
    }
  }
  this->drop();
  return *this;
}

SparseMatrixLIL& operator-=(const SparseMatrixLIL& mat){
  for(Index j = 0; j < static_cast<Index>(_n); ++j){
    const List& l = mat._data[j];
    for(const auto& [i, v] : l){
      this->insert(i, j, Scalar{-1} * v, sum_duplicates);
    }
  }
  this->drop();
  return *this;
}
// Operators:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Display function][Display function:1]]
// Pretty printing
  void display(const std::string& name="", std::ostream &out = std::cout) const {
    if(!name.empty()) out << name << '\n';
    out << "m: " << _m << " "
        << "n: " << _n << " "
        << "nnz: " << _nnz << '\n'
        << this->properties_str()
        << '\n' << '\n';

    for(Index j = 0; j < static_cast<Index>(_n); ++j){
      out << "Column " << j << '\n' << "i\tv" << '\n';
      const List& l = _data[j];
      for(const Pair& iv : l){
        out << iv.first << "\t" << iv.second << '\n';
      }
    }

    out << '\n';
  }
}; // class SparseMatrixLIL
// Display function:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Traits][Traits:1]]
template<typename Scalar> struct vector_type<SparseMatrixLIL<Scalar>> : public std::true_type {
  using type = Vector<Scalar>;
};

template<typename Scalar> struct dense_type<SparseMatrixLIL<Scalar>> : public std::true_type {
  using type = DenseMatrix<Scalar>;
};

template<typename Scalar> struct scalar_type<SparseMatrixLIL<Scalar>> : public std::true_type {
  using type = Scalar;
};

template<typename Scalar> struct is_sparse<SparseMatrixLIL<Scalar>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Out of class operators][Out of class operators:1]]
template<class Scalar, class Index = int> [[nodiscard]]
SparseMatrixLIL<Scalar, Index> operator+(SparseMatrixLIL<Scalar, Index> A, const SparseMatrixLIL<Scalar, Index>& B){
  A += B;
  return A;
}

template<class Scalar, class Index = int> [[nodiscard]]
SparseMatrixLIL<Scalar, Index> operator-(SparseMatrixLIL<Scalar, Index> A, const SparseMatrixLIL<Scalar, Index>& B){
  A -= B;
  return A;
}
// Out of class operators:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Interface functions][Interface functions:1]]
template<MPH_Scalar Scalar, MPH_Integral Index>
inline Size n_rows(const SparseMatrixLIL<Scalar, Index>& mat) { return mat.get_n_rows(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline Size n_cols(const SparseMatrixLIL<Scalar, Index>& mat) { return mat.get_n_cols(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline Size n_nonzero(const SparseMatrixLIL<Scalar, Index>& mat){ return mat.get_nnz(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
void display(const SparseMatrixLIL<Scalar, Index>& v, const std::string& name="", std::ostream &out = std::cout){ v.display(name, out); }
// Interface functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Footer][Footer:1]]
}  // namespace maphys
// Footer:1 ends here
