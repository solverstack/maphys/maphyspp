// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <algorithm>

namespace maphys {
  template<MPH_Scalar Scalar, int NbCol = -1> class DenseMatrix;

  template<MPH_Scalar Scalar>
  using Vector = DenseMatrix<Scalar, 1>;

  template<MPH_Scalar Scalar, int NbCol>
  void display(const DenseMatrix<Scalar, NbCol>&, const std::string& n="", std::ostream& o=std::cout);

  template<MPH_Scalar Scalar, int NbCol>
  Size size(const DenseMatrix<Scalar, NbCol>& mat);

  template<MPH_Scalar Scalar>
  constexpr Size n_cols(const DenseMatrix<Scalar, 1>&);

  template<MPH_Scalar Scalar, int NbCol>
  Size n_rows(const DenseMatrix<Scalar, NbCol>& mat);

  template<MPH_Scalar Scalar, int NbCol>
  Size n_cols(const DenseMatrix<Scalar, NbCol>& mat);

  template<MPH_Scalar Scalar, int NbCol>
  Scalar * get_ptr(DenseMatrix<Scalar, NbCol>& mat);

  template<MPH_Scalar Scalar, int NbCol>
  const Scalar * get_ptr(const DenseMatrix<Scalar, NbCol>& mat);

  template<MPH_Scalar Scalar, int NbCol> struct DenseMatrixIterator;
}

#include "maphys/loc_data/DiagonalMatrix.hpp"
#include "maphys/loc_data/SparseMatrixCSC.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/IndexArray.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/loc_data/ETMatVec.hpp"

#if defined(MAPHYSPP_USE_LAPACKPP)

#include "maphys/loc_data/DenseData.hpp"
#include "maphys/kernel/BlasKernels.hpp"
#include "maphys/solver/BlasSolver.hpp"

#endif // MAPHYSPP_USE_LAPACKPP undefined

#if defined(MAPHYSPP_USE_TLAPACK)

#include "maphys/loc_data/DenseData.hpp"
#include "maphys/kernel/TlapackKernels.hpp"
#include "maphys/solver/TlapackSolver.hpp"

#endif // MAPHYSPP_USE_LAPACKPP

#if defined(MAPHYSPP_USE_CHAMELEON)

#include "maphys/loc_data/DenseData.hpp"
#include "maphys/kernel/ChameleonKernels.hpp"
#include "maphys/solver/ChameleonSolver.hpp"

#endif // MAPHYSPP_USE_CHAMELEON

#if defined(MAPHYSPP_USE_CHAMELEON)
using COMPOSE_BLAS = maphys::chameleon_kernels;
template<typename Matrix, typename Vector>
using COMPOSE_SOLVER_BLAS = maphys::ChameleonSolver<Matrix, Vector>;

#else
#if defined(MAPHYSPP_USE_LAPACKPP)
using COMPOSE_BLAS = maphys::blas_kernels;
template<typename Matrix, typename Vector>
using COMPOSE_SOLVER_BLAS = maphys::BlasSolver<Matrix, Vector>;

#else
using COMPOSE_BLAS = maphys::tlapack_kernels;
template<typename Matrix, typename Vector>
using COMPOSE_SOLVER_BLAS = maphys::TlapackSolver<Matrix, Vector>;
#endif // MAPHYSPP_USE_LAPACKPP
#endif // MAPHYSPP_USE_CHAMELEON

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Attributes][Attributes:1]]
template<MPH_Scalar Scalar, int NbCol>
class DenseMatrix : public MatrixProperties<Scalar>
{

public:
  using scalar_type = Scalar;
  using local_type = Scalar;
  using value_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;
  static const int static_nb_col = NbCol;

private:
  using Real = real_type;
  using Data = DenseData<Scalar, NbCol>;

  Data _data;
  bool _fixed_ptr = false;
// Attributes:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Classic constructor][Classic constructor:1]]
public:
// Constructors
explicit DenseMatrix(): DenseMatrix(0,0) {}
explicit DenseMatrix(const Size m, const Size n = 1): _data{Data(m, n)} {}
explicit DenseMatrix(const Size m, const Size n, Scalar * ptr, bool fixed_ptr = false){
  if(fixed_ptr){
    _data = Data(m, n, ptr, m);
  }
  else{
    _data = Data(m, n);
    std::memcpy(_data.get_ptr(), ptr, m*n*sizeof(Scalar));
  }
  _fixed_ptr = fixed_ptr;
}

explicit DenseMatrix(std::initializer_list<Scalar> l, const Size m, const Size n = 1, const bool row_major = false){
  if(row_major){
    _data = Data(l, n, m);
    this->transpose();
  }
  else{
    _data = Data(l, m, n);
  }
}

explicit DenseMatrix(std::initializer_list<Scalar> l){
  static_assert(NbCol > 0, "DenseMatrix:: bracket initialization: static NbCol or dimension (m, n) must be specified");
  _data = Data(l);
}

// Copy constructor
DenseMatrix(const DenseMatrix& mat): MatrixProperties<Scalar>(mat){
  _fixed_ptr = false;
  _data.make_deepcopy_of(mat._data);
}

// Move constructor
DenseMatrix(DenseMatrix&& mat): _data(std::move(mat._data)){
  if(&mat == this) return;
  _fixed_ptr = std::exchange(mat._fixed_ptr, false);
  this->copy_properties(mat);
  mat.set_default_properties();
}

DenseMatrix(const Data& data, bool fixed_ptr = false){
  if(fixed_ptr){
    _data = data.shallowcopy();
  }
  else{
    _data = data.deepcopy();
  }
  _fixed_ptr = fixed_ptr;
}

DenseMatrix(Data&& data, bool fixed_ptr = false){
  _data = std::move(data);
  _fixed_ptr = fixed_ptr;
}

// Copying matrices with other NbCol
template<int OtherNbCol>
DenseMatrix(const DenseMatrix<Scalar, OtherNbCol>& mat){
  if(mat.is_ptr_fixed()){
    _fixed_ptr = true;
    _data.make_shallowcopy_of(mat.get_array());
  } else {
    _data.make_deepcopy_of(mat.get_array());
  }
  this->copy_properties(mat);
}

template<int OtherNbCol>
DenseMatrix(DenseMatrix<Scalar, OtherNbCol>&& mat){
  this->copy_properties(mat);
  _fixed_ptr = mat.is_ptr_fixed();
  _data = mat.move_array();
}
// Classic constructor:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Assignment operators][Assignment operators:1]]
private:
  template<int OtherNbCol>
  DenseMatrix& _assignment_lvalue(const DenseMatrix<Scalar, OtherNbCol>& mat){
    if constexpr(OtherNbCol == NbCol) { if(&mat == this) return *this; }
    if(_fixed_ptr){
      MAPHYSPP_DIM_ASSERT(get_n_rows(), mat.get_n_rows(), "DenseMatrix with fixed ptr: assignement with different nb rows");
      MAPHYSPP_DIM_ASSERT(get_n_cols(), mat.get_n_cols(), "DenseMatrix with fixed ptr: assignement with different nb cols");
      _data.copy_values(mat.get_array());
    }
    else{
        _data.make_deepcopy_of(mat.get_array());
    }
    this->copy_properties(mat);
    return *this;
  }

  template<int OtherNbCol>
  DenseMatrix& _assignment_rvalue(DenseMatrix<Scalar, OtherNbCol>&& mat){
    if constexpr(OtherNbCol == NbCol) { if(&mat == this) return *this; }
    if(_fixed_ptr){
      MAPHYSPP_DIM_ASSERT(get_n_rows(), mat.get_n_rows(), "DenseMatrix with fixed ptr: assignement with different nb rows");
      MAPHYSPP_DIM_ASSERT(get_n_cols(), mat.get_n_cols(), "DenseMatrix with fixed ptr: assignement with different nb cols");
      _data.copy_values(mat.get_array());
    }
    else{
      if(mat.is_ptr_fixed()){
	      _data.make_shallowcopy_of(mat.get_array());
	      _fixed_ptr = true;
      }
      else{
	      _data = mat.move_array();
      }
    }
    this->copy_properties(mat);
    mat.clear();
    return *this;
  }

public:
  template<int OtherNbCol>
  DenseMatrix& operator=(const DenseMatrix<Scalar, OtherNbCol>& mat){
    return _assignment_lvalue<OtherNbCol>(mat);
  }

  template<int OtherNbCol>
  DenseMatrix& operator=(DenseMatrix<Scalar, OtherNbCol>&& mat){
    return _assignment_rvalue<OtherNbCol>(std::move(mat));
  }

  DenseMatrix& operator=(const DenseMatrix& mat){
    return _assignment_lvalue<NbCol>(mat);
  }

  DenseMatrix& operator=(DenseMatrix&& mat){
    return _assignment_rvalue<NbCol>(std::move(mat));
  }
// Assignment operators:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Comparison operators][Comparison operators:1]]
template<int OtherNbCol>
bool operator==(const DenseMatrix<Scalar, OtherNbCol>& mat2) const {
  return _data == mat2.get_array();
}

template<int OtherNbCol>
bool operator!=(const DenseMatrix<Scalar, OtherNbCol>& mat2) const {
  return _data != mat2.get_array();
}
// Comparison operators:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Casting][Casting:1]]
// Cast operations
template<class OtherScalar>
[[nodiscard]] DenseMatrix<OtherScalar, NbCol> cast() const {
  DenseMatrix<OtherScalar, NbCol> casted(get_n_rows(), get_n_cols());
  copy_properties<Scalar, OtherScalar>(*this, casted);
  for(Size j = 0; j < get_n_cols(); ++j){
    for(Size i = 0; i < get_n_rows(); ++i){
      casted(i, j) = static_cast<OtherScalar>(_data(i,j));
    }
  }
  return casted;
}
// Casting:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Conversion functions][Conversion functions:1]]
DenseMatrix to_dense() const { return *this; };
void convert(DenseMatrix& out_mat) const { out_mat = *this; }
void convert(SparseMatrixCSC<Scalar>& out_mat) const { out_mat = SparseMatrixCSC<Scalar>(*this); }
void convert(SparseMatrixCOO<Scalar>& out_mat) const { out_mat = SparseMatrixCOO<Scalar>(*this); }
// Conversion functions:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Indexing][Indexing:1]]
// Element accessor
template<MPH_Integral Tint, MPH_Integral Sint>
[[nodiscard]] inline Scalar& operator()(const Tint i, const Sint j){ return _data(i, j); }
template<MPH_Integral Tint, MPH_Integral Sint>
[[nodiscard]] inline const Scalar& operator()(const Tint i, const Sint j) const { return _data(i, j); }
template<MPH_Integral Tint, MPH_Integral Sint>
[[nodiscard]] inline Scalar& at(const Tint i, const Sint j){ return _data.at(i, j); }
template<MPH_Integral Tint, MPH_Integral Sint>
[[nodiscard]] inline const Scalar& at(const Tint i, const Sint j) const { return _data.at(i, j); }

template<MPH_Integral Tint>
[[nodiscard]] inline const Scalar& operator()(const Tint i) const {
  static_assert(NbCol == 1, "DenseMatrix(i): only available with static NbCol = 1.");
  return _data[i];
}

template<MPH_Integral Tint>
[[nodiscard]] inline Scalar& operator()(const Tint i) {
  static_assert(NbCol == 1, "DenseMatrix(i): only available with static NbCol = 1.");
  return _data[i];
}

template<MPH_Integral Tint>
[[nodiscard]] inline const Scalar& operator[](const Tint i) const {
  static_assert(NbCol == 1, "DenseMatrix[i]: only available with static NbCol = 1.");
  return _data[i];
}

template<MPH_Integral Tint>
[[nodiscard]] inline Scalar& operator[](const Tint i) {
  static_assert(NbCol == 1, "DenseMatrix[i]: only available with static NbCol = 1.");
  return _data[i];
}

template<MPH_Integral Tint>
[[nodiscard]] inline Scalar& at(const Tint i){ return _data.at(i); }
template<MPH_Integral Tint>
[[nodiscard]] inline const Scalar& at(const Tint i) const { return _data.at(i); }
// Indexing:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Diagonal extraction][Diagonal extraction:1]]
[[nodiscard]] DiagonalMatrix<Scalar> diag() const {
  const Size k = std::min(get_n_rows(), get_n_cols());
  DiagonalMatrix<Scalar> diag(get_n_rows(), get_n_cols());
  for(Size i = 0; i < k; ++i){
    diag(i) = (*this)(i, i);
  }
  return diag;
}

[[nodiscard]] Vector<Scalar> diag_vect() const {
  const Size k = std::min(get_n_rows(), get_n_cols());
  Vector<Scalar> diag(k);
  for(Size i = 0; i < k; ++i){
    diag(i) = (*this)(i, i);
  }
  return diag;
}
// Diagonal extraction:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Re-indexing][Re-indexing:1]]
template<MPH_Integral Index>
void reindex(const IndexArray<Index> new_indices, int new_m = -1){
  SparseMatrixCOO<Scalar, Index> s;
  this->convert(s);
  s.reindex(new_indices, new_m);
  *this = s.to_dense();
}

template<MPH_Integral Index>
void reindex(const IndexArray<Index> new_indices, const IndexArray<Index> old_indices, int new_m = -1){
  SparseMatrixCOO<Scalar, Index> s;
  this->convert(s);
  s.reindex(new_indices, old_indices, new_m);
  *this = s.to_dense();
}
// Re-indexing:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Transposition][Transposition:1]]
void transpose(){
  // Square case: easy
  Size M = get_n_rows();
  Size N = get_n_cols();

  if(M == N){
    for(Size i = 0; i < M; ++i){
      for(Size j = i+1; j < N; ++j){
        std::swap((*this)(i, j), (*this)(j, i));
      }
    }
    return;
  }

  MAPHYSPP_ASSERT(!_data.is_view(), "DenseMatrix: rectangular transpose not allowed on views");
  MAPHYSPP_ASSERT(_fixed_ptr == false, "DenseMatrix: rectangular transpose unavaibled with fixed ptr");
  //Otherwise use a copy
  DenseMatrix new_mat(N, M);
  for(Size i = 0; i < M; ++i){
    for(Size j = 0; j < N; ++j){
      new_mat(j, i) = (*this)(i, j);
    }
  }
  (*this) = std::move(new_mat);
}

void conj_transpose(){
  transpose();
  if constexpr(is_complex<Scalar>::value){
    for(Size j = 0; j < get_n_cols(); ++j){
      for(Size i = 0; i < get_n_rows(); ++i){
        _data(i, j) = conj(_data(i, j));
      }
    }
  }
}

// t for conjugate transposed matrix?
[[nodiscard]] DenseMatrix<Scalar, -1> t() const {
  if(this->is_symmetric()) return *this;
  DenseMatrix<Scalar, -1> copy = (*this);
  copy.conj_transpose();
  return copy;
}
// Transposition:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Change storage][Change storage:1]]
void to_storage_full(){
  if(this->is_storage_full()) return;

  if(this->is_storage_lower()){
    for(Size j = 0; j < this->get_n_cols(); ++j){
      for(Size i = j + 1; i < this->get_n_rows(); ++i){
	if(this->is_hermitian()){
	  (*this)(j, i) = conj((*this)(i, j));
	}
	else if(this->is_symmetric()){
	  (*this)(j, i) = (*this)(i, j);
	}
      }
    }
  }

  else if(this->is_storage_upper()){
    if(this->is_sym_or_herm()){
      for(Size i = 0; i < this->get_n_rows(); ++i){
	for(Size j = i + 1; j < this->get_n_cols(); ++j){
	  if(this->is_hermitian()){
	    (*this)(j, i) = conj((*this)(i, j));
	  }
	  else if(this->is_symmetric()){
	    (*this)(j, i) = (*this)(i, j);
	  }
	}
      }
    }
  }

  this->set_property(MatrixStorage::full);
}

void break_symmetry(){
  this->to_storage_full();
  this->set_property(MatrixSymmetry::general);
}
// Change storage:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:1]]
// Matrix vector multiplication
// Y.gemv(A, X, alpha=1, beta=0)
// performs Y = alpha * op(A) * X + beta * Y
template<int OtherNbCol>
void gemv(const DenseMatrix<Scalar, OtherNbCol>& A, const DenseMatrix<Scalar, NbCol>& X,
	  const Scalar alpha=Scalar{1.0}, const Scalar beta=Scalar{0.0}, char opA = 'N'){
  if(&X == this){
    auto Xcpy = X;
    COMPOSE_BLAS::gemv(A, Xcpy, *this, opA, alpha, beta);
  }
  else{
    COMPOSE_BLAS::gemv(A, X, *this, opA, alpha, beta);
  }
}
// Matrix vector product:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Matrix matrix product][Matrix matrix product:1]]
// Matrix multiplication
// C.gemm(A, B, alpha=1, beta=0)
// performs C = alpha * A * B + beta * C
void gemm(const DenseMatrix& A, const DenseMatrix& B,
	  const Scalar alpha=Scalar{1.0}, const Scalar beta=Scalar{0.0}){
  this->break_symmetry();
  COMPOSE_BLAS::gemm(A, B, *this, 'N', 'N', alpha, beta);
}

// Matrix multiplication
// C.gemm(A, B, alpha=1, beta=0)
// performs C = alpha * T(A) * B + beta * C
void Tgemm(const DenseMatrix& A, const DenseMatrix& B,
	   const Scalar alpha=Scalar{1.0}, const Scalar beta=Scalar{0.0}){
  this->break_symmetry();
  COMPOSE_BLAS::gemm(A, B, *this, 'C', 'N', alpha, beta);
}
// Matrix matrix product:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Diagonal matrix matrix product][Diagonal matrix matrix product:1]]
// A.left_diagmm(D) performs
// A <- D * A
//    (M, K) * (K, N)
// Careful: break symmetry
void left_diagmm(const DiagonalMatrix<Scalar>& D){
  const Blas_int M = static_cast<Blas_int>(D.get_n_rows());
  const Blas_int N = static_cast<Blas_int>(this->get_n_cols());
  const Blas_int K = static_cast<Blas_int>(D.get_n_cols());

  MAPHYSPP_DIM_ASSERT(this->get_n_rows(), K, "(DiagonalMatrix) x (DenseMatrix) multiplication with wrong dimensions");

  this->break_symmetry();
  // Should scale each row of the dense matrix by the diagonal coefficient
  const Scalar * diag_coeff = D.get_ptr();
  for(int i = 0; i < std::min(M, K); ++i){
    auto rowi = this->get_row_view(i);
    COMPOSE_BLAS::scal(rowi, diag_coeff[i]);
  }

  // If dimensions are changing
  if(D.get_n_cols() != D.get_n_rows()){
    Data newdata(M, N);
    for(Blas_int j = 0; j < N; ++j){
      for(Blas_int i = 0; i < std::min(M, K); ++i){
	newdata(i, j) = _data(i, j);
      }
    }
    _data = std::move(newdata);
  }
}

// A.right_diagmm(D) performs
// A <- A * D
//    (M, K) * (K, N)
// Careful: break symmetry
void right_diagmm(const DiagonalMatrix<Scalar>& D){
  const Blas_int M = static_cast<Blas_int>(this->get_n_rows());
  const Blas_int N = static_cast<Blas_int>(D.get_n_cols());
  const Blas_int K = static_cast<Blas_int>(this->get_n_cols());

  MAPHYSPP_DIM_ASSERT(D.get_n_rows(), K, "(DenseMatrix) x (DiagonalMatrix) multiplication with wrong dimensions");

  this->break_symmetry();
  // Should scale each colums of the dense matrix by the diagonal coefficient
  const Scalar * diag_coeff = D.get_ptr();
  for(int j = 0; j < std::min(K, N); ++j){
    auto colj = this->get_vect_view(j);
    COMPOSE_BLAS::scal(colj, diag_coeff[j]);
  }

  // If dimensions are changing
  if(D.get_n_cols() != D.get_n_rows()){
    Data newdata(M, N);
    for(Blas_int j = 0; j < std::min(K, N); ++j){
      for(Blas_int i = 0; i < M; ++i){
	newdata(i, j) = _data(i, j);
      }
    }
    _data = std::move(newdata);
  }
}
// Diagonal matrix matrix product:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Diagonal matrix vector product][Diagonal matrix vector product:1]]
void diagmv(const DiagonalMatrix<Scalar>& D){
  MAPHYSPP_ASSERT( D.get_n_cols() == this->get_n_rows(), "Diagonal matrix vector multiplication with wrong dimensions");
  const Scalar * D_data = D.get_ptr();
  for(Size k = 0; k < D.get_n_cols(); ++k){
    this->_data[k] *= D_data[k];
  }
}
// Diagonal matrix vector product:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Matrix addition][Matrix addition:1]]
// Matrix addition
void add(const DenseMatrix& mat, Scalar alpha = Scalar{1.0}){
  _data.add(mat._data, alpha);
}

template<MPH_Integral Index>
void add(const SparseMatrixCSC<Scalar, Index>& mat, Scalar alpha = Scalar{1.0}){
  const Index * iptr = mat.get_i_ptr();
  const Index * jptr = mat.get_j_ptr();
  const Scalar * vptr = mat.get_v_ptr();

  for(Index j = 0; j < static_cast<Index>(get_n_cols()); ++j){
    for(Index k = jptr[j]; k < jptr[j+1]; ++k){
      (*this)(iptr[k], j) += alpha * vptr[k];
    }
  }
}

template<MPH_Integral Index>
void add(const SparseMatrixCOO<Scalar, Index>& mat, Scalar alpha = Scalar{1.0}){
  const Index * iptr = mat.get_i_ptr();
  const Index * jptr = mat.get_j_ptr();
  const Scalar * vptr = mat.get_v_ptr();
  const Size nnz = mat.get_nnz();

  for(Size k = 0; k < nnz; ++k){
    (*this)(iptr[k], jptr[k]) += alpha * vptr[k];
  }
}
// Matrix addition:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Diagonal addition][Diagonal addition:1]]
// Matrix addition
void add(const DiagonalMatrix<Scalar>& mat, Scalar alpha = Scalar{1.0}){
  const Size M = mat.get_n_rows();
  const Size N = mat.get_n_cols();
  MAPHYSPP_ASSERT( get_n_rows() == M , "Matrix + diagonal matrix addition with different M");
  MAPHYSPP_ASSERT( get_n_cols() == N , "Matrix + diagonal matrix addition with different N");
  const Scalar * diag = mat.get_ptr();
  for(Size k = 0; k < std::min(M, N); ++k){
    (*this)(k, k) += alpha * diag[k];
  }
}
// Diagonal addition:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Getters][Getters:1]]
public:
// Getters
[[nodiscard]] inline Size get_n_rows() const { return _data.get_n_rows(); }
[[nodiscard]] inline Size get_n_cols() const { return _data.get_n_cols(); }
[[nodiscard]] inline Size get_nb_row() const { return _data.get_n_rows(); }
[[nodiscard]] inline Size get_nb_col() const { return _data.get_n_cols(); }
[[nodiscard]] inline Size get_leading_dim() const { return _data.get_leading_dim(); }
[[nodiscard]] inline Size get_increment() const { return _data.get_increment(); }

[[nodiscard]] inline Scalar* get_ptr() { return _data.get_ptr(); }
[[nodiscard]] inline const Scalar* get_ptr() const { return _data.get_ptr(); }
template<MPH_Integral Tint>
[[nodiscard]] inline Scalar* get_ptr(Tint i, Tint j) { return &_data(i, j); }
template<MPH_Integral Tint>
[[nodiscard]] inline const Scalar* get_ptr(Tint i, Tint j) const { return &_data(i,j); }

template<MPH_Integral Tint>
[[nodiscard]] inline Scalar * get_vect_ptr(Tint j = 0) { return _data.get_vect_ptr(j); }
template<MPH_Integral Tint>
[[nodiscard]] inline const Scalar* get_vect_ptr(Tint j = 0) const { return _data.get_vect_ptr(j); }

template<MPH_Integral Tint>
[[nodiscard]] inline DenseMatrix<Scalar, 1> get_vect(Tint j = 0) { return DenseMatrix<Scalar, 1>(get_n_rows(), 1, get_vect_ptr(j)); }

template<MPH_Integral Tint>
[[nodiscard]] inline DenseMatrix<Scalar, 1> get_vect_view(Tint j = 0) {
  MAPHYSPP_ASSERT(j <= static_cast<Tint>(get_n_cols()), "DenseMatrix::get_vect_view: j > N");
  return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(get_n_rows(), 1, get_vect_ptr(j)), true);
}
template<MPH_Integral Tint>
[[nodiscard]] friend auto get_vect_view(DenseMatrix<Scalar, NbCol>& dm, Tint j = 0) { return dm.get_vect_view(j); }

template<MPH_Integral Tint>
[[nodiscard]] inline const DenseMatrix<Scalar, 1> get_vect_view(Tint j = 0) const {
  MAPHYSPP_ASSERT(j <= static_cast<Tint>(get_n_cols()), "DenseMatrix::get_vect_view: j > N");
  return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(get_n_rows(), 1, const_cast<Scalar*>(get_vect_ptr(j))), true);
}
template<MPH_Integral Tint>
[[nodiscard]] friend auto get_vect_view(const DenseMatrix<Scalar, NbCol>& dm, Tint j = 0) { return dm.get_vect_view(j); }

template<MPH_Integral Tint>
[[nodiscard]] inline DenseMatrix<Scalar, -1> get_vect_view(Tint j, Size nb_col) {
  MAPHYSPP_ASSERT(static_cast<Size>(j) + nb_col <= get_n_cols(), "DenseMatrix::get_vect_view: j + nb_col > N");
  return DenseMatrix<Scalar, -1>(DenseData<Scalar, -1>(this->get_n_rows(), nb_col, this->get_ptr(0, j)), true);
}
template<MPH_Integral Tint>
[[nodiscard]] friend auto get_vect_view(DenseMatrix<Scalar, NbCol>& dm, Tint j, Size nb_col) { return dm.get_vect_view(j, nb_col); }

template<MPH_Integral Tint>
[[nodiscard]] inline DenseMatrix<Scalar, -1> get_vect(Tint j, Size nb_col) {
  MAPHYSPP_ASSERT(static_cast<Size>(j) + nb_col <= get_n_cols(), "DenseMatrix::get_vect_view: j + nb_col > N");
  return DenseMatrix<Scalar, -1>(DenseData<Scalar, -1>(this->get_n_rows(), nb_col, this->get_ptr(0, j)));
}

template<MPH_Integral Tint>
[[nodiscard]] inline DenseMatrix<Scalar, 1> get_vect_view(Tint i, Tint j, Size m) {
  MAPHYSPP_ASSERT(j <= static_cast<Tint>(get_n_cols()), "DenseMatrix::get_vect_view: j > N");
  MAPHYSPP_ASSERT(i + m <= get_n_rows(), "DenseMatrix::get_vect_view: i + M > nb rows");
  return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(m, this->get_ptr(i, j)), true);
}
template<MPH_Integral Tint>
[[nodiscard]] friend auto get_vect_view(DenseMatrix<Scalar, NbCol>& dm, Tint i, Tint j, Size m) { return dm.get_vect_view(i, j, m); }

template<MPH_Integral Tint>
[[nodiscard]] inline const DenseMatrix<Scalar, 1> get_vect_view(Tint i, Tint j, Size m) const {
  MAPHYSPP_ASSERT(j <= static_cast<Tint>(get_n_cols()), "DenseMatrix::get_vect_view: j > N");
  MAPHYSPP_ASSERT(i + m <= get_n_rows(), "DenseMatrix::get_vect_view: i + M > nb rows");
  return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(m, const_cast<Scalar*>(this->get_ptr(i, j))), true);
}
template<MPH_Integral Tint>
[[nodiscard]] friend auto get_vect_view(const DenseMatrix<Scalar, NbCol>& dm, Tint i, Tint j, Size m) { return dm.get_vect_view(i, j, m); }

template<MPH_Integral Tint>
[[nodiscard]] inline const DenseMatrix<Scalar, 1> get_col_view(Tint i, Tint j, Size m) const {
  return get_vect_view(i, j, m);
}

template<MPH_Integral Tint>
[[nodiscard]] inline DenseMatrix<Scalar, 1> get_col_view(Tint i, Tint j, Size m) {
  return get_vect_view(i, j, m);
}

// Row and diag view: non-contiguous access, we need an increment
template<MPH_Integral Tint>
[[nodiscard]] inline const DenseMatrix<Scalar, 1> get_row_view(Tint i, Tint j, Size n) const {
  MAPHYSPP_ASSERT(i <= static_cast<Tint>(get_n_rows()), "DenseMatrix::get_vect_view: i > M");
  MAPHYSPP_ASSERT(j + n <= get_n_cols(), "DenseMatrix::get_vect_view: i + N > nb cols");
  int inc = static_cast<int>(this->get_leading_dim());
  return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(n, const_cast<Scalar*>(this->get_ptr(i, j)), 0, inc), true);
}

template<MPH_Integral Tint>
[[nodiscard]] inline const DenseMatrix<Scalar, 1> get_row_view(Tint i) const {
  return get_row_view(i, 0, get_n_cols());
}

template<MPH_Integral Tint>
[[nodiscard]] inline DenseMatrix<Scalar, 1> get_row_view(Tint i, Tint j, Size n) {
  MAPHYSPP_ASSERT(i <= static_cast<Tint>(get_n_rows()), "DenseMatrix::get_vect_view: i > M");
  MAPHYSPP_ASSERT(j + n <= get_n_cols(), "DenseMatrix::get_vect_view: i + N > nb cols");
  int inc = static_cast<int>(this->get_leading_dim());
  return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(n, this->get_ptr(i, j), 0, inc), true);
}

template<MPH_Integral Tint>
[[nodiscard]] inline const DenseMatrix<Scalar, 1> get_row_view(Tint i) {
  return get_row_view(i, 0, get_n_cols());
}

template<MPH_Integral Tint>
[[nodiscard]] inline const DenseMatrix<Scalar, 1> get_diag_view(Tint i) const {
  MAPHYSPP_ASSERT(i >= 0 || (-i < static_cast<Tint>(get_n_rows())), "DenseMatrix::get_diag_view -index > nb rows");
  MAPHYSPP_ASSERT(i <= 0 || (i < static_cast<Tint>(get_n_cols())), "DenseMatrix::get_diag_view index > nb cols");
  const Scalar * ptr = (i >= 0) ? this->get_ptr(0, i) : this->get_ptr(-i, 0);
  Size n_diag_elts = (i >= 0) ?
    std::min(get_n_rows() + i, get_n_cols()) - i :
    std::min(get_n_rows(), get_n_cols() - i) + i;
  int inc = static_cast<int>(this->get_leading_dim() + 1);
  return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(n_diag_elts, const_cast<Scalar*>(ptr), 0, inc), true);
}

template<MPH_Integral Tint>
[[nodiscard]] inline DenseMatrix<Scalar, 1> get_diag_view(Tint i){
  MAPHYSPP_ASSERT(i >= 0 || (-i < static_cast<Tint>(get_n_rows())), "DenseMatrix::get_diag_view -index > nb rows");
  MAPHYSPP_ASSERT(i <= 0 || (i < static_cast<Tint>(get_n_cols())), "DenseMatrix::get_diag_view index > nb cols");
  Scalar * ptr = (i >= 0) ? this->get_ptr(0, i) : this->get_ptr(-i, 0);
  Size n_diag_elts = (i >= 0) ?
    std::min(get_n_rows() + i, get_n_cols()) - i :
    std::min(get_n_rows(), get_n_cols() - i) + i;
  int inc = static_cast<int>(this->get_leading_dim() + 1);
  return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(n_diag_elts, ptr, 0, inc), true);
}

[[nodiscard]] inline const DenseMatrix<Scalar, 1> get_diag_view() const { return get_diag_view(0); }
[[nodiscard]] inline DenseMatrix<Scalar, 1> get_diag_view() { return get_diag_view(0); }

template<MPH_Integral Tint>
[[nodiscard]] DenseMatrix<Scalar> get_block_view(Tint i, Tint j, Size m, Size n){
   MAPHYSPP_ASSERT(i >= 0, "DenseMatrix::get_block_view: i < 0");
   MAPHYSPP_ASSERT(j >= 0, "DenseMatrix::get_block_view: j < 0");
   MAPHYSPP_ASSERT(i + m <= get_n_rows(), "DenseMatrix::get_block_view: i + M > nb rows");
   MAPHYSPP_ASSERT(j + n <= get_n_cols(), "DenseMatrix::get_block_view: j + N > nb rows");
   return DenseMatrix<Scalar>(DenseData<Scalar>(m, n, this->get_ptr(i, j), this->get_leading_dim()), true);
}

template<MPH_Integral Tint>
[[nodiscard]] const DenseMatrix<Scalar> get_block_view(Tint i, Tint j, Size m, Size n) const{
   MAPHYSPP_ASSERT(i >= 0, "DenseMatrix::get_block_view: i < 0");
   MAPHYSPP_ASSERT(j >= 0, "DenseMatrix::get_block_view: j < 0");
   MAPHYSPP_ASSERT(i + m <= get_n_rows(), "DenseMatrix::get_block_view: i + M > nb rows");
   MAPHYSPP_ASSERT(j + n <= get_n_cols(), "DenseMatrix::get_block_view: j + N > nb rows");
   return DenseMatrix<Scalar>(DenseData<Scalar>(m, n, const_cast<Scalar *>(this->get_ptr(i, j)), this->get_leading_dim()), true);
}

template<MPH_Integral Tint>
[[nodiscard]] const DenseMatrix<Scalar> get_rows_view(Tint i_beg, Tint i_end) const{
  return get_block_view(i_beg, 0, i_end - i_beg, get_n_cols());
}

template<MPH_Integral Tint>
[[nodiscard]] const DenseMatrix<Scalar> get_columns_view(Tint j_beg, Tint j_end) const{
  return get_block_view(0, j_beg, get_n_rows(), j_end - j_beg);
}
template<MPH_Integral Tint>
[[nodiscard]] friend auto get_columns_view(const DenseMatrix<Scalar, NbCol>& dm, Tint j_beg, Tint j_end){
  return dm.get_columns_view(j_beg, j_end);
}

template<MPH_Integral Tint>
[[nodiscard]] DenseMatrix<Scalar> get_block_copy(Tint i, Tint j, Size m, Size n) const {
   const DenseMatrix<Scalar> blk = this->get_block_view(i, j, m, n);
   return DenseMatrix<Scalar>(blk);
}

[[nodiscard]] inline const Data& get_array() const { return _data; }
[[nodiscard]] Data&& move_array() { return std::move(_data); }
[[nodiscard]] bool is_ptr_fixed() const { return _fixed_ptr; }
void copy_data(const Data &data) { _data.make_deepcopy_of(data); }

DenseMatrix copy() const { return DenseMatrix(*this); }

const DenseMatrix view() const {
  return DenseMatrix<Scalar>(DenseData<Scalar>(get_n_rows(), get_n_cols(), const_cast<Scalar *>(this->get_ptr()), this->get_leading_dim()), true);
}

DenseMatrix view() {
  return DenseMatrix<Scalar>(DenseData<Scalar>(get_n_rows(), get_n_cols(), const_cast<Scalar *>(this->get_ptr()), this->get_leading_dim()), true);
}
// Getters:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Setters][Setters:1]]
void clear(){
  _data = Data();
  _fixed_ptr = false;
}
// Setters:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Memory cost][Memory cost:1]]
[[nodiscard]] inline Size memcost() const {
  if(_data.is_view()) return sizeof(Scalar *);
  return get_n_rows() * get_n_cols() * sizeof(Scalar);
}
// Memory cost:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Begin and end][Begin and end:1]]
DenseMatrixIterator<Scalar, NbCol> begin() const{
  return DenseMatrixIterator<Scalar, NbCol>(*this);
}

DenseMatrixIterator<Scalar, NbCol> end() const{
  return DenseMatrixIterator<Scalar, NbCol>(*this).end();
}
// Begin and end:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Display function][Display function:1]]
// Pretty printing
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << name << '\n';
  out << "m: " << get_n_rows() << " ";
  if constexpr(NbCol == 1){
    out << "(vector)\n\n";
    for(Size i = 0; i < get_n_rows(); ++i){
      out << (*this)[i] << '\n';
    }
  }
  else{
    out << "n: " << get_n_cols() << " " << '\n'
	<< this->properties_str();
    out << "\n\n";

    for(Size i = 0; i < get_n_rows(); ++i){
      for(Size j = 0; j < get_n_cols(); ++j){
	if(this->is_storage_lower() && j > i){
	  out << "-" << "\t";
	}
	else if(this->is_storage_upper() && i > j){
	  out << "-" << "\t";
	}
	else{
	  out << (*this)(i, j) << "\t";
	}
      }
      out << '\n';
    }
    out << '\n';
  }
  out << '\n';
}
// Display function:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Operators][Operators:1]]
// Scalar multiplication
DenseMatrix& operator*= (const Scalar& scal){
  _data *= scal;
  return *this;
}

DenseMatrix& operator+=(const DenseMatrix& mat){
  add(mat);
  return *this;
}

DenseMatrix& operator+=(const DiagonalMatrix<Scalar>& mat){
  add(mat);
  return *this;
}

template<MPH_Integral Index>
DenseMatrix& operator+=(const SparseMatrixCSC<Scalar, Index>& mat){
  add(mat);
  return *this;
}

template<MPH_Integral Index>
DenseMatrix& operator+=(const SparseMatrixCOO<Scalar, Index>& mat){
  add(mat);
  return *this;
}

DenseMatrix& operator-=(const DenseMatrix& mat){
  add(mat, Scalar{-1.0});
  return *this;
}

DenseMatrix& operator-=(const DiagonalMatrix<Scalar>& mat){
  add(mat, Scalar{-1.0});
  return *this;
}

template<MPH_Integral Index>
DenseMatrix& operator-=(const SparseMatrixCSC<Scalar, Index>& mat){
  add(mat, Scalar{-1.0});
  return *this;
}

template<MPH_Integral Index>
DenseMatrix& operator-=(const SparseMatrixCOO<Scalar, Index>& mat){
  add(mat, Scalar{-1.0});
  return *this;
}

template<int OtherNbCol>
DenseMatrix& operator*=(const DenseMatrix<Scalar, OtherNbCol>& B){
  // We have to allocate a new matrix
  DenseMatrix<Scalar> A = *this;
  this->gemm(A, B, Scalar{1.0}, Scalar{0.0});
  return *this;
}

DenseMatrix& operator*= (const DiagonalMatrix<Scalar>& D){
  // We have to allocate a new matrix
  right_diagmm(D);
  return *this;
}
// Operators:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Frobenius norm][Frobenius norm:1]]
[[nodiscard]] Real frobenius_norm() const{
  Real norm{0.0};
  for(Size j = 0; j < get_n_cols(); ++j){
    for(Size i = 0; i < get_n_rows(); ++i){
      norm += std::norm(_data(i, j));
    }
  }
  return std::sqrt(norm);
}

Real norm() const { return frobenius_norm(); }
// Frobenius norm:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Dot product][Dot product:1]]
template<int OtherNbCol>
Scalar dot(const DenseMatrix<Scalar, OtherNbCol>& v2) const {
  MAPHYSPP_DIM_ASSERT(get_n_rows(), v2.get_n_rows(), "DenseMatrix::dot: Scalar dot product different row numbers");
  if constexpr(NbCol != 1){ MAPHYSPP_DIM_ASSERT( get_n_cols(), 1, "DenseMatrix::dot: dot only works on 1 column"); }
  if constexpr(OtherNbCol != 1) { MAPHYSPP_DIM_ASSERT( v2.get_n_cols(), 1, "DenseMatrix::dot: dot only works on 1 column"); }
  return _data.dot(v2._data);
}

template<int OtherNbCol>
std::vector<Scalar> cwise_dot(const DenseMatrix<Scalar, OtherNbCol>& M2) const {
  MAPHYSPP_DIM_ASSERT(get_n_rows(), M2.get_n_rows(), "DenseMatrix::dot: Matrix column-wise dot different row numbers");
  MAPHYSPP_DIM_ASSERT(get_n_cols(), M2.get_n_cols(), "DenseMatrix::dot: Matrix column-wise dot different col numbers");
  std::vector<Scalar> v_out(get_n_cols());
  for(Size j = 0; j < get_n_cols(); ++j){
    auto xj = M2.get_vect_view(j);
    auto yj = this->get_vect_view(j);
    v_out[j] = yj.dot(xj);
  }
  return v_out;
}

template<int NbCol2 = NbCol>
DenseMatrix<Scalar, NbCol> dot_block(const DenseMatrix<Scalar, NbCol2>& M2) const {
  MAPHYSPP_DIM_ASSERT( get_n_rows(), M2.get_n_rows(), "Matrix dot_block different row numbers");
  DenseMatrix<Scalar, NbCol> res(get_n_cols(), M2.get_n_cols());
  res.Tgemm(*this, M2);
  return res;
}
// Dot product:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Expression templates (matrix)][Expression templates (matrix):1]]
DenseMatrix& operator=(const ScalMat<Scalar>& sm){
  if(this != &(sm.m)) *this = sm.m;
  _data *= sm.alpha;
  return *this;
}
// Expression templates (matrix):1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Expression templates (vectors)][Expression templates (vectors):1]]
DenseMatrix(const MatVect<Scalar>& m){
  if constexpr(NbCol == 1){ this->gemv(m.m, m.v, m.alpha); }
  else{
    if(m.v.get_n_colss() == 1) { this->gemv(m.m, m.v, m.alpha); }
    else{ this->gemm(m.m, m.v, m.alpha); }
  }
}

DenseMatrix& operator=(const MatVect<Scalar>& m){
  if constexpr(NbCol == 1){ this->gemv(m.m, m.v, m.alpha); }
  else{
    if(m.v.get_n_cols() == 1) { this->gemv(m.m, m.v, m.alpha); }
    else{ this->gemm(m.m, m.v, m.alpha); }
  }
  return *this;
}

DenseMatrix(const ETgemv<Scalar>& m):
  DenseMatrix<Scalar, 1>(m.y)
{
  static_assert(NbCol == 1, "DenseMatrix(const ETgemv<Scalar>&): only available NbCol = 1");
  this->gemv(m.A, m.x, m.alpha, m.beta);
}

DenseMatrix& operator=(const ETgemv<Scalar>& m)
{
  static_assert(NbCol == 1, "DenseMatrix& operator=(const ETgemv<Scalar>&): only available NbCol = 1");
  // GEMV: Y <- alpha A X + beta Y
  // If Y == X, we need to make a copy
  // Y <- alpha A (copy(Y)) + beta Y
  if(this == &(m.x)){
    auto X_copy = m.x;
    if(this != &(m.y)) *this = m.y;
    this->gemv(m.A, X_copy, m.alpha, m.beta);
  }
  else{ // Y <- alpha A X + beta Y
    if(this != &(m.y)) *this = m.y;
    this->gemv(m.A, m.x, m.alpha, m.beta);
  }
  return *this;
}

DenseMatrix& operator=(const ScalVect<Scalar>& sv){
  static_assert(NbCol == 1, "DenseMatrix& operator=(const ScalVect<Scalar>&): only available NbCol = 1");
  if(this != &(sv.v)) *this = sv.v;
  _data *= sv.alpha;
  return *this;
}
// Expression templates (vectors):1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Serialization][Serialization:1]]
std::vector<char> serialize() const {
  // M, N, values
  int M = static_cast<int>(_data.get_n_rows());
  int N = static_cast<int>(_data.get_n_cols());
  std::vector<char> buffer(2 * sizeof(int) + M * N * sizeof(Scalar));
  int cnt = 0;

  std::memcpy(&buffer[cnt], &M, sizeof(int)); cnt += sizeof(int);
  std::memcpy(&buffer[cnt], &N, sizeof(int)); cnt += sizeof(int);

  if(static_cast<int>(_data.get_leading_dim()) == M){
    std::memcpy(&buffer[cnt], _data.get_ptr(), M * N * sizeof(Scalar)); cnt += M * N *sizeof(Scalar);
  }
  else{ // Copy vector by vector
    for(int j = 0; j < N; ++j){
      std::memcpy(&buffer[cnt], _data.get_vect_ptr(j), M * sizeof(Scalar)); cnt += M * sizeof(Scalar);
    }
  }

  return buffer;
}

void deserialize(const std::vector<char>& buffer){
  int M, N;
  int cnt = 0;
  std::memcpy(&M, &buffer[cnt], sizeof(int)); cnt += sizeof(int);
  std::memcpy(&N, &buffer[cnt], sizeof(int)); cnt += sizeof(int);
  _data = Data(static_cast<Size>(M), static_cast<Size>(N));
  std::memcpy(_data.get_ptr(), &buffer[cnt], N*M*sizeof(Scalar));
  _fixed_ptr = false;
}
// Serialization:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Factories][Factories:1]]
static DenseMatrix<Scalar, NbCol> full(const Size M, const Size N, const Scalar s){
  DenseMatrix<Scalar, NbCol> A(M, N);
  for(Size j = 0; j < N; ++j){
    for(Size i = 0; i < M; ++i){
      A(i, j) = s;
    }
  }
  return A;
}

static DenseMatrix<Scalar, 1> ones(const Size M){
  return DenseMatrix<Scalar, 1>::full(M, 1, Scalar{1});
}

static DenseMatrix<Scalar, NbCol> ones(const Size M, const Size N){
  return DenseMatrix<Scalar, NbCol>::full(M, N, Scalar{1});
}

static DenseMatrix<Scalar, NbCol> eye(const Size M, const Size N){
  DenseMatrix<Scalar, NbCol> A(M, N);
  for(Size i = 0; i < std::min(M, N); ++i){
    A(i, i) = Scalar{1};
  }
  return A;
}

static DenseMatrix<Scalar, NbCol> identity(const Size M){
  return DenseMatrix<Scalar, NbCol>::eye(M, M);
}
// Factories:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Factories][Factories:2]]
static DenseMatrix<Scalar, NbCol> view(const Size M, const Size N, Scalar * ptr, const Size ld){
  return DenseMatrix<Scalar, NbCol>(DenseData<Scalar, NbCol>(M, N, ptr, ld), true);
}

template<MPH_DenseVector OtherType>
static DenseMatrix<Scalar, 1> view_vector(OtherType& other){
  auto M = size(other);
  Scalar * ptr = maphys::get_ptr(other);
  return DenseMatrix<Scalar, 1>::view(M, 1, ptr, M);
}

template<MPH_DenseMatrix OtherType>
static DenseMatrix<Scalar, NbCol> view_matrix(OtherType& other){
  auto M = n_rows(other);
  auto N = n_cols(other);
  Scalar * ptr = maphys::get_ptr(other);
  auto ld = maphys::get_leading_dim(other);
  return DenseMatrix<Scalar, NbCol>::view(M, N, ptr, ld);
}

template<typename OtherType>
static DenseMatrix<Scalar, NbCol> view(OtherType& o){
  if constexpr(NbCol == 1){ return DenseMatrix<Scalar, NbCol>::view_vector(o); }
  else{ return DenseMatrix<Scalar, NbCol>::view_matrix(o); }
}

template<typename OtherType>
void update_view(OtherType& other){
  this->clear();
  (*this) = DenseMatrix::view(other);
}
// Factories:2 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Out of class operators][Out of class operators:1]]
}; // class DenseMatrix
// Out of class operators:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Out of class operators][Out of class operators:2]]
// Scalar multiplication
template<MPH_Scalar Scalar, int NbCol>
inline DenseMatrix<Scalar> operator* (DenseMatrix<Scalar, NbCol> mat, const Scalar& scal){
  mat *= scal;
  return mat;
}

template<MPH_Scalar Scalar, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator* (Scalar scal, const DenseMatrix<Scalar, NbCol>& mat){
  return mat * scal;
}

template<MPH_Scalar Scalar, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator/ (DenseMatrix<Scalar, NbCol> mat, const Scalar& scal){
  return mat * (Scalar{1.0}/scal);
}

// Matrix addition
template<MPH_Scalar Scalar, int NbCol1, int NbCol2>
inline DenseMatrix<Scalar, NbCol1> operator+ (DenseMatrix<Scalar, NbCol1> A, const DenseMatrix<Scalar, NbCol2>& B){
  A += B;
  return A;
}

template<MPH_Scalar Scalar, MPH_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator+ (DenseMatrix<Scalar, NbCol> A, const SparseMatrixCSC<Scalar, Index>& B){
  A += B;
  return A;
}

template<MPH_Scalar Scalar, MPH_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator+ (DenseMatrix<Scalar, NbCol> A, const SparseMatrixCOO<Scalar, Index>& B){
  A += B;
  return A;
}

template<MPH_Scalar Scalar, MPH_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator+ (const SparseMatrixCSC<Scalar, Index>& A, DenseMatrix<Scalar, NbCol> B){
  B += A;
  return B;
}

template<MPH_Scalar Scalar, MPH_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator+ (const SparseMatrixCOO<Scalar, Index>& A, DenseMatrix<Scalar, NbCol> B){
  B += A;
  return B;
}

// Matrix substraction
template<MPH_Scalar Scalar, int NbCol1, int NbCol2>
inline DenseMatrix<Scalar, NbCol1> operator- (DenseMatrix<Scalar, NbCol1> A, const DenseMatrix<Scalar, NbCol2>& B){
  A -= B;
  return A;
}

template<MPH_Scalar Scalar, MPH_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator- (DenseMatrix<Scalar, NbCol> A, const SparseMatrixCSC<Scalar, Index>& B){
  A -= B;
  return B;
}

template<MPH_Scalar Scalar, MPH_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator- (DenseMatrix<Scalar, NbCol> A, const SparseMatrixCOO<Scalar, Index>& B){
  A -= B;
  return B;
}

template<MPH_Scalar Scalar, MPH_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator- (const SparseMatrixCSC<Scalar, Index>& A, DenseMatrix<Scalar, NbCol> B){
  B *= Scalar{-1};
  B += A;
  return B;
}

template<MPH_Scalar Scalar, MPH_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol> operator- (const SparseMatrixCOO<Scalar, Index>& A, DenseMatrix<Scalar, NbCol> B){
  B *= Scalar{-1};
  B += A;
  return B;
}

// Matrix multiplication
template<MPH_Scalar Scalar, int NbCol1, int NbCol2>
inline DenseMatrix<Scalar, NbCol2> operator* (DenseMatrix<Scalar, NbCol1> A, const DenseMatrix<Scalar, NbCol2>& B){
  A *= B;
  return A;
}
// Out of class operators:2 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*GEMV interface][GEMV interface:1]]
// Y <- alpha A X + beta Y
template<MPH_Scalar Scalar, int NbCol, int NbCol2>
void gemv(const DenseMatrix<Scalar, NbCol>& A, const DenseMatrix<Scalar, NbCol2>& X,
          DenseMatrix<Scalar, NbCol2>& Y, const Scalar alpha=Scalar{1.0}, const Scalar beta=Scalar{0.0}, char opA = 'N'){
  Y.gemv(A, X, alpha, beta, opA);
}
// GEMV interface:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Traits][Traits:1]]
template<MPH_Scalar Scalar, int NbCol> struct vector_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = Vector<Scalar>;
};

template<MPH_Scalar Scalar, int NbCol> struct sparse_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = SparseMatrixCSC<Scalar>;
};

template<MPH_Scalar Scalar, int NbCol> struct dense_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = DenseMatrix<Scalar, NbCol>;
};

template<MPH_Scalar Scalar, int NbCol> struct diag_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = DiagonalMatrix<Scalar>;
};

template<MPH_Scalar Scalar, int NbCol> struct scalar_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = Scalar;
};

template<typename Scalar, int NbCol> struct is_dense<DenseMatrix<Scalar, NbCol>> : public std::true_type {};

template<MPH_Scalar Scalar, int NbCol> struct real_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using Real = typename arithmetic_real<Scalar>::type;
  using type = DenseMatrix<Real, NbCol>;
};

template<MPH_Scalar Scalar, int NbCol> struct complex_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using Complex = typename arithmetic_complex<Scalar>::type;
  using type = DenseMatrix<Complex, NbCol>;
};
// Traits:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Interface functions][Interface functions:1]]
template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline Size size(const DenseMatrix<Scalar, NbCol>& mat) { return mat.get_n_rows() * mat.get_n_cols(); }

template<MPH_Scalar Scalar>
[[nodiscard]] constexpr Size n_cols(const DenseMatrix<Scalar, 1>&) { return 1; }

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline Size n_rows(const DenseMatrix<Scalar, NbCol>& mat) { return mat.get_n_rows(); }

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline Size n_cols(const DenseMatrix<Scalar, NbCol>& mat) { return mat.get_n_cols(); }

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline Size get_leading_dim(const DenseMatrix<Scalar, NbCol>& mat) { return mat.get_leading_dim(); }

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline Size get_increment(const DenseMatrix<Scalar, NbCol>& mat) { return mat.get_increment(); }

template<MPH_Scalar Scalar, int NbCol1, int NbCol2>
inline Scalar dot(const DenseMatrix<Scalar, NbCol1>& v1, const DenseMatrix<Scalar, NbCol2>& v2){
  return v1.dot(v2);
}

template<MPH_Scalar Scalar, int NbCol1, int NbCol2>
inline std::vector<Scalar> cwise_dot(const DenseMatrix<Scalar, NbCol1>& v1, const DenseMatrix<Scalar, NbCol2>& v2){
  return v1.cwise_dot(v2);
}

template<MPH_Scalar Scalar, int NbCol1, int NbCol2>
inline DenseMatrix<Scalar, NbCol1> dot_block(const DenseMatrix<Scalar, NbCol1>& v1, const DenseMatrix<Scalar, NbCol2>& v2){
  MAPHYSPP_DIM_ASSERT( v1.get_n_rows(), v2.get_n_rows(), "Matrix dot_block1 different row numbers");
  return v1.dot_block(v2);
}

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline DiagonalMatrix<Scalar> diagonal(const DenseMatrix<Scalar, NbCol>& mat) { return mat.diag(); }

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline Vector<Scalar> diagonal_as_vector(const DenseMatrix<Scalar, NbCol>& mat) { return mat.diag_vect(); }

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline Scalar * get_ptr(DenseMatrix<Scalar, NbCol>& mat) { return mat.get_ptr(); }

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] inline const Scalar * get_ptr(const DenseMatrix<Scalar, NbCol>& mat) { return mat.get_ptr(); }

template<MPH_Scalar Scalar, int NbCol>
[[nodiscard]] DenseMatrix<Scalar, NbCol> adjoint(const DenseMatrix<Scalar, NbCol>& mat){ return mat.t(); }

template<MPH_Scalar Scalar, int NbCol>
void display(const DenseMatrix<Scalar, NbCol>& v, const std::string& name, std::ostream &out){ v.display(name, out); }

template<MPH_Scalar Scalar, int NbCol>
void build_matrix(DenseMatrix<Scalar, NbCol>& mat, const int M, const int N, const int nnz, int * i, int * j, Scalar * v,  const bool fill_symmetry = false){
  mat = DenseMatrix<Scalar, NbCol>(M, N);
  for(int k = 0; k < nnz; ++k){
    mat(i[k], j[k]) = v[k];
    if(fill_symmetry && (i[k] != j[k])){
      mat(j[k], i[k]) = v[k];
    }
  }
}

template<MPH_Scalar Scalar, int NbCol>
void build_dense_matrix(DenseMatrix<Scalar, NbCol>& mat, const int M, const int N, Scalar * values){
  mat = DenseMatrix<Scalar, NbCol>(M, N);
  std::memcpy(get_ptr(mat), values, M*N*sizeof(Scalar));
}
// Interface functions:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Matrix pseudo-inverse operator][Matrix pseudo-inverse operator:1]]
template<typename Scalar>
auto operator~(const DenseMatrix<Scalar>& A){
  return COMPOSE_SOLVER_BLAS<DenseMatrix<Scalar>, Vector<Scalar>>(A);
}

template<typename Scalar>
Vector<Scalar> operator%(const DenseMatrix<Scalar>& A, const Vector<Scalar>& b){
  return ~A * b;
}
// Matrix pseudo-inverse operator:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Iterator][Iterator:1]]
template<MPH_Scalar Scalar, int NbCol> struct DenseMatrixIterator{
  Size i;
  Size j;
  const DenseMatrix<Scalar, NbCol>& mat;

  using value_type = Scalar;

  DenseMatrixIterator(const DenseMatrix<Scalar, NbCol>& m): i{0}, j{0}, mat{m} {}

  DenseMatrixIterator(const DenseMatrixIterator&) = default;

  DenseMatrixIterator& operator++(){
    ++i;
    if (i == n_rows(mat)){
      i = 0;
      ++j;
    }
    return *this;
  }

  Scalar operator*() const {
    return mat(i, j);
  }

  DenseMatrixIterator begin(){
    return DenseMatrixIterator(mat);
  }

  DenseMatrixIterator end(){
    DenseMatrixIterator d(mat);
    d.i = 0;
    d.j = n_cols(mat);
    return d;
  }

  bool operator!=(const DenseMatrixIterator& other) const {
    return (i != other.i) or (j != other.j);
  }

  bool operator==(const DenseMatrixIterator& other) const {
    return !((*this) != other);
  }
};
// Iterator:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Footer namespace][Footer namespace:1]]
} // namespace maphys
// Footer namespace:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Tlapack interface][Tlapack interface:1]]
#ifdef MAPHYSPP_USE_TLAPACK
namespace tlapack {

  template<typename Scalar, int NbCol>
  maphys::Size nrows(const maphys::DenseMatrix<Scalar, NbCol>& M){ return maphys::n_rows(M); }

  template<typename Scalar, int NbCol>
  maphys::Size ncols(const maphys::DenseMatrix<Scalar, NbCol>& M){ return maphys::n_cols(M); }

  template<typename Scalar, int NbCol>
  maphys::Size size(const maphys::DenseMatrix<Scalar, NbCol>& M){ return maphys::n_rows(M) * maphys::n_cols(M); }

  template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
  slice(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> fidx, std::pair<int, int> lidx){
    auto& [fi, li] = fidx;
    auto& [fj, lj] = lidx;
    return M.get_block_view(fi, fj, li - fi, lj - fj);
  }

  template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
  rows(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> ridx){
    auto& [fi, li] = ridx;
    return M.get_block_view(fi, 0, li - fi, static_cast<int>(maphys::n_cols(M)));
  }

  template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
  cols(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> cidx){
    auto& [fj, lj] = cidx;
    return M.get_block_view(0, fj, static_cast<int>(maphys::n_rows(M)), lj - fj);
  }

  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  row(const maphys::DenseMatrix<Scalar, NbCol>& M, int i){
    return M.get_row_view(i, 0, maphys::n_cols(M));
  }

  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  col(const maphys::DenseMatrix<Scalar, NbCol>& M, int j){
    return M.get_vect_view(j);
  }

  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  slice(const maphys::DenseMatrix<Scalar, NbCol>& M, int i, std::pair<int, int> lidx){
    auto& [fj, lj] = lidx;
    MAPHYSPP_ASSERT(fj <= lj, "maphys::DenseMatrix::slice with first index > last index");
    return M.get_row_view(i, fj, static_cast<maphys::Size>(lj -fj));
  }

  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  slice(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> lidx, int j){
    auto& [fi, li] = lidx;
    MAPHYSPP_ASSERT(fi <= li, "maphys::DenseMatrix::slice with first index > last index");
    return M.get_col_view(fi, j, static_cast<maphys::Size>(li -fi));
  }

  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  diag(const maphys::DenseMatrix<Scalar, NbCol>& M){
    return M.get_diag_view();
  }

  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  diag(const maphys::DenseMatrix<Scalar, NbCol>& M, int i){
    return M.get_diag_view(i);
  }

  template<typename Scalar> maphys::Vector<Scalar>
  slice(const maphys::DenseMatrix<Scalar, 1>& M, std::pair<int, int> idxs){
    auto& [fi, li] = idxs;
    return M.get_block_view(fi, 0, li - fi, 1);
  }
} // namespace tlapack
#endif
// Tlapack interface:1 ends here
