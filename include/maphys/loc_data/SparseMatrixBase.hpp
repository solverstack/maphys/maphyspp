// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <numeric>
#include <string>

namespace maphys {
template<class, MPH_Integral = int> class SparseMatrixBase;
}

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/IO/MatrixMarketLoader.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/IndexArray.hpp"
#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/solver/LinearOperator.hpp"

#ifdef MAPHYSPP_USE_PASTIX
#include "maphys/solver/Pastix.hpp"
#endif
#ifdef MAPHYSPP_USE_MUMPS
#include "maphys/solver/Mumps.hpp"
#endif

namespace maphys {
template<typename SpMat, typename Vect>
struct default_sparse_solver{
#ifdef MAPHYSPP_USE_PASTIX
  using type = Pastix<SpMat, Vect>;
#else
#ifdef MAPHYSPP_USE_MUMPS
  using type = Mumps<SpMat, Vect>;
#else
  using type = Identity<SpMat, Vect>;
#endif
#endif
};
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Attributes][Attributes:1]]
template<class Scalar, MPH_Integral Index>
class SparseMatrixBase  : public MatrixProperties<Scalar>
{

public:
  using scalar_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;
  using index_type = Index;

protected:
  using Real = real_type;
  using Idx_arr = IndexArray<Index>;
  using Scal_arr = IndexArray<Scalar>;

  Size _m = 0;
  Size _n = 0;
  Size _nnz = 0;

  Idx_arr _i;
  Idx_arr _j;
  Scal_arr _v;
// Attributes:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Constructors][Constructors:1]]
public:

  SparseMatrixBase(const Size m, const Size n, const Size nnz): _m{m}, _n{n}, _nnz{nnz} {}
  SparseMatrixBase(const Size m, const Size n, const Size nnz, const Idx_arr& i, const Idx_arr& j, const Scal_arr& v): 
    _m{m}, _n{n}, _nnz{nnz}, _i{i}, _j{j}, _v{v} {}
  SparseMatrixBase(const Size m, const Size n, const Size nnz, Idx_arr&& i, Idx_arr&& j, Scal_arr&& v):
    _m{m}, _n{n}, _nnz{nnz}, _i{std::move(i)}, _j{std::move(j)}, _v{std::move(v)} {}

  // Copy constructor
  SparseMatrixBase(const SparseMatrixBase& spmat) = default;

  // Move constructor
  SparseMatrixBase(SparseMatrixBase&& spmat):
    _m{std::exchange(spmat._m, 0)},
    _n{std::exchange(spmat._n, 0)},
    _nnz{std::exchange(spmat._nnz, 0)},
    _i{std::move(spmat._i)},
    _j{std::move(spmat._j)},
    _v{std::move(spmat._v)}
  {
    this->copy_properties(spmat);
    spmat.set_default_properties();
  }

  // Copy
  SparseMatrixBase& operator= (const SparseMatrixBase& copy){
    _m = copy._m;
    _n = copy._n;
    _nnz = copy._nnz;
    
    _i = copy._i;
    _j = copy._j;
    _v = copy._v;
    this->copy_properties(copy);
    return *this;
  }

  // Move assignment operator
  SparseMatrixBase& operator= (SparseMatrixBase&& copy){
    copy.swap(*this);
    return *this;
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Swap functions][Swap functions:1]]
void swap(SparseMatrixBase& other){
  std::swap(_m, other._m);
  std::swap(_n, other._n);
  std::swap(_nnz, other._nnz);
  std::swap(_i, other._i);
  std::swap(_j, other._j);
  std::swap(_v, other._v);
  this->MatrixProperties<Scalar>::swap(other);
}

friend void swap(SparseMatrixBase& m1, SparseMatrixBase& m2){
  m1.swap(m2);
}
// Swap functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Diagonal extraction][Diagonal extraction:1]]
virtual DiagonalMatrix<Scalar> diag() const = 0;
virtual Vector<Scalar> diag_vect() const = 0;
// Diagonal extraction:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Re-indexing][Re-indexing:1]]
virtual void reindex(const IndexArray<Index> new_indices, int new_m = -1) = 0;
virtual void reindex(const IndexArray<Index> new_indices, const IndexArray<Index> old_indices, int new_m = -1) = 0;
// Re-indexing:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Frobenius norm][Frobenius norm:1]]
[[nodiscard]] Real frobenius_norm() const{
  auto square_v = [](const Real& sum, const Scalar& scal){ return sum + std::abs(scal*scal); };
  return std::sqrt(std::accumulate(_v.begin(), _v.end(), Real{0}, square_v));
}

[[nodiscard]] Real norm() const { return frobenius_norm(); }
// Frobenius norm:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Getters][Getters:1]]
// Getters
[[nodiscard]] inline Size get_n_rows() const { return _m; }
[[nodiscard]] inline Size get_n_cols() const { return _n; }
[[nodiscard]] inline Size get_nnz() const { return _nnz; }

[[nodiscard]] inline const Index * get_i_ptr() const { return &_i[0]; }
[[nodiscard]] inline const Index * get_j_ptr() const { return &_j[0]; }
[[nodiscard]] inline const Scalar * get_v_ptr() const { return &_v[0]; }

[[nodiscard]] inline Index * get_i_ptr() { return &_i[0]; }
[[nodiscard]] inline Index * get_j_ptr() { return &_j[0]; }
[[nodiscard]] inline Scalar * get_v_ptr() { return &_v[0]; }
// Getters:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Operators][Operators:1]]
// Scalar multiplication
SparseMatrixBase& operator*= (const Scalar& scal){
  _v *= scal;
  return *this;
}

SparseMatrixBase& operator/= (const Scalar& scal){
  const auto inv = Scalar{1.0}/scal;
  _v *= inv;
  return *this;
}
// Operators:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Display function][Display function:1]]
virtual void display(const std::string& name="", std::ostream &out = std::cout) const = 0;
}; // class SparseMatrixBase
// Display function:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Interface functions][Interface functions:1]]
template<MPH_Scalar Scalar, MPH_Integral Index>
inline Size n_rows(const SparseMatrixBase<Scalar, Index>& mat) { return mat.get_n_rows(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline Size n_cols(const SparseMatrixBase<Scalar, Index>& mat) { return mat.get_n_cols(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline Size n_nonzero(const SparseMatrixBase<Scalar, Index>& mat){ return mat.get_nnz(); }

template<MPH_Scalar Scalar, MPH_Integral Index> [[nodiscard]]
inline DiagonalMatrix<Scalar> diagonal(const SparseMatrixBase<Scalar, Index>& mat){ return mat.diag(); }

template<MPH_Scalar Scalar, MPH_Integral Index> [[nodiscard]]
inline Vector<Scalar> diagonal_as_vector(const SparseMatrixBase<Scalar, Index>& mat) { return mat.diag_vect(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline const Index * get_i_ptr(const SparseMatrixBase<Index>& mat){ return mat.get_i_ptr(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline const Index * get_j_ptr(const SparseMatrixBase<Scalar, Index>& mat){ return mat.get_j_ptr(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline const Scalar * get_v_ptr(const SparseMatrixBase<Scalar, Index>& mat){ return mat.get_v_ptr(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline Index * get_i_ptr(SparseMatrixBase<Scalar, Index>& mat){ return mat.get_i_ptr(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline Index * get_j_ptr(SparseMatrixBase<Scalar, Index>& mat){ return mat.get_j_ptr(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
inline Scalar * get_v_ptr(SparseMatrixBase<Scalar, Index>& mat){ return mat.get_v_ptr(); }

template<MPH_Scalar Scalar, MPH_Integral Index>
void display(const SparseMatrixBase<Scalar, Index>& v, const std::string& name="", std::ostream &out = std::cout){ v.display(name, out); }
// Interface functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixBase.org::*Footer][Footer:1]]
}  // namespace maphys
// Footer:1 ends here
