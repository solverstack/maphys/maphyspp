// [[file:../../../org/maphys/loc_data/Laplacian.org::*1D Laplacian operator][1D Laplacian operator:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <type_traits>
#include <blas.hh>

namespace maphys {
template<class> struct Laplacian;
}

#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"

namespace maphys {

// 1D Laplacian (finite differences)

template<class Scalar>
struct Laplacian {

  using scalar_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;

  Laplacian(){}

  void display(const std::string& name="", std::ostream &out = std::cout) const {
    if(!name.empty()) out << name << '\n';
    out << "Laplacian operator on arithmetic " << arithmetic_name<Scalar>::name << '\n';
  }
};

template<class Scalar>
Vector<Scalar> operator* (const Laplacian<Scalar>& l, const Vector<Scalar>& vect_in){
  (void) l;
  const Size sz = size(vect_in);
  Vector<Scalar> vect_out(sz);
  vect_out *= Scalar{0};

  Scalar * v_out = get_ptr(vect_out);
  const Scalar * v_in = get_ptr(vect_in);

  // Useful for complex types
  constexpr const Scalar two = Scalar{2.0};
  
  if(sz == 0){
    return vect_out;
  }
  else if(sz == 1){
    v_out[0] = two * v_in[0];
  }
  else{
    v_out[0] = two * v_in[0] - v_in[1];
    for(Size k = 1; k < sz - 1; ++k){
      v_out[k] = two * v_in[k] - v_in[k-1] - v_in[k+1];
    }
    v_out[sz-1] = two * v_in[sz-1] - v_in[sz-2];
  }

  return vect_out;
}

template<typename Scalar> struct vector_type<Laplacian<Scalar>> : public std::true_type {
  using type = Vector<Scalar>;
};

template<typename Scalar> struct scalar_type<Laplacian<Scalar>> : public std::true_type {
  using type = Scalar;
};

template<typename Scalar>
void display(const Laplacian<Scalar>& l, const std::string& name="", std::ostream &out = std::cout){
  l.display(name, out);
}

} // namespace maphys
// 1D Laplacian operator:1 ends here
