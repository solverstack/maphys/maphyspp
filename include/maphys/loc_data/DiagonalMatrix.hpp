// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Header][Header:1]]
#pragma once

namespace maphys {
template<class> class DiagonalMatrix;
}

#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/loc_data/SparseMatrixCSC.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/IndexArray.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/loc_data/ETMatVec.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Attributes][Attributes:1]]
template<class Scalar>
class DiagonalMatrix : public MatrixProperties<Scalar>
{

public:
  using scalar_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;

private:
  using Real = real_type;
  using DataArray = IndexArray<Scalar>;

  Size _m{0};
  Size _n{0};
  DataArray _data;
// Attributes:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Constructors][Constructors:1]]
public:
explicit DiagonalMatrix(const Size m, const Size n): _m{m}, _n{n}{
  _data = DataArray(std::min(_m, _n), Scalar{0});
}

explicit DiagonalMatrix(const Size m): DiagonalMatrix(m, m) {}

explicit DiagonalMatrix(): DiagonalMatrix(0, 0) {}

explicit DiagonalMatrix(const Size m, const Size n, Scalar * vect): _m{m}, _n{n} {
  _data = DataArray(vect, std::min(_m, _n));
}

//explicit DiagonalMatrix(const std::vector<Scalar>& v): DiagonalMatrix(v.size(), v.size()), _data(v) {}

explicit DiagonalMatrix(std::initializer_list<Scalar> l, const Size m = 0, const Size n = 0){
  Size diagsize = l.size();
  if(m == 0){
    _m = diagsize;
    _n = diagsize;
  }
  else if(n == 0){
    _m = m;
    _n = m;
  }
  else{
    _m = m;
    _n = n;
  }
  MAPHYSPP_ASSERT(diagsize == std::min(_m, _n), "DiagonalMatrix list initialization: size must be min(m, n)");
  _data = DataArray(l);
}

explicit DiagonalMatrix(const Vector<Scalar>& vect): _m{vect.get_n_rows()}, _n{vect.get_n_rows()}, _data{DataArray(vect.get_ptr(), vect.get_n_rows())} {}

explicit DiagonalMatrix(Vector<Scalar>&& vect): _m{vect.get_n_rows()}, _n{vect.get_n_rows()}, _data{vect.move_array().move_data_array()} {}

DiagonalMatrix(const DataArray& v, const Size m, const Size n = 0): DiagonalMatrix(m, n) { _data = v; }
DiagonalMatrix(DataArray&& v, const Size m, const Size n = 0): DiagonalMatrix(m, n) { _data = std::move(v); }

DiagonalMatrix(const DiagonalMatrix& mat) = default;
DiagonalMatrix(DiagonalMatrix&& mat):
  _m{std::exchange(mat._m, 0)},
  _n{std::exchange(mat._n, 0)},
  _data{std::move(mat._data)}
{
  this->copy_properties(mat);
  mat.set_default_properties();
}

// Copy and move assignment operator
DiagonalMatrix& operator= (DiagonalMatrix copy){
  copy.swap(*this);
  return *this;
}
// Constructors:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Swap function][Swap function:1]]
void swap(DiagonalMatrix& other){
  std::swap(_m, other._m);
  std::swap(_n, other._n);
  std::swap(_data, other._data);
}

friend void swap(DiagonalMatrix& m1, DiagonalMatrix& m2){
  m1.swap(m2);
}
// Swap function:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Comparison operator][Comparison operator:1]]
bool operator==(const DiagonalMatrix& mat2) const {
  if(_m != mat2._m) return false;
  if(_n != mat2._n) return false;
  if(_data != mat2._data) return false;
  return true;
}

bool operator!=(const DiagonalMatrix& mat2) const {
  return !(*this == mat2);
}
// Comparison operator:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Casting][Casting:1]]
template <class OtherScalar>
[[nodiscard]] DiagonalMatrix<OtherScalar> cast() const {
  DiagonalMatrix<OtherScalar> mat(_m, _n);
  copy_properties<Scalar, OtherScalar>(*this, mat);
  OtherScalar * ptr = mat.get_ptr();
  for(Size i = 0; i < std::min(_m, _n); ++i){
    ptr[i] = static_cast<OtherScalar>(_data[i]);
  }
  return mat;
}
// Casting:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Identity][Identity:1]]
void identity(){
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] = Scalar{1};
  }
}
// Identity:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Transposition][Transposition:1]]
void transpose(){ std::swap(_m, _n); }
void conj_transpose(){
  std::swap(_m, _n);
  if constexpr(is_complex<Scalar>::value){
    for(Size k = 0; k < _m; ++k)  _data[k] = conj(_data[k]);
  }
}

[[nodiscard]] DiagonalMatrix t() const {
  return DiagonalMatrix(_data, _n, _m);
}
// Transposition:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Scalar multiplication][Scalar multiplication:1]]
// Scalar multiplication
DiagonalMatrix& operator*= (const Scalar& scal){
  for(Size i = 0; i < std::min(_m, _n); ++i) (*this)[i] *= scal;
  return *this;
}
// Scalar multiplication:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Diagonal matrix product][Diagonal matrix product:1]]
// Diag *= Diag
void matprod(const DiagonalMatrix& other){
  MAPHYSPP_ASSERT(other.get_n_cols() == this->get_n_rows(), "DiagonalMatrix * DiagonalMatrix: wrong dimensions");
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] *= other._data[k];
  }
}

// Square the matrix
void square(){
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] *= _data[k];
  }
}

DiagonalMatrix& operator*=(const DiagonalMatrix& other){
  if(this == &other){
    this->square();
  }
  else{
    this->matprod(other);
  }
  return *this;
}
// Diagonal matrix product:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Diagonal matrix addition][Diagonal matrix addition:1]]
// Diag += Diag
void add(const DiagonalMatrix& other){
  MAPHYSPP_ASSERT(other.get_n_rows() == this->get_n_rows(), "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
  MAPHYSPP_ASSERT(other.get_n_cols() == this->get_n_cols(), "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] += other._data[k];
  }
}

void substract(const DiagonalMatrix& other){
  MAPHYSPP_ASSERT(other.get_n_rows() == this->get_n_rows(), "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
  MAPHYSPP_ASSERT(other.get_n_cols() == this->get_n_cols(), "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] += other._data[k];
  }
}

DiagonalMatrix& operator+=(const DiagonalMatrix& other){
  if(this == &other){
    (*this) *= Scalar{2};
  }
  else{
    this->add(other);
  }
  return *this;
}

DiagonalMatrix& operator-=(const DiagonalMatrix& other){
  if(this == &other){
    _data = DataArray(std::min(_m, _n), Scalar{0});
  }
  else{
    this->substract(other);
  }
  return *this;
}
// Diagonal matrix addition:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Getters][Getters:1]]
public:
  // Getters
  [[nodiscard]] inline Size get_n_rows() const { return _m; }
  [[nodiscard]] inline Size get_n_cols() const { return _n; }
  [[nodiscard]] inline Size constexpr get_leading_dim() const { return 1; }

  [[nodiscard]] inline Scalar* get_ptr() { return &_data[0]; }
  [[nodiscard]] inline const Scalar* get_ptr() const { return &_data[0]; }
  template<MPH_Integral Tint>
  [[nodiscard]] inline Scalar* get_ptr(Tint i, Tint j) {
    MAPHYSPP_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return &_data[i];
  }
  template<MPH_Integral Tint>
  [[nodiscard]] inline const Scalar* get_ptr(Tint i, Tint j) const {
    MAPHYSPP_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return &_data[i];
  }
  template<MPH_Integral Tint>
  [[nodiscard]] inline Scalar* get_ptr(Tint i) { return &_data[i]; }
  template<MPH_Integral Tint>
  [[nodiscard]] inline const Scalar* get_ptr(Tint i) const { return &_data[i]; }
  [[nodiscard]] inline DataArray get_array() const { return _data; }
// Getters:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Indexing][Indexing:1]]
// Element accessor
template<MPH_Integral Tint>
[[nodiscard]] inline Scalar& operator()(const Tint i, const Tint j){
  MAPHYSPP_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
  return _data[i];
}
template<MPH_Integral Tint>
[[nodiscard]] inline const Scalar& operator()(const Tint i, const Tint j) const {
  MAPHYSPP_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
  return _data[i];
}
template<MPH_Integral Tint>
[[nodiscard]] inline Scalar& operator()(const Tint i){ return _data[i]; }
template<MPH_Integral Tint>
[[nodiscard]] inline const Scalar& operator()(const Tint i) const { return _data[i]; }
// Indexing:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Memory cost][Memory cost:1]]
[[nodiscard]] inline Size memcost() const { return std::min(_m, _n) * sizeof(Scalar); }
// Memory cost:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Display function][Display function:1]]
// Pretty printing
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << name << '\n';
  out << "m: " << _m << " "
      << "n: " << _n << " " << '\n'
      << "Diagonal matrix\n"
      << this->properties_str()
      << '\n' << '\n';

  for(Size i = 0; i < _m; ++i){
    for(Size j = 0; j < _n; ++j){
      if(i == j){
        out << _data[i] << "\t";
      }
      else{
        out << "-" << "\t";
      }
    }
    out << '\n';
  }
  out << '\n';
}
// Display function:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Frobenius norm][Frobenius norm:1]]
[[nodiscard]] Real frobenius_norm() const{
  Real norm{0.0};
  for(Size k = 0; k < std::min(_m, _n); ++k){
    norm += std::norm(_data[k]);
  }
  return std::sqrt(norm);
}

Real norm() const { return frobenius_norm(); }
// Frobenius norm:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Out of class operators][Out of class operators:1]]
}; // class DiagonalMatrix
// Out of class operators:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Out of class operators][Out of class operators:2]]
// Scalar multiplication
template<class Scalar>
inline DiagonalMatrix<Scalar> operator* (DiagonalMatrix<Scalar> diag, const Scalar scal){
  diag *= scal;
  return diag;
}

template<class Scalar>
inline DiagonalMatrix<Scalar> operator* (const Scalar scal, DiagonalMatrix<Scalar> diag){
  diag *= scal;
  return diag;
}

template<class Scalar>
inline DiagonalMatrix<Scalar> operator/ (DiagonalMatrix<Scalar> diag, const Scalar scal){
  diag *= (Scalar{1}/scal);
  return diag;
}

// Diagonal addition / substraction
template<class Scalar>
inline DiagonalMatrix<Scalar> operator+ (DiagonalMatrix<Scalar> diag, const DiagonalMatrix<Scalar> other){
  diag += other;
  return diag;
}

template<class Scalar>
inline DiagonalMatrix<Scalar> operator- (DiagonalMatrix<Scalar> diag, const DiagonalMatrix<Scalar> other){
  diag *= Scalar{-1};
  diag += other;
  return diag;
}

// Vector <- Diagonal * Vector
template<class Scalar>
inline Vector<Scalar> operator* (const DiagonalMatrix<Scalar>& diag, Vector<Scalar> vect){
  vect.diagmv(diag);
  return vect;
}

// DenseMatrix <- Diagonal * DenseMatrix
template<class Scalar>
inline DenseMatrix<Scalar> operator*(const DiagonalMatrix<Scalar>& diag, DenseMatrix<Scalar> mat){
  mat.left_diagmm(diag);
  return mat;
}

// DenseMatrix <- DenseMatrix * Diagonal
template<class Scalar>
inline DenseMatrix<Scalar> operator*(DenseMatrix<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.right_diagmm(diag);
  return mat;
}

//DenseMatrix <- Diagonal + DenseMatrix
template<class Scalar>
inline DenseMatrix<Scalar> operator+(DenseMatrix<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.add(diag);
  return mat;
}

//DenseMatrix <- DenseMatrix + Diagonal
template<class Scalar>
inline DenseMatrix<Scalar> operator+(const DiagonalMatrix<Scalar>& diag, DenseMatrix<Scalar> mat){
  mat.add(diag);
  return mat;
}

//DenseMatrix <- DenseMatrix - Diagonal
template<class Scalar>
inline DenseMatrix<Scalar> operator-(DenseMatrix<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.add(diag, Scalar{-1.0});
  return mat;
}

//DenseMatrix <- Diagonal - DenseMatrix
template<class Scalar>
inline DenseMatrix<Scalar> operator-(const DiagonalMatrix<Scalar>& diag, DenseMatrix<Scalar> mat){
  mat *= Scalar{-1};
  mat.add(diag);
  return mat;
}

// COO <- COO * Diagonal
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator*(SparseMatrixCOO<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.right_diagmm(diag);
  return mat;
}

// COO <- Diagonal * COO
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator*(const DiagonalMatrix<Scalar>& diag, SparseMatrixCOO<Scalar> mat){
  mat.left_diagmm(diag);
  return mat;
}

//SparseMatrixCOO <- Diagonal + SparseMatrixCOO
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator+(SparseMatrixCOO<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat += diag;
  return mat;
}

//SparseMatrixCOO <- SparseMatrixCOO + Diagonal
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator+(const DiagonalMatrix<Scalar>& diag, SparseMatrixCOO<Scalar> mat){
  mat += diag;
  return mat;
}

//SparseMatrixCOO <- SparseMatrixCOO - Diagonal
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator-(SparseMatrixCOO<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat -= diag;
  return mat;
}

//SparseMatrixCOO <- Diagonal - SparseMatrixCOO
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator-(const DiagonalMatrix<Scalar>& diag, SparseMatrixCOO<Scalar> mat){
  mat *= Scalar{-1};
  mat += diag;
  return mat;
}

/*---------------*/
/*----- CSC -----*/
/*---------------*/

// CSC <- CSC * Diagonal
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator*(SparseMatrixCSC<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.right_diagmm(diag);
  return mat;
}

// CSC <- Diagonal * CSC
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator*(const DiagonalMatrix<Scalar>& diag, SparseMatrixCSC<Scalar> mat){
  mat.left_diagmm(diag);
  return mat;
}

//SparseMatrixCSC <- Diagonal + SparseMatrixCSC
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator+(SparseMatrixCSC<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat += diag;
  return mat;
}

//SparseMatrixCSC <- SparseMatrixCSC + Diagonal
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator+(const DiagonalMatrix<Scalar>& diag, SparseMatrixCSC<Scalar> mat){
  mat += diag;
  return mat;
}

//SparseMatrixCSC <- SparseMatrixCSC - Diagonal
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator-(SparseMatrixCSC<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat -= diag;
  return mat;
}

//SparseMatrixCSC <- Diagonal - SparseMatrixCSC
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator-(const DiagonalMatrix<Scalar>& diag, SparseMatrixCSC<Scalar> mat){
  mat *= Scalar{-1};
  mat += diag;
  return mat;
}
// Out of class operators:2 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Traits][Traits:1]]
template<typename Scalar> struct vector_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = Vector<Scalar>;
};

template<typename Scalar> struct sparse_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = SparseMatrixCSC<Scalar>;
};

template<typename Scalar> struct scalar_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = Scalar;
};

template<typename Scalar> struct is_dense<DiagonalMatrix<Scalar>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Container functions][Container functions:1]]
// Container functions
template<class Scalar>
Scalar * begin(DiagonalMatrix<Scalar>& v){ return v.get_ptr(); }
template<class Scalar>
const Scalar * begin(const DiagonalMatrix<Scalar>& v){ return v.get_ptr(); }

template<class Scalar>
Scalar * end(DiagonalMatrix<Scalar>& v){ return v.get_ptr() + v.get_n_rows(); }
template<class Scalar>
const Scalar * end(const DiagonalMatrix<Scalar>& v){ return v.get_ptr() + v.get_n_rows(); }
// Container functions:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Interface functions][Interface functions:1]]
template<typename Scalar>
[[nodiscard]] inline Size n_rows(const DiagonalMatrix<Scalar>& mat) { return mat.get_n_rows(); }

template<typename Scalar>
[[nodiscard]] inline Size n_cols(const DiagonalMatrix<Scalar>& mat) { return mat.get_n_cols(); }

template<typename Scalar>
[[nodiscard]] inline DiagonalMatrix<Scalar> diagonal(const DiagonalMatrix<Scalar>& mat) { return mat; }

template<typename Scalar>
[[nodiscard]] inline Scalar * get_ptr(DiagonalMatrix<Scalar>& mat) { return mat.get_ptr(); }

template<typename Scalar>
[[nodiscard]] inline const Scalar * get_ptr(const DiagonalMatrix<Scalar>& mat) { return mat.get_ptr(); }

template<class Scalar>
[[nodiscard]] DiagonalMatrix<Scalar> adjoint(const DiagonalMatrix<Scalar>& mat){ return mat.t(); }

template<class Scalar>
void display(const DiagonalMatrix<Scalar>& v, const std::string& name="", std::ostream &out = std::cout){ v.display(name, out); }

template<typename Scalar>
void build_matrix(DiagonalMatrix<Scalar>& mat, const int M, const int N, const int nnz, int * i, int * j, Scalar * v,  const bool){
  mat = DiagonalMatrix<Scalar>(M, N);
  MAPHYSPP_ASSERT(M == nnz, "build_matrix(Diagonal matrix) with nnz != M");
  MAPHYSPP_ASSERT(M == nnz, "build_matrix(Diagonal matrix) with nnz != N");
  for(int k = 0; k < nnz; ++k){
    MAPHYSPP_ASSERT(i[k] == j[k], "build_matrix(Diagonal matrix) with extradiagonal elements");
    mat(i[k]) = v[k];
  }
}
// Interface functions:1 ends here

// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
