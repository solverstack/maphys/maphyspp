// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>

namespace maphys {
template<MPH_Scalar, MPH_Integral = int> class SparseMatrixCSC;
template<class, class> struct CSCIterator;
}

#include "maphys/loc_data/SparseMatrixCOO.hpp"
#include "maphys/loc_data/SparseMatrixLIL.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Attributes][Attributes:1]]
template<MPH_Scalar Scalar, MPH_Integral Index>
class SparseMatrixCSC  : public SparseMatrixBase<Scalar, Index>
{

public:
  using local_type = SparseMatrixCSC<Scalar, Index>;

private:
  using BaseT = SparseMatrixBase<Scalar, Index>;

  using Real = typename BaseT::Real;
  using Idx_arr = typename BaseT::Idx_arr;
  using Scal_arr = typename BaseT::Scal_arr;

  using BaseT::_m;
  using BaseT::_n;
  using BaseT::_nnz;

  using BaseT::_i;
  using BaseT::_j;
  using BaseT::_v;
// Attributes:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Check consistency for attributes][Check consistency for attributes:1]]
// Check that values in i < M, in j < N
void check_values_range(){
  for(Size k = 0; k < _nnz; ++k ){ MAPHYSPP_ASSERT( _i[k] < static_cast<Index>(_m), "CSC: value in i >= m"); }
  for(Size k = 0; k < _n + 1; ++k ){ MAPHYSPP_ASSERT( _j[k] <= static_cast<Index>(_nnz), "CSC: value in j > nnz"); }
}
// Check consistency for attributes:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Constructors][Constructors:1]]
public:

  explicit SparseMatrixCSC(const Size m, const Size n): BaseT(m, n, 0) {}
  explicit SparseMatrixCSC(): BaseT(0, 0, 0) {}

  explicit SparseMatrixCSC(const Size m, const Size n, const Size nnz, Index * i, Index * j, Scalar * v):
    BaseT{m, n, nnz, Idx_arr(i, nnz), Idx_arr(j, n+1), Scal_arr(v, nnz)}
  {
    check_values_range();
  }

  explicit SparseMatrixCSC(const Size m, const Size n, const Size nnz, const Idx_arr& i, const Idx_arr& j, const Scal_arr& v):
    BaseT{m, n, nnz, i, j, v}
  {
    check_values_range();
  }

  explicit SparseMatrixCSC(const Size m, const Size n, const Size nnz, Idx_arr&& i, Idx_arr&& j, Scal_arr&& v):
    BaseT{m, n, nnz, std::move(i), std::move(j), std::move(v)}
  {
    check_values_range();
  }

  // Initialize with a triplet of vectors ({i}, {j}, {v})
  // Guess m and n as maximums of i and j (if not given)
  // To be used for small matrices (quick testing)
  explicit SparseMatrixCSC(std::initializer_list<Index> i, std::initializer_list<Index> j,
                           std::initializer_list<Scalar> v, const Size m = 0, const Size n = 0):
    BaseT(m, n, v.size())
  {
    MAPHYSPP_ASSERT( _nnz == i.size(), "Initialize sparse matrix with size(i) != size(v)");
    // Auto-detect matrix dimensions with max (when not given by the user)
    if(_n == 0 && _nnz != 0){
      _n = j.size() - 1;
    }
    else{
      MAPHYSPP_ASSERT( j.size() == _n + 1, "Initialize sparse matrix with size(j) != n + 1");
    }
    if (_m == 0 && _nnz != 0){
      for(Index k : i){ _m = static_cast<Size>((std::max(k, static_cast<Index>(_m)))); }
      ++_m; // Because starting at 0
    }
    _i = Idx_arr(i.begin(), _nnz);
    _j = Idx_arr(j.begin(), _n + 1);
    _v = Scal_arr(v.begin(), _nnz);
    check_values_range();
  }

  // From a dense matrix
  SparseMatrixCSC(const DenseMatrix<Scalar>& dm, Real drop=1e-15):
   BaseT(dm.get_n_rows(), dm.get_n_cols(), 0)
  {
    // Count nnz
    const Scalar * dm_data = dm.get_ptr();
    _nnz = 0;
    for(Size k = 0; k < _m * _n; ++k){
      if( std::abs(dm_data[k]) > drop) ++_nnz;
    }
    // Create and fill ijv
    _i = Idx_arr( _nnz );
    _j = Idx_arr( _n + 1 );
    _v = Scal_arr( _nnz );
    int k = 0;
    _j[0] = 0;
    for(Size j = 0; j < _n; ++j){
      for(Size i = 0; i < _m; ++i){
        if( std::abs(dm_data[j * _m + i]) > drop){
          _i[k] = i;
          _v[k] = dm_data[ j * _m + i ];
          k++;
        }
      }
      _j[j+1] = k;
    }
    this->copy_properties(dm);
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Conversion functions][Conversion functions:1]]
// Conversion
void from_coo(const SparseMatrixCOO<Scalar, Index>& mat){
  _m = n_rows(mat);
  _n = n_cols(mat);
  _nnz = n_nonzero(mat);

  _i = Idx_arr( _nnz );
  _j = Idx_arr( _n + 1 );
  _v = Scal_arr( _nnz );

  Idx_arr i(mat.get_i_ptr(), _nnz);
  Idx_arr j(mat.get_j_ptr(), _nnz);
  Scal_arr v(mat.get_v_ptr(), _nnz);

  // Sort in j direction
  auto indices = multilevel_argsort(j, i);
  i = i[indices];
  j = j[indices];
  v = v[indices];

  _j[0] = 0;
  Index cur_j = 0;

  for(Size k = 0; k < _nnz; ++k){
    _i[k] = i[k];
    _v[k] = v[k];
    while(j[k] > cur_j){
      cur_j++;
      _j[cur_j] = k;
    }
  }
  cur_j++;
  while(cur_j < static_cast<Index>(_n + 1)){
    _j[cur_j] = _nnz;
    cur_j++;
  }

  this->copy_properties(mat);
}

template<typename OutIndex = Index>
SparseMatrixCOO<Scalar, OutIndex> to_coo() const{
  SparseMatrixCOO<Scalar, OutIndex> out;
  out.template from_csc<Index>(*this);
  out.copy_properties(*this);
  return out;
}

DenseMatrix<Scalar> to_dense() const{
  DenseMatrix<Scalar> dense(_m, _n);
  for(Index k = 0; k < static_cast<Index>(_n); ++k){
    for(Index k2 = _j[k]; k2 < _j[k+1]; ++k2){
      dense(_i[k2], k) = _v[k2];
    }
  }
  dense.copy_properties(*this);
  return dense;
}

void convert(SparseMatrixCSC& out_mat) const { out_mat = *this; }
void convert(SparseMatrixCOO<Scalar>& out_mat) const { out_mat = this->to_coo(); }
void convert(DenseMatrix<Scalar>& out_mat) const { out_mat = this->to_dense(); }
// Conversion functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Load matrix from a matrix market file][Load matrix from a matrix market file:1]]
// From matrix_market file
void from_matrix_market_file(const std::string &filename){
  SparseMatrixCOO<Scalar> sp_mat;
  sp_mat.from_matrix_market_file(filename);
  this->from_coo(sp_mat);
}
// Load matrix from a matrix market file:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Comparison functions][Comparison functions:1]]
bool operator==(const SparseMatrixCSC& spmat2) const {
  if(this->_m != spmat2._m) return false;
  if(this->_n != spmat2._n) return false;
  if(this->_nnz != spmat2._nnz) return false;

  for(Size k = 0; k < this->_nnz; ++k){
    if(this->_i[k] != spmat2._i[k]) return false;
    if(this->_v[k] != spmat2._v[k]) return false;
  }
  for(Size k = 0; k < this->_n + 1; ++k){
    if(this->_j[k] != spmat2._j[k]) return false;
  }
  return true;
}

bool operator!=(const SparseMatrixCSC& spmat2) const { return !(*this == spmat2); }

void insert(Index i, Index j, Scalar v){
  MAPHYSPP_ASSERT(i < static_cast<Index>(_m), "SparseMatrixCSC: insert i >= M");
  MAPHYSPP_ASSERT(j < static_cast<Index>(_n), "SparseMatrixCSC: insert j >= N");

  for(Index k = j+1; k < static_cast<Index>(_n + 1); ++k){
    _j[k]++;
  }

  Idx_arr old_i = _i;
  Scal_arr old_v = _v;

  _nnz++;
  _i = Idx_arr(_nnz);
  _v = Scal_arr(_nnz);

  const Index pos = _j[j]+1;
  for(Index k2 = 0; k2 < pos; ++k2){
    _i[k2] = old_i[k2];
    _v[k2] = old_v[k2];
  }
  _i[pos] = i;
  _v[pos] = v;
  for(Index k2 = pos+1; k2 < static_cast<Index>(_nnz); ++k2){
    _i[k2] = old_i[k2-1];
    _v[k2] = old_v[k2-1];
  }
}
// Comparison functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Diagonal extraction][Diagonal extraction:1]]
DiagonalMatrix<Scalar> diag() const {
  DiagonalMatrix<Scalar> diag(_m, _n);
  const Size size = std::min(_m, _n);
  for(Index k = 0; k < static_cast<Index>(size); ++k){
    for(Index k2 = _j[k]; k2 < _j[k+1]; ++k2){
      if(k == _i[k2]) diag(k) = _v[k2];
    }
  }
  return diag;
}

Vector<Scalar> diag_vect() const {
  Vector<Scalar> diag(std::min(_m, _n));
  const Size size = std::min(_m, _n);
  for(Index k = 0; k < static_cast<Index>(size); ++k){
    for(Index k2 = _j[k]; k2 < _j[k+1]; ++k2){
      if(k == _i[k2]) diag(k) = _v[k2];
    }
  }
  return diag;
}
// Diagonal extraction:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Re-indexing][Re-indexing:1]]
void reindex(const IndexArray<Index> new_indices, int new_m = -1){
  SparseMatrixCOO<Scalar, Index> s = this->to_coo();
  s.reindex(new_indices, new_m);
  *this = s.to_csc();
}

void reindex(const IndexArray<Index> new_indices, const IndexArray<Index> old_indices, int new_m = -1){
  SparseMatrixCOO<Scalar, Index> s = this->to_coo();
  s.reindex(new_indices, old_indices, new_m);
  *this = s.to_csc();
}
// Re-indexing:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Cast function][Cast function:1]]
template<MPH_Scalar OtherScalar>
SparseMatrixCSC<OtherScalar, Index> cast() const { 
  SparseMatrixCSC<OtherScalar, Index> out(_m, _n, _nnz, _i, _j, IndexArray<OtherScalar>(_v));
  out.copy_properties(*this);
  return out;
}
// Cast function:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Fill matrix from half storage to full storage][Fill matrix from half storage to full storage:1]]
void fill_tri_to_full(){       
  if(this->is_storage_full()){ return; }

  SparseMatrixLIL<Scalar, Index> lilmat(this->get_n_rows(), this->get_n_cols());
  for(Index j = 0; j < static_cast<Index>(this->get_n_cols()); ++j){
    for(Index k2 = _j[j]; k2 < _j[j+1]; ++k2){
      lilmat.insert(_i[k2], j, _v[k2]);
      if(_i[k2] != j) lilmat.insert(j, _i[k2], _v[k2]);
    }
  }

  this->set_property(MatrixStorage::full);
  (*this) = std::move(lilmat.to_csc());
}
// Fill matrix from half storage to full storage:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Make storage triangular][Make storage triangular:1]]
void to_triangular(MatrixStorage storage = MatrixStorage::lower){
  MAPHYSPP_ASSERT(this->is_symmetric() || this->is_hermitian(),
                "SparseMatrixCOO: asking for half storage with non symmetric matrix");
  if(this->get_storage_type() == storage){ return; }
  if((this->get_storage_type() == MatrixStorage::lower && storage == MatrixStorage::upper)
     || (this->get_storage_type() == MatrixStorage::upper && storage == MatrixStorage::lower)){
     auto sym = this->get_symmetry();
     this->set_property(MatrixStorage::full, MatrixSymmetry::general);
     (*this) = this->t();
     this->set_property(sym, storage);
     return;
  }

  SparseMatrixLIL<Scalar, Index> lilmat(this->get_n_rows(), this->get_n_cols());
  for(Index j = 0; j < static_cast<Index>(this->get_n_cols()); ++j){
    for(Index k2 = _j[j]; k2 < _j[j+1]; ++k2){
      if(storage == MatrixStorage::lower && _i[k2] >= j) lilmat.insert(j, _i[k2], _v[k2]);
      else if(storage == MatrixStorage::upper && _i[k2] <= j) lilmat.insert(j, _i[k2], _v[k2]);
    }
  }

  this->set_property(storage);
  (*this) = std::move(lilmat.to_csc());
}
// Make storage triangular:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Break symmetry][Break symmetry:1]]
void break_symmetry(){
  this->fill_tri_to_full();
  this->set_property(MatrixSymmetry::general);
}
// Break symmetry:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Element accessor][Element accessor:1]]
#define SPMATCSC_COEFF_ACCESSOR do{               \
    if(this->is_storage_lower() && (i < j)){      \
      std::swap(i, j);                            \
    }                                             \
    else if(this->is_storage_upper() && (i > j)){ \
      std::swap(i, j);                            \
    }                                             \
                                                  \
  for(Index k = _j[j]; k < _j[j+1]; ++k){         \
    if(_i[k] == i){                               \
      return _v[k];                               \
    }                                             \
  }} while(0)

[[nodiscard]] Scalar coeff(Index i, Index j) const {
  SPMATCSC_COEFF_ACCESSOR;
  return Scalar{0};
}

// Element accessor (access is linear in m)
[[nodiscard]] const Scalar& coeffRef(Index i, Index j) const {
  SPMATCSC_COEFF_ACCESSOR;
  throw std::runtime_error("const SparseMatrixCSC(i,j) does not point to a non zero value");
  return _v[0];
}

[[nodiscard]] Scalar& coeffRef(Index i, Index j) {
  SPMATCSC_COEFF_ACCESSOR;
  throw std::runtime_error("const SparseMatrixCSC(i,j) does not point to a non zero value");
  return _v[0];
}

[[nodiscard]] const Scalar& operator()(Index i, Index j) const { return this->coeffRef(i, j); }
[[nodiscard]] Scalar& operator()(Index i, Index j) { return this->coeffRef(i, j); }
[[nodiscard]] std::tuple<Scalar*, Index*, Size> get_col(Index j) { return std::make_tuple(&_v[_j[j]], &_i[_j[j]], _j[j+1] - _j[j]); }
// Element accessor:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Memory cost][Memory cost:1]]
[[nodiscard]] inline Size memcost(){
  constexpr Size elt_size = (sizeof(Size) + sizeof(Scalar));
  return  elt_size * _nnz + (_n + 1) * sizeof(Size);
}
// Memory cost:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Begin and end][Begin and end:1]]
IJVIterator<Scalar, Index, SparseMatrixCSC<Scalar, Index>> begin() const{
  IJVIterator<Scalar, Index, SparseMatrixCSC<Scalar, Index>> ite(*this);
  return ite.begin();
}

IJVIterator<Scalar, Index, SparseMatrixCSC<Scalar, Index>> end() const{
  IJVIterator<Scalar, Index, SparseMatrixCSC<Scalar, Index>> ite(*this);    
  return ite.end();
}
// Begin and end:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Serialization][Serialization:1]]
std::vector<char> serialize() const {
  // M, N, values
  int M = static_cast<int>(_m);
  int N = static_cast<int>(_n);
  int NNZ = static_cast<int>(_nnz);
  std::vector<char> buffer((3 + NNZ + N + 1) * sizeof(int) + NNZ * sizeof(Scalar));
  int cnt = 0;

  std::memcpy(&buffer[cnt], &M, sizeof(int)); cnt += sizeof(int);
  std::memcpy(&buffer[cnt], &N, sizeof(int)); cnt += sizeof(int);
  std::memcpy(&buffer[cnt], &NNZ, sizeof(int)); cnt += sizeof(int);
  std::memcpy(&buffer[cnt], &_i[0], _nnz * sizeof(int)); cnt += _nnz * sizeof(int);
  std::memcpy(&buffer[cnt], &_j[0], (_n + 1) * sizeof(int)); cnt += (_n + 1) * sizeof(int);
  std::memcpy(&buffer[cnt], &_v[0], _nnz * sizeof(Scalar));

  return buffer;
}

void deserialize(const std::vector<char>& buffer){
  int M, N, NNZ;
  int cnt = 0;
  std::memcpy(&M, &buffer[cnt], sizeof(int)); cnt += sizeof(int);
  std::memcpy(&N, &buffer[cnt], sizeof(int)); cnt += sizeof(int);
  std::memcpy(&NNZ, &buffer[cnt], sizeof(int)); cnt += sizeof(int);
  _m = static_cast<Size>(M);
  _n = static_cast<Size>(N);
  _nnz = static_cast<Size>(NNZ);
  _i = Idx_arr(_nnz);
  _j = Idx_arr(_n + 1);
  _v = Scal_arr(_nnz);

  std::memcpy(&_i[0], &buffer[cnt], _nnz * sizeof(int)); cnt += _nnz * sizeof(int);
  std::memcpy(&_j[0], &buffer[cnt], (_n + 1) * sizeof(int)); cnt += (_n + 1) * sizeof(int);
  std::memcpy(&_v[0], &buffer[cnt], _nnz * sizeof(Scalar));
}
// Serialization:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Operators][Operators:1]]
// Scalar multiplication
using SparseMatrixBase<Scalar, Index>::operator*=;

// Addition and substraction (not implemented)
SparseMatrixCSC& operator+= (const SparseMatrixCSC& other){
  MAPHYSPP_DIM_ASSERT( _m, n_rows(other), "SparseMatrixCSC += SparseMatrixCSC: different nb rows.");
  MAPHYSPP_DIM_ASSERT( _n, n_cols(other), "SparseMatrixCSC += SparseMatrixCSC: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(_m, _n);
  lilmat.copy_properties(*this);
  lilmat.insert(*this);
  lilmat.insert(other); // Duplicates are summed by default
  lilmat.drop(); // Drop zeros
  *this = std::move(lilmat.to_csc());
  return *this;
}

SparseMatrixCSC& operator-= (const SparseMatrixCSC& other){
  MAPHYSPP_DIM_ASSERT( _m, n_rows(other), "SparseMatrixCSC += SparseMatrixCSC: different nb rows.");
  MAPHYSPP_DIM_ASSERT( _n, n_cols(other), "SparseMatrixCSC += SparseMatrixCSC: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(_m, _n);
  lilmat.copy_properties(*this);
  lilmat.insert(other);
  lilmat *= Scalar{-1};
  lilmat.insert(*this);
  lilmat.drop(); // Drop zeros
  *this = std::move(lilmat.to_csc());    
  return *this;
}

SparseMatrixCSC& operator*= (const SparseMatrixCSC& other){
  MAPHYSPP_DIM_ASSERT(_n, n_rows(other), "SparseMatrixCSC x SparseMatrixCSC");

  SparseMatrixLIL<Scalar> lilmat(_m, n_cols(other));

  auto insert_nnz = [&lilmat](const Index& i1, const Index& j1, const Index& i2, const Index& j2, const Scalar& v1, const Scalar& v2){
    if(j1 == i2){
      lilmat.insert(i1, j2, v1 * v2);
    }
  };

  const bool tri_storage_1 = !(this->is_storage_full());
  const bool tri_storage_2 = !(other.is_storage_full());

  for(Index j1 = 0; j1 < static_cast<Index>(_n); ++j1){
    for(Index k1 = _j[j1]; k1 < _j[j1+1]; ++k1){
      for(Index j2 = 0; j2 < static_cast<Index>(other._n); ++j2){
        for(Index k2 = other._j[j2]; k2 < other._j[j2+1]; ++k2){
          const Index& i1 = _i[k1];
          const Scalar& v1 = _v[k1];
          const Index& i2 = other._i[k2];
          const Scalar& v2 = other._v[k2];

          // A(i1 j1) x B(i1 j2)
          insert_nnz(i1, j1, i2, j2, v1, v2);

          if(tri_storage_1 && (i1 != j1)){
            // A(j1 i1) x B(i1 j2)
            insert_nnz(j1, i1, i2, j2, v1, v2);

            if(tri_storage_2 && (j2 != i2)){
              // A(j1 i1) x B(j2 i2)
              insert_nnz(j1, i1, j2, i2, v1, v2);
            }
          }

          if(tri_storage_2 && (j2 != i2)){
            // A(i1 j1) x B(j2 i2)
            insert_nnz(i1, j1, j2, i2, v1, v2);
          }
        }
      }
    }
  }

  lilmat.drop();
  *this = std::move(lilmat.to_csc());

  return *this;
}
// Operators:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Operators with diagonal matrix][Operators with diagonal matrix:1]]
SparseMatrixCSC& operator+= (const DiagonalMatrix<Scalar>& other){
  MAPHYSPP_ASSERT( _m == n_rows(other), "SparseMatrixCSC += SparseMatrixCSC: different nb rows.");
  MAPHYSPP_ASSERT( _n == n_cols(other), "SparseMatrixCSC += SparseMatrixCSC: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(_m, _n);
  lilmat.copy_properties(*this);
  lilmat.insert(*this);
  lilmat.insert(other);
  lilmat.drop(); // Drop zeros
  *this = std::move(lilmat.to_csc());
  return *this;
}

SparseMatrixCSC& operator-= (const DiagonalMatrix<Scalar>& other){
  MAPHYSPP_ASSERT( _m == n_rows(other), "SparseMatrixCSC += SparseMatrixCSC: different nb rows.");
  MAPHYSPP_ASSERT( _n == n_cols(other), "SparseMatrixCSC += SparseMatrixCSC: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(_m, _n);
  lilmat.copy_properties(*this);
  lilmat.insert(other);
  lilmat *= Scalar{-1};
  lilmat.insert(*this);
  lilmat.drop(); // Drop zeros
  *this = std::move(lilmat.to_csc());
  return *this;
}

// A <- A * D
void right_diagmm(const DiagonalMatrix<Scalar>& other){
  Size M = _m;
  Size N = n_cols(other);
  MAPHYSPP_DIM_ASSERT( _n, n_rows(other), "SparseMatrixCSC *= DiagonalMatrix");

  this->break_symmetry();
  SparseMatrixLIL<Scalar> lilmat(M, N);
  lilmat.copy_properties(*this);
  const Scalar * diag = other.get_ptr();

  // Multiplication by diagonal on the right is scaling the columns
  const Index dim = static_cast<Index>(std::min(N, _n));
  for(Index j = 0; j < dim; ++j){
    for(Index k = _j[j]; k < _j[j+1]; ++k){
      lilmat.insert(_i[k], j, _v[k] * diag[j]);
    }
  }

  lilmat.drop();
  *this = std::move(lilmat.to_csc());
}

// A <- D * A
void left_diagmm(const DiagonalMatrix<Scalar>& other){
  Size M = n_rows(other);
  Size N = _n;
  MAPHYSPP_DIM_ASSERT( _m, n_cols(other), "SparseMatrixCSC *= DiagonalMatrix");

  this->break_symmetry();
  SparseMatrixLIL<Scalar> lilmat(M, N);
  lilmat.copy_properties(*this);
  const Scalar * diag = other.get_ptr();

  // Multiplication by diagonal on the right is scaling the rows
  const Index dim = static_cast<Index>(std::min(M, _m));

  for(Size j = 0; j < _n; ++j){
    for(Index k = _j[j]; k < _j[j+1]; ++k){
      if(_i[k] >= dim) break;
      lilmat.insert(_i[k], j, _v[k] * diag[_i[k]]);
    }
  }

  lilmat.drop();
  *this = std::move(lilmat.to_csc());
}

SparseMatrixCSC& operator*= (const DiagonalMatrix<Scalar>& other){
  this->right_diagmm(other);
  return *this;
}
// Operators with diagonal matrix:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Display function][Display function:1]]
// Pretty printing
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << name << '\n';
  out << "m: " << _m << " "
      << "n: " << _n << " "
      << "nnz: " << _nnz << '\n'
      << this->properties_str()
      << '\n' << '\n';

  for(Size k = 0; k < _n; ++k){
    out << "Column " << k << '\n' << "i\tv" << '\n';
    for(Index k2 = _j[k]; k2 < _j[k+1]; ++k2){
      out << _i[k2] << "\t" << _v[k2] << '\n';
    }
  }

  out << '\n';
}
// Display function:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Transposition][Transposition:1]]
[[nodiscard]] SparseMatrixCSC t() const {
  if(this->is_symmetric()) return *this;
  SparseMatrixLIL<Scalar, Index> lil_m( _n, _m);
  for(Index col = 0; col < static_cast<Index>(_n); ++col){
    for(Index v_ptr = _j[col]; v_ptr < _j[col+1]; ++v_ptr){
      lil_m.insert(col, _i[v_ptr], conj<Scalar>(_v[v_ptr]));
    }
  }
  return lil_m.to_csc();
}
// Transposition:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Out of class operators][Out of class operators:1]]
}; // class SparseMatrixCSC
// Out of class operators:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Out of class operators][Out of class operators:2]]
// Scalar multiplication
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator* (SparseMatrixCSC<Scalar> sp_mat, const Scalar& scal){
  sp_mat *= scal;
  return sp_mat;
}

template<class Scalar>
inline SparseMatrixCSC<Scalar> operator* (Scalar scal, const SparseMatrixCSC<Scalar>& sp_mat){
  return sp_mat * scal;
}

template<class Scalar>
inline SparseMatrixCSC<Scalar> operator/ (SparseMatrixCSC<Scalar> sp_mat, const Scalar& scal){
  sp_mat /= scal;
  return sp_mat;
}

template<class Scalar>
inline SparseMatrixCSC<Scalar> operator+ (SparseMatrixCSC<Scalar> sp_mat, const SparseMatrixCSC<Scalar>& other){
  sp_mat += other;
  return sp_mat;
}

template<class Scalar>
inline SparseMatrixCSC<Scalar> operator- (SparseMatrixCSC<Scalar> sp_mat, const SparseMatrixCSC<Scalar>& other){
  sp_mat -= other;
  return sp_mat;
}

template<class Scalar>
inline SparseMatrixCSC<Scalar> operator* (SparseMatrixCSC<Scalar> sp_mat_l, const SparseMatrixCSC<Scalar>& sp_mat_r){
  sp_mat_l *= sp_mat_r;
  return sp_mat_l;
}
// Out of class operators:2 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Matrix inverse operator][Matrix inverse operator:1]]
template<MPH_Scalar Scalar, MPH_Integral Index>
auto operator~(const SparseMatrixCSC<Scalar, Index>& A){
  using solver = typename default_sparse_solver<SparseMatrixCSC<Scalar, Index>, Vector<Scalar>>::type;
  static_assert(!std::is_same_v<solver, Identity<SparseMatrixCSC<Scalar, Index>, Vector<Scalar>>>,
                "SparseMatrixCSC:operator~ Pastix or Mumps is required to solve sparse linear system");
  return solver(A);
}

template<MPH_Scalar Scalar, MPH_Integral Index>
Vector<Scalar> operator%(const SparseMatrixCSC<Scalar, Index>& A, const Vector<Scalar>& b){
  return ~A * b; 
}
// Matrix inverse operator:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Traits][Traits:1]]
template<typename Scalar, MPH_Integral Index> struct scalar_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = Scalar;
};

template<typename Scalar, MPH_Integral Index> struct vector_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = Vector<Scalar>;
};

template<typename Scalar, MPH_Integral Index> struct dense_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = DenseMatrix<Scalar>;
};

template<typename Scalar, MPH_Integral Index> struct diag_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = DiagonalMatrix<Scalar>;
};

template<typename Scalar, MPH_Integral Index> struct is_sparse<SparseMatrixCSC<Scalar, Index>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Interface functions][Interface functions:1]]
template<class Scalar>
SparseMatrixCSC<Scalar> adjoint(const SparseMatrixCSC<Scalar>& mat){ return mat.t(); }

template<typename Scalar>
void build_matrix(SparseMatrixCSC<Scalar>& mat, const int M, const int N, const int nnz, int * i, int * j, Scalar * v,
const bool fill_symmetry =  false){
  SparseMatrixCOO<Scalar> coo_mat;
  build_matrix(coo_mat, M, N, nnz, i, j, v, fill_symmetry);
  mat.from_coo(coo_mat);
}

template<MPH_Scalar Scalar, MPH_Integral Tint>
void reindex(SparseMatrixCSC<Scalar>& mat, const IndexArray<Tint> new_indices, int new_m = -1){
  mat.reindex(new_indices, new_m);
}
// Interface functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Iterator][Iterator:2]]
template<class Scalar, class Index>
struct IJVIterator<Scalar, Index, SparseMatrixCSC<Scalar, Index>>{
  using Triplet = typename std::tuple<Index, Index, Scalar>;
  Index cnt;
  Index next_j;
  const Index * i;
  const Index * j;
  const Scalar * v;
  Size nnz;

  IJVIterator(const SparseMatrixCSC<Scalar, Index>& coo):
    i{coo.get_i_ptr()}, j{coo.get_j_ptr()}, v{coo.get_v_ptr()}, nnz{coo.get_nnz()}
  {}

  IJVIterator& operator++(){
    cnt++;
    return *this;
  }

  Triplet operator*(){
    while(j[next_j] <= cnt){
      next_j++;
    }
    return std::make_tuple(i[cnt], next_j - 1, v[cnt]);
  }

  IJVIterator& begin(){
    cnt = 0;
    next_j = 0;
    return *this;
  }

  IJVIterator end(){
    IJVIterator end = *this;
    end.cnt = end.nnz;
    return end;
  }

  bool operator==(const IJVIterator& other) const {
    if(this->cnt != other.cnt) return false;
    if(this->i != other.i) return false;
    return true;
  }

  bool operator!=(const IJVIterator& other) const {
    return !(*this == other);
  }
};
// Iterator:2 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCSC.org::*Footer][Footer:1]]
}  // namespace maphys
// Footer:1 ends here
