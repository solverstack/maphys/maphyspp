// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>

#include "maphys/loc_data/DenseMatrix.hpp"

// Expression templates

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Scaled vector][Scaled vector:1]]
// Scalar * vector
template<class Scalar>
struct ScalVect{
  using Vect = Vector<Scalar>;
  const Vect& v;
  const Scalar alpha;
  ScalVect(const Vect& vv, const Scalar s): v{vv}, alpha{s} {}
};
// Scaled vector:1 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Scaled matrix][Scaled matrix:1]]
// Scalar * matrix
template<class Scalar>
struct ScalMat{
  using Mat = DenseMatrix<Scalar>;
  const Mat& m;
  const Scalar alpha;
  ScalMat(const Mat& mm, const Scalar s): m{mm}, alpha{s} {}
};
// Scaled matrix:1 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Matrix vector product][Matrix vector product:1]]
template<class Scalar>
struct MatVect{
  using Mat = DenseMatrix<Scalar>;
  using Vect = Vector<Scalar>;

  const Mat& m;
  const Vect& v;
  const Scalar alpha;

  //mat * vect
  //alpha * (mat * vect)
  MatVect(const Mat& mm, const Vect& vv, const Scalar al=Scalar{1.0}): m{mm}, v{vv}, alpha{al} {}
};
// Matrix vector product:1 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV-like][GEMV-like:1]]
template<class Scalar>
struct ETgemv{
  // alpha * M * X + beta * Y
  using Mat = DenseMatrix<Scalar>;
  using Vect = Vector<Scalar>;

  const Scalar alpha;
  const Scalar beta;
  const Mat& A;
  const Vect& x;
  const Vect& y;

  ETgemv(const Scalar al, const Scalar be, const MatVect<Scalar>& ax, const Vect& v2):
    alpha{al}, beta{be}, A{ax.m}, x{ax.v}, y{v2} {}
};
// GEMV-like:1 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Matrix vector product][Matrix vector product:1]]
// Matrix vector product
// Ax
template<class Scalar>
inline MatVect<Scalar> operator*(const DenseMatrix<Scalar>& m, const Vector<Scalar>& v){
  return MatVect<Scalar>(m, v);
}
// Matrix vector product:1 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Matrix vector product][Matrix vector product:2]]
// alpha * (Ax)
template<class Scalar>
inline MatVect<Scalar> operator*(const Scalar s, const MatVect<Scalar>& mv){
  return MatVect<Scalar>(mv.m, mv.v, mv.alpha * s);
}
// Matrix vector product:2 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Matrix vector product][Matrix vector product:3]]
// (Ax) * alpha
template<class Scalar>
inline MatVect<Scalar> operator*(const MatVect<Scalar>& mv, const Scalar s){
  return MatVect<Scalar>(mv.m, mv.v, mv.alpha * s);
}
// Matrix vector product:3 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Matrix vector product][Matrix vector product:4]]
// A * (beta * x)
template<class Scalar>
inline MatVect<Scalar> operator*(const DenseMatrix<Scalar>& m, const ScalVect<Scalar>& sv){
  return MatVect<Scalar>(m, sv.v, sv.alpha);
}
// Matrix vector product:4 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Matrix vector product][Matrix vector product:5]]
// (alpha * A) * x
template<class Scalar>
inline MatVect<Scalar> operator*(const ScalMat<Scalar>& sm, const Vector<Scalar>& v){
  return MatVect<Scalar>(sm.m, v, sm.alpha);
}
// Matrix vector product:5 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Matrix vector product][Matrix vector product:6]]
// (alpha * A) * (beta * x)
template<class Scalar>
inline MatVect<Scalar> operator*(const ScalMat<Scalar>& sm, const ScalVect<Scalar>& sv){
  return MatVect<Scalar>(sm.m, sv.v, sm.alpha * sv.alpha);
}
// Matrix vector product:6 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV][GEMV:1]]
// GEMV expression template: alpha * M * X + beta * Y
// y + (alpha * Ax)
template<class Scalar>
inline ETgemv<Scalar> operator+(const Vector<Scalar>& v2, const MatVect<Scalar>& ax){
  return ETgemv<Scalar>(ax.alpha, Scalar{1}, ax, v2);
}
// GEMV:1 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV][GEMV:2]]
// (alpha * Ax) + y
template<class Scalar>
inline ETgemv<Scalar> operator+(const MatVect<Scalar>& ax, const Vector<Scalar>& v2){
  return ETgemv<Scalar>(ax.alpha, Scalar{1}, ax, v2);
}
// GEMV:2 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV][GEMV:3]]
// y - (alpha * Ax)
template<class Scalar>
inline ETgemv<Scalar> operator-(const Vector<Scalar>& v2, const MatVect<Scalar>& ax){
  return ETgemv<Scalar>(Scalar{-1} * ax.alpha, Scalar{1}, ax, v2);
}
// GEMV:3 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV][GEMV:4]]
// (alpha * Ax) - y
template<class Scalar>
inline ETgemv<Scalar> operator-(const MatVect<Scalar>& ax, const Vector<Scalar>& v2){
  return ETgemv<Scalar>(ax.alpha, Scalar{-1}, ax, v2);
}
// GEMV:4 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV][GEMV:5]]
// (beta * y) + (alpha * Ax)
template<class Scalar>
inline ETgemv<Scalar> operator+(const ScalVect<Scalar>& sv, const MatVect<Scalar>& ax){
  return ETgemv<Scalar>(ax.alpha, sv.alpha, ax, sv.v);
}
// GEMV:5 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV][GEMV:6]]
// (alpha * Ax) + (beta * y)
template<class Scalar>
inline ETgemv<Scalar> operator+(const MatVect<Scalar>& ax, const ScalVect<Scalar>& sv){
  return ETgemv<Scalar>(ax.alpha, sv.alpha, ax, sv.v);
}
// GEMV:6 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV][GEMV:7]]
// (beta * y) - (alpha * Ax)
template<class Scalar>
inline ETgemv<Scalar> operator-(const ScalVect<Scalar>& sv, const MatVect<Scalar>& ax){
  return ETgemv<Scalar>(Scalar{-1} * ax.alpha, sv.alpha, ax, sv.v);
}
// GEMV:7 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*GEMV][GEMV:8]]
// (alpha * Ax) - (beta * y)
template<class Scalar>
inline ETgemv<Scalar> operator-(const MatVect<Scalar>& ax, const ScalVect<Scalar>& sv){
  return ETgemv<Scalar>(ax.alpha, Scalar{-1} * sv.alpha, ax, sv.v);
}
// GEMV:8 ends here

// [[file:../../../org/maphys/loc_data/ETMatVec.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
