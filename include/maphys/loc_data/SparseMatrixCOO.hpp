// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>

namespace maphys {
template<MPH_Scalar, MPH_Integral = int> class SparseMatrixCOO;
template<class, class> struct COOIterator;
}

#include "maphys/loc_data/SparseMatrixBase.hpp"
#include "maphys/loc_data/SparseMatrixLIL.hpp"
#include "maphys/loc_data/SparseMatrixCSC.hpp"

#ifndef MAPHYSPP_NO_MPI
#include "maphys/dist/MPI.hpp"
#endif

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Attributes][Attributes:1]]
template<MPH_Scalar Scalar, MPH_Integral Index>
class SparseMatrixCOO  : public SparseMatrixBase<Scalar, Index>
{
public:
  using local_type = SparseMatrixCOO<Scalar, Index>;

private:
  using BaseT = SparseMatrixBase<Scalar, Index>;

  using Real = typename BaseT::Real;
  using Idx_arr = typename BaseT::Idx_arr;
  using Scal_arr = typename BaseT::Scal_arr;

  using BaseT::_m;
  using BaseT::_n;
  using BaseT::_nnz;

  using BaseT::_i;
  using BaseT::_j;
  using BaseT::_v;

  bool _sorted = false;
// Attributes:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Check consistency for attributes][Check consistency for attributes:1]]
// Check that values in i < M, in j < N
void check_values_range(){
  for(Size k = 0; k < _nnz; ++k ){ MAPHYSPP_ASSERT( _i[k] < static_cast<Index>(_m), "Value in i >= m"); }
  for(Size k = 0; k < _nnz; ++k ){ MAPHYSPP_ASSERT( _j[k] < static_cast<Index>(_n), "Value in j >= n"); }
}
// Check consistency for attributes:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Order indices][Order indices:1]]
public:
  // Check that values in i < M, in j < N
  void order_indices(){
    if(_sorted) return;
    auto idx = multilevel_argsort(_j, _i);
    _i = _i[idx];
    _j = _j[idx];
    _v = _v[idx];
    _sorted = true;
  }
// Order indices:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Constructors][Constructors:1]]
public:

  explicit SparseMatrixCOO(const Size m, const Size n): BaseT(m, n, 0) {}
  explicit SparseMatrixCOO(): BaseT(0, 0, 0) {}
  explicit SparseMatrixCOO(const Size m, const Size n, const Size nnz, Index * i, Index * j, Scalar * v):
    BaseT{m, n, nnz, Idx_arr(i, nnz), Idx_arr(j, nnz), Scal_arr(v, nnz)}
  {
    check_values_range();
  }

  explicit SparseMatrixCOO(const Size m, const Size n, const Size nnz, const Idx_arr& i, const Idx_arr& j, const Scal_arr& v):
    BaseT{m, n, nnz, i, j, v}
  {
    check_values_range();
  }

  explicit SparseMatrixCOO(const Size m, const Size n, const Size nnz, Idx_arr&& i, Idx_arr&& j, Scal_arr&& v):
    BaseT{m, n, nnz, std::move(i), std::move(j), std::move(v)}
  {
    check_values_range();
  }

  // Initialize with a triplet of vectors ({i}, {j}, {v})
  // Guess m and n as maximums of i and j (if not given)
  // To be used for small matrices (quick testing)
  explicit SparseMatrixCOO(std::initializer_list<Index> i, std::initializer_list<Index> j,
                           std::initializer_list<Scalar> v, const Size m = 0, const Size n = 0):
    BaseT(m, n, i.size())
  {
    MAPHYSPP_ASSERT( _nnz == j.size(), "Initialize sparse matrix with size(i) != size(j)");
    MAPHYSPP_ASSERT( _nnz == v.size(), "Initialize sparse matrix with size(i) != size(v)");
    // Auto-detect matrix dimensions with max (when not given by the user)
    if (_m == 0 && _nnz != 0){
      for(auto k : i){ _m = std::max(k, static_cast<Index>(_m)); }
      ++_m; // Because starting at 0
    }
    if (_n == 0 && _nnz != 0){
      for(auto k : j){ _n = std::max(k, static_cast<Index>(_n)); }
      ++_n; // Because starting at 0
    }
    _i = Idx_arr(i.begin(), _nnz);
    _j = Idx_arr(j.begin(), _nnz);
    _v = Scal_arr(v.begin(), _nnz);
    check_values_range();
  }

  // From a dense matrix
  SparseMatrixCOO(DenseMatrix<Scalar> dm, Real drop=1e-15):
    BaseT(dm.get_n_rows(), dm.get_n_cols(), 0)
  {
    // Count nnz
    Scalar * dm_data = dm.get_ptr();
    for(Size k = 0; k < _m * _n; ++k){
      if( std::abs(dm_data[k]) > drop) ++_nnz;
    }
    // Create and fill ijv
    _i = Idx_arr( _nnz );
    _j = Idx_arr( _nnz );
    _v = Scal_arr( _nnz );
    auto k = 0;
    for(Size j = 0; j < _n; ++j){
      for(Size i = 0; i < _m; ++i){
        if( std::abs(dm_data[j * _m + i]) > drop){
          _i[k] = i;
          _j[k] = j;
          _v[k] = dm_data[ j * _m + i ];
          k++;
        }
      }
    }
    this->copy_properties(dm);
  }

  SparseMatrixCOO(const SparseMatrixCSC<Scalar, Index>& csc_mat){
    this->from_csc(csc_mat);
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Conversion functions][Conversion functions:1]]
// Conversion
template<typename InIndex = Index>
void from_csc(const SparseMatrixCSC<Scalar, InIndex>& mat){
  _m = n_rows(mat);
  _n = n_cols(mat);
  _nnz = n_nonzero(mat);

  _i = Idx_arr( _nnz );
  _j = Idx_arr( _nnz );
  _v = Scal_arr( _nnz );

  IndexArray<InIndex> i(mat.get_i_ptr(), _nnz);
  IndexArray<InIndex> j(mat.get_j_ptr(), _n + 1);
  Scal_arr v(mat.get_v_ptr(), _nnz);

  for(Size k = 0; k < _n; ++k){
    for(Index k2 = j[k]; k2 < j[k+1]; ++k2){
      _i[k2] = static_cast<Index>(i[k2]);
      _j[k2] = static_cast<Index>(k);
      _v[k2] = v[k2];
    }
  }
  _sorted = true;
  this->copy_properties(mat);
}

SparseMatrixCSC<Scalar, Index> to_csc() const{
  SparseMatrixCSC<Scalar, Index> out;
  out.from_coo(*this);
  return out;
}

template<typename OutIndex = Index>
SparseMatrixCOO<Scalar, OutIndex> to_coo() const {
  if constexpr(std::is_same<Index,OutIndex>::value){
    return *this;
  }
  else{
    IndexArray<OutIndex> iarr(_nnz);
    IndexArray<OutIndex> jarr(_nnz);
    IndexArray<Scalar> varr(_nnz);
    for(Size k = 0; k < _nnz; ++k){
      iarr[k] = static_cast<OutIndex>(_i[k]);
      jarr[k] = static_cast<OutIndex>(_j[k]);
      varr[k] = _v[k];
    }
    auto outmat = SparseMatrixCOO<Scalar, OutIndex>(_m, _n, _nnz, std::move(iarr), std::move(jarr), std::move(varr));
    outmat.copy_properties(*this);
    return outmat;
  }
}

DenseMatrix<Scalar> to_dense() const{
  DenseMatrix<Scalar> dense(_m, _n);
  for(Size k = 0; k < _nnz; ++k){
    dense(_i[k], _j[k]) = _v[k];
  }
  dense.copy_properties(*this);
  return dense;
}

void convert(SparseMatrixCOO& out_mat) const { out_mat = *this; }
void convert(SparseMatrixCSC<Scalar>& out_mat) const { out_mat = this->to_csc(); }
void convert(DenseMatrix<Scalar>& out_mat) const { out_mat = this->to_dense(); }
// Conversion functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Load matrix from a matrix market file][Load matrix from a matrix market file:1]]
void from_matrix_market_file(const std::string &filename){
  MatrixSymmetry sym;
  MatrixStorage stor;
  int m, n, nnz;
  matrix_market::load<Scalar>(filename,
                              _i, _j, _v,
                              m, n, nnz,
                              sym, stor);
  _m = m;
  _n = n;
  _nnz = nnz;
  this->set_property(sym);
  this->set_property(stor);
  check_values_range();
}
// Load matrix from a matrix market file:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Comparison functions][Comparison functions:1]]
bool operator==(const SparseMatrixCOO& spmat2) const {

  SparseMatrixCOO * sp1, * sp2;
  SparseMatrixCOO sp_data1, sp_data2;

  // Copy if not sorted
  if(this->_sorted){
    sp1 = const_cast<SparseMatrixCOO*>(this);
  }
  else{
    sp_data1 = *this;
    sp1 = &sp_data1;
    sp1->order_indices();
  }

  if(spmat2._sorted){
    sp2 = const_cast<SparseMatrixCOO*>(&spmat2);
  }
  else{
    sp_data2 = spmat2;
    sp2 = &sp_data2;
    sp2->order_indices();
  }

  if(sp1->_m != sp2->_m) return false;
  if(sp1->_n != sp2->_n) return false;
  if(sp1->_nnz != sp2->_nnz) return false;

  if(sp1->_i != sp2->_i) return false;
  if(sp1->_j != sp2->_j) return false;
  if(sp1->_v != sp2->_v) return false;

  return true;
}

bool operator!=(const SparseMatrixCOO& spmat2) const { return !(*this == spmat2); }
// Comparison functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Comparison functions][Comparison functions:2]]
bool equals(const SparseMatrixCOO& spmat2){
  this->order_indices();
  return (*this == spmat2);
}

bool equals(SparseMatrixCOO& spmat2){
  this->order_indices();
  spmat2.order_indices();
  return (*this == spmat2);
}
// Comparison functions:2 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Diagonal extraction][Diagonal extraction:1]]
DiagonalMatrix<Scalar> diag() const {
  DiagonalMatrix<Scalar> diag(_m, _n);
  const Index size = static_cast<Index>(std::min(_m, _n));
  for(Size k = 0; k < _nnz; ++k){
    if(_i[k] == _j[k]){
      if(_i[k] < size && _j[k] < size){
        diag(_i[k]) = _v[k];
      }
    }
  }
  return diag;
}

Vector<Scalar> diag_vect() const {
  const Index size = static_cast<Index>(std::min(_m, _n));
  Vector<Scalar> diag(std::min(_m, _n));
  for(Size k = 0; k < _nnz; ++k){
    if(_i[k] == _j[k]){
      if(_i[k] < size && _j[k] < size){
        diag(_i[k]) = _v[k];
      }
    }
  }
  return diag;
}
// Diagonal extraction:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Re-indexing][Re-indexing:1]]
void reindex(const IndexArray<Index> new_indices, int new_m = -1){
  if(new_m != -1){
    _m = new_m;
    _n = new_m;
  }

  for(Size k = 0; k < _nnz; ++k){
    _i[k] = new_indices[_i[k]];
    _j[k] = new_indices[_j[k]];
  }
  correct_storage();
}

void reindex(const IndexArray<Index> new_indices, const IndexArray<Index> old_indices, int new_m = -1){
  if(new_m != -1){
    _m = new_m;
    _n = new_m;
  }

  for(Size k = 0; k < _nnz; ++k){
    for(Size k2 = 0; k2 < old_indices.size(); ++k2){
      if(_i[k] == old_indices[k2]){
        _i[k] = new_indices[k2];
        break;
      }
    }
  }

  for(Size k = 0; k < _nnz; ++k){
    for(Size k2 = 0; k2 < old_indices.size(); ++k2){
      if(_j[k] == old_indices[k2]){
        _j[k] = new_indices[k2];
        break;
      }
    }
  }
  correct_storage();
}

void correct_storage(){
  if(this->is_storage_lower()){
    for(Size k = 0; k < _nnz; ++k){
      if(_i[k] < _j[k]) std::swap(_i[k], _j[k]);
    }
    _sorted = false;
  }
  else if(this->is_storage_upper()){
    for(Size k = 0; k < _nnz; ++k){
      if(_i[k] > _j[k]) std::swap(_i[k], _j[k]);
    }
    _sorted = false;
  }
}
// Re-indexing:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Cast function][Cast function:1]]
template<MPH_Scalar OtherScalar>
SparseMatrixCOO<OtherScalar, Index> cast() const {
  SparseMatrixCOO<OtherScalar, Index> out(_m, _n, _nnz, _i, _j, IndexArray<OtherScalar>(_v));
  out.copy_properties(*this);
  return out;
}
// Cast function:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Fill matrix from half storage to full storage][Fill matrix from half storage to full storage:1]]
void fill_tri_to_full(){
  if(this->is_storage_full()){ return; }

  int new_nnz = 0;
  for(Size k = 0; k < _nnz; ++k){
    new_nnz++;
    if(_i[k] != _j[k]) new_nnz++;
  }

  IndexArray<int> ii(new_nnz), jj(new_nnz);
  IndexArray<Scalar> vv(new_nnz);

  int idx = 0;
  for(Size k = 0; k < _nnz; ++k){
    ii[idx] = _i[k];
    jj[idx] = _j[k];
    vv[idx] = _v[k];
    idx++;
    if(_i[k] != _j[k]){
      ii[idx] = _j[k];
      jj[idx] = _i[k];
      vv[idx] = _v[k];
      idx++;
    }
  }

  this->set_property(MatrixStorage::full);
  _nnz = new_nnz;
  _i = std::move(ii);
  _j = std::move(jj);
  _v = std::move(vv);
}
// Fill matrix from half storage to full storage:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Make storage triangular][Make storage triangular:1]]
void to_triangular(MatrixStorage storage = MatrixStorage::lower){
  MAPHYSPP_ASSERT(this->is_symmetric() || this->is_hermitian(),
                "SparseMatrixCOO: asking for half storage with non symmetric matrix");
  if(this->get_storage_type() == storage){ return; }
  if((this->get_storage_type() == MatrixStorage::lower && storage == MatrixStorage::upper)
     || (this->get_storage_type() == MatrixStorage::upper && storage == MatrixStorage::lower)){
     auto sym = this->get_symmetry();
     this->set_property(MatrixStorage::full, MatrixSymmetry::general);
     *this = this->t();
     this->set_property(sym, storage);
    return;
  }

  int new_nnz = 0;
  std::function<bool(Index,Index)> to_keep;
  if(storage == MatrixStorage::lower){
    to_keep = [](Index indi, Index indj){ return indi >= indj; };
  }
  else{
    to_keep = [](Index indi, Index indj){ return indi <= indj; };
  }

  for(Size k = 0; k < _nnz; ++k){
    if(to_keep(_i[k],  _j[k])) new_nnz++;
  }

  IndexArray<int> ii(new_nnz), jj(new_nnz);
  IndexArray<Scalar> vv(new_nnz);

  int idx = 0;
  for(Size k = 0; k < _nnz; ++k){
    if(to_keep(_i[k],  _j[k])){
      ii[idx] = _i[k];
      jj[idx] = _j[k];
      vv[idx] = _v[k];
      idx++;
    }
  }

  this->set_property(storage);
  _nnz = new_nnz;
  _i = std::move(ii);
  _j = std::move(jj);
  _v = std::move(vv);
}
// Make storage triangular:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Break symmetry][Break symmetry:1]]
void break_symmetry(){
  this->fill_tri_to_full();
  this->set_property(MatrixSymmetry::general);
}
// Break symmetry:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Element accessor][Element accessor:1]]
private:
bool _search_sorted_ijv(const Index i, const Index j, Index& value_idx) const {
  Index j_idx = _j.searchsorted(j);
  Index jp1_idx = _j.searchsorted(j+1);
  Index i_idx = _i.searchsorted(i, j_idx, jp1_idx);
  if(i_idx < static_cast<Index>(_nnz) && _j[i_idx] == j && _i[i_idx] == i){
    value_idx = i_idx;
    return true;
  }
  return false;
}

void coeff_swap(Index& i, Index& j) const {
  if((this->is_storage_lower() && (i < j)) ||
     (this->is_storage_upper() && (i > j))){
     std::swap(i, j);
  }
}

public:
// Element accessor (access is linear in nnz if not sorted !!!)
[[nodiscard]] Scalar coeff(Index i, Index j) const {
  coeff_swap(i, j);
  if(_sorted){
    Index v_idx;
    if(_search_sorted_ijv(i, j, v_idx)) return _v[v_idx];
  }
  else{
    for(Size k = 0; k < _nnz; ++k){
      if(_i[k] == i && _j[k] == j) return _v[k];
    }
  }
  return Scalar{0};
}

[[nodiscard]] Scalar coeff(Index i, Index j){
  coeff_swap(i, j);
  if(!_sorted) order_indices();
  Index v_idx;
  if(_search_sorted_ijv(i, j, v_idx)) return _v[v_idx];
  return Scalar{0};
}

// Element accessor (access is linear in nnz if not sorted !!!)
[[nodiscard]] const Scalar& coeffRef(Index i, Index j) const {
  coeff_swap(i, j);
  if(_sorted){
    Index v_idx;
    if(_search_sorted_ijv(i, j, v_idx)) return _v[v_idx];
    throw std::runtime_error("const SparseMatrixCOO(i,j) does not point to a non zero value");
  }
  else{
    for(Size k = 0; k < _nnz; ++k){
      if(_i[k] == i && _j[k] == j) return _v[k];
    }
    throw std::runtime_error("const SparseMatrixCOO(i,j) does not point to a non zero value");
  }
  return _v[0];
}

[[nodiscard]] Scalar& coeffRef(Index i, Index j) {
  coeff_swap(i, j);
  if(!_sorted) order_indices();
  Index v_idx;
  if(_search_sorted_ijv(i, j, v_idx)) return _v[v_idx];
  throw std::runtime_error("SparseMatrixCOO(i,j) does not point to a non zero value");
  return _v[0];
}

[[nodiscard]] const Scalar& operator()(Index i, Index j) const { return this->coeffRef(i, j); }
[[nodiscard]] Scalar& operator()(Index i, Index j) { return this->coeffRef(i, j); }
// Element accessor:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Memory cost][Memory cost:1]]
inline Size memcost(){
  constexpr Size elt_size = (2 * sizeof(Index) + sizeof(Scalar));
  return  elt_size * _nnz;
}
// Memory cost:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Begin and end][Begin and end:1]]
IJVIterator<Scalar, Index, SparseMatrixCOO<Scalar, Index>> begin() const{
  IJVIterator<Scalar, Index, SparseMatrixCOO<Scalar, Index>> ite(*this);
  return ite.begin();
}

IJVIterator<Scalar, Index, SparseMatrixCOO<Scalar, Index>> end() const{
  IJVIterator<Scalar, Index, SparseMatrixCOO<Scalar, Index>> ite(*this);    
  return ite.end();
}
// Begin and end:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Serialization][Serialization:1]]
std::vector<char> serialize() const {
  // M, N, values
  char prop = this->properties_serialize();
  int M = static_cast<int>(_m);
  int N = static_cast<int>(_n);
  int NNZ = static_cast<int>(_nnz);
  std::vector<char> buffer(1 + (3 + 2 * _nnz) * sizeof(int) + _nnz * sizeof(Scalar));
  int cnt = 0;

  std::memcpy(&buffer[cnt], &prop, sizeof(char)); cnt += sizeof(char);
  std::memcpy(&buffer[cnt], &M, sizeof(int)); cnt += sizeof(int);
  std::memcpy(&buffer[cnt], &N, sizeof(int)); cnt += sizeof(int);
  std::memcpy(&buffer[cnt], &NNZ, sizeof(int)); cnt += sizeof(int);
  std::memcpy(&buffer[cnt], &_i[0], _nnz * sizeof(int)); cnt += _nnz * sizeof(int);
  std::memcpy(&buffer[cnt], &_j[0], _nnz * sizeof(int)); cnt += _nnz * sizeof(int);
  std::memcpy(&buffer[cnt], &_v[0], _nnz * sizeof(Scalar));

  return buffer;
}

void deserialize(const std::vector<char>& buffer){
  int M, N, NNZ;
  int cnt = 0;
  char prop;
  std::memcpy(&prop, &buffer[cnt], sizeof(char)); cnt += sizeof(char);
  std::memcpy(&M, &buffer[cnt], sizeof(int)); cnt += sizeof(int);
  std::memcpy(&N, &buffer[cnt], sizeof(int)); cnt += sizeof(int);
  std::memcpy(&NNZ, &buffer[cnt], sizeof(int)); cnt += sizeof(int);
  this->properties_deserialize(prop);
  _m = static_cast<Size>(M);
  _n = static_cast<Size>(N);
  _nnz = static_cast<Size>(NNZ);
  _i = Idx_arr(_nnz);
  _j = Idx_arr(_nnz);
  _v = Scal_arr(_nnz);

  std::memcpy(&_i[0], &buffer[cnt], _nnz * sizeof(int)); cnt += _nnz * sizeof(int);
  std::memcpy(&_j[0], &buffer[cnt], _nnz * sizeof(int)); cnt += _nnz * sizeof(int);
  std::memcpy(&_v[0], &buffer[cnt], _nnz * sizeof(Scalar));

  _sorted = false;
}
// Serialization:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Operators][Operators:1]]
// Addition and substraction
SparseMatrixCOO& operator+= (const SparseMatrixCOO& other){
  MAPHYSPP_DIM_ASSERT( _m, n_rows(other), "SparseMatrixCOO += SparseMatrixCOO: different nb rows.");
  MAPHYSPP_DIM_ASSERT( _n, n_cols(other), "SparseMatrixCOO += SparseMatrixCOO: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(_m, _n);
  lilmat.copy_properties(*this);
  lilmat.insert(*this);
  lilmat.insert(other); // Duplicates are summed by default
  lilmat.drop(); // Drop zeros
  *this = std::move(lilmat.to_coo());
  return *this;
}

SparseMatrixCOO& operator-= (const SparseMatrixCOO& other){
  MAPHYSPP_DIM_ASSERT( _m, n_rows(other), "SparseMatrixCOO += SparseMatrixCOO: different nb rows.");
  MAPHYSPP_DIM_ASSERT( _n, n_cols(other), "SparseMatrixCOO += SparseMatrixCOO: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(_m, _n);
  lilmat.copy_properties(*this);
  lilmat.insert(other);
  lilmat *= Scalar{-1};
  lilmat.insert(*this);
  lilmat.drop(); // Drop zeros
  *this = std::move(lilmat.to_coo());
  return *this;
}

using SparseMatrixBase<Scalar, Index>::operator*=;

SparseMatrixCOO& operator*= (const SparseMatrixCOO& other){
  MAPHYSPP_DIM_ASSERT(_n, n_rows(other), "SparseMatrixCOO x SparseMatrixCOO");

  SparseMatrixLIL<Scalar> lilmat(_m, n_cols(other));

  auto insert_nnz = [&lilmat](const Index& i1, const Index& j1, const Index& i2, const Index& j2, const Scalar& v1, const Scalar& v2){
    if(j1 == i2){
      lilmat.insert(i1, j2, v1 * v2);
    }
  };

  const bool tri_storage_1 = !(this->is_storage_full());
  const bool tri_storage_2 = !(other.is_storage_full());

  for(Size k1 = 0; k1 < _nnz; ++k1){
    for(Size k2 = 0; k2 < other._nnz; ++k2){
      const Index& i1 = _i[k1];
      const Index& j1 = _j[k1];
      const Scalar& v1 = _v[k1];

      const Index& i2 = other._i[k2];
      const Index& j2 = other._j[k2];
      const Scalar& v2 = other._v[k2];

      // A(i1 j1) x B(i1 j2)
      insert_nnz(i1, j1, i2, j2, v1, v2);

      if(tri_storage_1 && (i1 != j1)){
        // A(j1 i1) x B(i1 j2)
        insert_nnz(j1, i1, i2, j2, v1, v2);

        if(tri_storage_2 && (j2 != i2)){
          // A(j1 i1) x B(j2 i2)
          insert_nnz(j1, i1, j2, i2, v1, v2);
        }
      }

      if(tri_storage_2 && (j2 != i2)){
        // A(i1 j1) x B(j2 i2)
        insert_nnz(i1, j1, j2, i2, v1, v2);
      }
    }
  }

  lilmat.drop();
  *this = std::move(lilmat.to_coo());

  return *this;
}
// Operators:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Operators with diagonal matrix][Operators with diagonal matrix:1]]
SparseMatrixCOO& operator+= (const DiagonalMatrix<Scalar>& other){
  MAPHYSPP_ASSERT( _m == n_rows(other), "SparseMatrixCOO += SparseMatrixCOO: different nb rows.");
  MAPHYSPP_ASSERT( _n == n_cols(other), "SparseMatrixCOO += SparseMatrixCOO: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(_m, _n);
  lilmat.copy_properties(*this);
  lilmat.insert(*this);
  lilmat.insert(other);
  lilmat.drop(); // Drop zeros
  (*this) = std::move(lilmat.to_coo());
  return *this;
}

SparseMatrixCOO& operator-= (const DiagonalMatrix<Scalar>& other){
  MAPHYSPP_ASSERT( _m == n_rows(other), "SparseMatrixCOO += SparseMatrixCOO: different nb rows.");
  MAPHYSPP_ASSERT( _n == n_cols(other), "SparseMatrixCOO += SparseMatrixCOO: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(_m, _n);
  lilmat.copy_properties(*this);
  lilmat.insert(other);
  lilmat *= Scalar{-1};
  lilmat.insert(*this);
  lilmat.drop(); // Drop zeros
  *this = std::move(lilmat.to_coo());
  return *this;
}

// A <- A * D
void right_diagmm(const DiagonalMatrix<Scalar>& other){
  Size M = _m;
  Size N = n_cols(other);
  MAPHYSPP_DIM_ASSERT( _n, n_rows(other), "SparseMatrixCOO *= DiagonalMatrix");

  this->break_symmetry();
  SparseMatrixLIL<Scalar> lilmat(M, N);
  lilmat.copy_properties(*this);
  const Scalar * diag = other.get_ptr();

  // Multiplication by diagonal on the right is scaling the columns
  const Index dim = static_cast<Index>(std::min(N, _n));
  for(Size k = 0; k < _nnz; ++k){
    if(_j[k] < dim){
      lilmat.insert(_i[k], _j[k], _v[k] * diag[_j[k]]);
    }
  }

  lilmat.drop();
  *this = std::move(lilmat.to_coo());
}

// A <- D * A
void left_diagmm(const DiagonalMatrix<Scalar>& other){
  Size M = n_rows(other);
  Size N = _n;
  MAPHYSPP_DIM_ASSERT( _m, n_cols(other), "SparseMatrixCOO *= DiagonalMatrix");

  this->break_symmetry();
  SparseMatrixLIL<Scalar> lilmat(M, N);
  lilmat.copy_properties(*this);
  const Scalar * diag = other.get_ptr();

  // Multiplication by diagonal on the right is scaling the rows
  const Index dim = static_cast<Index>(std::min(M, _m));
  for(Size k = 0; k < _nnz; ++k){
    if(_i[k] < dim){
      lilmat.insert(_i[k], _j[k], _v[k] * diag[_i[k]]);
    }
  }

  lilmat.drop();
  *this = std::move(lilmat.to_coo());
}

SparseMatrixCOO& operator*= (const DiagonalMatrix<Scalar>& other){
  this->right_diagmm(other);
  return *this;
}
// Operators with diagonal matrix:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Display function][Display function:1]]
// Pretty printing
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << name << '\n';
  out << "m: " << _m << " "
      << "n: " << _n << " "
      << "nnz: " << _nnz << '\n'
      << this->properties_str()
      << '\n' << '\n';

  out << "i\tj\tv" << '\n' << '\n';
  for(Size k = 0; k<_nnz; ++k){
    out << _i[k] << "\t" << _j[k] << "\t" << _v[k] << '\n';
  }
  out << '\n';
}
// Display function:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Transposition][Transposition:1]]
void transpose(){
  if(this->is_symmetric()) return;
  std::swap(_i, _j);
  std::swap(_m, _n);
  _sorted = false;
}

void conj_transpose(){
  if(this->is_hermitian()) return;
  if constexpr(is_real<Scalar>::value){
    this->transpose();
  }
  else{
    std::swap(_i, _j);
    std::swap(_m, _n);
    for(auto& val : _v) val = conj<Scalar>(val);
    _sorted = false;
  }
}

[[nodiscard]] SparseMatrixCOO t() const{
  auto copy = *this;
  copy.conj_transpose();
  return copy;
}
// Transposition:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Send and receive a matrix][Send and receive a matrix:1]]
// Send / Recv
#ifndef MAPHYSPP_NO_MPI
  // Sendbuf of size 3, reqs of size 4
  void isend(std::array<int, 3>& sendbuf, const int dest, const int matrix_tag,
	     std::array<MPI_Request, 4>& reqs,
	     const MPI_Comm comm = MPI_COMM_WORLD, const int n_matrices = 1) const {
    sendbuf[0] = _m;
    sendbuf[1] = _n;
    sendbuf[2] = _nnz;
    MMPI::isend<int>   (&sendbuf[0],    3, matrix_tag                                         , dest, reqs[0], comm);
    MMPI::isend<Index> (this->get_i_ptr(), static_cast<int>(_nnz), matrix_tag +     n_matrices, dest, reqs[1], comm);
    MMPI::isend<Index> (this->get_j_ptr(), static_cast<int>(_nnz), matrix_tag + 2 * n_matrices, dest, reqs[2], comm);
    MMPI::isend<Scalar>(this->get_v_ptr(), static_cast<int>(_nnz), matrix_tag + 3 * n_matrices, dest, reqs[3], comm);
  }

  void recv(int source, int matrix_tag, const MPI_Comm comm = MPI_COMM_WORLD, const int n_matrices = 1){
    int buf[3];
    MMPI::recv<int>(buf, 3, matrix_tag, source, comm);
    _m = buf[0];
    _n = buf[1];
    _nnz = buf[2];
    _i = IndexArray<Index>(_nnz);
    _j = IndexArray<Index>(_nnz);
    _v = IndexArray<Scalar>(_nnz);
    MMPI::recv<Index> (&_i[0], static_cast<int>(_nnz), matrix_tag +     n_matrices, source, comm);
    MMPI::recv<Index> (&_j[0], static_cast<int>(_nnz), matrix_tag + 2 * n_matrices, source, comm);
    MMPI::recv<Scalar>(&_v[0], static_cast<int>(_nnz), matrix_tag + 3 * n_matrices, source, comm);
  }

  // reqs of size 4
  void wait(std::array<MPI_Request, 4> reqs) const {
    MPI_Status stat[4];
    MMPI::waitall(4, &reqs[0], &stat[0]);
  }
#endif
}; // class SparseMatrixCOO
// Send and receive a matrix:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Out of class operators][Out of class operators:1]]
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator+ (SparseMatrixCOO<Scalar> sp_mat, const SparseMatrixCOO<Scalar>& other){
  sp_mat += other;
  return sp_mat;
}

template<class Scalar>
inline SparseMatrixCOO<Scalar> operator- (SparseMatrixCOO<Scalar> sp_mat, const SparseMatrixCOO<Scalar>& other){
  sp_mat -= other;
  return sp_mat;
}

// Scalar multiplication
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator* (SparseMatrixCOO<Scalar> sp_mat, const Scalar& scal){
  sp_mat *= scal;
  return sp_mat;
}

template<class Scalar>
inline SparseMatrixCOO<Scalar> operator* (Scalar scal, const SparseMatrixCOO<Scalar>& sp_mat){
  return sp_mat * scal;
}

template<class Scalar>
inline SparseMatrixCOO<Scalar> operator/ (SparseMatrixCOO<Scalar> sp_mat, const Scalar& scal){
  sp_mat /= scal;
  return sp_mat;
}

template<class Scalar>
inline SparseMatrixCOO<Scalar> operator* (SparseMatrixCOO<Scalar> sp_mat_l, const SparseMatrixCOO<Scalar>& sp_mat_r){
  sp_mat_l *= sp_mat_r;
  return sp_mat_l;
}
// Out of class operators:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Matrix inverse operator][Matrix inverse operator:1]]
template<MPH_Scalar Scalar, MPH_Integral Index>
auto operator~(const SparseMatrixCOO<Scalar, Index>& A){
  using solver = typename default_sparse_solver<SparseMatrixCOO<Scalar, Index>, Vector<Scalar>>::type;
  static_assert(!std::is_same_v<solver, Identity<SparseMatrixCOO<Scalar, Index>, Vector<Scalar>>>,
                "SparseMatrixCOO:operator~ Pastix or Mumps is required to solve sparse linear system");
  return solver(A);
}

template<MPH_Scalar Scalar, MPH_Integral Index>
Vector<Scalar> operator%(const SparseMatrixCOO<Scalar, Index>& A, const Vector<Scalar>& b){
  return ~A * b; 
}
// Matrix inverse operator:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Traits][Traits:1]]
template<typename Scalar, MPH_Integral Index> struct scalar_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = Scalar;
};

template<typename Scalar, MPH_Integral Index> struct vector_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = Vector<Scalar>;
};

template<typename Scalar, MPH_Integral Index> struct dense_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = DenseMatrix<Scalar>;
};

template<typename Scalar, MPH_Integral Index> struct diag_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = DiagonalMatrix<Scalar>;
};

template<typename Scalar, MPH_Integral Index> struct is_sparse<SparseMatrixCOO<Scalar, Index>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Interface functions][Interface functions:1]]
template<typename Scalar, MPH_Integral Index = int>
SparseMatrixCOO<Scalar, Index> adjoint(const SparseMatrixCOO<Scalar, Index>& mat){ return mat.t(); }

template<typename Scalar, MPH_Integral Index = int>
void build_matrix(SparseMatrixCOO<Scalar, Index>& mat, const int M, const int N, const int nnz, int * i, int * j, Scalar * v,  const bool fill_symmetry = false){
  mat = SparseMatrixCOO<Scalar, Index>(M, N, nnz, i, j, v);
  if(fill_symmetry){
    mat.fill_tri_to_full();
  }
}

template<MPH_Scalar Scalar, MPH_Integral Tint>
void reindex(SparseMatrixCOO<Scalar>& mat, const IndexArray<Tint> new_indices, int new_m = -1){
  mat.reindex(new_indices, new_m);
}
// Interface functions:1 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Iterator][Iterator:2]]
template<typename Scalar, typename Index>
struct IJVIterator<Scalar, Index, SparseMatrixCOO<Scalar, Index>>{
  using Triplet = typename std::tuple<Index, Index, Scalar>;
  Size cnt;
  const Index * i;
  const Index * j;
  const Scalar * v;
  Size nnz;

  IJVIterator(const SparseMatrixCOO<Scalar, Index>& coo):
    i{coo.get_i_ptr()}, j{coo.get_j_ptr()}, v{coo.get_v_ptr()}, nnz{coo.get_nnz()}
  {}

  IJVIterator& operator++(){
    cnt++;
    return *this;
  }

  Triplet operator*(){
    return std::make_tuple(i[cnt], j[cnt], v[cnt]);
  }

  IJVIterator& begin(){
    cnt = 0;
    return *this;
  }

  IJVIterator end(){
    IJVIterator end = *this;
    end.cnt = end.nnz;
    return end;
  }

  bool operator==(const IJVIterator& other) const {
    if(this->cnt != other.cnt) return false;
    if(this->i != other.i) return false;
    return true;
  }

  bool operator!=(const IJVIterator& other) const {
    return !(*this == other);
  }
};
// Iterator:2 ends here

// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Footer][Footer:1]]
}  // namespace maphys
// Footer:1 ends here
