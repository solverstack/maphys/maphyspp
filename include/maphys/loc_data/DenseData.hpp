// [[file:../../../org/maphys/loc_data/DenseData.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <algorithm>

namespace maphys {
  const int DynamicSize = -1;
  template<class Scalar, int NbCol = DynamicSize> class DenseData;
}

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/IndexArray.hpp"
#include "maphys/utils/Error.hpp"

// Try using tlapack if nothing else is found
#if ! defined(MAPHYSPP_USE_CHAMELEON)
#if ! defined(MAPHYSPP_USE_LAPACKPP)
#define MAPHYSPP_USE_TLAPACK
#endif
#endif

#if defined(MAPHYSPP_USE_CHAMELEON)

#include "maphys/kernel/ChameleonKernels.hpp"

#endif // MAPHYSPP_USE_CHAMELEON

#if defined(MAPHYSPP_USE_LAPACKPP)

#include "maphys/kernel/BlasKernels.hpp"

#endif // MAPHYSPP_USE_LAPACKPP

#if defined(MAPHYSPP_USE_TLAPACK)

namespace tlapack {
  template<typename Scalar, int NbCol>
  maphys::Size size(const maphys::DenseData<Scalar, NbCol>&);
  template<typename Scalar>
  maphys::DenseData<Scalar, 1> slice(const maphys::DenseData<Scalar, 1>&, std::pair<int, int>);

  template<typename Scalar, int NbCol>
  maphys::Size nrows(const maphys::DenseMatrix<Scalar, NbCol>& M);
  template<typename Scalar, int NbCol>
  maphys::Size ncols(const maphys::DenseMatrix<Scalar, NbCol>& M);
  template<typename Scalar, int NbCol>
  maphys::Size size(const maphys::DenseMatrix<Scalar, NbCol>& M);
  template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
  slice(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> fidx, std::pair<int, int> lidx);
  template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
  rows(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> ridx);
  template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
  cols(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> cidx);
  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  row(const maphys::DenseMatrix<Scalar, NbCol>& M, int i);
  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  col(const maphys::DenseMatrix<Scalar, NbCol>& M, int j);
  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  slice(const maphys::DenseMatrix<Scalar, NbCol>& M, int i, std::pair<int, int> lidx);
  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  slice(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> lidx, int j);
  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  diag(const maphys::DenseMatrix<Scalar, NbCol>& M);
  template<typename Scalar, int NbCol> maphys::Vector<Scalar>
  diag(const maphys::DenseMatrix<Scalar, NbCol>& M, int i);
  template<typename Scalar> maphys::Vector<Scalar>
  slice(const maphys::DenseMatrix<Scalar, 1>& M, std::pair<int, int> idxs);
} // namespace tlapack

#include "maphys/kernel/TlapackKernels.hpp"

#endif // MAPHYSPP_USE_TLAPACK

#if defined(MAPHYSPP_USE_CHAMELEON)
using COMPOSE_BLAS = maphys::chameleon_kernels;
#else
#if defined(MAPHYSPP_USE_LAPACKPP)
using COMPOSE_BLAS = maphys::blas_kernels;
#else
using COMPOSE_BLAS = maphys::tlapack_kernels;
#endif // MAPHYSPP_USE_LAPACKPP
#endif // MAPHYSPP_USE_CHAMELEON

#include "maphys/loc_data/DenseMatrix.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Attributes][Attributes:1]]
template<class Scalar, int NbCol>
class DenseData {
public:
  using scalar_type = Scalar;
  using value_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;

private:
  bool _trans = false;

  using Real = real_type;
  using DataArray = std::vector<Scalar>;

  Size _m{0};
  Size _n{0};
  Size _ld{1};
  Size _inc{1};
  bool _is_view = false;
  DataArray _data;
  Scalar * _ptr = nullptr;
// Attributes:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Constructors][Constructors:1]]
public:

// Constructors without weak pointer
explicit DenseData(const Size m, const Size n = 1):
  _m{m},
  _n{n},
  _ld{m},
  _inc{1},
  _is_view{false}
{
  if constexpr(NbCol >= 0){
    if(n > 1){
      MAPHYSPP_DIM_ASSERT(n, NbCol, "Error: creating matrix with N != NbCol staticly specified");
    }
    _n = NbCol;
  }
  _data = DataArray(_m * _n, Scalar{0});
  _ptr = &_data[0];
}

explicit DenseData(const std::initializer_list<Scalar>& l, const Size m = 0, const Size n = 1):
  _m{m},
  _n{n},
  _ld{m},
  _inc{1},
  _is_view{false}
{
  if constexpr(NbCol >= 0){ _n = NbCol; }
  if(_m == 0) _m = l.size() / _n;
  _ld = _m;
  MAPHYSPP_ASSERT(_m * _n == l.size(), "DenseData::DenseData({}) List size different from m * n");
  _data = DataArray(l);
  _ptr = &_data[0];
}

explicit DenseData(): DenseData(0,Size{0}) {}

// Constructors with weak pointer
explicit DenseData(const Size m, Scalar * ptr, Size ld = 0, Size inc = 1):
  _m{m},
  _n{static_cast<Size>(NbCol)},
  _ld{ld},
  _inc{inc},
  _is_view{true},
  _ptr{ptr}
{
  if(_ld == 0){
    _ld = _m;
  }
  static_assert(NbCol != DynamicSize, "Error: while creating matrix, number of columns must be specified");
  MAPHYSPP_ASSERT(_ld >= _m, "DenseData: leading dimension smaller that row dimension.");
}

explicit DenseData(const Size m, const Size n, Scalar * ptr, Size ld = 0, Size inc = 1):
  _m{m},
  _n{n},
  _ld{ld},
  _inc{inc},
  _is_view{true},
  _ptr{ptr}
{
  if(_ld == 0){
    _ld = _m;
  }

  if constexpr (NbCol != DynamicSize){
    MAPHYSPP_DIM_ASSERT(n, NbCol, "Error: creating matrix with N != NbCol staticly specified");
  }
  MAPHYSPP_ASSERT(_ld >= _m, "DenseData: leading dimension smaller that row dimension.");
}

private:
void _set_ptr(Scalar * other_ptr){
  if(_is_view){
    _ptr = other_ptr;
  }
  else{
    _ptr = &_data[0];
  }
}

public:

// Copy constructor
DenseData(const DenseData& d):
  _m{d._m},
  _n{d._n},
  _ld{d._ld},
  _inc{d._inc},
  _is_view{d._is_view},
  _data{d._data}
{
  _set_ptr(d._ptr);
}

// Move constructor
DenseData(DenseData&& d):
  _m{std::exchange(d._m, 0)},
  _n{std::exchange(d._n, 0)},
  _ld{std::exchange(d._ld, 1)},
  _inc{std::exchange(d._inc, 1)},
  _is_view{std::exchange(d._is_view, false)},
  _data{std::move(d._data)}
{
  _set_ptr(d._ptr);
}

// Copy assignment
DenseData& operator=(const DenseData& copy){
  if(copy.get_n_rows() == _m && copy.get_n_cols() == _n){
    copy_values(copy);
  }
  else if(copy.is_view()){
    make_shallowcopy_of(copy);
  }
  else{
    make_deepcopy_of(copy);
  }
  return *this;
}

DenseData& operator=(DenseData&& copy){
  if(copy.get_n_rows() == _m && copy.get_n_cols() == _n){
    copy_values(copy);
  }
  else if(copy.is_view()){
    make_shallowcopy_of(copy);
  }
  else{
    _m = copy.get_n_rows();
    _n = copy.get_n_cols();
    _ld = copy.get_leading_dim();
    _inc = copy.get_increment();
    _is_view = false;
    _data = std::move(copy._data);
    _ptr =  &_data[0];
  }
  copy.make_deepcopy_of(DenseData());
  return *this;
}

template<int OtherNbCol>
DenseData& operator=(const DenseData<Scalar, OtherNbCol>& copy){
  static_assert(NbCol == DynamicSize || OtherNbCol == DynamicSize || NbCol ==  OtherNbCol, "DenseData copy: incompatible static NbCol");
  if(copy.is_view()){
    make_shallowcopy_of(copy);
  }
  else{
    make_deepcopy_of(copy);
  }
  return *this;
}

// Move assignment
template<int OtherNbCol>
DenseData& operator=(DenseData<Scalar, OtherNbCol>&& copy){
  static_assert(NbCol == DynamicSize || OtherNbCol == DynamicSize || NbCol ==  OtherNbCol, "DenseData copy: incompatible static NbCol");
  if(copy.is_view()){
    make_shallowcopy_of(copy);
  }
  else{
    _m = copy.get_n_rows();
    _n = copy.get_n_cols();
    _ld = copy.get_leading_dim();
    _inc = copy.get_increment();	 
    _is_view = false;
    _data = copy.move_data_array();
    _ptr = &_data[0];
  }
  copy.make_deepcopy_of(DenseData<Scalar, OtherNbCol>());
  return *this;
}
// Constructors:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Copy][Copy:1]]
// Copy values: dimensions must be identical (no check)
template<int OtherNbCol>
void copy_values(const DenseData<Scalar, OtherNbCol>& other){
  if(_n == 1 and (_inc != 1 or other.get_increment() != 1)){
    for(Size i = 0; i < _m; ++i){ (*this)[i] = other[i]; }
  } else {
    if((other.get_leading_dim() == other.get_n_rows()) && (this->get_leading_dim() == this->get_n_rows())){
      std::memcpy(_ptr, other.get_ptr(), _n * _m * sizeof(Scalar));
    }
    else{
      for(Size j = 0; j < _n; ++j){
	std::memcpy(this->get_vect_ptr(j), other.get_vect_ptr(j), _m * sizeof(Scalar));
      }
    }
  }
}

template<int OtherNbCol>
void make_deepcopy_of(const DenseData<Scalar, OtherNbCol>& other){
  if(_is_view == true || _m != other.get_n_rows() || _n != other.get_n_cols()){
    _data = DataArray(other.get_n_rows() * other.get_n_cols());
  }
  _m = other.get_n_rows();
  _n = other.get_n_cols();
  _ld = _m;
  _inc = 1;
  _is_view = false;
  _ptr = &_data[0];
  this->copy_values(other);
}

template<int OtherNbCol>
void make_shallowcopy_of(const DenseData<Scalar, OtherNbCol>& other){
  _m = other.get_n_rows();
  _n = other.get_n_cols();
  _ld = other.get_leading_dim();
  _inc = other.get_increment();
  _is_view = true;
  _data = DataArray();
  _ptr = const_cast<Scalar *>(other.get_ptr());
}

DenseData deepcopy() const {
  DenseData d;
  d.make_deepcopy_of(*this);
  return d;
}

DenseData shallowcopy() const {
  DenseData d;
  d.make_shallowcopy_of(*this);
  return d;
}
// Copy:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Comparison][Comparison:1]]
template<int OtherNbCol>
bool operator==(const DenseData<Scalar, OtherNbCol>& other) const {
  if(get_n_rows() != other.get_n_rows()) return false;
  if(get_n_cols() != other.get_n_cols()) return false;
  for(Size j = 0; j < get_n_cols(); ++j){
    for(Size i = 0; i < get_n_rows(); ++i){
      if((*this)(i, j) != other(i, j)) return false;
    }
  }
  return true;
}

template<int OtherNbCol>
bool operator!=(const DenseData<Scalar, OtherNbCol>& other) const {
  return !(*this == other);
}
// Comparison:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Reshape][Reshape:1]]
void reshape(Size new_m, Size new_n){
  MAPHYSPP_ASSERT(NbCol == DynamicSize || NbCol == static_cast<int>(new_n), "DenseData:reshape cannot be called with static NbCol != new_n");
  MAPHYSPP_ASSERT(!_is_view, "DenseData:reshape cannot be called weak pointer");
  MAPHYSPP_DIM_ASSERT(new_m * new_n, _m * _n, "DenseData:reshape (new nb of elts) =! (old nb of elts)");
  _m = new_m;
  _n = new_n;
  _ld = _m;
  _inc = 1;
}
// Reshape:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Scalar multiplication][Scalar multiplication:1]]
DenseData& operator*=(Scalar scal){
  for(Size j = 0; j < _n; ++j){
    auto xj = this->get_vect_view(j);
    COMPOSE_BLAS::scal(xj, scal);
  }
  return *this;
}
// Scalar multiplication:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Addition][Addition:1]]
void add(const DenseData& other, Scalar alpha = Scalar{1.0}){
  for(Size j = 0; j < _n; ++j){
    auto xj = other.get_vect_view(j);
    auto yj = this->get_vect_view(j);
    COMPOSE_BLAS::axpy(xj, yj, alpha);
  }
}

DenseData& operator+=(const DenseData& other){
  this->add(other);
  return *this;
}

DenseData& operator-=(const DenseData& other){
  this->add(other, Scalar{-1.0});
  return *this;
}
// Addition:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Dot][Dot:1]]
Scalar dot(const DenseData& other) const {
  return COMPOSE_BLAS::dot(*this, other);
}
// Dot:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Norm-2 and norm-2 squared][Norm-2 and norm-2 squared:1]]
Real squared() const {
  return COMPOSE_BLAS::norm2_squared(*this);
}

Real norm() const {
  return COMPOSE_BLAS::norm2(*this);
}
// Norm-2 and norm-2 squared:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Getters][Getters:1]]
[[nodiscard]] inline Size get_n_rows() const { return _trans ? _n : _m; }
[[nodiscard]] inline Size get_n_cols() const { return _trans ? _m : _n; }
[[nodiscard]] inline Size get_leading_dim() const { return _ld; }
[[nodiscard]] inline Size get_increment() const { return _inc; }
[[nodiscard]] inline const Scalar* get_ptr() const { return _ptr; }
[[nodiscard]] inline Scalar* get_ptr() { return _ptr; }
[[nodiscard]] inline bool is_view() const { return _is_view; }
[[nodiscard]] inline const DataArray get_data_array() const { return _data; }
[[nodiscard]] inline DataArray&& move_data_array() { return std::move(_data); }
[[nodiscard]] inline bool is_transpose() { return _trans; }
// Getters:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Accessors][Accessors:1]]
private:
inline int _index(const int i, const int j) const {
  if constexpr(NbCol == 1){
    (void) j;
    return i * _inc;
  } else {
    return _trans ? (i * _ld + j) : (j * _ld + i);
  }
}
// Accessors:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Accessors][Accessors:2]]
public:
[[nodiscard]] inline const Scalar& operator()(const int i, const int j) const {
  return _ptr[_index(i, j)];
}

[[nodiscard]] inline Scalar& operator()(const int i, const int j) {
  return _ptr[_index(i, j)];
}
// Accessors:2 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Accessors][Accessors:3]]
private:
[[nodiscard]] inline auto& _at(const int i, const int j){
  try {
    MAPHYSPP_ASSERT_POSITIVE(i, "DenseData.at(i, j) with i < 0" );
    MAPHYSPP_ASSERT_POSITIVE(j, "DenseData.at(i, j) with j < 0" );
    MAPHYSPP_ASSERT(i < static_cast<int>(get_n_rows()), "DenseData.at(i, j) with i >= nb rows");
    MAPHYSPP_ASSERT(j < static_cast<int>(get_n_cols()), "DenseData.at(i, j) with j >= nb cols");
  }
  catch(const std::runtime_error& e) {
    std::cerr << e.what();
    std::cerr << "(i, j), (nrows, ncols): " << '(' << i << ',' << j << "), (" << get_n_rows() << ',' << get_n_cols() << ")\n";
    MAPHYSPP_ASSERT(false, "Fatal error DenseData.at(i, j) index out of range");
  }
  return (*this)(i, j);
}

public:
[[nodiscard]] inline const Scalar& at(const int i, const int j) const { return this->_at(i, j); }
[[nodiscard]] inline Scalar& at(const int i, const int j) { return this->_at(i, j); }
// Accessors:3 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Accessors][Accessors:4]]
template<MPH_Integral Tint> [[nodiscard]] inline const Scalar& operator()(const Tint i) const { return _ptr[i * _inc]; }
template<MPH_Integral Tint> [[nodiscard]] inline Scalar& operator()(const Tint i) { return _ptr[i * _inc]; }
template<MPH_Integral Tint> [[nodiscard]] inline const Scalar& operator[](const Tint i) const { return _ptr[i * _inc]; }
template<MPH_Integral Tint> [[nodiscard]] inline Scalar& operator[](const Tint i) { return _ptr[i * _inc]; }

private:
template<MPH_Integral Tint> [[nodiscard]] inline auto& _at_vec(const Tint i){
  MAPHYSPP_ASSERT(get_n_cols() == 1, "Using DenseData.at(i) is not allowed if data is not a vector");
  bool in_range = true;
  if(i < 0) { std::cerr << "(i < 0): i = " << i << '\n'; in_range = false; }
  if(i >= static_cast<Tint>(get_n_rows())) { std::cerr << "(i >= M): i = " << i << " ; M = " << _m << '\n'; in_range = false; }
  MAPHYSPP_ASSERT(in_range, "DenseData.at(i) out of range M");
  return _ptr[i * _inc];
}

public:
template<MPH_Integral Tint> [[nodiscard]] inline const Scalar& at(const Tint i) const { return this->_at_vec(i); }
template<MPH_Integral Tint> [[nodiscard]] inline Scalar& at(const Tint i) { return this->_at_vec(i); }
// Accessors:4 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Accessors][Accessors:5]]
template<MPH_Integral Tint>
[[nodiscard]] inline Scalar * get_vect_ptr(Tint j = 0) { return &(*this)(0, static_cast<Tint>(j)); }
template<MPH_Integral Tint>
[[nodiscard]] inline const Scalar * get_vect_ptr(Tint j = 0) const { return &(*this)(0, static_cast<Tint>(j)); }

template<MPH_Integral Tint>
[[nodiscard]] inline DenseData<Scalar, 1> get_vect_view(Tint j = 0) {
  return DenseData<Scalar, 1>(this->_m, this->get_vect_ptr(j), 0, this->get_increment());
}
template<MPH_Integral Tint>
[[nodiscard]] inline const DenseData<Scalar, 1> get_vect_view(Tint j = 0) const {
  return DenseData<Scalar, 1>(this->_m, const_cast<Scalar*>(this->get_vect_ptr(j)), 0, this->get_increment());
}
// Accessors:5 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Transposition][Transposition:1]]
void transpose(){ _trans = !_trans; }
// Transposition:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Display][Display:1]]
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << name << '\n';
  out << "n_rows: " << get_n_rows() << " "
      << "n_cols: " << get_n_cols() << " ";
  if(_trans) out << " (transposed) ";
  out << "ld: " << _ld << "\n\n";
  out << "inc: " << _inc << "\n\n";       

  if(_n == 1 and _inc != 1){
    for(Size i = 0; i < get_n_rows(); ++i){
      out << (*this)[i] << '\n';
    }
  } else {

    for(Size i = 0; i < get_n_rows(); ++i){
      for(Size j = 0; j < get_n_cols(); ++j){
	out << (*this)(i, j) << '\t';
      }
      out << '\n';
    }

  }
  out << '\n';
}
// Display:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Interface functions][Interface functions:1]]
}; //class DenseData

template<MPH_Scalar Scalar, int NbCol>
Size size(const DenseData<Scalar, NbCol>& d){ return d.get_n_rows(); }

template<MPH_Scalar Scalar, int NbCol>
Size get_leading_dim(const DenseData<Scalar, NbCol>& d){ return d.get_leading_dim(); }

template<MPH_Scalar Scalar, int NbCol>
Size get_increment(const DenseData<Scalar, NbCol>& d){ return d.get_increment(); }

template<MPH_Scalar Scalar, int NbCol>
Scalar * get_ptr(DenseData<Scalar, NbCol>& d){ return d.get_ptr(); }

template<MPH_Scalar Scalar, int NbCol>
const Scalar * get_ptr(const DenseData<Scalar, NbCol>& d){ return d.get_ptr(); }
// Interface functions:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here

// [[file:../../../org/maphys/loc_data/DenseData.org::*Tlapack interface][Tlapack interface:1]]
#ifdef MAPHYSPP_USE_TLAPACK

namespace tlapack {
  template<typename Scalar, int NbCol>
  maphys::Size size(const maphys::DenseData<Scalar, NbCol>& d){ return d.get_n_rows(); }

  template<typename Scalar> maphys::DenseData<Scalar, 1>
  slice(const maphys::DenseData<Scalar, 1>& d, std::pair<int, int> idxs){
    return DenseData<Scalar, 1>(idxs.second - idxs.first, &d[idxs.first], 0, d.get_increment());
  }
} // namespace tlapack
#endif
// Tlapack interface:1 ends here
