// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Header][Header:1]]
#pragma once

#include "maphys/utils/SZ_compressor.hpp"
#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/loc_data/DiagonalMatrix.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Attributes][Attributes:1]]
template<MPH_Scalar Scalar>
class CompressedBasis{
public:
  using local_operator = SZ_compressor<Scalar>;
  using local_type = SZ_compressor<Scalar>;
  using matrix_type = SZ_compressor<Scalar>;
  using vector_type = DenseMatrix<Scalar, 1>;
  using scalar_type = Scalar;

private:
  using CprVect = std::vector<SZ_compressor<Scalar>>;
  std::shared_ptr<CprVect> _vectors;
  Size _n;
  double _zeta;

  // When taking a view to the data, first element may be shifted
  Size _first_elt = 0;
// Attributes:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Constructors][Constructors:1]]
public:
explicit CompressedBasis(): _n{0}, _zeta{0} {}

template<int NbCol>
explicit CompressedBasis(const DenseMatrix<Scalar, NbCol>& mat, double zeta): _n{n_cols(mat)}, _zeta{zeta}
{
  _vectors = std::make_shared<CprVect>(n_cols(mat));
  for(Size j = 0; j < _n; ++j){
    Scalar * mat_ptr = const_cast<Scalar *>(mat.get_vect_ptr(j));
    _vectors->at(j) = SZ_compressor<Scalar>(mat_ptr, n_rows(mat), _zeta);
  }
  _first_elt = 0;
}

explicit CompressedBasis(Size n, double zeta): _n{n}, _zeta{zeta}
{
  _vectors = std::make_shared<CprVect>(n);
  _first_elt = 0;
}

template<MPH_Integral Tint>
explicit CompressedBasis(CompressedBasis<Scalar>& other, Tint j_beg, Tint j_end)
{
  MAPHYSPP_ASSERT(j_end >= j_beg, "CompressedBasis: index error makeing view");
  _n = j_end - j_beg;
  _first_elt = other._first_elt + j_beg;
  _vectors = other._vectors;
}
// Constructors:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Access compressed vectors][Access compressed vectors:1]]
private:
  template<MPH_Integral Tint>
  SZ_compressor<Scalar>& _cpr_vector(Tint j){
    std::vector<SZ_compressor<Scalar>>& vects = *(_vectors.get());
    return vects.at(_first_elt + j);
  }

  template<MPH_Integral Tint>
  const SZ_compressor<Scalar>& _cpr_vector(Tint j) const {
    const std::vector<SZ_compressor<Scalar>>& vects = *(_vectors.get());
    return vects.at(_first_elt + j);
  }

public:
// Access compressed vectors:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Decompress vectors][Decompress vectors:1]]
template<MPH_Integral Tint>
[[nodiscard]] Vector<Scalar> get_vect(Tint j = 0) const {
  const SZ_compressor<Scalar>& compressed_V = _cpr_vector(j);
  Vector<Scalar> v_out(compressed_V.get_n_elts());
  compressed_V.decompress(get_ptr(v_out));
  return v_out;
}

template<MPH_Integral Tint>
[[nodiscard]] Vector<Scalar> get_vect_view(Tint j = 0) const {
  return get_vect(j);
}

template<MPH_Integral Tint>
[[nodiscard]] friend Vector<Scalar> get_vect_view(const CompressedBasis<Scalar>& b, Tint j = 0){ return b.get_vect_view(j); }

template<MPH_Integral Tint>
[[nodiscard]] DenseMatrix<Scalar, -1> get_block_copy(Tint i, Tint j, Size m, Size n) const {
  DenseMatrix<Scalar> m_out(m, n);
  for(Tint k = j; k < j + n; ++k){
    SZ_compressor<Scalar>& cpr_v = _cpr_vector(k);
    if(cpr_v.get_n_elts() == m){
      cpr_v.decompress(m_out.get_vect_ptr(j));
    }
    else{
      std::vector<Scalar> dcp_v = cpr_v.decompress();
      std::memcpy(m_out.get_ptr(i, j), &dcp_v[0], m);
    }
  }

  return m_out;
}

template<MPH_Integral Tint>
[[nodiscard]] DenseMatrix<Scalar, -1> get_block_view(Tint i, Tint j, Size m, Size n) const {
  return get_block_copy(i, j, m, n);
}
// Decompress vectors:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Compress vector][Compress vector:1]]
template<MPH_Integral Tint>
void set_vector(const Vector<Scalar>& v, Tint j){
  _cpr_vector(j) = SZ_compressor<Scalar>(const_cast<Scalar *>(get_ptr(v)), n_rows(v), _zeta);
}
// Compress vector:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Get a view subset of consecutive vectors][Get a view subset of consecutive vectors:1]]
template<MPH_Integral Tint>
[[nodiscard]] CompressedBasis<Scalar> get_columns_view(Tint j_beg, Tint j_end){
  return CompressedBasis<Scalar>(*this, j_beg, j_end);
}

template<MPH_Integral Tint>
[[nodiscard]] friend CompressedBasis<Scalar> get_columns_view(CompressedBasis<Scalar>& b, Tint j_beg, Tint j_end){
  return b.get_columns_view(j_beg, j_end);
}
// Get a view subset of consecutive vectors:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Matrix vector product][Matrix vector product:1]]
[[nodiscard]] Vector<Scalar> mat_vect(const Vector<Scalar> v_in) const {
  if(_n == 0) return Vector<Scalar>();

  Size M = n_rows(get_vect(0));
  Vector<Scalar> v_out(M);

  for(Size j = 0; j < _n; ++j){
    Vector<Scalar> vj = get_vect(j);
    for(Size i = 0; i < M; ++i){
      v_out[i] += vj[i] * v_in[j];
    }
  }

  return v_out;
}
// Matrix vector product:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Getters][Getters:1]]
template<MPH_Integral Tint>
[[nodiscard]] size_t get_compressed_size(Tint j) const { return _cpr_vector(j).get_compressed_size(); }

[[nodiscard]] Size get_n_rows() const {
  if(_n == 0) return 0;
  return _cpr_vector(0).get_n_elts();
}
[[nodiscard]] Size get_n_cols() const { return _n; }
// Getters:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Getters][Getters:2]]
}; // class CompressedBasis
// Getters:2 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Getters][Getters:3]]
template<MPH_Scalar Scalar>
[[nodiscard]] Size n_rows(const CompressedBasis<Scalar>& basis){ return basis.get_n_rows(); }
template<MPH_Scalar Scalar>
[[nodiscard]] Size n_cols(const CompressedBasis<Scalar>& basis){ return basis.get_n_cols(); }
// Getters:3 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Operators][Operators:1]]
template<MPH_Scalar Scalar>
Vector<Scalar> operator*(const CompressedBasis<Scalar>& basis, const Vector<Scalar>& RHS){
  return basis.mat_vect(RHS);
}
// Operators:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Traits][Traits:1]]
template<MPH_Scalar Scalar> struct vector_type<CompressedBasis<Scalar>> : public std::true_type {
  using type = typename CompressedBasis<Scalar>::vector_type;
};

template<MPH_Scalar Scalar> struct sparse_type<CompressedBasis<Scalar>> : public std::true_type {
  using type = SparseMatrixCSC<Scalar>;
};

template<MPH_Scalar Scalar> struct dense_type<CompressedBasis<Scalar>> : public std::true_type {
  using type = DenseMatrix<Scalar>;
};

template<MPH_Scalar Scalar> struct diag_type<CompressedBasis<Scalar>> : public std::true_type {
  using type = DiagonalMatrix<Scalar>;
};

template<MPH_Scalar Scalar> struct scalar_type<CompressedBasis<Scalar>> : public std::true_type {
  using type = Scalar;
};
// Traits:1 ends here

// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
