// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:1]]
#pragma once

#include <complex>
#include <type_traits>

namespace maphys {
// Arithmetic:1 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:2]]
template<class S> struct is_complex : public std::false_type {};
template<> struct is_complex<std::complex<float> >  : public std::true_type {};
template<> struct is_complex<std::complex<double> > : public std::true_type {};

template<class S> struct is_real  : public std::false_type {};
template<> struct is_real<float>  : public std::true_type {};
template<> struct is_real<double> : public std::true_type {};
// Arithmetic:2 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:3]]
template<class S>
struct arithmetic_name : public std::false_type {};
template<> struct arithmetic_name<float> : public std::true_type { static const constexpr char *name = "float"; };
template<> struct arithmetic_name<double> : public std::true_type { static const constexpr char *name = "double"; };
template<> struct arithmetic_name<std::complex<float>> : public std::true_type { static const constexpr char *name = "complex_float"; };
template<> struct arithmetic_name<std::complex<double>> : public std::true_type { static const constexpr char *name = "complex_double"; };
// Arithmetic:3 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:4]]
template<class S>
struct arithmetic_real : public std::false_type {};
template<> struct arithmetic_real<float> : public std::true_type { using type = float; };
template<> struct arithmetic_real<double> : public std::true_type { using type = double; };
template<> struct arithmetic_real<std::complex<float>> : public std::true_type { using type = float; };
template<> struct arithmetic_real<std::complex<double>> : public std::true_type { using type = double; };
// Arithmetic:4 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:5]]
template<class S>
struct arithmetic_complex : public std::false_type {};
template<> struct arithmetic_complex<float> : public std::true_type { using type = std::complex<float>; };
template<> struct arithmetic_complex<double> : public std::true_type { using type = std::complex<double>; };
template<> struct arithmetic_complex<std::complex<float>> : public std::true_type { using type = std::complex<float>; };
template<> struct arithmetic_complex<std::complex<double>> : public std::true_type { using type = std::complex<double>; };
// Arithmetic:5 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:6]]
template<class S>
struct arithmetic_tolerance : public std::false_type {};
template<> struct arithmetic_tolerance<float> : public std::true_type { static const constexpr float value = 5e-6f; };
template<> struct arithmetic_tolerance<double> : public std::true_type { static const constexpr double value = 1e-14; };
template<> struct arithmetic_tolerance<std::complex<float>> : public std::true_type { static const constexpr float value = 5e-6f; };
template<> struct arithmetic_tolerance<std::complex<double>> : public std::true_type { static const constexpr double value = 1e-14; };
// Arithmetic:6 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:7]]
template<class S> struct is_precision_double  : public std::false_type {};
template<> struct is_precision_double<double>  : public std::true_type {};
template<> struct is_precision_double<std::complex<double>> : public std::true_type {};
// Arithmetic:7 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:8]]
template<class Scalar>
[[nodiscard]] inline Scalar conj(const Scalar& s){
  if constexpr(is_complex<Scalar>::value){
    return std::conj(s);
  }
  else{
    return s;
  }
}
// Arithmetic:8 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:9]]
template<class Scalar>
[[nodiscard]] inline bool is_normal_or_zero(const Scalar& s){
  if constexpr(is_complex<Scalar>::value){
    if(s == Scalar{0, 0}) return true;
    return (is_normal_or_zero(std::real(s)) and is_normal_or_zero(std::imag(s)));
  }
  else{
    return (std::isnormal(s) or (s == Scalar{0}));
  }
}

template<class Scalar>
[[nodiscard]] inline bool is_normal(const Scalar& s){
  if constexpr(is_complex<Scalar>::value){
    if(s == Scalar{0, 0}) return false;
    return (is_normal_or_zero(std::real(s)) and is_normal_or_zero(std::imag(s)));
  }
  else{
    return std::isnan(s);
  }
}
// Arithmetic:9 ends here

// [[file:../../../org/maphys/utils/Arithmetic.org::*Arithmetic][Arithmetic:10]]
} // end namespace maphys
// Arithmetic:10 ends here
