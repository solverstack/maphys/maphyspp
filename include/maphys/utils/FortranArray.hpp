// [[file:../../../org/maphys/utils/FortranArray.org::*Header][Header:1]]
#pragma once

#include <vector>
#include <stdexcept>
#include "Error.hpp"
#include "maphys/interfaces/basic_concepts.hpp"
#include "maphys/interfaces/Common.hpp"
// Header:1 ends here

// [[file:../../../org/maphys/utils/FortranArray.org::*Attributes][Attributes:1]]
namespace maphys {
template<typename T>
class FortranArray{
private:
  Size _size;
  T * _data;
// Attributes:1 ends here

// [[file:../../../org/maphys/utils/FortranArray.org::*Constructors][Constructors:1]]
public:
  FortranArray(): _size{0}, _data{nullptr} {}
  FortranArray(Size s, T * d): _size{s}, _data{d} {}

  void set_data(Size s, T * d){
    _size = s;
    _data = d;
  }

  // No copy because using weak pointer
  FortranArray(const FortranArray&) = delete;
  FortranArray& operator=(const FortranArray& other) = delete;

  // Move allowed to take the pointer
  FortranArray(FortranArray&& other){
    _size = std::exchange(other._size, 0);
    _data = std::exchange(other._data, nullptr);
  }
  FortranArray& operator=(FortranArray&& other){
    if(&other == this) return *this;
    _size = std::exchange(other._size, 0);
    _data = std::exchange(other._data, nullptr);
    return *this;
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/utils/FortranArray.org::*Indexing][Indexing:1]]
// [] operator, no boundary checking
template<MPH_Integral Tint>
T& operator[](Tint idx){
  return _data[idx-1];
}
template<MPH_Integral Tint>
const T& operator[](Tint idx) const {
  return _data[idx-1];
}
// Indexing:1 ends here

// [[file:../../../org/maphys/utils/FortranArray.org::*Indexing][Indexing:2]]
// () operator, with boundary checking
template<MPH_Integral Tint>
T& operator()(Tint idx){
  if(idx <= Tint{0}){
    throw(std::out_of_range("Fortran array with index <= 0"));
  }
  if(idx > static_cast<Tint>(_size)){
    throw(std::out_of_range("Fortran array with index > size"));
  }
  return _data[idx-1];
}
template<MPH_Integral Tint>
const T& operator()(Tint idx) const {
  if(idx <= Tint{0}){
    throw(std::out_of_range("Fortran array with index <= 0"));
  }
  if(idx > static_cast<Tint>(_size)){
    throw(std::out_of_range("Fortran array with index > size"));
  }
  return _data[idx-1];
}
// Indexing:2 ends here

// [[file:../../../org/maphys/utils/FortranArray.org::*Footer][Footer:1]]
}; // FortranArray
} // namespace maphys
// Footer:1 ends here
