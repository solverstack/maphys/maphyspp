// [[file:../../../org/maphys/utils/Chameleon.org::*Header][Header:1]]
#pragma once

#include <chameleon.h>
#if defined(CHAMELEON_USE_MPI)
#include <mpi.h>
#endif

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/utils/Chameleon.org::*Class][Class:1]]
class Chameleon {
// Class:1 ends here

// [[file:../../../org/maphys/utils/Chameleon.org::*Attributes][Attributes:1]]
public:
  // static members
  static std::shared_ptr<Chameleon> _cham_glob_ptr;
  static int ncpus;     // number of worker of type CPU used by Chameleon
  static int ngpus;     // number of worker of type GPU used by Chameleon
  static int tile_size; // size of the tiles (blocks), square sub-matrices

private:
#if defined(CHAMELEON_USE_MPI)
  bool _handlempi = false; // whether or not we handle mpi init/finalize
#endif
  int _mpirank = 0;   // my mpirank
  int _mpinp = 1;     // number of mpi processus
// Attributes:1 ends here

// [[file:../../../org/maphys/utils/Chameleon.org::*Constructors][Constructors:1]]
public:
  Chameleon(){
    initialize();
  }
  ~Chameleon() {
    RUNTIME_stop_profiling();
    CHAMELEON_Resume();
    CHAMELEON_Finalize();
#if defined(CHAMELEON_USE_MPI)
    if (_handlempi) {
      MPI_Finalize();
    }
#endif
  }
// Constructors:1 ends here

// [[file:../../../org/maphys/utils/Chameleon.org::*Move operations][Move operations:1]]
public:
  Chameleon(const Chameleon&) = delete;
  Chameleon& operator=(const Chameleon&) = delete;

  Chameleon(Chameleon&&) = default;
  Chameleon& operator=(Chameleon&&) = default;
// Move operations:1 ends here

// [[file:../../../org/maphys/utils/Chameleon.org::*Initialize][Initialize:1]]
private:
  void initialize() {
    // if chameleon already initialized, finalize it
    if (CHAMELEON_Initialized()) {
      CHAMELEON_Resume();
      CHAMELEON_Finalize();
    }
    // initialize MPI
#if defined(CHAMELEON_USE_MPI)
    int mpiinit;
    MPI_Initialized(&mpiinit);
    if (!(mpiinit)) {
      MPI_Init(NULL, NULL);
      _handlempi = true;
    }
    MPI_Comm_rank(MPI_COMM_WORLD, &_mpirank);
    MPI_Comm_size(MPI_COMM_WORLD, &_mpinp);
#endif
    // initialize chameleon parameters
    CHAMELEON_Init(Chameleon::ncpus, Chameleon::ngpus);
    CHAMELEON_Enable(CHAMELEON_PROFILING_MODE);
    CHAMELEON_Disable(CHAMELEON_WARNINGS);
    CHAMELEON_Set(CHAMELEON_TILE_SIZE, Chameleon::tile_size);
    RUNTIME_start_profiling();
  }
// Initialize:1 ends here

// [[file:../../../org/maphys/utils/Chameleon.org::*Getters and Setters][Getters and Setters:1]]
public:
int getMpiNp() { return _mpinp; }
int getMpiRank() { return _mpirank; }
// Getters and Setters:1 ends here

// [[file:../../../org/maphys/utils/Chameleon.org::*Global and static variable initializations][Global and static variable initializations:1]]
}; //class Chameleon

#ifndef MAPHYSPP_CHAMELEON_STATIC_VARIABLES_ALREADY_DECLARED
  std::shared_ptr<Chameleon> Chameleon::_cham_glob_ptr;
  int Chameleon::ncpus = -1;
  int Chameleon::ngpus = 0;
  int Chameleon::tile_size = 320;
#endif
// Global and static variable initializations:1 ends here

// [[file:../../../org/maphys/utils/Chameleon.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
