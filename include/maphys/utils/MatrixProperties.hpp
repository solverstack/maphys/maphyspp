// [[file:../../../org/maphys/utils/MatrixProperties.org::*Header][Header:1]]
#pragma once

#include <type_traits>
#include <complex>
#include <utility>
#include "Error.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Enum class: Symmetry, Storage, Def, Positivity][Enum class: Symmetry, Storage, Def, Positivity:1]]
// Matrix properties
enum class MatrixSymmetry : int {general, symmetric, hermitian};
enum class MatrixStorage : int {full, lower, upper};
enum class MatrixDef : int {indefinite, definite, semidefinite};
enum class MatrixPositivity : int {indefinite, positive, negative};
constexpr const int Size_MatrixSymmetry = 3;
constexpr const int Size_MatrixStorage = 3;
constexpr const int Size_MatrixDef = 3;
constexpr const int Size_MatrixPositivity = 3;
// Enum class: Symmetry, Storage, Def, Positivity:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Attributes][Attributes:1]]
template<typename Scalar>
class MatrixProperties{

private:

  MatrixSymmetry _sym = MatrixSymmetry::general;
  MatrixStorage _storage = MatrixStorage::full;
  MatrixDef _definiteness = MatrixDef::indefinite;
  MatrixPositivity _positivity = MatrixPositivity::indefinite;

  static constexpr const bool _is_complex = is_complex<Scalar>::value;

  void check_consistency(){
    if (_sym == MatrixSymmetry::general){
      MAPHYSPP_ASSERT( _storage == MatrixStorage::full, "General matrix cannot be stored with triangular storage");
    }
    if( _definiteness == MatrixDef::indefinite) {
      MAPHYSPP_ASSERT( _positivity == MatrixPositivity::indefinite, "Indefinite matrix should have indefinite positivity" );
    }
    if( _positivity == MatrixPositivity::indefinite ){
      MAPHYSPP_ASSERT( _definiteness == MatrixDef::indefinite, "Indefinite matrix should have indefinite positivity" );
    }
    if( _sym == MatrixSymmetry::hermitian ){
      MAPHYSPP_ASSERT(  _is_complex, "Hermitian matrix with non complex scalar type" );
    }
  }
// Attributes:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Constructor][Constructor:1]]
public:

  MatrixProperties(MatrixSymmetry sym = MatrixSymmetry::general,
              MatrixStorage storage = MatrixStorage::full,
              MatrixDef def = MatrixDef::indefinite,
              MatrixPositivity pos = MatrixPositivity::indefinite):
    _sym{sym},
    _storage{storage},
    _definiteness{def},
    _positivity{pos}
  {
    check_consistency();
  }

  virtual ~MatrixProperties() = default;
// Constructor:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Getters][Getters:1]]
// Symmetry getters
MatrixSymmetry get_symmetry() const { return _sym; }
bool is_general()const { return (_sym == MatrixSymmetry::general); }
bool is_symmetric() const { return (_sym == MatrixSymmetry::symmetric); }
bool is_hermitian() const { return (_sym == MatrixSymmetry::hermitian); }

// Storage getters
MatrixStorage get_storage_type() const { return _storage; }
bool is_storage_full() const { return (_storage == MatrixStorage::full); }
bool is_storage_upper() const { return (_storage == MatrixStorage::upper); }
bool is_storage_lower() const { return (_storage == MatrixStorage::lower); }

// Definiteness getters
MatrixDef get_definiteness() const { return _definiteness; }
bool is_indefinite() const { return (_definiteness == MatrixDef::indefinite); }
bool is_definite() const { return (_definiteness == MatrixDef::definite); }
bool is_semidefinite() const { return (_definiteness == MatrixDef::semidefinite); }

// Definiteness getters
//indefinite, positive, negative
MatrixPositivity get_positivity() const { return _positivity; }
bool is_positive() const { return (_positivity == MatrixPositivity::positive); }
bool is_negative() const { return (_positivity == MatrixPositivity::negative); }

// Funkier getters
bool is_spd() const {
  return ( is_symmetric() && is_definite() && is_positive() );
}
bool is_hpd() const {
  return ( is_hermitian() && is_definite() && is_positive() );
}

bool is_sym_or_herm() const { return (is_symmetric() || is_hermitian()); }

bool is_spd_or_hpd() const {
  return ( is_sym_or_herm() && is_definite() && is_positive() );
}
// Getters:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Setters][Setters:1]]
// Setters
private:
  void _set_property(MatrixSymmetry s){ _sym = s; }
  void _set_property(MatrixStorage s){ _storage = s; }
  void _set_property(MatrixDef d){ _definiteness = d; }
  void _set_property(MatrixPositivity p){ _positivity = p; }
  void _set_property() {}

public:
  // Variadic template -> each parameter is a call to the _setup function
  template <typename... Types>
  void set_property(const Types&... args){
    ( void(_set_property(args)), ...);
    check_consistency();
  }

  void set_spd(MatrixStorage s){
    _sym = MatrixSymmetry::symmetric;
    _definiteness = MatrixDef::definite;
    _positivity = MatrixPositivity::positive;
    _storage = s;
  }

  void set_default_properties(){
    _sym = MatrixSymmetry::general;
    _storage = MatrixStorage::full;
    _definiteness = MatrixDef::indefinite;
    _positivity = MatrixPositivity::indefinite;
  }
// Setters:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Property copy][Property copy:1]]
template<class S1, class S2>
friend void copy_properties(const MatrixProperties<S1>& m_src, MatrixProperties<S2>& m_dst);

template<class S2>
void copy_properties(const MatrixProperties<S2>& other) {
  _sym = other.get_symmetry();
  _storage = other.get_storage_type();
  _definiteness = other.get_definiteness();
  _positivity = other.get_positivity();
}
// Property copy:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Swap][Swap:1]]
void swap(MatrixProperties& other){
  std::swap(_sym, other._sym);
  std::swap(_storage, other._storage);
  std::swap(_definiteness, other._definiteness);
  std::swap(_positivity, other._positivity);
}

friend void swap(MatrixProperties& m1, MatrixProperties& m2){
  m1.swap(m2);
}
// Swap:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Serialize / deserialize][Serialize / deserialize:1]]
char properties_serialize() const {
  static_assert(Size_MatrixSymmetry * Size_MatrixStorage * Size_MatrixDef * Size_MatrixPositivity < 127);
  int v = static_cast<int>(_sym);
  v += Size_MatrixSymmetry * static_cast<int>(_storage);
  v += Size_MatrixSymmetry * Size_MatrixStorage * static_cast<int>(_definiteness);
  v += Size_MatrixSymmetry * Size_MatrixStorage * Size_MatrixDef * static_cast<int>(_positivity);
  return static_cast<char>(v);
}

void properties_deserialize(char v){
  _positivity = static_cast<MatrixPositivity>(v / (Size_MatrixSymmetry * Size_MatrixStorage * Size_MatrixDef));
  v -= (Size_MatrixSymmetry * Size_MatrixStorage * Size_MatrixDef) * static_cast<int>(_positivity);
  _definiteness = static_cast<MatrixDef>(v / (Size_MatrixSymmetry * Size_MatrixStorage));
  v -= (Size_MatrixSymmetry * Size_MatrixStorage) * static_cast<int>(_definiteness);
  _storage = static_cast<MatrixStorage>(v / Size_MatrixSymmetry);
  v -= Size_MatrixSymmetry * static_cast<int>(_storage);
  _sym = static_cast<MatrixSymmetry>(v);
}

static MatrixSymmetry properties_deserialize_symmetry(char v){
  MatrixProperties<double> dummy;
  dummy.properties_deserialize(v);
  return dummy.get_symmetry();
}

static MatrixStorage properties_deserialize_storage(char v){
  MatrixProperties<double> dummy;
  dummy.properties_deserialize(v);
  return dummy.get_storage_type();
}

static MatrixDef properties_deserialize_definiteness(char v){
  MatrixProperties<double> dummy;
  dummy.properties_deserialize(v);
  return dummy.get_definiteness();
}

static MatrixPositivity properties_deserialize_positivity(char v){
  MatrixProperties<double> dummy;
  dummy.properties_deserialize(v);
  return dummy.get_positivity();
}
// Serialize / deserialize:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*String description of MatrixProperties][String description of MatrixProperties:1]]
std::string properties_str() const {
  std::string output = "Symmetry: ";
  switch(_sym){
     case MatrixSymmetry::general: output += "general"; break;
     case MatrixSymmetry::symmetric: output += "symmetric"; break;
     case MatrixSymmetry::hermitian: output += "hermitian"; break;
  }
  if( _sym != MatrixSymmetry::general){
    if (_definiteness != MatrixDef::indefinite) {
      output += "; Definiteness: ";
      switch (_definiteness) {
      case MatrixDef::indefinite: ; break;
      case MatrixDef::definite: output += "definite"; break;
      case MatrixDef::semidefinite: output += "semidefinite"; break;
      }
      output += "; Positivity: ";
      switch (_positivity) {
      case MatrixPositivity::indefinite: ; break;
      case MatrixPositivity::positive: output += "positive"; break;
      case MatrixPositivity::negative: output += "negative"; break;
      }
    }
  }
  output += "; Storage: ";
  switch(_storage){
  case MatrixStorage::full:  output += "full"; break;
  case MatrixStorage::upper: output += "upper"; break;
  case MatrixStorage::lower: output += "lower"; break;
  }

  return output;
}
// String description of MatrixProperties:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Comparison operators][Comparison operators:1]]
bool operator==(const MatrixProperties& other) {
    return (this->_sym == other._sym &&
            this->_storage == other._storage &&
            this->_positivity == other._positivity &&
            this->_definiteness == other._definiteness);
  }
  bool operator!=(const MatrixProperties& other) {
    return !( *this == other);
  }
};
// Comparison operators:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Out of class functions][Out of class functions:1]]
// Copy and cast
template<class S1, class S2>
void copy_properties(const MatrixProperties<S1>& m_src, MatrixProperties<S2>& m_dst){
  m_dst._sym = m_src._sym;
  m_dst._storage = m_src._storage;
  m_dst._definiteness = m_src._definiteness;
  m_dst._positivity = m_src._positivity;
}
// Out of class functions:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Interface][Interface:1]]
// Symmetry getters
template<typename Scalar> [[nodiscard]] MatrixSymmetry get_symmetry(const MatrixProperties<Scalar>& mt) { return mt.get_symmetry(); }
template<typename Scalar> [[nodiscard]] bool is_symmetric(const MatrixProperties<Scalar>& mt) { return mt.is_symmetric(); }
template<typename Scalar> [[nodiscard]] bool is_hermitian(const MatrixProperties<Scalar>& mt) { return mt.is_hermitian();}

// Storage getters
template<typename Scalar> [[nodiscard]] MatrixStorage get_storage_type(const MatrixProperties<Scalar>& mt) { return mt.get_storage_type(); }
template<typename Scalar> [[nodiscard]] bool is_storage_upper(const MatrixProperties<Scalar>& mt) { return mt.is_storage_upper(); }
template<typename Scalar> [[nodiscard]] bool is_storage_lower(const MatrixProperties<Scalar>& mt) { return mt.is_storage_lower(); }

// Definiteness getters
template<typename Scalar> [[nodiscard]] bool is_definite(const MatrixProperties<Scalar>& mt)     { return mt.is_definite(); }
template<typename Scalar> [[nodiscard]] bool is_semidefinite(const MatrixProperties<Scalar>& mt) { return mt.is_semidefinite(); }

// Positiveness
template<typename Scalar> [[nodiscard]] bool is_positive(const MatrixProperties<Scalar>& mt) { return mt.is_positive(); }
template<typename Scalar> [[nodiscard]] bool is_negative(const MatrixProperties<Scalar>& mt) { return mt.is_negative(); }

// Getters deducted from others
template<typename T> [[nodiscard]] bool is_indefinite(const T& mt)   { return !(is_definite(mt) or is_semidefinite(mt)); }
template<typename T> [[nodiscard]] bool is_storage_full(const T& mt)  { return !(is_storage_lower(mt) or is_storage_upper(mt)); }
template<typename T> [[nodiscard]] bool is_spd(const T& mt) { return is_symmetric(mt) and is_definite(mt) and is_positive(mt); }
template<typename T> [[nodiscard]] bool is_sym_or_herm(const T& mt) { return is_symmetric(mt) or is_hermitian(mt); }
template<typename T> [[nodiscard]] bool is_general(const T& mt) { return !(is_symmetric(mt) or is_hermitian(mt)); }
// Interface:1 ends here

// [[file:../../../org/maphys/utils/MatrixProperties.org::*Footer][Footer:1]]
} // end namespace maphys
// Footer:1 ends here
