// [[file:../../../org/maphys/utils/IndexArray.org::*Header][Header:1]]
#pragma once

#include <vector>
#include <algorithm>
#include <functional>
#include "Error.hpp"
#include "maphys/interfaces/basic_concepts.hpp"

namespace maphys {

/*
  Class inspired by numpy arrays to do:

  - Setting an array values from a filtered array

  A[i] = B

  Where A is an IndexArray<T>
  Where i is an IndexArray<int> or IndexArray<bool>
  Where B is an IndexArray<T> or a generator of element of type T or an element of type T

  - Filter an array

  A = B[i]
  Where A is an IndexArray<T>
  Where i is an IndexArray<int> or IndexArray<bool>
  Where B is an IndexArray<T>

 */

template<class> class IndexArray;
template<class, class> struct IndexArrayView;
template<class, class> struct ConstIndexArrayView;
// Header:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Attributes][Attributes:1]]
template<class T>
class IndexArray{

private:
  using BooleanFunc = std::function<bool(const T&)>;
  std::vector<T> _data;
// Attributes:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Traits][Traits:1]]
public:
using value_type       = typename std::vector<T>::value_type;
using size_type        = typename std::vector<T>::size_type;
using difference_type	 = typename std::vector<T>::difference_type;	
using reference	 = typename std::vector<T>::reference;
using const_reference	 = typename std::vector<T>::const_reference;	
using pointer		 = typename std::vector<T>::pointer;	
using const_pointer 	 = typename std::vector<T>::const_pointer; 	
using iterator 	 = typename std::vector<T>::iterator;
using const_iterator 	 = typename std::vector<T>::const_iterator; 	
using reverse_iterator = typename std::vector<T>::reverse_iterator;
using const_reverse_iterator = typename std::vector<T>::const_reverse_iterator;
// Traits:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Constructors][Constructors:1]]
IndexArray() = default;
IndexArray(const std::vector<T>& d): _data{d} {}
IndexArray(std::vector<T>&& d): _data{std::move(d)} {}
IndexArray(std::initializer_list<T> l): _data{std::vector<T>(l)} {}
IndexArray(size_t size, T val = T{0}): _data{std::vector<T>(size, val)} {}
IndexArray(const T * begin, size_t count): _data{std::vector<T>(count)}{
  std::memcpy(&_data[0], begin, count*sizeof(T));
}

IndexArray(const IndexArray& c) = default;
IndexArray(IndexArray&& c) = default;

IndexArray& operator=(const IndexArray& a) = default;
IndexArray& operator=(IndexArray&& a) = default;

// Conversion
template<typename OtherType>
IndexArray(const IndexArray<OtherType> other){
  _data = std::vector<T>(other.size());
  for(size_t k = 0; k < other.size(); ++k){
    _data[k] = static_cast<T>(other[k]);
  }
}

template<typename OtherType>
IndexArray(IndexArray<OtherType>&& other){
  _data = std::vector<T>(other.size());
  for(size_t k = 0; k < other.size(); ++k){
    _data[k] = static_cast<T>(other._data[k]);
  }
}
// Constructors:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Element accessor][Element accessor:1]]
template<MPH_Integral Tint>
decltype(auto) operator[](Tint i) const { return _data[i]; }
template<MPH_Integral Tint>
decltype(auto) operator[](Tint i) { return _data[i]; }
// Element accessor:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Sort][Sort:1]]
void sort(){
  std::sort(_data.begin(), _data.end());
}
// Sort:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Unique][Unique:1]]
// Mimic the numpy unique function: sort and keep unique values
void unique(){
  std::sort(_data.begin(), _data.end());
  auto last = std::unique(_data.begin(), _data.end());
  _data.erase(last, _data.end());
}
// Unique:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Concatenation][Concatenation:1]]
void insert(const IndexArray<T>& other){
  _data.insert( _data.end(), other.begin(), other.end() );
}
// Concatenation:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Union][Union:1]]
void union_with(const IndexArray<T>& other){
  this->insert( other );
  this->unique();
}
// Union:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Getting pointer over data][Getting pointer over data:1]]
const T* get_data_ptr(void) const {
  return _data.data();
}
// Getting pointer over data:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Getting indices to sort an array][Getting indices to sort an array:1]]
// In-place version
// out must alrdeady be a permutation vector of indices
void argsort(IndexArray<int>& out, int first_idx, int last_idx) const {
  const int size = last_idx - first_idx;
  MAPHYSPP_ASSERT_POSITIVE( size, "Error in argsort in-place: wrong indices given (last < first)");
  MAPHYSPP_ASSERT( static_cast<int>(out.size()) >= size, "Error in argsort in-place: wrong output array size");

  // Idea: sort with a vector of pair (value, idx) and return the array with indices
  std::vector<std::pair<T,int>> with_idx(size);

  for(int k = 0; k < size; ++k){
    with_idx[k].first = _data[out[first_idx + k]];
    with_idx[k].second = out[first_idx + k];
  }

  auto less_first = [](const std::pair<T,int>& a, const std::pair<T,int>& b){
    return (a.first < b.first);
  };

  std::sort(with_idx.begin(), with_idx.end(), less_first);

  for(int k = 0; k < size; ++k){
    out[first_idx + k] = with_idx[k].second;
  }
}

IndexArray<int> argsort() const {
  const int size = static_cast<int>(this->size());
  IndexArray<int> out(size);
  for(int k = 0; k < size; ++k) out[k] = k;
  this->argsort(out, 0, size);
  return out;
}
// Getting indices to sort an array:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Erase end of array][Erase end of array:1]]
void erase_after(const int last_idx){
  _data.erase(_data.begin() + last_idx, _data.end());
}
// Erase end of array:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Resize][Resize:1]]
void resize(const int n_elts){
  _data.resize(n_elts);
}
// Resize:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Getting non zero indices][Getting non zero indices:1]]
IndexArray<int> nonzero(double tolerance = -1) const {
  IndexArray<int> nnz( _data.size() );
  int idx = 0;
  int k = 0;

  if(tolerance == -1){
    for(auto d : _data){
      if(d != 0) nnz[k] = idx++;
      k++;
    }
  }
  else if(tolerance > 0){
    for(const auto d : _data){
      if(std::abs(static_cast<double>(d)) > tolerance) nnz[k] = idx++;
      k++;
    }
  }
  else{
    MAPHYSPP_ASSERT(false, "IndexArray::nonzero : Tolerance muse be strictly positive");
  }

  nnz.erase_after(idx);
  return nnz;
}
// Getting non zero indices:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Minimum and maximum][Minimum and maximum:1]]
T max() const {
  return *(std::max_element(_data.begin(), _data.end()));
}

T min() const {
  return *(std::min_element(_data.begin(), _data.end()));
}
// Minimum and maximum:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Swap with another array][Swap with another array:1]]
void swap(IndexArray<T>& other) {
  _data.swap(other._data);
}

friend void swap(IndexArray<T>& i1, IndexArray<T>& i2) noexcept {
  i1.swap(i2);
}
// Swap with another array:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Sum][Sum:1]]
T sum(int begin = 0, int end = -1, int stride = 1) const {
  if(end < 0) end = _data.size();
  T s{0};
  for(auto k = begin; k < end; k += stride) s += _data[k];
  return s;
}
// Sum:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Where][Where:1]]
// IndexArray<T>.where( BooleanFunc )
IndexArray<int> where(const BooleanFunc f) const {
  IndexArray<int> idx(this->size());
  int k = 0;
  for(int i = 0; i < static_cast<int>(this->size()); ++i){
    if( f(_data[i]) ) {
      idx[k] = i;
      k++;
    }
  }
  idx.erase_after(k);
  return idx;
}
// Where:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Find the index of a value][Find the index of a value:1]]
// Return index of the value (or size if not found)
int find_idx(const T& value) const {
  auto found = std::find(_data.begin(), _data.end(), value);
  return _data.size() - (_data.end() - found);
}
// Find the index of a value:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Binary search in sorted array][Binary search in sorted array:1]]
int searchsorted(const T& value, int beg_idx = 0, int end_idx = -1) const {
  auto beg = _data.begin() + beg_idx;
  auto end = _data.end();
  if(end_idx != -1) end = _data.begin() + end_idx;
  auto l = std::lower_bound(beg, end, value);
  return l - _data.begin();
}
// Binary search in sorted array:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Binary search in sorted array][Binary search in sorted array:2]]
// IndexArray<T>.searchsorted( IndexArray<T> values)
IndexArray<int> searchsorted(const IndexArray<T>& values, int beg_idx = 0, int end_idx = -1) const {
  IndexArray<int> out(values.size());
  int k = 0;
  for(const T& v : values){
    out[k++] = this->searchsorted(v, beg_idx, end_idx);
  }
  return out;
}
// Binary search in sorted array:2 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Binary search in sorted array][Binary search in sorted array:3]]
IndexArray<int> sort_and_search(const IndexArray<T>& values, int beg_idx = 0, int end_idx = -1){
  this->sort();
  return this->searchsorted(values, beg_idx, end_idx);
}
// Binary search in sorted array:3 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Wrapping std::vector functions][Wrapping std::vector functions:1]]
decltype(auto) begin() const { return _data.begin(); }
decltype(auto) begin() { return _data.begin(); }
decltype(auto) end() const { return _data.end(); }
std::size_t size() const { return _data.size(); }
decltype(auto) data() { return _data.data(); }
decltype(auto) data() const { return _data.data(); }

inline void push_back( const T& value ){ _data.push_back(value); }
inline void push_back( T&& value ){ _data.push_back(value); }
template< class... Args >
inline decltype(auto) emplace_back(Args&&... args){ return _data.emplace_back(std::forward<Args>(args)...); }

std::vector<T>&& to_vector() {
  return std::move(_data);
}
// Wrapping std::vector functions:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Display function][Display function:1]]
void display(const std::string& name="", std::ostream &out = std::cout) const {
  if(!name.empty()) out << name << '\n';
  for(auto& i : _data) out << i << " ";
  out << '\n';
}
// Display function:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Expression templates][Expression templates:1]]
template<class F>
IndexArrayView<T, IndexArray<F>> operator[]( const IndexArray<F>& a ){
  return IndexArrayView<T, IndexArray<F>>( this, &a );
}

template<class F>
ConstIndexArrayView<T, IndexArray<F>> operator[]( const IndexArray<F>& a ) const {
  return ConstIndexArrayView<T, IndexArray<F>>( this, &a );
}

template<MPH_Integral Tint>
IndexArray(const ConstIndexArrayView<T, IndexArray<Tint>>& rhs) { filter( *(rhs.arr), *(rhs.filter) ); }
IndexArray(const ConstIndexArrayView<T, IndexArray<bool>>& rhs) { filter( *(rhs.arr), *(rhs.filter) ); }

template<MPH_Integral Tint>
IndexArray(const IndexArrayView<T, IndexArray<Tint>>& rhs) { filter( *(rhs.arr), *(rhs.filter) ); }
IndexArray(const IndexArrayView<T, IndexArray<bool>>& rhs) { filter( *(rhs.arr), *(rhs.filter) ); }
// Expression templates:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Compute method][Compute method:1]]
// IndexArray<T>[ IndexArray<int>] = T
void compute(const IndexArray<int>& filt, const T& rhs){ for(auto& i : filt) _data[i] = rhs; }
// Compute method:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Compute method][Compute method:2]]
// IndexArray<T>[ IndexArray<int>] = IndexArray<T>
void compute(const IndexArray<int>& filt, const IndexArray<T>& rhs){
  MAPHYSPP_ASSERT( filt.size() <= rhs.size(),
                 "IndexArray<T>[ IndexArray<int>] = IndexArray<T>: size(filter) > size(rhs)");
  int k = 0;
  for(auto& i : filt){
    _data[i] = rhs[k];
    k++;
  }
}
// Compute method:2 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Compute method][Compute method:3]]
// IndexArray<T>[ IndexArray<bool>] = T
void compute(const IndexArray<bool>& filt, const T& rhs){
  for(auto i = 0; i < static_cast<int>(_data.size()); ++i){
    if(filt[i]) _data[i] = rhs;
  }
}
// Compute method:3 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Compute method][Compute method:4]]
// IndexArray<T>[ IndexArray<bool>] = IndexArray<T>
void compute(const IndexArray<bool>& filt, const IndexArray<T>& rhs){
  MAPHYSPP_ASSERT( filt.size() >= _data.size(),
                 "IndexArray<T>[ IndexArray<bool>] = IndexArray<T>: size(lhs) > size(filter)");
  auto k = 0;
  for(auto i = 0; i < static_cast<int>(_data.size()); ++i){
    if(filt[i]) {
      _data[i] = rhs[k];
      k++;
    }
  }
}
// Compute method:4 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Add method][Add method:1]]
// IndexArray<T>[ IndexArray<int>] += IndexArray<T>
void add(const IndexArray<int>& filt, const IndexArray<T>& rhs){
  MAPHYSPP_ASSERT( filt.size() <= rhs.size(),
                 "IndexArray<T>[ IndexArray<int>] += IndexArray<T>: size(filter) > size(rhs)");
  int k = 0;
  for(auto& i : filt){
    _data[i] += rhs[k];
    k++;
  }
}
// Add method:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Filter method][Filter method:1]]
template<MPH_Integral Tint>
void operator=(const IndexArrayView<T, IndexArray<Tint>>& rhs){ filter( *(rhs.arr), *(rhs.filter) ); }
template<MPH_Integral Tint>
void operator=(const ConstIndexArrayView<T, IndexArray<Tint>>& rhs){ filter( *(rhs.arr), *(rhs.filter) ); }
void operator=(const IndexArrayView<T, IndexArray<bool>>& rhs){ filter(*(rhs.arr), *(rhs.filter) ); }
void operator=(const ConstIndexArrayView<T, IndexArray<bool>>& rhs){ filter(*(rhs.arr), *(rhs.filter) ); }
// Filter method:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Filter method][Filter method:2]]
// IndexArray<T> = IndexArray<T> [ IndexArray<int> ]
template<MPH_Integral Tint>
void filter(const IndexArray& r, const IndexArray<Tint>& f){
  // Special case for reordering
  if(&r == this){
    MAPHYSPP_ASSERT( _data.size() >= f.size(),
                   "Reordering A = A [ IndexArray<int> ]: size(A) < size(filter)");
    std::vector<T> data_cpy(_data);
    _data = std::vector<T>( f.size() );
    int k = 0;
    for(const int i: f) {
      _data[k] = data_cpy[i];
      k++;
    }
  }
  else{
    _data = std::vector<T>( f.size() );
    int k = 0;
    for(const int i: f) {
      _data[k] = r[i];
      k++;
    }
  }
}
// Filter method:2 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Filter method][Filter method:3]]
// IndexArray<T> = IndexArray<T> [ IndexArray<bool> ]
void filter(const IndexArray& r, const IndexArray<bool>& f){
  MAPHYSPP_ASSERT( f.size() >= r.size(),
                 "IndexArray<T> = IndexArray<T> [ IndexArray<bool> ]: size(filter) < size(rhs)");
  int n_true = 0;
  for(const bool b : f) {
    if (b) ++n_true;
  }
  _data = std::vector<T>( n_true );
  int k = 0;
  int j = 0;
  for(const bool b : f) {
    if(b){
      _data[k] = r[j];
      k++;
    }
    ++j;
  }
}
// Filter method:3 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Term by term operations][Term by term operations:1]]
private:
  inline void termbyterm_op(const IndexArray<T>& rhs, void (*op) (T&, const T&)){
    MAPHYSPP_ASSERT( size() == rhs.size(),
                 "IndexArray<T> +=, -=, *=, /= IndexArray<T>: array of different sizes");
    for(int i = 0; i < static_cast<int>(size()); ++i){
      op(_data[i], rhs._data[i]);
    }
  }

public:
  //IndexArray<T> +=, -=, *=, /= IndexArray<T>
  void operator+=(const IndexArray<T>& rhs){ termbyterm_op(rhs, [](T& l, const T& r){ l += r; }); }
  void operator-=(const IndexArray<T>& rhs){ termbyterm_op(rhs, [](T& l, const T& r){ l -= r; }); }
  void operator*=(const IndexArray<T>& rhs){ termbyterm_op(rhs, [](T& l, const T& r){ l *= r; }); }
  void operator/=(const IndexArray<T>& rhs){ termbyterm_op(rhs, [](T& l, const T& r){ l /= r; }); }
// Term by term operations:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*One value operations][One value operations:1]]
private:
  inline void oneval_op(const T& rhs, void (*op) (T&, const T&)){
    for(int i = 0; i < static_cast<int>(size()); ++i){
      op(_data[i], rhs);
    }
  }

public:
  //IndexArray<T> +=, -=, *=, /= IndexArray<T>
  void operator+=(const T& rhs){ oneval_op(rhs, [](T& l, const T& r){ l += r; }); }
  void operator-=(const T& rhs){ oneval_op(rhs, [](T& l, const T& r){ l -= r; }); }
  void operator*=(const T& rhs){ oneval_op(rhs, [](T& l, const T& r){ l *= r; }); }
  void operator/=(const T& rhs){ oneval_op(rhs, [](T& l, const T& r){ l /= r; }); }
// One value operations:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Comparison operators][Comparison operators:1]]
// Test functions
  bool operator==(const IndexArray& other) const {
    if( other.size() != this->size() ) return false;
    for(auto i = 0; i < static_cast<int>(this->size()); ++i){
      if( _data[i] != other[i] ) return false;
    }
    return true;
  }

  bool operator!=(const IndexArray& other) const { return !(*this == other); }

}; // end class IndexArray
// Comparison operators:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Out of class operators][Out of class operators:1]]
template<class T>
inline IndexArray<T> operator+ (IndexArray<T> A, const IndexArray<T>& B){
  A += B;
  return A;
}

template<class T>
inline IndexArray<T> operator- (IndexArray<T> A, const IndexArray<T>& B){
  A -= B;
  return A;
}
// Out of class operators:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Out of class functions][Out of class functions:1]]
template<class T>
inline IndexArray<int> searchsorted(const IndexArray<T>& array, const IndexArray<T>& values){
  return array.searchsorted(values);
}

template<class T>
inline IndexArray<int> sort_and_search(IndexArray<T>& array, const IndexArray<T>& values){
  return array.sort_and_search(values);
}
// Out of class functions:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Multilevel argsort][Multilevel argsort:1]]
inline void multilevel_argsort_aux(IndexArray<int>&, int, int){}

template<class T, class ... Types>
inline void multilevel_argsort_aux(IndexArray<int>& idx, int begin, int end, const IndexArray<T>& array, Types... args){
  int k = begin;
  int l;
  array.argsort(idx, begin, end);

  for(l = begin+1; l < end; ++l){
    if(array[idx[l]] != array[idx[k]]){
      if(l-k > 1){
        multilevel_argsort_aux(idx, k, l, args...);
      }
      k = l;
    }
  }
  if(l-k > 1) multilevel_argsort_aux(idx, k, l, args...);
}

template<class T, class ... Types>
inline IndexArray<int> multilevel_argsort(const IndexArray<T>& array, Types... args){
  IndexArray<int> idx(array.size());
  for(int k = 0; k < static_cast<int>(array.size()); ++k) idx[k] = k;
  multilevel_argsort_aux(idx, 0, static_cast<int>(idx.size()), array, args...);
  return idx;
}
// Multilevel argsort:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Generators][Generators:1]]
template<typename T>
inline IndexArray<T> arange(const T end){
  MAPHYSPP_ASSERT_POSITIVE(end, "arange(end) with must have (end >= 0)");
  size_t n_elts = static_cast<size_t>(std::ceil(end));
  IndexArray<T> out(n_elts);
  for(size_t k = 0; k < n_elts; ++k) out[k] = static_cast<T>(k);
  return out;
}

template<typename T>
inline IndexArray<T> arange(const T begin, const T end, const T step = T{1}){
  if(step == T{0}){
    MAPHYSPP_ASSERT(begin == end, "arange(begin, end, step) with $ step = 0, end != begin $ is invalid");
    return IndexArray<T>();
  }
  if(begin == end){
    return IndexArray<T>();
  }
  if(step > 0){
    MAPHYSPP_ASSERT(end >= begin, "arange(begin, end, step) with step > 0 must have (end >= begin)");
  }
  else{
    MAPHYSPP_ASSERT(end <= begin, "arange(begin, end, step) with step < 0 must have (end <= begin)");
  }
  size_t n_elts = std::ceil((static_cast<double>(end) - static_cast<double>(begin)) / static_cast<double>(step));
  IndexArray<T> out(n_elts);
  T val = begin;
  for(size_t k = 0; k < n_elts; ++k){
    out[k] = val;
    val += step;
  }
  return out;
}
// Generators:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*IndexArrayView][IndexArrayView:1]]
template<class T, class F>
struct IndexArrayView{

  IndexArray<T> * arr;
  const F * filter;

  IndexArrayView(IndexArray<T> * l, const F * f): arr{l}, filter{f} {}

  void operator=(const IndexArray<T>& rhs) { arr->compute(*filter, rhs); }
  void operator=(const T& rhs) { arr->compute(*filter, rhs); }

  void operator+=(const IndexArray<T>& rhs) { arr->add(*filter, rhs); }
}; // end struct IndexArrayView

template<class T, class F>
struct ConstIndexArrayView{

  const IndexArray<T> * arr;
  const F * filter;

  ConstIndexArrayView(const IndexArray<T> * l, const F * f): arr{l}, filter{f} {}
}; // end struct ConstIndexArrayView
// IndexArrayView:1 ends here

// [[file:../../../org/maphys/utils/IndexArray.org::*Footer][Footer:1]]
} // end namespace maphys
// Footer:1 ends here
