// [[file:../../../org/maphys/utils/CrossProduct.org::*Header][Header:1]]
#pragma once
#include <vector>
#include <algorithm>
#include "Error.hpp"
#include "IndexArray.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/utils/CrossProduct.org::*Attributes and constructor][Attributes and constructor:1]]
template<class T1, class T2>
class CrossProduct{

private:
  const T1& _a1;
  const T2& _a2;
  typename T1::const_iterator _i1;
  typename T2::const_iterator _i2;

public:
  CrossProduct(const T1& a1, const T2& a2):
    _a1{a1}, _a2{a2}, _i1{a1.begin()}, _i2{a2.begin()}
  {}

  CrossProduct(const CrossProduct&) = default;
// Attributes and constructor:1 ends here

// [[file:../../../org/maphys/utils/CrossProduct.org::*Traits][Traits:1]]
using value_type = std::pair<typename T1::value_type, typename T2::value_type>;
// Traits:1 ends here

// [[file:../../../org/maphys/utils/CrossProduct.org::*Input iterator methods][Input iterator methods:1]]
CrossProduct& operator++(){
  ++_i2;
  if (_i2 == _a2.end()) {
    _i2 = _a2.begin();
    ++_i1;
  }
  return *this;
}

// Post-increment (x++)
CrossProduct operator++(int){
  CrossProduct p = *this;
  this->operator++();
  return p;
}

value_type operator*() const {
  return std::make_pair(*_i1, *_i2);
}

size_t size(){ return _a1.size() * _a2.size(); }
// Input iterator methods:1 ends here

// [[file:../../../org/maphys/utils/CrossProduct.org::*Range methods][Range methods:1]]
CrossProduct begin(){
  if(size() == 0) return end();
  CrossProduct p(_a1, _a2);
  p._i1 = _a1.begin();
  p._i2 = _a2.begin();
  return p;
}

CrossProduct end() const {
  CrossProduct p(_a1, _a2);
  p._i1 = _a1.end();
  p._i2 = _a2.begin();
  return p;
}
// Range methods:1 ends here

// [[file:../../../org/maphys/utils/CrossProduct.org::*Comparison functions][Comparison functions:1]]
bool operator!=(const CrossProduct& other) const {
  return (_i1 != other._i1) or (_i2 != other._i2);
}
// Comparison functions:1 ends here

// [[file:../../../org/maphys/utils/CrossProduct.org::*Footer][Footer:1]]
};// CrossProduct
} // end namespace maphys
// Footer:1 ends here
