// [[file:../../../org/maphys/utils/SZ_compressor.org::*Header][Header:1]]
#pragma once

#include <sz.h>
#include "Arithmetic.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/utils/SZ_compressor.org::*Initialize / finalize][Initialize / finalize:1]]
inline void SZ_compressor_init(){ SZ_Init(NULL); }
inline void SZ_compressor_finalize(){ SZ_Finalize(); }
// Initialize / finalize:1 ends here

// [[file:../../../org/maphys/utils/SZ_compressor.org::*SZ compressor class][SZ compressor class:1]]
template<MPH_Scalar Scalar>
class SZ_compressor{
private:
  size_t n_elts = 0;
  size_t n_reals = 0;
  size_t compressed_size = 0;
  unsigned char * compressed = nullptr;
  bool data_tofree = false;
  std::vector<Scalar> raw_data_copy;

public:
  SZ_compressor(){}

  template<typename VectURef>
  SZ_compressor(VectURef&& data, double zeta, int mode = PW_REL){ _compress(std::forward<VectURef>(data), zeta, mode); }
  SZ_compressor(Scalar * data, int size, double zeta, int mode = PW_REL){ _compress(data, size, zeta, mode); }

  ~SZ_compressor(){
    if(data_tofree) free(compressed);
  }

  // Copy is not OK
  SZ_compressor(const SZ_compressor&) = delete;
  SZ_compressor& operator=(const SZ_compressor&) = delete;

  // Move is OK
private:
  void _move(SZ_compressor&& other){
    n_elts = std::exchange(other.n_elts, 0);
    n_reals = std::exchange(other.n_reals, 0);
    compressed_size = std::exchange(other.compressed_size, 0);
    compressed = std::exchange(other.compressed, nullptr);
    data_tofree = std::exchange(other.data_tofree, false);
    raw_data_copy = std::move(other.raw_data_copy);
  }

public:
  SZ_compressor(SZ_compressor&& other){
    _move(std::move(other));
  }

  SZ_compressor& operator=(SZ_compressor&& other){
    if(data_tofree) free(compressed);
    _move(std::move(other));
    return *this;
  }

private:
  constexpr int get_datatype() const {
    if constexpr(std::is_same_v<Scalar, float> || std::is_same_v<Scalar, std::complex<float>>){
      return SZ_FLOAT;
    }
    else{
      return SZ_DOUBLE;
    }
  }

  void _compress(Scalar * data, int size, double zeta, int mode){
    n_elts = size;

    n_reals = n_elts;
    if constexpr(is_complex<Scalar>::value){
      n_reals = 2 * n_elts;
    }

    // Do not compress if too small
    if(n_elts * sizeof(Scalar) <= 1024){
      raw_data_copy = std::vector<Scalar>(n_elts);
      compressed_size = n_elts * sizeof(Scalar);
      std::memcpy(raw_data_copy.data(), data, compressed_size);
      return;
    }

    if(data_tofree) free(compressed);
    compressed = SZ_compress_args(get_datatype(), (void *) data, &compressed_size, mode,
                                  zeta, zeta, zeta,
                                  0, 0, 0, 0, n_reals);
    data_tofree = true;
  }

  template<typename VectURef>
  void _compress(VectURef&& values, double zeta, int mode){
    _compress(values.data(), static_cast<int>(values.size()), zeta, mode);
  }

public:
  void compress(const std::vector<Scalar>& values, double zeta, int mode = PW_REL){
    _compress(std::forward<std::vector<Scalar>>(values), zeta, mode);
  }

  void compress(std::vector<Scalar>&& values, double zeta, int mode = PW_REL){
    _compress(std::forward<std::vector<Scalar>>(values), zeta, mode);
    values.clear();
  }

  void compress(Scalar * data, int size, double zeta, int mode = PW_REL){
    _compress(data, size, zeta, mode);
  }

  void decompress(Scalar * ptr) const {
    if(raw_data_copy.size() > 0){
      std::memcpy(ptr, raw_data_copy.data(), raw_data_copy.size() * sizeof(Scalar));
      return;
    }

    MAPHYSPP_ASSERT(compressed != nullptr, "SZ compressor: decompress on nullptr");
    SZ_decompress_args(get_datatype(), compressed, compressed_size, ptr, 0, 0, 0, 0, n_reals);
  }

  std::vector<Scalar> decompress() const {
    std::vector<Scalar> out_vector(n_elts);
    decompress(out_vector.data());
    return out_vector;
  }

  void decompress_and_free(Scalar * ptr){
    // If not compressed
    if(raw_data_copy.size() > 0){
      std::memcpy(ptr, raw_data_copy.data(), raw_data_copy.size());
      raw_data_copy = std::vector<Scalar>();
      data_tofree = false;
      return;
    }

    SZ_decompress_args(get_datatype(), compressed, compressed_size, ptr, 0, 0, 0, 0, n_reals);
    free(compressed);
    data_tofree = false;
  }

  std::vector<Scalar> decompress_and_free(){
    std::vector<Scalar> out_vector(n_elts);
    decompress_and_free(out_vector.data());
    return out_vector;
  }

  [[nodiscard]] double get_ratio() const {
    if(compressed_size == 0) return 0;

    const int input_bytes = sizeof(Scalar) * n_elts;
    return static_cast<double>(input_bytes) / static_cast<double>(compressed_size);
  }

  [[nodiscard]] size_t get_compressed_size() const { return compressed_size; }

  [[nodiscard]] size_t get_n_elts() const { return n_elts; }
}; // class SZ_compressor
} // namespace maphys
// SZ compressor class:1 ends here
