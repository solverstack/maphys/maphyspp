// [[file:../../../org/maphys/utils/Error.org::*Error management][Error management:1]]
#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <exception>
#include <stdexcept>
#include <cstdio>
#include <execinfo.h>
#include <stdexcept>

#include <cstdlib>
#include <cstring>

struct ComposeAssertError: public std::runtime_error
{
  ComposeAssertError(const std::string& s): std::runtime_error(s) {}
};

inline void fatal_error(const std::string &message, int line, const std::string &file)
{
  std::string err("ASSERT FAILED: ");
  err += file + ':' + std::to_string(line) + '\n' + message + '\n';
  throw ComposeAssertError(err);
}

#define MAPHYSPP_ASSERT( cond_ , mess_)   do{ constexpr int l_l_l = __LINE__; const std::string f_f_f = __FILE__; \
    if (!(cond_)) {                                                     \
      std::string m_m_m{#cond_};                                        \
      m_m_m += "\n";                                                    \
      m_m_m += mess_;                                                   \
      fatal_error(m_m_m, l_l_l, f_f_f);                                 \
    }                                                                   \
  }while(0)
// Error management:1 ends here

// [[file:../../../org/maphys/utils/Error.org::*Error management][Error management:2]]
#define MAPHYSPP_DIM_ASSERT( dim1_, dim2_, mess_)   do{ constexpr int l_l_l = __LINE__; const std::string f_f_f = __FILE__; \
  if (static_cast<Size>(dim1_) != static_cast<Size>(dim2_)) {         \
    std::string m_m_m = "Dimensions not matching: ";                  \
    m_m_m += std::string{#dim1_} + " != " + std::string{#dim2_} + '\n'; \
    m_m_m += std::to_string(dim1_) + " != " + std::to_string(dim2_) + '\n'; \
    m_m_m += mess_;                                                   \
    fatal_error(m_m_m, l_l_l, f_f_f);                                 \
  }                                                                   \
}while(0)
// Error management:2 ends here

// [[file:../../../org/maphys/utils/Error.org::*Error management][Error management:3]]
#define MAPHYSPP_ASSERT_POSITIVE( val_, mess_ )   do{                     \
    if constexpr(std::is_signed<decltype(val_)>::value){                \
        constexpr int l_l_l = __LINE__;                                 \
        const std::string f_f_f = __FILE__;                             \
        if (val_ < 0){                                                  \
          std::string m_m_m = "Negative value: ";                       \
          m_m_m += std::to_string(val_) + '\n';                         \
          m_m_m += mess_;                                               \
          fatal_error(m_m_m, l_l_l, f_f_f);                             \
        }                                                               \
    }                                                                   \
    }while(0)
// Error management:3 ends here

// [[file:../../../org/maphys/utils/Error.org::*Error management][Error management:4]]
#define MAPHYSPP_WARNING( mess_ ) do{ constexpr int l_l_l_l = __LINE__; const std::string f_f_f_f = __FILE__; \
    std::string m_m_m = "Warning: " + f_f_f_f + ':' + std::to_string(l_l_l_l) + '\n'; \
    m_m_m += mess_;                                                     \
    std::cerr << m_m_m << '\n';                                         \
  }while(0)
// Error management:4 ends here
