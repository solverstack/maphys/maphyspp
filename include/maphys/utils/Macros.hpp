// [[file:../../../org/maphys/utils/Macros.org::*Macros][Macros:1]]
#pragma once
#define CREATE_STRUCT(name_) template<typename T> \
  struct name_{                                   \
    const T& value;                               \
    name_(const T& v): value{v} {} };

#define CREATE_STRUCT_NOCONST(name_) template<typename T> \
  struct name_{                                           \
    T& value;                                             \
    name_(T& v): value{v} {} };
// Macros:1 ends here
