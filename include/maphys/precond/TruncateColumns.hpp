// [[file:../../../org/maphys/precond/TruncateColumns.org::*Header][Header:1]]
#pragma once

#include "maphys/solver/LinearOperator.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
// Header:1 ends here

// [[file:../../../org/maphys/precond/TruncateColumns.org::*Attributes][Attributes:1]]
namespace maphys {
  template<typename Matrix, typename Vector, typename Solver,
	   // Warning: Numerator and Denominator needs to be co-primes to not ill-perform
	   int Numerator = 1/*Proportion numerator*/,
	   int Denominator = 2/*Proportion denominator*/>
    class TruncateColumns : public LinearOperator<Matrix, Vector>{

private:
  using Scalar = typename scalar_type<Vector>::type;
  using Real = typename arithmetic_real<Scalar>::type;
  static_assert(Numerator > 0 && Denominator > 0, "maphys::TruncateColumns undefined proportion");

public:
  using scalar_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;
  using matrix_type = Matrix;
  using vector_type = Vector;
    
private:
  Solver _F;
// Attributes:1 ends here

// [[file:../../../org/maphys/precond/TruncateColumns.org::*Removing the smallest values][Removing the smallest values:1]]
template<typename Size>
void _remove_smallest_values(SparseMatrixCSC<Scalar, Size>& A_csc){
  const double p = double{Numerator} / double{Denominator}; /*Proportion to keep*/
  const Size n = A_csc.get_n_cols();
  Real max = Real{0.0};
  for(Size j = 0; j < n; j++){
    std::vector<Size> column_idx{}; // Column value index
    std::vector<Real> column{}; // Column values
    Real min = Real{0.0}; // Minimum in column
    Size min_idx = 0; // Index of min in column
    Scalar* V = nullptr;
    Size* I = nullptr;
    Size nnz = 0;
    std::tie(V, I, nnz) = A_csc.get_col(j);
    if(nnz == 0) continue;
    for(Size i = 0; i < nnz; i++){
      Real Aij = std::abs(V[i]);
      if(Aij == Real{0.0}) continue;
      if(min > Aij || min == Real{0.0}){
        min = Aij;
        min_idx = column.size(); // Index of min in column
      }
      max = std::max(max, Aij);
      column.push_back(Aij);
      column_idx.push_back(I[i]);
    }

    // Deleting the lowest values of the list
    const Size colsize = static_cast<Size>(column.size());
    for(Size tmp = static_cast<Size>(colsize * p); tmp < colsize; tmp++){
      column[min_idx] = Real{0.0};
      min = Real{0.0};
      min_idx = 0;
      for(Size k = 0; k < colsize; k++){
        if(min > column[k] || min == Real{0.0}){
          min = column[k];
          min_idx = k;
        }
      }
    }

    // Modifying the matrix accordingly
    for(Size k = 0; k < colsize; k++){
      if(column[k] == Real{0.0}) A_csc(column_idx[k], j) = Scalar{0.0};
    }
  }
}
// Removing the smallest values:1 ends here

// [[file:../../../org/maphys/precond/TruncateColumns.org::*Local selection][Local selection:1]]
template<typename Size = typename Matrix::index_type>
void _setup_local(const Matrix& A){

  if constexpr(std::is_same<Matrix, SparseMatrixCSC<Scalar, Size>>::value){
	  SparseMatrixCSC<Scalar, Size> A_csc = A;
	  _remove_smallest_values(A_csc);
	  _F.setup(A_csc);
  } else if constexpr(std::is_same<Matrix, SparseMatrixCOO<Scalar, Size>>::value){
    SparseMatrixCSC<Scalar, Size> A_csc;
	  A_csc.from_coo(A);
	  _remove_smallest_values(A_csc);
	  Matrix A_coo;
	  A_coo.from_csc(A_csc);
	  _F.setup(A_coo);
  } else {

	  //MAPHYSPP_WARNING("maphyspp::TruncateColumns : matrix format not supported, attempt at converting to csc");
	  SparseMatrixCSC<Scalar, Size> A_csc = A.to_coo().to_csc();
	  _remove_smallest_values(A_csc);
	  if constexpr(std::is_same<typename Solver::matrix_type, SparseMatrixCSC<Scalar, Size>>::value) {
	    _F.setup(A_csc);
	  }
	  else if constexpr(std::is_same<typename Solver::matrix_type, SparseMatrixCOO<Scalar, Size>>::value) {
	    SparseMatrixCOO<Scalar, Size> A_coo = A_csc.to_coo();
	    _F.setup(A_coo);
	  }
	  else {
	    Matrix A2 = convert(A_csc);
	    _F.setup(A2);
	  } 
  }
}
// Local selection:1 ends here

// [[file:../../../org/maphys/precond/TruncateColumns.org::*Setup][Setup:1]]
public:
  void setup(const Matrix& A){
    if constexpr(!is_distributed<Matrix>::value){
      _setup_local(A);
    } else {
      MAPHYSPP_ASSERT(false, "maphyspp::TruncateColumns : distributed matrices not supported");
    }
  }
// Setup:1 ends here

// [[file:../../../org/maphys/precond/TruncateColumns.org::*Constructor][Constructor:1]]
TruncateColumns(){}

TruncateColumns(const Matrix& A){
  setup(A);
}
// Constructor:1 ends here

// [[file:../../../org/maphys/precond/TruncateColumns.org::*apply function][apply function:1]]
private:
  Vector _apply(Vector B) const {
    return _F * B;
  }

public:
  Vector apply(const Vector& B) {
    return _apply(B);
  }
  Vector apply(const Vector& B) const {
    return _apply(B);
  }
// apply function:1 ends here

// [[file:../../../org/maphys/precond/TruncateColumns.org::*Operator *][Operator *:1]]
Vector operator* (const Vector& vect) const{
  return this->apply(vect);
}
// Operator *:1 ends here

// [[file:../../../org/maphys/precond/TruncateColumns.org::*Footer][Footer:1]]
};
} // namespace maphys
// Footer:1 ends here
