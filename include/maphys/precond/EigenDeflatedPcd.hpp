// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Header][Header:1]]
#pragma once

#include "maphys/solver/CoarseSolve.hpp"
#include "maphys/solver/BlasSolver.hpp"
#include "maphys/kernel/BlasKernels.hpp"
#include "maphys/linalg/SpectralExtraction.hpp"
#include "maphys/precond/TwoLevelAbstractSchwarz.hpp"
#include "maphys/solver/Fabulous.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Class definition][Class definition:1]]
template<typename Matrix, typename Vector,
         typename LocMatrix = typename std::conditional<is_distributed<Matrix>::value, typename Matrix::local_type, Matrix>::type,
         typename LocVector = typename std::conditional<is_distributed<Vector>::value, typename Vector::local_type, Vector>::type,
         class A0Solver = BlasSolver<DenseMatrix<typename scalar_type<Vector>::type>, LocVector>>
class EigenDeflatedPcd : public LinearOperator<Matrix, Vector>{

private:
  using Real = typename Vector::real_type;
  using Scalar = typename scalar_type<Vector>::type;
  using InternalVector = typename vector_type<Matrix>::type;

public:
  using scalar_type = Scalar;
  using matrix_type = Matrix;
  using vector_type = Vector;
// Class definition:1 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Attributes][Attributes:1]]
private:
  const Real _threshold_SVD = 2.e-14;
  int _max_deflation_size;
  int _solve_deflation_size;
  int _deflation_increment;
  using DenseVect = std::conditional_t<is_distributed<Vector>::value, PartVector<DenseMatrix<Scalar>>, DenseMatrix<Scalar>>;
  DenseVect _V0_buffer; // buffer for the deflation space
  int _current_deflation_size;
  int _last_deflation_size;
  Vector _z;
  const Matrix *_A = nullptr;
  A0Solver _A0_inv;
  bool _is_setup = false;
  bool _init_guess_computed = false;
  //bool _constant_V0 = false;
  SpectralExtraction<Matrix, Vector> _spectral_extraction;
// Attributes:1 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Constructor and setup][Constructor and setup:1]]
public:
  EigenDeflatedPcd() {}
  EigenDeflatedPcd(const Matrix& A, const int max_deflation_size,
                       const int solve_deflation_size,
                       const int nev)
  {
    setup(A, max_deflation_size, solve_deflation_size, nev);
  }

  void setup(const Matrix& A, const int max_deflation_size,
             const int solve_deflation_size,
             const int deflation_increment, bool keep_deflation_space = false) {
    _max_deflation_size = max_deflation_size;
    _solve_deflation_size = solve_deflation_size;
    _deflation_increment = deflation_increment;
    setup(A, keep_deflation_space);
    _is_setup = true;
  }

  void setup(const Matrix& A) { setup(A, false); }
  void setup(const Matrix& A, bool keep_deflation_space) {
    auto A_ptr = &A;
    if(_A != A_ptr) {
      _A = A_ptr;
      if(n_cols(_V0_buffer) == 0 || !keep_deflation_space){
        if constexpr(is_distributed<Matrix>::value) {
          _V0_buffer.initialize(_A->get_proc());
          _V0_buffer.set_on_intrf(_A->on_intrf());
          _V0_buffer = DenseVect(_V0_buffer, _max_deflation_size);
          _z = DenseVect(_V0_buffer, /*nrhs*/1);
        } else {
          _V0_buffer = DenseVect(n_rows(*_A), _max_deflation_size);
          _z = DenseVect(n_rows(*_A), /*nrhs*/1);
        }
        _current_deflation_size = 0;
        _last_deflation_size = 0;
        _spectral_extraction.setup(_solve_deflation_size, _deflation_increment, &_current_deflation_size, &_V0_buffer, &_z);
      }
    }
    _compute_A0();
  }
// Constructor and setup:1 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Compute A0][Compute A0:1]]
void _compute_A0() {
  Timer<TIMER_ITERATION> t("Compute A0");
  if(_current_deflation_size == _last_deflation_size) return;
  const Matrix& A = *_A;
  _compute_V0();
  if(_current_deflation_size == 0) return;
  DenseVect V0 = _V0_buffer.get_vect_view(0, _current_deflation_size);
  DenseVect AV0 = A * V0;
  DenseMatrix<Scalar> A0 = dot_block(V0, A * V0);
  A0.set_spd(MatrixStorage::full);
  _A0_inv.setup(A0);
  _last_deflation_size = _current_deflation_size;
}
// Compute A0:1 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Compute initial guess][Compute initial guess:1]]
Vector init_guess(const Vector& B){
  MAPHYSPP_ASSERT(_is_setup, "EigenDeflatedPcd: calling init_guess before setup");
  _init_guess_computed = true;
  return _apply_V0_A0_1_V0T(B);
}
// Compute initial guess:1 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Apply][Apply:1]]
private:
void _compute_V0() { // FIXME: native qr not from fabulous
  MAPHYSPP_ASSERT(_current_deflation_size > 0, "EigenDeflatedPcd::_compute_V0: V0 is empty");
  DenseVect V0 = _V0_buffer.get_vect_view(0, _current_deflation_size);
  auto V0F = convert_to_fabulous(V0);
  fabulous::Block<Scalar> R{static_cast<int>(V0F.get_nb_col()), static_cast<int>(V0F.get_nb_col())};
  V0F.qr(V0F, R);

  for(int j = 0; j < R.get_nb_col(); j++){
    for(int i = 0; i <= j; i++){
      if(!is_normal_or_zero(R(i,j))) R(i,j) = Scalar{0.0};
    }
  }

  DenseMatrix<Scalar> RM = convert_to_mpp(R);

  auto [U, Sigma] = blas_kernels::svd_left_sv(RM);

  int svd_rank = 0;

  for(auto s : Sigma){
    if(s > _threshold_SVD) svd_rank++;
  }
  if(svd_rank < _current_deflation_size) {
    V0 = V0 * U;
    _current_deflation_size = svd_rank;
  }
  if(svd_rank == 0) MAPHYSPP_WARNING("EigenDeflatedPcd: SVD failed to find any singular value > 0");
}

Vector _apply_V0_A0_1_V0T(const Vector &x) { // to be called in apply
  Timer<TIMER_ITERATION> t("Apply V0 A0^{-1} V0^T");
  MAPHYSPP_ASSERT(_init_guess_computed, "EigenDeflatedPcd::apply: init guess must have been computed first");
  if(_current_deflation_size == 0) { // Empty deflation space
    Vector X0 = x; // copy
    return X0 * Scalar{0.0};
  }
  // Compute A0 if necessary
  _compute_A0();

  DenseVect V0 = _V0_buffer.get_vect_view(0, _current_deflation_size);
  auto tmp = _A0_inv * dot_block(V0, x);
  return V0 * tmp;
}

Vector _apply_P0(const Vector &x) {
  if(_current_deflation_size == 0) { // Empty deflation space
    Vector X0 = Vector(x); // copy
    return X0 * Scalar{0.0};
  }
  return _apply_V0_A0_1_V0T((*_A) * x);
}

Vector _apply(const Vector &x) {
  _z = x; // Copy: used in spectral extraction
  if(_current_deflation_size == 0) return x;
  else return x - _apply_P0(x);
}

public:
SpectralExtraction<Matrix, Vector>* get_spectral_extraction() {
  return &_spectral_extraction;
}

Vector project(Vector &x) {
  _z = x; // Copy: used in spectral extraction
  return _apply_P0(x);
}

Vector apply(const Vector& B) {
  return _apply(B);
}

Vector operator* (const Vector& vect) {
  return _apply(vect);
}
// Apply:1 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Trait][Trait:1]]
}; // class EigenDeflatedPcd
// Trait:1 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Trait][Trait:2]]
template<MPH_Matrix Matrix, MPH_Vector Vector, typename M1_type, typename A0Solver, bool deflated, bool init>
struct has_spectral_extraction<TwoLevelAbstractSchwarz<Matrix, Vector,
                                                       EigenDeflatedPcd<Matrix, Vector, A0Solver>,
                                                       M1_type, deflated, init>> : public std::true_type {};

template<MPH_Matrix Matrix, MPH_Vector Vector, typename A0Solver>
struct has_spectral_extraction<EigenDeflatedPcd<Matrix, Vector, A0Solver>> : public std::true_type {};

template<typename Mat, typename Vect>
struct has_init_guess_function<EigenDeflatedPcd<Mat, Vect>> : public std::true_type {};
// Trait:2 ends here

// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
