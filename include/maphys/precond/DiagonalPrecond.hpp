// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Header][Header:1]]
#pragma once

#include "maphys/interfaces/linalg_concepts.hpp"
namespace maphys {

  template<typename M>
  concept MPH_Matrix_diagonal_extract = MPH_Matrix<M> &&
    requires(M m){ diagonal_as_vector(m); };

  template<MPH_Matrix_diagonal_extract Matrix, MPH_Vector_Single_Or_Multiple Vector>
  class DiagonalPrecond;

  template<MPH_Matrix_diagonal_extract Matrix, MPH_Vector_Single_Or_Multiple Vector>
  struct ConjugateDiagonalPrecond;
}

#include "maphys/solver/LinearOperator.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"

#ifndef MAPHYSPP_NO_MPI
#include "maphys/part_data/PartMatrix.hpp"
#endif // MAPHYSPP_NO_MPI

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Attributes][Attributes:1]]
template<MPH_Matrix_diagonal_extract Matrix, MPH_Vector_Single_Or_Multiple Vector>
    class DiagonalPrecond : public LinearOperator<Matrix, Vector>{

    private:
      using Scalar = typename scalar_type<Vector>::type;
      using InternalVector = typename vector_type<Matrix>::type;

    public:
      using scalar_type = Scalar;
      using matrix_type = Matrix;
      using vector_type = Vector;

    private:
      std::unique_ptr<InternalVector> _inv_diag;
      Size _m;
// Attributes:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Store inverse of diagonal elements][Store inverse of diagonal elements:1]]
void _store_diagonal_inverse(const Matrix& mat){
    _m = n_rows(mat);
    MAPHYSPP_DIM_ASSERT(_m, n_cols(mat), "DiagonalPrecond must be on a squared matrix");

    auto inverse_term = [](Scalar& s){
                          if(s == Scalar{0}){
                            s = Scalar{1};
                          }
                          else{
                            s = Scalar{1}/s;
                          }
                        };

    if constexpr(!is_distributed<Matrix>::value){
      _inv_diag = std::make_unique<InternalVector>(diagonal_as_vector(mat));
      InternalVector& idiag = *_inv_diag;

      for(Size k = 0; k < _m; ++k){
        inverse_term(idiag[k]);
      }
    }
#ifndef MAPHYSPP_NO_MPI
    else{
      _inv_diag = std::make_unique<InternalVector>(diagonal_as_vector(mat));
      _inv_diag->assemble();
      _inv_diag->apply(inverse_term);
    }
#endif // MAPHYSPP_NO_MPI
  }
// Store inverse of diagonal elements:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Setup][Setup:1]]
public:
  void setup(const Matrix& A){
    Timer<TIMER_PRECOND> t("Precond setup diagonal");
    _store_diagonal_inverse(A);
  }
// Setup:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Constructor][Constructor:1]]
DiagonalPrecond(){}

DiagonalPrecond(const Matrix& A){
  setup(A);
}
// Constructor:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Application on a non distributed vector][Application on a non distributed vector:1]]
private:
  Vector _apply(Vector vect) const {
    MAPHYSPP_ASSERT(_inv_diag != nullptr, "DiagonalPrecond: a matrix must be setup before application");
    const Size nrhs = n_cols(vect);
    const Size ld = get_leading_dim(vect);
    Scalar * vect_ptr = get_ptr(vect);
    for(Size l = 0; l < nrhs; ++l){
      for(Size k = 0; k < _m; ++k){
        vect_ptr[l*ld + k] *= (*_inv_diag)[k];
      }
    }
    return vect;
  }

  Vector _apply_conj(Vector vect) const {
    MAPHYSPP_ASSERT(_inv_diag != nullptr, "DiagonalPrecond: a matrix must be setup before application");
    const Size nrhs = n_cols(vect);
    const Size ld = vect.get_leading_dim();
    Scalar * vect_ptr = get_ptr(vect);
    for(Size l = 0; l < nrhs; ++l){
      for(Size k = 0; k < _m; ++k){
        vect_ptr[l*ld + k] *= conj<Scalar>(*_inv_diag)[k];
      }
    }

    return vect;
  }
// Application on a non distributed vector:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Application on a distributed vector][Application on a distributed vector:1]]
#ifndef MAPHYSPP_NO_MPI
  // Distributed case
  Vector _apply_dist(Vector vect) const {
    Timer<TIMER_PRECOND> t("Precond apply diagonal");
    vect.apply([](Scalar& s1, const Scalar& s2){ s1 *= s2; }, *_inv_diag);
    return vect;
  }

  Vector _apply_conj_dist(Vector vect) const {
    Timer<TIMER_PRECOND> t("Precond apply diagonal");
    vect.apply([](Scalar& s1, const Scalar& s2){ s1 *= conj<Scalar>(s2); }, *_inv_diag);
    return vect;
  }
#endif // MAPHYSPP_NO_MPI
// Application on a distributed vector:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Public apply method][Public apply method:1]]
public:
Vector apply(const Vector& B) {
  if constexpr(is_distributed<Vector>::value){ return _apply_dist(B); }
  else{ return _apply(B); }
}
Vector apply(const Vector& B) const {
  if constexpr(is_distributed<Vector>::value){ return _apply_dist(B); }
  else{ return _apply(B); }
}
Vector apply_conj(const Vector& B) {
  if constexpr(is_distributed<Vector>::value){ return _apply_conj_dist(B); }
  else{ return _apply_conj(B); }
}
Vector apply_conj(const Vector& B) const {
  if constexpr(is_distributed<Vector>::value){ return _apply_conj_dist(B); }
  else{ return _apply_const(B); }
}
// Public apply method:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Getters][Getters:1]]
inline Size get_n_rows() const { return _m; }
inline Size get_n_cols() const { return _m; }
// Getters:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Conjugate of the preconditioner][Conjugate of the preconditioner:1]]
ConjugateDiagonalPrecond<Matrix, Vector> t() const {
    return ConjugateDiagonalPrecond<Matrix, Vector>( this );
  }
// Conjugate of the preconditioner:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Conjugate of the preconditioner][Conjugate of the preconditioner:2]]
}; // class DiagonalPrecond
// Conjugate of the preconditioner:2 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Conjugate of the preconditioner][Conjugate of the preconditioner:3]]
template<MPH_Matrix_diagonal_extract Matrix, typename Vector>
ConjugateDiagonalPrecond<Matrix, Vector> adjoint(const DiagonalPrecond<Matrix, Vector>& pcd ){ return pcd.t(); }
// Conjugate of the preconditioner:3 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Conjugate of the preconditioner view][Conjugate of the preconditioner view:1]]
template<MPH_Matrix_diagonal_extract Matrix, MPH_Vector_Single_Or_Multiple Vector>
  struct ConjugateDiagonalPrecond {

    using Scalar = typename scalar_type<Vector>::type;
    using scalar_type = Scalar;

    const DiagonalPrecond<Matrix, Vector> * pcd;

    ConjugateDiagonalPrecond(const DiagonalPrecond<Matrix, Vector> * p): pcd{p} {}
  }; //struct ConjugateDiagonalPrecond
// Conjugate of the preconditioner view:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Interface][Interface:1]]
template<MPH_Matrix_diagonal_extract Matrix, MPH_Vector Vector>
Vector operator* (const DiagonalPrecond<Matrix, Vector>& pcd, const Vector& vect){
  return pcd.apply(vect);
}

template<MPH_Matrix_diagonal_extract Matrix, MPH_Vector Vector>
Vector operator* (const ConjugateDiagonalPrecond<Matrix, Vector>& tpcd, const Vector& vect){
  return tpcd.pcd->apply_conj(vect);
}

template<MPH_Matrix_diagonal_extract Matrix, MPH_Vector Vector>
inline Size n_rows(const DiagonalPrecond<Matrix, Vector>& pcd) { return pcd.get_n_rows(); }
template<MPH_Matrix_diagonal_extract Matrix, MPH_Vector Vector>
inline Size n_cols(const DiagonalPrecond<Matrix, Vector>& pcd) { return pcd.get_n_cols(); }
// Interface:1 ends here

// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
