#pragma once

#include "maphys/solver/LinearOperator.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"

namespace maphys {
  template<typename Matrix, typename Vector, class InnerSolver>
    class DummyPrecond : public LinearOperator<Matrix, Vector>{

    private:
      using Scalar = typename scalar_type<Vector>::type;
      using InternalVector = typename vector_type<Matrix>::type;

    public:
      using scalar_type = Scalar;
      using matrix_type = Matrix;
      using vector_type = Vector;

    private:
      InnerSolver _IS;


public:
  void setup(const Matrix& A){
    if constexpr(!std::is_same<InnerSolver, NoPrecond>::value){
      _IS.setup(A);
    } else {
      (void) A;
    }
  }

  DummyPrecond(){}

  DummyPrecond(const Matrix& A){
    setup(A);
  }
private:
  Vector _apply(Vector vect) const {
    if constexpr(!std::is_same<InnerSolver, NoPrecond>::value){
      return _IS * vect;
    } else {
      return vect;
    }
  }

public:
  Vector apply(const Vector& B) {
    return _apply(B);
  }
  Vector apply(const Vector& B) const {
    return _apply(B);
  }


    Vector operator* (const Vector& vect) const{
      return this->apply(vect);
    }
  };


} // namespace maphys
