// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Header][Header:1]]
#pragma once

#include "maphys/solver/LinearOperator.hpp"
#include "maphys/part_data/PartOperator.hpp"
#include "maphys/part_data/PartMatrix.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*AbstractSchwarz parameters][AbstractSchwarz parameters:1]]
namespace parameters{
CREATE_STRUCT(Ti)
CREATE_STRUCT(setup_local_solver)
} // end namespace parameters
// AbstractSchwarz parameters:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Structures to use as template parameters][Structures to use as template parameters:1]]
enum class PartOfUnityMethod { none, multiplicity, boolean, matrix };
// Structures to use as template parameters:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Class definition][Class definition:1]]
template<MPH_Matrix PartMat,
	 MPH_Vector PartVect,
	 class LocalSolver = Identity<PartMat,PartVect>,
	 bool Assemble = false,
	 PartOfUnityMethod Di_type = PartOfUnityMethod::none,
	 bool TransmissionMatrix = false>

requires is_distributed<PartMat>::value && is_distributed<PartVect>::value

class AbstractSchwarz : public LinearOperator<PartMat, PartVect>{
// Class definition:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Attributes][Attributes:1]]
public:
  using scalar_type = typename LinearOperator<PartMat, PartVect>::scalar_type;
  using real_type = typename LinearOperator<PartMat, PartVect>::real_type;
  using matrix_type = PartMat;
  using vector_type = PartVect;

  static const constexpr bool is_AS = (Assemble == true);
  static const constexpr bool is_NN = !(Di_type == PartOfUnityMethod::none);
  static const constexpr bool is_RR = TransmissionMatrix;

private:
  using Scalar = scalar_type;
  using Real = real_type;
  using LocMat = typename PartMat::local_type;
  using LocVect = typename PartVect::local_type;
  using LocDiag = typename diag_type<LocMat>::type;
  using Diagonal = PartMatrix<LocDiag>;

  bool _A_given = false;
  PartMat _Aih;

  // Transmission matrices
  PartMat _Ti_matrix; // Full matrix
  Scalar _Ti_scalar = 1; // Scalar matrix
  bool _Ti_computed = false;

  PartOperator<LocalSolver> _loc_solver;
  bool _is_setup = false;
  std::function<void(LocalSolver&,const LocMat&)> _setup_local_solver;
// Attributes:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Constructors][Constructors:1]]
public:
AbstractSchwarz() {}
AbstractSchwarz(const PartMat& A){ setup(A); }

// No copy
AbstractSchwarz(const AbstractSchwarz&) = delete;
AbstractSchwarz& operator=(const AbstractSchwarz&) = delete;

// Move
private:
inline void _move_aS(AbstractSchwarz&& other){
  _Aih = std::move(other._Aih);
  _Ti_matrix = std::move(other._Ti_matrix);
  _Ti_scalar = std::exchange(other._Ti_scalar, 1);
  _Ti_computed = std::exchange(other._Ti_computed, false);
  _loc_solver = std::move(other._loc_solver);
  _is_setup = std::exchange(other._is_setup, false);
}
public:
AbstractSchwarz(AbstractSchwarz&& other){
  _move_aS(std::move(other));
}
AbstractSchwarz& operator=(AbstractSchwarz&& other) {
  if(&other == this) return *this;
  _move_aS(std::move(other));
  return *this;
}
// Constructors:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Setup parameters][Setup parameters:1]]
private:
  void _setup(const parameters::A<PartMat>&       v) { setup(v.value); }
  void _setup(const parameters::A<const PartMat>& v) { setup(v.value); }

  void _setup(const parameters::Ti<PartMat>& v)        { compute_tr_mat(v.value); }
  void _setup(const parameters::Ti<const PartMat>& v)  { compute_tr_mat(v.value); }
  void _setup(const parameters::Ti<PartVect>& v)       { compute_tr_mat(v.value); }
  void _setup(const parameters::Ti<const PartVect>& v) { compute_tr_mat(v.value); }
  void _setup(const parameters::Ti<Scalar> v)          { _Ti_scalar = v.value; }
  void _setup(const parameters::Ti<const Scalar> v)    { _Ti_scalar = v.value; }
  void _setup(parameters::setup_local_solver<std::function<void(LocalSolver&,const LocMat&)>> v) {
    _setup_local_solver = v.value;
  }

public:
  void setup(const PartMat& A){
    _Aih = A;
    _A_given = true;
  }
  void setup () const {}

  // Variadic template -> each parameter is a call to the _setup function
  template <typename... Types>
  void setup(const Types&... args) noexcept {
    ( void(_setup(args)), ...);
  }

  template<typename... Types>
  void setup(const PartMat& A, const Types&... args){
    setup(A);
    setup(args...);
  }
// Setup parameters:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Compute transfer matrix for Robin-Robin][Compute transfer matrix for Robin-Robin:1]]
private:
// Careful: for scalar, we need to be sure we have a Process to bind to
// so we use this function in setup_abstractschwarz
void compute_tr_mat_scalar(){
  MAPHYSPP_ASSERT(_Aih.get_proc() != nullptr, "AbstractSchwarz::compute_tr_mat_scalar: Aih not bound to any Process");
  _Ti_matrix = PartMat(_Aih.get_proc(), true);
  _Ti_matrix.identity(true, true);
  _Ti_matrix *= _Ti_scalar; // Use identity by default
  _Ti_computed = true;
}

void compute_tr_mat(const PartVect& vect){
  MAPHYSPP_ASSERT(TransmissionMatrix, "Error in AbstractSchwarz::compute_tr_mat(vect) on non RobinRobin preconditioner");

  _Ti_matrix = PartMat(vect.get_proc(), true);
  _Ti_matrix.from_diagonal(vect);
  _Ti_computed = true;
}

void compute_tr_mat(const PartMat& mat){
  MAPHYSPP_ASSERT(TransmissionMatrix, "Error in AbstractSchwarz::compute_tr_mat(mat) on non RobinRobin preconditioner");
  MAPHYSPP_ASSERT(mat.check_is_on_interface(), "Transmission matrix must be defined on the interface");

  _Ti_matrix = mat;
  _Ti_computed = true;
}
// Compute transfer matrix for Robin-Robin:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Safe inverse function][Safe inverse function:1]]
private:
  static void safe_inverse(Scalar &scal){
    // Threshold for low values
    const constexpr Real thresh = arithmetic_tolerance<Scalar>::value;
    const Scalar bigval = Scalar{10} / static_cast<Scalar>(thresh);
    if(std::abs(scal) < thresh){
      scal = bigval;
    }
    else{
      scal = Scalar{1} / scal;
    }
  }
// Safe inverse function:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Partition of unity][Partition of unity:1]]
public:
  static PartVect partition_of_unity(const PartMat& A){

    PartVect D = diagonal_as_vector(A);

    if constexpr(Di_type == PartOfUnityMethod::multiplicity){
      D.set_to_ones();
      D.assemble();
      D.apply(safe_inverse);
    }
    else if constexpr(Di_type == PartOfUnityMethod::matrix){
      // D = diagonal(A) / diagonal(assemble(A))
      PartVect assembled_diag = D;
      assembled_diag.assemble();
      auto divide = [](Scalar& s1, const Scalar& s2){ s1 /= s2; };
      D.apply(divide, assembled_diag);
    }
    else if constexpr(Di_type == PartOfUnityMethod::boolean){
      D.set_to_ones();
      D.disassemble();
    }

    return D;
  }
// Partition of unity:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Setup function][Setup function:1]]
void setup_abstractschwarz(){
  MAPHYSPP_ASSERT(_A_given, "AbstractSchwarz: no matrix has been setup");
  Timer<TIMER_PRECOND> t("Precond setup aS");

  // 1. Assembly step (AdditiveSchwarz)
  if constexpr(Assemble){
    _Aih.assemble();
  }

  // 2. Addition step (RobinRobin)
  if constexpr(TransmissionMatrix){
    if(!_Ti_computed){
      compute_tr_mat_scalar();
    }
    _Aih.add_on_interface(_Ti_matrix);
  }

  // 3. Partition of unity step (NeumannNeumann)
  if constexpr(Di_type != PartOfUnityMethod::none){
    PartVect D = partition_of_unity(_Aih); // Here should be A in theory
    D.apply(safe_inverse);
    // Aih <- Di^-1 . Aih . Di^-1

    // Careful: these products preserves symmetry of A
    MatrixSymmetry mat_sym = MatrixSymmetry::general;
    _Aih.apply_on_data([&mat_sym](LocMat& m){ mat_sym = get_symmetry(m); });

    Diagonal D_inv(D);
    //We do not want to assemble the matrix after the product
    //_Aih = D_inv * _Aih * D_inv;
    const bool no_assemble = false;
    _Aih.matprod(D_inv, no_assemble); // Aih *= D_inv
    _Aih = D_inv.matprod_left(_Aih, no_assemble); // Aih = D_inv * Aih

    // Set the symmetry back
    _Aih.apply_on_data([mat_sym](LocMat& m){ m.set_property(mat_sym); });
  }

  _Aih.apply_on_data([](LocMat& m){ m.set_property(MatrixDef::definite, MatrixPositivity::positive); });

  // 4. Matrix is factorized
  if constexpr(!std::is_same<LocalSolver, Identity<PartMat,PartVect>>::value){
    _loc_solver.initialize(_Aih.get_proc());

    // Use user setup function if given
    if(_setup_local_solver){
      _loc_solver.apply_on_data(_Aih, _setup_local_solver);
    }
    else{ // Otherwise just setup
      _loc_solver.setup(_Aih);
    }
  }

  _is_setup = true;
}
// Setup function:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Apply function][Apply function:1]]
PartVect apply(const PartVect& B){
  if(!_is_setup){
    setup_abstractschwarz();
  }
  Timer<TIMER_PRECOND> t("Precond apply aS");
  if constexpr(std::is_same<LocalSolver, Identity<PartMat,PartVect>>::value){
    return _Aih * B;
  }
  else{
    return _loc_solver * B;
  }
}
// Apply function:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Getters][Getters:1]]
PartMat& get_Aih() {
  if(!_is_setup) setup_abstractschwarz();
  return _Aih;
}

const PartMat& get_Aih() const {
  MAPHYSPP_ASSERT(_is_setup, "AbstractSchwarz: calling get_Aih before setup");
  return _Aih;
}
// Getters:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Class footer][Class footer:1]]
}; // class AbstractSchwarz
// Class footer:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*AdditiveSchwarz][AdditiveSchwarz:1]]
template<class PartMat, class PartVect, class LocalSolver>
using AdditiveSchwarz = AbstractSchwarz<PartMat, PartVect, LocalSolver, true>;
// AdditiveSchwarz:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*NeumannNeumann][NeumannNeumann:1]]
template<class PartMat, class PartVect, class LocalSolver, PartOfUnityMethod Di_type = PartOfUnityMethod::matrix>
using NeumannNeumann = AbstractSchwarz<PartMat, PartVect, LocalSolver, false, Di_type>;
// NeumannNeumann:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*RobinRobin][RobinRobin:1]]
template<class PartMat, class PartVect, class LocalSolver>
using RobinRobin = AbstractSchwarz<PartMat, PartVect, LocalSolver, false, PartOfUnityMethod::none, true>;
// RobinRobin:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Traits][Traits:1]]
template<typename T> struct is_abstractschwarz : public std::false_type {};

template<class PartMat, class PartVect, class LocalSolver, bool Assemble, PartOfUnityMethod Di_type, bool TransmissionMatrix>
struct is_abstractschwarz<AbstractSchwarz<PartMat, PartVect, LocalSolver, Assemble, Di_type, TransmissionMatrix>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
