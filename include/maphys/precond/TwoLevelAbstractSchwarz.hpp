// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Header][Header:1]]
#pragma once

#include "maphys/solver/CoarseSolve.hpp"
#include "maphys/solver/BlasSolver.hpp"
#include "maphys/solver/CentralizedSolver.hpp"
#include "maphys/kernel/BlasKernels.hpp"
#include "maphys/linalg/SpectralExtraction.hpp"
#ifdef MAPHYSPP_USE_ARPACK
#include "maphys/linalg/EigenArpackSolver.hpp"
#endif
namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*GenEO space][GenEO space:1]]
// Solve generalized EVD : A U = B U Lam with A symmetric and B spd
// And remove vectors for which eigenvalue lambda >= 1/omega (if omega > 0)
// NB: solver is necessary if arpack is used
template<MPH_Matrix Matrix, MPH_Vector Vector, typename Solver = Identity<Matrix, Vector>,
	 typename Scalar = typename Vector::scalar_type, typename Real = typename Vector::real_type>
std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
genEO_space_gevd(const Matrix& A, const Matrix& B, const int n_v, //In
		 const Real omega = 0, const Real tol = -1){      //Optional

  IndexArray<Real> Lam;
  DenseMatrix<Scalar> U;
#ifdef MAPHYSPP_USE_ARPACK
  static_assert(!std::is_same_v<Solver, Identity<Matrix, Vector>>,
		"genEO_space_gevd: a solver must be passed to use arpack");
  try{

    std::tie(Lam, U) = EigenArpackSolver<DenseMatrix<Scalar>, Matrix, Vector, Solver>::gen_eigh_smallest(A, B, n_v, tol);
  }
  catch(const ArpackNotConverged&){
    MAPHYSPP_WARNING("Arpack error: using lapack dense kernel for gen. evd");
    DenseMatrix<Scalar> dA, dB;
    A.convert(dA);
    B.convert(dB);
    std::tie(Lam, U) = blas_kernels::gen_eigh_smallest(dA, dB, n_v);
  }
#else
  (void) tol;
  static_assert(is_dense<Matrix>::value,
		"genEO_space_gevd: ARPACK is necessary to compute evd with sparse matrix");
  std::tie(Lam, U) = blas_kernels::gen_eigh_smallest(A, B, n_v);
#endif

  if(omega > 0){
    // Remove Lam >= (1 / omega)
    const auto inv_omega = Real{1.0} / omega;
    Size newsize = 0;
    for(Size j = 0; j < Lam.size(); ++j){
      newsize = j;
      if(Lam[j] >= inv_omega) break;
    }
    Lam.erase_after(newsize - 1);

    if(n_cols(U) > newsize){
      DenseMatrix<Scalar> new_U(n_rows(U), newsize);
      for(Size j = 0; j < newsize; ++j){
	new_U.get_vect_view(j) = U.get_vect_view(j);
      }
      U = new_U;
    }
  }

  return std::make_pair(Lam, U);
}

template<MPH_Matrix Matrix, MPH_Vector Vector, typename M1_type, typename Solver = Identity<Matrix, Vector>,
	 typename Scalar = typename Vector::scalar_type, typename Real = typename Vector::real_type>
PartMatrix<DenseMatrix<Scalar>> genEO_space(const PartMatrix<Matrix>& A, M1_type& M1,
					    const int n_v = 5,
					    [[maybe_unused]] const Real alpha = 0,
					    [[maybe_unused]] const Real beta = 0,
					    const Real tol = -1){
  Timer<TIMER_PRECOND> t("GenEO space generation");
  AdditiveSchwarz<PartMatrix<Matrix>, PartVector<Vector>, Identity<Matrix, Vector>> AS;
  NeumannNeumann<PartMatrix<Matrix>, PartVector<Vector>, Identity<Matrix, Vector>> NN;

  if constexpr(!M1_type::is_NN){ NN.setup(A);}
  if constexpr(!M1_type::is_AS){ AS.setup(A);}

  PartMatrix<DenseMatrix<Scalar>> V(A.get_proc());

  const IndexArray<int> local_subdomains = A.get_proc()->get_sd_ids();

  for(const auto sd_id : local_subdomains){
    auto& lV = V.get_local_matrix(sd_id);

    auto v1 = lV;
    auto v2 = lV;
    IndexArray<Real> w1, w2;

    // If M1 is not NeumannNeumann, solve GEVD and crop with alpha
    if constexpr(!M1_type::is_NN){
      const auto& lA = NN.get_Aih().get_local_matrix(sd_id);
      const auto& lB = M1.get_Aih().get_local_matrix(sd_id);
      std::tie(w1, v1) = genEO_space_gevd<Matrix, Vector, Solver>(lA, lB, n_v, alpha, tol);
    }

    // If M1 is not AdditiveSchwarz, solve GEVD and crop with beta
    if constexpr(!M1_type::is_AS){
      const auto& lA = M1.get_Aih().get_local_matrix(sd_id);
      const auto& lB = AS.get_Aih().get_local_matrix(sd_id);
      std::tie(w2, v2) = genEO_space_gevd<Matrix, Vector, Solver>(lA, lB, n_v, beta, tol);
    }

    if constexpr(M1_type::is_AS){ lV = std::move(v1); }
    else if constexpr(M1_type::is_NN){ lV = std::move(v2); }
    else{
      // If two sets of eigenvectors were computed, we need to get the
      // n_v eigenvectors corresponding to the n_v smallest eigenvalues

      IndexArray<Real> evals(w1);
      evals.insert(IndexArray<Real>(w2));
      IndexArray<int> order = evals.argsort();

      const int w1size = static_cast<int>(w1.size());
      const int w2size = static_cast<int>(w2.size());
      const int final_nv = std::min(n_v, w1size + w2size);
      lV = DenseMatrix<Scalar>(n_rows(A.get_local_matrix(sd_id)), final_nv);

      for(int k = 0; k < final_nv; ++k){
	if(order[k] >= w1size){
	  lV.get_vect_view(k) = v2.get_vect_view(order[k] - w1size);
	}
	else{
	  lV.get_vect_view(k) = v1.get_vect_view(order[k]);
	}
      }
    }
  }

  return V;
}
// GenEO space:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Traits definitions and parameters][Traits definitions and parameters:1]]
template<typename T> struct is_pcd_geneo : public std::false_type {};
template<typename T> struct is_pcd_coarse : public std::false_type {};

namespace deflation_params {
CREATE_STRUCT(A)
CREATE_STRUCT(max_deflation_size)
CREATE_STRUCT(alpha)
CREATE_STRUCT(beta)
CREATE_STRUCT(nev)
CREATE_STRUCT(solve_deflation_size)
CREATE_STRUCT(V0)
}
// Traits definitions and parameters:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Class definition][Class definition:1]]
template<MPH_Matrix PartMat,
	 MPH_Vector PartVect,
	 typename M0_type,
	 typename M1_type,
	 bool deflated = true,
	 bool init = false>
class TwoLevelAbstractSchwarz : public LinearOperator<PartMat, PartVect>{
// Class definition:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Attributes][Attributes:1]]
public:
using scalar_type = typename LinearOperator<PartMat, PartVect>::scalar_type;
using real_type = typename LinearOperator<PartMat, PartVect>::real_type;
using matrix_type = PartMat;
using vector_type = PartVect;

private:
using Scalar = scalar_type;
using Real = real_type;
using LocMat = typename PartMat::local_type;
using LocVect = typename PartVect::local_type;

M1_type _M1;
M0_type _M0;
int _max_deflation_size = 10;
int _solve_deflation_size = 6;
int _nev = 3;
Real _alpha = 0;
Real _beta = 0;
Real _tol = -1;
bool _is_setup = false;
bool _init_guess_computed = false;
// Attributes:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Constructor][Constructor:1]]
public:
TwoLevelAbstractSchwarz(){}
TwoLevelAbstractSchwarz(const PartMat& A, int n_vect = 10, Real alpha = 0, Real beta = 0, Real tol = -1) {
  set_n_vect(n_vect);
  set_alpha(alpha);
  set_beta(beta);
  set_tolerance(tol);
  setup(A);
}
// Constructor:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Setup][Setup:1]]
private:
  void _setup(const deflation_params::max_deflation_size<int>&   v) { set_n_vect(v.value); }
  void _setup(const deflation_params::nev<int>&    v) { set_nev(v.value); }
  void _setup(const deflation_params::alpha<Real>& v) { set_alpha(v.value); }
  void _setup(const deflation_params::beta<Real>&  v) { set_beta(v.value); }

  void _setup(const PartMat& A, [[maybe_unused]] bool keep_deflation_space = false){
    _M1.setup(A);
    // Better to test if coarse, but works fow now
    if constexpr(is_pcd_coarse<M0_type>::value) {
      _M0.set_coarse_space(genEO_space<LocMat, LocVect, M1_type, typename M0_type::local_solver_type/*Identity<LocVect, LocVect>*/>
                                       (A, _M1, _max_deflation_size, _alpha, _beta));
      _M0.setup(A);
    }
    else { // Eigen Deflation
      _M0.setup(A, _max_deflation_size, _solve_deflation_size, _nev, keep_deflation_space);
    }
    _is_setup = true;
  }

public:
  void setup(const PartMat& A) { _setup(A); }
  void setup(const PartMat& A, bool keep_deflation_space) {
    _setup(A, keep_deflation_space);
  }
  // Variadic template -> each parameter is a call to the _setup function
  template<typename... Types>
  void setup(const PartMat& A, const Types&... args){
    ( void(_setup(args)), ...);
    _setup(A);
  }
// Setup:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Apply][Apply:1]]
PartVect apply(const PartVect& B){
  MAPHYSPP_ASSERT(_is_setup, "TwoLevelAbstractSchwarz: calling apply before setup");
  if constexpr(deflated){
    MAPHYSPP_ASSERT(_init_guess_computed, "TwoLevelAbstractSchwarz::apply: init guess must have been computed first");
    auto y = _M1.apply(B);
    return y - _M0.project(y);
  }
  else{
    return _M0.apply(B) + _M1.apply(B);
  }
}
// Apply:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Compute initial guess][Compute initial guess:1]]
PartVect init_guess(const PartVect& B){
  MAPHYSPP_ASSERT(_is_setup, "TwoLevelAbstractSchwarz: calling init_guess before setup");
  _init_guess_computed = true;
  if constexpr(deflated){
    // To simplify with C++20 require
    if constexpr (has_init_guess_function<M0_type>::value) return _M0.init_guess(B);
    else return _M0.apply(B);
  }
  else{
    return B * Scalar{0};
  }
}
// Compute initial guess:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Setters][Setters:1]]
void set_n_vect(const int n_vect){
  MAPHYSPP_ASSERT(n_vect > 0, "TwoLevelAbstractSchwarz::set_n_vect: n_vect must be > 0");
  _max_deflation_size = n_vect;
}

void set_n_vect_solve(const int n_vect){
  MAPHYSPP_ASSERT(n_vect > 0, "TwoLevelAbstractSchwarz::set_n_vect_solve: n_vect must be > 0");
  _solve_deflation_size = n_vect;
}

void set_nev(const int n_vect){
  MAPHYSPP_ASSERT(n_vect > 0, "TwoLevelAbstractSchwarz::set_nev: nev must be > 0");
  _nev = n_vect;
}

void set_alpha(const Real alpha){
  MAPHYSPP_ASSERT(alpha >= 0, "TwoLevelAbstractSchwarz::set_alpha: beta must be positive");
  _alpha = alpha;
}

void set_beta(const Real beta){
  MAPHYSPP_ASSERT(beta >= 0, "TwoLevelAbstractSchwarz::set_beta: beta must be positive");
  _beta = beta;
}

void set_tolerance(const Real tol){
  _tol = tol;
}
// Setters:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Getters][Getters:1]]
template<class = std::enable_if<has_spectral_extraction<M0_type>::value>>
[[nodiscard]] SpectralExtraction<PartMat, PartVect>* get_spectral_extraction(){
  return _M0.get_spectral_extraction();
}

[[nodiscard]] M0_type& get_M0(){ return _M0; }
[[nodiscard]] M1_type& get_M1(){ return _M1; }
[[nodiscard]] const M0_type& get_M0() const { return _M0; }
[[nodiscard]] const M1_type& get_M1() const { return _M1; }
// Getters:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Traits][Traits:1]]
}; // class TwoLevelAbstractSchwarz
// Traits:1 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Traits][Traits:2]]
template<typename LocMat, typename LocVect, typename GlobSolver>
struct is_pcd_coarse<CoarseSolve<LocMat, LocVect, GlobSolver>> : public std::true_type {};

template<MPH_Matrix PartMat, MPH_Vector PartVect, typename M1_type, typename LocMat, typename LocVect, typename GlobSolver, bool deflated, bool init>
struct is_pcd_geneo<TwoLevelAbstractSchwarz<PartMat, PartVect, CoarseSolve<LocMat, LocVect, GlobSolver>, M1_type, deflated, init>> : public std::true_type {};

template<MPH_Matrix PartMat, MPH_Vector PartVect, typename M1_type, typename LocMat, typename LocVect, typename GlobSolver, bool deflated, bool init>
struct has_init_guess_function<TwoLevelAbstractSchwarz<PartMat, PartVect, CoarseSolve<LocMat, LocVect, GlobSolver>, M1_type, deflated, init>> : public std::true_type {};
// Traits:2 ends here

// [[file:../../../org/maphys/precond/TwoLevelAbstractSchwarz.org::*Footer][Footer:1]]
} // namespace maphys
// Footer:1 ends here
