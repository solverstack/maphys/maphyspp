// [[file:../../../org/maphys/kernel/ChameleonKernels.org::*Header][Header:1]]
#pragma once
#include <cctype>

#include <chameleon.h>
#include "maphys/utils/Chameleon.hpp"
#include "maphys/kernel/BlasKernels.hpp"
#include "maphys/solver/ChameleonSolver.hpp"

namespace maphys {
  struct chameleon_kernels : blas_kernels {
// Header:1 ends here

// [[file:../../../org/maphys/kernel/ChameleonKernels.org::*Blas 3][Blas 3:1]]
// C <- alpha * opA(A) * opB(B) + beta * C
template<BlasKernelMatrix Matrix, MPH_Scalar Scalar = typename Matrix::scalar_type>
static void gemm(const Matrix& A, const Matrix& B, Matrix& C, char opA = 'N', char opB = 'N', Scalar alpha = 1, Scalar beta = 0){

  using Vector = typename vector_type<Matrix>::type;

  opA = std::toupper(opA);
  opB = std::toupper(opB);
  const int opa_rows = static_cast<int>((opA == 'N') ? n_rows(A) : n_cols(A));
  const int opa_cols = static_cast<int>((opA == 'N') ? n_cols(A) : n_rows(A));
  const int opb_rows = static_cast<int>((opB == 'N') ? n_rows(B) : n_cols(B));
  const int opb_cols = static_cast<int>((opB == 'N') ? n_cols(B) : n_rows(B));

  MAPHYSPP_DIM_ASSERT(opa_cols, opb_rows, "Matrix matrix multiplication AB: nb cols(op(A)) != nb rows(op(B))");
  if(static_cast<int>(n_rows(C)) != opa_rows or static_cast<int>(n_cols(C)) != opb_cols){
    MAPHYSPP_ASSERT(beta == Scalar{0}, "ChameleonKernels::gemm wrong dimensions for C (and beta != 0)");
    C = Matrix(opa_rows, opb_cols);
  }

  const int ldc = static_cast<int>(get_leading_dim(C));

  auto getType = [](Scalar a) {
    if constexpr(is_real<Scalar>::value) {
      return is_precision_double<Scalar>::value ? ChamRealDouble : ChamRealFloat;
    } else {
      return is_precision_double<Scalar>::value ? ChamComplexDouble : ChamComplexFloat;
    }
  };

  auto lap_to_cham = []( const Matrix& A, CHAM_desc_t* chamA ){
    cham_uplo_t uplo;
    if ( A.is_storage_full() ){
      uplo = ChamUpperLower;
    } else {
      uplo = A.is_storage_upper() ? ChamUpper : ChamLower;
    }
    int LDA = static_cast<int>(n_rows(A));
    int err = 0;
    if constexpr(is_real<Scalar>::value) {
      if constexpr(is_precision_double<Scalar>::value) {
        err = CHAMELEON_dlaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
        if (err !=0 ) { return err; }
        return CHAMELEON_dLap2Desc(uplo, (Scalar*)get_ptr(A), LDA, chamA);
      } else {
        CHAMELEON_slaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
        if (err !=0 ) { return err; }
        return CHAMELEON_sLap2Desc(uplo, (Scalar*)get_ptr(A), LDA, chamA);
      }
    } else {
      if constexpr(is_precision_double<Scalar>::value) {
        CHAMELEON_zlaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
        if (err !=0 ) { return err; }
        return CHAMELEON_zLap2Desc(uplo, (Scalar*)get_ptr(A), LDA, chamA);
      } else {
        CHAMELEON_claset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
        if (err !=0 ) { return err; }
        return CHAMELEON_cLap2Desc(uplo, (Scalar*)get_ptr(A), LDA, chamA);
      }
    }
  };

  auto cham_to_lap = []( CHAM_desc_t* chamA, Matrix& A ){
    cham_uplo_t uplo;
    if ( A.is_storage_full() ){
      uplo = ChamUpperLower;
    } else {
      uplo = A.is_storage_upper() ? ChamUpper : ChamLower;
    }
    int LDA = static_cast<int>(n_rows(A));
    if constexpr(is_real<Scalar>::value) {
      if constexpr(is_precision_double<Scalar>::value) {
        return CHAMELEON_dDesc2Lap(uplo, chamA, get_ptr(A), LDA);
      } else {
        return CHAMELEON_sDesc2Lap(uplo, chamA, get_ptr(A), LDA);
      }
    } else {
      if constexpr(is_precision_double<Scalar>::value) {
        return CHAMELEON_zDesc2Lap(uplo, chamA, get_ptr(A), LDA);
      } else {
        return CHAMELEON_cDesc2Lap(uplo, chamA, get_ptr(A), LDA);
      }
    }
  };

  auto cham_gemm = []( cham_trans_t transA, cham_trans_t transB, Scalar alpha, CHAM_desc_t* A, CHAM_desc_t* B, Scalar beta, CHAM_desc_t* C ){
    if constexpr(is_real<Scalar>::value) {
      if constexpr(is_precision_double<Scalar>::value) {
        return CHAMELEON_dgemm_Tile( transA, transB, alpha, A, B, beta, C );
      } else {
        return CHAMELEON_sgemm_Tile( transA, transB, alpha, A, B, beta, C );
      }
    } else {
      if constexpr(is_precision_double<Scalar>::value) {
        return CHAMELEON_zgemm_Tile( transA, transB, alpha, A, B, beta, C );
      } else {
        return CHAMELEON_cgemm_Tile( transA, transB, alpha, A, B, beta, C );
      }
    }
  };

  auto cham_symm = []( cham_side_t side, cham_uplo_t uplo, Scalar alpha, CHAM_desc_t* A, CHAM_desc_t* B, Scalar beta, CHAM_desc_t* C ){
    if constexpr(is_real<Scalar>::value) {
      if constexpr(is_precision_double<Scalar>::value) {
        return CHAMELEON_dsymm_Tile( side, uplo, alpha, A, B, beta, C );
      } else {
        return CHAMELEON_ssymm_Tile( side, uplo, alpha, A, B, beta, C );
      }
    } else {
      if constexpr(is_precision_double<Scalar>::value) {
        return CHAMELEON_zsymm_Tile( side, uplo, alpha, A, B, beta, C );
      } else {
        return CHAMELEON_csymm_Tile( side, uplo, alpha, A, B, beta, C );
      }
    }
  };

  auto cham_hemm = []( cham_side_t side, cham_uplo_t uplo, Scalar alpha, CHAM_desc_t* A, CHAM_desc_t* B, Scalar beta, CHAM_desc_t* C ){
    if constexpr(is_complex<Scalar>::value) {
      if constexpr(is_precision_double<Scalar>::value) {
        return CHAMELEON_zhemm_Tile( side, uplo, alpha, A, B, beta, C );
      } else {
        return CHAMELEON_chemm_Tile( side, uplo, alpha, A, B, beta, C );
      }
    }
  };

  int NB;
  CHAMELEON_Get(CHAMELEON_TILE_SIZE, &NB);

  if(is_storage_full(A) and is_storage_full(B)){
    auto op_blas = [](char op){
      if(op == 'T') return ChamTrans;
      if(op == 'C') return ChamConjTrans;
      return ChamNoTrans;
    };

    const int lda = static_cast<int>(get_leading_dim(A));
    const int ldb = static_cast<int>(get_leading_dim(B));

    /* Initialize specific chameleon structures wrapping data */
    CHAM_desc_t* chamA;
    CHAMELEON_Desc_Create( &chamA, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, lda, opa_cols, 0, 0, opa_rows, opa_cols, 1, 1 );
    CHAM_desc_t* chamB;
    CHAMELEON_Desc_Create( &chamB, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ldb, opb_cols, 0, 0, opa_cols, opb_cols, 1, 1 );
    CHAM_desc_t* chamC;
    CHAMELEON_Desc_Create( &chamC, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ldc, opb_cols, 0, 0, opa_rows, opb_cols, 1, 1 );

    lap_to_cham(A, chamA);
    lap_to_cham(B, chamB);
    lap_to_cham(C, chamC);

    cham_gemm( op_blas(opA), op_blas(opB), alpha, chamA, chamB, beta, chamC );

    cham_to_lap( chamC, C );
  }
  else { // A or B half-stored

    cham_side_t side       = (is_storage_full(B)) ? ChamLeft : ChamRight;
    const Matrix& mat_half = (is_storage_full(B)) ? A : B;
    const Matrix& mat_full = (is_storage_full(B)) ? B : A;

    const int ld_half = static_cast<int>(get_leading_dim(mat_half));
    const int ld_full = static_cast<int>(get_leading_dim(mat_full));

    cham_uplo_t uplo = (is_storage_lower(mat_half)) ? ChamLower : ChamUpper;
    const int M_half = static_cast<int>(n_rows(mat_half));
    const int N_full = static_cast<int>(n_cols(mat_full));

    /* Initialize specific chameleon structures wrapping data */
    CHAM_desc_t* chamA;
    CHAMELEON_Desc_Create( &chamA, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ld_half, M_half, 0, 0, M_half, M_half, 1, 1 );
    CHAM_desc_t* chamB;
    CHAMELEON_Desc_Create( &chamB, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ld_full, N_full, 0, 0, M_half, N_full, 1, 1 );
    CHAM_desc_t* chamC;
    CHAMELEON_Desc_Create( &chamC, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ldc, opb_cols, 0, 0, opa_rows, opb_cols, 1, 1 );

    lap_to_cham(mat_half, chamA);
    lap_to_cham(mat_full, chamB);
    lap_to_cham(C, chamC);

    if(is_symmetric(mat_half)){

      cham_symm( side, uplo, alpha, chamA, chamB, beta, chamC );

    } else if(is_hermitian(mat_half)){

      cham_hemm( side, uplo, alpha, chamA, chamB, beta, chamC );

    } else{
      MAPHYSPP_ASSERT(false, "ChameleonKernels::gemm matrix is half stored but not sym/herm");
    }

    cham_to_lap( chamC, C );

  }
}
// Blas 3:1 ends here

// [[file:../../../org/maphys/kernel/ChameleonKernels.org::*Footer][Footer:1]]
}; // struct chameleon_kernels
} // namespace maphys
// Footer:1 ends here
