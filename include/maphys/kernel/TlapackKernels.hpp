// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Header][Header:1]]
#pragma once
#include <cctype>

// Use span as a sliceable vector of int for tlapack
#include <span>

namespace tlapack {
  template<typename T>
  const std::span<T> slice(const std::span<T>& v, std::pair<int, int> idxs){
    auto& [i, f] = idxs;
    return v.subspan(i, f - i);
  }

  template<typename T>
  std::span<T> slice(std::span<T>& v, std::pair<int, int> idxs){
    auto& [i, f] = idxs;
    return v.subspan(i, f - i);
  }
}

#include <tlapack.hpp>

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Concepts][Concepts:1]]
template<typename matrix>
concept TlapackKernelMatrix = tlapack::concepts::Matrix<matrix> &&
  requires(matrix m)
{
  { is_storage_full(m) }  -> std::same_as<bool>;
  { is_storage_lower(m) } -> std::same_as<bool>;
  { is_storage_upper(m) } -> std::same_as<bool>;
  { is_symmetric(m) }     -> std::same_as<bool>;
  { is_hermitian(m) }     -> std::same_as<bool>;
};
// Concepts:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Concepts][Concepts:2]]
struct tlapack_kernels {
// Concepts:2 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Blas 1][Blas 1:1]]
// X <- alpha * X (vector or matrix)
template<tlapack::concepts::Vector Vector, MPH_Scalar Scalar>
static void scal(Vector& X, Scalar alpha){
  tlapack::scal(alpha, X);
}

// Y <- alpha * X + Y
template<tlapack::concepts::Vector Vector, MPH_Scalar Scalar>
static void axpy(const Vector& X, Vector& Y, Scalar alpha){
  tlapack::axpy(alpha, X, Y);
}

// Return X dot Y
template<tlapack::concepts::Vector Vector>
[[nodiscard]] static MPH_Scalar auto dot(const Vector& X, const Vector& Y){
  return tlapack::dot(X, Y);
}

template<tlapack::concepts::Vector Vector>
[[nodiscard]] static MPH_Real auto norm2_squared(const Vector& X){
  return std::real(tlapack::dot(X, X));
}

template<tlapack::concepts::Vector Vector>
[[nodiscard]] static MPH_Real auto norm2(const Vector& X){
  return tlapack::nrm2(X);
}
// Blas 1:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Blas 2][Blas 2:1]]
// Y = alpha * op(A) * X + beta * Y
template<TlapackKernelMatrix Matrix, tlapack::concepts::Vector Vector, MPH_Scalar Scalar = typename Vector::scalar_type>
static void gemv(const Matrix& A, const Vector& X, Vector& Y, char opA = 'N', Scalar alpha = 1, Scalar beta = 0){
  const Size M = tlapack::nrows(A);
  const Size N = tlapack::ncols(A);
  opA = std::toupper(opA);

  const Size opa_rows = (opA == 'N') ? M : N;
  const Size opa_cols = (opA == 'N') ? N : M;

  MAPHYSPP_DIM_ASSERT(tlapack::nrows(X), opa_cols, "TlapackSolver::gemv dimensions of A and X do not match");
  if(tlapack::nrows(Y) != opa_rows){
    MAPHYSPP_ASSERT(beta == Scalar{0}, "TlapackSolver::gemv wrong dimensions for Y (and beta != 0)");
    Y = Vector(opa_rows); // Not captured by the concept of tlapack Vector
  }

  if(is_storage_full(A)){
    tlapack::Op op = tlapack::Op::NoTrans;
    if(opA == 'T') op = tlapack::Op::Trans;
    if(opA == 'C') op = tlapack::Op::ConjTrans;
    tlapack::gemv(op, alpha, A, X, beta, Y);
  } else {
    tlapack::Uplo uplo = (is_storage_lower(A)) ? tlapack::Uplo::Lower : tlapack::Uplo::Upper;
    if(is_symmetric(A)){
      tlapack::symv(uplo, alpha, A, X, beta, Y);
    }
    else if(is_hermitian(A)){
      if constexpr(is_complex<Scalar>::value){
	tlapack::hemv(uplo, alpha, A, X, beta, Y);
      }
    }
    else {
      MAPHYSPP_ASSERT(false, "TlapackSolver::gemv matrix A is half stored but not sym/herm");
    }
  }
}
// Blas 2:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Blas 3][Blas 3:1]]
// C <- alpha * opA(A) * opB(B) + beta * C
template<TlapackKernelMatrix Matrix, MPH_Scalar Scalar = typename Matrix::scalar_type>
static void gemm(const Matrix& A, const Matrix& B, Matrix& C, char opA = 'N', char opB = 'N', Scalar alpha = 1, Scalar beta = 0){
  opA = std::toupper(opA);
  opB = std::toupper(opB);
  const Size opa_rows = (opA == 'N') ? tlapack::nrows(A) : tlapack::ncols(A);
  const Size opa_cols = (opA == 'N') ? tlapack::ncols(A) : tlapack::nrows(A);
  const Size opb_rows = (opB == 'N') ? tlapack::nrows(B) : tlapack::ncols(B);
  const Size opb_cols = (opB == 'N') ? tlapack::ncols(B) : tlapack::nrows(B);

  MAPHYSPP_DIM_ASSERT(opa_cols, opb_rows, "TlapackSolver::gemm dimensions of A and B do not match");
  if(tlapack::nrows(C) != opa_rows or tlapack::ncols(C) != opb_cols){
    MAPHYSPP_ASSERT(beta == Scalar{0}, "TlapackSolver::gemm wrong dimensions for C (and beta != 0)");
    C = Matrix(opa_rows, opb_cols);
  }

  if(is_storage_full(A) and is_storage_full(B)){
    auto op_blas = [](char op){
      if(op == 'T') return tlapack::Op::Trans;
      if(op == 'C') return tlapack::Op::ConjTrans;
      return tlapack::Op::NoTrans;
    };

    tlapack::gemm(op_blas(opA), op_blas(opB), alpha, A, B, beta, C);
  }
  else {

    const Matrix& sym_mat  = (is_storage_full(A)) ? B : A;
    const Matrix& full_mat = (is_storage_full(A)) ? A : B;
    tlapack::Side side     = (is_storage_full(A)) ? tlapack::Side::Right : tlapack::Side::Left;
    tlapack::Uplo uplo     = (is_storage_lower(sym_mat)) ? tlapack::Uplo::Lower : tlapack::Uplo::Upper;

    if(is_symmetric(sym_mat)){
      tlapack::symm(side, uplo, alpha, sym_mat, full_mat, beta, C);
    }
    else if(is_hermitian(sym_mat)){
      if constexpr(is_complex<Scalar>::value){
	tlapack::hemm(side, uplo, alpha, sym_mat, full_mat, beta, C);
      }
    }
    else {
      MAPHYSPP_ASSERT(false, "TlapackSolver::gemm matrix is half stored but not sym/herm");
    }
  }
}
// Blas 3:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Eigenvalue decomposition][Eigenvalue decomposition:1]]
template<TlapackKernelMatrix Matrix,
	 TlapackKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Complex Complex = typename ComplexMatrix::scalar_type>
[[nodiscard]] static std::pair<IndexArray<Complex>, ComplexMatrix>
eig(const Matrix&) {
  MAPHYSPP_ASSERT(false, "Tlapack::eig not implemented");
  return std::pair<IndexArray<Complex>, ComplexMatrix>();
}

template<TlapackKernelMatrix Matrix,
	 TlapackKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Complex Complex = typename ComplexMatrix::scalar_type>
[[nodiscard]] static IndexArray<Complex>
eigvals(const Matrix&) {
  MAPHYSPP_ASSERT(false, "Tlapack::eigvals not implemented");
  return IndexArray<Complex>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
eigh(const Matrix&) {
  MAPHYSPP_ASSERT(false, "Tlapack::eigh not implemented");
  return std::pair<IndexArray<Real>, Matrix>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static IndexArray<Real>
eigvalsh(const Matrix&) {
  MAPHYSPP_ASSERT(false, "Tlapack::eigvalsh not implemented");
  return IndexArray<Real>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
eigh_select(const Matrix&, int, int) {
  MAPHYSPP_ASSERT(false, "Tlapack::eigh_select not implemented");
  return std::pair<IndexArray<Real>, DenseMatrix<Scalar>>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
eigh_smallest(const Matrix&, int) {
  MAPHYSPP_ASSERT(false, "Tlapack::eigh_smallest not implemented");
  return std::pair<IndexArray<Real>, DenseMatrix<Scalar>>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
eigh_largest(const Matrix&, int) {
  MAPHYSPP_ASSERT(false, "Tlapack::eigh_largest not implemented");
  return std::pair<IndexArray<Real>, DenseMatrix<Scalar>>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
gen_eigh_select(const Matrix&, const Matrix&, int, int) {
  MAPHYSPP_ASSERT(false, "Tlapack::gen_eigh_select not implemented");
  return std::pair<IndexArray<Real>, Matrix>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
gen_eigh_smallest(const Matrix&, const Matrix&, int) {
  MAPHYSPP_ASSERT(false, "Tlapack::gen_eigh_smallest not implemented");
  return std::pair<IndexArray<Real>, Matrix>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
gen_eigh_largest(const Matrix&, const Matrix&, int) {
  MAPHYSPP_ASSERT(false, "Tlapack::gen_eigh_largest not implemented");
  return std::pair<IndexArray<Real>, Matrix>();
}

template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::tuple<Matrix, IndexArray<Real>, Matrix>
svd(const Matrix&, bool reduced = true) {
  (void) reduced;
  MAPHYSPP_ASSERT(false, "Tlapack::svd not implemented");
  return std::tuple<Matrix, IndexArray<Real>, Matrix>();
}

// Returns U and sigmas only
template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<Matrix, IndexArray<Real>>
svd_left_sv(const Matrix&, bool reduced = true) {
  (void) reduced;
  MAPHYSPP_ASSERT(false, "Tlapack::svd_left_sv not implemented");
  return std::pair<Matrix, IndexArray<Real>>();
}

// Returns sigmas and Vt only
template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
svd_right_sv(const Matrix&, bool reduced = true) {
  (void) reduced;
  MAPHYSPP_ASSERT(false, "Tlapack::svd_right_sv not implemented");
  return std::pair<IndexArray<Real>, Matrix>();
}

// Returns only singular values
template<TlapackKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static IndexArray<Real>
svdvals(const Matrix&) {
  MAPHYSPP_ASSERT(false, "Tlapack::svdvals not implemented");
  return std::pair<IndexArray<Real>, Matrix>();
}
// Eigenvalue decomposition:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Footer][Footer:1]]
}; // struct tlapack_kernels
} // namespace maphys
// Footer:1 ends here
