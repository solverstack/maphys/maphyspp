// [[file:../../../org/maphys/kernel/BlasKernels.org::*Header][Header:1]]
#pragma once

#include <lapack.hh>
#include <blas.hh>
#include <cctype>
#include "maphys/interfaces/linalg_concepts.hpp"

namespace maphys {
// Header:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Concepts][Concepts:1]]
template<typename vect>
concept BlasKernelVector = MPH_Scalar<typename vect::scalar_type> &&
  MPH_Real<typename vect::real_type> &&
  requires(vect v, int i)
{
  { size(v) }          -> std::integral;
  { get_increment(v) } -> std::integral;
  { get_ptr(v) } -> std::convertible_to<typename vect::scalar_type *>;
  { vect(i) } -> std::convertible_to<vect>;
};

template<typename matrix>
concept BlasKernelMatrix = MPH_Scalar<typename matrix::scalar_type> &&
  MPH_Real<typename matrix::real_type> &&
  requires(matrix m)
{
  { n_rows(m) }            -> std::integral;
  { n_cols(m) }            -> std::integral;
  { get_leading_dim(m) }   -> std::integral;
  { is_storage_full(m) }  -> std::same_as<bool>;
  { is_storage_lower(m) } -> std::same_as<bool>;
  { is_storage_upper(m) } -> std::same_as<bool>;
  { is_symmetric(m) }     -> std::same_as<bool>;
  { is_hermitian(m) }     -> std::same_as<bool>;
  { get_ptr(m) } -> std::convertible_to<typename matrix::scalar_type *>;
};
// Concepts:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Concepts][Concepts:2]]
struct blas_kernels {
// Concepts:2 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Blas 1][Blas 1:1]]
// X <- alpha * X (vector or matrix)
// X (vector) may not be contiguous in memory (X has increment != 1)
template<BlasKernelVector Vector, MPH_Scalar Scalar>
static void scal(Vector& X, Scalar alpha){
  const Blas_int M = static_cast<Blas_int>(size(X));
  blas::scal(M, alpha, get_ptr(X), get_increment(X));
}

// Y <- alpha * X + Y
// X and Y (vector) may not be contiguous in memory (increment != 1)
template<BlasKernelVector Vector, MPH_Scalar Scalar>
static void axpy(const Vector& X, Vector& Y, Scalar alpha){
  MAPHYSPP_DIM_ASSERT(size(X), size(Y), "BlasKernels::axpy: different row numbers.");
  blas::axpy(static_cast<Blas_int>(size(X)), alpha, get_ptr(X), get_increment(X), get_ptr(Y), get_increment(Y));
}

// Return X dot Y
// X and Y may not be contiguous in memory (increment != 1)
template<BlasKernelVector Vect1, BlasKernelVector Vect2, MPH_Scalar Scalar = typename Vect1::scalar_type>
static Scalar dot(const Vect1& X, const Vect2& Y){
  const Blas_int Mx = static_cast<Blas_int>(size(X));
  const Blas_int My = static_cast<Blas_int>(size(Y));
  MAPHYSPP_DIM_ASSERT(Mx, My, "BlasKernels::dot: different row numbers.");
  return blas::dot(Mx, get_ptr(X), get_increment(X), get_ptr(Y), get_increment(Y));
}

template<BlasKernelVector Vector, typename Real = typename Vector::real_type>
static Real norm2_squared(const Vector& X){
  const Blas_int M = static_cast<Blas_int>(size(X));
  return std::real(blas::dot (M, get_ptr(X), get_increment(X), get_ptr(X), get_increment(X)));
}

template<BlasKernelVector Vector, typename Real = typename Vector::real_type>
static Real norm2(const Vector& X){
  return blas::nrm2(size(X), get_ptr(X), get_increment(X));
}
// Blas 1:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Blas 2][Blas 2:1]]
// Y = alpha * op(A) * X + beta * Y
template<BlasKernelMatrix Matrix, BlasKernelVector Vect1, BlasKernelVector Vect2, MPH_Scalar Scalar = typename Matrix::scalar_type>
static void gemv(const Matrix& A, const Vect1& X, Vect2& Y, char opA = 'N', Scalar alpha = 1, Scalar beta = 0){
  const Blas_int M = static_cast<Blas_int>(n_rows(A));
  const Blas_int N = static_cast<Blas_int>(n_cols(A));
  const Blas_int A_ld = static_cast<Blas_int>(get_leading_dim(A));
  opA = std::toupper(opA);

  const Size opa_rows = (opA == 'N') ? M : N;
  const Size opa_cols = (opA == 'N') ? N : M;

  MAPHYSPP_DIM_ASSERT(static_cast<Blas_int>(size(X)), opa_cols, "BlasKernels::gemv dimensions of A and X do not match");
  if(size(Y) != opa_rows){
    MAPHYSPP_ASSERT(beta == Scalar{0}, "BlasKernels::gemv wrong dimensions for Y (and beta != 0)");
    Y = Vect2(opa_rows);
  }

  if(is_storage_full(A)){
    blas::Op op = blas::Op::NoTrans;
    if(opA == 'T') op = blas::Op::Trans;
    if(opA == 'C') op = blas::Op::ConjTrans;
    blas::gemv(blas::Layout::ColMajor,
	       op, opa_rows, opa_cols,
	       alpha,
	       get_ptr(A), A_ld,
	       get_ptr(X), 1,
	       beta,
	       get_ptr(Y), 1);
  }
  else {
    blas::Uplo uplo = (is_storage_lower(A)) ? blas::Uplo::Lower : blas::Uplo::Upper;
    if(is_symmetric(A)){
      blas::symv(blas::Layout::ColMajor, uplo,
		 N,
		 alpha,
		 get_ptr(A), A_ld,
		 get_ptr(X), 1,
		 beta,
		 get_ptr(Y), 1);
    }
    else if(is_hermitian(A)){
      if constexpr(is_complex<Scalar>::value){
	blas::hemv(blas::Layout::ColMajor, uplo,
		   N,
		   alpha,
		   get_ptr(A), A_ld,
		   get_ptr(X), 1,
		   beta,
		   get_ptr(Y), 1);
      }
    }
    else {
      MAPHYSPP_ASSERT(false, "BlasKernels::gemv matrix A is half stored but not sym/herm");
    }
  }
}
// Blas 2:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Blas 3][Blas 3:1]]
// C <- alpha * opA(A) * opB(B) + beta * C
template<BlasKernelMatrix Mat1, BlasKernelMatrix Mat2, BlasKernelMatrix Mat3, MPH_Scalar Scalar = typename Mat1::scalar_type>
static void gemm(const Mat1& A, const Mat2& B, Mat3& C, char opA = 'N', char opB = 'N', Scalar alpha = 1, Scalar beta = 0){
  opA = std::toupper(opA);
  opB = std::toupper(opB);
  const Blas_int opa_rows = static_cast<Blas_int>((opA == 'N') ? n_rows(A) : n_cols(A));
  const Blas_int opa_cols = static_cast<Blas_int>((opA == 'N') ? n_cols(A) : n_rows(A));
  const Blas_int opb_rows = static_cast<Blas_int>((opB == 'N') ? n_rows(B) : n_cols(B));
  const Blas_int opb_cols = static_cast<Blas_int>((opB == 'N') ? n_cols(B) : n_rows(B));

  MAPHYSPP_DIM_ASSERT(opa_cols, opb_rows, "Matrix matrix multiplication AB: nb cols(op(A)) != nb rows(op(B))");
  if(static_cast<Blas_int>(n_rows(C)) != opa_rows or static_cast<Blas_int>(n_cols(C)) != opb_cols){
    MAPHYSPP_ASSERT(beta == Scalar{0}, "BlasKernels::gemm wrong dimensions for C (and beta != 0)");
    C = Mat3(opa_rows, opb_cols);
  }

  const Blas_int ldc = static_cast<Blas_int>(get_leading_dim(C));

  if(is_storage_full(A) and is_storage_full(B)){
    auto op_blas = [](char op){
      if(op == 'T') return blas::Op::Trans;
      if(op == 'C') return blas::Op::ConjTrans;
      return blas::Op::NoTrans;
    };

    const Blas_int lda = static_cast<Blas_int>(get_leading_dim(A));
    const Blas_int ldb = static_cast<Blas_int>(get_leading_dim(B));

    blas::gemm(blas::Layout::ColMajor, op_blas(opA), op_blas(opB),
	       opa_rows, opb_cols, opa_cols, alpha,
	       get_ptr(A), lda,
	       get_ptr(B), ldb,
	       beta,
	       get_ptr(C), ldc);
  }
  else { // A or B half-stored

    blas::Side side        = (is_storage_full(B)) ? blas::Side::Left : blas::Side::Right;
    const auto& mat_half = (is_storage_full(B)) ? A : B;
    const auto& mat_full = (is_storage_full(B)) ? B : A;

    const Blas_int ld_half = static_cast<Blas_int>(get_leading_dim(mat_half));
    const Blas_int ld_full = static_cast<Blas_int>(get_leading_dim(mat_full));

    blas::Uplo uplo = (is_storage_lower(mat_half)) ? blas::Uplo::Lower : blas::Uplo::Upper;
    const Blas_int M_half = static_cast<Blas_int>(n_rows(mat_half));
    const Blas_int N_full = static_cast<Blas_int>(n_cols(mat_full));

    if(is_symmetric(mat_half)){
      blas::symm(blas::Layout::ColMajor, side, uplo,
		 M_half, N_full, alpha,
		 get_ptr(mat_half), ld_half,
		 get_ptr(mat_full), ld_full,
		 beta,
		 get_ptr(C), ldc);
    }
    else if(is_hermitian(mat_half)){

      if constexpr(is_complex<Scalar>::value){
	blas::hemm(blas::Layout::ColMajor, side, uplo,
		   M_half, N_full, alpha,
		   get_ptr(mat_half), ld_half,
		   get_ptr(mat_full), ld_full,
		   beta,
		   get_ptr(C), ldc);
      }
    }
    else{
      MAPHYSPP_ASSERT(false, "BlasKernels::gemm matrix is half stored but not sym/herm");
    }
  }
}
// Blas 3:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Eigenvalue decomposition][Eigenvalue decomposition:1]]
// Low level function
template<BlasKernelMatrix Matrix,
	 BlasKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Complex Complex = typename ComplexMatrix::scalar_type>
[[nodiscard]] static std::pair<IndexArray<Complex>, ComplexMatrix>
_eig_geev(Matrix A, bool compute_evectors) {
  //NB: A is copied because it is overwritten by the lapack routine

  //int64_t geev(
  //		 lapack::Job jobvl, lapack::Job jobvr, int64_t n,
  //		 float* A, int64_t lda,
  //		 std::complex<float>* W,
  //		 float* VL, int64_t ldvl,
  //		 float* VR, int64_t ldvr );

  Blas_int n = static_cast<Blas_int>(n_rows(A));
  IndexArray<Complex> eigenvalues(n_rows(A));
  Scalar * Aptr = get_ptr(A);
  Complex * Wptr = &eigenvalues[0];
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(A));
  Blas_int err;

  lapack::Job jobvr;
  ComplexMatrix U;
  Matrix U_real;
  Scalar * Uptr;
  Blas_int ldu;

  if(compute_evectors){
    jobvr = lapack::Job::Vec;
    if constexpr(is_real<Scalar>::value){
      U_real = Matrix(n_rows(A), n_rows(A));
      Uptr = get_ptr(U_real);
      ldu = static_cast<Blas_int>(get_leading_dim(U_real));
    } else{
      U = ComplexMatrix(n_rows(A), n_rows(A));
      Uptr = get_ptr(U);
      ldu = static_cast<Blas_int>(get_leading_dim(U));
    }
  } else { // Compute only eigenvalues
    jobvr = lapack::Job::NoVec;
    Uptr = nullptr;
    ldu = Blas_int{1};
  }

  err = lapack::geev(lapack::Job::NoVec, jobvr, n, Aptr, lda,
		     Wptr, /*vl*/nullptr, /*ldvl*/1, Uptr, ldu);

  if(err != 0) std::cerr << "Error in lapack::geev, returned with value " << err << '\n';

  if(compute_evectors){
    // W: (d/s arithmetic) is complex
    // Complex conjugate pairs of eigenvalues appear consecutively
    // with the eigenvalue having the positive imaginary part
    // first.

    // VR: (d/s arithmetic) is real
    // If the j-th eigenvalue is real, then v(j) = VR(:,j),
    // the j-th column of VR.
    // If the j-th and (j+1)-st eigenvalues form a complex
    // conjugate pair, then v(j) = VR(:,j) + i*VR(:,j+1) and
    // v(j+1) = VR(:,j) - i*VR(:,j+1)

    // For float/double -> change from the lapack output format to the
    // true matrix of complex eigenvectors
    if constexpr(is_real<Scalar>::value){
      U = ComplexMatrix(n_rows(A), n_rows(A));
      for(Blas_int k = 0; k < n; ++k){
	const Complex& ev = eigenvalues[k];
	const auto Urk = U_real.get_vect_view(k);
	auto Uk = U.get_vect_view(k);
	if(ev.imag() == 0){
	  for(Blas_int j = 0; j < n; ++j){
	    Uk[j] = Complex{Urk[j], 0};
	  }
	} else {
	  const Complex cplx_i{0, 1};
	  const auto Urkp1 = U_real.get_vect_view(k+1);
	  auto Ukp1 = U.get_vect_view(k+1);

	  for(Blas_int ii = 0; ii < n; ++ii){
	    Uk[ii] = Urk[ii] + cplx_i * Urkp1[ii];
	    Ukp1[ii] = Urk[ii] - cplx_i * Urkp1[ii];
	  }

	  k++;
	}
      }
    }
  } // if compute_evectors

  return std::make_pair(std::move(eigenvalues), std::move(U));
}

template<BlasKernelMatrix Matrix,
	 BlasKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Complex Complex = typename ComplexMatrix::scalar_type>
[[nodiscard]] static  std::pair<IndexArray<Complex>, ComplexMatrix>
eig(const Matrix& A) {
  return _eig_geev<Matrix, ComplexMatrix, Scalar, Complex>(A, true);
}

template<BlasKernelMatrix Matrix,
	 BlasKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Complex Complex = typename ComplexMatrix::scalar_type>
[[nodiscard]] static  IndexArray<Complex>
eigvals(const Matrix& A) {
  auto [eigenvalues, eigenvectors] = _eig_geev<Matrix, ComplexMatrix, Scalar, Complex>(A, false);
  return eigenvalues;
}
// Eigenvalue decomposition:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Eigenvalue decomposition][Eigenvalue decomposition:2]]
// Low level function
template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, Matrix>
_eigh_heevr(Matrix A, bool compute_evectors) {
  //NB: A is copied because it is overwritten by the lapack routine

  //int64_t heevr(
  //  lapack::Job jobz, lapack::Range range, lapack::Uplo uplo, int64_t n,
  //  std::complex<double>* A, int64_t lda, double vl, double vu, int64_t il, int64_t iu, double abstol,
  //  int64_t* m,
  //  double* W,
  //  std::complex<double>* Z, int64_t ldz,
  //  int64_t* isuppz );

  IndexArray<Real> eigenvalues(n_rows(A));
  Matrix U;

  lapack::Range range = lapack::Range::All; // Compute all eigenvalues and eigenvectors
  lapack::Uplo uplo = (is_storage_lower(A)) ? lapack::Uplo::Lower : lapack::Uplo::Upper;
  Blas_int n = static_cast<Blas_int>(n_rows(A));
  Scalar * Aptr = get_ptr(A);
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(A));
  Real vl{1}, vu{1}; // Unused by lapack
  Blas_int il{1}, iu{1}; // Unused by lapack
  Real abs_tol = is_precision_double<Scalar>::value ? 1e-14 : 5e-6;
  Blas_int nv_found; // "m"
  Real * Wptr = &eigenvalues[0];
  IndexArray<Blas_int> isuppz(2 * n);
  Blas_int err;

  if(compute_evectors){
    lapack::Job jobz = lapack::Job::Vec; // Compute eigenvalues and eigenvectors
    U = Matrix(n_rows(A), n_rows(A));
    Blas_int ldz = static_cast<Blas_int>(get_leading_dim(U));
    Scalar * Zptr = U.get_ptr();
    err = lapack::heevr(jobz, range, uplo, n, Aptr, lda, vl, vu, il, iu, abs_tol, &nv_found, Wptr, Zptr, ldz, &isuppz[0]);
  } else {
    lapack::Job jobz = lapack::Job::NoVec; // Compute only eigenvalues
    err = lapack::heevr(jobz, range, uplo, n, Aptr, lda, vl, vu, il, iu, abs_tol, &nv_found, Wptr, nullptr, 1, &isuppz[0]);
  }

  if(err != 0) std::cerr << "Error in lapack::heerv, returned with value " << err << '\n';

  return std::make_pair(eigenvalues, U);
}

template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, Matrix>
eigh(const Matrix& A) {
  return _eigh_heevr(A, true);
}

template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  IndexArray<Real>
eigvalsh(const Matrix& A) {
  auto [eigenvalues, eigenvectors] = _eigh_heevr(A, false);
  return eigenvalues;
}
// Eigenvalue decomposition:2 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Eigenvalue decomposition for selected eigenvalues][Eigenvalue decomposition for selected eigenvalues:1]]
// NB: first and last idx start at 0
// Eigenvalues are computed in the range [first_idx, last_idx]
// Low level funtion
template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, Matrix>
_eigh_heevx(Matrix A, int first_idx, int last_idx) {
  //NB: A is copied because it is overwritten by the lapack routine
  MAPHYSPP_ASSERT(last_idx >= first_idx, "BlasSolver::_eig_heevx parameter order error");
  MAPHYSPP_ASSERT(first_idx >= 0, "BlasSolver::_eig_heevx first index must be positive");

  int n_v = last_idx - first_idx + 1;

  if((n_rows(A) == 0) or (n_v == 0)) return std::make_pair(IndexArray<Real>(), Matrix());

  IndexArray<Real> eigenvalues(n_rows(A));

  if(static_cast<int>(n_rows(A)) < n_v){
    std::string warn_mess("Asking for ");
    warn_mess += std::to_string(n_v) + std::string(" eigenvectors in matrix of size ") + std::to_string(n_rows(A));
    MAPHYSPP_WARNING(warn_mess);
    first_idx = 0;
    last_idx = n_rows(A) - 1;
    n_v = last_idx - first_idx + 1;
  }

  Matrix U(n_rows(A), n_v);

  //int64_t heevx(
  //  lapack::Job jobz, lapack::Range range, lapack::Uplo uplo, int64_t n,
  //  std::complex<float>* A, int64_t lda, float vl, float vu, int64_t il, int64_t iu, float abstol,
  //  int64_t* m,
  //  float* W,
  //  std::complex<float>* Z, int64_t ldz,
  //  int64_t* ifail );

  lapack::Job jobz = lapack::Job::Vec; // Compute eigenvalues and eigenvectors
  lapack::Range range = lapack::Range::Index; // the IL-th through IU-th eigenvalues will be found
  lapack::Uplo uplo = is_storage_lower(A) ? lapack::Uplo::Lower : lapack::Uplo::Upper;
  Blas_int n = static_cast<Blas_int>(n_rows(A));
  Scalar * Aptr = get_ptr(A);
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(A));
  Real vl = 0; // Not used for range = 'I'
  Real vu = 0; // Not used for range = 'I'
  Blas_int il = static_cast<Blas_int>(first_idx + 1); //Starts at 1
  Blas_int iu = static_cast<Blas_int>(last_idx + 1);
  Real tol = is_precision_double<Scalar>::value ? 1e-12 : 1e-6;

  Blas_int nv_found;
  Real * Wptr = eigenvalues.data();
  Scalar * Zptr = get_ptr(U);
  Blas_int ldz = n;
  std::vector<Blas_int> ifail(n);

  Blas_int err = lapack::heevx(jobz, range, uplo, n, Aptr, lda,
			       vl, vu, il, iu, tol, &nv_found,
			       Wptr, Zptr, ldz, ifail.data());

  // Some error handling
  if(err != 0){
    std::string routine_name = std::string("lapack::hegvx");
    std::string error_message = routine_name + std::string(" error : ");
    if(err < 0){
      error_message += std::string("illegal value for parameter ") + std::to_string(-err);
    }
    else if(err <= n){
      error_message += std::string("failed to converge for ") + std::to_string(err) + std::string(" eigenvectors");
    }
    else{
      error_message = std::string("unkown error (memory corruption?), returned err = ") + std::to_string(err);
    }
    MAPHYSPP_ASSERT(err == 0, error_message);
  }

  eigenvalues.resize(n_v);

  constexpr const Real prune_tol = is_precision_double<Scalar>::value ? 1e-15 : 5e-7;
  for(Size j = 0; j < n_cols(U); ++j){
    for(Size i = 0; i < n_rows(U); ++i){
      if(std::abs(U(i, j)) < prune_tol) U(i,j) = Scalar{0};
    }
  }

  return std::make_pair(eigenvalues, U);
}

template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
eigh_select(const Matrix& A, int first_idx, int last_idx) {
  return _eigh_heevx<Matrix, Scalar, Real>(A, first_idx, last_idx);
}

template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
eigh_smallest(const Matrix& A, int n_v) {
  return _eigh_heevx<Matrix, Scalar, Real>(A, 0, n_v - 1);
}

template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
eigh_largest(const Matrix& A, int n_v) {
  int size_A = static_cast<int>(n_rows(A));
  return _eigh_heevx<Matrix, Scalar, Real>(A, size_A - n_v, size_A - 1);
}
// Eigenvalue decomposition for selected eigenvalues:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Generalized eigenvalue decomposition][Generalized eigenvalue decomposition:1]]
// Eigenvalues are computed in the range [first_idx, last_idx]
template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, Matrix>
_gen_eigh_hegvx(Matrix A, Matrix B, int first_idx, int last_idx) {
  //NB: A and B are taken by copy because they are overwritten by the lapack routine
  MAPHYSPP_ASSERT(last_idx >= first_idx, "BlasSolver::_eig_heevx parameter order error");
  MAPHYSPP_ASSERT(first_idx >= 0, "BlasSolver::_eig_heevx first index must be positive");

  int n_v = last_idx - first_idx + 1;

  Timer<TIMER_EVD> t("lapack_GEVD_pb");

  if(n_rows(A) == 0) return std::pair<IndexArray<Real>, Matrix>();
  IndexArray<Real> eigenvalues(n_rows(A));

  if(static_cast<int>(n_rows(A)) < n_v){
    std::string warn_mess("Asking for ");
    warn_mess += std::to_string(n_v) + std::string(" eigenvectors in matrix of size ") + std::to_string(n_rows(A));
    MAPHYSPP_WARNING(warn_mess);
    first_idx = 0;
    last_idx = n_rows(A) - 1;
    n_v = last_idx - first_idx + 1;
  }

  Matrix U(n_rows(A), n_v);

  Blas_int itype = 1; // solve A . u = \lambda B u
  lapack::Job jobz = lapack::Job::Vec; // Compute eigenvalues and eigenvectors
  lapack::Range range = lapack::Range::Index; // the IL-th through IU-th eigenvalues will be found
  lapack::Uplo uplo = lapack::Uplo::Upper;
  if(is_storage_lower(A)){
    uplo = lapack::Uplo::Lower;
    MAPHYSPP_ASSERT(!is_storage_upper(B), "lapack::sy/he gvx : A and B must have same storage");
  }
  else if(is_storage_upper(A)){
    MAPHYSPP_ASSERT(!is_storage_lower(B), "lapack::sy/he gvx : A and B must have same storage");
  }

  Blas_int n = static_cast<Blas_int>(n_rows(A));
  Scalar * Aptr = get_ptr(A);
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(A));
  Scalar * Bptr = get_ptr(B);
  Blas_int ldb = static_cast<Blas_int>(get_leading_dim(B));
  Real vl = 0; // Not used for range = 'I'
  Real vu = 0; // Not used for range = 'I'
  Blas_int il = static_cast<Blas_int>(first_idx + 1); //Starts at 1
  Blas_int iu = static_cast<Blas_int>(last_idx + 1);
  Real tol = is_precision_double<Scalar>::value ? 1e-12 : 1e-6;

  Blas_int nv_found;
  Real * W = eigenvalues.data();
  Scalar * Z = get_ptr(U);
  Blas_int ldz = n;
  std::vector<Blas_int> ifail(n);
  Blas_int err;

  err = lapack::hegvx(itype, jobz, range, uplo, n, Aptr, lda,
		      Bptr, ldb, vl, vu, il, iu, tol, &nv_found,
		      W, Z, ldz, ifail.data());

  // Some error handling
  if(err != 0){
    std::string routine_name = is_real<Scalar>::value ? std::string("lapack::sygvx") : std::string("lapack::hegvx");
    std::string error_message = routine_name + std::string(" error : ");
    if(err < 0){
      error_message += std::string("illegal value for parameter ") + std::to_string(-err);
    }
    else if(err <= n){
      error_message += std::string("failed to converge for ") + std::to_string(err) + std::string(" eigenvectors");
    }
    else if(err <= 2*n){
      error_message += std::string("leading minor of order ") + std::to_string(err - n) + std::string(" of B is not positive definite");
    }
    else{
      error_message = std::string("unkown error (memory corruption?), returned err = ") + std::to_string(err);
    }
    MAPHYSPP_ASSERT(err == 0, error_message);
  }

  eigenvalues.resize(n_v);

  constexpr const Real prune_tol = is_precision_double<Scalar>::value ? 1e-15 : 5e-7;
  for(Size j = 0; j < n_cols(U); ++j){
    for(Size i = 0; i < n_rows(U); ++i){
      if(std::abs(U(i, j)) < prune_tol) U(i,j) = Scalar{0};
    }
  }

  return std::make_pair(eigenvalues, U);
}

template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, Matrix>
gen_eigh_select(const Matrix& A, const Matrix& B, int first_idx, int last_idx) {
  return _gen_eigh_hegvx<Matrix, Scalar, Real>(A, B, first_idx, last_idx);
}

template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, Matrix>
gen_eigh_smallest(const Matrix& A, const Matrix& B, int n_v) {
  return _gen_eigh_hegvx<Matrix, Scalar, Real>(A, B, 0, n_v - 1);
}

template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, Matrix>
gen_eigh_largest(const Matrix& A, const Matrix& B, int n_v) {
  int size_A = static_cast<int>(n_rows(A));
  return _gen_eigh_hegvx<Matrix, Scalar, Real>(A, B, size_A - n_v, size_A - 1);
}
// Generalized eigenvalue decomposition:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Singular value decomposition][Singular value decomposition:1]]
template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::tuple<Matrix, IndexArray<Real>, Matrix>
_svd_gesvd(Matrix A, bool compute_u = true, bool compute_vt = true, bool reduced = true) {
  //NB: A is copied because it is overwritten by the lapack routine
  //int64_t gesvd(
  //  lapack::Job jobu, lapack::Job jobvt, int64_t m, int64_t n,
  //  std::complex<float>* A, int64_t lda,
  //  float* S,
  //  std::complex<float>* U, int64_t ldu,
  //  std::complex<float>* VT, int64_t ldvt );

  Blas_int M = static_cast<Blas_int>(n_rows(A));
  Blas_int N = static_cast<Blas_int>(n_cols(A));
  Blas_int K = std::min(M, N);
  IndexArray<Real> svalues(K);
  Real * Sptr = &svalues[0];
  Matrix U, Vt;
  Scalar * Aptr = get_ptr(A);
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(A));

  lapack::Job jobu = lapack::Job::NoVec;
  lapack::Job jobvt = lapack::Job::NoVec;
  Blas_int ldu = 1;
  Blas_int ldvt = 1;

  if(compute_u){
    if(reduced){
      jobu = lapack::Job::SomeVec;
      U = Matrix(M, K);
    } else {
      jobu = lapack::Job::AllVec;
      U = Matrix(M, M);
    }
    ldu = static_cast<Blas_int>(get_leading_dim(U));
  }

  if(compute_vt){
    if(reduced){
      jobvt = lapack::Job::SomeVec;
      Vt = Matrix(K, N);
    } else {
      jobvt = lapack::Job::AllVec;
      Vt = Matrix(N, N);
    }
    ldvt = static_cast<Blas_int>(get_leading_dim(Vt));
  }

  Blas_int err;
  Scalar * Uptr = get_ptr(U);
  Scalar * Vtptr = get_ptr(Vt);

  err = lapack::gesvd(jobu, jobvt, M, N, Aptr, lda, Sptr, Uptr, ldu, Vtptr, ldvt);

  if(err != 0) std::cerr << "Error in lapack::gesvd, returned with value " << err << '\n';

  return std::make_tuple(U, svalues, Vt);
}

// By default, we return the reduced U, sigmas and V^*
template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::tuple<Matrix, IndexArray<Real>, Matrix>
svd(const Matrix& A, bool reduced = true) {
  return _svd_gesvd<Matrix, Scalar, Real>(A, true, true, reduced);
}

// Returns U and sigmas only
template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<Matrix, IndexArray<Real>>
svd_left_sv(const Matrix& A, bool reduced = true) {
  auto [U, sigmas, Vt] = _svd_gesvd<Matrix, Scalar, Real>(A, true, false, reduced);
  return std::make_pair(U, sigmas);
}

// Returns sigmas and Vt only
template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  std::pair<IndexArray<Real>, Matrix>
svd_right_sv(const Matrix& A, bool reduced = true) {
  auto [U, sigmas, Vt] = _svd_gesvd<Matrix, Scalar, Real>(A, false, true, reduced);
  return std::make_pair(sigmas, Vt);
}

// Returns only singular values
template<BlasKernelMatrix Matrix,
	 MPH_Scalar Scalar = typename Matrix::scalar_type,
	 MPH_Real Real = typename Matrix::real_type>
[[nodiscard]] static  IndexArray<Real>
svdvals(const Matrix& A) {
  auto [U, sigmas, Vt] = _svd_gesvd<Matrix, Scalar, Real>(A, false, false);
  return sigmas;
}
// Singular value decomposition:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Footer][Footer:1]]
}; // struct blas_kernels
} // namespace maphys
// Footer:1 ends here
