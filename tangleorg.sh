#!/usr/bin/env sh

set -ex

emacs --batch --load publish.el --eval '(org-publish "generate-source-code")'
