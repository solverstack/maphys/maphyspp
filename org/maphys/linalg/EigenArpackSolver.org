label:linalg:eigenarpacksolver

* Eigen problem solver
:PROPERTIES:
:header-args: c++ :tangle ../../../include/maphys/linalg/EigenArpackSolver.hpp :comments link
:END:

** Header

   #+begin_src c++
     #pragma once

     #include <lapack.hh>
     #include <blas.hh>
     #define _COMPLEX_H 1
     // Dirty but for some reason, arpack includes "complex.h"
     // This define should avoid this include
     #include <arpack.hpp>
     #include <debug_c.hpp>

     #include <exception>
     #include "maphys/utils/Arithmetic.hpp"

     namespace maphys {
   #+end_src

** Exception when convergence fails

  #+begin_src c++
  struct ArpackNotConverged: public std::exception
  {
    const char* what() const throw()
    {
      return "Arpack convergence failed";
    }
  };
  #+end_src

** Class

#+begin_src c++
  template<MPH_DenseMatrix OutMatrix, MPH_Matrix Matrix, MPH_Vector Vector, typename Solver, int force_use_mode = -1>
  struct EigenArpackSolver {

    using Real = typename Vector::real_type;
    using Scalar = typename scalar_type<Matrix>::type;
#+end_src
  
** Generalized eigen problem

   Solve $A u = \lambda B u$ where $A$ and $B$ are matrices, $\lambda$ the
   eigenvalue and $u$ the associated eigenvector.

   In this version we are find the $n_v$ smallest eigenvalues and their
   eigenvectors. If $A$ is $m \times m$, it returns $U$, the $m \times n_v$
   matrix containing the eigenvectors.

   Arpack only allows to compute $m - 1$ eigenvectors at most.

   For direct solvers that provide separate triangular solves, =force_use_mode=
   can be set to force usage of arpack mode (1 or 2, 1 being supposedly faster).
   This is mostly for benchmark purposes, automatic detection is recommended.

   #+begin_src c++
     [[nodiscard]] static std::pair<IndexArray<Real>, OutMatrix>
     gen_eigh_smallest(const Matrix& A, const Matrix& B, int n_v, Real tol = -1){
       Timer<TIMER_EVD> t("arpack_GEVD_pb");

       static_assert(is_dense<OutMatrix>::value, "Arpack - generalized EVD only returns dense matrix");

       if(n_rows(A) <= 1) return std::make_pair(IndexArray<Real>(), OutMatrix());

       MAPHYSPP_ASSERT(A.is_sym_or_herm(), "arpack_generalized_eigen_smallest: A.U = Lam.B.x only implemented with A sym or herm");
       MAPHYSPP_ASSERT(B.is_spd_or_hpd(), "arpack_generalized_eigen_smallest: A.U = Lam.B.x only implemented with B spd or hpd");

       // Choosing mode: MODE = 1 is supposed to be more efficient but it requires to be able to triangular solves separately
       // Cannot use MODE = 1 work with sparse at the moment (for instance MUMPS doesn't give access to triangular solve)

       constexpr auto mode_to_use = [](){
				      if constexpr(force_use_mode == 1 || force_use_mode == 2) return static_cast<a_int>(force_use_mode);
     #if __cplusplus > 201703L // C++20 version
				      if constexpr( requires(Solver r_s, Vector r_v1, Vector r_v2)
					{ r_s.triangular_solve(r_v1, r_v2, MatrixStorage::lower, true); }){ return 1; }
     #else // C++ 17 version
				      if constexpr(has_triangular_solve<Solver>::value){ return 1; }
     #endif
				      return 2;
				    };

       constexpr const a_int MODE = mode_to_use();
       //std::cout << "MODE: " << MODE << '\n';
       //debug_c(6, -6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

       if(static_cast<int>(n_rows(A)) < n_v){
	 std::string warn_mess("Asking for ");
	 warn_mess += std::to_string(n_v) + std::string(" eigenvectors in matrix of size ") + std::to_string(n_rows(A));
	 MAPHYSPP_WARNING(warn_mess);
	 n_v = n_rows(A);
       }

       MAPHYSPP_ASSERT((A.is_symmetric() || A.is_hermitian()), "Eigen solve : only implemented for sym/herm matrices");

       a_int ido = 0; // Instruction to perfrom (reverse communication)
       constexpr const arpack::bmat bmat = (MODE == 1) ? arpack::bmat::identity : arpack::bmat::generalized;
       const a_int N = static_cast<a_int>(n_rows(A));
       const a_int nev = std::min(N-1, static_cast<a_int>(n_v));
       if(tol <= 0){
	 tol = is_precision_double<Scalar>::value ? 1e-14 : 1e-6;
       }
       else{
	 if(!is_precision_double<Scalar>::value && tol < 1e-8){
	   MAPHYSPP_WARNING("arpack gen. evd : tolerance lower than 1e-8 in single precision");
	 }
       }

       Vector resid(N);
       const a_int ncv = std::min(2 * nev + 1, N); // Muse be in [2 * nev, N]
       std::vector<Scalar> V(ncv * N);
       const a_int ldv = N;

       std::array<a_int, 11> iparam{};
       iparam[0] = 1; // Shift provided by arpack
       iparam[2] = 10 * N; // Max iter
       iparam[3] = 1; // Only 1 supported
       iparam[4] = 0; // Number of ev found by arpack.

       //If M can be factored into a Cholesky factorization M = LL`
       //then Mode = 2 should not be selected.  Instead one should use
       //Mode = 1 with  OP = inv(L)*A*inv(L`)

       // Mode 1:  A*x = lambda*x, A symmetric
       //          ===> OP = A  and  B = I.
       // Mode 2:  A*x = lambda*M*x, A symmetric, M symmetric positive definite
       //          ===> OP = inv[M]*A  and  B = M.

       iparam[6] = MODE;

       std::array<a_int, 14> ipntr{}; // Not sure why 14 (see arpack-ng/TESTS/icb_arpack_cpp.cpp)

       std::vector<Scalar> workd(3*N);
       const a_int lworkl = ncv * (3 * ncv + 8);// Must be >= NCV**2 + 8*NCV
       std::vector<Scalar> workl(lworkl);
       a_int info = 0;
       // Factorize B with Cholesky
       Solver solver(B);

       // SEUPD extra parameters
       const a_int rvec = 1; // Get eigenvectors

       OutMatrix U(N, nev);
       std::vector<Scalar> d_array(nev);
       Scalar * z_ptr = get_ptr(U);
       Scalar * d_ptr = d_array.data();
       const a_int ldz = get_leading_dim(U);
       Scalar sigma = Scalar{0};
       std::vector<a_int> select(ncv); // Used as workspace

       const Size Vsize = static_cast<Size>(N);

       while(ido != 99){
	 arpack::saupd(ido, bmat, N,
		       arpack::which::smallest_magnitude, nev, tol, get_ptr(resid), ncv,
		       V.data(), ldv, iparam.data(), ipntr.data(), workd.data(),
		       workl.data(), lworkl, info);

	 if(info != 0){
	   std::string warn("arpack::saupd returned with info = ");
	   warn += std::to_string(info);
	   MAPHYSPP_WARNING(warn);
	 }
	 //std::cerr << "Returned IDO: " << ido << '\n';

	 Vector X(DenseData<Scalar, 1>(Vsize, &(workd[ipntr[0] - 1])), true);
	 Vector Y(DenseData<Scalar, 1>(Vsize, &(workd[ipntr[1] - 1])), true);

	 if(ido == 99){ break; }

	 if constexpr(MODE == 1){
	   // With B = L . L^T :
	   // Compute Y <- L^{-1} . A . L^{-T} . X
	   solver.triangular_solve(X, Y, MatrixStorage::lower, true);
	   X = A * Y;
	   solver.triangular_solve(X, Y, MatrixStorage::lower, false);
	 }
	 else if constexpr(MODE == 2){
	   if(ido == 1 or ido == -1){
	     X = A * X;
	     solver.apply(X, Y);
	   }
	   else if(ido == 2){
	     Y = B * X;
	   }
	 }
       }

       // Check we found enough eigenvalues
       if(iparam[4] < nev) throw ArpackNotConverged();

       arpack::seupd(rvec, arpack::howmny::ritz_vectors, select.data(), d_ptr,
		     z_ptr, ldz, sigma, // Here we must copy parameters passed to saupd
		     bmat, N,
		     arpack::which::smallest_magnitude, nev, tol, get_ptr(resid), ncv,
		     V.data(), ldv, iparam.data(), ipntr.data(), workd.data(),
		     workl.data(), lworkl, info);
       if(info != 0){
	 std::string warn("arpack::seupd returned with info = ");
	 warn += std::to_string(info);
	 MAPHYSPP_WARNING(warn);
       }

       IndexArray<Real> lambda(nev);
       for(a_int k = 0; k < nev; ++k){
	 lambda[k] = static_cast<Real>(d_array[k]);
       }

       //std::cout << "Lambdas:\n";
       //for(auto l : lambda) std::cout << l << ", ";
       //std::cout << '\n';

       // See remark 3 of dsaupd.f
       // After convergence, an approximate
       // eigenvector z of the original problem is recovered by solving
       // L`z = x  where x is a Ritz vector of OP.
       if constexpr(MODE == 1){
	 for(a_int j = 0; j < nev; ++j){
	   auto Uj = U.get_vect_view(j);
	   auto tmp = U.get_vect(j);
	   solver.triangular_solve(tmp, Uj, MatrixStorage::lower, true);
	 }
       }

       constexpr const Real prune_tol = is_precision_double<Scalar>::value ? 1e-15 : 5e-7;
       for(Size j = 0; j < n_cols(U); ++j){
	 for(Size i = 0; i < n_rows(U); ++i){
	   if(std::abs(U(i, j)) < prune_tol) U(i,j) = Scalar{0};
	 }
       }

       return std::make_pair(lambda, U);
     }
   #+end_src

** Footer

   #+begin_src c++
     }; // struct EigenArpackSolver
     } // namespace maphys
   #+end_src

* Tests
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../src/test/unittest/test_eigenpb_arpack.cpp :comments link
:END:

  #+begin_src c++
    #include <iostream>
    #include <maphys.hpp>
    #include <maphys/loc_data/DenseMatrix.hpp>
    #include <maphys/testing/TestMatrix.hpp>
    #include <maphys/solver/BlasSolver.hpp>
    #include <maphys/solver/Pastix.hpp>
    #include <maphys/linalg/EigenArpackSolver.hpp>

    #include <catch2/catch_test_macros.hpp>
    #include <catch2/catch_template_test_macros.hpp>

    TEMPLATE_TEST_CASE("EigenSolver", "[eigen_problem][arpack]", float, double){
      using namespace maphys;

      using Scalar = TestType;
      using Real = typename maphys::arithmetic_real<Scalar>::type;
      using DenseMat = DenseMatrix<Scalar>;
      using SparseMat = SparseMatrixCOO<Scalar>;
      using SolverSparse = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
      using SolverDense = BlasSolver<DenseMat, Vector<Scalar>>;

      const int M = 100;
      const int n_v = 5;

      // We want to find U so that A U = LAM B U
      // With U the n_v eigenvectors associated to the smallest eigenvalues
      auto build_mat = [](){
	DenseMat mat = test_matrix::random_matrix<Scalar, DenseMat>(3*M, Real{-1}, Real{1}, M, M).matrix;
	for(int i = 0; i < M; ++i) mat(i,i) += (5 + i);
	mat *= mat.t();
	for(int j = 1; j < M; ++j){
	  for(int i = 0; i < j; ++i){
	    mat(i,j) = 0;
	  }
	}
	mat.set_spd(MatrixStorage::lower);
	return mat;
      };

      Real tol = is_precision_double<Scalar>::value ? 1e-12 : 1e-6;

      SECTION("Test with A and B dense"){
	std::cout << "---dense---\n";
	const DenseMat A = build_mat();
	const DenseMat B = build_mat();

	auto [lambda, U] = EigenArpackSolver<DenseMat, DenseMat, Vector<Scalar>, SolverDense>::gen_eigh_smallest(A, B, n_v);

	// In case A or B is too big, we multiply tolerance by (norm of A + norm of B) (Frobenius norm)
	Real scal_norm = A.norm() + B.norm();
	tol *= scal_norm;

	Real prev_lambda = Real{0};
	for(int j = 0; j < n_v; ++j){
	  // Check lambdas are in asending order
	  REQUIRE(lambda[j] > prev_lambda);
	  prev_lambda = lambda[j];
	  Vector<Scalar> Uj = U.get_vect_view(j);
	  REQUIRE(Uj.norm() > 0);
	  Vector<Scalar> A_u = A * Uj;
	  Vector<Scalar> l_B_u = static_cast<Scalar>(lambda[j]) * (B * Uj);

	  std::cout << "Lambda : " << lambda[j] << '\n';
	  Vector<Scalar> diff = A_u - l_B_u;
	  std::cout << "Diff norm: " << diff.norm() << '\n';
	  REQUIRE(diff.norm() < tol);
	}
      }

      SECTION("Test with A and B sparse"){
	std::cout << "---sparse---\n";
	SparseMat A = build_mat();
	SparseMat B = build_mat();
	A.to_triangular(MatrixStorage::lower);
	B.to_triangular(MatrixStorage::lower);

	auto [lambda, U] = EigenArpackSolver<DenseMat, SparseMat, Vector<Scalar>, SolverSparse>::gen_eigh_smallest(A, B, n_v);

	// In case A or B is too big, we multiply tolerance by (norm of A + norm of B) (Frobenius norm)
	Real scal_norm = A.norm() + B.norm();
	tol *= scal_norm;

	Real prev_lambda = Real{0};
	for(int j = 0; j < n_v; ++j){
	  // Check lambdas are in asending order
	  REQUIRE(lambda[j] > prev_lambda);
	  prev_lambda = lambda[j];
	  Vector<Scalar> Uj = U.get_vect_view(j);
	  REQUIRE(Uj.norm() > 0);
	  Vector<Scalar> A_u = A * Uj;
	  Vector<Scalar> l_B_u = static_cast<Scalar>(lambda[j]) * (B * Uj);

	  std::cout << "Lambda : " << lambda[j] << '\n';
	  Vector<Scalar> diff = A_u - l_B_u;
	  std::cout << "Diff norm: " << diff.norm() << '\n';
	  REQUIRE(diff.norm() < tol);
	}
      }
    }
  #+end_src
