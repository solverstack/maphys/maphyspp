label:linalg:matrixnorm

Routine to compute a matrix norm.

* Approximate matrix norm
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/linalg/MatrixNorm.hpp :comments link
:END:

A routine to compute the matrix norm induces by the vector 2-norm.

For a matrix $A$ of size $n$:

$$||A|| = \underset{x \in \mathbb{C}^n, ||x||_2 = 1}{sup} ||Ax||_2$$

#+begin_src c++
#pragma once

#include "../utils/Arithmetic.hpp"
#include <random>

namespace maphys {

template<typename Matrix,
         typename Vector = typename vector_type<Matrix>::type,
         typename Scalar = typename scalar_type<Matrix>::type,
         typename Real = typename arithmetic_real<Scalar>::type>
Real approximate_mat_norm(const Matrix& A, size_t nb_rand_vect = 100, int seed = 0){

  if(seed == 0){
    std::random_device rd;
    seed = rd();
  }

  std::mt19937 gen(seed);
  std::uniform_real_distribution<> dis(double{-1}, double{1});

  auto random_scalar = [&](){
    if constexpr(is_complex<Scalar>::value){
      Real re = static_cast<Real>(dis(gen));
      Real im = static_cast<Real>(dis(gen));
      return Scalar{re, im};
    }
    else{
      return static_cast<Scalar>(dis(gen));
    }
  };

  auto M = n_rows(A);

  Real approx_norm_sq = Real{0};
  for(size_t k = 0; k < nb_rand_vect; ++k){
    Vector u(M);
    Scalar sum_squares = Scalar{0};

    for(int i = 0; i < static_cast<int>(M); ++i){
      u[i] = random_scalar();
      sum_squares += conj(u[i]) * u[i];
    }

    Scalar inv_norm = Scalar{1.0} / Scalar{std::sqrt(std::real(sum_squares))};

    for(int i = 0; i < static_cast<int>(M); ++i){
      u[i] *= inv_norm;
    }

    Vector mv = A * u;
    approx_norm_sq = std::max(approx_norm_sq, std::real(dot(mv, mv)));
  }

  return std::sqrt(approx_norm_sq);
}

} // end namespace maphys
#+end_src

* Test
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../src/test/unittest/test_matrix_norm.cpp :comments link
:END:

#+begin_src c++
  #include <iostream>
  #include <lapack.hh>
  #include <maphys.hpp>
  #include <maphys/loc_data/DenseMatrix.hpp>
  #include <maphys/loc_data/SparseMatrixCOO.hpp>
  #include <maphys/solver/BlasSolver.hpp>
  #include <maphys/kernel/BlasKernels.hpp>
  #include <maphys/linalg/MatrixNorm.hpp>
  #include <maphys/testing/TestMatrix.hpp>

  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>

  using namespace maphys;

  template<typename Scalar, typename Real = typename maphys::arithmetic_real<Scalar>::type>
  Real matrix_norm_ref(const DenseMatrix<Scalar>& mat){
    auto sing_vals = blas_kernels::svdvals(mat);
    return sing_vals[0];
  }

  TEMPLATE_TEST_CASE("MatrixNorm", "[matrix_norm]", float, double, std::complex<float>, std::complex<double>){

    using Scalar = TestType;
    //using Scalar = double;
    using Real = typename maphys::arithmetic_real<Scalar>::type;

    const int M = 1000;
    const int NNZ = 3333;
    const int NB_TESTS = 1;

    for(int i = 0; i < NB_TESTS; ++i){

      DenseMatrix<Scalar> A = test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(NNZ, Real{-10}, Real{10}, M, M).matrix;
      const Real ref_norm = matrix_norm_ref(A);
      //std::cout << "-----------> " << ref_norm << '\n';
      Real norm = approximate_mat_norm(A);
      Real rel_err = (ref_norm - norm) / ref_norm;
      //std::cout << "Dense: Norm -> " << norm <<  '\n';
      std::cout << "Rel err -> " << rel_err <<  '\n';
      //REQUIRE(rel_err < 1); // Relative error lower than 1 -> correct order of magnitude

      SparseMatrixCOO<Scalar> Asparse(A);
      norm = approximate_mat_norm(Asparse);
      rel_err = (ref_norm - norm) / ref_norm;
      //std::cout << "Spars: Norm -> " << norm <<  '\n';
      std::cout << "Rel err -> " << rel_err <<  '\n';
      REQUIRE(rel_err < 1);
    }
  }
#+end_src
