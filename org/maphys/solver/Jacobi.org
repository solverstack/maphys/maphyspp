label:solver:jacobi

* Class
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/solver/Jacobi.hpp :comments link
:END:

** Header

#+begin_src c++
  #pragma once

  #include "maphys/utils/Arithmetic.hpp"
  #include "maphys/utils/MatrixProperties.hpp"
  #include "maphys/utils/Error.hpp"
  #include "maphys/utils/Macros.hpp"
  #include "maphys/solver/IterativeSolver.hpp"

  namespace maphys {
#+end_src

** Base class

This class inherits the base class {{{link(solver:iterativesolver, IterativeSolver.html, IterativeSolver)}}}.

#+begin_src c++
  template<typename Matrix, MPH_Vector Vector>
  class Jacobi :
      public IterativeSolver<Matrix, Vector> {
#+end_src

** Algorithm
*** Basic Jacobi method

The system to solve is written $Ax = b$. It is assumed that the diagonal entries of $A$ are nonzero.

We consider matrix $A$ as $A = D -E -F$, where $D$ is the diagonal of $A$,
$-E$ is the strictly lower part and $-F$ the strictly upper part.

At each iteration $k$, we compute:

$$x_{k+1} = D^{-1}(E + F) x_k + D^{-1}b$$

For this implementation, we choose to compute first
matrix $Z$ and vector $t$ as following:

$$Z = D^{-1}(E + F)$$

$$t = D^{-1}b$$

So the iteration can be simply written:

$$x_{k+1} = Z x_k + t$$

*** Weighted Jacobi method

Reference: _Iterative methods for sparse linear systems_, Y. Saad, pages 414-415.
To improve convergence, a weight $\omega$ is often used.

The iteration is then written:

$$x_{k+1} = \omega(D^{-1}(E + F) x_k + D^{-1}b) + (1 - \omega) x_k$$
$$x_{k+1} = [(1 - \omega) I + \omega D^{-1}(E + F)] x_k + \omega D^{-1}b$$

So we can write the weighted Jacobi iteration:

$$x_{k+1} = Z x_k + t$$

With:

$$Z = (1 - \omega) I + \omega D^{-1}(E + F)$$
$$t = \omega D^{-1}b$$

Or using the fact that $E + F = D - A$

$$Z = I - \omega D^{-1}A$$

** Attributes

We need to store $Z$ and $t$.

#+begin_src c++
  private:
    using IterativeSolver<Matrix, Vector>::_A;
    using IterativeSolver<Matrix, Vector>::_B;
    using IterativeSolver<Matrix, Vector>::_X;
    using IterativeSolver<Matrix, Vector>::_R;
    using IterativeSolver<Matrix, Vector>::_squared;
    using IterativeSolver<Matrix, Vector>::_tol_sq;
    using IterativeSolver<Matrix, Vector>::_max_iter;
    using IterativeSolver<Matrix, Vector>::_residual_sq;
    using IterativeSolver<Matrix, Vector>::_verbose;
    using IterativeSolver<Matrix, Vector>::_stop_crit_denom_inv;

    using Scalar = typename IterativeSolver<Matrix, Vector>::scalar_type;
    using Real = typename IterativeSolver<Matrix, Vector>::real_type;

    bool _compute_Z = true;
    bool _compute_t = true;
    bool _fixed_Z = false;
    bool _fixed_t = false;

    // weight
    Scalar omega = Scalar{1};
    // D^-1
    Vector D_inv;
    //Z = D^-1(E+F)
    //or Z = I - (omega D^-1 A)
    Matrix Z;
    //t = omega D^-1 * b
    Vector t;
#+end_src

** Computation of auxilary matrix and vector

#+begin_src c++
  // Compute from A the left matrix Z = D^-1(E+F)
  void compute_Z(const Matrix& A){
    Z = A;
    if constexpr(is_distributed<Matrix>::value){
      for(int sd_id : Z.get_sd_ids()){
        auto& loc_Z = Z.get_local_matrix(sd_id);
        const auto& loc_diag = D_inv.get_local_vector(sd_id);
        compute_Z_local(loc_Z, loc_diag);
      }
    }
    else{
      compute_Z_local(Z, D_inv);
    }
    if(_fixed_Z) _compute_Z = false;
  }

  template<MPH_DenseVector DenseV, typename DenseM>
  void compute_Z_local(DenseM& Z_loc, const DenseV& D_inv_loc){
    const Size m = n_rows(Z_loc);
    const Size n = n_cols(Z_loc);
    MAPHYSPP_ASSERT( m == n, "Jacobi solver must be on squared matrix");

    const Scalar diag_val = Scalar{1} - omega;

    for(Size i = 0; i < m; ++i){
      for(Size j = 0; j < m; ++j){
        if(i == j){
          Z_loc(i, j) = diag_val;
        }
        else{
          Z_loc(i, j) = -Z_loc(i, j) * D_inv_loc(i) * omega;
        }
      }
    }
  }

  template<MPH_DenseVector DenseV, MPH_DenseMatrix DenseM>
  void compute_Z_local_weighted(DenseM& Z_loc, const DenseV& D_inv_loc){

    const Size m = n_rows(Z_loc);
    const Size n = n_cols(Z_loc);
    MAPHYSPP_ASSERT( m == n, "Jacobi solver must be on squared matrix");

    for(Size i = 0; i < m; ++i){
      for(Size j = 0; j < m; ++j){
        if(i == j){
          Z_loc(i, j) = Scalar{0};
        }
        else{
          Z_loc(i, j) = -Z_loc(i, j) * D_inv_loc(i);
        }
      }
    }
  }

  void compute_t(const Vector& B){
    t = B;

    auto multiply_terms = [](Scalar& s1, const Scalar& s2){
      s1 *= s2;
    };
    if constexpr (is_distributed<Vector>::value){
      t.apply(multiply_terms, D_inv);
    }
    else{ // Local vector
      for(Size i = 0; i < size(B); ++i){
        multiply_terms(t(i), D_inv(i));
      }
    }

    if(omega != Scalar{1}) t *= omega;

    if(_fixed_t) _compute_t = false;
  }
#+end_src

** Iterative solve

#+begin_src c++
  int iterative_solve() override {
    Timer<TIMER_ITERATION> t0("Jacobi iteration 0");
    // No need for IterativeSolver to recompute the residual
    // because here R is already the true residual
    this->_check_true_residual = false;
    const Matrix& A = *_A;
    const Vector& B = *_B;
    Vector& X = *_X;
    Vector& R = _R;

    this->setup_initial_stop_crit("Jacobi");

    auto inverse_diag = [](Scalar& s){
                          if(s == Scalar{0}){
                            s = Scalar{1};
                          }
                          else{
                            s = Scalar{1}/s;
                          }
                        };
    if(_compute_Z){
      D_inv = diagonal_as_vector(A);
      if constexpr (is_distributed<Matrix>::value){
        D_inv.assemble(); // Not sure about that
        D_inv.apply(inverse_diag);
      }
      else{
        for(Size i = 0; i < size(B); ++i){
          inverse_diag(D_inv(i));
        }
      }
      compute_Z(A);
    }
    if(_compute_t){
      compute_t(B);
    }
    R = B - A * X;

    _residual_sq = _squared(R) * _stop_crit_denom_inv;
    t0.stop();

    if(_verbose) std::cout << "||B-AX||/||B||\t" << std::sqrt(_residual_sq) << '\n';
    if(_residual_sq < _tol_sq) return 0;

    for(int iter = 1; iter < _max_iter + 1; ++iter){
      Timer<TIMER_ITERATION> t_iter("Jacobi iteration");
      if(_verbose) std::cout << iter << " -\t";

      // X(k+1) = D^-1 * (E+F) * X(k) + D^-1 * b
      X = Z * X + t;
      R = B - A * X;
      _residual_sq = _squared(R) * _stop_crit_denom_inv;
      if(_verbose) std::cout << "||B-AX||/||B||\t" << std::sqrt(_residual_sq) << '\n';
      if(_residual_sq < _tol_sq) return iter;
    }

    return -1;
  }
#+end_src

** Constructors

#+begin_src c++
  public:

    Jacobi(): IterativeSolver<Matrix, Vector>() {}

    Jacobi(const Matrix& A, bool verb=false):
      IterativeSolver<Matrix, Vector>(A, verb) {}
#+end_src

** Setup functions

#+begin_src c++
  void _setup(const parameters::fixed_matrix<bool>& v) override {
    _fixed_Z = v.value;
    if(!_fixed_Z) _compute_Z = true;
  }
  void _setup(const parameters::fixed_rhs<bool>& v) override {
    _fixed_t = v.value;
    if(!_fixed_t) _compute_t = true;
  }
  void _setup(const parameters::weight<Scalar>& v) override {
    omega = v.value;
  }
  }; // class Jacobi
#+end_src

** Traits

#+begin_src c++
  template<typename Matrix, typename Vector>
  struct is_solver_direct<Jacobi<Matrix, Vector>> : public std::false_type {};
  template<typename Matrix, typename Vector>
  struct is_solver_iterative<Jacobi<Matrix, Vector>> : public std::true_type {};
#+end_src

** Footer

#+begin_src c++
  } // namespace maphys
#+end_src


* Test

** Sequential

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_jacobi.cpp :comments link
  #include <iostream>
  #include <vector>

  #ifdef MAPHYSPP_USE_EIGEN
  #include <maphys/wrappers/Eigen/Eigen_header.hpp>
  #endif
  #ifdef MAPHYSPP_USE_ARMA
  #include <maphys/wrappers/armadillo/Armadillo_header.hpp>
  #endif
  #ifdef MAPHYSPP_USE_EIGEN
  #include <maphys/wrappers/Eigen/Eigen.hpp>
  #endif
  #ifdef MAPHYSPP_USE_ARMA
  #include <maphys/wrappers/armadillo/Armadillo.hpp>
  #endif

  #include <maphys.hpp>
  #include <maphys/solver/Jacobi.hpp>
  #include <maphys/testing/TestMatrix.hpp>
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>
  #include "test_iterative_solver.hpp"

  using namespace maphys;

  template<typename Scalar, typename Matrix>
  void test(const std::string mat_type){
    using Vect = typename vector_type<Matrix>::type;

    // Make a diagonal dominant matrix
    Matrix A = test_matrix::general_matrix<Scalar, Matrix>().matrix;
    for(size_t i = 0; i < n_rows(A); ++i){ A(i, i) += 10; }

    SECTION(std::string("Jacobi, matrix ") + mat_type){
      test_solver<Scalar, Matrix, Vect, Jacobi<Matrix, Vect>>(A,
							      test_matrix::simple_vector<Scalar, Vect>().vector);
    }
  }

  TEMPLATE_TEST_CASE("Jacobi", "[Jacobi][iterative][sequential]", float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;

    test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
  #ifdef MAPHYSPP_USE_EIGEN
    using EigenDenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
    test<Scalar, EigenDenseMatrix>("Eigen dense");
  #endif
  #ifdef MAPHYSPP_USE_ARMA
    test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
  #endif
  } // TEMPLATE_PRODUCT_TEST_CASE
#+end_src

** Distributed

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_jacobi_part.cpp :comments link
  #include <iostream>
  #include <vector>

  #ifdef MAPHYSPP_USE_EIGEN
  #include <maphys/wrappers/Eigen/Eigen_header.hpp>
  #endif
  #ifdef MAPHYSPP_USE_ARMA
  #include <maphys/wrappers/armadillo/Armadillo_header.hpp>
  #endif
  #ifdef MAPHYSPP_USE_EIGEN
  #include <maphys/wrappers/Eigen/Eigen.hpp>
  #endif
  #ifdef MAPHYSPP_USE_ARMA
  #include <maphys/wrappers/armadillo/Armadillo.hpp>
  #endif

  #include <maphys.hpp>
  #include <maphys/solver/Jacobi.hpp>
  #include <maphys/testing/TestMatrix.hpp>
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>
  #include "test_iterative_solver.hpp"

  #include <maphys/part_data/PartMatrix.hpp>
  using namespace maphys;

  template<typename Scalar, typename LocMatrix>
  void test(const std::string& mat_type){
    using Matrix = PartMatrix<LocMatrix>;
    using LocVect = typename vector_type<LocMatrix>::type;
    using Vect = PartVector<LocVect>;

    std::shared_ptr<Process> p = test_matrix::get_distr_process();

    SECTION(std::string("Jacobi, distributed, matrix ") + mat_type){
      test_solver<Scalar, Matrix, Vect, Jacobi<Matrix, Vect>>(
							      test_matrix::dist_general_matrix<Scalar, LocMatrix>(p).matrix,
							      test_matrix::dist_vector<Scalar, LocVect>(p).vector);
    }
  }

  TEMPLATE_TEST_CASE("Jacobi", "[Jacobi][iterative][distributed]", float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;

    test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
  #ifdef MAPHYSPP_USE_EIGEN
    using EigenDenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
    test<Scalar, EigenDenseMatrix>("Eigen dense");
  #endif
  #ifdef MAPHYSPP_USE_ARMA
    test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
  #endif
  } // TEMPLATE_PRODUCT_TEST_CASE
#+end_src
