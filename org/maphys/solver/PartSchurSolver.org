label:solver:partschursolver

* Method

  The numerical method is described in the
  {{{link(solver:schursolver,SchurSolver.html,Schur solver)}}} file.

  In this file we implement the case where the input matrix and right hand side
  are partitioned.

* Class

#+begin_src c++ :results silent :tangle ../../../include/maphys/solver/PartSchurSolver.hpp :comments link
    #pragma once

    #include <memory>
    #include <cassert>
    #include <limits>
    #include <iostream>
    #include <iomanip>
    #include <string>
    #include <type_traits>
    #include <cmath>
    #include <map>

    #include "maphys/utils/Arithmetic.hpp"
    #include "maphys/utils/MatrixProperties.hpp"
    #include "maphys/utils/Error.hpp"
    #include "maphys/utils/Macros.hpp"
    #include "maphys/part_data/PartOperator.hpp"
    #include "maphys/solver/SchurSolver.hpp"

    namespace maphys {

    template<MPH_Matrix Matrix, MPH_Vector Vector, typename SolverK, typename SolverS>
    class PartSchurSolver:
        public SchurSolverBase<Matrix, Vector, SolverK, SolverS>{

    private:
      using BaseT = SchurSolverBase<Matrix, Vector, SolverK, SolverS>;
      using SchurOperator = typename BaseT::schur_type;
      using BaseT::_S;
      using BaseT::_setup_solver_K;

      using SchurOperatorLoc = typename SchurOperator::local_type;
      using MatrixLoc = typename Matrix::local_type;
      using VectorLoc = typename Vector::local_type;

      constexpr const static bool is_implicit = std::is_same<ImplicitSchur<MatrixLoc,VectorLoc,SolverK>,SchurOperatorLoc>::value;

      PartOperator<SolverK,MatrixLoc,VectorLoc> _solver_K;
      // Keep a valid pointer to the explicit Schur if needed for Preconditioner

      std::enable_if_t<is_solver_iterative<SolverS>::value>
      setup_precond(){
        using Precond = typename SolverS::precond_type;
        using SchurPcdType = typename Precond::matrix_type;
        using SchurPcdVectType = typename Precond::vector_type;
        if constexpr(!std::is_same<Precond, Identity<SchurPcdType, SchurPcdVectType>>::value){
          using SchurPcdTypeLoc = typename SchurPcdType::local_type;

          auto fct_setup_pcd = [&](const SchurOperator& Simpl, Precond& M){
                                 SchurPcdType pcd_S(_S->get_proc(), /*on_interface*/true);

                                 auto get_expl_schur = [](SchurPcdTypeLoc& out_S, const SchurOperatorLoc& loc_impl_S){
                                                         out_S = const_cast<SchurOperatorLoc&>(loc_impl_S).get_dense_schur();
                                                       };

                                 pcd_S.template apply_on_data<SchurOperatorLoc>(Simpl, get_expl_schur);
                                 M.setup(pcd_S);
                               };
          BaseT::_solver_S.setup(parameters::setup_pcd<std::function<void(const SchurOperator&, Precond&)>>{fct_setup_pcd});
        }
      }

      void get_schur_complement(const Matrix& K){
        Timer<TIMER_SOLVER> t("Compute Schur");

        std::shared_ptr<Process> proc = K.get_proc();
        _S = std::make_unique<SchurOperator>(proc, /*on_interface*/ true);

        if constexpr(is_implicit){
            // Set matrix
            _S->setup(K);
            // Set schurlist
            std::map<int, IndexArray<int>> interfaces = proc->get_interface();
            auto fct_schur_impl = [&](int sd_id, SchurOperatorLoc& is){
                                    is.setup_schurlist(interfaces[sd_id]);
                                    is.split_matrix();
                                    _setup_solver_K(is.get_solver_K());
                                  };
            _S->apply_on_data_id(fct_schur_impl);
            if constexpr(is_solver_iterative<SolverS>::value) setup_precond();
          }
        else{
          _solver_K.initialize(proc);
          _solver_K.setup(K);
          auto fct_schur = [&](const int sd_id, SchurOperatorLoc& Sloc, SolverK& solver){
                             _setup_solver_K(solver);
                             IndexArray<int> intrf = proc->get_interface(sd_id);
                             Sloc = solver.get_schur(intrf);
                           };
          _S->template apply_on_data_id<SolverK>(_solver_K, fct_schur);
        }

      //_S-> display("Schur");
      }

      Vector get_schur_rhs(const Vector& B){
        Timer<TIMER_SOLVER> t("PartSchurSolver b2f");
        Vector F(B.get_proc(), /*on_interface*/ true);
        Vector B_dis = B;
        B_dis.disassemble();

        if constexpr(is_implicit){
          auto fct_rhs_impl = [](SchurOperatorLoc& Sloc, const VectorLoc& Bloc){ return Sloc.compute_rhs(Bloc); };
          _S->template apply_on_data<VectorLoc,VectorLoc>(B_dis, F, fct_rhs_impl);
        }
        else{
          auto fct_rhs = [](SolverK& solver, const VectorLoc& Bloc){ return solver.b2f(Bloc); };
          _solver_K.template apply_on_data<VectorLoc,VectorLoc>(B_dis, F, fct_rhs);
        }
        F.assemble();
        return F;
      }

      void get_schur_solution(const Vector& Ug, Vector& U){
        Timer<TIMER_SOLVER> t("PartSchurSolver y2x");

        if constexpr(is_implicit){
            auto fct_sol_impl = [](SchurOperatorLoc& Sloc, const VectorLoc& UgLoc){ return Sloc.get_solution(UgLoc); };
            _S->template apply_on_data<VectorLoc,VectorLoc>(Ug, U, fct_sol_impl);
          }
        else{
          auto fct_sol = [](SolverK& solver, const VectorLoc& Ugloc){ return solver.y2x(Ugloc); };
          _solver_K.template apply_on_data<VectorLoc,VectorLoc>(Ug, U, fct_sol);
        }
        U.set_on_intrf(false);
      }

    public:

      [[nodiscard]] PartOperator<SolverK,MatrixLoc,VectorLoc>& get_solver_K() { return _solver_K; }

    }; // class PartSchurSolver

    template<typename Matrix, typename Vector, typename SolverK, typename SolverS>
    struct is_matrix_free<PartSchurSolver<Matrix, Vector, SolverK, SolverS>> : public std::false_type {};

    } // namespace maphys

  #+end_src

* Test

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_partschur_solver.cpp :comments link
    #include <iostream>
    #include <vector>

    #include <maphys.hpp>
    #include <maphys/testing/TestMatrix.hpp>
    #include <maphys/solver/PartSchurSolver.hpp>
    #include <maphys/solver/Pastix.hpp>
    #include <maphys/solver/ConjugateGradient.hpp>
    #include <maphys/precond/DiagonalPrecond.hpp>
    #include <maphys/precond/AbstractSchwarz.hpp>

    #include <catch2/catch_test_macros.hpp>

    using namespace maphys;

    template<typename Matrix, typename Vect, typename SolverS, typename HybSolver>
    Vect hybrid_solve(const Vect& B, const Matrix& K){
      HybSolver hybrid_solver;

      bool verbose = false;
      if(MMPI::rank() == 0) verbose = true;

      SolverS& iter_solver = hybrid_solver.get_solver_S();
      if constexpr(is_solver_iterative<SolverS>::value){
        iter_solver.setup(parameters::max_iter{10},
                          parameters::tolerance{1e-8},
                          parameters::verbose{verbose});
      }

      hybrid_solver.setup(parameters::A{K},
                          parameters::verbose{verbose});

      using SolverK = typename HybSolver::solver_on_K;
      if constexpr(is_solver_iterative<SolverK>::value){
        auto setup_solverk = [verbose](SolverK& solver){
                               solver.setup(parameters::max_iter{10},
                                            parameters::verbose{verbose});
                             };
        hybrid_solver.setup(parameters::setup_solver_K<std::function<void(SolverK&)>>{setup_solverk});
      }

      return hybrid_solver * B;
    }

    template<typename Scalar>
    bool check_result(const PartVector<Vector<Scalar>>& X){
      using Real = typename arithmetic_real<Scalar>::type;

      X.display_centralized("X");

      std::shared_ptr<Process> p = X.get_proc();
      PartVector<Vector<Scalar>> X_expected = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
      PartVector<Vector<Scalar>> diff = X - X_expected;
      Real direct_diff_norm = diff.norm();

      std::cout << "||X_exp - X|| = " << direct_diff_norm << '\n';
      return (direct_diff_norm < Real{1e-6});
    }

    TEST_CASE("PartSchurSolver", "[CG][schur][distributed]"){

      using Scalar = double;
      using SpMat = PartMatrix<SparseMatrixCOO<Scalar>>;
      using DMat = PartMatrix<DenseMatrix<Scalar>>;
      using Vect = PartVector<Vector<Scalar>>;

      std::shared_ptr<Process> p = test_matrix::get_distr_process();

      //const SpMat dA = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
      SpMat dA = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
      dA.apply_on_data([](SparseMatrixCOO<Scalar>& m){
                         m.to_triangular(MatrixStorage::lower);
                         m.set_spd(MatrixStorage::lower);
                         m.order_indices();
                       });

      Vect dX_expected = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
      const Vect dB = dA * dX_expected;

      dX_expected.display_centralized("dX_exp");
      dB.display_centralized("dB");

      SECTION("PartSchur( Pastix, CG ), explicit"){
        using Solver_K = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
        using Solver_S = ConjugateGradient<DMat, Vect>;
        using Solver = PartSchurSolver<SpMat, Vect, Solver_K, Solver_S>;

        Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
        REQUIRE(check_result(X));
      }

      SECTION("PartSchur( Pastix, CG + diag pcd ), explicit"){
        using Precond = DiagonalPrecond<DMat, Vect>;
        using Solver_K = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
        using Solver_S = ConjugateGradient<DMat, Vect, Precond>;
        using Solver = PartSchurSolver<SpMat, Vect, Solver_K, Solver_S>;

        Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
        REQUIRE(check_result(X));
      }

      SECTION("PartSchur( Pastix, CG + AS ), explicit"){
        using Precond = AdditiveSchwarz<DMat, Vect, Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>>;
        using Solver_K = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
        using Solver_S = ConjugateGradient<DMat, Vect, Precond>;
        using Solver = PartSchurSolver<SpMat, Vect, Solver_K, Solver_S>;

        Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
        REQUIRE(check_result(X));
      }

      SECTION("PartSchurSolver( Pastix, CG ), implicit"){
        using Solver_K = Pastix<SparseMatrixCOO<Scalar>,Vector<Scalar>>;
        using ImplSchur = ImplicitSchur<SparseMatrixCOO<Scalar>,Vector<Scalar>,Solver_K>;
        using Solver_S = ConjugateGradient<PartOperator<ImplSchur,SparseMatrixCOO<Scalar>,Vector<Scalar>>,Vect>;
        using Solver = PartSchurSolver<SpMat,Vect, Solver_K,Solver_S>;

        Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
        REQUIRE(check_result(X));
      }

      SECTION("PartSchurSolver( Pastix, CG + diag pcd ), implicit"){
        using Precond = DiagonalPrecond<PartMatrix<DenseMatrix<Scalar>>,Vect>;
        using Solver_K = Pastix<SparseMatrixCOO<Scalar>,Vector<Scalar>>;
        using ImplSchur = ImplicitSchur<SparseMatrixCOO<Scalar>,Vector<Scalar>,Solver_K>;
        using Solver_S = ConjugateGradient<PartOperator<ImplSchur,SparseMatrixCOO<Scalar>,Vector<Scalar>>,Vect, Precond>;
        using Solver = PartSchurSolver<SpMat,Vect, Solver_K,Solver_S>;

        Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
        REQUIRE(check_result(X));
      }

      SECTION("PartSchurSolver(CG, CG), implicit"){
        using Solver_K = ConjugateGradient<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
        using ImplSchurCG = ImplicitSchur<SparseMatrixCOO<Scalar>,Vector<Scalar>,Solver_K>;
        using Solver_S = ConjugateGradient<PartOperator<ImplSchurCG,SparseMatrixCOO<Scalar>,Vector<Scalar>>,Vect>;
        using Solver = PartSchurSolver<SpMat, Vect, Solver_K, Solver_S>;

        Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
        REQUIRE(check_result(X));
      }
    }
  #+end_src
