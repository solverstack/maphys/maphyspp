label:solver:castoperator

* CastOperator

  This operator is an intermediary operator between an external operator and an
  inner operator with different =Scalar= types, requiring casting.

#+begin_src c++ :results silent :tangle ../../../include/maphys/solver/CastOperator.hpp :comments link
  #pragma once

  #include "maphys/utils/Error.hpp"
  #include "maphys/solver/LinearOperator.hpp"

  namespace maphys {
  template<typename Operator, typename Operand, typename InnerOperator>
  struct CastOperator: public LinearOperator<Operator, Operand> {

    InnerOperator inner_op;

    using Scalar = typename Operator::scalar_type;
    using operator_type = Operator;
    using matrix_type = Operator;
    using vector_type = Operand;

    using InnerScalar = typename InnerOperator::scalar_type;
    using inner_operator_type = InnerOperator;
    using inner_vector_type = typename InnerOperator::vector_type;
    using inner_scalar_type = InnerScalar;

    void setup(const Operator& A){
      inner_op.setup(A.template cast<InnerScalar>());
    }

    Operand apply(const Operand& B){
      auto out = inner_op.apply(B.template cast<InnerScalar>());
      return out.template cast<Scalar>();
    }

    void apply(const Operand& B, Operand X){
      auto out = inner_op.apply(B.template cast<InnerScalar>());
      X = out.template cast<Scalar>();
    }

    inline int get_n_iter() const { 
      if constexpr(is_solver_iterative<InnerOperator>::value){
        return inner_op.get_n_iter();
      } else { return 1; }
    }

    inline int get_n_iter_performed() const { 
      if constexpr(is_solver_iterative<InnerOperator>::value){
        return inner_op.get_n_iter_performed();
      } else { return 1; }
    }
  }; // struct CastOperator

  template<typename Operator, typename Operand, typename InnerOperator>
  struct is_solver_direct<CastOperator<Operator, Operand, InnerOperator>>{ static constexpr const bool value = is_solver_direct<InnerOperator>::value; };
  template<typename Operator, typename Operand, typename InnerOperator>
  struct is_solver_iterative<CastOperator<Operator, Operand, InnerOperator>>{ static constexpr const bool value = is_solver_iterative<InnerOperator>::value; };
  template<typename Operator, typename Operand, typename InnerOperator>
  struct is_matrix_free<CastOperator<Operator, Operand, InnerOperator>>{ static constexpr const bool value = is_matrix_free<InnerOperator>::value; };

  template<typename Operator, typename Operand, typename InnerOperator>
  struct vector_type<CastOperator<Operator, Operand, InnerOperator>> : public std::true_type {
    using type = typename CastOperator<Operator, Operand, InnerOperator>::vector_type;
  };

  template<typename Operator, typename Operand, typename InnerOperator>
  struct scalar_type<CastOperator<Operator, Operand, InnerOperator>> : public std::true_type {
    using type = typename CastOperator<Operator, Operand, InnerOperator>::scalar_type;
  };
} // namespace maphys
#+end_src

* Tests

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_castoperator.cpp :comments link
  #include <maphys.hpp>
  #include <maphys/solver/CastOperator.hpp>
  #include <catch2/catch_test_macros.hpp>

  using namespace maphys;

  struct MatMultDouble: public LinearOperator<DenseMatrix<double>, Vector<double>>{
    DenseMatrix<double> m;
    void setup(const DenseMatrix<double>& M){ m = M; }
    Vector<double> apply(const Vector<double>& U){ return m * U; }
  };

  struct MatMultFloat: public LinearOperator<DenseMatrix<float>, Vector<float>>{
    DenseMatrix<float> m;
    void setup(const DenseMatrix<float>& M){ m = M; }
    Vector<float> apply(const Vector<float>& U){
      U.display("U");
      return m * U; 
    }
  };

  TEST_CASE("CastOperator", "[operator]"){
    const DenseMatrix<double> A({1, 1, 1, 1, 1, 1, 1, 1, 1}, 3, 3);
    const Vector<double> U{1e8, 1, 0}; // Because for float: 1e8 + 1 = 1e8

    MatMultDouble matmultd;
    matmultd.setup(A);
    Vector<double> outd = matmultd.apply(U);
    outd.display("outd");

    CastOperator<DenseMatrix<double>, Vector<double>, MatMultFloat> castop;
    castop.setup(A);
    Vector<double> outf = castop.apply(U);
    outf.display("outf");

    const Vector<double> diff_exp{1, 1, 1};
    Vector<double> diff = outd - outf;
    diff.display("diff");
    REQUIRE(diff == diff_exp);
  }
#+end_src
