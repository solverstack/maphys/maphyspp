label:solver:bicgstab

* Class
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/solver/BiCGSTAB.hpp :comments link
:END:

** Header

#+begin_src c++
#pragma once

#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/utils/Macros.hpp"
#include "maphys/solver/IterativeSolver.hpp"

namespace maphys {
#+end_src

** Base class

This class inherits the base class {{{link(solver:iterativesolver, IterativeSolver.html, IterativeSolver)}}}.

#+begin_src c++
template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond = Identity<Matrix, Vector>>
class BiCGSTAB :
    public IterativeSolver<Matrix, Vector, Precond> {
#+end_src

** Attributes

#+begin_src c++
private:
  using Scalar = typename IterativeSolver<Matrix, Vector, Precond>::scalar_type;
  using Real = typename IterativeSolver<Matrix, Vector, Precond>::real_type;
  using IterativeSolver<Matrix, Vector, Precond>::_A;
  using IterativeSolver<Matrix, Vector, Precond>::_B;
  using IterativeSolver<Matrix, Vector, Precond>::_X;
  using IterativeSolver<Matrix, Vector, Precond>::_R;
  using IterativeSolver<Matrix, Vector, Precond>::_M;
  using IterativeSolver<Matrix, Vector, Precond>::_dot;
  using IterativeSolver<Matrix, Vector, Precond>::_squared;
  using IterativeSolver<Matrix, Vector, Precond>::_tol_sq;
  using IterativeSolver<Matrix, Vector, Precond>::_max_iter;
  using IterativeSolver<Matrix, Vector, Precond>::_check_true_residual;
  using IterativeSolver<Matrix, Vector, Precond>::_always_true_residual;
  using IterativeSolver<Matrix, Vector, Precond>::_residual_sq;
  using IterativeSolver<Matrix, Vector, Precond>::_verbose;
  using IterativeSolver<Matrix, Vector, Precond>::_convergence_achieved;

public:
  using scalar_type = Scalar;
  using vector_type = Vector;
  using matrix_type = Matrix;
  using real_type = Real;
#+end_src

** BiCGSTAB algorithm

Input:
- Matrix: $A$
- Vector: $B$ (rhs), $X$ (initial guess if given, null vector otherwise)
- Preconditioner: $M$
- Integer: $maxiter$
- Scalar: $tolerance$
Output:
- Vector: $X$ (solution)
- Integer: $niter$ (-1 if convergence was not achieved)

#+OPTIONS: tex:imagemagick
\begin{algorithmic}
\State $stop \gets tolerance^2 \times ||b||^2$
\State $R = B - A * X$
\State $\hat{R} = R$
\State $\rho = \alpha = \omega = 1$
\State $\nu = P = null\_vector()$
\If{ $||R||^2 < stop$ }
      \State \Return $X, 0$
\EndIf
\For{ $iter = 1, maxiter$ }
      \State $\rho_{old} = \rho
      \State $\rho = \hat{R}.R$
      \State $\beta = \frac{\rho \alpha}{\rho_{old} \omega}$
      \State $P = R + \beta (P - \omega \nu)$
      \State $Y = M^{-1} P$
      \State $\nu = A Y$
      \State $alpha = \frac{\rho}{\hat{R} \nu}$
      \State $H = X + \alpha Y$
      \State $S = R + \alpha \nu$
      \State $Z = M^{-1} S$
      \State $T = A Z$
      \State $\omega = \frac{M^{-1}T . M^{-1}S}{M^{-1}T . M^{-1}T}$
      \State $X = H + \omega Z$
      \State $R = S - \omega T$
      \If{ $||R||^2 < stop$ }
           \State \Return $X, iter$
      \EndIf
\EndFor
\State \Return $X, -1$
\end{algorithmic}

** C++ code

#+begin_src c++
  int iterative_solve() override {

    Timer<TIMER_ITERATION> t0("BiCGSTAB iteration 0");
    const Matrix& A = *_A;
    const Vector& B = *_B;
    Vector& X = *_X;
    Vector& R = _R;

    this->setup_initial_stop_crit("BiCGSTAB");

    R = B - A * X;
    if(_convergence_achieved()) return 0;

    Vector R_hat = R;

    Vector P  = R * Scalar{0};
    Vector NU = P;
    Vector H  = P;
    Vector S  = P;
    Vector T  = P;
    Vector Z  = P;
    Vector MT = P;
    Vector Y  = P;

    Scalar rho = 1;
    Scalar alpha = 1;
    Scalar omega = 1;

    t0.stop();

    for(int iter = 1; iter < _max_iter + 1; ++iter){
      Timer<TIMER_ITERATION> t("BiCGSTAB iteration");
      if(_verbose) std::cout << iter << " -\t";

      Scalar rho_old = rho;
      rho = _dot(R_hat, R);

      Scalar beta = (rho * alpha) / ( rho_old * omega);
      P = R + beta * (P - omega * NU);
      Y = _M * P;
      NU = A * Y;
      alpha = rho / _dot(R_hat, NU);
      H = X + alpha * Y;
      S = R - alpha * NU;
      Z = _M * S;
      T = A * Z;
      MT = _M * T;
      omega = _dot( MT, _M * S)/ _squared(MT);
      X = H + omega * Z;
      R = S - omega * T;

      if(_convergence_achieved()) return iter;
    }

    return -1;
  }
#+end_src

** Constructors

#+begin_src c++
public:

  BiCGSTAB(): IterativeSolver<Matrix, Vector, Precond>() {}

  BiCGSTAB(const Matrix& A, bool verb=false):
    IterativeSolver<Matrix, Vector, Precond>(A, verb) {}
}; //class BiCGSTAB
#+end_src

** Traits

#+begin_src c++
// Set traits
template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_solver_direct<BiCGSTAB<Matrix, Vector, Precond>> : public std::false_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_solver_iterative<BiCGSTAB<Matrix, Vector, Precond>> : public std::true_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct is_matrix_free<BiCGSTAB<Matrix, Vector, Precond>> : public std::true_type {};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct vector_type<BiCGSTAB<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename BiCGSTAB<Matrix, Vector, Precond>::vector_type;
};

template<MPH_LinearOperator Matrix, MPH_Vector Vector, class Precond>
struct scalar_type<BiCGSTAB<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename BiCGSTAB<Matrix, Vector, Precond>::scalar_type;
};
#+end_src

** Footer

#+begin_src c++
} // namespace maphys

#+end_src

* Tests

** Sequential

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_bicgstab.cpp :comments link
  #include <iostream>
  #include <vector>

  #ifdef MAPHYSPP_USE_EIGEN
  #include <maphys/wrappers/Eigen/Eigen_header.hpp>
  #endif
  #ifdef MAPHYSPP_USE_ARMA
  #include <maphys/wrappers/armadillo/Armadillo_header.hpp>
  #endif
  #ifdef MAPHYSPP_USE_EIGEN
  #include <maphys/wrappers/Eigen/Eigen.hpp>
  #endif
  #ifdef MAPHYSPP_USE_ARMA
  #include <maphys/wrappers/armadillo/Armadillo.hpp>
  #endif

  #include <maphys.hpp>
  #include <maphys/solver/BiCGSTAB.hpp>
  #include <maphys/precond/DiagonalPrecond.hpp>
  #include <maphys/testing/TestMatrix.hpp>
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>
  #include "test_iterative_solver.hpp"

  using namespace maphys;

  template<typename Scalar, typename Matrix>
  void test(const std::string mat_type){
    using Vect = typename vector_type<Matrix>::type;

    SECTION(std::string("BiCGSTAB, no preconditioner, matrix ") + mat_type){
      test_solver<Scalar, Matrix, Vect, BiCGSTAB<Matrix, Vect>>(
								test_matrix::general_matrix<Scalar, Matrix>().matrix,
								test_matrix::simple_vector<Scalar, Vect>().vector);
    }

    SECTION(std::string("BiCGSTAB, diagonal preconditioner, matrix ") + mat_type){
      using PcdType = DiagonalPrecond<Matrix, Vect>;
      test_solver<Scalar, Matrix, Vect, BiCGSTAB<Matrix, Vect, PcdType>>(
									 test_matrix::general_matrix<Scalar, Matrix>().matrix,
									 test_matrix::simple_vector<Scalar, Vect>().vector);
    }
  }

  TEMPLATE_TEST_CASE("BiCGSTAB", "[BiCGSTAB][iterative][sequential]", std::complex<float>){ //float, double, std::complex<float>, std::complex<double>){, float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;

    test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
    test<Scalar, SparseMatrixCOO<Scalar>>("SparseMatrixCOO");
    test<Scalar, SparseMatrixCSC<Scalar>>("SparseMatrixCSC");

  #ifdef MAPHYSPP_USE_EIGEN
    using EigenDenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
    using EigenSparseMatrix = Eigen::SparseMatrix<Scalar>;

    test<Scalar, EigenDenseMatrix>("Eigen dense");
    test<Scalar, EigenSparseMatrix>("Eigen sparse");
  #endif // MAPHYSPP_USE_EIGEN

  #ifdef MAPHYSPP_USE_ARMA
    test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
    test<Scalar, arma::SpMat<Scalar>>("Armadillo sparse");
  #endif
  } // TEMPLATE_PRODUCT_TEST_CASE
#+end_src

** Distributed

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_bicgstab_part.cpp :comments link
  #include <iostream>
  #include <vector>

  #ifdef MAPHYSPP_USE_EIGEN
  #include <maphys/wrappers/Eigen/Eigen_header.hpp>
  #endif
  #ifdef MAPHYSPP_USE_ARMA
  #include <maphys/wrappers/armadillo/Armadillo_header.hpp>
  #endif
  #ifdef MAPHYSPP_USE_EIGEN
  #include <maphys/wrappers/Eigen/Eigen.hpp>
  #endif
  #ifdef MAPHYSPP_USE_ARMA
  #include <maphys/wrappers/armadillo/Armadillo.hpp>
  #endif

  #include <maphys.hpp>
  #include <maphys/solver/BiCGSTAB.hpp>
  #include <maphys/precond/DiagonalPrecond.hpp>
  #include <maphys/testing/TestMatrix.hpp>
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>
  #include "test_iterative_solver.hpp"

  #include <maphys/part_data/PartMatrix.hpp>
  using namespace maphys;

  template<typename Scalar, typename LocMatrix>
  void test(const std::string& mat_type){
    using Matrix = PartMatrix<LocMatrix>;
    using LocVect = typename vector_type<LocMatrix>::type;
    using Vect = PartVector<LocVect>;

    std::shared_ptr<Process> p = test_matrix::get_distr_process();

    SECTION(std::string("BiCGSTAB, distributed, no preconditioner, matrix ") + mat_type){
      test_solver<Scalar, Matrix, Vect, BiCGSTAB<Matrix, Vect>>(
								test_matrix::dist_general_matrix<Scalar, LocMatrix>(p).matrix,
								test_matrix::dist_vector<Scalar, LocVect>(p).vector, 100, (MMPI::rank() == 0));
    }

    SECTION(std::string("BiCGSTAB, distributed, diagonal preconditioner, matrix ") + mat_type){
      using PcdType = DiagonalPrecond<Matrix, Vect>;
      test_solver<Scalar, Matrix, Vect, BiCGSTAB<Matrix, Vect, PcdType>>(
									 test_matrix::dist_general_matrix<Scalar, LocMatrix>(p).matrix,
									 test_matrix::dist_vector<Scalar, LocVect>(p).vector);
    }
  }

  TEMPLATE_TEST_CASE("BiCGSTAB", "[BiCGSTAB][iterative][distributed]", float){ //float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;

    test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
    test<Scalar, SparseMatrixCOO<Scalar>>("SparseMatrixCOO");
    test<Scalar, SparseMatrixCSC<Scalar>>("SparseMatrixCSC");

  #ifdef MAPHYSPP_USE_EIGEN
    using EigenDenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
    using EigenSparseMatrix = Eigen::SparseMatrix<Scalar>;

    test<Scalar, EigenDenseMatrix>("Eigen dense");
    test<Scalar, EigenSparseMatrix>("Eigen sparse");
  #endif // MAPHYSPP_USE_EIGEN

  #ifdef MAPHYSPP_USE_ARMA
    test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
    test<Scalar, arma::SpMat<Scalar>>("Armadillo sparse");
  #endif
  } // TEMPLATE_PRODUCT_TEST_CASE
#+end_src
