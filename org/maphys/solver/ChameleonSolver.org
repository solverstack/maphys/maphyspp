label:solver:chameleonsolver

In this class we use CHAMELEON routines to factorize dense matrices and solve dense linear systems.

* Class
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/solver/ChameleonSolver.hpp :comments link
:END:

** Header

#+begin_src c++
  #pragma once

  #include <memory>
  #include <chameleon.h>

  #include "maphys/utils/Chameleon.hpp"
  #include "maphys/interfaces/linalg_concepts.hpp"
  namespace maphys {
  template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector> class ChameleonSolver;
  }

  #include "maphys/loc_data/DenseMatrix.hpp"
  #include "maphys/solver/LinearOperator.hpp"

  namespace maphys {
#+end_src

** Class

#+begin_src c++
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
class ChameleonSolver :
    public LinearOperator<Matrix, Vector> {
#+end_src

** Attributes

We only week the factorized matrix.

#+begin_src c++
  private:
    using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
    using Real = typename LinearOperator<Matrix, Vector>::real_type;
    using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar, std::complex<Real>>::type;

  public:
    using matrix_type = Matrix;
    using vector_type = Vector;
    using scalar_type = Scalar;

  private:
    Matrix _A;
    CHAM_desc_t* _chamA = nullptr;
    CHAM_desc_t* _chamIPIV = nullptr;
    CHAM_desc_t* _chamT = nullptr;
    bool _is_setup = false;
    bool _is_facto = false;
    cham_uplo_t _facto_uplo = ChamLower; // storage location if symmetric/triangular matrix
    /* some operations are not available in chameleon for now, rely on lapack for them (getrf, sytrf) */
    IndexArray<Blas_int> _ipiv;
    lapack::Uplo _facto_uplo_lap = lapack::Uplo::Lower;
#+end_src

To make sure initialization occures only when the first instance is
create and finalization occures only when the last instance is
destructed, we keep a global count of the number of instaces through a
=shared_ptr= of the class handling initialization and finalization
(=Chameleon=), with a global variable initialized there.

#+begin_src c++
  std::shared_ptr<Chameleon> _cham_loc_ptr;

  void _init_cham_handle(){
    if(!Chameleon::_cham_glob_ptr){
      Chameleon::_cham_glob_ptr = std::make_shared<Chameleon>();
    } else {
      _cham_loc_ptr = Chameleon::_cham_glob_ptr;
    }
  }

  void _finalize_cham_handle(){
    if(Chameleon::_cham_glob_ptr.use_count() == 1){ Chameleon::_cham_glob_ptr.reset(); }
  }
#+end_src

** Constructors

#+begin_src c++
  public:
    ChameleonSolver(){}
    ChameleonSolver(const Matrix& A){
      setup(A);
    }
    ChameleonSolver(Matrix&& A){
      setup(A);
    }
    ~ChameleonSolver(){
      if (_chamT != nullptr){
        CHAMELEON_Desc_Destroy(&_chamT);
      }
      if (_chamIPIV != nullptr){
        CHAMELEON_Desc_Destroy(&_chamIPIV);
      }
      if (_chamA != nullptr){
        CHAMELEON_Desc_Destroy(&_chamA);
      }
      //_finalize_cham_handle();
    }
#+end_src

** Move operations

#+begin_src c++
public:
  ChameleonSolver(const ChameleonSolver&) = delete;
  ChameleonSolver& operator=(const ChameleonSolver&) = delete;

  ChameleonSolver(ChameleonSolver&&) = default;
  ChameleonSolver& operator=(ChameleonSolver&&) = default;
#+end_src

** Setup

#+begin_src c++
  private:
  inline cham_flttype_t getType() {
    if constexpr(is_real<Scalar>::value) {
      return is_precision_double<Scalar>::value ? ChamRealDouble : ChamRealFloat;
    } else {
      return is_precision_double<Scalar>::value ? ChamComplexDouble : ChamComplexFloat;
    }
  }

  public:
  void setup(const Matrix& A){
    //_init_cham_handle();
    _A = A;
    _is_setup = true;
    _is_facto = false;
  }
  void setup(Matrix&& A){
    //_init_cham_handle();
    _A = std::move(A);
    _is_setup = true;
    _is_facto = false;
  }
#+end_src

** Wrappers to Chameleon C functions

#+begin_src c++
private:
int lap_to_cham( Matrix& A, CHAM_desc_t* chamA ){
  cham_uplo_t uplo;
  if ( A.is_storage_full() ){
    uplo = ChamUpperLower;
  } else {
    uplo = A.is_storage_upper() ? ChamUpper : ChamLower;
  }
  int LDA = static_cast<int>(n_rows(A));
  int err = 0;
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      err = CHAMELEON_dlaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
      if (err !=0 ) { return err; }
      return CHAMELEON_dLap2Desc(uplo, get_ptr(A), LDA, chamA);
    } else {
      CHAMELEON_slaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
      if (err !=0 ) { return err; }
      return CHAMELEON_sLap2Desc(uplo, get_ptr(A), LDA, chamA);
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      CHAMELEON_zlaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
      if (err !=0 ) { return err; }
      return CHAMELEON_zLap2Desc(uplo, get_ptr(A), LDA, chamA);
    } else {
      CHAMELEON_claset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
      if (err !=0 ) { return err; }
      return CHAMELEON_cLap2Desc(uplo, get_ptr(A), LDA, chamA);
    }
  }
}

int geqrf(){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dgeqrf_Tile( _chamA, _chamT );
    } else {
      return CHAMELEON_sgeqrf_Tile( _chamA, _chamT );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zgeqrf_Tile( _chamA, _chamT );
    } else {
      return CHAMELEON_cgeqrf_Tile( _chamA, _chamT );
    }
  }
}
int geqrs( CHAM_desc_t* chamX ){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dgeqrs_Tile( _chamA, _chamT, chamX );
    } else {
      return CHAMELEON_sgeqrs_Tile( _chamA, _chamT, chamX );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zgeqrs_Tile( _chamA, _chamT, chamX );
    } else {
      return CHAMELEON_cgeqrs_Tile( _chamA, _chamT, chamX );
    }
  }
}

int gelqf(){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dgelqf_Tile( _chamA, _chamT );
    } else {
      return CHAMELEON_sgelqf_Tile( _chamA, _chamT );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zgelqf_Tile( _chamA, _chamT );
    } else {
      return CHAMELEON_cgelqf_Tile( _chamA, _chamT );
    }
  }
}
int gelqs( CHAM_desc_t* chamX ){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dgelqs_Tile( _chamA, _chamT, chamX );
    } else {
      return CHAMELEON_sgelqs_Tile( _chamA, _chamT, chamX );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zgelqs_Tile( _chamA, _chamT, chamX );
    } else {
      return CHAMELEON_cgelqs_Tile( _chamA, _chamT, chamX );
    }
  }
}

int getrf(){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dgetrf_Tile( _chamA, _chamIPIV );
    } else {
      return CHAMELEON_sgetrf_Tile( _chamA, _chamIPIV );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zgetrf_Tile( _chamA, _chamIPIV );
    } else {
      return CHAMELEON_cgetrf_Tile( _chamA, _chamIPIV );
    }
  }
}

int getrf_nopiv(){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dgetrf_nopiv_Tile( _chamA );
    } else {
      return CHAMELEON_sgetrf_nopiv_Tile( _chamA );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zgetrf_nopiv_Tile( _chamA );
    } else {
      return CHAMELEON_cgetrf_nopiv_Tile( _chamA );
    }
  }
}
int getrs_nopiv( CHAM_desc_t* chamX ){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dgetrs_nopiv_Tile( _chamA, chamX );
    } else {
      return CHAMELEON_sgetrs_nopiv_Tile( _chamA, chamX );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zgetrs_nopiv_Tile( _chamA, chamX );
    } else {
      return CHAMELEON_cgetrs_nopiv_Tile( _chamA, chamX );
    }
  }
}

int potrf(){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dpotrf_Tile( _facto_uplo, _chamA );
    } else {
      return CHAMELEON_spotrf_Tile( _facto_uplo, _chamA );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zpotrf_Tile( _facto_uplo, _chamA );
    } else {
      return CHAMELEON_cpotrf_Tile( _facto_uplo, _chamA );
    }
  }
}

int potrs( CHAM_desc_t* chamX ){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dpotrs_Tile( _facto_uplo, _chamA, chamX );
    } else {
      return CHAMELEON_spotrs_Tile( _facto_uplo, _chamA, chamX );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zpotrs_Tile( _facto_uplo, _chamA, chamX );
    } else {
      return CHAMELEON_cpotrs_Tile( _facto_uplo, _chamA, chamX );
    }
  }
}

int sytrf(){
  MAPHYSPP_ASSERT(is_complex<Scalar>::value == true, "ChameleonSolver::sytrf makes sense only for complex matrix");
  if constexpr(is_complex<Scalar>::value){
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zsytrf_Tile( _facto_uplo, _chamA );
    } else {
      return CHAMELEON_csytrf_Tile( _facto_uplo, _chamA );
    }
  }
  return 0;
}

int sytrs( CHAM_desc_t* chamX ){
  MAPHYSPP_ASSERT(is_complex<Scalar>::value == true, "ChameleonSolver::sytrs makes sense only for complex matrix");
  if constexpr(is_complex<Scalar>::value){
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_zsytrs_Tile( _facto_uplo, _chamA, chamX );
    } else {
      return CHAMELEON_csytrs_Tile( _facto_uplo, _chamA, chamX );
    }
  }
  return 0;
}

int trsm( cham_side_t side, cham_uplo_t uplo, cham_trans_t trans, cham_diag_t diag, Scalar alpha, CHAM_desc_t* chamX ){
  if constexpr(is_real<Scalar>::value) {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_dtrsm_Tile( side, uplo, trans, diag, alpha, _chamA, chamX );
    } else {
      return CHAMELEON_strsm_Tile( side, uplo, trans, diag, alpha, _chamA, chamX );
    }
  } else {
    if constexpr(is_precision_double<Scalar>::value) {
      return CHAMELEON_ztrsm_Tile( side, uplo, trans, diag, alpha, _chamA, chamX );
    } else {
      return CHAMELEON_ctrsm_Tile( side, uplo, trans, diag, alpha, _chamA, chamX );
    }
  }
}
#+end_src

** Factorization

The factorization type depends on the matrix properties. If $A$ is an $M$ by $N$ matrix:
- if $M > N$, we perform a $QR$ factorization
- if $M < N$, we perform an $LQ$ factorization
- if $A$ is symmetric positive definite, we perform an $LL^T$ factorization
- if $A$ is only symmetric, we perform an $LDL^T$ factorization
- otherwise, we perform an $LU$ factorization

#+begin_src c++
public:
void factorize(){
  MAPHYSPP_ASSERT(_is_setup == true, "ChameleonSolver::factorize: calling factorize before setup(A)");
  Timer<TIMER_SOLVER> t("Dense facto");
  int M = static_cast<int>(n_rows(_A));
  int N = static_cast<int>(n_cols(_A));
  int LDA = M;
  Blas_int Mb = static_cast<Blas_int>(n_rows(_A));
  Blas_int Nb = static_cast<Blas_int>(n_cols(_A));
  Blas_int LDAb = Mb;
  int NB;
  CHAMELEON_Get(CHAMELEON_TILE_SIZE, &NB);

  if(M == 0 || N == 0) return;

  int err = 0;

  /* Initialize specific chameleon structures wrapping data */
  err = CHAMELEON_Desc_Create( &_chamA, CHAMELEON_MAT_ALLOC_TILE, getType(), NB, NB, NB * NB, LDA, N, 0, 0, M, N, 1, 1 );
  if(err != 0){
    std::string message("Error: CHAMELEON_Desc_Create on _chamA ");
    message += std::string(" returned with value: ") + std::to_string(err);
    MAPHYSPP_ASSERT(err == 0, message);
  }
  /* Convert the Lapack matrix to a Chameleon tile matrix */
  err = lap_to_cham(_A, _chamA);
  if(err != 0){
    std::string message("Error: lap_to_cham on _A -> _chamA ");
    message += std::string(" returned with value: ") + std::to_string(err);
    MAPHYSPP_ASSERT(err == 0, message);
  }

  if (M != N){ // Rectangular
    err = CHAMELEON_Alloc_Workspace_zgels( M, N, &_chamT, 1, 1 );
    if(err != 0){
      std::string message("Error: CHAMELEON_Alloc_Workspace_zgels on _chamT ");
      message += std::string(" returned with value: ") + std::to_string(err);
      MAPHYSPP_ASSERT(err == 0, message);
    }
  } else if(_A.is_general()){ // Square general -> LU
    // TODO: use getrf (with partial pivoting as soon as it is supported in chameleon)
    //int minMN = chameleon_min( M, N );
    //err = CHAMELEON_Desc_Create( &_chamIPIV, CHAMELEON_MAT_ALLOC_TILE, ChamInteger, NB, 1, NB, minMN, 1, 0, 0, minMN, 1, 1, 1 );
    //if(err != 0){
    //  std::string message("Error: CHAMELEON_Desc_Create _chamIPIV ");
    //  message += std::string(" returned with value: ") + std::to_string(err);
    //  MAPHYSPP_ASSERT(err == 0, message);
    //}
  }

  /* Call the fine algorithm depending on the matrix properties */
  if(M > N){ // Rectangular -> least squares -> QR
    err = geqrf();
  }
  else if(M < N){ // Rectangular -> minimum norm -> LQ
    err = gelqf();
  }
  else if(_A.is_general()){ // Square general -> LU
    _ipiv = IndexArray<Blas_int>(std::min(Mb, Nb));
    err = lapack::getrf(Mb, Nb, get_ptr(_A), LDAb, &_ipiv[0]);
    // TODO: use getrf (with partial pivoting as soon as it is supported in chameleon)
    //err = getrf();
    //err = getrf_nopiv();
  }
  else{
    _facto_uplo = _A.is_storage_upper() ? ChamUpper : ChamLower;
    _facto_uplo_lap = _A.is_storage_upper() ? lapack::Uplo::Upper : lapack::Uplo::Lower;
    if(_A.is_spd()){ // Square SPD -> LLT
      err = potrf();
    }
    else{ // Square symmetric -> LDLT
      if constexpr(is_real<Scalar>::value) {
        MAPHYSPP_WARNING("ChameleonSolver::sytrf makes sense only for complex matrix, fallback lapack::sytrf");
        _ipiv = IndexArray<Blas_int>(Nb);
        err = lapack::sytrf(_facto_uplo_lap, Nb, get_ptr(_A), LDAb, &_ipiv[0]);
      } else {
        err = sytrf();
      }
    }
  }
  if(err != 0){
    std::string message("Error: ChameleonSolver::factorize ");
    if(M > N){ message += std::string("geqrf"); }
    else if(M < N){ message += std::string("gelqf"); }
    else if(_A.is_general()) { message += std::string("getrf"); }
    else if(_A.is_spd()) { message += std::string("potrf"); }
    else { message += std::string("sytrf"); }
    message += std::string(" returned with value: ") + std::to_string(err);
    MAPHYSPP_ASSERT(err == 0, message);
  }

  _is_facto = true;
}
#+end_src

** Solve

The solve step depends only the properties of the matrix, as for the
factorization step. We want so solve $Ax = b$ where $A$ is an $M$ by $N$ matrix,
$x$ the solution vector of size $N$ and $b$ the right hand side vector of size
$M$.
- if $M > N$, we have $QR$ factorization, we compute $y = Q^T x$ and then solve
  the triangular system $Rx = y$. We obtain the solution of the least squares
  system $||b - A x||^2$.
- if $M < N$, we have an $LQ$ factorization, we solve the triangular system $Ly
  = b$ and then compute $x = Q^T y$. We obtain the minimum norm solution of the
  least squares system $||b - A x||^2$.
- if $A$ is square, we have a decomposition of $A$ in two triangular matrices
  (and possibly a diagonal one), we find the unique solution by performing the
  triangular solves using LAPACK =??trs= methods.

#+begin_src c++
void apply(const Vector& B, Vector& X) {
  if(!_is_facto){
    factorize();
  }

  Timer<TIMER_SOLVER> t("Dense solve");

  int M = static_cast<int>(n_rows(_A));
  int N = static_cast<int>(n_cols(_A));
  int NRHS = static_cast<int>(n_cols(B));
  int LDB = static_cast<int>(n_rows(B));

  Blas_int Nb = static_cast<Blas_int>(n_cols(_A));
  Blas_int LDAb = M;
  Blas_int NRHSb = static_cast<Blas_int>(n_cols(B));
  Blas_int LDBb = static_cast<Blas_int>(n_rows(B));
  int NB;
  CHAMELEON_Get(CHAMELEON_TILE_SIZE, &NB);

  if(M == 0 or N == 0){
    X = Vector();
    return;
  }

  int err = 0;

  auto err_occured_cham = [](const std::string& fct_name, int ierr){
    if(ierr != 0){
      std::cerr << "Error: chameleon " << fct_name << " returned with value: " << ierr  << '\n';
      return true;
    }
    return false;
  };

  auto err_occured_lap = [](const std::string& fct_name, Blas_int ierr){
    if(ierr != 0){
      std::cerr << "Error: blas/lapack " << fct_name << " returned with value: " << ierr  << '\n';
      return true;
    }
    return false;
  };

  if(M > N){
    // Compute Q^T * B and solve R * X = (Q^T * B)
    Vector QB = B;
    auto LDBQ = M;
    CHAM_desc_t* chamQB;
    err = CHAMELEON_Desc_Create( &chamQB, get_ptr(QB), getType(), NB, NB, NB * NB, LDBQ, NRHS, 0, 0, M, NRHS, 1, 1 );
    err_occured_cham("CHAMELEON_Desc_Create on chamQB", err);
    err = geqrs( chamQB );
    err_occured_cham("geqrs", err);
    X = QB.get_block_copy(0, 0, N, 1);
    CHAMELEON_Desc_Destroy( &chamQB );
  }
  else if(M < N){
    // Solve L * Y = B and Compute X = Q^T * Y
    X = Vector(N);
    X.get_block_view(0, 0, M, 1) = B;
    CHAM_desc_t* chamX;
    err = CHAMELEON_Desc_Create( &chamX, get_ptr(X), getType(), NB, NB, NB * NB, N, NRHS, 0, 0, N, NRHS, 1, 1 );
    err = gelqs( chamX );
    err_occured_cham("gelqs", err);
    CHAMELEON_Desc_Destroy( &chamX );
  }
  else{ // A is square
    X = B;
    if(_A.is_general()){
      lapack::Op trans = lapack::Op::NoTrans;
      err = lapack::getrs(trans, Nb, NRHSb, get_ptr(_A), LDAb, &_ipiv[0], get_ptr(X), LDBb);
      err_occured_lap("getrs", err);
      // TODO: use getrf (with partial pivoting as soon as it is supported in chameleon)
      //err = getrs_nopiv();
      //err_occured("getrs", err);
    }
    else{
      if(_A.is_spd()){
        CHAM_desc_t* chamX;
        err = CHAMELEON_Desc_Create( &chamX, get_ptr(X), getType(), NB, NB, NB * NB, LDB, NRHS, 0, 0, N, NRHS, 1, 1 );
        err_occured_cham("CHAMELEON_Desc_Create on chamX", err);
        err = potrs( chamX );
        err_occured_cham("potrs", err);
        CHAMELEON_Desc_Destroy( &chamX );
      }
      else{
        if constexpr(is_real<Scalar>::value) {
          MAPHYSPP_WARNING("ChameleonSolver::sytrs makes sense only for complex matrix, fallback lapack::sytrs");
          err = lapack::sytrs(_facto_uplo_lap, Nb, NRHSb, get_ptr(_A), LDAb, &_ipiv[0], get_ptr(X), LDBb);
          err_occured_lap("sytrs", err);
        } else {
          CHAM_desc_t* chamX;
          err = CHAMELEON_Desc_Create( &chamX, get_ptr(X), getType(), NB, NB, NB * NB, LDB, NRHS, 0, 0, N, NRHS, 1, 1 );
          err_occured_cham("CHAMELEON_Desc_Create on chamX", err);
          err = sytrs( chamX );
          err_occured_cham("sytrs", err);
        }
      }
    }
  }
}

void solve(const Vector& B, Vector& X) {
  X = B;
  apply(B, X);
}

Vector apply(const Vector& B){
  Vector X = B; // Create X same size as B
  apply(B, X);
  return X;
}

Vector solve(const Vector& B) { return apply(B); }
#+end_src

** Triangular solve

#+begin_src c++
void triangular_solve(const Vector& B, Vector& X, MatrixStorage uplo, bool transposed = false){
  if(!_is_facto){
    factorize();
  }
  Timer<TIMER_SOLVER> t("Dense triangular solve");

  int N = static_cast<int>(n_cols(_A));
  int NRHS = static_cast<int>(n_cols(B));
  int LDB = static_cast<int>(n_rows(B));
  int NB;
  CHAMELEON_Get(CHAMELEON_TILE_SIZE, &NB);
  cham_side_t c_side = ChamLeft;
  cham_uplo_t c_uplo = (uplo == MatrixStorage::upper) ? ChamUpper : ChamLower;
  if(_A.is_sym_or_herm()){
    if(_facto_uplo == ChamLower){
      if(c_uplo == ChamUpper) MAPHYSPP_WARNING("ChameleonSolver::triangular_solve asked for upper part on sym/herm matrix factorized lower");
      c_uplo = ChamLower;
    }
    else{
      if(c_uplo == ChamLower) MAPHYSPP_WARNING("ChameleonSolver::triangular_solve asked for lower part on sym/herm matrix factorized upper");
      c_uplo = ChamUpper;
    }
  }
  cham_trans_t c_trans = transposed ? ChamTrans : ChamNoTrans;
  if(transposed and is_complex<Scalar>::value) c_trans = ChamConjTrans;
  const cham_diag_t c_diag = ChamNonUnit;

  int err = 0;

  auto err_occured_cham = [](const std::string& fct_name, int ierr){
    if(ierr != 0){
      std::cerr << "Error: ChameleonSolver::triangular_solve " << fct_name << " returned with value: " << ierr  << '\n';
      return true;
    }
    return false;
  };

  X = B;
  CHAM_desc_t* chamX;
  err = CHAMELEON_Desc_Create( &chamX, get_ptr(X), getType(), NB, NB, NB * NB, LDB, NRHS, 0, 0, N, NRHS, 1, 1 );
  err_occured_cham("CHAMELEON_Desc_Create on chamX", err);

  Scalar alpha{1};
  err = trsm( c_side, c_uplo, c_trans, c_diag, alpha, chamX );
  err_occured_cham("trsm", err);
  err = CHAMELEON_Desc_Destroy( &chamX );
  err_occured_cham("CHAMELEON_Desc_Destroy on chamX", err);
}

Vector triangular_solve(const Vector& B, MatrixStorage uplo, bool transposed = false){
  Vector X = B; // Create X same size as B
  triangular_solve(B, X, uplo, transposed);
  return X;
}
#+end_src

** Set some Chameleon parameters (threads, tile size)

#+begin_src c++
template<MPH_Integral Tint>
void set_n_tile(Tint n_tile){
  int NB = static_cast<int>(n_tile);
  CHAMELEON_Set(CHAMELEON_TILE_SIZE, NB);
}
#+end_src

** Display

#+begin_src c++
  void display(const std::string& name="", std::ostream &out = std::cout) const {
    if(!name.empty()) out << "Chameleon solver name: " << name << '\n';
    out << "On matrix: \n";
    _A.display("", out);
    out << "_is_setup: " << _is_setup << '\n';
    out << "_is_facto: " << _is_facto << '\n';
  }
#+end_src

** Traits

#+begin_src c++
  }; //class ChameleonSolver
#+end_src

#+begin_src c++
// Set traits
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_solver_direct<ChameleonSolver<Matrix, Vector>> : public std::true_type {};
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_solver_iterative<ChameleonSolver<Matrix, Vector>> : public std::false_type {};
template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct is_matrix_free<ChameleonSolver<Matrix, Vector>> : public std::false_type {};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct has_triangular_solve<ChameleonSolver<Matrix, Vector>> : public std::true_type {};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct vector_type<ChameleonSolver<Matrix, Vector>> : public std::true_type {
  using type = typename ChameleonSolver<Matrix, Vector>::vector_type;
};

template<MPH_DenseMatrix Matrix, MPH_Vector_Single_Or_Multiple Vector>
struct scalar_type<ChameleonSolver<Matrix, Vector>> : public std::true_type {
  using type = typename ChameleonSolver<Matrix, Vector>::scalar_type;
};
#+end_src

** Footer

#+begin_src c++
} // namespace maphys
#+end_src

* Tests
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../src/test/unittest/test_chameleonsolver.cpp :comments link
:END:

#+begin_src c++
  #include <maphys.hpp>
  #include <maphys/solver/ChameleonSolver.hpp>
  #include <maphys/testing/TestMatrix.hpp>
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>

  using namespace maphys;

  TEMPLATE_TEST_CASE("ChameleonSolver", "[dense][sequential][chameleon]", float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;
    using Real = typename arithmetic_real<Scalar>::type;
    using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar, std::complex<Real>>::type;

    // optional: set number of workers used by Chameleon (ncpus, ngpus and tile size)
    Chameleon::ncpus = 2;
    Chameleon::ngpus = 0;
    Chameleon::tile_size = 2;

    // Initialize Chameleon context
    Chameleon chameleon;

    const DenseMatrix<Scalar> A_gen = test_matrix::general_matrix<Scalar, DenseMatrix<Scalar>>().matrix;
    DenseMatrix<Scalar> A_spd({3, 2, 2, 1,
                               0, 4, 1, 1,
                               0, 0, 3, 2,
                               0, 0, 0, 4}, 4, 4, true);
    A_spd *= A_spd.t();
    A_spd.set_spd(MatrixStorage::full);

    DenseMatrix<Scalar> A_sym({1, 2, 3, 4,
                               2, 2, 3, 1,
                               3, 3, 4, 3,
                               4, 1, 3, 1}, 4, 4, true);

    A_sym.set_property(MatrixSymmetry::symmetric);

    const Real tol = arithmetic_tolerance<Scalar>::value;
#+end_src

** Square systems

#+begin_src c++
  SECTION("Single solve Ax=b, A full"){
    auto& A = A_gen;
    Vector<Scalar> b{1,2,3,4};
    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> s;
    s.setup(A);

    Vector<Scalar> x = s * b;

    Vector<Scalar> direct_err = A * x - b;
    REQUIRE(direct_err.norm() <= tol);
  }

  SECTION("Single solve Ax=b, A spd"){
    Vector<Scalar> b{1,2,3,4};
    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> s;
    s.setup(A_spd);

    Vector<Scalar> x = s * b;

    Vector<Scalar> direct_err = A_spd * x - b;
    REQUIRE(direct_err.norm() <= tol);
  }

  SECTION("Single solve Ax=b, A sym"){
    Vector<Scalar> b{1,2,3,4};
    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> s;
    s.setup(A_sym);

    Vector<Scalar> x = s * b;

    Vector<Scalar> direct_err = A_sym * x - b;
    REQUIRE(direct_err.norm() <= tol);
  }

  SECTION("Consecutive solve Ax=b with different b"){
    auto& A = A_gen;
    Vector<Scalar> b1{1,2,3,4};
    Vector<Scalar> b2{2,2,3,-7};
    Vector<Scalar> b3{1,-22,33,4};
    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> s;
    s.setup(A);

    Vector<Scalar> x1 = s * b1;
    Vector<Scalar> x2 = s * b2;
    Vector<Scalar> x3 = s * b3;

    Vector<Scalar> direct_err1 = A * x1 - b1;
    REQUIRE(direct_err1.norm() <= tol);
    Vector<Scalar> direct_err2 = A * x2 - b2;
    REQUIRE(direct_err2.norm() <= tol);
    Vector<Scalar> direct_err3 = A * x3 - b3;
    REQUIRE(direct_err3.norm() <= tol);
  }

  SECTION("Solve multirhs AX=B, A full, B is a matrix"){
    auto& A = A_gen;
    DenseMatrix<Scalar> B({1,  2, -10, 0,
                           2,  3,  2, -1,
                           4,  2,  3,  10}, 4, 3);

    ChameleonSolver<DenseMatrix<Scalar>, DenseMatrix<Scalar>> s;
    s.setup(A);
    DenseMatrix<Scalar> X = s * B;
    DenseMatrix<Scalar> direct_err = A * X - B;
    REQUIRE(direct_err.norm() <= tol);
  }

  SECTION("Triangular solve with SPD matrix"){
    ChameleonSolver<DenseMatrix<Scalar>, DenseMatrix<Scalar>> s;
    s.setup(A_spd);

    // Perform the solve in two steps
    // 1-  y <- solve L . y = b
    // 2-  x <- solve L^T x = y
    // And compare x with x_exp use to generate b = A * x_exp
    const Vector<Scalar> x_exp{1,2,3,4};
    const Vector<Scalar> b = A_spd * x_exp; // (80. 78. 89. 100.)

    const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-8 : 1e-4;
    const Vector<Scalar> y_exp{18.85618083, 8.66856112, 17.29826783, 13.15562323};
    auto y = s.triangular_solve(b, MatrixStorage::lower, false);
    Real direct_err = (y - y_exp).norm();
    REQUIRE(direct_err <= large_tol);
    auto x = s.triangular_solve(y, MatrixStorage::lower, true);
    direct_err = (x - x_exp).norm();
    REQUIRE(direct_err <= tol);
  }
#+end_src

** Overdetermined system

  Test least squares $x = argmin || b - A x ||^2_2$:

  $$A = \begin{pmatrix}
    1 & 2 \\
    3 & 4 \\
    5 & 6
  \end{pmatrix}, x = \begin{pmatrix}
  2 \\
  -1.5
  \end{pmatrix}, b = \begin{pmatrix}
  -1 \\
  0 \\
  1
  \end{pmatrix}$$

#+begin_src c++
  SECTION("Least square solve (A m x n with m > n)"){
    const DenseMatrix<Scalar> Ar({1, 3, 5, 2, 4, 6}, 3, 2);
    const Vector<Scalar> b({-1, 0, 1});
    const Vector<Scalar> x_exp({2, -1.5});

    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> solver(Ar);
    Vector<Scalar> x = solver * b;

    const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-6 : 1e-2;
    REQUIRE((x - x_exp).norm() < large_tol);
  }
#+end_src

** Underdetermined system

  Test minimal norm solution $x$ minimizing $|| b - A x ||^2_2$:

  $$A = \begin{pmatrix}
    1 & 3 & 5 \\
    2 & 4 & 6
  \end{pmatrix}, x = \begin{pmatrix}
  3.5 \\
  1 \\
  -1.5
  \end{pmatrix}, b = \begin{pmatrix}
  -1 \\
  2
  \end{pmatrix}$$

#+begin_src c++
  SECTION("Minimal norm (A m x n with m < n)"){
    const DenseMatrix<Scalar> Ar({1, 2, 3, 4, 5, 6}, 2, 3);
    const Vector<Scalar> b({-1, 2});
    const Vector<Scalar> x_exp({3.5, 1, -1.5});

    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> solver(Ar);
    Vector<Scalar> x = solver * b;

    const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-6 : 1e-1;
    REQUIRE((x - x_exp).norm() < large_tol);
  }
#+end_src

** Test chameleon initalization and finalization (multiple instances)

#+begin_src c++
  SECTION("Multiple nested instances"){
    const Vector<Scalar> b{1,2};

    const DenseMatrix<Scalar> M1({1,2,3,4}, 2,2);
    const DenseMatrix<Scalar> M2({1,1,3,4}, 2 ,2);

    const Vector<Scalar> x_exp1{1,0};
    const Vector<Scalar> x_exp2{-2,1};

    const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-8 : 1e-4;

    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> s1(M1);
    {
      ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> s2(M2);
      Vector<Scalar> x2 = s2 * b;
      REQUIRE((x2 - x_exp2).norm() < large_tol);
    }
    Vector<Scalar> x1 = s1 * b;

    REQUIRE((x1 - x_exp1).norm() < large_tol);
  }
#+end_src

** Footer

#+begin_src c++
}
#+end_src
