label:solver:chameleonkernels

* Class
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/kernel/ChameleonKernels.hpp :comments link
:END:

** Header

#+begin_src c++
  #pragma once
  #include <cctype>

  #include <chameleon.h>
  #include "maphys/utils/Chameleon.hpp"
  #include "maphys/kernel/BlasKernels.hpp"
  #include "maphys/solver/ChameleonSolver.hpp"

  namespace maphys {
    struct chameleon_kernels : blas_kernels {
#+end_src

** Blas 3

#+begin_src c++
  // C <- alpha * opA(A) * opB(B) + beta * C
  template<BlasKernelMatrix Matrix, MPH_Scalar Scalar = typename Matrix::scalar_type>
  static void gemm(const Matrix& A, const Matrix& B, Matrix& C, char opA = 'N', char opB = 'N', Scalar alpha = 1, Scalar beta = 0){

    using Vector = typename vector_type<Matrix>::type;

    opA = std::toupper(opA);
    opB = std::toupper(opB);
    const int opa_rows = static_cast<int>((opA == 'N') ? n_rows(A) : n_cols(A));
    const int opa_cols = static_cast<int>((opA == 'N') ? n_cols(A) : n_rows(A));
    const int opb_rows = static_cast<int>((opB == 'N') ? n_rows(B) : n_cols(B));
    const int opb_cols = static_cast<int>((opB == 'N') ? n_cols(B) : n_rows(B));

    MAPHYSPP_DIM_ASSERT(opa_cols, opb_rows, "Matrix matrix multiplication AB: nb cols(op(A)) != nb rows(op(B))");
    if(static_cast<int>(n_rows(C)) != opa_rows or static_cast<int>(n_cols(C)) != opb_cols){
      MAPHYSPP_ASSERT(beta == Scalar{0}, "ChameleonKernels::gemm wrong dimensions for C (and beta != 0)");
      C = Matrix(opa_rows, opb_cols);
    }

    const int ldc = static_cast<int>(get_leading_dim(C));

    auto getType = [](Scalar a) {
      if constexpr(is_real<Scalar>::value) {
        return is_precision_double<Scalar>::value ? ChamRealDouble : ChamRealFloat;
      } else {
        return is_precision_double<Scalar>::value ? ChamComplexDouble : ChamComplexFloat;
      }
    };

    auto lap_to_cham = []( const Matrix& A, CHAM_desc_t* chamA ){
      cham_uplo_t uplo;
      if ( A.is_storage_full() ){
        uplo = ChamUpperLower;
      } else {
        uplo = A.is_storage_upper() ? ChamUpper : ChamLower;
      }
      int LDA = static_cast<int>(n_rows(A));
      int err = 0;
      if constexpr(is_real<Scalar>::value) {
        if constexpr(is_precision_double<Scalar>::value) {
          err = CHAMELEON_dlaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
          if (err !=0 ) { return err; }
          return CHAMELEON_dLap2Desc(uplo, (Scalar*)get_ptr(A), LDA, chamA);
        } else {
          CHAMELEON_slaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
          if (err !=0 ) { return err; }
          return CHAMELEON_sLap2Desc(uplo, (Scalar*)get_ptr(A), LDA, chamA);
        }
      } else {
        if constexpr(is_precision_double<Scalar>::value) {
          CHAMELEON_zlaset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
          if (err !=0 ) { return err; }
          return CHAMELEON_zLap2Desc(uplo, (Scalar*)get_ptr(A), LDA, chamA);
        } else {
          CHAMELEON_claset_Tile(uplo, static_cast<Scalar>(0.), static_cast<Scalar>(0.), chamA);
          if (err !=0 ) { return err; }
          return CHAMELEON_cLap2Desc(uplo, (Scalar*)get_ptr(A), LDA, chamA);
        }
      }
    };

    auto cham_to_lap = []( CHAM_desc_t* chamA, Matrix& A ){
      cham_uplo_t uplo;
      if ( A.is_storage_full() ){
        uplo = ChamUpperLower;
      } else {
        uplo = A.is_storage_upper() ? ChamUpper : ChamLower;
      }
      int LDA = static_cast<int>(n_rows(A));
      if constexpr(is_real<Scalar>::value) {
        if constexpr(is_precision_double<Scalar>::value) {
          return CHAMELEON_dDesc2Lap(uplo, chamA, get_ptr(A), LDA);
        } else {
          return CHAMELEON_sDesc2Lap(uplo, chamA, get_ptr(A), LDA);
        }
      } else {
        if constexpr(is_precision_double<Scalar>::value) {
          return CHAMELEON_zDesc2Lap(uplo, chamA, get_ptr(A), LDA);
        } else {
          return CHAMELEON_cDesc2Lap(uplo, chamA, get_ptr(A), LDA);
        }
      }
    };

    auto cham_gemm = []( cham_trans_t transA, cham_trans_t transB, Scalar alpha, CHAM_desc_t* A, CHAM_desc_t* B, Scalar beta, CHAM_desc_t* C ){
      if constexpr(is_real<Scalar>::value) {
        if constexpr(is_precision_double<Scalar>::value) {
          return CHAMELEON_dgemm_Tile( transA, transB, alpha, A, B, beta, C );
        } else {
          return CHAMELEON_sgemm_Tile( transA, transB, alpha, A, B, beta, C );
        }
      } else {
        if constexpr(is_precision_double<Scalar>::value) {
          return CHAMELEON_zgemm_Tile( transA, transB, alpha, A, B, beta, C );
        } else {
          return CHAMELEON_cgemm_Tile( transA, transB, alpha, A, B, beta, C );
        }
      }
    };

    auto cham_symm = []( cham_side_t side, cham_uplo_t uplo, Scalar alpha, CHAM_desc_t* A, CHAM_desc_t* B, Scalar beta, CHAM_desc_t* C ){
      if constexpr(is_real<Scalar>::value) {
        if constexpr(is_precision_double<Scalar>::value) {
          return CHAMELEON_dsymm_Tile( side, uplo, alpha, A, B, beta, C );
        } else {
          return CHAMELEON_ssymm_Tile( side, uplo, alpha, A, B, beta, C );
        }
      } else {
        if constexpr(is_precision_double<Scalar>::value) {
          return CHAMELEON_zsymm_Tile( side, uplo, alpha, A, B, beta, C );
        } else {
          return CHAMELEON_csymm_Tile( side, uplo, alpha, A, B, beta, C );
        }
      }
    };

    auto cham_hemm = []( cham_side_t side, cham_uplo_t uplo, Scalar alpha, CHAM_desc_t* A, CHAM_desc_t* B, Scalar beta, CHAM_desc_t* C ){
      if constexpr(is_complex<Scalar>::value) {
        if constexpr(is_precision_double<Scalar>::value) {
          return CHAMELEON_zhemm_Tile( side, uplo, alpha, A, B, beta, C );
        } else {
          return CHAMELEON_chemm_Tile( side, uplo, alpha, A, B, beta, C );
        }
      }
    };

    int NB;
    CHAMELEON_Get(CHAMELEON_TILE_SIZE, &NB);

    if(is_storage_full(A) and is_storage_full(B)){
      auto op_blas = [](char op){
        if(op == 'T') return ChamTrans;
        if(op == 'C') return ChamConjTrans;
        return ChamNoTrans;
      };

      const int lda = static_cast<int>(get_leading_dim(A));
      const int ldb = static_cast<int>(get_leading_dim(B));

      /* Initialize specific chameleon structures wrapping data */
      CHAM_desc_t* chamA;
      CHAMELEON_Desc_Create( &chamA, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, lda, opa_cols, 0, 0, opa_rows, opa_cols, 1, 1 );
      CHAM_desc_t* chamB;
      CHAMELEON_Desc_Create( &chamB, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ldb, opb_cols, 0, 0, opa_cols, opb_cols, 1, 1 );
      CHAM_desc_t* chamC;
      CHAMELEON_Desc_Create( &chamC, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ldc, opb_cols, 0, 0, opa_rows, opb_cols, 1, 1 );

      lap_to_cham(A, chamA);
      lap_to_cham(B, chamB);
      lap_to_cham(C, chamC);

      cham_gemm( op_blas(opA), op_blas(opB), alpha, chamA, chamB, beta, chamC );

      cham_to_lap( chamC, C );
    }
    else { // A or B half-stored

      cham_side_t side       = (is_storage_full(B)) ? ChamLeft : ChamRight;
      const Matrix& mat_half = (is_storage_full(B)) ? A : B;
      const Matrix& mat_full = (is_storage_full(B)) ? B : A;

      const int ld_half = static_cast<int>(get_leading_dim(mat_half));
      const int ld_full = static_cast<int>(get_leading_dim(mat_full));

      cham_uplo_t uplo = (is_storage_lower(mat_half)) ? ChamLower : ChamUpper;
      const int M_half = static_cast<int>(n_rows(mat_half));
      const int N_full = static_cast<int>(n_cols(mat_full));

      /* Initialize specific chameleon structures wrapping data */
      CHAM_desc_t* chamA;
      CHAMELEON_Desc_Create( &chamA, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ld_half, M_half, 0, 0, M_half, M_half, 1, 1 );
      CHAM_desc_t* chamB;
      CHAMELEON_Desc_Create( &chamB, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ld_full, N_full, 0, 0, M_half, N_full, 1, 1 );
      CHAM_desc_t* chamC;
      CHAMELEON_Desc_Create( &chamC, CHAMELEON_MAT_ALLOC_TILE, getType(alpha), NB, NB, NB * NB, ldc, opb_cols, 0, 0, opa_rows, opb_cols, 1, 1 );

      lap_to_cham(mat_half, chamA);
      lap_to_cham(mat_full, chamB);
      lap_to_cham(C, chamC);

      if(is_symmetric(mat_half)){

        cham_symm( side, uplo, alpha, chamA, chamB, beta, chamC );

      } else if(is_hermitian(mat_half)){

        cham_hemm( side, uplo, alpha, chamA, chamB, beta, chamC );

      } else{
        MAPHYSPP_ASSERT(false, "ChameleonKernels::gemm matrix is half stored but not sym/herm");
      }

      cham_to_lap( chamC, C );

    }
  }
#+end_src

** Footer

#+begin_src c++
  }; // struct chameleon_kernels
  } // namespace maphys
#+end_src

* Tests
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../src/test/unittest/test_chameleon_kernels.cpp :comments link
:END:

#+begin_src c++
  #include <maphys.hpp>
  #include <maphys/kernel/ChameleonKernels.hpp>
  #include <maphys/loc_data/DenseMatrix.hpp>
  #include <maphys/testing/TestMatrix.hpp>
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>
  #include <maphys/testing/Catch2DenseMatrixMatchers.hpp>

  using namespace maphys;

  TEMPLATE_TEST_CASE("ChameleonKernels", "[dense][sequential][chameleon][kernel]", float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;
    using EqualsMatrixPW = maphys::EqualsMatrixPW<DenseMatrix<Scalar>>;

    // optional: set number of workers used by Chameleon (ncpus, ngpus and tile size)
    Chameleon::ncpus = 2;
    Chameleon::ngpus = 0;
    Chameleon::tile_size = 2;

    // Initialize Chameleon context
    Chameleon chameleon;

    //using Real = typename arithmetic_real<Scalar>::type;
    //using Complex = typename arithmetic_complex<Scalar>::type;
    //constexpr const Real ev_tol = (is_precision_double<Scalar>::value) ? 1e-12 : 1e-5;
#+end_src

** Blas 3

#+begin_src c++
  SECTION("Blas 3 - GEMM - general"){
    const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
    const DenseMatrix<Scalar> B({1.0,  2.0, 3.0,  4.0, 5.0,  6.0}, 3, 2);

    //            1 4
    //  1  2  3 x 2 5 =  14  32
    // -1 -2 -3   3 6   -14 -32

    const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
    DenseMatrix<Scalar> C(maphys::n_rows(A), maphys::n_cols(B));

    chameleon_kernels::gemm(A, B, C);
    REQUIRE_THAT(C_check, EqualsMatrixPW(C));
  }

  SECTION("Blas 3 - GEMM - symmetric / hermitian"){
    DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

    DenseMatrix<Scalar> ML({-14,-16,-18, 5, 7, 9, 36, 39, 42}, 3,3);
    DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
    if constexpr(maphys::is_complex<Scalar>::value){
      M += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 1,
                                               -1, 2, -2,
                                               1, -1, -0.5}, 3, 3);
      L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(maphys::MatrixSymmetry::hermitian);
      ML = DenseMatrix<Scalar>({{-16, -3}, {-13, 11}, {-19.5, -0.5}, {6, -9}, {6, -19}, {7, -11.5}, {39, 8}, {34, 8}, {47, 1.5}}, 3, 3);
      LM = DenseMatrix<Scalar>({{-4, -7}, {2, 9}, {11, -2}, {-16, 10}, {12, 2}, {28, -11}, {-17.5, -2.5}, {17, 13.5}, {29, -15.5}}, 3, 3);
    }

    DenseMatrix<Scalar> my_ML(3, 3);
    chameleon_kernels::gemm(M, L, my_ML);
    REQUIRE_THAT(ML, EqualsMatrixPW(my_ML));

    DenseMatrix<Scalar> my_LM(3, 3);
    chameleon_kernels::gemm(L, M, my_LM);
    REQUIRE_THAT(LM, EqualsMatrixPW(my_LM));
  }
#+end_src

** Footer

#+begin_src c++
  }
#+end_src
