label:kernel:spkernel

* Header

#+begin_src c++ :results silent :tangle ../../../include/maphys/kernel/spkernel.hpp :comments link
  #pragma once

  #include <memory>
  #include <cassert>
  #include <limits>
  #include <iostream>
  #include <iomanip>
  #include <functional>
  #include <string>

  #include "maphys/utils/Arithmetic.hpp"
  #include "maphys/utils/MatrixProperties.hpp"
  #include "maphys/utils/Error.hpp"
  #include "maphys/loc_data/SparseMatrixCOO.hpp"
  #include "maphys/loc_data/SparseMatrixCSC.hpp"
  #include "maphys/loc_data/DenseMatrix.hpp"

  #ifdef MAPHYSPP_USE_MKL_SPBLAS
  #ifdef MAPHYSPP_USE_RSB_SPBLAS
  #error "Can not use mkl or librsb at the same time"
  #endif
  #endif

  #ifdef MAPHYSPP_USE_RSB_SPBLAS
  #include <rsb.h>
  #include <blas_sparse.h>
  #endif

  #ifdef MAPHYSPP_USE_MKL_SPBLAS
  #ifndef INTEL_MKL_VERSION
  #ifndef MAPHYSPP_USE_FABULOUS
  #include <lapack.hh>
  #include <mkl.h>
  #else
  #define FABULOUS_USE_MKL_SPARSE
  #include <mkl_spblas.h>
  #endif
  #endif
  #endif


  namespace maphys {
#+end_src

* MKL kernels
  
If available, we can use MKL kernels for sparse matrix multiplication.

#+begin_src c++ :results silent :tangle ../../../include/maphys/kernel/spkernel.hpp :comments link
  #ifdef MAPHYSPP_USE_MKL_SPBLAS

  template<typename Scalar>
  struct mkl_spblas : public std::false_type {};

  template<> struct mkl_spblas<float>{
    template<typename... Args> static auto create_coomat(Args... args){ return mkl_sparse_s_create_coo(args...); }
    template<typename... Args> static auto create_cscmat(Args... args){ return mkl_sparse_s_create_csc(args...); }
    template<typename... Args> static auto matvect      (Args... args){ return mkl_sparse_s_mv(args...); }
    template<typename... Args> static auto matmat       (Args... args){ return mkl_sparse_s_mm(args...); }
    using valtype = float;
    constexpr static const float one = float{1};
    constexpr static const float zero = float{0};
  };
  template<> struct mkl_spblas<double>{
    template<typename... Args> static auto create_coomat(Args... args){ return mkl_sparse_d_create_coo(args...); }
    template<typename... Args> static auto create_cscmat(Args... args){ return mkl_sparse_d_create_csc(args...); }
    template<typename... Args> static auto matvect      (Args... args){ return mkl_sparse_d_mv(args...); }
    template<typename... Args> static auto matmat       (Args... args){ return mkl_sparse_d_mm(args...); }
    using valtype = double;
    constexpr static const double one = double{1};
    constexpr static const double zero = double{0};
  };
  template<> struct mkl_spblas<std::complex<float>>{
    template<typename... Args> static auto create_coomat(Args... args){ return mkl_sparse_c_create_coo(args...); }
    template<typename... Args> static auto create_cscmat(Args... args){ return mkl_sparse_c_create_csc(args...); }
    template<typename... Args> static auto matvect      (Args... args){ return mkl_sparse_c_mv(args...); }
    template<typename... Args> static auto matmat       (Args... args){ return mkl_sparse_c_mm(args...); }
    using valtype = MKL_Complex8;
    constexpr static const MKL_Complex8 one = MKL_Complex8{1, 0};
    constexpr static const MKL_Complex8 zero = MKL_Complex8{0, 0};
  };
  template<> struct mkl_spblas<std::complex<double>>{
    template<typename... Args> static auto create_coomat(Args... args){ return mkl_sparse_z_create_coo(args...); }
    template<typename... Args> static auto create_cscmat(Args... args){ return mkl_sparse_z_create_csc(args...); }
    template<typename... Args> static auto matvect      (Args... args){ return mkl_sparse_z_mv(args...); }
    template<typename... Args> static auto matmat       (Args... args){ return mkl_sparse_z_mm(args...); }
    using valtype = MKL_Complex16;
    constexpr static const MKL_Complex16 one = MKL_Complex16{1, 0};
    constexpr static const MKL_Complex16 zero = MKL_Complex16{0, 0};
  };

  template<typename Matrix>
  auto describe_matrix(const Matrix& mat){
    struct matrix_descr desc;

    if(mat.is_symmetric())      { desc.type = SPARSE_MATRIX_TYPE_SYMMETRIC; }
    else if(mat.is_hermitian()) { desc.type = SPARSE_MATRIX_TYPE_HERMITIAN; }
    else                        { desc.type = SPARSE_MATRIX_TYPE_GENERAL; }

    if(mat.is_storage_upper())      { desc.mode = SPARSE_FILL_MODE_UPPER; }
    else if(mat.is_storage_lower()) { desc.mode = SPARSE_FILL_MODE_LOWER; }
    else                            { desc.mode = SPARSE_FILL_MODE_FULL; }

    desc.diag = SPARSE_DIAG_NON_UNIT;
    return desc;
  }

  template<class SPMat, class DM> // SPMat is CSC or COO
  inline void mkl_sparse_matrix_dense_matrix_product(const SPMat& sp_mat, const DM& dm_in, DM& dm_out){
    using Index = typename SPMat::index_type;
    using Scalar = typename SPMat::scalar_type;
    using MKLSP = mkl_spblas<Scalar>;
    using MKLSCALAR = typename MKLSP::valtype;

    MKL_INT m = static_cast<MKL_INT>(n_rows(sp_mat));
    MKL_INT n = static_cast<MKL_INT>(n_cols(sp_mat));
    sparse_matrix_t matmkl;

    Index * i_ptr = const_cast<Index *>(sp_mat.get_i_ptr());
    Index * j_ptr = const_cast<Index *>(sp_mat.get_j_ptr());
    MKLSCALAR * v_ptr = (MKLSCALAR *) (sp_mat.get_v_ptr());

    if constexpr(std::is_same_v<SPMat, SparseMatrixCOO<Scalar, Index>>){
      MKL_INT nnz = static_cast<MKL_INT>(n_nonzero(sp_mat));
      MKLSP::create_coomat(&matmkl, SPARSE_INDEX_BASE_ZERO, m, n, nnz, i_ptr, j_ptr, v_ptr);
    }
    else if(std::is_same_v<SPMat, SparseMatrixCSC<Scalar, Index>>){
      MKLSP::create_cscmat(&matmkl, SPARSE_INDEX_BASE_ZERO, m, n, j_ptr, &(j_ptr[1]), i_ptr, v_ptr);
    }

    const struct matrix_descr desc = describe_matrix(sp_mat);

    const MKLSCALAR * in_v = (const MKLSCALAR *) (dm_in.get_ptr());
    MKLSCALAR * out_v = (MKLSCALAR *) (dm_out.get_ptr());
    constexpr const MKLSCALAR alpha = MKLSP::one;
    constexpr const MKLSCALAR beta = MKLSP::zero;

    if(dm_in.get_n_cols() == 1){ // Matrix vector product
      MKLSP::matvect(SPARSE_OPERATION_NON_TRANSPOSE, alpha, matmkl, desc, in_v, beta, out_v);
    }
    else{ // Matrix matrix product
       const MKL_INT ncol_out = static_cast<MKL_INT>(dm_in.get_n_cols());
      // Matrix prdouct function not supported with theses formats yet
      /*
      auto info = MKLSP::matmat(SPARSE_OPERATION_NON_TRANSPOSE, alpha, matmkl, desc, SPARSE_LAYOUT_ROW_MAJOR, in_v, ncol_out,
                  get_leading_dim(dm_in), beta, out_v, get_leading_dim(dm_out));
      */
      for(MKL_INT irhs = 0; irhs < ncol_out; irhs++){
        const MKLSCALAR * in_v_irhs = (const MKLSCALAR *) (dm_in.get_ptr(0, irhs));
        out_v = (MKLSCALAR *) (dm_out.get_ptr(0, irhs));
        MKLSP::matvect(SPARSE_OPERATION_NON_TRANSPOSE, alpha, matmkl, desc, in_v_irhs, beta, out_v);
      }
    }
  }
  #endif // not MAPHYSPP_USE_MKL_SPBLAS
#+end_src

* Librsb kernels

If available, we can use librsb kernels for sparse matrix multiplication.

#+begin_src c++ :results silent :tangle ../../../include/maphys/kernel/spkernel.hpp :comments link
  #ifdef MAPHYSPP_USE_RSB_SPBLAS

  template<typename Scalar>
  struct rsb_spblas : public std::false_type {};

  template<> struct rsb_spblas<float>{
    template<typename... Args> static auto start_create(Args... args){ return BLAS_suscr_begin(args...); }
    template<typename... Args> static auto insert_data (Args... args){ return BLAS_suscr_insert_entries(args...); }
    template<typename... Args> static auto end_create  (Args... args){ return BLAS_suscr_end(args...); }
    template<typename... Args> static auto matvect     (Args... args){ return BLAS_susmv(args...); }
    template<typename... Args> static auto matmat      (Args... args){ return BLAS_susmm(args...); }
    using valtype = float;
    constexpr static const float one = float{1};
    constexpr static const float zero = float{0};
  };
  template<> struct rsb_spblas<double>{
    template<typename... Args> static auto start_create(Args... args){ return BLAS_duscr_begin(args...); }
    template<typename... Args> static auto insert_data (Args... args){ return BLAS_duscr_insert_entries(args...); }
    template<typename... Args> static auto end_create  (Args... args){ return BLAS_duscr_end(args...); }
    template<typename... Args> static auto matvect     (Args... args){ return BLAS_dusmv(args...); }
    template<typename... Args> static auto matmat      (Args... args){ return BLAS_dusmm(args...); }
    using valtype = double;
    constexpr static const double one = double{1};
    constexpr static const double zero = double{0};
  };
  template<> struct rsb_spblas<std::complex<float>>{
    template<typename... Args> static auto start_create(Args... args){ return BLAS_cuscr_begin(args...); }
    template<typename... Args> static auto insert_data (Args... args){ return BLAS_cuscr_insert_entries(args...); }
    template<typename... Args> static auto end_create  (Args... args){ return BLAS_cuscr_end(args...); }
    template<typename... Args>
    static auto matvect(enum blas_trans_type transA, const std::complex<float> alpha,
			blas_sparse_matrix A, const std::complex<float> *x, int incx,
			std::complex<float> *y, int incy){
      return BLAS_cusmv(transA, (const void *) &alpha, A, (const void *) x, incx, (void *) y, incy);
    }
    template<typename... Args>
    static auto matmat(enum blas_order_type order, enum blas_trans_type transA,
		       int nrhs, const std::complex<float> alpha, blas_sparse_matrix A,
		       const std::complex<float> * b, int ldb, std::complex<float> * c, int ldc){
      return BLAS_cusmm(order, transA, nrhs, (const void *) &alpha, A, (const void *) b, ldb, (void *) c, ldc);
    }
    using valtype = std::complex<float>;
    constexpr static const std::complex<float> one = std::complex<float>{1, 0};
    constexpr static const std::complex<float> zero = std::complex<float>{0, 0};
  };
  template<> struct rsb_spblas<std::complex<double>>{
    template<typename... Args> static auto start_create(Args... args){ return BLAS_zuscr_begin(args...); }
    template<typename... Args> static auto insert_data (Args... args){ return BLAS_zuscr_insert_entries(args...); }
    template<typename... Args> static auto end_create  (Args... args){ return BLAS_zuscr_end(args...); }
    template<typename... Args>
    static auto matvect(enum blas_trans_type transA, const std::complex<double> alpha,
			blas_sparse_matrix A, const std::complex<double> *x, int incx,
			std::complex<double> *y, int incy){
      return BLAS_zusmv(transA, (const void *) &alpha, A, (const void *) x, incx, (void *) y, incy);
    }
    template<typename... Args>
    static auto matmat(enum blas_order_type order, enum blas_trans_type transA,
		       int nrhs, const std::complex<double> alpha, blas_sparse_matrix A,
		       const std::complex<double> * b, int ldb, std::complex<double> * c, int ldc){
      return BLAS_zusmm(order, transA, nrhs, (const void *) &alpha, A, (const void *) b, ldb, (void *) c, ldc);
    }
    using valtype = std::complex<double>;
    constexpr static const std::complex<double> one = std::complex<double>{1, 0};
    constexpr static const std::complex<double> zero = std::complex<double>{0, 0};
  };


  template<typename Matrix>
  void describe_matrix(const Matrix& mat, blas_sparse_matrix &A){
    int err = 0;
    if(mat.is_symmetric()) {
      if(mat.is_storage_upper()) {
	//err += BLAS_ussp(A, blas_upper);
	err += BLAS_ussp(A, blas_upper_symmetric);
      } else if(mat.is_storage_lower()) {
	//err += BLAS_ussp(A, blas_lower);
	err += BLAS_ussp(A, blas_lower_symmetric);
      }
    }
    else if(mat.is_hermitian()) {
      if(mat.is_storage_upper()) {
	//BLAS_ussp(A, blas_upper);
	err += BLAS_ussp(A, blas_upper_hermitian);
      } else if(mat.is_storage_lower()) {
	//BLAS_ussp(A, blas_lower);
	err += BLAS_ussp(A, blas_lower_hermitian);
      }
    }
    
    err += BLAS_ussp(A, blas_zero_base);
    err += BLAS_ussp(A, blas_non_unit_diag);
    MAPHYSPP_ASSERT(err == 0, "Error in librsb set properties");
    
  }

  template<class SPMat, class DM> // SPMat is CSC or COO
  inline void rsb_sparse_matrix_dense_matrix_product(const SPMat& sp_mat, const DM& dm_in, DM& dm_out){
    //using Index = typename SPMat::index_type;
    using Scalar = typename SPMat::scalar_type;
    using RSBSP = rsb_spblas<Scalar>;
    using RSBSCALAR = typename RSBSP::valtype;

    int m = n_rows(sp_mat);
    int n = n_cols(sp_mat);
    int nnz = sp_mat.get_nnz();
    const int * i_ptr = sp_mat.get_i_ptr();
    const int * j_ptr = sp_mat.get_j_ptr();
    const RSBSCALAR * v_ptr = (const RSBSCALAR *) sp_mat.get_v_ptr();
    
    blas_sparse_matrix A = RSBSP::start_create(m, n);
    MAPHYSPP_ASSERT(A != blas_invalid_handle, "Error in librsb matrix creation");
    BLAS_ussp(A, blas_rsb_rep_coo);
    /*if constexpr(std::is_same_v<SPMat, SparseMatrixCOO<Scalar, Index>>){
      BLAS_ussp(A, blas_rsb_rep_coo);
    }
    else if(std::is_same_v<SPMat, SparseMatrixCSC<Scalar, Index>>){
      MAPHYSPP_ASSERT(false, "CSC format not supported by librsb");
    }*/
    describe_matrix(sp_mat, A);

    int err = RSBSP::insert_data(A, nnz, v_ptr, i_ptr, j_ptr);
    MAPHYSPP_ASSERT(err == 0, "Error in librsb insert data");
    MAPHYSPP_ASSERT(RSBSP::end_create(A) != blas_invalid_handle, "Error in librsb end matrix creation");
    
    
    
    const RSBSCALAR * in_v = (const RSBSCALAR *) (dm_in.get_ptr());
    RSBSCALAR * out_v = (RSBSCALAR *) (dm_out.get_ptr());
    constexpr const RSBSCALAR alpha = RSBSP::one;
    //constexpr const RSBSCALAR beta = RSBSP::zero;
    const int nrhs = dm_in.get_n_cols();
    //std::memset(out_v, 0, nrhs * dm_in.get_n_rows() * sizeof(RSBSCALAR));
    
    
    if(nrhs == 1){ // Matrix vector product
      err = RSBSP::matvect(blas_no_trans, alpha, A, in_v, 1, out_v, 1);
      MAPHYSPP_ASSERT(err == 0, "Error in librsb matvect");
    }
    else{ // Matrix matrix product
      err = RSBSP::matmat(blas_colmajor, blas_no_trans, nrhs, alpha, A, in_v, dm_in.get_leading_dim(), out_v, dm_out.get_leading_dim());
      MAPHYSPP_ASSERT(err == 0, "Error in librsb matmat");
    }
    // free A
    MAPHYSPP_ASSERT(BLAS_usds(A) == 0, "Error in librsb matrix destruction");
  }
  #endif // not MAPHYSPP_USE_RSB_SPBLAS
#+end_src


* Matrix product: COO Matrix x DenseMatrix (or vector)

#+begin_src c++ :results silent :tangle ../../../include/maphys/kernel/spkernel.hpp :comments link
  template<class Scalar, class Index, class DM>
  inline void sparse_matrix_dense_matrix_product(const SparseMatrixCOO<Scalar, Index>& sp_mat, const DM& dm_in, DM& dm_out){

    Timer<10000> t{"kernel: COOxDense"};
    // Y(dense) <- M(sparse) * X(dense)
    // Dimensions:
    // (M x N) <- (M x K) * (K x N)

    const Size M = static_cast<Size>(sp_mat.get_n_rows());
    const Size K = static_cast<Size>(sp_mat.get_n_cols());
    const Size N = static_cast<Size>(dm_in.get_n_cols());

    MAPHYSPP_ASSERT( M == static_cast<Size>(dm_out.get_n_rows()), "Sp mat x dense mat: wrong M dimension");
    MAPHYSPP_ASSERT( N == static_cast<Size>(dm_out.get_n_cols()), "Sp mat x dense mat: wrong N dimension");
    MAPHYSPP_ASSERT( K == static_cast<Size>(dm_in.get_n_rows()) , "Sp mat x dense mat: wrong K dimension");

    const Index * Mi = sp_mat.get_i_ptr();
    const Index * Mj = sp_mat.get_j_ptr();
    const Scalar * Mv = sp_mat.get_v_ptr();
    const Size nnz = sp_mat.get_nnz();

    const Scalar * X = dm_in.get_ptr();
    const Size X_ld = dm_in.get_leading_dim();
    const Size Y_ld = dm_out.get_leading_dim();
    Scalar * Y = dm_out.get_ptr();

    // Full storage

    if(sp_mat.is_storage_full()){
      for (Size k = 0; k < nnz; ++k){
        auto& row = Mi[k];
        auto& col = Mj[k];
        auto& val = Mv[k];

        for(Size l = 0; l < N; ++l){
          Y[ l * Y_ld + row ] += val * X[ l * X_ld + col ];
        }
      }
    }
    else{
    // Half storage
      for (Size k = 0; k < nnz; ++k){
        auto& row = Mi[k];
        auto& col = Mj[k];
        auto& val = Mv[k];

        for(Size l = 0; l < N; ++l){
          Y[ l * Y_ld + row ] += val * X[ l * X_ld + col ];
        }

        if(row != col){
          for(Size l = 0; l < N; ++l){
            Y[ l * Y_ld + col ] += val * X[ l * X_ld + row ];
          }
        }
      }
    }
  }
#+end_src

* Matrix product: CSC Matrix x DenseMatrix (or vector)

#+begin_src c++ :results silent :tangle ../../../include/maphys/kernel/spkernel.hpp :comments link
  template<class Scalar, class Index, class DM>
  inline void sparse_matrix_dense_matrix_product(const SparseMatrixCSC<Scalar, Index>& sp_mat, const DM& dm_in, DM& dm_out){

    Timer<10000> t{"kernel: CSCxDense"};
    // Y(dense) <- M(sparse) * X(dense)
    // Dimensions:
    // (M x N) <- (M x K) * (K x N)

    const Size M = static_cast<Size>(sp_mat.get_n_rows());
    const Size K = static_cast<Size>(sp_mat.get_n_cols());
    const Size N = static_cast<Size>(dm_in.get_n_cols());

    MAPHYSPP_ASSERT( M == static_cast<Size>(dm_out.get_n_rows()), "Sp mat x dense mat: wrong M dimension");
    MAPHYSPP_ASSERT( N == static_cast<Size>(dm_out.get_n_cols()), "Sp mat x dense mat: wrong N dimension");
    MAPHYSPP_ASSERT( K == static_cast<Size>(dm_in.get_n_rows()) , "Sp mat x dense mat: wrong K dimension");

    const Index * Mia = sp_mat.get_i_ptr();
    const Index * Mja = sp_mat.get_j_ptr();
    const Scalar * Mv = sp_mat.get_v_ptr();

    const Scalar * X = dm_in.get_ptr();
    const Size X_ld = dm_in.get_leading_dim();
    const Size Y_ld = dm_out.get_leading_dim();
    Scalar * Y = dm_out.get_ptr();

    // Full storage

    if(sp_mat.is_storage_full()){
      for(Size k = 0; k < K; ++k){
        Index col = static_cast<Index>(k);
        for(Index k2 = Mja[k]; k2 < Mja[k+1]; ++k2){
          auto& row = Mia[k2];
          auto& val = Mv[k2];
          for(Size l = 0; l < N; ++l){
            Y[ l * Y_ld + row ] += val * X[ l * X_ld + col ];
          }
        }
      }
    }
    else{
      // Half storage
      for(Size k = 0; k < K; ++k){
        Index col = static_cast<Index>(k);
        for(Index k2 = Mja[k]; k2 < Mja[k+1]; ++k2){
          auto& row = Mia[k2];
          auto& val = Mv[k2];
          for(Size l = 0; l < N; ++l){
            Y[ l * Y_ld + row ] += val * X[ l * X_ld + col ];
          }

          if(row != col){
            for(Size l = 0; l < N; ++l){
              Y[ l * Y_ld + col ] += val * X[ l * X_ld + row ];
            }
          }
        }
      }
    }
  }
#+end_src

* Multiplication operators

  #+begin_src c++ :results silent :tangle ../../../include/maphys/kernel/spkernel.hpp :comments link
    template<class Scalar, class Index, class DM>
    inline DM operator* (const SparseMatrixCOO<Scalar, Index>& sp_mat, const DM& dm_in){
      DM dm_out(sp_mat.get_n_rows(), dm_in.get_n_cols());
    #ifdef MAPHYSPP_USE_MKL_SPBLAS
    // If Index is not MKL_INT, use other functions
    if constexpr(std::is_same_v<Index,MKL_INT>){
      mkl_sparse_matrix_dense_matrix_product(sp_mat, dm_in, dm_out);
    }
    else{
      sparse_matrix_dense_matrix_product(sp_mat, dm_in, dm_out);
    }
    #else 
    #ifdef MAPHYSPP_USE_RSB_SPBLAS
      rsb_sparse_matrix_dense_matrix_product(sp_mat, dm_in, dm_out);
    #else 
      sparse_matrix_dense_matrix_product(sp_mat, dm_in, dm_out);
    #endif
    #endif
      return dm_out;
    }

    template<class Scalar, class Index, class DM>
    inline DM operator* (const SparseMatrixCSC<Scalar, Index>& sp_mat, const DM& dm_in){
      DM dm_out(sp_mat.get_n_rows(), dm_in.get_n_cols());
    #ifdef MAPHYSPP_USE_MKL_SPBLAS
    // If Index is not MKL_INT, use other functions
    if constexpr(std::is_same_v<Index,MKL_INT>){
      mkl_sparse_matrix_dense_matrix_product(sp_mat, dm_in, dm_out);
    }
    else{
      sparse_matrix_dense_matrix_product(sp_mat, dm_in, dm_out);
    }
    #else
      sparse_matrix_dense_matrix_product(sp_mat, dm_in, dm_out);
    #endif
      return dm_out;
    }
    } // namespace maphys
  #+end_src
