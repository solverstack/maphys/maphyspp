label:solver:tlapackkernels

* Class
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/kernel/TlapackKernels.hpp :comments link
:END:

** Header

#+begin_src c++
  #pragma once
  #include <cctype>

  // Use span as a sliceable vector of int for tlapack
  #include <span>

  namespace tlapack {
    template<typename T>
    const std::span<T> slice(const std::span<T>& v, std::pair<int, int> idxs){
      auto& [i, f] = idxs;
      return v.subspan(i, f - i);
    }

    template<typename T>
    std::span<T> slice(std::span<T>& v, std::pair<int, int> idxs){
      auto& [i, f] = idxs;
      return v.subspan(i, f - i);
    }
  }

  #include <tlapack.hpp>

  namespace maphys {
#+end_src

** Concepts

Definition of the concepts of the object interfaces (matrix,
vector...) necessary to call the tlapack functions.

#+begin_src c++
  template<typename matrix>
  concept TlapackKernelMatrix = tlapack::concepts::Matrix<matrix> &&
    requires(matrix m)
  {
    { is_storage_full(m) }  -> std::same_as<bool>;
    { is_storage_lower(m) } -> std::same_as<bool>;
    { is_storage_upper(m) } -> std::same_as<bool>;
    { is_symmetric(m) }     -> std::same_as<bool>;
    { is_hermitian(m) }     -> std::same_as<bool>;
  };
#+end_src

#+begin_src c++
  struct tlapack_kernels {
#+end_src

** Blas 1

#+begin_src c++
  // X <- alpha * X (vector or matrix)
  template<tlapack::concepts::Vector Vector, MPH_Scalar Scalar>
  static void scal(Vector& X, Scalar alpha){
    tlapack::scal(alpha, X);
  }

  // Y <- alpha * X + Y
  template<tlapack::concepts::Vector Vector, MPH_Scalar Scalar>
  static void axpy(const Vector& X, Vector& Y, Scalar alpha){
    tlapack::axpy(alpha, X, Y);
  }

  // Return X dot Y
  template<tlapack::concepts::Vector Vector>
  [[nodiscard]] static MPH_Scalar auto dot(const Vector& X, const Vector& Y){
    return tlapack::dot(X, Y);
  }

  template<tlapack::concepts::Vector Vector>
  [[nodiscard]] static MPH_Real auto norm2_squared(const Vector& X){
    return std::real(tlapack::dot(X, X));
  }

  template<tlapack::concepts::Vector Vector>
  [[nodiscard]] static MPH_Real auto norm2(const Vector& X){
    return tlapack::nrm2(X);
  }
#+end_src

** Blas 2

#+begin_src c++
  // Y = alpha * op(A) * X + beta * Y
  template<TlapackKernelMatrix Matrix, tlapack::concepts::Vector Vector, MPH_Scalar Scalar = typename Vector::scalar_type>
  static void gemv(const Matrix& A, const Vector& X, Vector& Y, char opA = 'N', Scalar alpha = 1, Scalar beta = 0){
    const Size M = tlapack::nrows(A);
    const Size N = tlapack::ncols(A);
    opA = std::toupper(opA);

    const Size opa_rows = (opA == 'N') ? M : N;
    const Size opa_cols = (opA == 'N') ? N : M;

    MAPHYSPP_DIM_ASSERT(tlapack::nrows(X), opa_cols, "TlapackSolver::gemv dimensions of A and X do not match");
    if(tlapack::nrows(Y) != opa_rows){
      MAPHYSPP_ASSERT(beta == Scalar{0}, "TlapackSolver::gemv wrong dimensions for Y (and beta != 0)");
      Y = Vector(opa_rows); // Not captured by the concept of tlapack Vector
    }

    if(is_storage_full(A)){
      tlapack::Op op = tlapack::Op::NoTrans;
      if(opA == 'T') op = tlapack::Op::Trans;
      if(opA == 'C') op = tlapack::Op::ConjTrans;
      tlapack::gemv(op, alpha, A, X, beta, Y);
    } else {
      tlapack::Uplo uplo = (is_storage_lower(A)) ? tlapack::Uplo::Lower : tlapack::Uplo::Upper;
      if(is_symmetric(A)){
	tlapack::symv(uplo, alpha, A, X, beta, Y);
      }
      else if(is_hermitian(A)){
	if constexpr(is_complex<Scalar>::value){
	  tlapack::hemv(uplo, alpha, A, X, beta, Y);
	}
      }
      else {
	MAPHYSPP_ASSERT(false, "TlapackSolver::gemv matrix A is half stored but not sym/herm");
      }
    }
  }
#+end_src

** Blas 3

#+begin_src c++
  // C <- alpha * opA(A) * opB(B) + beta * C
  template<TlapackKernelMatrix Matrix, MPH_Scalar Scalar = typename Matrix::scalar_type>
  static void gemm(const Matrix& A, const Matrix& B, Matrix& C, char opA = 'N', char opB = 'N', Scalar alpha = 1, Scalar beta = 0){
    opA = std::toupper(opA);
    opB = std::toupper(opB);
    const Size opa_rows = (opA == 'N') ? tlapack::nrows(A) : tlapack::ncols(A);
    const Size opa_cols = (opA == 'N') ? tlapack::ncols(A) : tlapack::nrows(A);
    const Size opb_rows = (opB == 'N') ? tlapack::nrows(B) : tlapack::ncols(B);
    const Size opb_cols = (opB == 'N') ? tlapack::ncols(B) : tlapack::nrows(B);

    MAPHYSPP_DIM_ASSERT(opa_cols, opb_rows, "TlapackSolver::gemm dimensions of A and B do not match");
    if(tlapack::nrows(C) != opa_rows or tlapack::ncols(C) != opb_cols){
      MAPHYSPP_ASSERT(beta == Scalar{0}, "TlapackSolver::gemm wrong dimensions for C (and beta != 0)");
      C = Matrix(opa_rows, opb_cols);
    }

    if(is_storage_full(A) and is_storage_full(B)){
      auto op_blas = [](char op){
	if(op == 'T') return tlapack::Op::Trans;
	if(op == 'C') return tlapack::Op::ConjTrans;
	return tlapack::Op::NoTrans;
      };

      tlapack::gemm(op_blas(opA), op_blas(opB), alpha, A, B, beta, C);
    }
    else {

      const Matrix& sym_mat  = (is_storage_full(A)) ? B : A;
      const Matrix& full_mat = (is_storage_full(A)) ? A : B;
      tlapack::Side side     = (is_storage_full(A)) ? tlapack::Side::Right : tlapack::Side::Left;
      tlapack::Uplo uplo     = (is_storage_lower(sym_mat)) ? tlapack::Uplo::Lower : tlapack::Uplo::Upper;

      if(is_symmetric(sym_mat)){
	tlapack::symm(side, uplo, alpha, sym_mat, full_mat, beta, C);
      }
      else if(is_hermitian(sym_mat)){
	if constexpr(is_complex<Scalar>::value){
	  tlapack::hemm(side, uplo, alpha, sym_mat, full_mat, beta, C);
	}
      }
      else {
	MAPHYSPP_ASSERT(false, "TlapackSolver::gemm matrix is half stored but not sym/herm");
      }
    }
  }
#+end_src

** Eigenvalue decomposition


#+begin_src c++
  template<TlapackKernelMatrix Matrix,
	   TlapackKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Complex Complex = typename ComplexMatrix::scalar_type>
  [[nodiscard]] static std::pair<IndexArray<Complex>, ComplexMatrix>
  eig(const Matrix&) {
    MAPHYSPP_ASSERT(false, "Tlapack::eig not implemented");
    return std::pair<IndexArray<Complex>, ComplexMatrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   TlapackKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Complex Complex = typename ComplexMatrix::scalar_type>
  [[nodiscard]] static IndexArray<Complex>
  eigvals(const Matrix&) {
    MAPHYSPP_ASSERT(false, "Tlapack::eigvals not implemented");
    return IndexArray<Complex>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
  eigh(const Matrix&) {
    MAPHYSPP_ASSERT(false, "Tlapack::eigh not implemented");
    return std::pair<IndexArray<Real>, Matrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static IndexArray<Real>
  eigvalsh(const Matrix&) {
    MAPHYSPP_ASSERT(false, "Tlapack::eigvalsh not implemented");
    return IndexArray<Real>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
  eigh_select(const Matrix&, int, int) {
    MAPHYSPP_ASSERT(false, "Tlapack::eigh_select not implemented");
    return std::pair<IndexArray<Real>, DenseMatrix<Scalar>>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
  eigh_smallest(const Matrix&, int) {
    MAPHYSPP_ASSERT(false, "Tlapack::eigh_smallest not implemented");
    return std::pair<IndexArray<Real>, DenseMatrix<Scalar>>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
  eigh_largest(const Matrix&, int) {
    MAPHYSPP_ASSERT(false, "Tlapack::eigh_largest not implemented");
    return std::pair<IndexArray<Real>, DenseMatrix<Scalar>>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
  gen_eigh_select(const Matrix&, const Matrix&, int, int) {
    MAPHYSPP_ASSERT(false, "Tlapack::gen_eigh_select not implemented");
    return std::pair<IndexArray<Real>, Matrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
  gen_eigh_smallest(const Matrix&, const Matrix&, int) {
    MAPHYSPP_ASSERT(false, "Tlapack::gen_eigh_smallest not implemented");
    return std::pair<IndexArray<Real>, Matrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
  gen_eigh_largest(const Matrix&, const Matrix&, int) {
    MAPHYSPP_ASSERT(false, "Tlapack::gen_eigh_largest not implemented");
    return std::pair<IndexArray<Real>, Matrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, IndexArray<Real>, Matrix>
  svd(const Matrix&, bool reduced = true) {
    (void) reduced;
    MAPHYSPP_ASSERT(false, "Tlapack::svd not implemented");
    return std::tuple<Matrix, IndexArray<Real>, Matrix>();
  }

  // Returns U and sigmas only
  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<Matrix, IndexArray<Real>>
  svd_left_sv(const Matrix&, bool reduced = true) {
    (void) reduced;
    MAPHYSPP_ASSERT(false, "Tlapack::svd_left_sv not implemented");
    return std::pair<Matrix, IndexArray<Real>>();
  }

  // Returns sigmas and Vt only
  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<IndexArray<Real>, Matrix>
  svd_right_sv(const Matrix&, bool reduced = true) {
    (void) reduced;
    MAPHYSPP_ASSERT(false, "Tlapack::svd_right_sv not implemented");
    return std::pair<IndexArray<Real>, Matrix>();
  }

  // Returns only singular values
  template<TlapackKernelMatrix Matrix,
	   MPH_Scalar Scalar = typename Matrix::scalar_type,
	   MPH_Real Real = typename Matrix::real_type>
  [[nodiscard]] static IndexArray<Real>
  svdvals(const Matrix&) {
    MAPHYSPP_ASSERT(false, "Tlapack::svdvals not implemented");
    return std::pair<IndexArray<Real>, Matrix>();
  }
#+end_src

** Footer

#+begin_src c++
  }; // struct tlapack_kernels
  } // namespace maphys
#+end_src

* Tests
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../src/test/unittest/test_tlapack_kernels.cpp :comments link
:END:

#+begin_src c++
  #include <maphys.hpp>
  #include <maphys/kernel/TlapackKernels.hpp>
  #include <maphys/loc_data/DenseMatrix.hpp>
  #include <maphys/testing/TestMatrix.hpp>
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>

  using namespace maphys;

  TEMPLATE_TEST_CASE("TlapackKernels", "[dense][sequential][tlapack][kernel]", float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;
    using Real = typename arithmetic_real<Scalar>::type;
    //using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar, std::complex<Real>>::type;
#+end_src

** Blas 1

#+begin_src c++
  SECTION("Blas 1 - SCAL"){
    const Vector<Scalar> x({1, -1, 3});
    const DenseMatrix<Scalar> mat({1, 2, 3, 4}, 2, 2);    

    // Scal (*2)
    const Vector<Scalar> x_scal({2, -2, 6});

    Vector<Scalar> x2(x);
    tlapack_kernels::scal(x2, Scalar{2});
    REQUIRE(x2 == x_scal);

    // Test with increment
    DenseMatrix<Scalar> mat2(mat);
    Vector<Scalar> y = mat2.get_row_view(0);

    const Vector<Scalar> y_scal({2, 6});
    tlapack_kernels::scal(y, Scalar{2});

    REQUIRE(y == y_scal);
  }

  SECTION("Blas 1 - AXPY"){
    const Vector<Scalar> x({1, -1, 3});

    // AXPY (*2 + (-1,-1,-1)T)
    Vector<Scalar> y({-1, -1, -1});
    const Vector<Scalar> x2py({1, -3, 5});
    tlapack_kernels::axpy(x, y, Scalar{2});
    REQUIRE(y == x2py);
  }

  // dot
  SECTION("Blas 1 - DOT"){
    const Vector<Scalar> x({1, -1, 3});

    const Vector<Scalar> y({2, -4, -1});
    const Scalar xdoty{3};
    REQUIRE(tlapack_kernels::dot(x, y) == xdoty);
  }

  // norm2
  SECTION("Blas 1 - NORM2"){
    const Vector<Scalar> x({1, -1, 3});

    const Real normx_sq = 11;
    const Real normx = std::sqrt(normx_sq);
    REQUIRE(tlapack_kernels::norm2_squared(x) == normx_sq);
    REQUIRE(tlapack_kernels::norm2(x) == normx);
  }
#+end_src

** Blas 2

#+begin_src c++
  SECTION("Blas 2 -GEMV"){
    //static void gemv(const Matrix& A, const Vector& X, Vector& Y, Scalar alpha = 1, Scalar beta = 0)

    // 1 4        3
    // 2 5 x -1 = 3
    // 3 6    1   3
    const DenseMatrix<Scalar> MA({1, 2, 3, 4, 5, 6}, 3, 2);
    const Vector<Scalar> x({-1, 1});
    const Vector<Scalar> sol_check({3, 3, 3});
    Vector<Scalar> y(maphys::n_rows(MA));

    tlapack_kernels::gemv(MA, x, y);

    REQUIRE(sol_check == y);
  }

  SECTION("TLAPACK 2 - Gemv sym/herm"){
    Vector<Scalar> X({3, 2, 1});
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

    Vector<Scalar> LX({-2, 5, -2});
    if constexpr(maphys::is_complex<Scalar>::value){
      X += Scalar{0, 1} * Vector<Scalar>({1, 2, -1});
      L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(maphys::MatrixSymmetry::hermitian);
      LX = Vector<Scalar>({{-5, 7}, {8, -2}, {1, -7}});
    }

    Vector<Scalar> my_LX(3);
    tlapack_kernels::gemv(L, X, my_LX);
    REQUIRE(my_LX == LX);
  }

#+end_src

** Blas 3

#+begin_src c++
  SECTION("Blas 3 - GEMM - general"){
    const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
    const DenseMatrix<Scalar> B({1.0,  2.0, 3.0,  4.0, 5.0,  6.0}, 3, 2);

    //            1 4
    //  1  2  3 x 2 5 =  14  32
    // -1 -2 -3   3 6   -14 -32

    const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
    DenseMatrix<Scalar> C(maphys::n_rows(A), maphys::n_cols(B));

    tlapack_kernels::gemm(A, B, C);
    REQUIRE(C == C_check);
  }

  SECTION("Blas 3 - GEMM - symmetric / hermitian"){
    DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

    DenseMatrix<Scalar> ML({-14,-16,-18, 5, 7, 9, 36, 39, 42}, 3,3);
    DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
    if constexpr(maphys::is_complex<Scalar>::value){
      M += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 1,
	  -1, 2, -2,
	  1, -1, -0.5}, 3, 3);
      L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(maphys::MatrixSymmetry::hermitian);
      ML = DenseMatrix<Scalar>({{-16, -3}, {-13, 11}, {-19.5, -0.5}, {6, -9}, {6, -19}, {7, -11.5}, {39, 8}, {34, 8}, {47, 1.5}}, 3, 3);
      LM = DenseMatrix<Scalar>({{-4, -7}, {2, 9}, {11, -2}, {-16, 10}, {12, 2}, {28, -11}, {-17.5, -2.5}, {17, 13.5}, {29, -15.5}}, 3, 3);
    }

    DenseMatrix<Scalar> my_ML(3, 3);
    tlapack_kernels::gemm(M, L, my_ML);
    REQUIRE(my_ML == ML);

    DenseMatrix<Scalar> my_LM(3, 3);
    tlapack_kernels::gemm(L, M, my_LM);

    REQUIRE(my_LM == LM);
  }
#+end_src

** Footer

#+begin_src c++
  }
#+end_src
