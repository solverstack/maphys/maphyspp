label:preconditioner:twolevelas

* 2 level AbstractSchwarz preconditioners
:PROPERTIES:
:header-args: c++ :tangle ../../../include/maphys/precond/TwoLevelAbstractSchwarz.hpp :comments link
:END:

** Header

   #+begin_src c++
     #pragma once

     #include "maphys/solver/CoarseSolve.hpp"
     #include "maphys/solver/BlasSolver.hpp"
     #include "maphys/solver/CentralizedSolver.hpp"
     #include "maphys/kernel/BlasKernels.hpp"
     #include "maphys/linalg/SpectralExtraction.hpp"
     #ifdef MAPHYSPP_USE_ARPACK
     #include "maphys/linalg/EigenArpackSolver.hpp"
     #endif
     namespace maphys {
   #+end_src

** GenEO space

   #+begin_src c++
     // Solve generalized EVD : A U = B U Lam with A symmetric and B spd
     // And remove vectors for which eigenvalue lambda >= 1/omega (if omega > 0)
     // NB: solver is necessary if arpack is used
     template<MPH_Matrix Matrix, MPH_Vector Vector, typename Solver = Identity<Matrix, Vector>,
	      typename Scalar = typename Vector::scalar_type, typename Real = typename Vector::real_type>
     std::pair<IndexArray<Real>, DenseMatrix<Scalar>>
     genEO_space_gevd(const Matrix& A, const Matrix& B, const int n_v, //In
		      const Real omega = 0, const Real tol = -1){      //Optional

       IndexArray<Real> Lam;
       DenseMatrix<Scalar> U;
     #ifdef MAPHYSPP_USE_ARPACK
       static_assert(!std::is_same_v<Solver, Identity<Matrix, Vector>>,
		     "genEO_space_gevd: a solver must be passed to use arpack");
       try{

	 std::tie(Lam, U) = EigenArpackSolver<DenseMatrix<Scalar>, Matrix, Vector, Solver>::gen_eigh_smallest(A, B, n_v, tol);
       }
       catch(const ArpackNotConverged&){
	 MAPHYSPP_WARNING("Arpack error: using lapack dense kernel for gen. evd");
	 DenseMatrix<Scalar> dA, dB;
	 A.convert(dA);
	 B.convert(dB);
	 std::tie(Lam, U) = blas_kernels::gen_eigh_smallest(dA, dB, n_v);
       }
     #else
       (void) tol;
       static_assert(is_dense<Matrix>::value,
		     "genEO_space_gevd: ARPACK is necessary to compute evd with sparse matrix");
       std::tie(Lam, U) = blas_kernels::gen_eigh_smallest(A, B, n_v);
     #endif

       if(omega > 0){
	 // Remove Lam >= (1 / omega)
	 const auto inv_omega = Real{1.0} / omega;
	 Size newsize = 0;
	 for(Size j = 0; j < Lam.size(); ++j){
	   newsize = j;
	   if(Lam[j] >= inv_omega) break;
	 }
	 Lam.erase_after(newsize - 1);

	 if(n_cols(U) > newsize){
	   DenseMatrix<Scalar> new_U(n_rows(U), newsize);
	   for(Size j = 0; j < newsize; ++j){
	     new_U.get_vect_view(j) = U.get_vect_view(j);
	   }
	   U = new_U;
	 }
       }

       return std::make_pair(Lam, U);
     }

     template<MPH_Matrix Matrix, MPH_Vector Vector, typename M1_type, typename Solver = Identity<Matrix, Vector>,
	      typename Scalar = typename Vector::scalar_type, typename Real = typename Vector::real_type>
     PartMatrix<DenseMatrix<Scalar>> genEO_space(const PartMatrix<Matrix>& A, M1_type& M1,
						 const int n_v = 5,
						 [[maybe_unused]] const Real alpha = 0,
						 [[maybe_unused]] const Real beta = 0,
						 const Real tol = -1){
       Timer<TIMER_PRECOND> t("GenEO space generation");
       AdditiveSchwarz<PartMatrix<Matrix>, PartVector<Vector>, Identity<Matrix, Vector>> AS;
       NeumannNeumann<PartMatrix<Matrix>, PartVector<Vector>, Identity<Matrix, Vector>> NN;

       if constexpr(!M1_type::is_NN){ NN.setup(A);}
       if constexpr(!M1_type::is_AS){ AS.setup(A);}

       PartMatrix<DenseMatrix<Scalar>> V(A.get_proc());

       const IndexArray<int> local_subdomains = A.get_proc()->get_sd_ids();

       for(const auto sd_id : local_subdomains){
	 auto& lV = V.get_local_matrix(sd_id);

	 auto v1 = lV;
	 auto v2 = lV;
	 IndexArray<Real> w1, w2;

	 // If M1 is not NeumannNeumann, solve GEVD and crop with alpha
	 if constexpr(!M1_type::is_NN){
	   const auto& lA = NN.get_Aih().get_local_matrix(sd_id);
	   const auto& lB = M1.get_Aih().get_local_matrix(sd_id);
	   std::tie(w1, v1) = genEO_space_gevd<Matrix, Vector, Solver>(lA, lB, n_v, alpha, tol);
	 }

	 // If M1 is not AdditiveSchwarz, solve GEVD and crop with beta
	 if constexpr(!M1_type::is_AS){
	   const auto& lA = M1.get_Aih().get_local_matrix(sd_id);
	   const auto& lB = AS.get_Aih().get_local_matrix(sd_id);
	   std::tie(w2, v2) = genEO_space_gevd<Matrix, Vector, Solver>(lA, lB, n_v, beta, tol);
	 }

	 if constexpr(M1_type::is_AS){ lV = std::move(v1); }
	 else if constexpr(M1_type::is_NN){ lV = std::move(v2); }
	 else{
	   // If two sets of eigenvectors were computed, we need to get the
	   // n_v eigenvectors corresponding to the n_v smallest eigenvalues

	   IndexArray<Real> evals(w1);
	   evals.insert(IndexArray<Real>(w2));
	   IndexArray<int> order = evals.argsort();

	   const int w1size = static_cast<int>(w1.size());
	   const int w2size = static_cast<int>(w2.size());
	   const int final_nv = std::min(n_v, w1size + w2size);
	   lV = DenseMatrix<Scalar>(n_rows(A.get_local_matrix(sd_id)), final_nv);

	   for(int k = 0; k < final_nv; ++k){
	     if(order[k] >= w1size){
	       lV.get_vect_view(k) = v2.get_vect_view(order[k] - w1size);
	     }
	     else{
	       lV.get_vect_view(k) = v1.get_vect_view(order[k]);
	     }
	   }
	 }
       }

       return V;
     }
   #+end_src


** Purpose

   The one-level aS preconditioners can be combined, either additively or using deflation, to build a two-level aS preconditioner.
   The additive/deflated version can be deduced from the more general =TwoLevelAbstractSchwarz=,
   the correspondance being =AdditiveTwoLevelAbstractSchwarz<M0, M1>= $\equiv$ =TwoLevelAbstractSchwarz<M0, M1, false>=
   and =DeflatedTwoLevelAbstractSchwarz<M0, M1>= $\equiv$ =TwoLevelAbstractSchwarz<M0, M1, true>=.
   And init variant can also be called with the correspondance being =InitDeflatedTwoLevelAbstractSchwarz<M0, M1>= $\equiv$ =TwoLevelAbstractSchwarz<M0, M1, true, true>=.
   =TwoLevelAbstractSchwarz<M0, M1, false, true>= can be used but it equivalent to having a one-level aS preconditioner
   i.e. =TwoLevelAbstractSchwarz<M0, M1, false, true>= $\equiv$ =AbstractSchwarz<M1>=.

** Traits definitions and parameters

   #+begin_src c++
     template<typename T> struct is_pcd_geneo : public std::false_type {};
     template<typename T> struct is_pcd_coarse : public std::false_type {};
          
     namespace deflation_params {
     CREATE_STRUCT(A)
     CREATE_STRUCT(max_deflation_size)
     CREATE_STRUCT(alpha)
     CREATE_STRUCT(beta)
     CREATE_STRUCT(nev)
     CREATE_STRUCT(solve_deflation_size)
     CREATE_STRUCT(V0)
     }
   #+end_src
   
** Class definition

   #+begin_src c++
   template<MPH_Matrix PartMat,
	    MPH_Vector PartVect,
	    typename M0_type,
	    typename M1_type,
	    bool deflated = true,
	    bool init = false>
   class TwoLevelAbstractSchwarz : public LinearOperator<PartMat, PartVect>{
   #+end_src

** Attributes

   #+begin_src c++
     public:
     using scalar_type = typename LinearOperator<PartMat, PartVect>::scalar_type;
     using real_type = typename LinearOperator<PartMat, PartVect>::real_type;
     using matrix_type = PartMat;
     using vector_type = PartVect;
     
     private:
     using Scalar = scalar_type;
     using Real = real_type;
     using LocMat = typename PartMat::local_type;
     using LocVect = typename PartVect::local_type;
     
     M1_type _M1;
     M0_type _M0;
     int _max_deflation_size = 10;
     int _solve_deflation_size = 6;
     int _nev = 3;
     Real _alpha = 0;
     Real _beta = 0;
     Real _tol = -1;
     bool _is_setup = false;
     bool _init_guess_computed = false;
   #+end_src

** Constructor

   #+begin_src c++
   public:
   TwoLevelAbstractSchwarz(){}
   TwoLevelAbstractSchwarz(const PartMat& A, int n_vect = 10, Real alpha = 0, Real beta = 0, Real tol = -1) {
     set_n_vect(n_vect);
     set_alpha(alpha);
     set_beta(beta);
     set_tolerance(tol);
     setup(A);
   }
   #+end_src

** Setup
   #+begin_src c++
     private:
       void _setup(const deflation_params::max_deflation_size<int>&   v) { set_n_vect(v.value); }
       void _setup(const deflation_params::nev<int>&    v) { set_nev(v.value); }
       void _setup(const deflation_params::alpha<Real>& v) { set_alpha(v.value); }
       void _setup(const deflation_params::beta<Real>&  v) { set_beta(v.value); }
     
       void _setup(const PartMat& A, [[maybe_unused]] bool keep_deflation_space = false){
         _M1.setup(A);
         // Better to test if coarse, but works fow now
         if constexpr(is_pcd_coarse<M0_type>::value) {
           _M0.set_coarse_space(genEO_space<LocMat, LocVect, M1_type, typename M0_type::local_solver_type/*Identity<LocVect, LocVect>*/>
                                            (A, _M1, _max_deflation_size, _alpha, _beta));
           _M0.setup(A);
         }
         else { // Eigen Deflation
           _M0.setup(A, _max_deflation_size, _solve_deflation_size, _nev, keep_deflation_space);
         }
         _is_setup = true;
       }
     
     public:
       void setup(const PartMat& A) { _setup(A); }
       void setup(const PartMat& A, bool keep_deflation_space) {
         _setup(A, keep_deflation_space);
       }
       // Variadic template -> each parameter is a call to the _setup function
       template<typename... Types>
       void setup(const PartMat& A, const Types&... args){
         ( void(_setup(args)), ...);
         _setup(A);
       }
   #+end_src

** Apply

   #+begin_src c++
   PartVect apply(const PartVect& B){
     MAPHYSPP_ASSERT(_is_setup, "TwoLevelAbstractSchwarz: calling apply before setup");
     if constexpr(deflated){
       MAPHYSPP_ASSERT(_init_guess_computed, "TwoLevelAbstractSchwarz::apply: init guess must have been computed first");
       auto y = _M1.apply(B);
       return y - _M0.project(y);
     }
     else{
       return _M0.apply(B) + _M1.apply(B);
     }
   }
   #+end_src

** Compute initial guess

   #+begin_src c++
   PartVect init_guess(const PartVect& B){
     MAPHYSPP_ASSERT(_is_setup, "TwoLevelAbstractSchwarz: calling init_guess before setup");
     _init_guess_computed = true;
     if constexpr(deflated){
       // To simplify with C++20 require
       if constexpr (has_init_guess_function<M0_type>::value) return _M0.init_guess(B);
       else return _M0.apply(B);
     }
     else{
       return B * Scalar{0};
     }
   }
   #+end_src

** Setters

   #+begin_src c++
   void set_n_vect(const int n_vect){
     MAPHYSPP_ASSERT(n_vect > 0, "TwoLevelAbstractSchwarz::set_n_vect: n_vect must be > 0");
     _max_deflation_size = n_vect;
   }

   void set_n_vect_solve(const int n_vect){
     MAPHYSPP_ASSERT(n_vect > 0, "TwoLevelAbstractSchwarz::set_n_vect_solve: n_vect must be > 0");
     _solve_deflation_size = n_vect;
   }

   void set_nev(const int n_vect){
     MAPHYSPP_ASSERT(n_vect > 0, "TwoLevelAbstractSchwarz::set_nev: nev must be > 0");
     _nev = n_vect;
   }

   void set_alpha(const Real alpha){
     MAPHYSPP_ASSERT(alpha >= 0, "TwoLevelAbstractSchwarz::set_alpha: beta must be positive");
     _alpha = alpha;
   }

   void set_beta(const Real beta){
     MAPHYSPP_ASSERT(beta >= 0, "TwoLevelAbstractSchwarz::set_beta: beta must be positive");
     _beta = beta;
   }

   void set_tolerance(const Real tol){
     _tol = tol;
   }
   #+end_src

** Getters

   #+begin_src c++
     template<class = std::enable_if<has_spectral_extraction<M0_type>::value>>
     [[nodiscard]] SpectralExtraction<PartMat, PartVect>* get_spectral_extraction(){
       return _M0.get_spectral_extraction();
     }
     
     [[nodiscard]] M0_type& get_M0(){ return _M0; }
     [[nodiscard]] M1_type& get_M1(){ return _M1; }
     [[nodiscard]] const M0_type& get_M0() const { return _M0; }
     [[nodiscard]] const M1_type& get_M1() const { return _M1; }
   #+end_src

** Traits

   #+begin_src c++
   }; // class TwoLevelAbstractSchwarz
   #+end_src

   #+begin_src c++     
     template<typename LocMat, typename LocVect, typename GlobSolver>
     struct is_pcd_coarse<CoarseSolve<LocMat, LocVect, GlobSolver>> : public std::true_type {};
     
     template<MPH_Matrix PartMat, MPH_Vector PartVect, typename M1_type, typename LocMat, typename LocVect, typename GlobSolver, bool deflated, bool init>
     struct is_pcd_geneo<TwoLevelAbstractSchwarz<PartMat, PartVect, CoarseSolve<LocMat, LocVect, GlobSolver>, M1_type, deflated, init>> : public std::true_type {};
     
     template<MPH_Matrix PartMat, MPH_Vector PartVect, typename M1_type, typename LocMat, typename LocVect, typename GlobSolver, bool deflated, bool init>
     struct has_init_guess_function<TwoLevelAbstractSchwarz<PartMat, PartVect, CoarseSolve<LocMat, LocVect, GlobSolver>, M1_type, deflated, init>> : public std::true_type {};
   #+end_src

** Footer
   #+begin_src c++
     } // namespace maphys
   #+end_src

* Test GeneoPCD
:PROPERTIES:
:header-args: c++ :tangle ../../../src/test/unittest/test_geneo_pcd.cpp :comments link
:END:

#+begin_src c++
  #include <iostream>
  #include <iomanip>

  #include <maphys.hpp>
  #include <maphys/precond/TwoLevelAbstractSchwarz.hpp>
  #include <maphys/solver/BlasSolver.hpp>
  #ifdef MAPHYSPP_USE_PASTIX
  #include <maphys/solver/Pastix.hpp>
  #endif // MAPHYSPP_USE_PASTIX
  #include <maphys/solver/ConjugateGradient.hpp>
  #include <maphys/solver/PartSchurSolver.hpp>
  #include <maphys/solver/CentralizedSolver.hpp>
  #include <maphys/testing/TestMatrix.hpp>

  #include <catch2/catch_test_macros.hpp>

  using namespace maphys;

  using Scalar = double;
  using Real = double;
  static const Real test_alpha = 0;
  static const Real test_beta = 0;
  static const Real test_tol = arithmetic_tolerance<Real>::value * 1e11;
  static const int  test_max_vect = 3;

  template<typename PcdType, typename Mat, typename Vect, typename Real>
  void test_cg_geneo(const Mat& A, const Vect& b, int n_v){
    ConjugateGradient<Mat, Vect, PcdType> cg;
    int max_it = std::max(1, static_cast<int>(n_rows(A)));
    max_it = std::min(100, max_it);

    auto setup_geneo = [n_v](const Mat& mat, PcdType& pcd){
      if(MMPI::rank() == 0) std::cout << "NV = " << n_v << '\n';
      pcd.set_n_vect(n_v);
      pcd.set_alpha(test_alpha);
      pcd.set_beta(test_beta);
      pcd.setup(mat);
    };

    auto geneo_initguess = [](const Mat& mat, const Vect& rhs, PcdType& pcd){
      (void) mat;
      return pcd.init_guess(rhs);
    };

    cg.setup(parameters::A{A},
	     parameters::max_iter{max_it},
	     parameters::tolerance{test_tol},
	     parameters::verbose{(MMPI::rank() == 0)},
	     parameters::setup_pcd<std::function<void(const Mat&, PcdType&)>>{setup_geneo},
	     parameters::setup_init_guess<std::function<Vect(const Mat&, const Vect&, PcdType&)>>{geneo_initguess});

    auto x = cg * b;

    REQUIRE(cg.get_n_iter() != -1);
  }

  TEST_CASE("GenEO preconditioner (dense)", "[precond][geneo][dense]"){
    using LocSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
    using PMat = PartMatrix<DenseMatrix<Scalar>>;
    using PVect = PartVector<Vector<Scalar>>;
    using GlobSolver = CentralizedSolver<PMat, PVect, LocSolver>;
    using M0_type = CoarseSolve<DenseMatrix<Scalar>, Vector<Scalar>, GlobSolver>;
    //std::setprecision(std::numeric_limits<Scalar>::digits10 + 1);

    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    const PMat A = test_matrix::dist_spd_matrix<Scalar,DenseMatrix<Scalar>>(p).matrix;
    PVect x_exp = test_matrix::dist_vector<Scalar,Vector<Scalar>>(p).vector;
    for(int sd_id : p->get_sd_ids()){
      x_exp.get_local_vector(sd_id) = Vector<Scalar>{1,1,1,1};
    }
    const PVect b = A * x_exp;

    SECTION("GenEO space generation"){
      auto sds = p->get_sd_ids();
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	NeumannNeumann<PMat, PVect, LocSolver> NN(A);
	PMat U1 = genEO_space<DenseMatrix<Scalar>, Vector<Scalar>, decltype(NN), LocSolver>(A, NN, n_v);
	//U1.display("U1");
	REQUIRE(n_rows(U1) == n_rows(A));
	for(int sd_id : sds){
	  REQUIRE(n_cols(U1.get_local_matrix(sd_id)) == static_cast<Size>(n_v));
	}

	AdditiveSchwarz<PMat, PVect, LocSolver> AS(A);
	PMat U2 = genEO_space<DenseMatrix<Scalar>, Vector<Scalar>, decltype(AS), LocSolver>(A, AS, n_v);
	//U2.display("U2");
	REQUIRE(n_rows(U2) == n_rows(A));
	for(int sd_id : sds){
	  REQUIRE(n_cols(U2.get_local_matrix(sd_id)) == static_cast<Size>(n_v));
	}

	RobinRobin<PMat, PVect, LocSolver> RR(A);
	PMat U3 = genEO_space<DenseMatrix<Scalar>, Vector<Scalar>, decltype(RR), LocSolver>(A, RR, n_v);
	//U3.display("U3");
	REQUIRE(n_rows(U3) == n_rows(A));
	for(int sd_id : sds){
	  REQUIRE(n_cols(U3.get_local_matrix(sd_id)) == static_cast<Size>(n_v));
	}
      }
    }

    SECTION("GenEO additive pcd AS +"){
      using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, false>;
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
      }
    }

    SECTION("GenEO deflated pcd AS deflated"){
      using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
      }
    }


    SECTION("GenEO deflated pcd NN deflated"){
      using M1_type = NeumannNeumann<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
      }
    }


    SECTION("GenEO deflated pcd RR deflated"){
      using M1_type = RobinRobin<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
      }
    }
  }

  /* FIXME
     #ifdef MAPHYSPP_USE_PASTIX
     TEST_CASE("GenEO preconditioner on the Schur", "[precond][geneo][schur]"){
     using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
     using PMatDense = PartMatrix<DenseMatrix<Scalar>>;
     using PVect = PartVector<Vector<Scalar>>;
     using DenseSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
     using SpSolver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
     using GlobSolver = CentralizedSolver<PMatDense, PVect, DenseSolver>;

     using M0_type = CoarseSolve<DenseMatrix<Scalar>, Vector<Scalar>, GlobSolver>;
     using M1_type = AdditiveSchwarz<PMatDense, PVect, DenseSolver>;
     using Pcd = TwoLevelAbstractSchwarz<PMatDense, PVect, M0_type, M1_type, false>;

     using ItSolver = ConjugateGradient<PMatDense, PVect, Pcd>;
     using Solver = PartSchurSolver<PMat, PVect, SpSolver, ItSolver>;

     std::shared_ptr<Process> p = test_matrix::get_distr_process();
     PMat Atmp = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
     Atmp.apply_on_data([](SparseMatrixCOO<Scalar>& m){ m.to_triangular(); });
     const PMat A(std::move(Atmp));
     PVect x_exp = test_matrix::dist_vector<Scalar,Vector<Scalar>>(p).vector;
     for(int sd_id : p->get_sd_ids()){
     x_exp.get_local_vector(sd_id) = Vector<Scalar>{1,1,1,1};
     }
     const PVect b = A * x_exp;

     Solver solver;
     solver.setup(parameters::A{A});

     auto& cg = solver.get_solver_S();

     int max_it = std::max(1, static_cast<int>(n_rows(A)));
     max_it = std::min(100, max_it);
     const int n_v = 2;
     auto setup_geneo = [n_v](const PMatDense& mat, Pcd& pcd){
     if(MMPI::rank() == 0) std::cout << "NV = " << n_v << '\n';
     pcd.set_n_vect(n_v);
     pcd.set_alpha(test_alpha);
     pcd.set_beta(test_beta);
     pcd.setup(mat);
     };

     auto geneo_initguess = [](const PMatDense& mat, const PVect& rhs, Pcd& pcd){
     (void) mat;
     return pcd.init_guess(rhs);
     };

     cg.setup(parameters::max_iter{max_it},
     parameters::tolerance{test_tol},
     parameters::verbose{(MMPI::rank() == 0)},
     parameters::setup_pcd<std::function<void(const PMatDense&, Pcd&)>>{setup_geneo},
     parameters::setup_init_guess<std::function<PVect(const PMatDense&, const PVect&, Pcd&)>>{geneo_initguess});

     auto x = solver * b;

     REQUIRE(cg.get_n_iter() != -1);
     }
     #endif // MAPHYSPP_USE_PASTIX
  ,*/
  
  #ifdef MAPHYSPP_USE_ARPACK
  #ifdef MAPHYSPP_USE_PASTIX
  TEST_CASE("GenEO preconditioner (sparse)", "[precond][geneo][sparse]"){
    using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
    using PVect = PartVector<Vector<Scalar>>;
    using LocSolver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using GlobSolver = CentralizedSolver<PMat, PVect, LocSolver>;
    using M0_type = CoarseSolve<SparseMatrixCOO<Scalar>, Vector<Scalar>, GlobSolver>;

    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    PartMatrix<SparseMatrixCOO<Scalar>> Atmp = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
    Atmp.apply_on_data([](SparseMatrixCOO<Scalar>& m){ m.to_triangular(); });
    const PMat A(std::move(Atmp));
    PVect x_exp = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
    for(int sd_id : p->get_sd_ids()){
      x_exp.get_local_vector(sd_id) = Vector<Scalar>{1,1,1,1};
    }
    const PVect b = A * x_exp;

    SECTION("GenEO additive pcd AS +"){
      using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, false>;
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
      }
    }

    SECTION("GenEO deflated pcd AS deflated"){
      using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
      }
    }

    SECTION("GenEO deflated pcd NN deflated"){
      using M1_type = NeumannNeumann<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
      }
    }

    SECTION("GenEO deflated pcd RR deflated"){
      using M1_type = RobinRobin<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
      for(int n_v = 1; n_v <= test_max_vect; ++n_v){
	test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
      }
    }
  }


  TEST_CASE("GenEO preconditioner on the Schur", "[precond][geneo][schur]"){
    using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
    using PMatDense = PartMatrix<DenseMatrix<Scalar>>;
    using PVect = PartVector<Vector<Scalar>>;
    using DenseSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
    using SpSolver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using GlobSolver = CentralizedSolver<PMatDense, PVect, DenseSolver>;

    using M1_type = AdditiveSchwarz<PMatDense, PVect, DenseSolver>;
    using M0_type = CoarseSolve<typename PMatDense::local_type, typename PVect::local_type, GlobSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMatDense, PVect, M0_type, M1_type, true>;

    using ItSolver = ConjugateGradient<PMatDense, PVect, Pcd>;
    using Solver = PartSchurSolver<PMat, PVect, SpSolver, ItSolver>;

    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    PMat Atmp = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
    Atmp.apply_on_data([](SparseMatrixCOO<Scalar>& m){ m.to_triangular(); });
    const PMat A(std::move(Atmp));
    PVect x_exp = test_matrix::dist_vector<Scalar,Vector<Scalar>>(p).vector;
    for(int sd_id : p->get_sd_ids()){
      x_exp.get_local_vector(sd_id) = Vector<Scalar>{1,1,1,1};
    }
    const PVect b = A * x_exp;

    Solver solver;
    solver.setup(parameters::A{A});

    auto& cg = solver.get_solver_S();

    int max_it = std::max(1, static_cast<int>(n_rows(A)));
    max_it = std::min(100, max_it);
    const int n_v = 2;
    auto setup_geneo = [](const PMatDense& mat, Pcd& pcd){
      if(MMPI::rank() == 0) std::cout << "NV = " << n_v << '\n';
      pcd.set_n_vect(n_v);
      pcd.set_alpha(test_alpha);
      pcd.set_beta(test_beta);
      pcd.set_tolerance(test_tol);
      pcd.setup(mat);
    };

    auto geneo_initguess = [](const PMatDense& mat, const PVect& rhs, Pcd& pcd){
      (void) mat;
      return pcd.init_guess(rhs);
    };

    cg.setup(parameters::max_iter{max_it},
	     parameters::tolerance{test_tol},
	     parameters::verbose{(MMPI::rank() == 0)},
	     parameters::setup_pcd<std::function<void(const PMatDense&, Pcd&)>>{setup_geneo},
	     parameters::setup_init_guess<std::function<PVect(const PMatDense&, const PVect&, Pcd&)>>{geneo_initguess});

    auto x = solver * b;

    REQUIRE(cg.get_n_iter() != -1);
  }

  TEST_CASE("GenEO preconditioner 8 subdomains (sparse)", "[precond][geneo][sparse]"){
    using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
    using PVect = PartVector<Vector<Scalar>>;
    using LocSolver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using GlobSolver = CentralizedSolver<PMat, PVect, LocSolver>;
    using M0_type = CoarseSolve<SparseMatrixCOO<Scalar>, Vector<Scalar>, GlobSolver>;

    const std::string path_to_part = "@MAPHYSPP_MATRIX_PARTITIONS_8@";
    const int n_subdomains = 8;
    //Timer<0>::set_debug(true);
    // Subdomain topology
    std::shared_ptr<Process> p = bind_subdomains(n_subdomains);

    // Load matrix and RHS
    PMat Atmp(p);
    PVect x_exp(p);
    load_subdomains_and_data(path_to_part, n_subdomains, p, Atmp, x_exp);
    const PMat A = std::move(Atmp);
    x_exp.apply_on_data([](Vector<Scalar>& v){
      for(Size k = 0; k < n_rows(v); ++k){
	v[k] = Scalar{1};
      }
    });
    const PartVector<Vector<Scalar>> b = A * x_exp;
    const int n_v_big = 5;

    SECTION("GenEO additive pcd AS +"){
      using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, false>;
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v_big);
    }

    // Too long for unit tests
    /*
      SECTION("GenEO deflated pcd NN deflated"){
      using M1_type = NeumannNeumann<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v_big);
      }

      SECTION("GenEO deflated pcd AS deflated"){
      using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M1_type, GlobSolver, LocSolver, true>;
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v_big);
      }

      SECTION("GenEO deflated pcd RR deflated"){
      using M1_type = RobinRobin<PMat, PVect, LocSolver>;
      using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M1_type, GlobSolver, LocSolver, true>;
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v_big);
      }*/
  }
  #endif // MAPHYSPP_USE_PASTIX
  #endif // MAPHYSPP_USE_ARPACK
#+end_src

