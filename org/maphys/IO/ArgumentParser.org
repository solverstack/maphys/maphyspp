* Argument parser
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/IO/ArgumentParser.hpp :comments link
:END:

#+begin_src c++
  #pragma once

  #include <iostream>
  #include <map>
  #include <string>

  using std::string;
  namespace maphys {

  std::map<string, string> parse_arguments(int argc, char ** argv){

    std::map<string, string> arguments;

    if(argc <= 1) return arguments;

    // Ex: --parameter
    auto dashdash =  [](const string& s){
                       return s.size() >= 3 && s[0] == '-' && s[1] == '-';
                     };

    // Ex: -P
    auto onedash =  [](const string& s){
                      return s.size() == 2 && s[0] == '-' && s[1] != '-';
                    };

    string key;
    for(int i = 1; i < argc; ++i){
      string word(argv[i]);

      if(dashdash(word)){
        key = word.substr(2, word.size());
        arguments[key] = std::string("");
      }
      else if(onedash(word)){
        key = word.substr(1, 2);
        arguments[key] = std::string("");
      }
      else if(!key.empty()){
        arguments[key] =  word;
        key.clear();
      }
    }

    return arguments;
  }

  struct Argument{
    string key;
    string description;
    char char_key = '-';

    Argument(): key{string()}, description{string()}, char_key{'-'} {}
    Argument(string _key, string _description, char _char_key):
      key{_key}, description{_description}, char_key{_char_key} {}
  }; // struct Argument

  struct ArgumentList{
    std::map<string, int> int_values;
    std::map<string, string> str_values;
    std::map<string, double> real_values;
    std::map<string, bool> bool_values;
    std::map<string, Argument> argument_dict;

    void add_argument_int(const char * key, const char * description, int default_value, char char_key = '-'){
      const string skey(key);
      int_values[skey] = default_value;
      argument_dict[skey] = Argument(key, std::string(description), char_key);
    }

    void add_argument_str(const char * key, const char * description, string default_value, char char_key = '-'){
      const string skey(key);
      str_values[skey] = default_value;
      argument_dict[skey] = Argument(key, std::string(description), char_key);
    }

    void add_argument_real(const char * key, const char * description, double default_value, char char_key = '-'){
      const string skey(key);
      real_values[skey] = default_value;
      argument_dict[skey] = Argument(key, std::string(description), char_key);
    }

    void add_argument_bool(const char * key, const char * description, char char_key = '-'){
      const string skey(key);
      bool_values[skey] = false;
      argument_dict[skey] = Argument(key, std::string(description), char_key);
    }

    string usage() const {
      auto descr = [](const Argument& arg, const string& def_val){
                     string out_line("");
                     if(arg.char_key != '-'){
                       out_line += string("-") + string(1, arg.char_key) + string(", ");
                     }
                     out_line += string("--") + arg.key + string(" : ");
                     out_line += arg.description;
                     if(!def_val.empty()){
                       out_line += string(" (default ") + def_val + string(")");
                     }
                     out_line += string("\n");
                     return out_line;
                   };

      string out_str;
      for(const auto& [key, value] : str_values){
        const Argument& arg = argument_dict.at(key);
        out_str += string("(str) ");
        out_str += descr(arg, value);
      }
      for(const auto& [key, value] : int_values){
        const Argument& arg = argument_dict.at(key);
        out_str += string("(int) ");
        out_str += descr(arg, std::to_string(value));
      }
      for(const auto& [key, value] : real_values){
        const Argument& arg = argument_dict.at(key);
        out_str += string("(real) ");
        out_str += descr(arg, std::to_string(value));
      }
      for(const auto& keyval : bool_values){
        const auto& key = keyval.first;
        const Argument& arg = argument_dict.at(key);
        out_str += "(bool) ";
        out_str += descr(arg, string());
      }
      return out_str;
    }

    template<typename T>
    void _generic_fill_value(const std::map<string, string>& parsed_args, const char * ckey, T& v,
                             const std::map<string, T>& t_values, std::function<T(const string&)> stoT) const {
      const string key = std::string(ckey);
      if(argument_dict.count(key) == 0) return; // Unknown key
      if(t_values.count(key) == 0) return; // Unknown key
      const auto& arg = argument_dict.at(key);

      if(parsed_args.count(arg.key)){
        v = stoT(parsed_args.at(arg.key));
        return;
      }
      if(arg.char_key != '-' && (parsed_args.count(std::string(1, arg.char_key)))){
        v = stoT(parsed_args.at(std::string(1, arg.char_key)));
        return;
      }
      v = t_values.at(key);
    }

    void fill_value(const std::map<string, string>& parsed_args, const char * ckey, int& v) const {
      try{
        _generic_fill_value<int>(parsed_args, ckey, v, int_values, [](const string& s){ return std::stoi(s); });
      } catch(std::invalid_argument&){
        const auto key = string(ckey);
        const auto& value = parsed_args.at(key);
        MAPHYSPP_WARNING("Parameter - failed to convert to integer, key / value: " + key + string(" / ") + value);
      }
    }

    void fill_value(const std::map<string, string>& parsed_args, const char * ckey, string& v) const {
      _generic_fill_value<string>(parsed_args, ckey, v, str_values, [](const string& s){ return s; });
    }

    void fill_value(const std::map<string, string>& parsed_args, const char * ckey, double& v) const {
      try{
        _generic_fill_value<double>(parsed_args, ckey, v, real_values, [](const string& s){ return std::stod(s); });
      } catch(std::invalid_argument&){
        const auto key = string(ckey);
        const auto& value = parsed_args.at(key);
        MAPHYSPP_WARNING("Parameter - failed to convert to real (double), key / value: " + key + string(" / ") + value);
      }
    }

    // For booleans, the presence of the key means true, unless the value given is "0" or "false"
    void fill_value(const std::map<string, string>& parsed_args, const char * ckey, bool& v) const {
      v = false;  
      auto str_to_bool = [](const string& s){
                           if(s == string("false"))  { return false; }
                           else if(s == string("0")) { return false; }
                           return true;
                         };
      _generic_fill_value<bool>(parsed_args, ckey, v, bool_values, str_to_bool);
    }

    void clear(){
      int_values.clear();
      str_values.clear();
      real_values.clear();
      bool_values.clear();
      argument_dict.clear();  
    }
  }; //struct ArgumentList

  } // namespace maphys
#+end_src
