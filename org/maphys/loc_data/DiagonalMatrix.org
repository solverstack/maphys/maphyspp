label:data:diagonalmatrix

#+PROPERTY: header-args: c++ :results silent

* Diagonal matrix
:PROPERTIES:
:header-args: c++ :tangle ../../../include/maphys/loc_data/DiagonalMatrix.hpp :comments link
:END:

 This is an implementation of a diagonal square matrix.

** Header

#+begin_src c++
#pragma once

namespace maphys {
template<class> class DiagonalMatrix;
}

#include "maphys/loc_data/DenseMatrix.hpp"
#include "maphys/loc_data/SparseMatrixCSC.hpp"
#include "maphys/utils/Arithmetic.hpp"
#include "maphys/utils/MatrixProperties.hpp"
#include "maphys/utils/IndexArray.hpp"
#include "maphys/utils/Error.hpp"
#include "maphys/loc_data/ETMatVec.hpp"

namespace maphys {
#+end_src

** Attributes

#+begin_src c++
  template<class Scalar>
  class DiagonalMatrix : public MatrixProperties<Scalar>
  {

  public:
    using scalar_type = Scalar;
    using real_type = typename arithmetic_real<Scalar>::type;

  private:
    using Real = real_type;
    using DataArray = IndexArray<Scalar>;

    Size _m{0};
    Size _n{0};
    DataArray _data;
#+end_src

** Constructors

#+begin_src c++
  public:
  explicit DiagonalMatrix(const Size m, const Size n): _m{m}, _n{n}{
    _data = DataArray(std::min(_m, _n), Scalar{0});
  }
  
  explicit DiagonalMatrix(const Size m): DiagonalMatrix(m, m) {}
  
  explicit DiagonalMatrix(): DiagonalMatrix(0, 0) {}
  
  explicit DiagonalMatrix(const Size m, const Size n, Scalar * vect): _m{m}, _n{n} {
    _data = DataArray(vect, std::min(_m, _n));
  }
  
  //explicit DiagonalMatrix(const std::vector<Scalar>& v): DiagonalMatrix(v.size(), v.size()), _data(v) {}
  
  explicit DiagonalMatrix(std::initializer_list<Scalar> l, const Size m = 0, const Size n = 0){
    Size diagsize = l.size();
    if(m == 0){
      _m = diagsize;
      _n = diagsize;
    }
    else if(n == 0){
      _m = m;
      _n = m;
    }
    else{
      _m = m;
      _n = n;
    }
    MAPHYSPP_ASSERT(diagsize == std::min(_m, _n), "DiagonalMatrix list initialization: size must be min(m, n)");
    _data = DataArray(l);
  }
  
  explicit DiagonalMatrix(const Vector<Scalar>& vect): _m{vect.get_n_rows()}, _n{vect.get_n_rows()}, _data{DataArray(vect.get_ptr(), vect.get_n_rows())} {}
  
  explicit DiagonalMatrix(Vector<Scalar>&& vect): _m{vect.get_n_rows()}, _n{vect.get_n_rows()}, _data{vect.move_array().move_data_array()} {}

  DiagonalMatrix(const DataArray& v, const Size m, const Size n = 0): DiagonalMatrix(m, n) { _data = v; }
  DiagonalMatrix(DataArray&& v, const Size m, const Size n = 0): DiagonalMatrix(m, n) { _data = std::move(v); }
  
  DiagonalMatrix(const DiagonalMatrix& mat) = default;
  DiagonalMatrix(DiagonalMatrix&& mat):
    _m{std::exchange(mat._m, 0)},
    _n{std::exchange(mat._n, 0)},
    _data{std::move(mat._data)}
  {
    this->copy_properties(mat);
    mat.set_default_properties();
  }
  
  // Copy and move assignment operator
  DiagonalMatrix& operator= (DiagonalMatrix copy){
    copy.swap(*this);
    return *this;
  }
#+end_src
** Swap function

#+begin_src c++ :results silent :tangle ../../../include/maphys/loc_data/DiagonalMatrix.hpp :comments link
  void swap(DiagonalMatrix& other){
    std::swap(_m, other._m);
    std::swap(_n, other._n);
    std::swap(_data, other._data);
  }

  friend void swap(DiagonalMatrix& m1, DiagonalMatrix& m2){
    m1.swap(m2);
  }
#+end_src
** Comparison operator

#+begin_src c++
  bool operator==(const DiagonalMatrix& mat2) const {
    if(_m != mat2._m) return false;
    if(_n != mat2._n) return false;
    if(_data != mat2._data) return false;
    return true;
  }

  bool operator!=(const DiagonalMatrix& mat2) const {
    return !(*this == mat2);
  }
#+end_src

** Casting

#+begin_src c++
template <class OtherScalar>
[[nodiscard]] DiagonalMatrix<OtherScalar> cast() const {
  DiagonalMatrix<OtherScalar> mat(_m, _n);
  copy_properties<Scalar, OtherScalar>(*this, mat);
  OtherScalar * ptr = mat.get_ptr();
  for(Size i = 0; i < std::min(_m, _n); ++i){
    ptr[i] = static_cast<OtherScalar>(_data[i]);
  }
  return mat;
}
#+end_src

** Identity

#+begin_src c++
  void identity(){
    for(Size k = 0; k < std::min(_m, _n); ++k){
      _data[k] = Scalar{1};
    }
  }
#+end_src

** Transposition

#+begin_src c++
void transpose(){ std::swap(_m, _n); }
void conj_transpose(){
  std::swap(_m, _n);
  if constexpr(is_complex<Scalar>::value){
    for(Size k = 0; k < _m; ++k)  _data[k] = conj(_data[k]);
  }
}

[[nodiscard]] DiagonalMatrix t() const {
  return DiagonalMatrix(_data, _n, _m);
}
#+end_src

** Scalar multiplication

#+begin_src c++
  // Scalar multiplication
  DiagonalMatrix& operator*= (const Scalar& scal){
    for(Size i = 0; i < std::min(_m, _n); ++i) (*this)[i] *= scal;
    return *this;
  }
#+end_src

** Diagonal matrix product

#+begin_src c++
// Diag *= Diag
void matprod(const DiagonalMatrix& other){
  MAPHYSPP_ASSERT(other.get_n_cols() == this->get_n_rows(), "DiagonalMatrix * DiagonalMatrix: wrong dimensions");
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] *= other._data[k];
  }
}

// Square the matrix
void square(){
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] *= _data[k];
  }
}

DiagonalMatrix& operator*=(const DiagonalMatrix& other){
  if(this == &other){
    this->square();
  }
  else{
    this->matprod(other);
  }
  return *this;
}
#+end_src

** Diagonal matrix addition

#+begin_src c++
// Diag += Diag
void add(const DiagonalMatrix& other){
  MAPHYSPP_ASSERT(other.get_n_rows() == this->get_n_rows(), "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
  MAPHYSPP_ASSERT(other.get_n_cols() == this->get_n_cols(), "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] += other._data[k];
  }
}

void substract(const DiagonalMatrix& other){
  MAPHYSPP_ASSERT(other.get_n_rows() == this->get_n_rows(), "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
  MAPHYSPP_ASSERT(other.get_n_cols() == this->get_n_cols(), "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
  for(Size k = 0; k < std::min(_m, _n); ++k){
    _data[k] += other._data[k];
  }
}

DiagonalMatrix& operator+=(const DiagonalMatrix& other){
  if(this == &other){
    (*this) *= Scalar{2};
  }
  else{
    this->add(other);
  }
  return *this;
}

DiagonalMatrix& operator-=(const DiagonalMatrix& other){
  if(this == &other){
    _data = DataArray(std::min(_m, _n), Scalar{0});
  }
  else{
    this->substract(other);
  }
  return *this;
}
#+end_src

** Getters

#+begin_src c++
public:
  // Getters
  [[nodiscard]] inline Size get_n_rows() const { return _m; }
  [[nodiscard]] inline Size get_n_cols() const { return _n; }
  [[nodiscard]] inline Size constexpr get_leading_dim() const { return 1; }

  [[nodiscard]] inline Scalar* get_ptr() { return &_data[0]; }
  [[nodiscard]] inline const Scalar* get_ptr() const { return &_data[0]; }
  template<MPH_Integral Tint>
  [[nodiscard]] inline Scalar* get_ptr(Tint i, Tint j) {
    MAPHYSPP_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return &_data[i];
  }
  template<MPH_Integral Tint>
  [[nodiscard]] inline const Scalar* get_ptr(Tint i, Tint j) const {
    MAPHYSPP_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return &_data[i];
  }
  template<MPH_Integral Tint>
  [[nodiscard]] inline Scalar* get_ptr(Tint i) { return &_data[i]; }
  template<MPH_Integral Tint>
  [[nodiscard]] inline const Scalar* get_ptr(Tint i) const { return &_data[i]; }
  [[nodiscard]] inline DataArray get_array() const { return _data; }
#+end_src

** Indexing

#+begin_src c++
  // Element accessor
  template<MPH_Integral Tint>
  [[nodiscard]] inline Scalar& operator()(const Tint i, const Tint j){
    MAPHYSPP_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return _data[i];
  }
  template<MPH_Integral Tint>
  [[nodiscard]] inline const Scalar& operator()(const Tint i, const Tint j) const {
    MAPHYSPP_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return _data[i];
  }
  template<MPH_Integral Tint>
  [[nodiscard]] inline Scalar& operator()(const Tint i){ return _data[i]; }
  template<MPH_Integral Tint>
  [[nodiscard]] inline const Scalar& operator()(const Tint i) const { return _data[i]; }
#+end_src

** Memory cost

#+begin_src c++
  [[nodiscard]] inline Size memcost() const { return std::min(_m, _n) * sizeof(Scalar); }
#+end_src

** Display function

#+begin_src c++
  // Pretty printing
  void display(const std::string& name="", std::ostream &out = std::cout) const {
    if(!name.empty()) out << name << '\n';
    out << "m: " << _m << " "
        << "n: " << _n << " " << '\n'
        << "Diagonal matrix\n"
        << this->properties_str()
        << '\n' << '\n';

    for(Size i = 0; i < _m; ++i){
      for(Size j = 0; j < _n; ++j){
        if(i == j){
          out << _data[i] << "\t";
        }
        else{
          out << "-" << "\t";
        }
      }
      out << '\n';
    }
    out << '\n';
  }
#+end_src

** Frobenius norm

#+begin_src c++
[[nodiscard]] Real frobenius_norm() const{
  Real norm{0.0};
  for(Size k = 0; k < std::min(_m, _n); ++k){
    norm += std::norm(_data[k]);
  }
  return std::sqrt(norm);
}

Real norm() const { return frobenius_norm(); }
#+end_src

** Out of class operators

#+begin_src c++
}; // class DiagonalMatrix
#+end_src

#+begin_src c++
// Scalar multiplication
template<class Scalar>
inline DiagonalMatrix<Scalar> operator* (DiagonalMatrix<Scalar> diag, const Scalar scal){
  diag *= scal;
  return diag;
}

template<class Scalar>
inline DiagonalMatrix<Scalar> operator* (const Scalar scal, DiagonalMatrix<Scalar> diag){
  diag *= scal;
  return diag;
}

template<class Scalar>
inline DiagonalMatrix<Scalar> operator/ (DiagonalMatrix<Scalar> diag, const Scalar scal){
  diag *= (Scalar{1}/scal);
  return diag;
}

// Diagonal addition / substraction
template<class Scalar>
inline DiagonalMatrix<Scalar> operator+ (DiagonalMatrix<Scalar> diag, const DiagonalMatrix<Scalar> other){
  diag += other;
  return diag;
}

template<class Scalar>
inline DiagonalMatrix<Scalar> operator- (DiagonalMatrix<Scalar> diag, const DiagonalMatrix<Scalar> other){
  diag *= Scalar{-1};
  diag += other;
  return diag;
}

// Vector <- Diagonal * Vector
template<class Scalar>
inline Vector<Scalar> operator* (const DiagonalMatrix<Scalar>& diag, Vector<Scalar> vect){
  vect.diagmv(diag);
  return vect;
}

// DenseMatrix <- Diagonal * DenseMatrix
template<class Scalar>
inline DenseMatrix<Scalar> operator*(const DiagonalMatrix<Scalar>& diag, DenseMatrix<Scalar> mat){
  mat.left_diagmm(diag);
  return mat;
}

// DenseMatrix <- DenseMatrix * Diagonal
template<class Scalar>
inline DenseMatrix<Scalar> operator*(DenseMatrix<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.right_diagmm(diag);
  return mat;
}

//DenseMatrix <- Diagonal + DenseMatrix
template<class Scalar>
inline DenseMatrix<Scalar> operator+(DenseMatrix<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.add(diag);
  return mat;
}

//DenseMatrix <- DenseMatrix + Diagonal
template<class Scalar>
inline DenseMatrix<Scalar> operator+(const DiagonalMatrix<Scalar>& diag, DenseMatrix<Scalar> mat){
  mat.add(diag);
  return mat;
}

//DenseMatrix <- DenseMatrix - Diagonal
template<class Scalar>
inline DenseMatrix<Scalar> operator-(DenseMatrix<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.add(diag, Scalar{-1.0});
  return mat;
}

//DenseMatrix <- Diagonal - DenseMatrix
template<class Scalar>
inline DenseMatrix<Scalar> operator-(const DiagonalMatrix<Scalar>& diag, DenseMatrix<Scalar> mat){
  mat *= Scalar{-1};
  mat.add(diag);
  return mat;
}

// COO <- COO * Diagonal
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator*(SparseMatrixCOO<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.right_diagmm(diag);
  return mat;
}

// COO <- Diagonal * COO
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator*(const DiagonalMatrix<Scalar>& diag, SparseMatrixCOO<Scalar> mat){
  mat.left_diagmm(diag);
  return mat;
}

//SparseMatrixCOO <- Diagonal + SparseMatrixCOO
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator+(SparseMatrixCOO<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat += diag;
  return mat;
}

//SparseMatrixCOO <- SparseMatrixCOO + Diagonal
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator+(const DiagonalMatrix<Scalar>& diag, SparseMatrixCOO<Scalar> mat){
  mat += diag;
  return mat;
}

//SparseMatrixCOO <- SparseMatrixCOO - Diagonal
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator-(SparseMatrixCOO<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat -= diag;
  return mat;
}

//SparseMatrixCOO <- Diagonal - SparseMatrixCOO
template<class Scalar>
inline SparseMatrixCOO<Scalar> operator-(const DiagonalMatrix<Scalar>& diag, SparseMatrixCOO<Scalar> mat){
  mat *= Scalar{-1};
  mat += diag;
  return mat;
}

/*---------------*/
/*----- CSC -----*/
/*---------------*/

// CSC <- CSC * Diagonal
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator*(SparseMatrixCSC<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat.right_diagmm(diag);
  return mat;
}

// CSC <- Diagonal * CSC
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator*(const DiagonalMatrix<Scalar>& diag, SparseMatrixCSC<Scalar> mat){
  mat.left_diagmm(diag);
  return mat;
}

//SparseMatrixCSC <- Diagonal + SparseMatrixCSC
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator+(SparseMatrixCSC<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat += diag;
  return mat;
}

//SparseMatrixCSC <- SparseMatrixCSC + Diagonal
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator+(const DiagonalMatrix<Scalar>& diag, SparseMatrixCSC<Scalar> mat){
  mat += diag;
  return mat;
}

//SparseMatrixCSC <- SparseMatrixCSC - Diagonal
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator-(SparseMatrixCSC<Scalar> mat, const DiagonalMatrix<Scalar>& diag){
  mat -= diag;
  return mat;
}

//SparseMatrixCSC <- Diagonal - SparseMatrixCSC
template<class Scalar>
inline SparseMatrixCSC<Scalar> operator-(const DiagonalMatrix<Scalar>& diag, SparseMatrixCSC<Scalar> mat){
  mat *= Scalar{-1};
  mat += diag;
  return mat;
}
#+end_src

** Traits

#+begin_src c++
template<typename Scalar> struct vector_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = Vector<Scalar>;
};

template<typename Scalar> struct sparse_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = SparseMatrixCSC<Scalar>;
};

template<typename Scalar> struct scalar_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = Scalar;
};

template<typename Scalar> struct is_dense<DiagonalMatrix<Scalar>> : public std::true_type {};
#+end_src

** Container functions

#+begin_src c++
// Container functions
template<class Scalar>
Scalar * begin(DiagonalMatrix<Scalar>& v){ return v.get_ptr(); }
template<class Scalar>
const Scalar * begin(const DiagonalMatrix<Scalar>& v){ return v.get_ptr(); }

template<class Scalar>
Scalar * end(DiagonalMatrix<Scalar>& v){ return v.get_ptr() + v.get_n_rows(); }
template<class Scalar>
const Scalar * end(const DiagonalMatrix<Scalar>& v){ return v.get_ptr() + v.get_n_rows(); }
#+end_src

** Interface functions

#+begin_src c++
template<typename Scalar>
[[nodiscard]] inline Size n_rows(const DiagonalMatrix<Scalar>& mat) { return mat.get_n_rows(); }

template<typename Scalar>
[[nodiscard]] inline Size n_cols(const DiagonalMatrix<Scalar>& mat) { return mat.get_n_cols(); }

template<typename Scalar>
[[nodiscard]] inline DiagonalMatrix<Scalar> diagonal(const DiagonalMatrix<Scalar>& mat) { return mat; }

template<typename Scalar>
[[nodiscard]] inline Scalar * get_ptr(DiagonalMatrix<Scalar>& mat) { return mat.get_ptr(); }

template<typename Scalar>
[[nodiscard]] inline const Scalar * get_ptr(const DiagonalMatrix<Scalar>& mat) { return mat.get_ptr(); }

template<class Scalar>
[[nodiscard]] DiagonalMatrix<Scalar> adjoint(const DiagonalMatrix<Scalar>& mat){ return mat.t(); }

template<class Scalar>
void display(const DiagonalMatrix<Scalar>& v, const std::string& name="", std::ostream &out = std::cout){ v.display(name, out); }

template<typename Scalar>
void build_matrix(DiagonalMatrix<Scalar>& mat, const int M, const int N, const int nnz, int * i, int * j, Scalar * v,  const bool){
  mat = DiagonalMatrix<Scalar>(M, N);
  MAPHYSPP_ASSERT(M == nnz, "build_matrix(Diagonal matrix) with nnz != M");
  MAPHYSPP_ASSERT(M == nnz, "build_matrix(Diagonal matrix) with nnz != N");
  for(int k = 0; k < nnz; ++k){
    MAPHYSPP_ASSERT(i[k] == j[k], "build_matrix(Diagonal matrix) with extradiagonal elements");
    mat(i[k]) = v[k];
  }
}
#+end_src

** Footer

#+begin_src c++
} // namespace maphys
#+end_src

* Tests
:PROPERTIES:
:header-args: c++ :tangle ../../../src/test/unittest/test_diagonal_mat.cpp :comments link
:END:

#+begin_src c++
  #include <iostream>
  #include <vector>
  #include <complex>

  #include <maphys.hpp>

  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>

  using namespace maphys;

  TEMPLATE_TEST_CASE("Diagonal matrix", "[diagonalmatrix]", float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;

    //using Vect = maphys::Vector<Scalar>;
    using DiagMat = maphys::DiagonalMatrix<Scalar>;
    //using Real = typename maphys::arithmetic_real<Scalar>::type;
    using std::size_t;

    SECTION("Test constructors"){
      DiagMat M1;
      REQUIRE( n_rows(M1) == 0 );
      REQUIRE( n_cols(M1) == 0 );
      REQUIRE( M1.get_array().size() == 0 );

      DiagMat M2(3, 4);
      REQUIRE( n_rows(M2) == 3 );
      REQUIRE( n_cols(M2) == 4 );
      REQUIRE( M2.get_array().size() == 3 );
      bool is_fullofzeros = true;
      for (auto k = 0; k < 3; ++k){
	is_fullofzeros &= (M2.get_ptr()[k] == Scalar{0.0});
      }
      REQUIRE( is_fullofzeros );

      std::vector<Scalar> M3_data{ 1.0, 2.0, 3.0 };
      Scalar * M3_data_ptr = M3_data.data();
      DiagMat M3(3, 3, M3_data_ptr);

      DiagMat M4{ 1.0, 2.0, 3.0 };
      REQUIRE( M3 == M4 );
    }
  }

  template<typename Scalar>
  using DenseMat = DenseMatrix<Scalar, -1>;
  TEMPLATE_PRODUCT_TEST_CASE("Diagonal matrix operations", "[diagonalmatrix]", (DenseMat, SparseMatrixCOO, SparseMatrixCSC), (float, double, std::complex<float>, std::complex<double>)){
    using Matrix = TestType;
    using Scalar = typename Matrix::scalar_type;
    //using Vect = maphys::Vector<Scalar>;
    using DiagMat = maphys::DiagonalMatrix<Scalar>;
    //using Real = typename maphys::arithmetic_real<Scalar>::type;

    // ( 1  0 -2 )       ( 2      )
    // ( 2 -4  0 )       (   4    )
    // ( 0  0 -1 ) + / - (     -1 )
    // ( 3  3  0 )       (        )
    //      A      + / -     D
    const int M0 = 4;
    const int N0 = 3;
    const int NNZ = 7;
    std::vector<int> iA{0, 0, 1, 1, 2, 3, 3};
    std::vector<int> jA{0, 2, 0, 1, 2, 0, 1};
    std::vector<Scalar> vA{1, -2, 2, -4, -1, 3, 3};

    const DiagMat D({2, 4, -1}, M0, N0);

    Matrix A;
    build_matrix(A, M0, N0, NNZ, iA.data(), jA.data(), vA.data());

    SECTION("Addition"){
      // A + D
      Matrix A_add;
      std::vector<int> iA_add{0, 0, 1, 2, 3, 3};
      std::vector<int> jA_add{0, 2, 0, 2, 0, 1};
      std::vector<Scalar> vA_add{3, -2, 2, -2, 3, 3};
      build_matrix(A_add, M0, N0, NNZ-1, iA_add.data(), jA_add.data(), vA_add.data());

      Matrix res = A + D;
      REQUIRE(res == A_add);
      Matrix res2 = D + A;
      REQUIRE(res2 == A_add);
      Matrix Acpy = A;
      Acpy += D;
      REQUIRE(Acpy == A_add);
    }

    SECTION("Substraction"){
      // D - A
      Matrix A_leftsub;
      std::vector<int> iA_sub{0, 0, 1, 1, 3, 3};
      std::vector<int> jA_sub{0, 2, 0, 1, 0, 1};
      std::vector<Scalar> vA_leftsub{1, 2, -2, 8, -3, -3};
      build_matrix(A_leftsub, M0, N0, NNZ-1, iA_sub.data(), jA_sub.data(), vA_leftsub.data());

      // A - D
      Matrix A_rightsub;
      std::vector<Scalar> vA_rightsub{-1, -2, 2, -8, 3, 3};
      build_matrix(A_rightsub, M0, N0, NNZ-1, iA_sub.data(), jA_sub.data(), vA_rightsub.data());

      Matrix res = A - D;
      REQUIRE(res == A_rightsub);
      Matrix res2 = D - A;
      REQUIRE(res2 == A_leftsub);
      Matrix Acpy = A;
      Acpy -= D;
      REQUIRE(Acpy == A_rightsub);
    }

    // ( 1  0 -2 )   ( 2      0 )
    // ( 2 -4  0 )   (   4    0 )
    // ( 0  0 -1 ) * (     -1 0 )
    // ( 3  3  0 )
    //      A      *     F
    // 
    // B = A^T 
    // G = F^T

    const DiagMat F({2, 4, -1}, 3, 4);
    const DiagMat G(F.t());
    const Matrix B(A.t());

    // We want to test left and right multiplication
    // in the cases where dimensions of the input (non diagonal) matrix
    // are decreased and increased

    Matrix B_leftmult;
    std::vector<int> iB_leftmult{0, 0, 0, 1, 1, 2, 2};
    std::vector<int> jB_leftmult{0, 1, 3, 1, 3, 0, 2};
    std::vector<Scalar> vB_leftmult{2, 4, 6, -16, 12, 2, 1};
    build_matrix(B_leftmult, 4, 4, NNZ, iB_leftmult.data(), jB_leftmult.data(), vB_leftmult.data());

    Matrix B_rightmult;
    std::vector<int> iB_rightmult{0, 0, 1, 2, 2};
    std::vector<int> jB_rightmult{0, 1, 1, 0, 2};
    std::vector<Scalar> vB_rightmult{2, 8, -16, -4, 1};
    build_matrix(B_rightmult, 3, 3, 5, iB_rightmult.data(), jB_rightmult.data(), vB_rightmult.data());

    SECTION("Multiplication by B"){
      // G * B
      //( 4, 3) * (3, 4) -> (4, 4)
      // left, increasing
      Matrix res = G * B;
      REQUIRE(res == B_leftmult);

      // B * G
      // (3, 4) * (4, 3) -> (3, 3)
      // right, decreasing
      Matrix res2 = B * G; 
      REQUIRE(res2 == B_rightmult);
      Matrix Bcpy = B;
      Bcpy *= G;
      REQUIRE(Bcpy == B_rightmult);
    }

    SECTION("Multiplication by A"){
      // A * F
      //( 4, 3) * (3, 4) -> (4, 4)
      // right, increasing
      Matrix A_rightmult(B_leftmult.t());
      Matrix res = A * F;
      A.display("A");
      F.display("F");
      res.display("AF");
      A_rightmult.display("A_rightmult");
      REQUIRE(res == A_rightmult);
      Matrix Acpy = A;
      Acpy *= F;
      REQUIRE(Acpy == A_rightmult);

      // F * A
      // (3, 4) * (4, 3) -> (3, 3)
      // left, decreasing
      Matrix A_leftmult(B_rightmult.t());
      Matrix res2 = F * A;
      REQUIRE(res2 == A_leftmult);
    }
  }
#+end_src
