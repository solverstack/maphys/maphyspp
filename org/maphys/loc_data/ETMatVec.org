label:data:etmatvec

* Purpose

In this file we implement expression templates to call =BLAS=
function where adding and multiplying dense matrices and vectors.

We note:
- Matrices: $A$ and $B$
- Vectors: $x$ and $y$
- Scalar: $\alpha$ and $\beta$

* Header

#+begin_src c++ :results silent :tangle ../../../include/maphys/loc_data/ETMatVec.hpp :comments link
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>

#include "maphys/loc_data/DenseMatrix.hpp"

// Expression templates

namespace maphys {
#+end_src

* Types
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/loc_data/ETMatVec.hpp :comments link
:END:

** Scaled vector

#+begin_src c++
// Scalar * vector
template<class Scalar>
struct ScalVect{
  using Vect = Vector<Scalar>;
  const Vect& v;
  const Scalar alpha;
  ScalVect(const Vect& vv, const Scalar s): v{vv}, alpha{s} {}
};
#+end_src

** Scaled matrix

#+begin_src c++
// Scalar * matrix
template<class Scalar>
struct ScalMat{
  using Mat = DenseMatrix<Scalar>;
  const Mat& m;
  const Scalar alpha;
  ScalMat(const Mat& mm, const Scalar s): m{mm}, alpha{s} {}
};
#+end_src

** Matrix vector product

#+begin_src c++
template<class Scalar>
struct MatVect{
  using Mat = DenseMatrix<Scalar>;
  using Vect = Vector<Scalar>;

  const Mat& m;
  const Vect& v;
  const Scalar alpha;

  //mat * vect
  //alpha * (mat * vect)
  MatVect(const Mat& mm, const Vect& vv, const Scalar al=Scalar{1.0}): m{mm}, v{vv}, alpha{al} {}
};
#+end_src

** GEMV-like

$\alpha A x + \beta y$

#+begin_src c++
template<class Scalar>
struct ETgemv{
  // alpha * M * X + beta * Y
  using Mat = DenseMatrix<Scalar>;
  using Vect = Vector<Scalar>;

  const Scalar alpha;
  const Scalar beta;
  const Mat& A;
  const Vect& x;
  const Vect& y;

  ETgemv(const Scalar al, const Scalar be, const MatVect<Scalar>& ax, const Vect& v2):
    alpha{al}, beta{be}, A{ax.m}, x{ax.v}, y{v2} {}
};
#+end_src

* Operations
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/loc_data/ETMatVec.hpp :comments link
:END:

** Matrix vector product
$Ax$ 
#+begin_src c++
// Matrix vector product
// Ax
template<class Scalar>
inline MatVect<Scalar> operator*(const DenseMatrix<Scalar>& m, const Vector<Scalar>& v){
  return MatVect<Scalar>(m, v);
}
#+end_src
$\alpha A x$
#+begin_src c++
// alpha * (Ax)
template<class Scalar>
inline MatVect<Scalar> operator*(const Scalar s, const MatVect<Scalar>& mv){
  return MatVect<Scalar>(mv.m, mv.v, mv.alpha * s);
}
#+end_src
$(A x) \alpha$
#+begin_src c++
// (Ax) * alpha
template<class Scalar>
inline MatVect<Scalar> operator*(const MatVect<Scalar>& mv, const Scalar s){
  return MatVect<Scalar>(mv.m, mv.v, mv.alpha * s);
}
#+end_src
$A(\beta x)$
#+begin_src c++
// A * (beta * x)
template<class Scalar>
inline MatVect<Scalar> operator*(const DenseMatrix<Scalar>& m, const ScalVect<Scalar>& sv){
  return MatVect<Scalar>(m, sv.v, sv.alpha);
}
#+end_src
$(\alpha A) x$
#+begin_src c++
// (alpha * A) * x
template<class Scalar>
inline MatVect<Scalar> operator*(const ScalMat<Scalar>& sm, const Vector<Scalar>& v){
  return MatVect<Scalar>(sm.m, v, sm.alpha);
}
#+end_src
$(\alpha A) (\beta x)$
#+begin_src c++
// (alpha * A) * (beta * x)
template<class Scalar>
inline MatVect<Scalar> operator*(const ScalMat<Scalar>& sm, const ScalVect<Scalar>& sv){
  return MatVect<Scalar>(sm.m, sv.v, sm.alpha * sv.alpha);
}
#+end_src

** GEMV

$y + (\alpha A x)$
#+begin_src c++
// GEMV expression template: alpha * M * X + beta * Y
// y + (alpha * Ax)
template<class Scalar>
inline ETgemv<Scalar> operator+(const Vector<Scalar>& v2, const MatVect<Scalar>& ax){
  return ETgemv<Scalar>(ax.alpha, Scalar{1}, ax, v2);
}
#+end_src

$(\alpha A x) + y$
#+begin_src c++
// (alpha * Ax) + y
template<class Scalar>
inline ETgemv<Scalar> operator+(const MatVect<Scalar>& ax, const Vector<Scalar>& v2){
  return ETgemv<Scalar>(ax.alpha, Scalar{1}, ax, v2);
}
#+end_src

$y - (\alpha A x)$
#+begin_src c++
// y - (alpha * Ax)
template<class Scalar>
inline ETgemv<Scalar> operator-(const Vector<Scalar>& v2, const MatVect<Scalar>& ax){
  return ETgemv<Scalar>(Scalar{-1} * ax.alpha, Scalar{1}, ax, v2);
}
#+end_src

$(\alpha A x) - y$
#+begin_src c++
// (alpha * Ax) - y
template<class Scalar>
inline ETgemv<Scalar> operator-(const MatVect<Scalar>& ax, const Vector<Scalar>& v2){
  return ETgemv<Scalar>(ax.alpha, Scalar{-1}, ax, v2);
}
#+end_src

$(\beta y) + (\alpha A x)$
#+begin_src c++
// (beta * y) + (alpha * Ax)
template<class Scalar>
inline ETgemv<Scalar> operator+(const ScalVect<Scalar>& sv, const MatVect<Scalar>& ax){
  return ETgemv<Scalar>(ax.alpha, sv.alpha, ax, sv.v);
}
#+end_src

$(\alpha A x) + (\beta y)$
#+begin_src c++
// (alpha * Ax) + (beta * y)
template<class Scalar>
inline ETgemv<Scalar> operator+(const MatVect<Scalar>& ax, const ScalVect<Scalar>& sv){
  return ETgemv<Scalar>(ax.alpha, sv.alpha, ax, sv.v);
}
#+end_src

$(\beta y) - (\alpha A x)$
#+begin_src c++
// (beta * y) - (alpha * Ax)
template<class Scalar>
inline ETgemv<Scalar> operator-(const ScalVect<Scalar>& sv, const MatVect<Scalar>& ax){
  return ETgemv<Scalar>(Scalar{-1} * ax.alpha, sv.alpha, ax, sv.v);
}
#+end_src

$(\alpha A x) - (\beta y)$
#+begin_src c++
// (alpha * Ax) - (beta * y)
template<class Scalar>
inline ETgemv<Scalar> operator-(const MatVect<Scalar>& ax, const ScalVect<Scalar>& sv){
  return ETgemv<Scalar>(ax.alpha, Scalar{-1} * sv.alpha, ax, sv.v);
}
#+end_src

* Footer

#+begin_src c++ :results silent :tangle ../../../include/maphys/loc_data/ETMatVec.hpp :comments link
} // namespace maphys

#+end_src
