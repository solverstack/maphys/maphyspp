label:data:densedata

#+PROPERTY: header-args: c++ :results silent

* Dense data
  :PROPERTIES:
  :header-args: c++ :tangle ../../../include/maphys/loc_data/DenseData.hpp :comments link
  :END:

  This class allows to manipulate dense data. The memory is either controlled by
  the class itself or can be a pointer given by the user.

  The data itself is viewed as a matrix stored in column-major format with a
  leading dimension. It can perform some 1 level BLAS operations on the columns
  of the matrix (or the vector).

** Header
   #+begin_src c++
     #pragma once

     #include <memory>
     #include <cassert>
     #include <limits>
     #include <iostream>
     #include <iomanip>
     #include <vector>
     #include <string>
     #include <algorithm>

     namespace maphys {
       const int DynamicSize = -1;
       template<class Scalar, int NbCol = DynamicSize> class DenseData;
     }

     #include "maphys/utils/Arithmetic.hpp"
     #include "maphys/utils/MatrixProperties.hpp"
     #include "maphys/utils/IndexArray.hpp"
     #include "maphys/utils/Error.hpp"

     // Try using tlapack if nothing else is found
     #if ! defined(MAPHYSPP_USE_CHAMELEON)
     #if ! defined(MAPHYSPP_USE_LAPACKPP)
     #define MAPHYSPP_USE_TLAPACK
     #endif
     #endif

     #if defined(MAPHYSPP_USE_CHAMELEON)

     #include "maphys/kernel/ChameleonKernels.hpp"

     #endif // MAPHYSPP_USE_CHAMELEON

     #if defined(MAPHYSPP_USE_LAPACKPP)

     #include "maphys/kernel/BlasKernels.hpp"

     #endif // MAPHYSPP_USE_LAPACKPP

     #if defined(MAPHYSPP_USE_TLAPACK)

     namespace tlapack {
       template<typename Scalar, int NbCol>
       maphys::Size size(const maphys::DenseData<Scalar, NbCol>&);
       template<typename Scalar>
       maphys::DenseData<Scalar, 1> slice(const maphys::DenseData<Scalar, 1>&, std::pair<int, int>);

       template<typename Scalar, int NbCol>
       maphys::Size nrows(const maphys::DenseMatrix<Scalar, NbCol>& M);
       template<typename Scalar, int NbCol>
       maphys::Size ncols(const maphys::DenseMatrix<Scalar, NbCol>& M);
       template<typename Scalar, int NbCol>
       maphys::Size size(const maphys::DenseMatrix<Scalar, NbCol>& M);
       template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
       slice(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> fidx, std::pair<int, int> lidx);
       template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
       rows(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> ridx);
       template<typename Scalar, int NbCol> maphys::DenseMatrix<Scalar, NbCol>
       cols(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> cidx);
       template<typename Scalar, int NbCol> maphys::Vector<Scalar>
       row(const maphys::DenseMatrix<Scalar, NbCol>& M, int i);
       template<typename Scalar, int NbCol> maphys::Vector<Scalar>
       col(const maphys::DenseMatrix<Scalar, NbCol>& M, int j);
       template<typename Scalar, int NbCol> maphys::Vector<Scalar>
       slice(const maphys::DenseMatrix<Scalar, NbCol>& M, int i, std::pair<int, int> lidx);
       template<typename Scalar, int NbCol> maphys::Vector<Scalar>
       slice(const maphys::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> lidx, int j);
       template<typename Scalar, int NbCol> maphys::Vector<Scalar>
       diag(const maphys::DenseMatrix<Scalar, NbCol>& M);
       template<typename Scalar, int NbCol> maphys::Vector<Scalar>
       diag(const maphys::DenseMatrix<Scalar, NbCol>& M, int i);
       template<typename Scalar> maphys::Vector<Scalar>
       slice(const maphys::DenseMatrix<Scalar, 1>& M, std::pair<int, int> idxs);
     } // namespace tlapack

     #include "maphys/kernel/TlapackKernels.hpp"

     #endif // MAPHYSPP_USE_TLAPACK

     #if defined(MAPHYSPP_USE_CHAMELEON)
     using COMPOSE_BLAS = maphys::chameleon_kernels;
     #else
     #if defined(MAPHYSPP_USE_LAPACKPP)
     using COMPOSE_BLAS = maphys::blas_kernels;
     #else
     using COMPOSE_BLAS = maphys::tlapack_kernels;
     #endif // MAPHYSPP_USE_LAPACKPP
     #endif // MAPHYSPP_USE_CHAMELEON

     #include "maphys/loc_data/DenseMatrix.hpp"

     namespace maphys {
   #+end_src

** Attributes

   The data is an =_m= by =_n= matrix, with a leading dimension =_ld=. It is
   also possible to set the number of columns =NbCol= staticly, which allows to
   use more "vector-like" methods.

   Concerning the ownership of the data, two cases are possible.
   - The class instance owns the data (=_is_view= is =false=), which is stored in
     =_data=, and =_ptr= points to the address of the first element of =_data=.
     Then =_ld= is always equal to =_m=.
   - The class instance does not own the data (=_is_view= is =true=). The user gave a
     raw pointer to the class, which is the address of the first element, copied
     in =_ptr=. In that case =_ld= can be superior to =_m=, and =_data= is an
     empty array.

   #+begin_src c++
     template<class Scalar, int NbCol>
     class DenseData {
     public:
       using scalar_type = Scalar;
       using value_type = Scalar;
       using real_type = typename arithmetic_real<Scalar>::type;

     private:
       bool _trans = false;

       using Real = real_type;
       using DataArray = std::vector<Scalar>;

       Size _m{0};
       Size _n{0};
       Size _ld{1};
       Size _inc{1};
       bool _is_view = false;
       DataArray _data;
       Scalar * _ptr = nullptr;
   #+end_src

** Constructors

  If the user gives a raw pointer, the class will not copy or own any data.
  Otherwise it will manipulate the data in =_data=.

   #+begin_src c++
     public:

     // Constructors without weak pointer
     explicit DenseData(const Size m, const Size n = 1):
       _m{m},
       _n{n},
       _ld{m},
       _inc{1},
       _is_view{false}
     {
       if constexpr(NbCol >= 0){
	 if(n > 1){
	   MAPHYSPP_DIM_ASSERT(n, NbCol, "Error: creating matrix with N != NbCol staticly specified");
	 }
	 _n = NbCol;
       }
       _data = DataArray(_m * _n, Scalar{0});
       _ptr = &_data[0];
     }

     explicit DenseData(const std::initializer_list<Scalar>& l, const Size m = 0, const Size n = 1):
       _m{m},
       _n{n},
       _ld{m},
       _inc{1},
       _is_view{false}
     {
       if constexpr(NbCol >= 0){ _n = NbCol; }
       if(_m == 0) _m = l.size() / _n;
       _ld = _m;
       MAPHYSPP_ASSERT(_m * _n == l.size(), "DenseData::DenseData({}) List size different from m * n");
       _data = DataArray(l);
       _ptr = &_data[0];
     }

     explicit DenseData(): DenseData(0,Size{0}) {}

     // Constructors with weak pointer
     explicit DenseData(const Size m, Scalar * ptr, Size ld = 0, Size inc = 1):
       _m{m},
       _n{static_cast<Size>(NbCol)},
       _ld{ld},
       _inc{inc},
       _is_view{true},
       _ptr{ptr}
     {
       if(_ld == 0){
	 _ld = _m;
       }
       static_assert(NbCol != DynamicSize, "Error: while creating matrix, number of columns must be specified");
       MAPHYSPP_ASSERT(_ld >= _m, "DenseData: leading dimension smaller that row dimension.");
     }

     explicit DenseData(const Size m, const Size n, Scalar * ptr, Size ld = 0, Size inc = 1):
       _m{m},
       _n{n},
       _ld{ld},
       _inc{inc},
       _is_view{true},
       _ptr{ptr}
     {
       if(_ld == 0){
	 _ld = _m;
       }

       if constexpr (NbCol != DynamicSize){
	 MAPHYSPP_DIM_ASSERT(n, NbCol, "Error: creating matrix with N != NbCol staticly specified");
       }
       MAPHYSPP_ASSERT(_ld >= _m, "DenseData: leading dimension smaller that row dimension.");
     }

     private:
     void _set_ptr(Scalar * other_ptr){
       if(_is_view){
	 _ptr = other_ptr;
       }
       else{
	 _ptr = &_data[0];
       }
     }

     public:

     // Copy constructor
     DenseData(const DenseData& d):
       _m{d._m},
       _n{d._n},
       _ld{d._ld},
       _inc{d._inc},
       _is_view{d._is_view},
       _data{d._data}
     {
       _set_ptr(d._ptr);
     }

     // Move constructor
     DenseData(DenseData&& d):
       _m{std::exchange(d._m, 0)},
       _n{std::exchange(d._n, 0)},
       _ld{std::exchange(d._ld, 1)},
       _inc{std::exchange(d._inc, 1)},
       _is_view{std::exchange(d._is_view, false)},
       _data{std::move(d._data)}
     {
       _set_ptr(d._ptr);
     }

     // Copy assignment
     DenseData& operator=(const DenseData& copy){
       if(copy.get_n_rows() == _m && copy.get_n_cols() == _n){
	 copy_values(copy);
       }
       else if(copy.is_view()){
	 make_shallowcopy_of(copy);
       }
       else{
	 make_deepcopy_of(copy);
       }
       return *this;
     }

     DenseData& operator=(DenseData&& copy){
       if(copy.get_n_rows() == _m && copy.get_n_cols() == _n){
	 copy_values(copy);
       }
       else if(copy.is_view()){
	 make_shallowcopy_of(copy);
       }
       else{
	 _m = copy.get_n_rows();
	 _n = copy.get_n_cols();
	 _ld = copy.get_leading_dim();
	 _inc = copy.get_increment();
	 _is_view = false;
	 _data = std::move(copy._data);
	 _ptr =  &_data[0];
       }
       copy.make_deepcopy_of(DenseData());
       return *this;
     }

     template<int OtherNbCol>
     DenseData& operator=(const DenseData<Scalar, OtherNbCol>& copy){
       static_assert(NbCol == DynamicSize || OtherNbCol == DynamicSize || NbCol ==  OtherNbCol, "DenseData copy: incompatible static NbCol");
       if(copy.is_view()){
	 make_shallowcopy_of(copy);
       }
       else{
	 make_deepcopy_of(copy);
       }
       return *this;
     }

     // Move assignment
     template<int OtherNbCol>
     DenseData& operator=(DenseData<Scalar, OtherNbCol>&& copy){
       static_assert(NbCol == DynamicSize || OtherNbCol == DynamicSize || NbCol ==  OtherNbCol, "DenseData copy: incompatible static NbCol");
       if(copy.is_view()){
	 make_shallowcopy_of(copy);
       }
       else{
	 _m = copy.get_n_rows();
	 _n = copy.get_n_cols();
	 _ld = copy.get_leading_dim();
	 _inc = copy.get_increment();	 
	 _is_view = false;
	 _data = copy.move_data_array();
	 _ptr = &_data[0];
       }
       copy.make_deepcopy_of(DenseData<Scalar, OtherNbCol>());
       return *this;
     }
   #+end_src

** Copy

   By default, the copy constructor and copy assignment operators will make a
   shallow copy if the pointer is weak and a deep copy otherwise. You can be more
   specific with this function if you want a deep or a shallow copy.

   #+begin_src c++
     // Copy values: dimensions must be identical (no check)
     template<int OtherNbCol>
     void copy_values(const DenseData<Scalar, OtherNbCol>& other){
       if(_n == 1 and (_inc != 1 or other.get_increment() != 1)){
	 for(Size i = 0; i < _m; ++i){ (*this)[i] = other[i]; }
       } else {
	 if((other.get_leading_dim() == other.get_n_rows()) && (this->get_leading_dim() == this->get_n_rows())){
	   std::memcpy(_ptr, other.get_ptr(), _n * _m * sizeof(Scalar));
	 }
	 else{
	   for(Size j = 0; j < _n; ++j){
	     std::memcpy(this->get_vect_ptr(j), other.get_vect_ptr(j), _m * sizeof(Scalar));
	   }
	 }
       }
     }

     template<int OtherNbCol>
     void make_deepcopy_of(const DenseData<Scalar, OtherNbCol>& other){
       if(_is_view == true || _m != other.get_n_rows() || _n != other.get_n_cols()){
	 _data = DataArray(other.get_n_rows() * other.get_n_cols());
       }
       _m = other.get_n_rows();
       _n = other.get_n_cols();
       _ld = _m;
       _inc = 1;
       _is_view = false;
       _ptr = &_data[0];
       this->copy_values(other);
     }

     template<int OtherNbCol>
     void make_shallowcopy_of(const DenseData<Scalar, OtherNbCol>& other){
       _m = other.get_n_rows();
       _n = other.get_n_cols();
       _ld = other.get_leading_dim();
       _inc = other.get_increment();
       _is_view = true;
       _data = DataArray();
       _ptr = const_cast<Scalar *>(other.get_ptr());
     }

     DenseData deepcopy() const {
       DenseData d;
       d.make_deepcopy_of(*this);
       return d;
     }

     DenseData shallowcopy() const {
       DenseData d;
       d.make_shallowcopy_of(*this);
       return d;
     }
   #+end_src

** Comparison

   #+begin_src c++
     template<int OtherNbCol>
     bool operator==(const DenseData<Scalar, OtherNbCol>& other) const {
       if(get_n_rows() != other.get_n_rows()) return false;
       if(get_n_cols() != other.get_n_cols()) return false;
       for(Size j = 0; j < get_n_cols(); ++j){
	 for(Size i = 0; i < get_n_rows(); ++i){
	   if((*this)(i, j) != other(i, j)) return false;
	 }
       }
       return true;
     }

     template<int OtherNbCol>
     bool operator!=(const DenseData<Scalar, OtherNbCol>& other) const {
       return !(*this == other);
     }
   #+end_src

** Reshape

   #+begin_src c++
     void reshape(Size new_m, Size new_n){
       MAPHYSPP_ASSERT(NbCol == DynamicSize || NbCol == static_cast<int>(new_n), "DenseData:reshape cannot be called with static NbCol != new_n");
       MAPHYSPP_ASSERT(!_is_view, "DenseData:reshape cannot be called weak pointer");
       MAPHYSPP_DIM_ASSERT(new_m * new_n, _m * _n, "DenseData:reshape (new nb of elts) =! (old nb of elts)");
       _m = new_m;
       _n = new_n;
       _ld = _m;
       _inc = 1;
     }
   #+end_src

** Scalar multiplication

#+begin_src c++
  DenseData& operator*=(Scalar scal){
    for(Size j = 0; j < _n; ++j){
      auto xj = this->get_vect_view(j);
      COMPOSE_BLAS::scal(xj, scal);
    }
    return *this;
  }
#+end_src

** Addition

   #+begin_src c++
     void add(const DenseData& other, Scalar alpha = Scalar{1.0}){
       for(Size j = 0; j < _n; ++j){
	 auto xj = other.get_vect_view(j);
	 auto yj = this->get_vect_view(j);
	 COMPOSE_BLAS::axpy(xj, yj, alpha);
       }
     }

     DenseData& operator+=(const DenseData& other){
       this->add(other);
       return *this;
     }

     DenseData& operator-=(const DenseData& other){
       this->add(other, Scalar{-1.0});
       return *this;
     }
   #+end_src

** Dot

   Dot product of the vector in column $j_1$ with the vector in column $j_2$ of
   =other=.

   #+begin_src c++
     Scalar dot(const DenseData& other) const {
       return COMPOSE_BLAS::dot(*this, other);
     }
   #+end_src

** Norm-2 and norm-2 squared

   Careful: The norm-2 and norm-2 squared return =Real= valued (not =Scalar=).
   If a =Scalar= is needed it is more convenient to use the =dot= method directly.

   The =squared= method is the dot product of the vector in column $j$ with itself;
   =norm= is the square root of this value.

   #+begin_src c++
     Real squared() const {
       return COMPOSE_BLAS::norm2_squared(*this);
     }

     Real norm() const {
       return COMPOSE_BLAS::norm2(*this);
     }
   #+end_src

** Getters

   #+begin_src c++
     [[nodiscard]] inline Size get_n_rows() const { return _trans ? _n : _m; }
     [[nodiscard]] inline Size get_n_cols() const { return _trans ? _m : _n; }
     [[nodiscard]] inline Size get_leading_dim() const { return _ld; }
     [[nodiscard]] inline Size get_increment() const { return _inc; }
     [[nodiscard]] inline const Scalar* get_ptr() const { return _ptr; }
     [[nodiscard]] inline Scalar* get_ptr() { return _ptr; }
     [[nodiscard]] inline bool is_view() const { return _is_view; }
     [[nodiscard]] inline const DataArray get_data_array() const { return _data; }
     [[nodiscard]] inline DataArray&& move_data_array() { return std::move(_data); }
     [[nodiscard]] inline bool is_transpose() { return _trans; }
   #+end_src

** Accessors

   Private method to obtain the index of the element of indices =(i, j)=.

   #+begin_src c++
     private:
     inline int _index(const int i, const int j) const {
       if constexpr(NbCol == 1){
	 (void) j;
	 return i * _inc;
       } else {
	 return _trans ? (i * _ld + j) : (j * _ld + i);
       }
     }
   #+end_src

   Use =operator()(i, j)= to obtain a reference to the coefficient on indices $i, j$.

   #+begin_src c++
     public:
     [[nodiscard]] inline const Scalar& operator()(const int i, const int j) const {
       return _ptr[_index(i, j)];
     }

     [[nodiscard]] inline Scalar& operator()(const int i, const int j) {
       return _ptr[_index(i, j)];
     }
   #+end_src

   Use =at= to obtain a reference, but checking boundaries. Useful for debugging.

   #+begin_src c++
     private:
     [[nodiscard]] inline auto& _at(const int i, const int j){
       try {
	 MAPHYSPP_ASSERT_POSITIVE(i, "DenseData.at(i, j) with i < 0" );
	 MAPHYSPP_ASSERT_POSITIVE(j, "DenseData.at(i, j) with j < 0" );
	 MAPHYSPP_ASSERT(i < static_cast<int>(get_n_rows()), "DenseData.at(i, j) with i >= nb rows");
	 MAPHYSPP_ASSERT(j < static_cast<int>(get_n_cols()), "DenseData.at(i, j) with j >= nb cols");
       }
       catch(const std::runtime_error& e) {
	 std::cerr << e.what();
	 std::cerr << "(i, j), (nrows, ncols): " << '(' << i << ',' << j << "), (" << get_n_rows() << ',' << get_n_cols() << ")\n";
	 MAPHYSPP_ASSERT(false, "Fatal error DenseData.at(i, j) index out of range");
       }
       return (*this)(i, j);
     }

     public:
     [[nodiscard]] inline const Scalar& at(const int i, const int j) const { return this->_at(i, j); }
     [[nodiscard]] inline Scalar& at(const int i, const int j) { return this->_at(i, j); }
   #+end_src

   If we have a vector, we also want =operator()(i)= to work as well as =operator[](i)= and =operator[](i)=.

   #+begin_src c++
     template<MPH_Integral Tint> [[nodiscard]] inline const Scalar& operator()(const Tint i) const { return _ptr[i * _inc]; }
     template<MPH_Integral Tint> [[nodiscard]] inline Scalar& operator()(const Tint i) { return _ptr[i * _inc]; }
     template<MPH_Integral Tint> [[nodiscard]] inline const Scalar& operator[](const Tint i) const { return _ptr[i * _inc]; }
     template<MPH_Integral Tint> [[nodiscard]] inline Scalar& operator[](const Tint i) { return _ptr[i * _inc]; }

     private:
     template<MPH_Integral Tint> [[nodiscard]] inline auto& _at_vec(const Tint i){
       MAPHYSPP_ASSERT(get_n_cols() == 1, "Using DenseData.at(i) is not allowed if data is not a vector");
       bool in_range = true;
       if(i < 0) { std::cerr << "(i < 0): i = " << i << '\n'; in_range = false; }
       if(i >= static_cast<Tint>(get_n_rows())) { std::cerr << "(i >= M): i = " << i << " ; M = " << _m << '\n'; in_range = false; }
       MAPHYSPP_ASSERT(in_range, "DenseData.at(i) out of range M");
       return _ptr[i * _inc];
     }
     
     public:
     template<MPH_Integral Tint> [[nodiscard]] inline const Scalar& at(const Tint i) const { return this->_at_vec(i); }
     template<MPH_Integral Tint> [[nodiscard]] inline Scalar& at(const Tint i) { return this->_at_vec(i); }
   #+end_src

   Get a vector from the matrix.

   #+begin_src c++
     template<MPH_Integral Tint>
     [[nodiscard]] inline Scalar * get_vect_ptr(Tint j = 0) { return &(*this)(0, static_cast<Tint>(j)); }
     template<MPH_Integral Tint>
     [[nodiscard]] inline const Scalar * get_vect_ptr(Tint j = 0) const { return &(*this)(0, static_cast<Tint>(j)); }

     template<MPH_Integral Tint>
     [[nodiscard]] inline DenseData<Scalar, 1> get_vect_view(Tint j = 0) {
       return DenseData<Scalar, 1>(this->_m, this->get_vect_ptr(j), 0, this->get_increment());
     }
     template<MPH_Integral Tint>
     [[nodiscard]] inline const DenseData<Scalar, 1> get_vect_view(Tint j = 0) const {
       return DenseData<Scalar, 1>(this->_m, const_cast<Scalar*>(this->get_vect_ptr(j)), 0, this->get_increment());
     }
   #+end_src

** Transposition

   #+begin_src c++
   void transpose(){ _trans = !_trans; }
   #+end_src

** Display

   #+begin_src c++
     void display(const std::string& name="", std::ostream &out = std::cout) const {
       if(!name.empty()) out << name << '\n';
       out << "n_rows: " << get_n_rows() << " "
	   << "n_cols: " << get_n_cols() << " ";
       if(_trans) out << " (transposed) ";
       out << "ld: " << _ld << "\n\n";
       out << "inc: " << _inc << "\n\n";       

       if(_n == 1 and _inc != 1){
	 for(Size i = 0; i < get_n_rows(); ++i){
	   out << (*this)[i] << '\n';
	 }
       } else {

	 for(Size i = 0; i < get_n_rows(); ++i){
	   for(Size j = 0; j < get_n_cols(); ++j){
	     out << (*this)(i, j) << '\t';
	   }
	   out << '\n';
	 }

       }
       out << '\n';
     }
   #+end_src

** Interface functions

   #+begin_src c++
     }; //class DenseData

     template<MPH_Scalar Scalar, int NbCol>
     Size size(const DenseData<Scalar, NbCol>& d){ return d.get_n_rows(); }

     template<MPH_Scalar Scalar, int NbCol>
     Size get_leading_dim(const DenseData<Scalar, NbCol>& d){ return d.get_leading_dim(); }

     template<MPH_Scalar Scalar, int NbCol>
     Size get_increment(const DenseData<Scalar, NbCol>& d){ return d.get_increment(); }

     template<MPH_Scalar Scalar, int NbCol>
     Scalar * get_ptr(DenseData<Scalar, NbCol>& d){ return d.get_ptr(); }

     template<MPH_Scalar Scalar, int NbCol>
     const Scalar * get_ptr(const DenseData<Scalar, NbCol>& d){ return d.get_ptr(); }
   #+end_src

** Footer

   #+begin_src c++
     } // namespace maphys
   #+end_src

** Tlapack interface

#+begin_src c++
  #ifdef MAPHYSPP_USE_TLAPACK

  namespace tlapack {
    template<typename Scalar, int NbCol>
    maphys::Size size(const maphys::DenseData<Scalar, NbCol>& d){ return d.get_n_rows(); }

    template<typename Scalar> maphys::DenseData<Scalar, 1>
    slice(const maphys::DenseData<Scalar, 1>& d, std::pair<int, int> idxs){
      return DenseData<Scalar, 1>(idxs.second - idxs.first, &d[idxs.first], 0, d.get_increment());
    }
  } // namespace tlapack
  #endif
#+end_src

   
* Tests
  :PROPERTIES:
  :header-args: c++ :tangle ../../../src/test/unittest/test_dense_data.cpp :comments link
  :END:

  #+begin_src c++
    #include <iostream>
    #include <vector>
    #include <complex>
    #include <maphys.hpp>
    #include <maphys/loc_data/DenseData.hpp>
    #include <catch2/catch_test_macros.hpp>
    #include <catch2/catch_template_test_macros.hpp>

    using namespace maphys;

    TEMPLATE_TEST_CASE("Dense data", "[densedata]", float, double, std::complex<float>, std::complex<double>){
      using Scalar = TestType;
      using Real = typename arithmetic_real<Scalar>::type;

      std::vector<Real> some_reals1{1.0, -1.0, 3.33, 0.0, -2.1, 12.0, 6.6, 6.8, 0.0, -23};
      std::vector<Scalar> scv(some_reals1.size());
      if constexpr(is_complex<Scalar>::value){
	  std::vector<Real> some_reals2{1.0, 1.0, 2.33,  0.0, -8.1, 1.0, 6.6, 0.0, 0.0, 1.0};
	  for(int i = 0; i < static_cast<int>(some_reals1.size()); ++i) scv[i] = some_reals1[i] + Scalar{0, 1} * some_reals2[i];
	}
      else{
	for(int i = 0; i < static_cast<int>(some_reals1.size()); ++i) scv[i] = static_cast<Scalar>(some_reals1[i]);
      }

      // Constructors

      SECTION("Constructor DenseData(m, n=1)"){
	DenseData<Scalar> vect_3(3);
	REQUIRE(vect_3.get_n_rows() == 3);
	REQUIRE(vect_3.get_n_cols() == 1);
	REQUIRE(vect_3.get_leading_dim() == 3);

	DenseData<Scalar> mat_3_2(4, 2);
	REQUIRE(mat_3_2.get_n_rows() == 4);
	REQUIRE(mat_3_2.get_n_cols() == 2);
	REQUIRE(mat_3_2.get_leading_dim() == 4);

	// Error if specifying n != static NbCol
	REQUIRE_THROWS(DenseData<Scalar, 3>(3, 2));

	DenseData<Scalar> v0;
	REQUIRE(v0.get_n_rows() == 0);
	REQUIRE(v0.get_n_cols() == 0);
      }

      SECTION("Constructor DenseData({1, 2, 3}, m = 0, n = 1)"){
	DenseData<Scalar> v123({scv[0], scv[1], scv[2]});
	for(int i = 0; i < 3; ++i) REQUIRE(v123.at(i) == scv[i]);
	DenseData<Scalar> v123_m3({scv[0], scv[1], scv[2]}, 3);
	for(int i = 0; i < 3; ++i) REQUIRE(v123_m3.at(i) == scv[i]);
	DenseData<Scalar, 2> m_3_2({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5]}, 3);
	REQUIRE(m_3_2.get_n_rows() == 3);
	REQUIRE(m_3_2.get_n_cols() == 2);
	DenseData<Scalar> m_3_2_dyn({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5]}, 3, 2);
	REQUIRE(m_3_2_dyn.get_n_rows() == 3);
	REQUIRE(m_3_2_dyn.get_n_cols() == 2);
	for(int j = 0; j < 2; ++j){
	  for(int i = 0; i < 3; ++i){
	    REQUIRE(m_3_2.at(i, j) == scv[j*3+i]);
	    REQUIRE(m_3_2_dyn.at(i, j) == scv[j*3+i]);
	  }
	}
      }

      SECTION("Constructor DenseData(m, * ptr, ld = 0)"){
	DenseData<Scalar, 1> v1(3, &scv[0]);
	REQUIRE(v1.get_n_rows() == 3);
	REQUIRE(v1.get_n_cols() == 1);
	REQUIRE(v1.get_leading_dim() == 3);

	DenseData<Scalar, 2> v2(3, &scv[0]);
	REQUIRE(v2.get_n_rows() == 3);
	REQUIRE(v2.get_n_cols() == 2);
	REQUIRE(v2.get_leading_dim() == 3);

	DenseData<Scalar, 2> v3(3, &scv[0], 4);
	REQUIRE(v3.get_n_rows() == 3);
	REQUIRE(v3.get_n_cols() == 2);
	REQUIRE(v3.get_leading_dim() == 4);
      }

      SECTION("Constructor DenseData(m, n, * ptr, ld = 0)"){
	DenseData<Scalar> m1(3, 2, &scv[0]);
	REQUIRE(m1.get_n_rows() == 3);
	REQUIRE(m1.get_n_cols() == 2);
	REQUIRE(m1.get_leading_dim() == 3);

	DenseData<Scalar> m2(3, 2, &scv[0], 4);
	REQUIRE(m2.get_n_rows() == 3);
	REQUIRE(m2.get_n_cols() == 2);
	REQUIRE(m2.get_leading_dim() == 4);

	REQUIRE_THROWS(DenseData<Scalar>(3, 2, &scv[0], 2));
	REQUIRE_THROWS(DenseData<Scalar, 3>(3, 2, &scv[0]));
      }

      SECTION("Copies"){
	DenseData<Scalar> m_base_weak(3, 2, &scv[0]);
	DenseData<Scalar> deep1 = m_base_weak.deepcopy();
	DenseData<Scalar> shal1 = m_base_weak.shallowcopy();
	REQUIRE(m_base_weak.get_ptr() == shal1.get_ptr());
	REQUIRE(m_base_weak.get_ptr() != deep1.get_ptr());
	DenseData<Scalar> m_base_strong(3, 2);
	DenseData<Scalar> deep2 = m_base_strong.deepcopy();
	DenseData<Scalar> shal2 = m_base_strong.shallowcopy();
	REQUIRE(m_base_strong.get_ptr() == shal2.get_ptr());
	REQUIRE(m_base_strong.get_ptr() != deep2.get_ptr());
      }

      SECTION("Move"){
	const DenseData<Scalar> v_ref({scv[0], scv[1], scv[2]}, 3);
	DenseData<Scalar> v(v_ref);
	DenseData<Scalar> vm(std::move(v));
	REQUIRE(vm == v_ref);
	REQUIRE(v.get_n_rows() == 0);
	REQUIRE(v.get_n_cols() == 0);
	REQUIRE(v.get_leading_dim() == 1);
	DenseData<Scalar> v2(v_ref);
	DenseData<Scalar> vm2 = std::move(v2);
	REQUIRE(vm2 == v_ref);
	REQUIRE(v2.get_n_rows() == 0);
	REQUIRE(v2.get_n_cols() == 0);
	REQUIRE(v2.get_leading_dim() == 1);
      }

      SECTION("Move with view"){
	const DenseData<Scalar> v_ref({scv[0], scv[1], scv[2]}, 3);
	DenseData<Scalar> v(3, 1, &scv[0]);
	DenseData<Scalar> vm(std::move(v));
	REQUIRE(vm == v_ref);
	REQUIRE(v.get_n_rows() == 0);
	REQUIRE(v.get_n_cols() == 0);
	REQUIRE(v.get_leading_dim() == 1);
	REQUIRE(vm.get_ptr() == &scv[0]);
	DenseData<Scalar> v2(3, 1, &scv[0]);
	DenseData<Scalar> vm2 = std::move(v2);
	REQUIRE(vm2 == v_ref);
	REQUIRE(v2.get_n_rows() == 0);
	REQUIRE(v2.get_n_cols() == 0);
	REQUIRE(v2.get_leading_dim() == 1);
	REQUIRE(vm2.get_ptr() == &scv[0]);
      }

      SECTION("Scalar multiplication"){
	DenseData<Scalar, 1> v{scv[0], scv[1], scv[2]};
	const Scalar s{2.0};
	v *= s;
	for(int i = 0; i < 3; ++i) REQUIRE(v.at(i) == (s * scv[i]));

	DenseData<Scalar> m({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5]}, 3, 2);
	m *= 2;
	for(int j = 0; j < 2; ++j){
	  for(int i = 0; i < 3; ++i){
	    REQUIRE(m.at(i, j) == scv[j*3+i] * s);
	  }
	}
      }

      SECTION("Addition"){
	DenseData<Scalar, 1> v1{scv[0], scv[1], scv[2]};
	DenseData<Scalar, 1> v2{scv[3], scv[4], scv[5]};
	v1 += v2;
	for(int i = 0; i < 3; ++i) REQUIRE(v1.at(i) == scv[i] + scv[3+i]);

	DenseData<Scalar> m1({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5]}, 3, 2);
	DenseData<Scalar> m2({scv[1], scv[2], scv[3], scv[4], scv[5], scv[6]}, 3, 2);
	m1 -= m2;
	for(int j = 0; j < 2; ++j){
	  for(int i = 0; i < 3; ++i){
	    REQUIRE(m1.at(i, j) == (scv[j*3+i] - scv[j*3+i+1]));
	  }
	}
      }

      SECTION("Dot, squared, norm"){
	Real tol = arithmetic_tolerance<Scalar>::value;
	DenseData<Scalar, 1> v1{scv[0], scv[1], scv[2]};
	DenseData<Scalar, 1> v2{scv[3], scv[4], scv[5]};

	Scalar dot_exp = conj(scv[0]) * scv[3] + conj(scv[1]) * scv[4] + conj(scv[2]) * scv[5];
	REQUIRE(std::abs(std::abs(v1.dot(v2)) - std::abs(dot_exp)) < tol);

	Real squared_exp = std::real(conj(scv[0]) * scv[0] + conj(scv[1]) * scv[1] + conj(scv[2]) * scv[2]);
	REQUIRE(std::abs(v1.squared() - std::abs(squared_exp)) < tol);

	Real norm_exp = std::real(std::sqrt(squared_exp));
	REQUIRE(std::abs(v1.norm() - norm_exp) < tol);
      }

      SECTION("Testing scal on a block (ld > m)"){
	// We multiply the 2x2 middle block of a 4x2 matrix by 2
	// 0 4     - -
	// 1 5     X X
	// 2 6 --> X X
	// 3 7     - -
	// with - outside of the block and X in the block
	const Scalar scal{2};
	DenseData<Scalar> mi({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5], scv[6], scv[7]}, 4, 2);
	DenseData<Scalar> mf({scv[0], scal*scv[1], scal*scv[2], scv[3], scv[4], scal*scv[5], scal*scv[6], scv[7]}, 4, 2);

	DenseData<Scalar> block(2, 2, &mi(1, 0), 4);
	block *= scal;
	REQUIRE(mi == mf);
      }

      SECTION("Transposition"){
	DenseData<Scalar>       vect_test({1, 2, 3, 4, 5, 6}, 2, 3);
	const DenseData<Scalar> vect_orig({1, 2, 3, 4, 5, 6}, 2, 3);
	const DenseData<Scalar> vect_tr({1, 3, 5, 2, 4, 6}, 3, 2);

	vect_test.transpose();
	REQUIRE(vect_test == vect_tr);

	vect_test.transpose();
	REQUIRE(vect_test == vect_orig);
      }
    }
#+end_src
