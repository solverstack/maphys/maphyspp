label:dist:mpi

* Purpose

In this file we want to implement mpi
routine with templates. Such implementations
already exist but are not standard as opposed to the
C interface underneath.

In order to avoid conflicts with MPI namespaces
(such as ones in openmpi), we will use the namespace
=MMPI= (MaPHyS MPI) to implement our functions.

Lastly we will use the interface to write a function
=isendMessages= that takes messages in input,
as needed for the =Subdomain= class.


* MPI class
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/maphys/dist/MPI.hpp :comments link
:END:

** Header

#+begin_src c++
  #pragma once

  #include <mpi.h>
  #include <memory>
  #include <cassert>
  #include <iostream>
  #include <vector>
  #include <tuple>
  #include <cstring>

  #include "maphys/utils/Arithmetic.hpp"
  #include "maphys/utils/Error.hpp"
  #include "maphys/utils/IndexArray.hpp"

  namespace maphys {
#+end_src

** Message structure

We want a simple messages structure in input
that is not MPI dependant. The simplest description
of a message is given here:

#+begin_src c++
  template <class T>
  struct Message {
    T* buffer = nullptr;
    int size = 0;
    int destination = -1;
    int tag = 1993;

    Message() = default;
    Message(T* b, int s, int d, int t = 1993)
      : buffer{b}, size{s}, destination{d}, tag{t} {}
    Message(const T* b, int s, int d, int t = 1993)
      : buffer{const_cast<T*>(b)}, size{s}, destination{d}, tag{t} {}
  };
#+end_src

** MMPI namespace

In this namespace we wrap the MPI routines
in C++. It might seem that we reinvent the wheel,
but this it allows us to have more control over
MPI routines, include timers or counters if necessary,
ensure portability with all MPI implementations...

#+begin_src c++
    namespace MMPI {

      static int current_tag = 0;  // To be used for isendMessages
      static int provided;
      template <class T>
      inline MPI_Datatype mpi_type() {
        return MPI_DATATYPE_NULL;
      }

  #define MPH_DEFINE_MPI_DATATYPE(_c_type, _mpi_type) \
      template <>                                     \
      inline MPI_Datatype mpi_type<_c_type>() {       \
        return _mpi_type;                             \
      }
      MPH_DEFINE_MPI_DATATYPE(char, MPI_CHAR)
      MPH_DEFINE_MPI_DATATYPE(int, MPI_INT)
      MPH_DEFINE_MPI_DATATYPE(long int, MPI_LONG)
      MPH_DEFINE_MPI_DATATYPE(unsigned int, MPI_UNSIGNED)
      MPH_DEFINE_MPI_DATATYPE(float, MPI_FLOAT)
      MPH_DEFINE_MPI_DATATYPE(double, MPI_DOUBLE)
      MPH_DEFINE_MPI_DATATYPE(std::complex<float>, MPI_C_FLOAT_COMPLEX)
      MPH_DEFINE_MPI_DATATYPE(std::complex<double>, MPI_C_DOUBLE_COMPLEX)

      inline int init(int thread_required = MPI_THREAD_MULTIPLE) {
        int res_mpi_init = MPI_Init_thread(NULL, NULL, thread_required, &provided);
        if (provided < thread_required)
          MAPHYSPP_WARNING(
                           "The threading support level is less than that demanded.\n");
        return res_mpi_init;
      }
      inline int finalize() { return MPI_Finalize(); }

      inline int rank(const MPI_Comm comm = MPI_COMM_WORLD) {
        int rank;
        MPI_Comm_rank(comm, &rank);
        return rank;
      }

      inline int size(const MPI_Comm comm = MPI_COMM_WORLD) {
        int size;
        MPI_Comm_size(comm, &size);
        return size;
      }

      inline MPI_Comm comm_dup(const MPI_Comm comm) {
        MPI_Comm duplicated;
        MPI_Comm_dup(comm, &duplicated);
        return duplicated;
      }

      template <class T>
      inline void send(const T* buffer, const int count, const int tag,
                       const int dest, const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Send(buffer, count, mpi_type<T>(), dest, tag, comm);
      }

      template <class T>
      inline MPI_Status recv(T* buffer, const int count, const int tag,
                             const int source, const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Status stat;
        MPI_Recv(buffer, count, mpi_type<T>(), source, tag, comm, &stat);
        return stat;
      }

      template <class T>
      inline void isend(const T* buffer, const int count, const int tag,
                        const int dest, MPI_Request& req,
                        const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Isend(buffer, count, mpi_type<T>(), dest, tag, comm, &req);
      }

      template <class T>
      inline void irecv(T* buffer, const int count, const int tag, const int source,
                        MPI_Request& req, const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Irecv(buffer, count, mpi_type<T>(), source, tag, comm, &req);
      }

      //----
      template <class T>
      inline void gather(const T* send_buffer, T* recv_buffer, const int count,
                         const int root, const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Gather(send_buffer, count, mpi_type<T>(), recv_buffer, count,
                   mpi_type<T>(), root, comm);
      }

      template <class T, class IntArray>
      inline void gatherv(const T* send_buffer, T* recv_buffer, const int count,
                          const IntArray& recvcounts, const IntArray& displ,
                          const int root, const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Gatherv(send_buffer, count, mpi_type<T>(), recv_buffer, recvcounts.data(),
                    displ.data(), mpi_type<T>(), root, comm);
      }

      template <class T>
      inline void reduce(const T* send_buffer, T* recv_buffer, const int count,
                         const MPI_Op operation, const int root,
                         const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Reduce(send_buffer, recv_buffer, count, mpi_type<T>(), operation, root,
                   comm);
      }
      //----

      template <class T>
      inline void allgather(const T* send_buffer, T* recv_buffer, const int count,
                            const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Allgather(send_buffer, count, mpi_type<T>(), recv_buffer, count,
                      mpi_type<T>(), comm);
      }

      template <class T, class IntArray>
      inline void allgatherv(const T* send_buffer, T* recv_buffer, const int count,
                             const IntArray& recvcounts, const IntArray& displ,
                             const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Allgatherv(send_buffer, count, mpi_type<T>(), recv_buffer,
                       recvcounts.data(), displ.data(), mpi_type<T>(), comm);
      }

      template <class T>
      inline void allreduce(const T* send_buffer, T* recv_buffer, const int count,
                            const MPI_Op operation,
                            const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Allreduce(send_buffer, recv_buffer, count, mpi_type<T>(), operation,
                      comm);
      }

      template <class T>
      inline void bcast(T* buffer, const int count, const int root,
                        const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Bcast(buffer, count, mpi_type<T>(), root, comm);
      }

      inline MPI_Status probe(const int source, const int tag,
                              const MPI_Comm comm = MPI_COMM_WORLD) {
        MPI_Status stat;
        MPI_Probe(source, tag, comm, &stat);
        return stat;
      }

      inline bool test(MPI_Request& r, MPI_Status& status) {
        int flag;
        MPI_Test(&r, &flag, &status);
        return (flag != 0);
      }

      inline void barrier(const MPI_Comm comm = MPI_COMM_WORLD) { MPI_Barrier(comm); }

      inline void wait(MPI_Request& req, MPI_Status& stat) { MPI_Wait(&req, &stat); }

      inline int waitall(const int count, MPI_Request* reqs, MPI_Status* stats) {
        return MPI_Waitall(count, reqs, stats);
      }

      inline int waitall(const int count, MPI_Request* reqs) {
        return MPI_Waitall(count, reqs, MPI_STATUSES_IGNORE);
      }

      inline int tag() { return current_tag; }

      template <class T>
      inline int get_count(MPI_Status& stat) {
        int count;
        MPI_Get_count(&stat, mpi_type<T>(), &count);
        return count;
      }

      inline int get_source(MPI_Status& stat) { return stat.MPI_SOURCE; }

      inline int get_tag(MPI_Status& stat) { return stat.MPI_TAG; }

      inline int get_provided() { return provided; }

      // Use .display() function on an object and pretty print it in the rank order
      template <class T>
      inline void ordered_display(const T& obj, std::ostream& out = std::cout,
                                  const MPI_Comm comm = MPI_COMM_WORLD) {
        const int lrank = rank(comm);
        const int lsize = size(comm);

        int token_s = 0, token_r = 0;
        if (lrank == 0) {
          out << "---Ordered display---\nRank: 0\n";
          obj.display("", out);
          if (lsize > 1) {
            send<int>(&token_s, 1, 10, 1, comm);
          }
        } else {
          recv<int>(&token_r, 1, 10, lrank - 1, comm);
          out << "Rank: " << lrank << "\n";
          obj.display("", out);
          if (lrank < (lsize - 1)) {
            send<int>(&token_s, 1, 10, lrank + 1, comm);
          }
        }
        barrier(comm);
      }

    }  // namespace MMPI
#+end_src

** =MPI_Receptor= class

The purpose of this class is to separate the main steps of MPI communications on
a set of messages.
- The non-blocking sending of the messages.
- The reception of the messages. Each call to the function returns a vector with a received message (using =MPI_Probe= and =MPI_Recv= internally).
- A function to wait for all the send to be performed

The number of messages to be received is given in input of the function
=isendMessages=, and the number of messages left to receive can be gotten by
calling the =get_n_recv= method. One can loop over this method to receive and
treat all the messages one by one. The source of the message is returned as an
output parameter.

=MPI_Request= is non-copyable, so we assume it has to be handled the
old-fashioned way with pointers. Ownership is transferred to the =MPI_Receptor=,
so a =unique_ptr= is sufficient.

#+begin_src c++
  template <class T>
  struct MPI_Receptor {
    MPI_Comm _comm;
    std::vector<MPI_Request> _reqs;
    int _n_recv;
    int _tag;

    MPI_Receptor(MPI_Comm comm) : _comm{comm} {}

    ~MPI_Receptor() { wait(); }

    MPI_Receptor(const MPI_Receptor&) = delete;  // Non copyable

    MPI_Receptor(MPI_Receptor&& other) {
      _comm = std::exchange(other._comm, MPI_COMM_NULL);
      _reqs = std::move(other._reqs);
      _n_recv = std::exchange(other._n_recv, 0);
      _tag = std::exchange(other._tag, 0);
    }

    MPI_Receptor& operator=(const MPI_Receptor&) = delete;

    MPI_Receptor& operator=(MPI_Receptor&& other) {
      if (&other == this) return *this;
      _comm = std::exchange(other._comm, MPI_COMM_NULL);
      _reqs = std::move(other._reqs);
      _n_recv = std::exchange(other._n_recv, 0);
      _tag = std::exchange(other._tag, 0);
      return *this;
    }

    std::vector<T> operator()(int& source, int& tag, bool force_tag = false,
                              bool force_source = false) {
      if (_n_recv == 0) {
        source = -1;
        return std::vector<T>();
      }

      MPI_Status stat;
      if (force_tag and force_source) {
        stat = MMPI::probe(source, tag, _comm);
      } else if (force_tag) {
        stat = MMPI::probe(MPI_ANY_SOURCE, tag, _comm);
        source = MMPI::get_source(stat);
      } else if (force_source) {
        stat = MMPI::probe(source, MPI_ANY_TAG, _comm);
        tag = MMPI::get_tag(stat);
      } else {
        stat = MMPI::probe(MPI_ANY_SOURCE, MPI_ANY_TAG, _comm);
        tag = MMPI::get_tag(stat);
        source = MMPI::get_source(stat);
      }

      int count = MMPI::get_count<T>(stat);
      std::vector<T> out(count);

      MMPI::recv<T>(&out[0], count, tag, source, _comm);

      _n_recv--;
      return out;
    }

    void probe_no_recv(int& source, int& tag) {
      MPI_Status stat;
      stat = MMPI::probe(MPI_ANY_SOURCE, MPI_ANY_TAG, _comm);
      tag = MMPI::get_tag(stat);
      source = MMPI::get_source(stat);
    }

    void wait() {
      int n_sent = static_cast<int>(_reqs.size());
      std::vector<MPI_Status> stats(n_sent);
      MMPI::waitall(n_sent, _reqs.data(), &stats[0]);
    }

    int get_n_recv() const { return _n_recv; }
  };  // class MPI_Receptor
#+end_src

#+begin_src c++
  template <class T>
  struct MPI_IReceptor {
    MPI_Comm _comm;
    std::vector<MPI_Request> _s_reqs;
    std::vector<MPI_Request> _r_reqs;
    int _n_recv;

    MPI_IReceptor(MPI_Comm comm) : _comm{comm} {}

    MPI_IReceptor(const MPI_IReceptor&) = delete;  // Non copyable

    MPI_IReceptor(MPI_IReceptor&& other) {
      _comm = std::exchange(other._comm, MPI_COMM_NULL);
      _s_reqs = std::move(other._s_reqs);
      _r_reqs = std::move(other._r_reqs);
      _n_recv = std::exchange(other._n_recv, 0);
    }

    MPI_IReceptor& operator=(const MPI_IReceptor&) = delete;

    MPI_IReceptor& operator=(MPI_IReceptor&& other) {
      if (&other == this) return *this;
      _comm = std::exchange(other._comm, MPI_COMM_NULL);
      _s_reqs = std::move(other._s_reqs);
      _r_reqs = std::move(other._r_reqs);
      _n_recv = std::exchange(other._n_recv, 0);
      return *this;
    }

    template <class IntArray>
    std::vector<T>& operator()(int& source,
                               const std::vector<IntArray>& recv_buffers) {
      if (_n_recv == 0) {
        source = -1;
        return std::vector<T>();
      }
      MPI_Status stat;
      bool no_receive = true;
      int k;
      while (no_receive) {
        k = 0;
        for (auto& r : _r_reqs) {
          if (MMPI::test(r, stat)) {
            no_receive = false;
            break;
          }
          k++;
        }
      }

      source = MMPI::get_source(stat);
      _n_recv--;
      return recv_buffers[k];
    }

    void wait() {
      MMPI::waitall(static_cast<int>(_s_reqs.size()), _s_reqs.data());
      MMPI::waitall(static_cast<int>(_r_reqs.size()), _r_reqs.data());
    }

    void wait(std::vector<MPI_Status>& s_statuses,
              std::vector<MPI_Status>& r_statuses) {
      s_statuses = std::vector<MPI_Status>(static_cast<int>(_s_reqs.size()));
      r_statuses = std::vector<MPI_Status>(static_cast<int>(_r_reqs.size()));
      MMPI::waitall(static_cast<int>(_s_reqs.size()), _s_reqs.data(),
                    s_statuses.data());
      MMPI::waitall(static_cast<int>(_r_reqs.size()), _r_reqs.data(),
                    r_statuses.data());
    }

    int get_n_recv() const { return _n_recv; }
  };  // class MPI_IReceptor
#+end_src

** isendMessages function

Now that we implemented the =MPI_Receptor= class,
we can write the =isendMessages= function that takes
in input the messages to send and return the =MPI_Receptor=
instance for the individual reception of the messages.

This function basically performs the non-blocking sending
and stores the data necessary for the reception
in the =MPI_Receptor= instance.

#+begin_src c++
  namespace MMPI {
    template <class T>
    inline MPI_Receptor<T> isendMessages(const std::vector<Message<T>>& messages,
                                         const int n_recv,
                                         const MPI_Comm comm = MPI_COMM_WORLD) {
      MPI_Receptor<T> receptor(comm);
      int n_mess = static_cast<int>(messages.size());

      receptor._reqs = std::vector<MPI_Request>(n_mess);
      std::vector<MPI_Request>& reqs = receptor._reqs;

      int i = 0;
      for (auto mess : messages) {
        MMPI::isend<T>(mess.buffer, mess.size, mess.tag, mess.destination,
                       reqs.at(i), comm);
        i++;
      }

      receptor._tag = current_tag;
      receptor._n_recv = n_recv;

      return receptor;
    }
  }  // namespace MMPI
#+end_src

** isendRecvMessages function

On the same model as =isendMessages= this function creates an =MPI_IReceptor= class.
However this time, both send and receive buffers must be provided
and the receive is non-blocking.

A user can either treat the receive buffers one by one depending on what has
been received (see =MPI_IReceptor::operator()=), or just wait for all Isend and
Irecv to be completed (see =MPI_IReceptor::wait()=).

#+begin_src c++
  namespace MMPI {
    template <class T>
    inline MPI_IReceptor<T> isendRecvMessages(
                                              const std::vector<Message<T>>& send_messages,
                                              const std::vector<Message<T>>& recv_messages,
                                              const MPI_Comm comm = MPI_COMM_WORLD) {
      MPI_IReceptor<T> receptor(comm);
      int n_mess = static_cast<int>(send_messages.size());

      receptor._s_reqs = std::vector<MPI_Request>(n_mess);
      std::vector<MPI_Request>& send_reqs = receptor._s_reqs;

      int i = 0;
      for (auto& mess : send_messages) {
        MMPI::isend<T>(mess.buffer, mess.size, mess.tag, mess.destination,
                       send_reqs.at(i), comm);
        i++;
      }

      n_mess = static_cast<int>(recv_messages.size());

      receptor._r_reqs = std::vector<MPI_Request>(n_mess);
      std::vector<MPI_Request>& recv_reqs = receptor._r_reqs;

      i = 0;
      for (auto& mess : recv_messages) {
        MMPI::irecv<T>(mess.buffer, mess.size, mess.tag, mess.destination,
                       recv_reqs.at(i), comm);
        i++;
      }

      receptor._n_recv = i;

      return receptor;
    }
  }  // namespace MMPI
#+end_src

** Data serialization

Serialize a vector of tuples as a vector of =char=.

#+begin_src c++ :results silent :tangle ../../../include/maphys/dist/MPI.hpp :comments link
  namespace MMPI {
    template <class TupleType>
    inline std::vector<char> serialize(const std::vector<TupleType>& data) {
      int total_size = sizeof(TupleType) * data.size();
      std::vector<char> serialized(total_size);
      std::memset(serialized.data(), 0, total_size);
      std::memcpy(static_cast<void*>(serialized.data()), data.data(), total_size);
      return serialized;
    }

    template <class TupleType>
    inline std::vector<TupleType> deserialize(const std::vector<char>& serialized) {
      int nb_elems = serialized.size() / sizeof(TupleType);
      std::vector<TupleType> deserialized(nb_elems);
      std::memcpy(static_cast<void*>(deserialized.data()), serialized.data(),
                  serialized.size());
      return deserialized;
    }
  }  // namespace MMPI
#+end_src

** Footer

#+begin_src c++
  }  // namespace maphys
#+end_src

* Tests

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_MPI.cpp :comments link
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include "maphys/dist/MPI.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("MMPI", "[mpi]"){

  using C = std::complex<float>;
  using Z = std::complex<double>;

  using namespace maphys;

  int rank = MMPI::rank(MPI_COMM_WORLD);
  int size = MMPI::size(MPI_COMM_WORLD);

  int    sbuf_i[10], rbuf_i[10];
  float  sbuf_s[10], rbuf_s[10];
  double sbuf_d[10], rbuf_d[10];
  C      sbuf_c[10], rbuf_c[10];
  Z      sbuf_z[10], rbuf_z[10];

  for(auto i = 0; i < 10; ++i){
    float  sval = (float)  i * rank;
    double dval = (double) i * rank;
    sbuf_i[i] = i * rank;
    sbuf_s[i] = sval;
    sbuf_d[i] = dval;
    sbuf_c[i] = {sval, -sval};
    sbuf_z[i] = {dval, -dval};
  }

  int dest = (rank + 1) % size;

  MMPI::send<int>   (sbuf_i, 10, 11, dest, MPI_COMM_WORLD);
  MMPI::send<float> (sbuf_s, 10, 22, dest, MPI_COMM_WORLD);
  MMPI::send<double>(sbuf_d, 10, 33, dest, MPI_COMM_WORLD);
  MMPI::send<C>     (sbuf_c, 10, 44, dest, MPI_COMM_WORLD);
  MMPI::send<Z>     (sbuf_z, 10, 55, dest, MPI_COMM_WORLD);

  int src = (rank + size - 1) % size;

  MMPI::recv<int>   (rbuf_i, 10, 11, src, MPI_COMM_WORLD);
  MMPI::recv<float> (rbuf_s, 10, 22, src, MPI_COMM_WORLD);
  MMPI::recv<double>(rbuf_d, 10, 33, src, MPI_COMM_WORLD);
  MMPI::recv<C>     (rbuf_c, 10, 44, src, MPI_COMM_WORLD);
  MMPI::recv<Z>     (rbuf_z, 10, 55, src, MPI_COMM_WORLD);

  SECTION("Send -  recv"){
    for(auto i = 0; i < 10; ++i){
      float  sval = (float)  i * src;
      double dval = (double) i * src;
      REQUIRE( rbuf_i[i] == (src * i) );
      REQUIRE(std::abs(rbuf_s[i] - src * i ) < 1e-6);
      REQUIRE(std::abs(rbuf_d[i] - src * i ) < 1e-14);
      REQUIRE(std::abs(rbuf_c[i] - C{sval, -sval}) < 1e-6);
      REQUIRE(std::abs(rbuf_z[i] - Z{dval, -dval}) < 1e-14);
    }
  }

  std::vector<Message<double>> mess(5);
  // On double only
  double send_buffers[5][5];
  for(auto i = 0; i < 5; ++i){
    for(auto j = 0; j < 5; ++j){
      send_buffers[i][j] = (double) (5 * 5 * dest + 5 * rank + j);
      mess[i].buffer = send_buffers[i];
      mess[i].destination = dest;
      mess[i].size = 5;
    }
  }

  auto receptor = MMPI::isendMessages(mess, 5);

  SECTION("isendMessages"){
    int source, tag;
    while( receptor.get_n_recv() ){
      std::vector<double> received = receptor(source, tag);
      REQUIRE( source == src );
      for(auto j = 0; j < 5; ++j){
        REQUIRE( received[j] == (5 * 5 * rank + 5 * source + j));
      }
    }
    receptor.wait();
  }

  int sbuf1[] = {1  * (rank + 1), 2  * (rank + 1), 3  * (rank + 1)};
  int sbuf2[] = {10 * (rank + 1), 20 * (rank + 1), 30 * (rank + 1), 40 * (rank + 1)};
  int rbuf1[] = {0, 0, 0};
  int rbuf2[] = {0, 0, 0, 0};
  std::vector<Message<int>> smess(2);
  std::vector<Message<int>> rmess(2);

  smess[0] = Message<int>(sbuf1, 3, dest, 189);
  smess[1] = Message<int>(sbuf2, 4, dest, 2332);

  rmess[0] = Message<int>(rbuf1, 3, src, 189);
  rmess[1] = Message<int>(rbuf2, 4, src, 2332);

  auto receptor_SR = MMPI::isendRecvMessages(smess, rmess);
  receptor_SR.wait();
  MMPI::barrier();

  SECTION("isendRecvMessages"){
    for(auto i = 0; i < 3; ++i){
      //std::cout << rbuf1[i] << " / " << i * (src + 1) << '\n';
      REQUIRE(rbuf1[i] == (i+1) * (src + 1));
      REQUIRE(rbuf2[i] == 10 * (i+1) * (src + 1));
    }
    REQUIRE(rbuf2[3] == 10 * 4 * (src + 1));
  }

  SECTION("Serialization"){
    using Triplet = std::tuple<int, int, double>;
    const std::vector<Triplet> tosend{{3, 3, 12.44}, {4, 2, -55.3}, {1, 9, 4627.104}};
    std::vector<char> s = MMPI::serialize<Triplet>(tosend);
    MPI_Request req;
    MMPI::isend<char>(s.data(), static_cast<int>(s.size()), 11, dest, req, MPI_COMM_WORLD);

    MPI_Status stat = MMPI::probe(src, 11, MPI_COMM_WORLD);
    int count = MMPI::get_count<char>(stat);
    std::vector<char> r(count);
    MMPI::recv<char>(r.data(), count, 11, src, MPI_COMM_WORLD);

    std::vector<Triplet> received = MMPI::deserialize<Triplet>(r);
    REQUIRE(tosend == received);
  }

} //TEST_CASE
#+end_src
