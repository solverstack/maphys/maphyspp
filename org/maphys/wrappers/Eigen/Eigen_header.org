label:wrappers:eigen_header

#+PROPERTY: header-args: c++ :results silent

* Eigen wrapper header
  :PROPERTIES:
  :header-args: c++ :tangle ../../../../include/maphys/wrappers/Eigen/Eigen_header.hpp :comments link
  :END:

** Header

#+begin_src c++
  #pragma once

  #include <Eigen/Core>
  #include <Eigen/Sparse>
  #include <maphys/interfaces/basic_concepts.hpp>

  namespace maphys {
    // Some aliasing
    template<MPH_Scalar Scalar>
    using E_Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;

    template<MPH_Scalar Scalar>
    using E_DenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

    template<MPH_Scalar Scalar>
    using E_DiagMatrix = Eigen::DiagonalMatrix<Scalar, Eigen::Dynamic>;

    template<MPH_Scalar Scalar>
    using E_SparseMatrix = Eigen::SparseMatrix<Scalar>;

    // Vector
    template<MPH_Scalar Scalar>
    [[nodiscard]] Scalar dot(const E_Vector<Scalar>&, const E_Vector<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t size(const E_Vector<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] Scalar * get_ptr(E_Vector<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t size(const E_Vector<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t n_rows(const E_Vector<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] constexpr size_t n_cols(const E_Vector<Scalar>&){ return size_t{1};}

    template<MPH_Scalar Scalar>
    [[nodiscard]] constexpr size_t get_increment(const E_Vector<Scalar>&){ return size_t{1};}

    template<MPH_Scalar Scalar>
    [[nodiscard]] constexpr size_t get_increment(E_Vector<Scalar>&){ return size_t{1};}

    template<MPH_Scalar Scalar>
    [[nodiscard]] Scalar * get_ptr(E_Vector<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] const Scalar * get_ptr(const E_Vector<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t get_leading_dim(const E_Vector<Scalar>&);

    // Diagonal matrix
    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t n_rows(const E_DiagMatrix<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t n_cols(const E_DiagMatrix<Scalar>&);

    // Dense matrix
    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t n_rows(const E_DenseMatrix<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t n_cols(const E_DenseMatrix<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] E_Vector<Scalar> diagonal_as_vector(const E_DenseMatrix<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t get_leading_dim(const E_DenseMatrix<Scalar>&);

    // Sparse matrix
    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t n_rows(const E_SparseMatrix<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] size_t n_cols(const E_SparseMatrix<Scalar>&);

    template<MPH_Scalar Scalar>
    [[nodiscard]] E_Vector<Scalar> diagonal_as_vector(const E_SparseMatrix<Scalar>&);
  } // namespace maphys
#+end_src
