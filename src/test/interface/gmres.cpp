// [[file:../../../org/maphys/solver/GMRES.org::*Interface][Interface:1]]
template<class Scalar> struct FakeVector;
namespace maphys{
  template<class Scalar> Scalar * get_ptr(FakeVector<Scalar>&);
}

#include <maphys.hpp>
#include <maphys/solver/GMRES.hpp>

// Vector type
template<class Scalar>
struct FakeVector{
  Scalar dummy;
  FakeVector operator+=(const FakeVector&){ return *this; }
  FakeVector operator-=(const FakeVector&){ return *this; }
  FakeVector operator*=(Scalar){ return *this; }
  FakeVector(){}
  FakeVector(int){}
  Scalar& operator[](int){ return dummy; }
  Scalar& operator()(int){ return dummy; }
};


template<typename T> struct maphys::scalar_type<FakeVector<T>>{ using type = T; };

template<class Scalar> Scalar dot(const FakeVector<Scalar>&, const FakeVector<Scalar>&) { return Scalar{1}; }
template<class Scalar> size_t size(const FakeVector<Scalar>&){ return 1; }
template<class Scalar> size_t n_rows(const FakeVector<Scalar>&){ return 1; }
template<class Scalar> size_t n_cols(const FakeVector<Scalar>&){ return 1; }
template<class Scalar> Scalar * maphys::get_ptr(FakeVector<Scalar>& v){ return &(v.dummy); }

template<class Scalar> FakeVector<Scalar> operator*(Scalar, FakeVector<Scalar> v){ return v; }
template<class Scalar> FakeVector<Scalar> operator*(FakeVector<Scalar> v, Scalar){ return v; }
template<class Scalar> FakeVector<Scalar> operator/(FakeVector<Scalar> v, Scalar){ return v; }
template<class Scalar> FakeVector<Scalar> operator-(const FakeVector<Scalar>&, FakeVector<Scalar> v) { return v; }
template<class Scalar> FakeVector<Scalar> operator+(const FakeVector<Scalar>&, FakeVector<Scalar> v) { return v; }

// Matrix type
template<class Scalar> struct FakeMatrix{};
template<typename Scalar> FakeVector<Scalar> operator*(const FakeMatrix<Scalar>&, FakeVector<Scalar> v){ return v; }
template<typename T> struct maphys::scalar_type<FakeMatrix<T>>{ using type = T; };
template<typename T> struct maphys::vector_type<FakeMatrix<T>>{ using type = FakeVector<T>; };

int main(){

  FakeMatrix<double> A;
  FakeVector<double> b;

  maphys::GMRES<FakeMatrix<double>, FakeVector<double>> solver;
  solver.setup(maphys::parameters::A{A},
	       maphys::parameters::verbose{true},
	       maphys::parameters::max_iter{5},
	       maphys::parameters::tolerance{1e-8});

  [[maybe_unused]] FakeVector<double> x = solver * b;

  return 0;
}
// Interface:1 ends here
