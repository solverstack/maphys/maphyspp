# Put in a function to keep a local scope
function(_maphyspp_interface_compilation)
  set( _TESTS
    cg
    gmres
    )

  foreach(_test ${_TESTS})
    add_executable(${_test} ${CMAKE_CURRENT_SOURCE_DIR}/${_test}.cpp)
    target_compile_options(${_test} PRIVATE "$<$<CONFIG:Debug>:-Wall>" "$<$<CONFIG:Debug>:-Og>")
    target_link_libraries(${_test} PRIVATE maphyspp_minimal blaspp lapackpp MPI::MPI_CXX)
    target_compile_definitions(${_test} PRIVATE MAPHYSPP_USE_LAPACKPP)
  endforeach()

endfunction()

_maphyspp_interface_compilation()
