// [[file:../../../org/maphys/precond/DiagonalPrecond.org::*Tests][Tests:1]]
#include <iostream>
#include <maphys.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>
#include <maphys/loc_data/SparseMatrixCOO.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>

using namespace maphys;
TEST_CASE("Diagonal Precond", "[DiagonalPrecond][preconditioner]"){
  using Scalar = double;
  using Real = typename arithmetic_real<Scalar>::type;

  const Real tol = arithmetic_tolerance<Scalar>::value;

  DenseMatrix<Scalar> A({1, 0, 0,
                         0, 2, 0,
                         0, 0, 3}, 3, 3);
  SparseMatrixCOO<Scalar> spA(A);

  const Vector<Scalar> res{1, 2, 3};
  SECTION("Diag pcd Dense Matrix + apply on single vector"){
    DiagonalPrecond<decltype(A), Vector<Scalar>> pcd;
    Vector<Scalar> U{1, 4, 9};
    pcd.setup(A);
    U = pcd * U;
    REQUIRE((U - res).norm() < tol);
  }

  SECTION("Diag pcd COO Matrix + apply on single vector"){
    DiagonalPrecond<decltype(spA), Vector<Scalar>> pcd;
    Vector<Scalar> U{1, 4, 9};
    pcd.setup(spA);
    U = pcd * U;
    REQUIRE((U - res).norm() < tol);
  }

  const DenseMatrix<Scalar> Mres({1, 2, 3, -2, 0, 1}, 3, 2);
  SECTION("Diag pcd Dense Matrix + apply on Dense Matrix"){
    DiagonalPrecond<decltype(A), DenseMatrix<Scalar>> pcd;
    DenseMatrix<Scalar> U({1, 4, 9, -2, 0, 3}, 3, 2);
    pcd.setup(A);
    U = pcd * U;
    REQUIRE((U - Mres).norm() < tol);
  }
}
// Tests:1 ends here
