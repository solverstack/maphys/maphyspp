// [[file:../../../org/maphys/loc_data/SparseMatrixCOO.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace maphys;

TEMPLATE_TEST_CASE("SparseMatrixCOO", "[sparse][coo_matrix]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;
  using Index = int;
  using Spmat = SparseMatrixCOO<Scalar, Index>;
  using Vect = Vector<Scalar>;
  using Dmat = DenseMatrix<Scalar>;
  using Diag = DiagonalMatrix<Scalar>;

  SECTION("Empty constructor"){
    Spmat M;
    REQUIRE( M.get_n_rows() == 0 );
    REQUIRE( M.get_n_cols() == 0 );
    REQUIRE( M.get_nnz() == 0 );
  }

  SECTION("Size constructor"){
    Spmat M(3, 4);
    REQUIRE( M.get_n_rows() == 3 );
    REQUIRE( M.get_n_cols() == 4 );
    REQUIRE( M.get_nnz() == 0 );
  }

  SECTION("Pointer constructor VS list constructor"){
    // ( 0  1  1  0)
    // ( 0  0  0  2)
    std::vector<Index> sm4_i_data{0, 0, 1};
    std::vector<Index> sm4_j_data{1, 2, 3};
    std::vector<Scalar> sm4_v_data{1.0, 1.0, 2.0};

    Index * sm4_i = sm4_i_data.data();
    Index * sm4_j = sm4_j_data.data();
    Scalar * sm4_v = sm4_v_data.data();
    Spmat M(2, 4, 3, sm4_i, sm4_j, sm4_v);

    Spmat M_check( {0, 0, 1}, {1, 2, 3}, {1.0, 1.0, 2.0}, 2, 4);
    REQUIRE(M_check == M);
  }

  const Spmat Mg( {0, 0, 1}, {1, 2, 3}, {1.0, 1.0, 2.0}, 2, 4);
  SECTION("Copy constructor"){
    Spmat M(Mg);
    REQUIRE(M == Mg);
  }

  SECTION("Move constructor"){
    Spmat Mg_bis(Mg);
    Spmat M(std::move(Mg_bis));
    REQUIRE(Mg == M);
    REQUIRE(Mg_bis.get_n_rows() == 0);
    REQUIRE(Mg_bis.get_n_cols() == 0);
    REQUIRE(Mg_bis.get_nnz() == 0);
  }

  /*#####################
    ## Test assignment ##
    #####################*/

  SECTION("Copy assignment"){
    Spmat M;
    M = Mg;
    REQUIRE(M == Mg);
  }

  SECTION("Move assignment"){
    Spmat Mg_bis(Mg);
    Spmat M;
    M = std::move(Mg_bis);
    REQUIRE(M == Mg);
  }

  /*#######################
    ## Matrix arithmetic ##
    #######################*/

  SECTION("Scalar multiplication"){
    Spmat M = Mg;
    Spmat Mover2( {0, 0, 1}, {1, 2, 3}, {0.5, 0.5, 1.0}, 2, 4);

    REQUIRE( (M * Scalar{0.5}) == Mover2);
    REQUIRE( (Scalar{0.5} * M) == Mover2);
    REQUIRE( (M / Scalar{2.0}) == Mover2);
  }

  const Spmat Mg2({0, 1, 0, 0, 1}, {0, 1, 2, 3, 3}, {1.0, 2.0, -3.0, 4.0, 4.0});
  const Vect Vg{1.0, 2.0, -1.0, 1.0};
  SECTION("Matrix vector multiplication (full storage)"){

    //                 1
    //                 2
    // ( 1 0 -3 4 ) x -1 = 8
    // ( 0 2  0 4 )    1   8

    Vect Mg2timesVg = Mg2 * Vg;
    Vect Vg_check{8.0, 8.0};
  }

  SECTION("Matrix vector multiplication (half storage)"){

    // ( 1 3 0 0 )    1    7
    // ( . 2 0 2 )    2    9
    // ( . . 1 0 ) x -1 = -1
    // ( . . . 2 )    1    6

    Spmat SM11({0, 0, 1, 1, 2, 3}, {0, 1, 1, 3, 2, 3}, {1.0, 3.0, 2.0, 2.0, 1.0, 2.0});
    SM11.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    Vect SM11timesVg = SM11 * Vg;
    Vect Vg_check2{7.0, 9.0, -1.0, 6.0};
    REQUIRE( SM11timesVg == Vg_check2 );
  }

  SECTION("Matrix x diagonal matrix multiplication (half storage)"){

    // ( 1 3 0 0 )    ( 1.5  .    .     . )   ( 1.5 1.5  0    0  )
    // ( . 2 0 2 )    ( .    0.5  .     . )   ( 4.5  1   0    4  )
    // ( . . 1 0 ) x -( .    .    0.25  . ) = (  0   0  0.25  0  )
    // ( . . . 2 )    ( .    .    .     2 )   (  0   1   0    4  )

    Spmat SM11({0, 0, 1, 1, 2, 3}, {0, 1, 1, 3, 2, 3}, {1.0, 3.0, 2.0, 2.0, 1.0, 2.0});
    SM11.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    Diag D({1.5, 0.5, 0.25, 2});
    SM11 = SM11 * D;
    const Spmat SM11D_check({0, 0, 1, 1, 1, 2, 3, 3}, {0, 1, 0, 1, 3, 2, 1, 3}, {1.5, 1.5, 4.5, 1, 4, 0.25, 1, 4});
    SM11.to_csc().display("SM11");
    SM11D_check.to_csc().display("SM11D_check");
    REQUIRE( SM11 == SM11D_check );
  }

  SECTION("Sparse matrix times dense matrix"){
    //                 ( 1  2)
    //                 ( 2  3)
    // ( 1 0 -3 4 ) x  (-2 -4) = (11 33)
    // ( 0 2  0 4 )    ( 1  5)   (8  26)

    Dmat D1({1.0, 2.0, -2.0, 1.0, 2.0, 3.0, -4.0, 5.0}, 4, 2);
    Dmat Mg2timesD1 = Mg2 * D1;
    Dmat D1_check({11.0, 8.0, 34.0 ,26.0}, 2, 2);

    REQUIRE( Mg2timesD1 == D1_check );
  }

  SECTION("CSC conversion"){
    Spmat coo1 = Mg;
    SparseMatrixCSC<Scalar> csc1 = coo1.to_csc();
    Spmat coo2;
    coo2.from_csc(csc1);

    REQUIRE(coo1 == coo2);
  }

  SECTION("Diagonal extraction"){
    //          ( 1 4 7 )    1
    // diagonal ( 2 5 8 )  = 5
    //          ( 3 6 9 )    9

    Spmat M({0, 0, 0, 1, 1, 1, 2, 2, 2}, {0, 1, 2, 0, 1, 2, 0, 1, 2}, {1, 4, 7, 2, 5, 8, 3, 6, 9});
    Vect d = diagonal_as_vector(M);
    Vect d_expected{1, 5, 9};

    REQUIRE(d == d_expected);
  }

  SECTION("Accessor"){
    // ( 0  1  1  0)
    // ( 0  0  0  2)
    REQUIRE(Mg.coeff(0, 0) == Scalar{0});
    REQUIRE(Mg.coeff(0, 1) == Scalar{1});
    REQUIRE(Mg.coeff(0, 2) == Scalar{1});
    REQUIRE(Mg.coeff(0, 3) == Scalar{0});
    REQUIRE(Mg.coeff(1, 0) == Scalar{0});
    REQUIRE(Mg.coeff(1, 1) == Scalar{0});
    REQUIRE(Mg.coeff(1, 2) == Scalar{0});
    REQUIRE(Mg.coeff(1, 3) == Scalar{2});
  }

  SECTION("Adjoint"){
    // ( 0  1  1  0)    ( 0  0 )
    // ( 0  0  0  2) -> ( 1  0 )
    //                  ( 1  0 )
    //                  ( 0  2 )
    Spmat orig = Mg;
    const Spmat exp({1, 2, 3}, {0, 0, 1}, {1, 1, 2}, 4, 2);
    Spmat adj = adjoint(orig);
    REQUIRE(adj == exp);
  }

  SECTION("Test with integer 64: COO x Dense mat"){
    const SparseMatrixCOO<Scalar, long int> M({0, 1, 0, 0, 1}, {0, 1, 2, 3, 3}, {1.0, 2.0, -3.0, 4.0, 4.0});
    Dmat D1({1.0, 2.0, -2.0, 1.0, 2.0, 3.0, -4.0, 5.0}, 4, 2);
    Dmat Mg2timesD1 = M * D1;
    Dmat D1_check({11.0, 8.0, 34.0 ,26.0}, 2, 2);

    REQUIRE( Mg2timesD1 == D1_check );
  }

  const Spmat second_term({0, 1, 1, 1}, {2, 0, 2, 3}, {1, -1, 2, -2}, 2, 4);
  SECTION("Addition spm + spm"){
    // ( 0  1  1  0)    ( 0  0  1  0 )   ( 0  1  2  0 )
    // ( 0  0  0  2) +  (-1  0  2 -2 ) = (-1  0  2  0 )
    const Spmat orig = Mg;
    const Spmat expected({0, 0, 1, 1}, {1, 2, 0, 2}, {1, 2, -1, 2}, 2, 4);

    Spmat found = orig + second_term;
    REQUIRE(found == expected);
  }

  SECTION("Substraction spm - spm"){
    // ( 0  1  1  0)    ( 0  0  1  0 )   ( 0  1  0  0 )
    // ( 0  0  0  2) -  (-1  0  2 -2 ) = ( 1  0 -2  4 )
    const Spmat orig = Mg;
    const Spmat expected({0, 1, 1, 1}, {1, 0, 2, 3}, {1, 1, -2, 4}, 2, 4);

    Spmat found = orig - second_term;
    REQUIRE(found == expected);
  }

  SECTION("Multiplication spm x spm"){
    // ( 1 0 2 ) ( 1  -2 0 )     (-9  10  14 )
    // ( 0 3 0 ) ( 3  -4 0 )  =  ( 9 -12   0 )
    // ( 4 5 0 ) (-5   6 7 )     (19 -28   0 )

    const Spmat A({0, 0, 1, 2, 2}, {0, 2, 1, 0, 1}, {1, 2, 3, 4, 5}, 3, 3);
    const Spmat B({0, 0, 1, 1, 2, 2, 2}, {0, 1, 0, 1, 0, 1, 2}, {1, -2, 3, -4, -5, 6, 7}, 3, 3);
    const Spmat expected({0, 0, 0, 1, 1, 2, 2}, {0, 1, 2, 0, 1, 0, 1}, {-9, 10, 14, 9, -12, 19, -28}, 3, 3);

    Spmat C = A * B;
    REQUIRE(C == expected);
  }

  SECTION("Multiplication spm (half storage) x spm"){
    // ( 1 2 3 ) ( 1  -2 0 )     (-8   8  21 )
    // ( - 0 0 ) ( 3  -4 0 )  =  ( 2  -4   0 )
    // ( - - 4 ) (-5   6 7 )     (-17 18 28 )

    Spmat A({0, 0, 0, 2}, {0, 1, 2, 2}, {1, 2, 3, 4}, 3, 3);
    A.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    const Spmat B({0, 0, 1, 1, 2, 2, 2}, {0, 1, 0, 1, 0, 1, 2}, {1, -2, 3, -4, -5, 6, 7}, 3, 3);
    const Spmat expected({0, 0, 0, 1, 1, 2, 2, 2}, {0, 1, 2, 0, 1, 0, 1, 2}, {-8, 8, 21, 2, -4, -17, 18, 28}, 3, 3);

    Spmat C_u = A * B;
    REQUIRE(C_u == expected);

    A.to_triangular(MatrixStorage::lower);
    Spmat C_l = A * B;
    REQUIRE(C_l == expected);
  }

  SECTION("Multiplication spm with both half storage"){
    // ( 1 2 3 ) ( 1   -  - )     ( 7  13  12 )
    // ( - 0 0 ) ( 3  -4  - )  =  ( 2   6   0 )
    // ( - - 4 ) ( 0   6  0 )     ( 3  33   0 )
    Spmat A({0, 0, 0, 2}, {0, 1, 2, 2}, {1, 2, 3, 4}, 3, 3);
    A.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    Spmat B({0, 1, 1, 2}, {0, 0, 1, 1}, {1, 3, -4, 6}, 3, 3);
    B.set_property(MatrixSymmetry::symmetric, MatrixStorage::lower);
    const Spmat expected({0, 0, 0, 1, 1, 2, 2}, {0, 1, 2, 0, 1, 0, 1}, {7, 13, 12, 2, 6, 3, 33}, 3, 3);

    // upper x lower
    //Spmat C_ul = A * B;
    //REQUIRE(C_ul == expected);

    // upper x upper
    B.to_triangular(MatrixStorage::upper);
    Spmat C_uu = A * B;
    REQUIRE(C_uu == expected);

    // lower x upper
    A.to_triangular(MatrixStorage::lower);
    Spmat C_lu = A * B;
    REQUIRE(C_lu == expected);

    // lower x lower
    B.to_triangular(MatrixStorage::lower);
    Spmat C_ll = A * B;
    REQUIRE(C_ll == expected);
  }

  SECTION("Associativity test multiplication spm x spm x spm"){
    // ( 1 0 2 ) ( 1  -2 0 ) (-1 0 0 )   (-9   20  34 )
    // ( 0 3 0 ) ( 3  -4 0 ) ( 0 2 2 ) = (-9  -24 -24 )
    // ( 4 5 0 ) (-5   6 7 ) ( 0 0 1 )   (-19 -56 -56 )

    const Spmat A({0, 0, 1, 2, 2}, {0, 2, 1, 0, 1}, {1, 2, 3, 4, 5}, 3, 3);
    const Spmat B({0, 0, 1, 1, 2, 2, 2}, {0, 1, 0, 1, 0, 1, 2}, {1, -2, 3, -4, -5, 6, 7}, 3, 3);
    const Spmat C({0, 1, 1, 2}, {0, 1, 2, 2}, {-1, 2, 2, 1}, 3, 3);
    const Spmat expected({0, 0, 0, 1, 1, 1, 2, 2, 2}, {0, 1, 2, 0, 1, 2, 0, 1, 2}, {9, 20, 34, -9, -24, -24, -19, -56, -56}, 3, 3);

    Spmat D1 = A * B * C;
    Spmat D2 = (A * B) * C;
    Spmat D3 = A * (B * C);
    REQUIRE(D1 == D2);
    REQUIRE(D2 == D3);
    REQUIRE(D1 == expected);
  }

  SECTION("Multiplication rectangular spm x spm"){
    // ( 1 0 ) ( 1 0 0 )     ( 1 0 0 )
    // ( 0 2 ) ( 2 0 3 )  =  ( 4 0 6 )
    // ( 3 5 )               ( 3 0 0 )
    // ( 0 0 )               ( 0 0 0 )

    const Spmat A({0, 1, 2}, {0, 1, 0}, {1, 2, 3}, 4, 2);
    const Spmat B({0, 1, 1}, {0, 0, 2}, {1, 2, 3}, 2, 3);
    const Spmat expected({0, 1, 1, 2}, {0, 0, 2, 0}, {1, 4, 6, 3}, 4, 3);

    Spmat C = A * B;
    REQUIRE(C == expected);
  }

  SECTION("Triangular to full storage (and reverse) testing"){
    //TODO
  }

  SECTION("Reindexing loc2glob"){
    Spmat A({0, 0, 1, 2, 2}, {0, 2, 1, 0, 1}, {1, 2, 3, 4, 5}, 3, 3);
    const Spmat A_reind({10, 10, 20, 30, 30}, {10, 30, 20, 10, 20}, {1, 2, 3, 4, 5}, 31, 31);
    IndexArray<int> indices{10, 20, 30};
    A.reindex(indices, 31);
    REQUIRE(A == A_reind);
  }

  SECTION("Reindexing glob2loc"){
    Spmat A({10, 10, 20, 30, 30}, {10, 30, 20, 10, 20}, {1, 2, 3, 4, 5}, 31, 31);
    const Spmat A_reind({5, 5, 10, 15, 15}, {5, 15, 10, 5, 10}, {1, 2, 3, 4, 5}, 16, 16);
    IndexArray<int> old_indices{10, 20, 30};
    IndexArray<int> new_indices{5, 10, 15};
    A.reindex(new_indices, old_indices, 16);
    REQUIRE(A == A_reind);
  }

  SECTION("Iterator"){
    const Spmat M = Mg;
    //{0, 0, 1}, {1, 2, 3}, {1.0, 1.0, 2.0}
    std::vector<std::tuple<Index, Index, Scalar>> check{{0, 1, 1.0}, {0, 2, 1.0}, {1, 3, 2.0}};
    IJVIterator<Scalar, Index, SparseMatrixCOO<Scalar, Index>> it(M);
    int k = 0;
    for(auto ijv : it){
      REQUIRE(ijv == check[k++]);
    }
    REQUIRE(k == static_cast<int>(M.get_nnz()));
  }

  SECTION("Serialization / deserialization"){
    const Spmat M = Mg;
    std::vector<char> buf = M.serialize();
    Spmat O;
    O.deserialize(buf);
    REQUIRE(O == M);
  }

} // TEMPLATE_TEST_CASE
// Tests:1 ends here
