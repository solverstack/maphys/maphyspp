// [[file:../../../org/maphys/graph/Paddle.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <complex>

#include <maphys.hpp>
#include <maphys/graph/Paddle.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/solver/GCR.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/solver/PartSchurSolver.hpp>

using namespace maphys;

template<typename Scalar>
void test_paddle(const SparseMatrixCOO<Scalar, int>& a_in, const Vector<Scalar> x_exp){
  using Real = typename arithmetic_real<Scalar>::type;
  int rank = MMPI::rank();

  //a_in.display("A IN");
  //x_exp.display("X exp");

  Paddle<Scalar> paddle;
  paddle.set_verbose();

  const Vector<Scalar> b_in = a_in * x_exp;

  auto [p_A, p_rhs] = paddle.centralized_input(a_in, b_in);

  ConjugateGradient<PartMatrix<SparseMatrixCOO<Scalar, int>>, PartVector<Vector<Scalar>>> solver(p_A);
  solver.setup(parameters::max_iter{50}, parameters::tolerance{Real{1e-5}}, parameters::verbose{(rank == 0)});
  auto p_x = solver * p_rhs;

  auto ctr_x = paddle.solution_redistribution(p_x);

  MMPI::barrier();
  if(rank == 0){
    REQUIRE((x_exp - ctr_x).norm() < Real{1e-4});
  }
  else{
    REQUIRE(ctr_x.get_n_rows() == 0);
  }
}

TEMPLATE_TEST_CASE("Paddle", "[graph]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;

  int rank = MMPI::rank();
  const int N = 40;

  SECTION("Test - full storage"){
    SparseMatrixCOO<Scalar, int> a_coo;
    Vector<Scalar> x_exp;
    if(rank == 0){
      SparseMatrixLIL<Scalar, int> a_lil(N, N);
      for(int k = 0; k < N - 1; ++k){
	a_lil.insert(k, k,  2.0);
	a_lil.insert(k + 1, k, -1.0);
	a_lil.insert(k, k + 1, -1.0);
      }
      a_lil.insert(N - 1, N - 1, 2.0);
      a_lil.convert(a_coo);
      a_coo.set_spd(MatrixStorage::full);

      x_exp = Vector<Scalar>(N);
      for(int k = 0; k < N; ++k){ x_exp[k] = k; }
    }
    test_paddle<Scalar>(a_coo, x_exp);
  }

  SECTION("Test - half storage"){
    SparseMatrixCOO<Scalar, int> a_coo;
    Vector<Scalar> x_exp;
    if(rank == 0){
      SparseMatrixLIL<Scalar, int> a_lil(N, N);
      for(int k = 0; k < N - 1; ++k){
	a_lil.insert(k, k,  2.0);
	a_lil.insert(k + 1, k, -1.0);
      }
      a_lil.insert(N - 1, N - 1, 2.0);
      a_lil.convert(a_coo);
      a_coo.set_spd(MatrixStorage::lower);

      x_exp = Vector<Scalar>(N);
      for(int k = 0; k < N; ++k){ x_exp[k] = k; }
    }
    test_paddle<Scalar>(a_coo, x_exp);
  }
}

TEST_CASE("Paddle - bigger matrix", "[graph]"){
  using Scalar = double;
  using Real = double;

  int rank = MMPI::rank();
  SparseMatrixCOO<Scalar, int> a_in;
  Vector<Scalar> x_exp;

  int N;
  if(rank == 0){
    const std::string path_to_matrix = "@MAPHYSPP_MATRICES_DIR@/bcsstk17.mtx";
    a_in.from_matrix_market_file(path_to_matrix);
    a_in.set_property(MatrixPositivity::positive, MatrixDef::definite);

    N = static_cast<int>(a_in.get_n_rows());
    x_exp = Vector<Scalar>(N);
    for(int k = 0; k < N; ++k){ x_exp[k] = Scalar{1}; }
  }
  MMPI::bcast(&N, 1, 0);

  const Vector<Scalar> b_in = a_in * x_exp;

  Paddle<Scalar> paddle;
  paddle.set_verbose();

  auto [p_A, p_rhs] = paddle.centralized_input(a_in, b_in);

  using DenseSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
  using Solver_K = Pastix<SparseMatrixCOO<Scalar, int>, Vector<Scalar>>;
  using Precond = AdditiveSchwarz<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>, DenseSolver>;
  using Solver_S = ConjugateGradient<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>, Precond>;
  PartSchurSolver<PartMatrix<SparseMatrixCOO<Scalar, int>>, PartVector<Vector<Scalar>>, Solver_K, Solver_S> solver;
  solver.setup(parameters::A{p_A});

  auto& CG = solver.get_solver_S();
  CG.setup(parameters::max_iter{N}, parameters::tolerance{Real{1e-8}}, parameters::verbose{(rank == 0)});

  auto p_x = solver * p_rhs;

  auto ctr_x = paddle.solution_redistribution(p_x);

  auto n_iter = CG.get_n_iter();
  MMPI::barrier();

  REQUIRE(n_iter > 0);
  if(rank != 0){
    REQUIRE(ctr_x.get_n_rows() == 0);
  }
}

// A =
// array([[10.,  0., -4., -1.,  0.,  0.,  0.,  0.],
//        [ 0., 11.,  0.,  0., -1.,  0., -3.,  0.],
//        [ 0.,  0., 12.,  0.,  0., -4., -4.,  0.],
//        [ 0.,  0.,  0., 13.,  0.,  0., -2.,  0.],
//        [ 0.,  0.,  0.,  0., 14.,  0.,  0.,  0.],
//        [ 0., -2.,  0.,  0.,  0., 15.,  0.,  0.],
//        [ 0.,  0.,  0.,  0.,  0.,  0., 16.,  0.],
//        [ 0.,  0.,  0.,  0.,  0.,  0., -2., 17.]])
// X = [1. 2. 3. 4. 5. 6. 7. 8.]^T
// B = [ -6.,  -4., -16.,  38.,  70.,  86., 112., 122.]^T
//
// Distributed by rows (4 procs, 2 rows per proc)

TEST_CASE("Paddle (dist input)", "[graph]"){
  using Scalar = double;
  using Real = double;

  const int rank = MMPI::rank();
  const int size = MMPI::size();
  REQUIRE(size == 4);

  const size_t n_glob = 8;
  SparseMatrixCOO<Scalar> K;
  IndexArray<int> rhs_i;
  IndexArray<Scalar> rhs_v;
  Vector<Scalar> expected_X;

  if(rank == 0){
    //0 -> [10.,  0., -4., -1.,  0.,  0.,  0.,  0.]
    //1 -> [ 0., 11.,  0.,  0., -1.,  0., -3.,  0.]
    K = SparseMatrixCOO<Scalar>({ 0, 0, 0, 1, 1, 1},
				{ 0, 2, 3, 1, 4, 6},
				{10,-4,-1,11,-1,-3}, n_glob, n_glob);
    rhs_i = IndexArray<int>{0, 1};
    rhs_v = IndexArray<Scalar>{-6, -4};
    expected_X = Vector<Scalar>{1, 2};
  } else if(rank == 1){
    //2 -> [ 0.,  0., 12.,  0.,  0., -4., -4.,  0.]
    //3 -> [ 0.,  0.,  0., 13.,  0.,  0., -2.,  0.]
    K = SparseMatrixCOO<Scalar>({ 2, 2, 2, 3, 3},
				{ 2, 5, 6, 3, 6},
				{12,-4,-4,13,-2}, n_glob, n_glob);
    rhs_i = IndexArray<int>{2, 3};
    rhs_v = IndexArray<Scalar>{-16, 38};
    expected_X = Vector<Scalar>{3, 4};
  } else if(rank == 2){
    //4 -> [ 0.,  0.,  0.,  0., 14.,  0.,  0.,  0.]
    //5 -> [ 0., -2.,  0.,  0.,  0., 15.,  0.,  0.]
    K = SparseMatrixCOO<Scalar>({ 4, 5, 5},
				{ 4, 1, 5},
				{14,-2,15}, n_glob, n_glob);
    rhs_i = IndexArray<int>{4, 5};
    rhs_v = IndexArray<Scalar>{70, 86};
    expected_X = Vector<Scalar>{5, 6};
  } else if(rank == 3){
    //6 -> [ 0.,  0.,  0.,  0.,  0.,  0., 16.,  0.],
    //7 -> [ 0.,  0.,  0.,  0.,  0.,  0., -2., 17.]
    K = SparseMatrixCOO<Scalar>({ 6, 7, 7},
				{ 6, 6 ,7},
				{16,-2,17}, n_glob, n_glob);
    rhs_i = IndexArray<int>{6, 7};
    rhs_v = IndexArray<Scalar>{112, 122};
    expected_X = Vector<Scalar>{7, 8};
  }

  Paddle<Scalar> paddle;
  paddle.set_verbose();

  auto [p_A, p_rhs] = paddle.distributed_input(K, rhs_i, rhs_v);
  GCR<PartMatrix<SparseMatrixCOO<Scalar, int>>, PartVector<Vector<Scalar>>> solver(p_A);
  solver.setup(parameters::max_iter{50}, parameters::tolerance{Real{1e-9}}, parameters::verbose{(rank == 0)});
  auto p_x = solver * p_rhs;
  REQUIRE(solver.get_n_iter() != -1);

  auto dist_x = paddle.solution_redistribution(p_x);

  REQUIRE((dist_x - expected_X).norm() < 1e-12);

  // Now let's make a second solve with:
  // X = [-10. -8. -6. 0. 0. 1. 2. 3.]^T
  // B = [-76., -94., -84.,  -4.,   0.,  31.,  32.,  47.]^T

  IndexArray<Scalar> rhs2_v;
  Vector<Scalar> expected_X2;
  if(rank == 0){
    rhs2_v = IndexArray<Scalar>{-76, -94};
    expected_X2 = Vector<Scalar>{-10,-8};
  } else if(rank == 1){
    rhs2_v = IndexArray<Scalar>{-84, -4};
    expected_X2 = Vector<Scalar>{-6,0};
  } else if(rank == 2){
    rhs2_v = IndexArray<Scalar>{0, 31};
    expected_X2 = Vector<Scalar>{0, 1};
  } else if(rank == 3){
    rhs2_v = IndexArray<Scalar>{32, 47};
    expected_X2 = Vector<Scalar>{2, 3};
  }

  auto p_rhs2 = paddle.setup_next_rhs(rhs2_v);
  auto p_x2 = solver * p_rhs2;
  auto dist_x2 = paddle.solution_redistribution(p_x2);

  //dist_x2.display_centralized("p_x2");
  REQUIRE((dist_x2 - expected_X2).norm() < 1e-12);
}
// Tests:1 ends here
