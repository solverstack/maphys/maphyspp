// [[file:../../../org/maphys/dist/Process.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include "maphys/dist/MPI.hpp"
#include "maphys/dist/Process.hpp"
#include "maphys/testing/TestMatrix.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Process", "[process][distributed]"){
  using namespace maphys;

  SECTION("Process consistency tests"){
    const std::string path_to_part = "@MAPHYSPP_MATRIX_PARTITIONS_8@";
    std::shared_ptr<Process> p = bind_subdomains(8);
    p->load_subdomains(path_to_part);
    //p->display();
    REQUIRE(p->check_subdomains_part_of_unity());
  }

  SECTION("Process subdomain centralize on all procs"){
    std::shared_ptr<Process> p = test_matrix::get_distr_process();

    std::map<int, IndexArray<int>> m;
    if(p->owns_subdomain(0)) m.emplace(0, std::vector<int>{0, 1, 2, 3});
    if(p->owns_subdomain(1)) m.emplace(1, std::vector<int>{0, 0, 4, 5});
    if(p->owns_subdomain(2)) m.emplace(2, std::vector<int>{0, 6, 0, 0});
    const std::vector<int> v_centr = p->subdomain_centralize(m);
    if(p->is_master()){
      const std::vector<int> v_centr_exp{0, 1, 2, 3, 4, 5, 6};
      REQUIRE(v_centr == v_centr_exp);
    }
    else{
      REQUIRE(v_centr == std::vector<int>());
    }
  }

  SECTION("Process subdomain centralize on 1 process"){
    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    int root = 0;
    if(p->is_master()){
      const int master_comm_size = MMPI::size(p->master_comm());
      root = std::min(master_comm_size-1, 1); // Process 1 of master comm if possible
    }
    std::map<int, IndexArray<int>> m;
    if(p->owns_subdomain(0)) m.emplace(0, std::vector<int>{0, 1, 2, 3});
    if(p->owns_subdomain(1)) m.emplace(1, std::vector<int>{0, 0, 4, 5});
    if(p->owns_subdomain(2)) m.emplace(2, std::vector<int>{0, 6, 0, 0});
    const std::vector<int> v_centr = p->subdomain_centralize(m, root, false);
    if(p->is_master() && MMPI::rank(p->master_comm()) == root){
      const std::vector<int> v_centr_exp{0, 1, 2, 3, 4, 5, 6};
      REQUIRE(v_centr == v_centr_exp);
    }
    else{
      REQUIRE(v_centr == std::vector<int>());
    }
  }

  SECTION("Process subdomain centralize on 1 process on interface"){
    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    int root = 0;
    if(p->is_master()){
      const int master_comm_size = MMPI::size(p->master_comm());
      root = std::min(master_comm_size-1, 1); // Process 1 of master comm if possible
    }
    std::map<int, IndexArray<int>> m;
    if(p->owns_subdomain(0)) m.emplace(0, std::vector<int>{0, 1, 2});
    if(p->owns_subdomain(1)) m.emplace(1, std::vector<int>{0, 0, 3});
    if(p->owns_subdomain(2)) m.emplace(2, std::vector<int>{0, 0, 0});
    const std::vector<int> v_centr = p->subdomain_centralize(m, root, true);
    if(p->is_master() && MMPI::rank(p->master_comm()) == root){
      const std::vector<int> v_centr_exp{0, 1, 2, 3};
      REQUIRE(v_centr == v_centr_exp);
    }
    else{
      REQUIRE(v_centr == std::vector<int>());
    }
  }
}
// Tests:1 ends here
