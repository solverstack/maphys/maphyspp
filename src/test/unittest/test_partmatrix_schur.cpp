// [[file:../../../org/maphys/part_data/PartMatrix.org::*Schur test][Schur test:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include "maphys/part_data/PartMatrix.hpp"
#include "maphys/part_data/PartOperator.hpp"
#include "maphys/solver/Pastix.hpp"
#include "maphys/testing/TestMatrix.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("partmatrix_schur", "[partmatrix][distributed][schur]"){

  using namespace maphys;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();
  PartMatrix<SparseMatrixCOO<double>> p_m = test_matrix::dist_spd_matrix<double,SparseMatrixCOO<double>>(p).matrix;
  p_m.apply_on_data([](SparseMatrixCOO<double>& m){ m.to_triangular(); });

  using DirSolver = Pastix<SparseMatrixCOO<double>, Vector<double>>;

  PartOperator<DirSolver,SparseMatrixCOO<double>,Vector<double>> dir_solver;
  dir_solver.initialize(p);
  dir_solver.setup(p_m);

  PartMatrix<DenseMatrix<double>> schur(p, /*on_interface*/ true);

  auto fct_schur = [&](const int sd_id, DenseMatrix<double>& Sloc, DirSolver& solver){
                     IndexArray<int> intrf = p->get_interface(sd_id);
                     Sloc = solver.get_schur(intrf);
                   };
  schur.template apply_on_data_id<DirSolver>(dir_solver, fct_schur);

  schur.display_local("Schur");

  SECTION("Test schur local values"){
    if(p->owns_subdomain(0)){
      DenseMatrix<double> m({2.75, -0.25, -1.25, -0.25, 2.75, -1.25, -1.25, -1.25, 3.75}, 3, 3);
      DenseMatrix<double> s_loc = schur.get_local_matrix(0);
      REQUIRE(m == s_loc);
    }
    if(p->owns_subdomain(1)){
      DenseMatrix<double> m({3., -1., -1., -1., 11./3, -4./3, -1., -4./3, 11./3}, 3, 3);
      DenseMatrix<double> s_loc = schur.get_local_matrix(1);
      REQUIRE((m - s_loc).norm() < 1e-10);
    }
    if(p->owns_subdomain(2)){
      DenseMatrix<double> m({2.75, -0.25, -1.25, -0.25, 2.75, -1.25, -1.25, -1.25, 3.75}, 3, 3);
      DenseMatrix<double> s_loc = schur.get_local_matrix(2);
      double diff_norm = (m - s_loc).norm();
      REQUIRE(diff_norm <= 1e-10);
    }
  }
} // TEST_CASE
// Schur test:1 ends here
