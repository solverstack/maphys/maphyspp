// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Header][Header:1]]
#include <iostream>
#include <vector>
#include <complex>

#include <maphys.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
//#include <catch2/matchers/catch_matchers_range_equals.hpp>
#include <maphys/testing/Catch2DenseMatrixMatchers.hpp>

/*--------------------*/
// Real MM tests
// With symmetric matrix
//  1 10  x 1 3 = 21 43
// 10 -2    2 4    6 22
//----------------------
// 1 3  x  1 10 = 31  4
// 2 4    10  1   42 12
/*--------------------*/
template<class Scalar>
typename std::enable_if<maphys::is_real<Scalar>::value>::type
sym_or_herm_mat(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>( {1., 10., 10., -2.}, 2, 2);
  M.set_property(maphys::MatrixSymmetry::symmetric);
}

template<class Scalar>
typename std::enable_if<maphys::is_real<Scalar>::value>::type
sym_or_herm_sol(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>({21., 6., 43., 22.}, 2, 2);
}

template<class Scalar>
typename std::enable_if<maphys::is_real<Scalar>::value>::type
sym_or_herm_sol2(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>({31., 42., 4., 12.}, 2, 2);
}

/*----------------*/
// Adjoint testing
//           1 2
// 1 3 5 * = 3 4
// 2 4 6     5 6
/*----------------*/

template<class Scalar>
typename std::enable_if<maphys::is_real<Scalar>::value>::type
testmat_adjoint(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>({1, 2, 3, 4, 5, 6}, 2, 3);
}
template<class Scalar>
typename std::enable_if<maphys::is_real<Scalar>::value>::type
testmat_adjoint_exp(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>({1, 3, 5, 2, 4, 6}, 3, 2);
}

/*----------------------------------------------------*/
// Complex MM tests
// With hermitian matrix
//(1.     ) (10.-3.i)    1. 3.   (21. -6.i)  (43.-12.i)
//(10.+3.i) (2.     ) x  2. 4. = (14. +3.i)  (38. +9.i)
//-----------------------------------------------------
// 1. 3.   ( 1.    ) (10.-3.i)   (31. +9.i)  (16. -3.i)
// 2. 4. x (10.+3.i) ( 2.    ) = (42.+12.i)  (28. -6.i)
/*----------------------------------------------------*/
template<class Scalar>
typename std::enable_if<maphys::is_complex<Scalar>::value>::type
sym_or_herm_mat(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>( {{1., 0.}, {10., 3.}, {10., -3}, {2., 0.}}, 2, 2);
  M.set_property(maphys::MatrixSymmetry::hermitian);
}

template<class Scalar>
typename std::enable_if<maphys::is_complex<Scalar>::value>::type
sym_or_herm_sol(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>( {{21., -6.}, {14., 3.}, {43., -12.}, {38., 9.}}, 2, 2);
}

template<class Scalar>
typename std::enable_if<maphys::is_complex<Scalar>::value>::type
sym_or_herm_sol2(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>( {{31., 9.}, {42., 12.}, {16., -3.}, {28., -6.}}, 2, 2);
}

/*----------------*/
// Adjoint testing
//                  1-i  2-i
// 1+i 3-i  5   * = 3+i  4+2i
// 2+i 4-2i 6+i     5    6-i
/*----------------*/

template<class Scalar>
typename std::enable_if<maphys::is_complex<Scalar>::value>::type
testmat_adjoint(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>({{1, 1}, {2, 1}, {3, -1}, {4, -2}, {5, 0}, {6, 1}}, 2, 3);
}
template<class Scalar>
typename std::enable_if<maphys::is_complex<Scalar>::value>::type
testmat_adjoint_exp(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>({{1, -1}, {3, 1}, {5, 0}, {2, -1}, {4, 2}, {6, -1}}, 3, 2);
}

//--------------------
// Real MV tests
// With symmetric matrix
//  1 10  x 1 = 31
// 10 -2    3    4
//--------------------
template<class Scalar>
typename std::enable_if<maphys::is_real<Scalar>::value>::type
mv_sym_or_herm_mat(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>( {1., 10., 0, -2.}, 2, 2);
  M.set_property(maphys::MatrixSymmetry::symmetric, maphys::MatrixStorage::lower);
}

template<class Scalar>
typename std::enable_if<maphys::is_real<Scalar>::value>::type
mv_sym_or_herm_sol(maphys::Vector<Scalar>& M){
  M = maphys::Vector<Scalar>({31., 4.});
}

//----------------------------------------------------
// Complex MV tests
// With hermitian matrix
//(1.     ) (10.-3.i)    1.   (31. -9.i)
//(10.+3.i) (2.     ) x  3. = (16. +3.i)
//----------------------------------------------------
template<class Scalar>
typename std::enable_if<maphys::is_complex<Scalar>::value>::type
mv_sym_or_herm_mat(maphys::DenseMatrix<Scalar>& M){
  M = maphys::DenseMatrix<Scalar>( {{1., 0.}, {10., 3.}, {0., 0.}, {2., 0.}}, 2, 2);
  M.set_property(maphys::MatrixSymmetry::hermitian, maphys::MatrixStorage::lower);
}

template<class Scalar>
typename std::enable_if<maphys::is_complex<Scalar>::value>::type
mv_sym_or_herm_sol(maphys::Vector<Scalar>& M){
  M = maphys::Vector<Scalar>({{31., -9.}, {16., 3.}});
}

TEMPLATE_TEST_CASE("Dense matrix", "[densematrix]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;

  using Vect = maphys::DenseMatrix<Scalar, 1>;
  using Mat = maphys::DenseMatrix<Scalar>;
  using Real = typename maphys::arithmetic_real<Scalar>::type;
  using EqualsVectorPW = maphys::EqualsVectorPW<Vect>;
  using EqualsMatrixPW = maphys::EqualsMatrixPW<Mat>;
  using std::size_t;
// Header:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Constructors and assignment][Constructors and assignment:1]]
SECTION(" Test constructors"){
  Mat M1;
  auto array_size = [](const Mat& m){ return m.get_array().get_n_rows() * m.get_array().get_n_cols(); };
  REQUIRE( n_rows(M1) == 0 );
  REQUIRE( n_cols(M1) == 0 );
  REQUIRE( array_size(M1) == 0 );

  Mat M2(3, 4);
  REQUIRE( n_rows(M2) == 3 );
  REQUIRE( n_cols(M2) == 4 );
  REQUIRE( array_size(M2) == 3*4 );
  bool is_fullofzeros = true;
  for (auto k = 0; k < 3*4; ++k){
    is_fullofzeros &= (M2.get_ptr()[k] == Scalar{0.0});
  }
  REQUIRE( is_fullofzeros );

  //Pointer constructor
  std::vector<Scalar> M3_data{ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 };
  Mat M3(2, 3, M3_data.data());

  // 1 3 5
  // 2 4 6
  Mat M4 ({ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 }, 2, 3);
  REQUIRE_THAT(M3, EqualsMatrixPW(M4));

  Mat M4bis ({ 1.0, 3.0, 5.0, 2.0, 4.0, 6.0 }, 2, 3, true);
  REQUIRE_THAT(M4bis, EqualsMatrixPW(M4));
}

SECTION("From a vector (copy)"){
  Vect V1({1.0, 2.0, 3.0}, 3);
  Mat M5(V1);
  REQUIRE( M5.get_n_rows() == 3 );
  REQUIRE( M5.get_n_cols() == 1 );
  REQUIRE( V1.get_n_rows() == 3 );
}

SECTION("Move / copy constructors"){
  Vect V1({1.0, 2.0, 3.0}, 3);
  Mat M6(std::move(V1));
  REQUIRE( M6.get_n_rows() == 3 );
  REQUIRE( M6.get_n_cols() == 1 );
  REQUIRE( V1.get_n_rows() == 0 );
  // Copy
  Mat M7(M6);
  REQUIRE( M6 == M7 );
  // Move

  Mat M8 = std::move(M7);
  REQUIRE( M8 == M6 );
  REQUIRE( M7.get_n_rows() == 0 );
  REQUIRE( M7.get_n_cols() == 0 );
}

SECTION("Copy / move assignment"){
  Mat M9_base({ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 }, 2, 3);
  Mat M9;
  M9 = M9_base;
  REQUIRE( M9 == M9_base );

  Mat M10_base({ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 }, 2, 3);
  Mat M10;
  M10 = std::move(M10_base);
  REQUIRE( M10 == M9 );
  REQUIRE( M10_base.get_n_rows() == 0 );
  REQUIRE( M10_base.get_n_cols() == 0 );
}

SECTION("Copy / move CTor & assignment with fixed ptr"){
  Mat M9_base({ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 }, 3, 2);
  Mat empty;

  std::vector<Scalar> data{ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 };
  Scalar * data_ptr = &data[0];
  Mat M9(3, 2, data_ptr, true);
  Mat M10(3, 2, data_ptr, true);

  Mat M10b(std::move(M9));
  REQUIRE(M10b.get_ptr() == data_ptr);
  REQUIRE(M10b == M9_base);
  REQUIRE(M9 == empty);

  Mat M11b;
  M11b = std::move(M10);
  REQUIRE(M11b.get_ptr() == data_ptr);
  REQUIRE(M11b == M9_base);
  REQUIRE(M10 == empty);
}

const Vect V2({1.0, 2.0, 3.0}, 3);
SECTION("Copy from a vector"){
  Mat M11 = V2;
  REQUIRE(M11.get_n_rows() == 3);
  REQUIRE(M11.get_n_cols() == 1);
}

SECTION("Copy from a vector and transpose"){
  Mat M12 = V2;
  M12.transpose();
  REQUIRE(M12.get_n_rows() == 1);
  REQUIRE(M12.get_n_cols() == 3);
}

SECTION("Move from a vector"){
  Vect V2_loc = V2;
  Mat M13 = std::move(V2_loc);
  REQUIRE(M13.get_n_rows() == 3);
  REQUIRE(M13.get_n_cols() == 1);
  REQUIRE(V2_loc.get_n_rows() == 0);
}
// Constructors and assignment:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Accessors][Accessors:1]]
SECTION("Accessors (2D)"){
  Mat M9({ 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 }, 2, 3);

  REQUIRE(M9(0, 0) == Scalar{1.0});
  REQUIRE(M9(1, 0) == Scalar{2.0});
  REQUIRE(M9(0, 1) == Scalar{3.0});
  REQUIRE(M9(1, 1) == Scalar{4.0});
  REQUIRE(M9(0, 2) == Scalar{5.0});
  REQUIRE(M9(1, 2) == Scalar{6.0});
  REQUIRE(M9.at(1, 2) == Scalar{6.0});

  Scalar s;
  REQUIRE_THROWS(s = M9.at(2, 1));
  REQUIRE_THROWS(s = M9.at(1, 3));
}

SECTION("Accessors (1D)"){
  Vect VV({ 1.0, 2.0, 3.0, 4.0}, 4);
  REQUIRE(VV(0)    == Scalar{1.0});
  REQUIRE(VV(1)    == Scalar{2.0});
  REQUIRE(VV(2)    == Scalar{3.0});
  REQUIRE(VV(3)    == Scalar{4.0});
  REQUIRE(VV[0]    == Scalar{1.0});
  REQUIRE(VV[1]    == Scalar{2.0});
  REQUIRE(VV[2]    == Scalar{3.0});
  REQUIRE(VV[3]    == Scalar{4.0});
  REQUIRE(VV.at(0) == Scalar{1.0});
  REQUIRE(VV.at(1) == Scalar{2.0});
  REQUIRE(VV.at(2) == Scalar{3.0});
  REQUIRE(VV.at(3) == Scalar{4.0});
  Scalar s;
  REQUIRE_THROWS(s = VV.at(4));
}

//Accessor
SECTION("Matrix assgnment with (i, j)"){
  Mat MM( {1.0,-1.0,2.0,-2.0,3.0,-3.0}, 2, 3);
  for(size_t j = 0; j < n_cols(MM); ++j){
    for(size_t i = 0; i < n_rows(MM); ++i){
      MM(i, j) = (Scalar) i * Scalar{13.6} + (Scalar) j;
    }
  }
  for(size_t j = 0; j < n_cols(MM); ++j){
    for(size_t i = 0; i < n_rows(MM); ++i){
      REQUIRE(MM(i, j) == ((Scalar) i * Scalar{13.6} + (Scalar) j));
    }
  }
}
// Accessors:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Operations on vectors and matrices][Operations on vectors and matrices:1]]
// 1 4
// 2 5
// 3 6
// Frobenius norm: 9.539392014
const Mat M14( { 1.0,  2.0, 3.0, 4.0, 5.0, 6.0}, 3, 2);
const Mat M15( {-1.0, -2.0, 0.0, 1.0, 1.0, 1.0}, 3, 2);

SECTION("Matrix addition"){
  Mat M14pM15 ( { 0.0,  0.0, 3.0, 5.0, 6.0, 7.0}, 3, 2);
  REQUIRE( (M14 + M15) == M14pM15);
}

SECTION("Matrix substraction"){
  Mat M14mM15 ( { 2.0,  4.0, 3.0, 3.0, 4.0, 5.0}, 3, 2);
  REQUIRE( (M14 - M15) == M14mM15);
}

SECTION("Matrix frobenius norm"){
  REQUIRE( M14.norm() - Real{9.539392014} < Real{1e-5} );
}

SECTION("Matrix scalar multiplication"){
  Mat M14over2 ( {0.5, 1.0, 1.5, 2.0, 2.5, 3.0}, 3, 2);
  REQUIRE_THAT(M14over2,
	       EqualsMatrixPW(M14 * Scalar{0.5}) and
	       EqualsMatrixPW(Scalar{0.5} * M14) and
	       EqualsMatrixPW(M14 / Scalar{2.0}));
}

SECTION("Matrix scalar multiplication & fixed ptr: V = a U"){
  std::vector<Scalar> d{1, 2, 3};
  const Vect U(maphys::DenseData<Scalar, 1>(3, &d[0]), true);
  Vect U3({3.0, 6.0, 9.0}, 3);
  Vect V = Scalar{3} * U;

  const Vect Uref{1.0, 2.0, 3.0};
  REQUIRE_THAT(V,    EqualsVectorPW(U3));
  REQUIRE_THAT(Uref, EqualsVectorPW(U));
}

SECTION("Matrix vector multiplication"){
  Vect V3({1.0, -1.0}, 2);
  Vect M14timesV3{-3.0, -3.0, -3.0};
  Vect res = M14 * V3; // To be sure result is cast to vector
  REQUIRE_THAT(M14timesV3, EqualsVectorPW(M14timesV3));
}

const Mat M16( {1.0,-1.0,2.0,-2.0,3.0,-3.0}, 2, 3);
SECTION("Matrix matrix multiplication"){
  //            1 4
  //  1  2  3 x 2 5 =  14  32
  // -1 -2 -3   3 6   -14 -32

  Mat M16timesM14( {14.0, -14.0, 32.0, -32.0}, 2, 2);
  Mat res2 = M16 * M14;
  REQUIRE_THAT(M16timesM14, EqualsMatrixPW(res2));

  // With symmetric / hermitian matrix
  Mat Msym;
  sym_or_herm_mat(Msym);

  const Mat MB({1., 2., 3., 4.}, 2, 2);

  Mat M_exp;
  sym_or_herm_sol(M_exp);

  Mat M_found = Msym * MB;
  REQUIRE_THAT(M_exp, EqualsMatrixPW(M_found));

  Mat M_exp2;
  sym_or_herm_sol2(M_exp2);
  Mat M_found2 = MB * Msym;
  REQUIRE_THAT(M_exp2, EqualsMatrixPW(M_found2));
}

SECTION("Transposition"){
  Mat M;
  testmat_adjoint(M);
  Mat M_adj;
  testmat_adjoint_exp(M_adj);
  REQUIRE_THAT(adjoint(M), EqualsMatrixPW(M_adj));

  Mat M_square({1, 2, 3, 4}, 2, 2);
  Mat M_sqaure_adj({1, 3, 2, 4}, 2, 2);
  REQUIRE_THAT(adjoint(M_square), EqualsMatrixPW(M_sqaure_adj));
}

const Vect V13({  1.0, 2.0, 4.0 }, 3);
const Vect V14({ -1.0, 1.0, 2.0 }, 3);

SECTION( "Dot product"){
  Scalar V13dotV14 = 9.0;
  REQUIRE( maphys::dot(V13, V14) == V13dotV14 );
  REQUIRE( maphys::dot(V14, V13) == V13dotV14 );
}

SECTION( "Matrix vector product (rect matrix)"){
  //                  1
  //                  2
  // (1  3 -1 -2)  x -1 =  6
  // (2 -2  3  4)     1   -1
  Mat M2({1.0, 2.0, 3.0, -2.0, -1.0, 3.0, -2.0, 4.0}, 2, 4);
  Vect V17({1.0, 2.0, -1.0, 1.0});
  Vect M2timesV17({ 6.0, -1.0 });
  Vect M2timesV17_check = M2 * V17;
  REQUIRE_THAT(M2timesV17_check, EqualsVectorPW(M2timesV17));
  V17 = M2 * V17;
  REQUIRE_THAT(M2timesV17, EqualsVectorPW(V17));
}

SECTION( "Matrix vector product (sym matrix)"){
  Mat Msym;
  mv_sym_or_herm_mat(Msym);
  Vect V_exp;
  mv_sym_or_herm_sol(V_exp);
  Vect V22({1., 3.}, 2);
  Vect V_found = Msym * V22;
  REQUIRE_THAT(V_exp, EqualsVectorPW(V_found));
}

SECTION( "Vector scalar multiplication"){
  Vect V13over2({0.5, 1.0, 2.0});
  REQUIRE_THAT(V13over2,
	       EqualsVectorPW(Vect(V13 * Scalar{0.5})) and
	       EqualsVectorPW(Vect(Scalar{0.5} * V13)) and
	       EqualsVectorPW(Vect(V13 / Scalar{2.0})));
}
// Operations on vectors and matrices:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*GEMV GEMM operations][GEMV GEMM operations:1]]
//                  1
//                  2
// (1  3 -1 -2)  x -1 + 3 = 9
// (2 -2  3  4)     1   2 = 1
Mat M20({1.0, 2.0, 3.0, -2.0, -1.0, 3.0, -2.0, 4.0}, 2, 4);
Vect V18({1.0, 2.0, -1.0, 1.0}, 4);
Vect V19({3.0, 2.0}, 2);
const Vect M20timesV18plusV19{ 9.0, 1.0 };

SECTION( "Test Y <- AX + B"){
  Vect M20timesV18plusV19_test = M20 * V18 + V19;
  REQUIRE_THAT(M20timesV18plusV19, EqualsVectorPW(M20timesV18plusV19_test));
}

SECTION( "Test Y <- AY + B"){
  V18 = M20 * V18 + V19;
  REQUIRE_THAT(M20timesV18plusV19, EqualsVectorPW(V18));
}

SECTION( "Test Y <- AX + Y"){
  V18 = Vect{1.0, 2.0, -1.0, 1.0};
  V19 = M20 * V18 + V19;
  REQUIRE_THAT(M20timesV18plusV19, EqualsVectorPW(V19));
}

SECTION( "Test Y <- AX + Y"){
  Vect M20timesV18minusV19({3, -3}, 2);
  V18 = Vect{1.0, 2.0, -1.0, 1.0};
  Vect V20 = M20 * V18 - V19;
  REQUIRE_THAT(M20timesV18minusV19, EqualsVectorPW(V20));
}

SECTION("X = A * X"){
  Vect X{1,2};
  const Mat M25({1,1,1,-1}, 2, 2);
  X = M25 * X;
  REQUIRE_THAT(Vect({3, -1}), EqualsVectorPW(X));

  // Test with view
  X = Vect{1,2};
  Vect Xv = X.get_vect_view(0);
  Xv = M25 * Xv;
  REQUIRE_THAT(Vect({3, -1}), EqualsVectorPW(Xv));
}

SECTION("C = A * C"){
  Mat MC({1,2,3,4,5,6}, 2, 3);
  const Mat MA({1,1,1,-1}, 2, 2);
  const Mat MAMC({3, -1, 7, -1, 11, -1}, 2, 3);
  MC = MA * MC;
  REQUIRE_THAT(MAMC, EqualsMatrixPW(MC));
}
// GEMV GEMM operations:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Dot block and views][Dot block and views:1]]
// (1  3 -1 -2)  (dot) (1  2 -1  1) = ( 7 10 10 -18)
// (2 -2  3  4)        (3 -2  3 -4)
SECTION("Column-wise dot product"){
  std::vector<Scalar> dots{7, 10, 10, -18};
  Mat M21({1.0, 2.0, -1.0, 1.0, 3.0, -2.0, 3.0, -4.0}, 2, 4, true);
  REQUIRE(maphys::cwise_dot(M20, M21) == dots);
}

// ( 1  2 ) ^T  ( 1  3 )
// ( 3 -2 )     ( 2 -2 ) = ( 6  2 )
// (-1  3 )     (-1  3 )   (-1  3 )
// (-2  4 )     ( 1 -4 )
SECTION("Dot block"){
  const Mat MA({1, 3, -1, -2, 2, -2, 3, 4}, 4, 2);
  const Mat MB({1, 2, -1, 1, 3, -2, 3, -4}, 4, 2);
  const Mat Msol({6, -1, 2, 3}, 2, 2);
  REQUIRE(maphys::dot_block(MA, MB) == Msol);
}

SECTION("Block view test read"){
  const Mat M22({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}, 4, 4);
  const Vect V22({1,2}, 2);

  const Vect Res_0_0_2_2 = M22.get_block_view(0, 0, 2, 2) * V22;
  const Vect Res_0_0_2_2_exp({11,14}, 2);
  REQUIRE_THAT(Res_0_0_2_2_exp, EqualsVectorPW(Res_0_0_2_2));

  const Vect Res_1_1_3_3 = M22.get_block_view(1, 1, 2, 2) * V22;
  const Vect Res_1_1_3_3_exp({26,29}, 2);
  REQUIRE_THAT(Res_1_1_3_3_exp, EqualsVectorPW(Res_1_1_3_3));

  const Vect Res_2_2_4_4 = M22.get_block_view(2, 2, 2, 2) * V22;
  const Vect Res_2_2_4_4_exp({41,44}, 2);
  REQUIRE_THAT(Res_2_2_4_4_exp, EqualsVectorPW(Res_2_2_4_4));
}

SECTION("Vector view test write"){
  Mat M23({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
  const Vect V23({11,12,13}, 3);

  // Replace second vector
  M23.get_vect_view(1) = V23;

  const Mat M23_exp({1, 2, 3, 11, 12, 13, 7, 8, 9}, 3, 3);
  REQUIRE_THAT(M23_exp, EqualsMatrixPW(M23));
}

SECTION("Block view test write"){
  Mat M23({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
  const Mat MB23({11,12,14,15}, 2, 2);

  // Replace block 2x2
  M23.get_block_view(0, 0, 2, 2) = MB23;

  const Mat M23_exp({11, 12, 3, 14, 15, 6, 7, 8, 9}, 3, 3);
  REQUIRE_THAT(M23_exp, EqualsMatrixPW(M23));
}

SECTION("Column view"){
  Mat M231({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
  Vect c1({1, 2, 3});
  Vect c2({4, 5, 6});
  Vect c3({7, 8, 9});
  auto v1 = M231.get_col_view(0, 0, 3);
  auto v2 = M231.get_col_view(0, 1, 3);
  auto v3 = M231.get_col_view(0, 2, 3);
  REQUIRE_THAT(v1, EqualsVectorPW(v1));
  REQUIRE_THAT(v2, EqualsVectorPW(v2));
  REQUIRE_THAT(v3, EqualsVectorPW(v3));

  Mat M231_c({1, 2, 3, 8, 10, 12, -7, -8, -9}, 3, 3);
  v2 *= Scalar{2};
  v3 *= Scalar{-1};
  REQUIRE_THAT(M231_c, EqualsMatrixPW(M231));
}

SECTION("Row view"){
  Mat M231({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
  Vect r1({1, 4, 7});
  Vect r2({2, 5, 8});
  Vect r3({3, 6, 9});
  auto v1 = M231.get_row_view(0, 0, 3);
  auto v2 = M231.get_row_view(1, 0, 3);
  auto v3 = M231.get_row_view(2, 0, 3);

  REQUIRE_THAT(r1, EqualsVectorPW(v1));
  REQUIRE_THAT(r2, EqualsVectorPW(v2));
  REQUIRE_THAT(r3, EqualsVectorPW(v3));

  Mat M231_c({1, 4, -3, 4, 10, -6, 7, 16, -9}, 3, 3);
  v2 *= Scalar{2};
  v3 *= Scalar{-1};
  REQUIRE_THAT(M231_c, EqualsMatrixPW(M231));
}

SECTION("Diag view"){
  Mat M231({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
  Vect d0({1, 5, 9}); // Main diag
  Vect d1({4, 8}); // Sub diag +1
  Vect dm1({2, 6}); // Sub diag -1
  auto v1 = M231.get_diag_view();
  auto v2 = M231.get_diag_view(1);
  auto v3 = M231.get_diag_view(-1);

  REQUIRE_THAT(d0, EqualsVectorPW(v1));
  REQUIRE_THAT(d1, EqualsVectorPW(v2));
  REQUIRE_THAT(dm1, EqualsVectorPW(v3));

  Mat M231_c({1, -2, 3, 8, 5, -6, 7, 16, 9}, 3, 3);
  v2 *= Scalar{2};
  v3 *= Scalar{-1};
  REQUIRE_THAT(M231_c, EqualsMatrixPW(M231));
}
// Dot block and views:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Storage change][Storage change:1]]
SECTION("Change matrix storage"){
  Mat A({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
  A.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

  if constexpr(maphys::is_complex<Scalar>::value){
    A += Scalar{0, 1} * Mat({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
    A.set_property(maphys::MatrixSymmetry::hermitian);
  }

  A.break_symmetry();
  REQUIRE(is_storage_full(A));
  REQUIRE(is_general(A));
  for(int i = 0; i < 3; ++i){
    for(int j = 0; j < 3; ++j){
      REQUIRE(A(i, j) == maphys::conj(A(j, i)));
    }
  }
}
// Storage change:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Factories / pseudo inverse][Factories / pseudo inverse:1]]
SECTION("Factory: ones"){
  const Mat MC({1,1,1,1,1,1}, 2, 3);
  REQUIRE_THAT(Mat::ones(2, 3), EqualsMatrixPW(MC));
}

SECTION("Factory: full"){
  const Mat MC({3,3,3,3,3,3}, 2, 3);
  REQUIRE_THAT(Mat::full(2, 3, Scalar{3}), EqualsMatrixPW(MC));
}

SECTION("Factory: eye"){
  const Mat MC1(
		{1, 0, 0,
		 0, 1, 0}, 2, 3, true);
  const Mat MC2(
		{1, 0,
		 0, 1,
		 0, 0}, 3, 2, true);
  REQUIRE(Mat::eye(2, 3) == MC1);
  REQUIRE(Mat::eye(3, 2) == MC2);
}

SECTION("Factory: identity"){
  const Mat MC({1,0,0,0,1,0,0,0,1}, 3, 3);
  REQUIRE(Mat::identity(3) == MC);
}

SECTION("Pseudo inverse with ~A"){
  const Mat A({2, -1, -1, 2}, 2, 2);
  auto pseudoinv_A = ~A;
  const Vect x_exp({-1, 1});
  const Vect b = A * x_exp;
  REQUIRE_THAT((pseudoinv_A * b), EqualsVectorPW(x_exp));
}

SECTION("Solve system with A % b"){
  const Mat A({2, -1, -1, 2}, 2, 2);
  const Vect x_exp({-1, 1});
  const Vect b = A * x_exp;
  REQUIRE_THAT((A % b), EqualsVectorPW(x_exp));
}
// Factories / pseudo inverse:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Serialization tests][Serialization tests:1]]
SECTION("Serialization / deserialization"){
  const Mat M24({1, 2, 3, 4, 5, 6}, 2, 3);
  std::vector<char> buf = M24.serialize();
  Mat M24_check;
  M24_check.deserialize(buf);
  REQUIRE_THAT(M24, EqualsMatrixPW(M24_check));
}
// Serialization tests:1 ends here

// [[file:../../../org/maphys/loc_data/DenseMatrix.org::*Footer][Footer:1]]
} // TEMPLATE_TEST_CASE
// Footer:1 ends here
