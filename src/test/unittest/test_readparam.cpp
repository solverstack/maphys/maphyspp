// [[file:../../../org/maphys/IO/ReadParam.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <string>

#include <maphys.hpp>
#include "maphys/IO/ReadParam.hpp"
#include "maphys/utils/Error.hpp"

#include <catch2/catch_test_macros.hpp>

void write_test_input(){

  std::ofstream file{"input.txt"};

  file << "  un: 1            " << '\n';
  file << "  deux: 2          " << '\n';
  file << "  mille: 1000      " << '\n';
  file << "  pi: 3.14         " << '\n';
  file << "  dixpi: 31.4 #testcomment      " << '\n';
  file << "  name: test # test comment 2 # blabla # blabla" << '\n';
  file << "#Another test comment name: test # test comment 2 # blabla # blabla" << '\n';
  file << "" << '\n';
  file << "     " << '\n';
  file << "    #  Another test comment name: test # test comment 2 # blabla # blabla" << '\n';
  file << "  file: ./input.txt" << '\n';
  file << "  centdix:110     " << '\n';
  file << "  # centonze: 111  " << '\n';
}

TEST_CASE("Readparam", "[IO]"){

  write_test_input();

  std::map<std::string, std::string> param_map = maphys::read_param_file("input.txt");

  for(const auto& [k, v] : param_map){
    std::cout << k << " -> " << v << '\n';
  }

  REQUIRE( std::stoi(param_map.at("un")) == 1);
  REQUIRE( std::stoi(param_map.at("deux")) == 2);
  REQUIRE( std::stoi(param_map.at("mille")) == 1000);
  REQUIRE( std::stoi(param_map.at("centdix")) == 110);
  REQUIRE( param_map.count("centonze") == 0);

  REQUIRE( std::stod(param_map.at("pi")) == 3.14);
  REQUIRE( std::stod(param_map.at("dixpi")) == 31.4);

  REQUIRE( param_map.at("name") == "test");
  REQUIRE( param_map.at("file") == "./input.txt");
}
// Tests:1 ends here
