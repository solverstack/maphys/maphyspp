// [[file:../../../org/maphys/loc_data/DiagonalMatrix.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <complex>

#include <maphys.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace maphys;

TEMPLATE_TEST_CASE("Diagonal matrix", "[diagonalmatrix]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;

  //using Vect = maphys::Vector<Scalar>;
  using DiagMat = maphys::DiagonalMatrix<Scalar>;
  //using Real = typename maphys::arithmetic_real<Scalar>::type;
  using std::size_t;

  SECTION("Test constructors"){
    DiagMat M1;
    REQUIRE( n_rows(M1) == 0 );
    REQUIRE( n_cols(M1) == 0 );
    REQUIRE( M1.get_array().size() == 0 );

    DiagMat M2(3, 4);
    REQUIRE( n_rows(M2) == 3 );
    REQUIRE( n_cols(M2) == 4 );
    REQUIRE( M2.get_array().size() == 3 );
    bool is_fullofzeros = true;
    for (auto k = 0; k < 3; ++k){
      is_fullofzeros &= (M2.get_ptr()[k] == Scalar{0.0});
    }
    REQUIRE( is_fullofzeros );

    std::vector<Scalar> M3_data{ 1.0, 2.0, 3.0 };
    Scalar * M3_data_ptr = M3_data.data();
    DiagMat M3(3, 3, M3_data_ptr);

    DiagMat M4{ 1.0, 2.0, 3.0 };
    REQUIRE( M3 == M4 );
  }
}

template<typename Scalar>
using DenseMat = DenseMatrix<Scalar, -1>;
TEMPLATE_PRODUCT_TEST_CASE("Diagonal matrix operations", "[diagonalmatrix]", (DenseMat, SparseMatrixCOO, SparseMatrixCSC), (float, double, std::complex<float>, std::complex<double>)){
  using Matrix = TestType;
  using Scalar = typename Matrix::scalar_type;
  //using Vect = maphys::Vector<Scalar>;
  using DiagMat = maphys::DiagonalMatrix<Scalar>;
  //using Real = typename maphys::arithmetic_real<Scalar>::type;

  // ( 1  0 -2 )       ( 2      )
  // ( 2 -4  0 )       (   4    )
  // ( 0  0 -1 ) + / - (     -1 )
  // ( 3  3  0 )       (        )
  //      A      + / -     D
  const int M0 = 4;
  const int N0 = 3;
  const int NNZ = 7;
  std::vector<int> iA{0, 0, 1, 1, 2, 3, 3};
  std::vector<int> jA{0, 2, 0, 1, 2, 0, 1};
  std::vector<Scalar> vA{1, -2, 2, -4, -1, 3, 3};

  const DiagMat D({2, 4, -1}, M0, N0);

  Matrix A;
  build_matrix(A, M0, N0, NNZ, iA.data(), jA.data(), vA.data());

  SECTION("Addition"){
    // A + D
    Matrix A_add;
    std::vector<int> iA_add{0, 0, 1, 2, 3, 3};
    std::vector<int> jA_add{0, 2, 0, 2, 0, 1};
    std::vector<Scalar> vA_add{3, -2, 2, -2, 3, 3};
    build_matrix(A_add, M0, N0, NNZ-1, iA_add.data(), jA_add.data(), vA_add.data());

    Matrix res = A + D;
    REQUIRE(res == A_add);
    Matrix res2 = D + A;
    REQUIRE(res2 == A_add);
    Matrix Acpy = A;
    Acpy += D;
    REQUIRE(Acpy == A_add);
  }

  SECTION("Substraction"){
    // D - A
    Matrix A_leftsub;
    std::vector<int> iA_sub{0, 0, 1, 1, 3, 3};
    std::vector<int> jA_sub{0, 2, 0, 1, 0, 1};
    std::vector<Scalar> vA_leftsub{1, 2, -2, 8, -3, -3};
    build_matrix(A_leftsub, M0, N0, NNZ-1, iA_sub.data(), jA_sub.data(), vA_leftsub.data());

    // A - D
    Matrix A_rightsub;
    std::vector<Scalar> vA_rightsub{-1, -2, 2, -8, 3, 3};
    build_matrix(A_rightsub, M0, N0, NNZ-1, iA_sub.data(), jA_sub.data(), vA_rightsub.data());

    Matrix res = A - D;
    REQUIRE(res == A_rightsub);
    Matrix res2 = D - A;
    REQUIRE(res2 == A_leftsub);
    Matrix Acpy = A;
    Acpy -= D;
    REQUIRE(Acpy == A_rightsub);
  }

  // ( 1  0 -2 )   ( 2      0 )
  // ( 2 -4  0 )   (   4    0 )
  // ( 0  0 -1 ) * (     -1 0 )
  // ( 3  3  0 )
  //      A      *     F
  // 
  // B = A^T 
  // G = F^T

  const DiagMat F({2, 4, -1}, 3, 4);
  const DiagMat G(F.t());
  const Matrix B(A.t());

  // We want to test left and right multiplication
  // in the cases where dimensions of the input (non diagonal) matrix
  // are decreased and increased

  Matrix B_leftmult;
  std::vector<int> iB_leftmult{0, 0, 0, 1, 1, 2, 2};
  std::vector<int> jB_leftmult{0, 1, 3, 1, 3, 0, 2};
  std::vector<Scalar> vB_leftmult{2, 4, 6, -16, 12, 2, 1};
  build_matrix(B_leftmult, 4, 4, NNZ, iB_leftmult.data(), jB_leftmult.data(), vB_leftmult.data());

  Matrix B_rightmult;
  std::vector<int> iB_rightmult{0, 0, 1, 2, 2};
  std::vector<int> jB_rightmult{0, 1, 1, 0, 2};
  std::vector<Scalar> vB_rightmult{2, 8, -16, -4, 1};
  build_matrix(B_rightmult, 3, 3, 5, iB_rightmult.data(), jB_rightmult.data(), vB_rightmult.data());

  SECTION("Multiplication by B"){
    // G * B
    //( 4, 3) * (3, 4) -> (4, 4)
    // left, increasing
    Matrix res = G * B;
    REQUIRE(res == B_leftmult);

    // B * G
    // (3, 4) * (4, 3) -> (3, 3)
    // right, decreasing
    Matrix res2 = B * G; 
    REQUIRE(res2 == B_rightmult);
    Matrix Bcpy = B;
    Bcpy *= G;
    REQUIRE(Bcpy == B_rightmult);
  }

  SECTION("Multiplication by A"){
    // A * F
    //( 4, 3) * (3, 4) -> (4, 4)
    // right, increasing
    Matrix A_rightmult(B_leftmult.t());
    Matrix res = A * F;
    A.display("A");
    F.display("F");
    res.display("AF");
    A_rightmult.display("A_rightmult");
    REQUIRE(res == A_rightmult);
    Matrix Acpy = A;
    Acpy *= F;
    REQUIRE(Acpy == A_rightmult);

    // F * A
    // (3, 4) * (4, 3) -> (3, 3)
    // left, decreasing
    Matrix A_leftmult(B_rightmult.t());
    Matrix res2 = F * A;
    REQUIRE(res2 == A_leftmult);
  }
}
// Tests:1 ends here
