// [[file:../../../org/maphys/precond/EigenDeflatedPcd.org::*Test DeflatedPCD][Test DeflatedPCD:1]]
#include <iostream>
#include <iomanip>
#include <maphys.hpp>
#include <maphys/precond/TwoLevelAbstractSchwarz.hpp>
#include <maphys/precond/EigenDeflatedPcd.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/solver/PartSchurSolver.hpp>
#include <maphys/solver/CentralizedSolver.hpp>
#include <maphys/testing/TestMatrix.hpp>

#include <catch2/catch_test_macros.hpp>

using namespace maphys;

using Scalar = double;
using Real = double;
const Real tol = 1e-12;
const int max_defl_space = 15;
const int max_defl_space_schur = 5;

template<typename PcdType, typename Mat, typename Vect, typename Real>
void test_cg_geneo(const Mat& A, const Vect& b){
  ConjugateGradient<Mat, Vect, PcdType> cg;
  int max_it = std::max(1, static_cast<int>(n_rows(A)));
  max_it = std::min(100, max_it);

  auto setup_geneo = [](const Mat& mat, PcdType& pcd){
                       if(MMPI::rank() == 0) std::cout << "NV = " << max_defl_space << '\n';
                       pcd.set_n_vect(max_defl_space);
                       pcd.set_n_vect_solve(2 * max_defl_space + 1);
                       pcd.set_nev(max_defl_space);
                       pcd.setup(mat);
                     };

  auto geneo_initguess = [](const Mat& mat, const Vect& rhs, PcdType& pcd){
                           (void) mat;
                           return pcd.init_guess(rhs);
                         };

  cg.setup(parameters::A{A},
           parameters::max_iter{max_it},
           parameters::tolerance{tol},
           parameters::verbose{(MMPI::rank() == 0)},
           parameters::setup_pcd<std::function<void(const Mat&, PcdType&)>>{setup_geneo},
           parameters::setup_init_guess<std::function<Vect(const Mat&, const Vect&, PcdType&)>>{geneo_initguess});

  if(MMPI::rank() == 0) std::cout << "first solve\n";
  auto x = cg * b;
  if(MMPI::rank() == 0) std::cout << "second solve\n";
  auto y = cg * (b+x);

  REQUIRE(cg.get_n_iter() != -1);
}

template<typename PcdType, typename Mat, typename Vect, typename Real>
void test_cg_geneo_schur(const Mat& A, const Vect& b){
  using PDMat = PartMatrix<DenseMatrix<Scalar>>;
  using ConjGrad = ConjugateGradient<PDMat, Vect, PcdType>;
  PartSchurSolver<Mat, Vect, Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>, ConjGrad> schur_solver;
  schur_solver.setup(parameters::A{A});

  ConjGrad& cg = schur_solver.get_solver_S();
  int max_it = std::max(1, static_cast<int>(n_rows(A)));
  max_it = std::min(100, max_it);

  auto setup_geneo = [](const PDMat& mat, PcdType& pcd){
                       if(MMPI::rank() == 0) std::cout << "NV = " << max_defl_space_schur << '\n';
                       pcd.set_n_vect(max_defl_space_schur);
                       pcd.set_n_vect_solve(2 * max_defl_space_schur + 1);
                       pcd.set_nev(max_defl_space_schur);
                       pcd.setup(mat);
                     };

  auto geneo_initguess = [](const PDMat&, const Vect& rhs, PcdType& pcd){
                           return pcd.init_guess(rhs);
                         };

  cg.setup(parameters::max_iter{max_it},
           parameters::tolerance{tol},
           parameters::verbose{(MMPI::rank() == 0)},
           parameters::setup_pcd<std::function<void(const PDMat&, PcdType&)>>{setup_geneo},
           parameters::setup_init_guess<std::function<Vect(const PDMat&, const Vect&, PcdType&)>>{geneo_initguess});


  if(MMPI::rank() == 0) std::cout << "first solve\n";
  auto x = schur_solver * b;
  if(MMPI::rank() == 0) std::cout << "second solve\n";
  auto y = schur_solver * (b+x);

  REQUIRE(cg.get_n_iter() != -1);
}

TEST_CASE("EigenDeflation preconditioner (dense)", "[precond][geneo][dense]"){
  using DMat = DenseMatrix<Scalar>;
  using LocSolver = Pastix<SparseMatrixCOO<Scalar>, DMat>;
  using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
  using PVect = PartVector<Vector<Scalar>>;

  const std::string path_to_part = "@MAPHYSPP_MATRIX_PARTITIONS_8@";
  const int n_subdomains = 8;

  // Subdomain topology
  std::shared_ptr<Process> p = bind_subdomains(n_subdomains);

  // Load matrix and RHS
  PMat A(p);
  PVect x_exp(p);
  load_subdomains_and_data(path_to_part, n_subdomains, p, A, x_exp);
  auto set_spd_lower = [](SparseMatrixCOO<Scalar>& loc_mat){
                         loc_mat.set_spd(MatrixStorage::lower);
                         loc_mat.order_indices();
                       };
  A.apply_on_data(set_spd_lower);

  for(int sd_id : x_exp.get_sd_ids()){
    auto& x_loc = x_exp.get_local_vector(sd_id);
    auto M = x_loc.get_n_rows();
    x_loc = test_matrix::random_matrix<Scalar, DMat>(M / 2, -10.0, 10.0, M, 1).matrix;
  }
  x_exp.assemble();

  PVect b = A * x_exp;

  SECTION("EigenDeflation additive pcd AS +"){
    using M0_type = EigenDeflatedPcd<PMat, PVect>;
    using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    test_cg_geneo<Pcd, PMat, PVect, Real>(A, b);
  }

  SECTION("EigenDeflation additive pcd AS + (Schur)"){
    using PDMat = PartMatrix<DenseMatrix<Scalar>>;
    using DLocSolver = BlasSolver<DenseMatrix<Scalar>, DenseMatrix<Scalar>>;
    using M0_type = EigenDeflatedPcd<PDMat, PVect>;
    using M1_type = AdditiveSchwarz<PDMat, PVect, DLocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PDMat, PVect, M0_type, M1_type, true>;
    test_cg_geneo_schur<Pcd, PMat, PVect, Real>(A, b);
  }

  /*
    SECTION("EigenDeflation deflated pcd NN deflated"){
    using M0_type = EigenDeflatedPcd<PMat, PVect>;
    using M1_type = NeumannNeumann<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    for(int n_v = 1; n_v <= max_vect_test; ++n_v){
    test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
    }

  SECTION("EigenDeflation deflated pcd RR deflated"){
    using M0_type = EigenDeflatedPcd<PMat, PVect>;
    using M1_type = RobinRobin<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    test_cg_geneo<Pcd, PMat, PVect, Real>(A, b);
  }*/
}
// Test DeflatedPCD:1 ends here
