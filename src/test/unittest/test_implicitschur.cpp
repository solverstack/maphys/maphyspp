// [[file:../../../org/maphys/solver/ImplicitSchur.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#ifdef MAPHYSPP_USE_EIGEN
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <maphys/wrappers/Eigen/Eigen.hpp>
#endif

#ifdef MAPHYSPP_USE_ARMA
#include <armadillo>
#include <maphys/wrappers/armadillo/Armadillo.hpp>
#endif

#include <maphys.hpp>
#include <maphys/loc_data/SparseMatrixCOO.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/solver/ImplicitSchur.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

TEMPLATE_TEST_CASE("ImplicitSchur", "[schur][sequential]", float, double, std::complex<float>, std::complex<double>){
  using namespace maphys;
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  using SpMat = SparseMatrixCOO<Scalar>;
  using SolverK = Pastix<SpMat, Vector<Scalar>>;

  Real tol = 1.0e-5;

  SECTION("Test implicit schur VS real schur vector multiplication"){
    SpMat K = test_matrix::general_matrix<Scalar, SpMat>().matrix;

    // Schur complement of the general matrix on the 2 last unknowns
    DenseMatrix<Scalar> schur;
    if constexpr(is_real<Scalar>::value){
	schur = DenseMatrix<Scalar>({6.,  0.23076922, 1., 7.61538458}, 2, 2, /*row_major*/ true);
      }
    else{
      schur = DenseMatrix<Scalar>({{6.35051546, -0.28865979}, {0.27835052, 0.12371134},
				   {1.08247423, 1.81443299}, {7.53608247, 1.79381443}},
	2, 2, /*row_major*/ true);
    }

    ImplicitSchur<SpMat, Vector<Scalar>, SolverK> impschur;
    impschur.setup(K);
    impschur.setup_schurlist(IndexArray<int>{2, 3});

    Vector<Scalar> u{-1, 3};
    Vector<Scalar> res_exp = schur * u;
    Vector<Scalar> res = impschur * u;
    REQUIRE((res_exp - res).norm() < tol);
  }

  if constexpr(is_real<Scalar>::value){
      SECTION("Test implicit schur - half stored input matrix"){
	SpMat Kspd = test_matrix::spd_matrix<Scalar, SpMat>();
	Kspd.to_triangular();

	DenseMatrix<Scalar> schur;
	schur = DenseMatrix<Scalar>({25.68897638, 1.97244094, 1.97244094, 53.68110236}, 2, 2);          
	/*else{
	  schur = DenseMatrix<Scalar>({{28.36645963,0}, {4.36645963,-5.75776398},
	    {4.36645963,5.75776398},  {52.95031056,0}}, 2, 2);
	}*/
	ImplicitSchur<SpMat, Vector<Scalar>, SolverK> impschur;
	impschur.setup(Kspd);
	impschur.setup_schurlist(IndexArray<int>{2, 3});

	Vector<Scalar> u{-1, 3};
	Vector<Scalar> res_exp = schur * u;
	Vector<Scalar> res = impschur * u;
	REQUIRE((res_exp - res).norm() < tol);
      }
    }
}
// Tests:1 ends here
