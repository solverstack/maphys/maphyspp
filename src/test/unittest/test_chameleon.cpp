// [[file:../../../org/maphys/utils/Chameleon.org::*Tests][Tests:1]]
#include <maphys.hpp>
#ifndef MAPHYSPP_NO_MPI
#include <maphys/dist/MPI.hpp>
#endif
#include <maphys/utils/Chameleon.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>


TEST_CASE("Chameleon", "[chameleon]"){
  using namespace maphys;

#ifndef MAPHYSPP_NO_MPI
  int nmpi = MMPI::size();
  int rmpi = MMPI::rank();
#else
  int nmpi = 1;
  int rmpi = 1;
#endif

  SECTION("Default constructor"){
    auto chameleon = Chameleon();
    REQUIRE( chameleon.getMpiNp() == nmpi );
    REQUIRE( chameleon.getMpiRank() == rmpi );
  }

  SECTION("Setters (parallel parameter and tilesize"){
    const int nt = 3;
    const int ng = 0;
    const int nb = 3;

    Chameleon::ncpus = nt;
    Chameleon::ngpus = ng;
    Chameleon::tile_size = nb;

    auto chameleon = Chameleon();

    REQUIRE(CHAMELEON_GetThreadNbr() == nt);
    int nb_check;
    CHAMELEON_Get(CHAMELEON_TILE_SIZE, &nb_check);
    REQUIRE(nb_check == nb);
  }

} //TEST_CASE
// Tests:1 ends here
