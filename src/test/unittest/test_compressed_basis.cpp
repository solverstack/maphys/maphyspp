// [[file:../../../org/maphys/loc_data/CompressedBasis.org::*Tests][Tests:1]]
#include <iostream>
#include <random>

#include <maphys.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "maphys/loc_data/CompressedBasis.hpp"

TEMPLATE_TEST_CASE("Compressed basis", "[compressor]", /*float,*/ double, std::complex<float>, std::complex<double>){
  using namespace maphys;

  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;

  const double zeta = double{1e-4};

  using Real = typename arithmetic_real<Scalar>::type;
  std::random_device rd;
  std::mt19937 gen(rd());
  Real min = -10;
  Real max = 10;
  std::uniform_real_distribution<> dis(min, max);
  auto random = [&gen,&dis](){ return static_cast<Real>(dis(gen)); };

  auto generate_elts = [random](int n){
    Vector<Scalar> cpr(n);
    for(size_t i = 0; i < n_rows(cpr); ++i){
      if constexpr(is_complex<Scalar>::value){
	cpr[i] = Scalar{random(), random()};
      }
      else{
	cpr[i] = Scalar{random()};
      }
    }
    return cpr;
  };

  auto pwdiff = [](Vector<Scalar>& v_ex, Vector<Scalar>& v_cpr){
    Real diff = 0;
    for(Size k = 0; k < n_rows(v_ex); ++k){
      diff = std::max(diff, std::abs((v_ex[k] - v_cpr[k]) / v_ex[k]));
    }
    return diff;
  };

  SZ_compressor_init();

  SECTION("CTor with dense matrix"){
    DenseMatrix<Scalar> mat(100, 5);
    for(int k = 0; k < 5; ++k){
      mat.get_vect_view(k) = generate_elts(100);
    }
    CompressedBasis<Scalar> basis(mat, zeta);

    for(int k = 0; k < 5; ++k){
      Vector<Scalar> vect = basis.get_vect(k);
      Vector<Scalar> matview = mat.get_vect_view(k);
      auto diff = pwdiff(matview, vect);
      std::cout << diff << '\n';
      REQUIRE(diff <= zeta);
    }
  }

  SECTION("Test with individual vectors"){
    CompressedBasis<Scalar> basis(3, zeta);

    Vector<Scalar> u1 = generate_elts(100);
    Vector<Scalar> u2 = generate_elts(100);
    Vector<Scalar> u3 = generate_elts(100);

    basis.set_vector(u1, 0);
    basis.set_vector(u2, 1);
    basis.set_vector(u3, 2);
    Vector<Scalar> ut1 = basis.get_vect(0);
    Vector<Scalar> ut2 = basis.get_vect_view(1);
    Vector<Scalar> ut3 = basis.get_vect_view(2);

    REQUIRE(pwdiff(u1, ut1) < zeta);
    REQUIRE(pwdiff(u2, ut2) < zeta);
    REQUIRE(pwdiff(u3, ut3) < zeta);
  }

  SECTION("Basis x vector"){
    const Size M = 20;
    const Size N = 10;

    DenseMatrix<Scalar> mat(M, N);
    for(Size k = 0; k < N; ++k){
      mat.get_vect_view(k) = generate_elts(M);
    }

    const CompressedBasis<Scalar> basis(mat, zeta);
    const Vector<Scalar> vect = generate_elts(N);
    const Vector<Scalar> mv_nocomp = mat * vect;
    const Vector<Scalar> mv_comp = basis * vect;

    auto diff = (mv_nocomp - mv_comp).norm();
    std::cout << diff << '\n';
    REQUIRE(diff < (10 * zeta));
  }

  SZ_compressor_finalize();
}
// Tests:1 ends here
