// [[file:../../../org/maphys/loc_data/SparseMatrixLIL.org::*Test][Test:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include "maphys/loc_data/SparseMatrixLIL.hpp"
#include "maphys/loc_data/SparseMatrixCOO.hpp"
#include "maphys/loc_data/SparseMatrixCSC.hpp"
#include "maphys/loc_data/DenseMatrix.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

TEMPLATE_TEST_CASE("SparseMatrixLIL", "[sparse][lil_matrix]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;
  using Spmat = maphys::SparseMatrixLIL<Scalar>;

  SECTION("Constructor"){
    Spmat SM(4, 4);
    REQUIRE( SM.get_n_rows() == 4 );
    REQUIRE( SM.get_n_cols() == 4 );
    REQUIRE( SM.get_nnz() == 0 );
  }

  SECTION("insertion"){
    Spmat SM(4, 4);
    SM.insert(0, 0, Scalar{1});
    REQUIRE( SM.get_nnz() == 1 );

    SM.insert(0, 1, Scalar{1});
    REQUIRE( SM.get_nnz() == 2 );
    SM.insert(0, 1, Scalar{1});
    REQUIRE( SM.get_nnz() == 2 );
  }

  SECTION("operator()"){
    Spmat SM(4, 4);
    SM.insert(3, 2, Scalar{13});
    REQUIRE(SM(3, 2) == Scalar{13});
  }

  SECTION("Duplicate insertions summed"){
    Spmat SM(4, 4);

    SM.insert(0, 1, Scalar{1});
    SM.insert(0, 1, Scalar{2});

    REQUIRE(SM(0, 1) == Scalar{3});

    SM.insert(2, 3, Scalar{-1});
    SM.insert(2, 3, Scalar{2});
    REQUIRE(SM(2, 3) == Scalar{1});

    SM.insert(1, 1, Scalar{1});
    SM.insert(3, 1, Scalar{1});
    SM.insert(0, 1, Scalar{3});
    REQUIRE(SM(0, 1) == Scalar{6});
  }

  // 1 . . 1
  // 2 3 5 2
  // . . 2 3
  // . 1 . 4

  Spmat SM(4, 4);
  SM.insert(0, 0, Scalar{1});
  SM.insert(0, 3, Scalar{1});
  SM.insert(1, 0, Scalar{2});
  SM.insert(1, 1, Scalar{3});
  SM.insert(1, 2, Scalar{5});
  SM.insert(1, 3, Scalar{2});
  SM.insert(2, 2, Scalar{2});
  SM.insert(2, 3, Scalar{3});
  SM.insert(3, 1, Scalar{1});
  SM.insert(3, 3, Scalar{4});

  SECTION("Conversion to coo"){
    const maphys::SparseMatrixCOO<Scalar> exp_mat(
					  {0, 0, 1, 1, 1, 1, 2, 2, 3, 3},
					  {0, 3, 0, 1, 2, 3, 2, 3, 1, 3},
					  {1, 1, 2, 3, 5, 2, 2, 3, 1, 4}, 4, 4);
    maphys::SparseMatrixCOO<Scalar> converted = SM.to_coo();
    REQUIRE(exp_mat == converted);
  }

  SECTION("Conversion to csc"){
    const maphys::SparseMatrixCSC<Scalar> exp_mat(
					  {0, 1, 1, 3, 1, 2, 0, 1, 2, 3},
					  {0, 2, 4, 6, 10},
					  {1, 2, 3, 1, 5, 2, 1, 2, 3, 4}, 4, 4);
    maphys::SparseMatrixCSC<Scalar> converted = SM.to_csc();
    REQUIRE(exp_mat == converted);
  }

  SECTION("Conversion to dense"){
    const maphys::DenseMatrix<Scalar> exp_mat(
				      {1, 0, 0, 1,
				       2, 3, 5, 2,
				       0, 0, 2, 3,
				       0, 1, 0, 4}, 4, 4, true);
    maphys::DenseMatrix<Scalar> converted = SM.to_dense();
    REQUIRE(exp_mat == converted);
  }

  SECTION("Operator *="){
    Spmat A(2, 2);
    A.insert(0, 0, Scalar{1});
    A.insert(0, 1, Scalar{2});
    A.insert(1, 1, Scalar{-2});

    Spmat halfA(2, 2);
    halfA.insert(0, 0, Scalar{1./2});
    halfA.insert(0, 1, Scalar{1});
    halfA.insert(1, 1, Scalar{-1});

    A *= Scalar{1./2};
    REQUIRE(A == halfA);
  }

  SECTION("Dropping"){
    // 1 0
    // 0 1
    Spmat SMA(2, 2);
    SMA.insert(0, 0, Scalar{1});
    SMA.insert(1, 1, Scalar{1});

    Spmat SMd = SMA;
    SMd.insert(0, 1, Scalar{0});
    SMd.insert(1, 0, Scalar{0});
    SMd.drop();
    REQUIRE(SMd == SMA);

    SMd.insert(2, 1, Scalar{1e-10});
    SMd.insert(3, 0, Scalar{-1e-10});
    SMd.drop(1e-9);
    REQUIRE(SMd == SMA);

    SMd.insert(3, 0, Scalar{-1e-6});
    REQUIRE(SMd.get_nnz() == (SMA.get_nnz() + 1));
  }

  SECTION("Operator + / -"){
    // ( 1  2 )
    // ( 0 -2 )
    Spmat A(2, 2);
    A.insert(0, 0, Scalar{1});
    A.insert(0, 1, Scalar{2});
    A.insert(1, 1, Scalar{-2});

    // ( 1  0 )
    // (-3  2 )
    Spmat B(2, 2);
    B.insert(0, 0, Scalar{1});
    B.insert(1, 0, Scalar{-3});
    B.insert(1, 1, Scalar{2});

    // ( 2  2 )
    // (-3  0 )
    Spmat AplusB(2, 2);
    AplusB.insert(0, 0, Scalar{2});
    AplusB.insert(0, 1, Scalar{2});
    AplusB.insert(1, 0, Scalar{-3});

    // ( 0  2 )
    // ( 3 -4 )
    Spmat AminusB(2, 2);
    AminusB.insert(0, 1, Scalar{2});
    AminusB.insert(1, 0, Scalar{3});
    AminusB.insert(1, 1, Scalar{-4});

    Spmat ApB = A + B;
    Spmat AmB = A - B;

    REQUIRE(ApB == AplusB);
    REQUIRE(AmB == AminusB);
  }

  // 1 . . 1   . . . 1   1 . . 2
  // 2 3 5 2 + .-3 . . = 2 . 5 2
  // . . 2 3   . 2 . .   . 2 2 3
  // . 1 . 4   . . . .   . 1 . 4
  SECTION("Insertion COO"){

    const maphys::SparseMatrixCOO<Scalar> coomat(
					  {0, 0, 1, 1, 1, 1, 2, 2, 3, 3},
					  {0, 3, 0, 1, 2, 3, 2, 3, 1, 3},
					  {1, 1, 2, 3, 5, 2, 2, 3, 1, 4}, 4, 4);
    Spmat SM_init(4, 4);
    SM_init.insert(0, 3, 1);
    SM_init.insert(1, 1, -3);
    SM_init.insert(2, 1, 2);
    Spmat SM_exp = SM + SM_init;
    SM_init.insert(coomat);
    SM_init.drop();
    REQUIRE(SM_init == SM_exp);
  }

  SECTION("Insertion CSC"){
    const maphys::SparseMatrixCSC<Scalar> cscmat(
					  {0, 1, 1, 3, 1, 2, 0, 1, 2, 3},
					  {0, 2, 4, 6, 10},
					  {1, 2, 3, 1, 5, 2, 1, 2, 3, 4}, 4, 4);
    Spmat SM_init(4, 4);
    SM_init.insert(0, 3, 1);
    SM_init.insert(1, 1, -3);
    SM_init.insert(2, 1, 2);
    Spmat SM_exp = SM + SM_init;
    SM_init.insert(cscmat);
    SM_init.drop();
    REQUIRE(SM_init == SM_exp);
  }

  SECTION("Coefficient accessors"){
    Spmat SM_nocst = SM;
    const Spmat SM_cst = SM;
    REQUIRE(SM_nocst(1,2) == Scalar{5});
    REQUIRE(SM_cst(1,2) == Scalar{5});
    REQUIRE(SM_nocst.coeff(1,2) == Scalar{5});
    REQUIRE(SM_cst.coeff(1,2) == Scalar{5});
    REQUIRE(SM_nocst.coeffRef(1,2) == Scalar{5});
    REQUIRE(SM_cst.coeffRef(1,2) == Scalar{5});

    // Missing value : throws if nonzero taken by ref
    REQUIRE_THROWS(SM_nocst(2,1));
    REQUIRE_THROWS(SM_cst(2,1));
    REQUIRE(SM_cst.coeff(2,1) == Scalar{0});
  }
} // TEMPLATE_TEST_CASE
// Test:1 ends here
