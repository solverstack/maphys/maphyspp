// [[file:../../../org/maphys/solver/Mumps.org::*Header][Header:1]]
#include <iostream>

#include <maphys.hpp>
#include <maphys/solver/Mumps.hpp>
#include <maphys/solver/ConjugateGradient.hpp>

#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
// Header:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Direct solve][Direct solve:1]]
using namespace maphys;

TEMPLATE_TEST_CASE("Mumps direct with real", "[mumps][direct][real]", float, double){

  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  using Vect = Vector<Scalar>;
  using MatCOO = SparseMatrixCOO<Scalar>;
  using MatCSC = SparseMatrixCSC<Scalar>;
  using DenseMat = DenseMatrix<Scalar>;

  // A            X =  B
  //--------------------
  // 2  -  -  -   3    1
  //-1  2  -  -   5    0
  // 0 -1  2  - x 7 =  0
  // 0  0 -1  2   9   11

  MatCOO spA_coo({  0,    1,   1,    2,   2,    3,   3},
                 {  0,    0,   1,    1,   2,    2,   3},
                 {2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0},
                 4, 4);
  spA_coo.set_spd(MatrixStorage::lower);
  MatCSC spA_csc = spA_coo.to_csc();

  const Vect B{ 1., 0., 0., 11. };
  const Vect X_exp{3., 5., 7., 9.};
  const Real test_tol = arithmetic_tolerance<Scalar>::value * 10;

  SECTION("Mumps direct solve with IJV format"){
    Mumps<MatCOO, Vect> solver_mumps(spA_coo);
    Vect X_found = solver_mumps * B;
    Real direct_err = (X_found - X_exp).norm();
    REQUIRE(direct_err < test_tol);
  }

  SECTION("Mumps direct solve with CSC format"){
    Mumps<MatCSC, Vect> solver_mumps(spA_csc);
    Vect X_found = solver_mumps * B;
    Real direct_err = (X_found - X_exp).norm();
    REQUIRE(direct_err < test_tol);
  }

  SECTION("Mumps check perfect forwarding for B"){
    Mumps<MatCOO, Vect> solver_mumps(spA_coo);
    Vect B_cpy = B;
    Vect X_found = solver_mumps.solve(std::move(B_cpy));
    Real direct_err = (X_found - X_exp).norm();
    REQUIRE(n_rows(B_cpy) == 0);
    REQUIRE(direct_err < test_tol);

    Vect B_cpy2 = B;
    X_found = solver_mumps.solve(B_cpy2);
    direct_err = (X_found - X_exp).norm();
    REQUIRE(n_rows(B_cpy2) == n_rows(B));
    REQUIRE(direct_err < test_tol);
  }

  SECTION("Mumps move basic test"){
    Mumps<MatCOO, Vect> solver_mumps(spA_coo);
    Vect X_found = solver_mumps * B;
    Real direct_err = (X_found - X_exp).norm();
    REQUIRE(direct_err < test_tol);

    Mumps<MatCOO, Vect> other_mumps = std::move(solver_mumps);
    other_mumps.display("Other mumps");
    REQUIRE_THROWS(X_found = solver_mumps * B);

    X_found = other_mumps * B;
    direct_err = (X_found - X_exp).norm();
    REQUIRE(direct_err < test_tol);
  }

  SECTION("Mumps direct solve with IJV format and 2 RHS"){
    Mumps<MatCOO, DenseMat> solver_mumps(spA_coo);
    const DenseMat multiX_exp({3., 5., 7., 9.,
                               2., 1., 0., -1.}, 4, 2);
    DenseMat multiB = spA_coo * multiX_exp;
    DenseMat X_found = solver_mumps * multiB;
    Real direct_err = (X_found - multiX_exp).norm();
    REQUIRE(direct_err < test_tol);
  }

 SECTION("Mumps - check multiple setup"){
   Mumps<MatCOO, Vect> solver_mumps;
   solver_mumps.setup(spA_coo);
   Vect X_found = solver_mumps * B;
   double direct_err = (X_found - X_exp).norm();
   REQUIRE(direct_err < test_tol);

   auto spA_cpy = spA_coo;
   solver_mumps.setup(spA_cpy);
   X_found = solver_mumps * B;
   direct_err = (X_found - X_exp).norm();
   REQUIRE(direct_err < test_tol);
 }

} // TEMPLATE_TEST_CASE

TEMPLATE_TEST_CASE("Mumps complex direct with complex", "[mumps][direct][complex]", std::complex<float>, std::complex<double>){
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  using Vect = Vector<Scalar>;
  using MatCOO = SparseMatrixCOO<Scalar>;
  using MatCSC = SparseMatrixCSC<Scalar>;

  const Real test_tol = arithmetic_tolerance<Scalar>::value * 10;

  const MatCOO A_coo = test_matrix::general_matrix<Scalar, MatCOO>().matrix;
  const MatCSC A_csc = test_matrix::general_matrix<Scalar, MatCSC>().matrix;
  const Vect X_exp{{1, 0}, {2, 1}, {3, -1}, {4, 2}};
  const Vect B = A_coo * X_exp;

  SECTION("Mumps direct solve with IJV format"){
    Mumps<MatCOO, Vect> solver_mumps(A_coo);
    Vect X_found = solver_mumps * B;
    Real direct_err = (X_found - X_exp).norm();
    REQUIRE(direct_err < test_tol);
  }

  SECTION("Mumps direct solve with CSC format"){
    Mumps<MatCSC, Vect> solver_mumps(A_csc);
    Vect X_found = solver_mumps * B;
    Real direct_err = (X_found - X_exp).norm();
    REQUIRE(direct_err < test_tol);
  }
} // TEMPLATE_TEST_CASE
// Direct solve:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Solving passing by the Schur complement][Solving passing by the Schur complement:1]]
TEST_CASE("Mumps schur", "[mumps][direct][schur]"){

  using Vect = Vector<double>;
  using MatCOO = SparseMatrixCOO<double>;
  using DenseMat = DenseMatrix<double>;

  // Solving with Schur complement
  //S = Schur complement of A on (1, 3):
  // S        x Y = f
  //------------------------------------
  //   1  -1/2    5   1/2
  // -1/2  3/2  x 9 = 11

  MatCOO spA_coo({  0,    1,   1,    2,   2,    3,   3},
                 {  0,    0,   1,    1,   2,    2,   3},
                 {2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0},
                 4, 4);
  spA_coo.set_spd(MatrixStorage::lower);

  const double test_tol = 1e-12;
  const IndexArray<int> schurlist{1, 3};
  DenseMat schur_exp_({1, -0.5, -0.5, 1.5}, 2, 2);
  schur_exp_.set_property(MatrixSymmetry::symmetric);
  const DenseMat schur_exp = schur_exp_;
  const Vect B{ 1., 0., 0., 11. };
  const Vect X_exp{3., 5., 7., 9.};
  const Vect f_exp{1./2., 11.};
  const Vect y_exp{5., 9.};

  SECTION("Mumps get Schur complement"){
    Mumps<MatCOO, Vect> schur_mumps(spA_coo);
    DenseMat schur_mat = schur_mumps.get_schur(schurlist);
    schur_mat.display("Schur complement (1, 3)");

    double schur_diff = (schur_mat - schur_exp).norm();
    REQUIRE(schur_diff < test_tol);
  }

  SECTION("Mumps Schur b2f"){
    Mumps<MatCOO, Vect> schur_mumps(spA_coo);
    DenseMat schur_mat = schur_mumps.get_schur(schurlist);
    Vect f = schur_mumps.b2f(B);

    double f_diff = (f - f_exp).norm();
    //f.display("f");
    REQUIRE(f_diff < test_tol);
  }

  SECTION("Mumps Schur solve Sy = f with CG"){
    const Vect f = f_exp;
    const DenseMat S = schur_exp;

    ConjugateGradient<DenseMat, Vect> cg_on_schur(S);
    cg_on_schur.setup(parameters::max_iter{10});
    Vect y = cg_on_schur * f;
    double y_diff = (y - y_exp).norm();
    //y.display("y");
    REQUIRE( y_diff < test_tol );
  }

  SECTION("Mumps Schur backsolve y2x"){
    Mumps<MatCOO, Vect> schur_mumps(spA_coo);
    const DenseMat schur_mat = schur_mumps.get_schur(schurlist);
    const Vect f = schur_mumps.b2f(B);
    const Vect y = y_exp;
    Vect X = schur_mumps.y2x(y);
    //X.display("X");
    //X_exp.display("Expected solution");
    double direct_err = (X - X_exp).norm();
    std::cout << "|| X - X_exp ||_2 : " << direct_err << '\n';
    REQUIRE(direct_err < test_tol);
  }

  SECTION("Mumps full solve with Schur complement + move"){
    Mumps<MatCOO, Vect> schur_mumps(spA_coo);
    DenseMat schur_mat = schur_mumps.get_schur(schurlist);
    Vect f = schur_mumps.b2f(B);

    ConjugateGradient<DenseMat, Vect> cg_on_schur(schur_mat);
    cg_on_schur.setup(parameters::max_iter{10});
    Vect y = cg_on_schur * f;

    // Let's move the mumps instance here
    Mumps<MatCOO, Vect> other_mumps = std::move(schur_mumps);

    Vect X = other_mumps.y2x(y);

    double direct_err = (X - X_exp).norm();
    std::cout << "|| X - X_exp ||_2 : " << direct_err << '\n';
    REQUIRE(direct_err < test_tol);

    schur_mumps.display("Mumps test old instance (moved)");
    other_mumps.display("Mumps test new instance");
  }
} // TEST_CASE
// Solving passing by the Schur complement:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Using low-rank features][Using low-rank features:1]]
TEST_CASE("Mumps low rank", "[mumps][direct][low_rank]"){
 using namespace maphys;
 using Scalar = double;

 // This matrix is too small to get any gain from blr...
 auto get_A = [](){
                SparseMatrixCOO<Scalar> M;
                M.from_matrix_market_file("@MAPHYSPP_MATRICES_DIR@/bcsstk17.mtx");
                M.set_spd(MatrixStorage::lower);
                return M;
              };

 const SparseMatrixCOO<Scalar> A(get_A());
 const auto m = A.get_n_rows();
 Vector<Scalar> X_exp(m);
 std::iota(&X_exp[0], &X_exp[m], Scalar{1.0});
 const Vector<Scalar> B = A * X_exp;

 const double compress_tol = 1.0e-3;
 Mumps<SparseMatrixCOO<Scalar>, Vector<Scalar>> mumps;
 mumps.setup(A);
 mumps.set_verbose(2);
 mumps.set_low_rank(compress_tol);

 auto X = mumps * B;

 auto dir_err = (X - X_exp).norm();
 auto b_e = (B - A * X).norm() / B.norm();

 std::cout << "Asked for compression tolerance: " << compress_tol << '\n';
 std::cout << "Direct error: " << dir_err << '\n';
 std::cout << "Backward error: " << b_e << '\n';
}
// Using low-rank features:1 ends here
