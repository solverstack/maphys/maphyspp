// [[file:../../../org/maphys/kernel/ChameleonKernels.org::*Tests][Tests:1]]
#include <maphys.hpp>
#include <maphys/kernel/ChameleonKernels.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <maphys/testing/Catch2DenseMatrixMatchers.hpp>

using namespace maphys;

TEMPLATE_TEST_CASE("ChameleonKernels", "[dense][sequential][chameleon][kernel]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;
  using EqualsMatrixPW = maphys::EqualsMatrixPW<DenseMatrix<Scalar>>;

  // optional: set number of workers used by Chameleon (ncpus, ngpus and tile size)
  Chameleon::ncpus = 2;
  Chameleon::ngpus = 0;
  Chameleon::tile_size = 2;

  // Initialize Chameleon context
  Chameleon chameleon;

  //using Real = typename arithmetic_real<Scalar>::type;
  //using Complex = typename arithmetic_complex<Scalar>::type;
  //constexpr const Real ev_tol = (is_precision_double<Scalar>::value) ? 1e-12 : 1e-5;
// Tests:1 ends here

// [[file:../../../org/maphys/kernel/ChameleonKernels.org::*Blas 3][Blas 3:1]]
SECTION("Blas 3 - GEMM - general"){
  const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
  const DenseMatrix<Scalar> B({1.0,  2.0, 3.0,  4.0, 5.0,  6.0}, 3, 2);

  //            1 4
  //  1  2  3 x 2 5 =  14  32
  // -1 -2 -3   3 6   -14 -32

  const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
  DenseMatrix<Scalar> C(maphys::n_rows(A), maphys::n_cols(B));

  chameleon_kernels::gemm(A, B, C);
  REQUIRE_THAT(C_check, EqualsMatrixPW(C));
}

SECTION("Blas 3 - GEMM - symmetric / hermitian"){
  DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
  DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
  L.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

  DenseMatrix<Scalar> ML({-14,-16,-18, 5, 7, 9, 36, 39, 42}, 3,3);
  DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
  if constexpr(maphys::is_complex<Scalar>::value){
    M += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 1,
                                             -1, 2, -2,
                                             1, -1, -0.5}, 3, 3);
    L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
    L.set_property(maphys::MatrixSymmetry::hermitian);
    ML = DenseMatrix<Scalar>({{-16, -3}, {-13, 11}, {-19.5, -0.5}, {6, -9}, {6, -19}, {7, -11.5}, {39, 8}, {34, 8}, {47, 1.5}}, 3, 3);
    LM = DenseMatrix<Scalar>({{-4, -7}, {2, 9}, {11, -2}, {-16, 10}, {12, 2}, {28, -11}, {-17.5, -2.5}, {17, 13.5}, {29, -15.5}}, 3, 3);
  }

  DenseMatrix<Scalar> my_ML(3, 3);
  chameleon_kernels::gemm(M, L, my_ML);
  REQUIRE_THAT(ML, EqualsMatrixPW(my_ML));

  DenseMatrix<Scalar> my_LM(3, 3);
  chameleon_kernels::gemm(L, M, my_LM);
  REQUIRE_THAT(LM, EqualsMatrixPW(my_LM));
}
// Blas 3:1 ends here

// [[file:../../../org/maphys/kernel/ChameleonKernels.org::*Footer][Footer:1]]
}
// Footer:1 ends here
