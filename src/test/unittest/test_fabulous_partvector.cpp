// [[file:../../../org/maphys/solver/Fabulous.org::*Tests distributed][Tests distributed:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include "maphys/dist/MPI.hpp"
#include "maphys/solver/Fabulous.hpp"

#include <catch2/catch_test_macros.hpp>

// Non master processes do not own data.
// Therefore they are not concerned by the tests.
#define REQ_MASTER( cond_ ) do {                \
    if(p->is_master()) REQUIRE( cond_ );         \
  } while(0)

TEST_CASE("distvectorfabulous", "[distvectorfabulous][distributed]"){
  using namespace maphys;
  using Nei_map = std::map<int, IndexArray<int>>;

  const int n_dofs = 4;

  // SD 1
  Nei_map NM0 {{1, {2, 3}},
   {2, {0, 3}}};

  // SD 2
  Nei_map NM1 {{0, {1, 0}},
  {2, {0, 3}}};

  // SD 3
  Nei_map NM2 {{0, {2, 3}},
   {1, {3, 0}}};

  std::vector<Subdomain> sd;
  sd.emplace_back(0, n_dofs, std::move(NM0), false);
  sd.emplace_back(1, n_dofs, std::move(NM1), false);
  sd.emplace_back(2, n_dofs, std::move(NM2), false);

  std::shared_ptr<Process> p = bind_subdomains( static_cast<int>(sd.size()) );
  p->load_subdomains(sd);

  //V = (0, 1, 2, 3, 4, 5, 6)^T
  //    (0, 2, 4, 6, 8, 10, 12)^T
  std::map<int, DenseMatrix<double, 2>> m1;
  if(p->owns_subdomain(0)) m1.emplace(0, DenseMatrix<double, 2>{{0.0, 1.0, 2.0, 3.0, 0.0, 2.0, 4.0, 6.0},4,2});
  if(p->owns_subdomain(1)) m1.emplace(1, DenseMatrix<double, 2>{{3.0, 2.0, 4.0, 5.0, 6.0, 4.0, 8.0, 10.0},4,2});
  if(p->owns_subdomain(2)) m1.emplace(2, DenseMatrix<double, 2>{{5.0, 6.0, 0.0, 3.0, 10.0, 12.0, 0.0, 6.0},4,2});
  //W = (2, 2, 2, 2, 2, 2, 2)^T

  std::map<int, DenseMatrix<double, 2>> m2;
  if(p->owns_subdomain(0)) m2.emplace(0, DenseMatrix<double, 2>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
  if(p->owns_subdomain(1)) m2.emplace(1, DenseMatrix<double, 2>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
  if(p->owns_subdomain(2)) m2.emplace(2, DenseMatrix<double, 2>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});

  const FabulousPartVector<DenseMatrix<double, 2>> dv = convert_to_fabulous(PartVector<DenseMatrix<double, 2>>(p, m1));
  const FabulousPartVector<DenseMatrix<double, 2>> dw = convert_to_fabulous(PartVector<DenseMatrix<double, 2>>(p, m2));
  SECTION("dot product"){
    fabulous::Block<double> vw(2, 2);
    dv.dot(dw, vw);
    double vw_exp = 42;
    REQ_MASTER(vw(0, 0) == vw_exp);
    REQ_MASTER(vw(0, 1) == 2*vw_exp);
    REQ_MASTER(vw(1, 0) == 2*vw_exp);
    REQ_MASTER(vw(1, 1) == 4*vw_exp);
  }

  SECTION("Number of columns"){
    REQ_MASTER(dv.get_n_cols() == 2);
    FabulousPartVector<DenseMatrix<double>> empty{};
    REQ_MASTER(empty.get_n_cols() == 0);
  }
  const DenseMatrix<double, 2> dv_centr_exp{{0.0,1.0,2.0,3.0,4.0,5.0,6.0,0.0,2.0,4.0,6.0,8.0,10.0,12.0},7,2};
  const DenseMatrix<double, 2> dw_centr_exp{{2.0,2.0,2.0,2.0,2.0,2.0,2.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0},7,2};

  SECTION("axpy"){
    //((2,2,2,2,2,2,2),        = ((6,6,6,6,6,6,6),
    // (4,4,4,4,4,4,4))^T * A     (12,12,12,12,12,12,12))^T
    std::map<int, DenseMatrix<double>> m;
    if(p->owns_subdomain(0)) m.emplace(0, DenseMatrix<double>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
    if(p->owns_subdomain(1)) m.emplace(1, DenseMatrix<double>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
    if(p->owns_subdomain(2)) m.emplace(2, DenseMatrix<double>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});

    FabulousPartVector<DenseMatrix<double>> U = convert_to_fabulous(PartVector<DenseMatrix<double>>(p, m));
    DenseMatrix<double> dw_centr = U.centralize();
    DenseMatrix<double> A22m = DenseMatrix<double>{{1.0, 2.0, 3.0, 4.0},2,2};
    DenseMatrix<double> A21m = DenseMatrix<double>{{5.0, 6.0},2,1};
    fabulous::Block<double> A22 = convert_to_fabulous(A22m);
    fabulous::Block<double> A21 = convert_to_fabulous(A21m);

    const DenseMatrix<double, 2> UA22_ref{{12.0,12.0,12.0,12.0,12.0,12.0,12.0,26.0,26.0,26.0,26.0,26.0,26.0,26.0},7,2};
    const DenseMatrix<double, 1> UA21_ref{{36.0,36.0,36.0,36.0,36.0,36.0,36.0},7,1};
    FabulousPartVector<DenseMatrix<double>> UA22 = U.copy(); // U += U * A22;
    FabulousPartVector<DenseMatrix<double>> UA21 = convert_to_fabulous(U.get_vect(0)); // U += U * A21;
    UA22.axpy(U, A22);
    UA21.axpy(U, A21);
    DenseMatrix<double> UA22_centr = UA22.centralize();
    DenseMatrix<double> U_centr = U.centralize();
    DenseMatrix<double> UA21_centr = UA21.centralize();
    REQ_MASTER(UA22_centr == UA22_ref);
    REQ_MASTER(UA21_centr == UA21_ref);
    REQ_MASTER(U_centr == dw_centr);
  }

  SECTION("conversion dense"){
    DenseMatrix<double> U{{2.0,2.0,2.0,2.0,2.0,2.0,2.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0},7,2};
    fabulous::Block<double> Uf = convert_to_fabulous(U);
    const DenseMatrix<double, 2> dw3{{6.0,6.0,6.0,6.0,6.0,6.0,6.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0},7,2};
    Uf *= 3.0;
    DenseMatrix<double> Um = convert_to_mpp(Uf);
    REQ_MASTER(U == dw3);
    REQ_MASTER(Um == dw3);
  }

  SECTION("conversion PartVector"){
    PartVector<DenseMatrix<double, 2>> U = dw;
    FabulousPartVector<DenseMatrix<double, 2>> Uf = convert_to_fabulous(U);
    const DenseMatrix<double, 2> dw3{{6.0,6.0,6.0,6.0,6.0,6.0,6.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0},7,2};
    Uf *= 3;
    PartVector<DenseMatrix<double, 2>> Um = convert_to_mpp(Uf);
    DenseMatrix<double, 2> Umc = Um.centralize();
    DenseMatrix<double, 2> Uc = U.centralize();
    REQ_MASTER(Uc != dw3);
    REQ_MASTER(Umc == dw3);
  }

  SECTION("trsm"){
    const DenseMatrix<double, 2> res{{1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0},7,2};
    PartVector<DenseMatrix<double, 2>> U = dw;
    FabulousPartVector<DenseMatrix<double, 2>> Uf = convert_to_fabulous(U);
    fabulous::Block<double> r(2, 2);
    r(0, 0) = 2.0;
    r(0, 1) = 1.0;
    r(1, 1) = 3.0;
    Uf.trsm(Uf, r);
    DenseMatrix<double, 2> Uc = Uf.centralize();
    REQ_MASTER(res == Uc);
  }

  SECTION("same distribution diffrent number of columns"){
    PartVector<DenseMatrix<double, 2>> dv_bis2 = dv;
    PartVector<DenseMatrix<double>> dv_bis3 = *((PartVector<DenseMatrix<double>>*)&dv_bis2);
    FabulousPartVector<DenseMatrix<double>> dv_bis = convert_to_fabulous(dv_bis3);
    //const DenseMatrix<double> dv0_centr_exp{7,1};
    const DenseMatrix<double> dv1_centr_exp{{0.0,0.0,0.0,0.0,0.0,0.0,0.0},7,1};
    const DenseMatrix<double> dv3_centr_exp{{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},7,3};
    FabulousPartVector<DenseMatrix<double>> dv0{dv_bis, 0};
    FabulousPartVector<DenseMatrix<double>> dv1{dv_bis, 1};
    FabulousPartVector<DenseMatrix<double>> dv3{dv_bis, 3};
    DenseMatrix<double> dv0c = dv0.centralize();
    DenseMatrix<double> dv1c = dv1.centralize();
    DenseMatrix<double> dv3c = dv3.centralize();
    DenseMatrix<double> dv_centr_bis = dv_bis.centralize();
    REQ_MASTER(dv0c.get_n_cols() == 0);
    REQ_MASTER(dv1c.get_n_cols() == 1);
    REQ_MASTER(dv3c.get_n_cols() == 3);
    //REQ_MASTER(dv0_centr_exp == dv0c);
    REQ_MASTER(dv1_centr_exp == dv1c);
    REQ_MASTER(dv3_centr_exp == dv3c);
    REQ_MASTER(dv_centr_exp == dv_centr_bis);
  }

  SECTION("qr"){
    std::map<int, DenseMatrix<double, 2>> m;
    if(p->owns_subdomain(0)) m.emplace(0, DenseMatrix<double, 2>{{2.0, 2.0, 2.0, 7.0, 4.0, 4.0, 4.0, 4.0},4,2});
    if(p->owns_subdomain(1)) m.emplace(1, DenseMatrix<double, 2>{{7.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
    if(p->owns_subdomain(2)) m.emplace(2, DenseMatrix<double, 2>{{2.0, 2.0, 2.0, 7.0, 4.0, 4.0, 4.0, 4.0},4,2});

    PartVector<DenseMatrix<double, 2>> U(p, m);
    FabulousPartVector<DenseMatrix<double, 2>> Uf = convert_to_fabulous(U);
    fabulous::Block<double> r(2, 2);
    Uf.qr(Uf, r);
    fabulous::Block<double> Id(2, 2);
    Uf.dot(Uf, Id);
    REQ_MASTER(Id(0, 0) - 1.0 < 1e-6);
    REQ_MASTER(Id(0, 1) < 1e-6);
    REQ_MASTER(Id(1, 0) < 1e-6);
    REQ_MASTER(Id(1, 1) - 1.0 < 1e-6);
    //REQ_MASTER((U1c == Uc || U1c == -1.0 * Uc));
    REQ_MASTER(r(0,0) - 8.544 < 1e-5);
    REQ_MASTER(r(0,1) - 8.89513 < 1e-5);
    REQ_MASTER(r(1,0) == 0.0);
    REQ_MASTER(r(1,1) - 5.73382 < 1e-5);
  }
} // TEST_CASE
// Tests distributed:1 ends here
