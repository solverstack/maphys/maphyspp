// [[file:../../../org/maphys/solver/Pastix.org::*Test header][Test header:1]]
#include <maphys.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/testing/TestMatrix.hpp>
extern "C" {
#include <pastix.h>
#include <spm.h>
}

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Pastix", "[pastix][direct][schur]"){

  using namespace maphys;

  using Vect = Vector<double>;
  using MatCOO = SparseMatrixCOO<double>;
  using MatCSC = SparseMatrixCSC<double>;
  using DenseMat = DenseMatrix<double>;
// Test header:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Direct solve][Direct solve:1]]
// A            X =  B
//--------------------
// 2  -  -  -   3    1
//-1  2  -  -   5    0
// 0 -1  2  - x 7 =  0
// 0  0 -1  2   9   11

MatCOO spA_coo({  0,    1,   1,    2,   2,    3,   3},
               {  0,    0,   1,    1,   2,    2,   3},
               {2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0},
               4, 4);
spA_coo.set_spd(MatrixStorage::lower);
MatCSC spA_csc = spA_coo.to_csc();

const Vect B{ 1., 0., 0., 11. };
const Vect X_exp{3., 5., 7., 9.};
const double test_tol = 1e-12;

SECTION("Pastix direct solve with IJV format - SPD matrix"){
  Pastix<MatCOO, Vect> solver_pastix(spA_coo);
  Vect X_found = solver_pastix * B;
  double direct_err = (X_found - X_exp).norm();
  REQUIRE(direct_err < test_tol);
}

SECTION("Pastix direct solve with CSC format - SPD matrix"){
  Pastix<MatCSC, Vect> solver_pastix(spA_csc);
  Vect X_found = solver_pastix * B;
  double direct_err = (X_found - X_exp).norm();
  REQUIRE(direct_err < test_tol);
}

SECTION("Pastix direct solve - LDLT"){
  IndexArray<int>    i{  0,    1,   1,    2,   2,    3,   3};
  IndexArray<int>    j{  0,    0,   1,    1,   2,    2,   3};
  IndexArray<double> v_sym{3.0, 3.0, 1.0, -1.0, 10.0, 2.0, 4.0};
  MatCSC sym_spA;
  build_matrix(sym_spA, 4, 4, 7, &i[0], &j[0], &v_sym[0]);
  sym_spA.set_property(MatrixSymmetry::symmetric, MatrixStorage::lower);
  Vect B_sym = sym_spA * X_exp;
  Pastix<MatCSC, Vect> solver(sym_spA);
  Vect X_found = solver * B_sym;
  double direct_err = (X_found - X_exp).norm();
  REQUIRE(direct_err < test_tol);
}

SECTION("Pastix direct solve - LU"){
  MatCSC ge_spA = test_matrix::general_matrix<double, MatCSC>().matrix;
  Vect B_ge = ge_spA * X_exp;
  Pastix<MatCSC, Vect> solver(ge_spA);
  Vect X_found = solver * B_ge;
  double direct_err = (X_found - X_exp).norm();
  REQUIRE(direct_err < test_tol);
}

SECTION("Pastix direct solve - LU - multiple RHS"){
  MatCSC ge_spA = test_matrix::general_matrix<double, MatCSC>().matrix;
  const DenseMat multiX_exp({3., 5., 7., 9.,
                             2., 1., 0., -1.}, 4, 2);
  DenseMat B_ge = ge_spA * multiX_exp;
  Pastix<MatCSC, DenseMat> solver(ge_spA);

  DenseMat X_found = solver * B_ge;
  double direct_err = (X_found - multiX_exp).norm();
  REQUIRE(direct_err < test_tol);
}

SECTION("Pastix direct solve - 2 thread (no binding)"){
  Pastix<MatCOO, Vect> solver_pastix;
  solver_pastix.set_n_threads(2);
  solver_pastix.setup(spA_coo);
  Vect X_found = solver_pastix * B;
  double direct_err = (X_found - X_exp).norm();
  REQUIRE(direct_err < test_tol);
}

SECTION("Pastix direct solve - 2 thread (with explicit binding)"){
  Pastix<MatCOO, Vect> solver_pastix;
  std::vector<int> bindtab{-1, 1};
  solver_pastix.set_n_threads(bindtab);
  solver_pastix.setup(spA_coo);
  Vect X_found = solver_pastix * B;
  double direct_err = (X_found - X_exp).norm();
  REQUIRE(direct_err < test_tol);
}

SECTION("Pastix - check multiple setup"){
  Pastix<MatCOO, Vect> solver_pastix;
  solver_pastix.setup(spA_coo);
  Vect X_found = solver_pastix * B;
  double direct_err = (X_found - X_exp).norm();
  REQUIRE(direct_err < test_tol);

  auto spA_cpy = spA_coo;
  solver_pastix.setup(spA_cpy);
  X_found = solver_pastix * B;
  direct_err = (X_found - X_exp).norm();
  REQUIRE(direct_err < test_tol);
}

SECTION("Triangular solves (SPD matrix)"){
  Pastix<MatCOO, Vect> solver_pastix;
  solver_pastix.setup(spA_coo);
  solver_pastix.set_verbose(2);
  spA_coo.display("A");

  // Perform the solve in two steps
  // 1-  y <- solve L . y = b
  // 2-  x <- solve L^T x = y
  // And compare x with x_exp use to generate b = A * x_exp
  const Vect x_exp{0,1,4,9};
  const Vect b = spA_coo * x_exp;
  b.display("b");

  auto y = solver_pastix.triangular_solve(b, MatrixStorage::lower, false);
  y.display("y");

  auto x = solver_pastix.triangular_solve(y, MatrixStorage::lower, true);
  x.display("x");
  double direct_err_x = (x - x_exp).norm();
  REQUIRE(direct_err_x < test_tol);
}

SECTION("Triangular solves (general matrix)"){
  Pastix<MatCOO, Vect> solver_pastix;
  MatCOO ge_spA(test_matrix::general_matrix<double, MatCOO>().matrix);
  solver_pastix.setup(ge_spA);
  solver_pastix.set_verbose(2);
  ge_spA.display("A");

  // Perform the solve in two steps
  // 1-  y <- solve L . y = b
  // 2-  x <- solve U x = y
  // And compare x with x_exp use to generate b = A * x_exp
  const Vect x_exp{0,1,4,9};
  const Vect b = ge_spA * x_exp;
  b.display("b");

  auto y = solver_pastix.triangular_solve(b, MatrixStorage::lower, false);
  y.display("y");

  auto x = solver_pastix.triangular_solve(y, MatrixStorage::upper, false);
  x.display("x");
  double direct_err_x = (x - x_exp).norm();
  REQUIRE(direct_err_x < test_tol);
}
// Direct solve:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Solving passing by the Schur complement][Solving passing by the Schur complement:1]]
// Solving with Schur complement
  //S = Schur complement of A on (1, 3):
  // S        x Y = f
  //------------------------------------
  //   1  -1/2    5   1/2
  // -1/2  3/2  x 9 = 11

  SECTION("Pastix solve with Schur complement"){
    Pastix<MatCOO, Vect> schur_pastix(spA_coo);

    IndexArray<int> schurlist{1, 3};

    DenseMat schur_mat = schur_pastix.get_schur(schurlist);
    schur_mat.display("Schur complement (1, 3)");

    DenseMat schur_exp({1, -0.5, -0.5, 1.5}, 2, 2);
    schur_exp.set_property(MatrixSymmetry::symmetric);
    double schur_diff = (schur_mat - schur_exp).norm();
    REQUIRE(schur_diff < test_tol);

    Vect f = schur_pastix.b2f(B);
    const Vect f_exp{1./2., 11.};
    double f_diff = (f - f_exp).norm();
    //f.display("f");
    REQUIRE(f_diff < test_tol);

    ConjugateGradient<DenseMat, Vect> cg_on_schur(schur_mat);
    cg_on_schur.setup(parameters::max_iter{10});
    Vect y = cg_on_schur * f;
    const Vect y_exp{5., 9.};
    double y_diff = (y - y_exp).norm();
    //y.display("y");
    REQUIRE( y_diff < test_tol );

    Vect X = schur_pastix.y2x(y);
    //X.display("X");
    //X_exp.display("Expected solution");

    double direct_err = (X - X_exp).norm();
    std::cout << "|| X - X_exp ||_2 : " << direct_err << '\n';
    REQUIRE(direct_err < test_tol);
  }

  SECTION("Pastix solve with Schur complement, different schurlist"){
    Pastix<MatCOO, Vect> schur_pastix(spA_coo);
    auto get_result = [&](const std::vector<int>& l_schurlist){
                        DenseMat schur_mat = schur_pastix.get_schur(IndexArray<int>(l_schurlist));
                        Vect f = schur_pastix.b2f(B);
                        ConjugateGradient<DenseMat, Vect> cg_on_schur(schur_mat);
                        cg_on_schur.setup(parameters::max_iter{10});
                        Vect y = cg_on_schur * f;
                        return schur_pastix.y2x(y);
                      };

    double direct_err;

    std::vector<std::vector<int>> schurlists{{0, 1}, {1, 2}, {2, 3}, {0, 1}, {1, 2}, {2, 1}, {0, 3}, {3, 1}, {1, 2, 3}, {0, 2, 3}};
    for(const auto& s : schurlists){
      Vect X = get_result(s);
      direct_err = (X - X_exp).norm();
      REQUIRE(direct_err < test_tol);
    }
  }
} // TEST_CASE
// Solving passing by the Schur complement:1 ends here

// [[file:../../../org/maphys/solver/Pastix.org::*Test with low-rank][Test with low-rank:1]]
TEST_CASE("Pastix low rank", "[pastix][direct][low_rank]"){
  using namespace maphys;
  using Scalar = double;

  // This matrix is too small to get any gain from blr...
  auto get_A = [](){
                 SparseMatrixCOO<Scalar> M;
                 M.from_matrix_market_file("@MAPHYSPP_MATRICES_DIR@/bcsstk17.mtx");
                 M.set_spd(MatrixStorage::lower);
                 return M;
               };

  const SparseMatrixCOO<Scalar> A(get_A());
  const auto m = A.get_n_rows();
  Vector<Scalar> X_exp(m);
  std::iota(&X_exp[0], &X_exp[m], Scalar{1.0});
  const Vector<Scalar> B = A * X_exp;

  const double compress_tol = 1.0e-3;
  Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>> pastix;
  pastix.set_verbose(2);
  pastix.set_low_rank(compress_tol);
  pastix.setup(A);

  auto X = pastix * B;

  auto dir_err = (X - X_exp).norm();
  auto b_e = (B - A * X).norm() / B.norm();

  std::cout << "Asked for compression tolerance: " << compress_tol << '\n';
  std::cout << "Direct error: " << dir_err << '\n';
  std::cout << "Backward error: " << b_e << '\n';
} // TEST_CASE
// Test with low-rank:1 ends here
