// [[file:../../../org/maphys/solver/GMRES.org::*Compression][Compression:1]]
#include <maphys.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <maphys/solver/GMRES.hpp>

using namespace maphys;

TEST_CASE("GMRES", "[GMRES][iterative][compression]"){
  using Scalar = std::complex<double>;

  const std::string mat_path = "../../../../matrices/young1c.mtx";

  SparseMatrixCSC<Scalar> A;
  A.from_matrix_market_file(mat_path);
  const Size M = n_rows(A);
  Vector<Scalar> x_exp(M);
  for(Size k = 0; k < M; ++k) x_exp[k] = 1;

  Vector<Scalar> b = A * x_exp;

  SZ_compressor_init();

  GMRES<SparseMatrixCSC<Scalar>, Vector<Scalar>> gmres(A);
  gmres.setup(parameters::tolerance<double>{1e-6},
              parameters::max_iter<int>{300},
              parameters::restart<int>{300},
              parameters::compression_accuracy<double>{1e-6},
              parameters::orthogonalization<Ortho>{Ortho::MGS2},
              parameters::verbose<bool>{true});

  Vector<Scalar> x = gmres * b;

  double diffnorm = (x - x_exp).norm();

  std::cout << "|| x* - x || = " << diffnorm << '\n';

  REQUIRE(gmres.get_n_iter() > 0);

  SZ_compressor_finalize();
}
// Compression:1 ends here
