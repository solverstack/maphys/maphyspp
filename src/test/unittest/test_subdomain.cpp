// [[file:../../../org/maphys/dist/Subdomain.org::*Test][Test:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include "maphys/dist/Subdomain.hpp"
#include "maphys/dist/Process.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("subdomain", "[subdomain][distributed]"){
  using namespace maphys;

  const int n_dofs = 4;

  using Nei_map = std::map<int, IndexArray<int>>;

  // SD 1
  Nei_map NM0 {{1, {2, 3}},
               {2, {0, 3}}};

  // SD 2
  Nei_map NM1 {{0, {1, 0}},
               {2, {0, 3}}};

  // SD 3
  Nei_map NM2 {{0, {2, 3}},
               {1, {3, 0}}};

  std::vector<Subdomain> sd;
  sd.emplace_back(0, n_dofs, std::move(NM0), false);
  sd.emplace_back(1, n_dofs, std::move(NM1), false);
  sd.emplace_back(2, n_dofs, std::move(NM2), false);

  std::shared_ptr<Process> p = bind_subdomains(static_cast<int>(sd.size()));
  p->load_subdomains(sd);

  MMPI::ordered_display(*p);

  p->check_subdomains_part_of_unity();

  auto ones = [&p](const size_t size){
    std::map<int, IndexArray<double>> values;
    if(p->owns_subdomain(0)) values[0] = IndexArray<double>(size, 1.0);
    if(p->owns_subdomain(1)) values[1] = IndexArray<double>(size, 1.0);
    if(p->owns_subdomain(2)) values[2] = IndexArray<double>(size, 1.0);
    return values;
  };

  SECTION("Assembly 2-way sum"){
    // Sum
    auto values = ones(4);
    std::map<int, IndexArray<double>> multiplicity;
    if(p->owns_subdomain(0)) multiplicity[0] = IndexArray<double>{2.0, 1.0, 2.0, 3.0};
    if(p->owns_subdomain(1)) multiplicity[1] = IndexArray<double>{3.0, 2.0, 1.0, 2.0};
    if(p->owns_subdomain(2)) multiplicity[2] = IndexArray<double>{2.0, 1.0, 2.0, 3.0};

    p->subdomain_assemble<double>(values, false);
    if(p->owns_subdomain(0)) REQUIRE( multiplicity[0] == values[0]);
    if(p->owns_subdomain(1)) REQUIRE( multiplicity[1] == values[1]);
    if(p->owns_subdomain(2)) REQUIRE( multiplicity[2] == values[2]);
  }

  SECTION("Assembly 2-way sum - interface"){
    // Sum
    auto values = ones(3);
    std::map<int, IndexArray<double>> multiplicity;
    if(p->owns_subdomain(0)) multiplicity[0] = IndexArray<double>{2.0, 2.0, 3.0};
    if(p->owns_subdomain(1)) multiplicity[1] = IndexArray<double>{3.0, 2.0, 2.0};
    if(p->owns_subdomain(2)) multiplicity[2] = IndexArray<double>{2.0, 2.0, 3.0};

    p->subdomain_assemble<double>(values, true);
    if(p->owns_subdomain(0)) REQUIRE( multiplicity[0] == values[0]);
    if(p->owns_subdomain(1)) REQUIRE( multiplicity[1] == values[1]);
    if(p->owns_subdomain(2)) REQUIRE( multiplicity[2] == values[2]);
  }

  SECTION("Disassembly"){
    std::map<int, IndexArray<double>> part_of_u;
    if(p->owns_subdomain(0)) part_of_u[0] = IndexArray<double>{1.0, 1.0, 1.0, 1.0};
    if(p->owns_subdomain(1)) part_of_u[1] = IndexArray<double>{0.0, 0.0, 1.0, 1.0};
    if(p->owns_subdomain(2)) part_of_u[2] = IndexArray<double>{0.0, 1.0, 0.0, 0.0};

    auto values = ones(4);
    p->disassemble(values);
    if(p->owns_subdomain(0)) REQUIRE( part_of_u[0] == values[0]);
    if(p->owns_subdomain(1)) REQUIRE( part_of_u[1] == values[1]);
    if(p->owns_subdomain(2)) REQUIRE( part_of_u[2] == values[2]);
  }

  SECTION("Owner values"){
    std::map<int, IndexArray<double>> initval;
    if(p->owns_subdomain(0)) initval[0] = IndexArray<double>{1.0, 2.0, 3.0, 4.0};
    if(p->owns_subdomain(1)) initval[1] = IndexArray<double>{5.0, 6.0, 7.0, 8.0};
    if(p->owns_subdomain(2)) initval[2] = IndexArray<double>{9.0, 10.0, 11.0, 12.0};

    std::map<int, IndexArray<double>> expected;
    if(p->owns_subdomain(0)) expected[0] = IndexArray<double>{1.0, 2.0, 3.0, 4.0};
    if(p->owns_subdomain(1)) expected[1] = IndexArray<double>{4.0, 3.0, 7.0, 8.0};
    if(p->owns_subdomain(2)) expected[2] = IndexArray<double>{8.0, 10.0, 1.0, 4.0};

    p->subdomain_assemble<double>(initval, false, Reduction::owner_value);
    if(p->owns_subdomain(0)) REQUIRE( expected[0] == initval[0]);
    if(p->owns_subdomain(1)) REQUIRE( expected[1] == initval[1]);
    if(p->owns_subdomain(2)) REQUIRE( expected[2] == initval[2]);
  }

  SECTION("Allreduce sum"){
    std::vector<double> values_tosum;
    if(p->owns_subdomain(0)) values_tosum.push_back(double{1.0});
    if(p->owns_subdomain(1)) values_tosum.push_back(double{10.0});
    if(p->owns_subdomain(2)) values_tosum.push_back(double{100.0});

    double sum = p->subdomain_allreduce(values_tosum);
    if(MMPI::rank() == 0) std::cout << "SUM: " << sum << '\n';
    if(p->is_master()){
      REQUIRE(sum == 111);
    }
  }
}
// Test:1 ends here
