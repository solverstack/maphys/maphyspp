// [[file:../../../org/maphys/part_data/PartMatrix.org::*Unit tests][Unit tests:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include "maphys/part_data/PartMatrix.hpp"
#include "maphys/testing/TestMatrix.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace maphys;

template<typename Matrix>
void test_assembled_matrix(Matrix& m, const int sd_id){
  const bool row_major = true;
  using Scalar = typename scalar_type<Matrix>::type;
  DenseMatrix<Scalar> m_dense;

  if constexpr(is_real<Scalar>::value){
    if(sd_id == 0){
      m_dense = DenseMatrix<Scalar>({15.0,  -1.0,   0.0,  -2.0,
				     -2.0,   6.0,  -2.0,  -1.0,
				      0.0,  -1.0,  14.0,  -3.0,
				     -4.0,  -1.0,  -4.0,  21.0}, 4, 4, row_major);
    }
    else if(sd_id == 1){

      m_dense = DenseMatrix<Scalar>({21.0,  -4.0,   0.0,  -2.0,
				     -3.0,  14.0,  -2.0,  -1.0,
				      0.0,  -1.0,   8.0,  -1.0,
				     -2.0,  -1.0,  -3.0,  14.0}, 4, 4, row_major);
    }
    else if(sd_id == 2){
      m_dense = DenseMatrix<Scalar>({14.0,  -1.0,   0.0,  -2.0,
				     -2.0,   6.0,  -2.0,  -1.0,
				      0.0,  -1.0,  15.0,  -2.0,
				     -2.0,  -1.0,  -4.0,  21.0}, 4, 4, row_major);
    }
  }
  else{
    if(sd_id == 0){
      m_dense = DenseMatrix<std::complex<double>>({{15.0,0.0}, {-1.0,-1.0},  {0.0,0.0}, {-2.0,0.0},
						   {-2.0,1.0},   {6.0,1.0}, {-2.0,3.0}, {-1.0,1.0},
						    {0.0,0.0},  {-1.0,0.0}, {14.0,1.0}, {-3.0,1.0},
						   {-4.0,3.0},  {-1.0,0.0}, {-4.0,0.0}, {21.0,-2.0}}, 4, 4, row_major);
    }
    else if(sd_id == 1){
      m_dense = DenseMatrix<std::complex<double>>({{21.0,-2.0}, {-4.0,0.0},  {0.0,0.0}, {-2.0,2.0},
						    {-3.0,1.0}, {14.0,1.0}, {-2.0,3.0}, {-1.0,1.0},
						     {0.0,0.0}, {-1.0,0.0},  {8.0,0.0}, {-1.0,0.0},
						    {-2.0,2.0}, {-1.0,0.0}, {-3.0,1.0}, {14.0,-1.0}}, 4, 4, row_major);
    }
    else if(sd_id == 2){
      m_dense = DenseMatrix<std::complex<double>>({{14.0,-1.0}, {-1.0,-1.0},  {0.0,0.0}, {-2.0,2.0},
						    {-2.0,1.0},   {6.0,1.0}, {-2.0,3.0}, {-1.0,1.0},
						     {0.0,0.0},  {-1.0,0.0}, {15.0,0.0}, {-2.0,0.0},
						    {-2.0,2.0},  {-1.0,0.0}, {-4.0,3.0}, {21.0,-2.0}}, 4, 4, row_major);
    }
  }
  m_dense.convert(m);
}

template<typename Scalar>
using DenseMat = DenseMatrix<Scalar, -1>;
TEMPLATE_PRODUCT_TEST_CASE("partmatrix", "[partmatrix][distributed]", (DenseMat, SparseMatrixCOO, SparseMatrixCSC), (double, std::complex<double>)){

  using LocMatrix = TestType;
  using Scalar = typename scalar_type<LocMatrix>::type;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();

  PartMatrix<LocMatrix> p_m = test_matrix::dist_spd_matrix<Scalar, LocMatrix>(p).matrix;
  PartMatrix<LocMatrix> dense_p_m = test_matrix::dist_spd_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
  PartVector<Vector<Scalar>> p_v = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;

  // Actually tests norms, partmatrix and distvector construction and conversion
  SECTION("PartMatrix construction"){
    if(p->is_master()){
      REQUIRE(p_m.norm() > 0);
      REQUIRE(p_m.norm() == dense_p_m.norm());
    }
  }

  //pmpv.display_local("p_m * p_v");
  SECTION("Distributed matrix vector product"){
    PartVector<Vector<Scalar>> pmpv = p_m * p_v;
    if(p->owns_subdomain(0)){
      Vector<Scalar> v({-13.0,-1.0,-2.0, 12.0}, 4, 1);
      Vector<Scalar> v_loc = pmpv.get_local_vector(0);
      REQUIRE(v == v_loc);
    }
    if(p->owns_subdomain(1)){
      Vector<Scalar> v({12.0,-2.0,5.0,17.0}, 4, 1);
      Vector<Scalar> v_loc = pmpv.get_local_vector(1);
      REQUIRE(v == v_loc);
    }
    if(p->owns_subdomain(2)){
      Vector<Scalar> v({17.0, 16.0, -13.0, 12.0}, 4, 1);
      Vector<Scalar> v_loc = pmpv.get_local_vector(2);
      REQUIRE(v == v_loc);
    }
    //pmpv.display_local("pmpv after assemble");
  }

  SECTION("Diagonal extraction"){
    PartVector<Vector<Scalar>> p_m_diag = p_m.diag_vect();
    p_m_diag.assemble();
    Vector<Scalar> p_m_diag_centr = p_m_diag.centralize();
    if( p->is_master()){
      Vector<Scalar> v_exp({6.0, 4.0, 7.0, 11.0, 3.0, 7.0, 4.0}, 7, 1);
      REQUIRE(p_m_diag_centr == v_exp);
    }
  }
  //p_m_diag.display_centralized("p_m_diag");

  std::function<LocMatrix (const SparseMatrixCOO<Scalar>&)> from_coo;
  if constexpr(std::is_same<LocMatrix, DenseMatrix<Scalar>>::value){
    from_coo = [](const SparseMatrixCOO<Scalar>& coo){ return coo.to_dense(); };
  }
  else if constexpr(std::is_same<LocMatrix, SparseMatrixCSC<Scalar>>::value){
    from_coo = [](const SparseMatrixCOO<Scalar>& coo){ return coo.to_csc(); };
  }
  else{
    from_coo = [](const SparseMatrixCOO<Scalar>& coo){ return coo; };
  }

    // Local to global indexing
  SECTION("Local to global indexing"){
    PartMatrix<SparseMatrixCOO<Scalar>> p_m_glob_coo = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;

    PartMatrix<LocMatrix> p_m_glob = p_m_glob_coo.convert(from_coo);
    p_m_glob.local_to_global();

    if(p->owns_subdomain(0)){
      SparseMatrixCOO<Scalar> m_exp({0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3},
				    {0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3},
				    {3, -1, -1, -1, 4, -1, -1, -1, 3, -1, -1, -1, -1, 4}, 7, 7);
      LocMatrix m_loc = p_m_glob.get_local_matrix(0);
      REQUIRE(from_coo(m_exp) == m_loc);
    }
    if(p->owns_subdomain(1)){
      SparseMatrixCOO<Scalar> m_exp({3, 3, 3, 2, 2, 2, 2, 4, 4, 4, 5, 5, 5, 5},
				    {3, 2, 5, 3, 2, 4, 5, 2, 4, 5, 3, 2, 4, 5},
				    {3, -1, -1, -1, 4, -1, -1, -1, 3, -1, -1, -1, -1, 4}, 7, 7);
      LocMatrix m_loc = p_m_glob.get_local_matrix(1);
      REQUIRE(from_coo(m_exp) == m_loc);
    }
    if(p->owns_subdomain(2)){
      SparseMatrixCOO<Scalar> m_exp({5, 5, 5, 6, 6, 6, 6, 0, 0, 0, 3, 3, 3, 3},
				    {5, 6, 3, 5, 6, 0, 3, 6, 0, 3, 5, 6, 0, 3},
				    {3, -1, -1, -1, 4, -1, -1, -1, 3, -1, -1, -1, -1, 4}, 7, 7);
      LocMatrix m_loc = p_m_glob.get_local_matrix(2);
      REQUIRE(from_coo(m_exp) == m_loc);
    }
  }

  SECTION("Centralization"){
    LocMatrix mat_exp = test_matrix::dist_general_matrix_global<Scalar, LocMatrix>();
    PartMatrix<LocMatrix> p_mm = test_matrix::dist_general_matrix<Scalar, LocMatrix>(p).matrix;
    LocMatrix p_mm_centr = p_mm.centralize(0);
    //if(MMPI::rank() == 0) p_mm_centr.to_dense().display("p_mm_centr");
    if(MMPI::rank() == 0) REQUIRE(mat_exp == p_mm_centr);
  }

  SECTION("Centralization on interface"){
    const SparseMatrixCOO<Scalar> m_loc_coo({0,1,2,0,1,2,0,1,2}, {0,0,0,1,1,1,2,2,2}, {1,1,1,1,1,1,1,1,1}, 3, 3);
    const LocMatrix m_loc(from_coo(m_loc_coo));
    PartMatrix<LocMatrix> p_mm(p, true);
    for(int i = 0; i < 3; ++i) { if(p->owns_subdomain(i)) p_mm.add_subdomain(i, m_loc); }

    LocMatrix p_mm_centr = p_mm.centralize(0);
    const SparseMatrixCOO<Scalar> m_exp_coo({0,1,2,3,0,1,2,3,0,1,2,3,0,1,2,3},
					    {0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3},
					    {2,1,2,1,1,2,2,1,2,2,3,2,1,1,2,2}, 4, 4);
    const LocMatrix m_exp(from_coo(m_exp_coo));

    //if(MMPI::rank() == 0) p_mm_centr.to_dense().display("p_mm_centr");
    if(MMPI::rank() == 0) REQUIRE(m_exp == p_mm_centr);
  }

  SECTION("Assembly"){
    PartMatrix<LocMatrix> p_mm = test_matrix::dist_general_matrix<Scalar, LocMatrix>(p).matrix;
    p_mm.assemble();

    for (int k = 0; k < 3; ++k){
      if(p->owns_subdomain(k)){
	LocMatrix m_exp;
	test_assembled_matrix(m_exp, k);
	LocMatrix m_loc = p_mm.get_local_matrix(k);

	REQUIRE(m_exp == m_loc);
      }
    }
  }

  SECTION("Assembly with upper storage"){

    DenseMatrix<Scalar> dm_loc({3,-1, 0,-1,
				0, 4,-1,-1,
				0, 0, 3,-1,
				0, 0, 0, 4}, 4, 4, true /*row_major*/);
    dm_loc.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    LocMatrix loc_mat;
    dm_loc.convert(loc_mat);
    PartMatrix<LocMatrix> p_mm(p);
    for (int k = 0; k < 3; ++k){
      if(p->owns_subdomain(k)){
	p_mm.add_subdomain(k, loc_mat);
      }
    }

    p_mm.assemble();

    for (int sd_id = 0; sd_id < 3; ++sd_id){
      if(p->owns_subdomain(sd_id)){
	DenseMatrix<Scalar> m_dense;
	if(sd_id == 0){
	  m_dense = DenseMatrix<Scalar>({ 6, -1,  0, -2,
					  0,  4, -1, -1,
					  0,  0,  7, -2,
					  0,  0,  0, 11}, 4, 4, true);
	}
	else if(sd_id == 1){
	  m_dense = DenseMatrix<Scalar>({11, -2,  0, -2,
					 0,  7, -1, -1,
					 0,  0,  3, -1,
					 0,  0,  0,  7}, 4, 4, true);
	}
	else if(sd_id == 2){
	  m_dense = DenseMatrix<Scalar>({ 7, -1,  0, -2,
					  0,  4, -1, -1,
					  0,  0,  6, -2,
					  0,  0,  0, 11}, 4, 4, true);
	}
	m_dense.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
	loc_mat = p_mm.get_local_matrix(sd_id);
	LocMatrix m_exp;
	m_dense.convert(m_exp);
	if(m_dense.get_n_rows() > 0){
	  REQUIRE(m_exp == loc_mat);
	}
      }
    }
  }

  SECTION("Transposition (adjoint matrix)"){
    PartMatrix<LocMatrix> p_mm = test_matrix::dist_general_matrix<Scalar, LocMatrix>(p).matrix;
    PartMatrix<LocMatrix> p_mm_adj = adjoint(p_mm);
    const LocMatrix mat_exp_adj = test_matrix::dist_general_matrix_global<Scalar, LocMatrix>();
    const LocMatrix mat_exp = adjoint(mat_exp_adj);

    LocMatrix p_mm_centr = p_mm_adj.centralize(0);
    //if(MMPI::rank() == 0) p_mm_centr.display("p_mm_centr");
    //if(MMPI::rank() == 0) mat_exp.display("mat_exp");
    if(MMPI::rank() == 0) REQUIRE(mat_exp == p_mm_centr);
  }

  SECTION("Identity"){
    PartMatrix<LocMatrix> id(p);
    id.identity();
    PartVector<Vector<Scalar>> v_ones = diagonal_as_vector(id);
    v_ones.assemble();
    PartVector<Vector<Scalar>> v_ones_check(p);
    v_ones_check.set_to_ones();
    REQUIRE((v_ones - v_ones_check).norm() < arithmetic_tolerance<Scalar>::value);
  }

  SECTION("Add Id on interface"){
    PartMatrix<LocMatrix> id(p);
    id.identity(/* on interface*/true, /*assembled*/true);

    DenseMatrix<Scalar> dm_loc({3,-1, 0,-1,
				0, 4,-1,-1,
				0, 0, 3,-1,
				0, 0, 0, 4}, 4, 4, true /*row_macjor*/);
    dm_loc.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    LocMatrix loc_mat;
    dm_loc.convert(loc_mat);
    PartMatrix<LocMatrix> p_mm(p);
    for (int k = 0; k < 3; ++k){
      if(p->owns_subdomain(k)){
	p_mm.add_subdomain(k, loc_mat);
      }
    }

    p_mm.add_on_interface(id);
    for (int sd_id = 0; sd_id < 3; ++sd_id){
      if(p->owns_subdomain(sd_id)){
	DenseMatrix<Scalar> m_dense;
	if(sd_id == 0){
	  m_dense = DenseMatrix<Scalar>({ 4, -1,  0, -1,
					  0,  4, -1, -1,
					  0,  0,  4, -1,
					  0,  0,  0,  5}, 4, 4, true);
	}
	else if(sd_id == 1){
	  m_dense = DenseMatrix<Scalar>({ 4, -1,  0, -1,
					  0,  5, -1, -1,
					  0,  0,  3, -1,
					  0,  0,  0,  5}, 4, 4, true);
	}
	else if(sd_id == 2){
	  m_dense = DenseMatrix<Scalar>({ 4, -1,  0, -1,
					  0,  4, -1, -1,
					  0,  0,  4, -1,
					  0,  0,  0,  5}, 4, 4, true);
	}
	m_dense.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
	loc_mat = p_mm.get_local_matrix(sd_id);
	LocMatrix m_exp;
	m_dense.convert(m_exp);
	if(m_dense.get_n_rows() > 0){
	  if(m_exp != loc_mat){
	    m_exp.display("m_exp");
	    loc_mat.display("loc_mat");
	  }
	  REQUIRE(m_exp == loc_mat);
	}
      }
    }
  }
}
// Unit tests:1 ends here
