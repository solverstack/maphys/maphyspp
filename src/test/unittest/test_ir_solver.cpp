// [[file:../../../org/maphys/solver/IRSolver.org::*Tests][Tests:1]]
#include <maphys.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/solver/IRSolver.hpp>
#include <maphys/solver/GMRES.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>

using namespace maphys;

TEST_CASE("IR solver", "[IR][iterative]"){
  using Scalar = double;
  //using Real = double;
  using Matrix = DenseMatrix<Scalar>;
  using Vect = Vector<Scalar>;

  using InnerSolver = GMRES<Matrix, Vect, DiagonalPrecond<Matrix, Vect>>;

  Matrix A = test_matrix::general_matrix<Scalar, Matrix>().matrix;
  Vect x_exp = test_matrix::simple_vector<Scalar, Vect>().vector;
  Vect b = A * x_exp;

  SECTION("Without exact solver"){
    using Solver = IRSolver<Matrix, Vect, InnerSolver>;

    Solver ir(A);
    auto& inner_s = ir.get_inner_solver();

    inner_s.setup(parameters::max_iter{10},
                  parameters::tolerance{1e-4},
                  parameters::verbose{true});
    inner_s.display("Inner solver");

    ir.setup(parameters::max_iter{5},
             parameters::tolerance{1e-8},
             parameters::verbose{true});

    Vect x = ir * b;

    x_exp.display("x_exp");
    x.display("x");

    REQUIRE((x - x_exp).norm() < 1e-6);
  }

  SECTION("With exact solver"){
    using ExactSolver = BlasSolver<Matrix, Vect>;
    using Solver = IRSolver<Matrix, Vect, InnerSolver, ExactSolver>;

    Solver ir(A);
    auto& inner_s = ir.get_inner_solver();
    auto& exact_s = ir.get_exact_solver();
    inner_s.display("Inner solver");
    exact_s.display("Exact solver");

    inner_s.setup(parameters::max_iter{10},
                  parameters::tolerance{1e-4},
                  parameters::verbose{true});

    ir.setup(parameters::max_iter{5},
             parameters::tolerance{1e-8},
             parameters::verbose{true});

    Vect x = ir * b;

    x_exp.display("x_exp");
    x.display("x");

    REQUIRE((x - x_exp).norm() < 1e-6);
  }
}
// Tests:1 ends here
