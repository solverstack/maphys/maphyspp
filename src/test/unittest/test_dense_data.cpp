// [[file:../../../org/maphys/loc_data/DenseData.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <complex>
#include <maphys.hpp>
#include <maphys/loc_data/DenseData.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace maphys;

TEMPLATE_TEST_CASE("Dense data", "[densedata]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;

  std::vector<Real> some_reals1{1.0, -1.0, 3.33, 0.0, -2.1, 12.0, 6.6, 6.8, 0.0, -23};
  std::vector<Scalar> scv(some_reals1.size());
  if constexpr(is_complex<Scalar>::value){
      std::vector<Real> some_reals2{1.0, 1.0, 2.33,  0.0, -8.1, 1.0, 6.6, 0.0, 0.0, 1.0};
      for(int i = 0; i < static_cast<int>(some_reals1.size()); ++i) scv[i] = some_reals1[i] + Scalar{0, 1} * some_reals2[i];
    }
  else{
    for(int i = 0; i < static_cast<int>(some_reals1.size()); ++i) scv[i] = static_cast<Scalar>(some_reals1[i]);
  }

  // Constructors

  SECTION("Constructor DenseData(m, n=1)"){
    DenseData<Scalar> vect_3(3);
    REQUIRE(vect_3.get_n_rows() == 3);
    REQUIRE(vect_3.get_n_cols() == 1);
    REQUIRE(vect_3.get_leading_dim() == 3);

    DenseData<Scalar> mat_3_2(4, 2);
    REQUIRE(mat_3_2.get_n_rows() == 4);
    REQUIRE(mat_3_2.get_n_cols() == 2);
    REQUIRE(mat_3_2.get_leading_dim() == 4);

    // Error if specifying n != static NbCol
    REQUIRE_THROWS(DenseData<Scalar, 3>(3, 2));

    DenseData<Scalar> v0;
    REQUIRE(v0.get_n_rows() == 0);
    REQUIRE(v0.get_n_cols() == 0);
  }

  SECTION("Constructor DenseData({1, 2, 3}, m = 0, n = 1)"){
    DenseData<Scalar> v123({scv[0], scv[1], scv[2]});
    for(int i = 0; i < 3; ++i) REQUIRE(v123.at(i) == scv[i]);
    DenseData<Scalar> v123_m3({scv[0], scv[1], scv[2]}, 3);
    for(int i = 0; i < 3; ++i) REQUIRE(v123_m3.at(i) == scv[i]);
    DenseData<Scalar, 2> m_3_2({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5]}, 3);
    REQUIRE(m_3_2.get_n_rows() == 3);
    REQUIRE(m_3_2.get_n_cols() == 2);
    DenseData<Scalar> m_3_2_dyn({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5]}, 3, 2);
    REQUIRE(m_3_2_dyn.get_n_rows() == 3);
    REQUIRE(m_3_2_dyn.get_n_cols() == 2);
    for(int j = 0; j < 2; ++j){
      for(int i = 0; i < 3; ++i){
	REQUIRE(m_3_2.at(i, j) == scv[j*3+i]);
	REQUIRE(m_3_2_dyn.at(i, j) == scv[j*3+i]);
      }
    }
  }

  SECTION("Constructor DenseData(m, * ptr, ld = 0)"){
    DenseData<Scalar, 1> v1(3, &scv[0]);
    REQUIRE(v1.get_n_rows() == 3);
    REQUIRE(v1.get_n_cols() == 1);
    REQUIRE(v1.get_leading_dim() == 3);

    DenseData<Scalar, 2> v2(3, &scv[0]);
    REQUIRE(v2.get_n_rows() == 3);
    REQUIRE(v2.get_n_cols() == 2);
    REQUIRE(v2.get_leading_dim() == 3);

    DenseData<Scalar, 2> v3(3, &scv[0], 4);
    REQUIRE(v3.get_n_rows() == 3);
    REQUIRE(v3.get_n_cols() == 2);
    REQUIRE(v3.get_leading_dim() == 4);
  }

  SECTION("Constructor DenseData(m, n, * ptr, ld = 0)"){
    DenseData<Scalar> m1(3, 2, &scv[0]);
    REQUIRE(m1.get_n_rows() == 3);
    REQUIRE(m1.get_n_cols() == 2);
    REQUIRE(m1.get_leading_dim() == 3);

    DenseData<Scalar> m2(3, 2, &scv[0], 4);
    REQUIRE(m2.get_n_rows() == 3);
    REQUIRE(m2.get_n_cols() == 2);
    REQUIRE(m2.get_leading_dim() == 4);

    REQUIRE_THROWS(DenseData<Scalar>(3, 2, &scv[0], 2));
    REQUIRE_THROWS(DenseData<Scalar, 3>(3, 2, &scv[0]));
  }

  SECTION("Copies"){
    DenseData<Scalar> m_base_weak(3, 2, &scv[0]);
    DenseData<Scalar> deep1 = m_base_weak.deepcopy();
    DenseData<Scalar> shal1 = m_base_weak.shallowcopy();
    REQUIRE(m_base_weak.get_ptr() == shal1.get_ptr());
    REQUIRE(m_base_weak.get_ptr() != deep1.get_ptr());
    DenseData<Scalar> m_base_strong(3, 2);
    DenseData<Scalar> deep2 = m_base_strong.deepcopy();
    DenseData<Scalar> shal2 = m_base_strong.shallowcopy();
    REQUIRE(m_base_strong.get_ptr() == shal2.get_ptr());
    REQUIRE(m_base_strong.get_ptr() != deep2.get_ptr());
  }

  SECTION("Move"){
    const DenseData<Scalar> v_ref({scv[0], scv[1], scv[2]}, 3);
    DenseData<Scalar> v(v_ref);
    DenseData<Scalar> vm(std::move(v));
    REQUIRE(vm == v_ref);
    REQUIRE(v.get_n_rows() == 0);
    REQUIRE(v.get_n_cols() == 0);
    REQUIRE(v.get_leading_dim() == 1);
    DenseData<Scalar> v2(v_ref);
    DenseData<Scalar> vm2 = std::move(v2);
    REQUIRE(vm2 == v_ref);
    REQUIRE(v2.get_n_rows() == 0);
    REQUIRE(v2.get_n_cols() == 0);
    REQUIRE(v2.get_leading_dim() == 1);
  }

  SECTION("Move with view"){
    const DenseData<Scalar> v_ref({scv[0], scv[1], scv[2]}, 3);
    DenseData<Scalar> v(3, 1, &scv[0]);
    DenseData<Scalar> vm(std::move(v));
    REQUIRE(vm == v_ref);
    REQUIRE(v.get_n_rows() == 0);
    REQUIRE(v.get_n_cols() == 0);
    REQUIRE(v.get_leading_dim() == 1);
    REQUIRE(vm.get_ptr() == &scv[0]);
    DenseData<Scalar> v2(3, 1, &scv[0]);
    DenseData<Scalar> vm2 = std::move(v2);
    REQUIRE(vm2 == v_ref);
    REQUIRE(v2.get_n_rows() == 0);
    REQUIRE(v2.get_n_cols() == 0);
    REQUIRE(v2.get_leading_dim() == 1);
    REQUIRE(vm2.get_ptr() == &scv[0]);
  }

  SECTION("Scalar multiplication"){
    DenseData<Scalar, 1> v{scv[0], scv[1], scv[2]};
    const Scalar s{2.0};
    v *= s;
    for(int i = 0; i < 3; ++i) REQUIRE(v.at(i) == (s * scv[i]));

    DenseData<Scalar> m({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5]}, 3, 2);
    m *= 2;
    for(int j = 0; j < 2; ++j){
      for(int i = 0; i < 3; ++i){
	REQUIRE(m.at(i, j) == scv[j*3+i] * s);
      }
    }
  }

  SECTION("Addition"){
    DenseData<Scalar, 1> v1{scv[0], scv[1], scv[2]};
    DenseData<Scalar, 1> v2{scv[3], scv[4], scv[5]};
    v1 += v2;
    for(int i = 0; i < 3; ++i) REQUIRE(v1.at(i) == scv[i] + scv[3+i]);

    DenseData<Scalar> m1({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5]}, 3, 2);
    DenseData<Scalar> m2({scv[1], scv[2], scv[3], scv[4], scv[5], scv[6]}, 3, 2);
    m1 -= m2;
    for(int j = 0; j < 2; ++j){
      for(int i = 0; i < 3; ++i){
	REQUIRE(m1.at(i, j) == (scv[j*3+i] - scv[j*3+i+1]));
      }
    }
  }

  SECTION("Dot, squared, norm"){
    Real tol = arithmetic_tolerance<Scalar>::value;
    DenseData<Scalar, 1> v1{scv[0], scv[1], scv[2]};
    DenseData<Scalar, 1> v2{scv[3], scv[4], scv[5]};

    Scalar dot_exp = conj(scv[0]) * scv[3] + conj(scv[1]) * scv[4] + conj(scv[2]) * scv[5];
    REQUIRE(std::abs(std::abs(v1.dot(v2)) - std::abs(dot_exp)) < tol);

    Real squared_exp = std::real(conj(scv[0]) * scv[0] + conj(scv[1]) * scv[1] + conj(scv[2]) * scv[2]);
    REQUIRE(std::abs(v1.squared() - std::abs(squared_exp)) < tol);

    Real norm_exp = std::real(std::sqrt(squared_exp));
    REQUIRE(std::abs(v1.norm() - norm_exp) < tol);
  }

  SECTION("Testing scal on a block (ld > m)"){
    // We multiply the 2x2 middle block of a 4x2 matrix by 2
    // 0 4     - -
    // 1 5     X X
    // 2 6 --> X X
    // 3 7     - -
    // with - outside of the block and X in the block
    const Scalar scal{2};
    DenseData<Scalar> mi({scv[0], scv[1], scv[2], scv[3], scv[4], scv[5], scv[6], scv[7]}, 4, 2);
    DenseData<Scalar> mf({scv[0], scal*scv[1], scal*scv[2], scv[3], scv[4], scal*scv[5], scal*scv[6], scv[7]}, 4, 2);

    DenseData<Scalar> block(2, 2, &mi(1, 0), 4);
    block *= scal;
    REQUIRE(mi == mf);
  }

  SECTION("Transposition"){
    DenseData<Scalar>       vect_test({1, 2, 3, 4, 5, 6}, 2, 3);
    const DenseData<Scalar> vect_orig({1, 2, 3, 4, 5, 6}, 2, 3);
    const DenseData<Scalar> vect_tr({1, 3, 5, 2, 4, 6}, 3, 2);

    vect_test.transpose();
    REQUIRE(vect_test == vect_tr);

    vect_test.transpose();
    REQUIRE(vect_test == vect_orig);
  }
}
// Tests:1 ends here
