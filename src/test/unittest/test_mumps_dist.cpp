// [[file:../../../org/maphys/solver/Mumps.org::*Header][Header:1]]
#include <iostream>

#include <maphys.hpp>
#include <maphys/solver/Mumps.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/testing/TestMatrix.hpp>

#include <catch2/catch_test_macros.hpp>
// Header:1 ends here

// [[file:../../../org/maphys/solver/Mumps.org::*Direct solve][Direct solve:1]]
TEST_CASE("Mumps distibuted", "[mumps][direct][distributed]"){

  using Scalar = double;
  using namespace maphys;
  std::shared_ptr<Process> p = test_matrix::get_distr_process();

  PartMatrix<SparseMatrixCOO<Scalar>> dm = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
  PartVector<Vector<Scalar>> dv = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;

  SECTION("Mumps distributed"){
    Mumps<decltype(dm), decltype(dv)> solver_mumps(dm);
    auto X = solver_mumps * dv;
    auto dmx = dm * X;
    dmx.display_centralized("A * X");
    dv.display_centralized("B");
    double res = (dm * X - dv).norm();
    REQUIRE(res < 1e-12);
  }
}
// Direct solve:1 ends here
