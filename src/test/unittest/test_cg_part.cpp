// [[file:../../../org/maphys/solver/ConjugateGradient.org::*Distributed][Distributed:1]]
#include <iostream>
#include <vector>

#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen_header.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMA
#include <maphys/wrappers/armadillo/Armadillo_header.hpp>
#endif
#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMA
#include <maphys/wrappers/armadillo/Armadillo.hpp>
#endif

#include <maphys.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "test_iterative_solver.hpp"
#include <maphys/part_data/PartMatrix.hpp>
using namespace maphys;

template<typename Scalar, typename LocMatrix>
void test(const std::string& mat_type){
  using Matrix = PartMatrix<LocMatrix>;
  using LocVect = typename vector_type<LocMatrix>::type;
  using Vect = PartVector<LocVect>;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();

  SECTION(std::string("ConjugateGradient, distributed, no preconditioner, matrix ") + mat_type){
    test_solver<Scalar, Matrix, Vect, ConjugateGradient<Matrix, Vect>>(
								       test_matrix::dist_spd_matrix<Scalar, LocMatrix>(p).matrix,
								       test_matrix::dist_vector<Scalar, LocVect>(p).vector);
  }

  SECTION(std::string("ConjugateGradient, distributed, diagonal preconditioner, matrix ") + mat_type){
    using PcdType = DiagonalPrecond<Matrix, Vect>;
    test_solver<Scalar, Matrix, Vect, ConjugateGradient<Matrix, Vect, PcdType>>(
										test_matrix::dist_spd_matrix<Scalar, LocMatrix>(p).matrix,
										test_matrix::dist_vector<Scalar, LocVect>(p).vector);
  }
}

TEMPLATE_TEST_CASE("ConjugateGradient", "[ConjugateGradient][iterative][distributed]", /*float,*/ double){
  using Scalar = TestType;

  test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
  test<Scalar, SparseMatrixCOO<Scalar>>("SparseMatrixCOO");
  test<Scalar, SparseMatrixCSC<Scalar>>("SparseMatrixCSC");
#ifdef MAPHYSPP_USE_EIGEN
  using EigenDenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
  using EigenSparseMatrix = Eigen::SparseMatrix<Scalar>;

  test<Scalar, EigenDenseMatrix>("Eigen dense");
  test<Scalar, EigenSparseMatrix>("Eigen sparse");
#endif
#ifdef MAPHYSPP_USE_ARMA
  test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
  test<Scalar, arma::SpMat<Scalar>>("Armadillo sparse");
#endif

} // TEMPLATE_PRODUCT_TEST_CASE
// Distributed:1 ends here
