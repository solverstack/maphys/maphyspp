// [[file:../../../org/maphys/utils/CrossProduct.org::*Tests][Tests:1]]
#include <maphys.hpp>
#include "maphys/utils/CrossProduct.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("CrossProduct", "[cross_product]"){

  using namespace maphys;

  SECTION("Basic cross product test"){
    IndexArray<int> v1{0, 2, 3};
    IndexArray<int> v2{4, 5};

    std::vector<std::pair<int,int>> vals({{0, 4},{0, 5},{2, 4},{2, 5},{3, 4},{3, 5}});
    std::size_t k = 0;

    for(auto [first, second] : CrossProduct(v1, v2)){
      REQUIRE(first == vals[k].first);
      REQUIRE(second == vals[k].second);
      k++;
    }
    REQUIRE(k == vals.size());
  }

  SECTION("0 iteration test (v2 size 0)"){
    IndexArray<int> v1{0, 2, 3};
    IndexArray<int> v2;
    int passed_in_the_loop = 0;
    for(auto cp : CrossProduct(v1, v2)){
      std::cout << cp.first << ", " << cp.second << '\n';
      passed_in_the_loop++;
    }
    REQUIRE(passed_in_the_loop == 0);
  }

  SECTION("0 iteration test (v1 size 0)"){
    IndexArray<int> v1;
    IndexArray<int> v2{0,1,2};
    int passed_in_the_loop = 0;
    for(auto cp : CrossProduct(v1, v2)){
      std::cout << cp.first << ", " << cp.second << '\n';
      passed_in_the_loop++;
    }
    REQUIRE(passed_in_the_loop == 0);
  }
}
// Tests:1 ends here
