// [[file:../../../org/maphys/solver/PartSchurSolver.org::*Test][Test:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <maphys/solver/PartSchurSolver.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>

#include <catch2/catch_test_macros.hpp>

using namespace maphys;

template<typename Matrix, typename Vect, typename SolverS, typename HybSolver>
Vect hybrid_solve(const Vect& B, const Matrix& K){
  HybSolver hybrid_solver;

  bool verbose = false;
  if(MMPI::rank() == 0) verbose = true;

  SolverS& iter_solver = hybrid_solver.get_solver_S();
  if constexpr(is_solver_iterative<SolverS>::value){
    iter_solver.setup(parameters::max_iter{10},
                      parameters::tolerance{1e-8},
                      parameters::verbose{verbose});
  }

  hybrid_solver.setup(parameters::A{K},
                      parameters::verbose{verbose});

  using SolverK = typename HybSolver::solver_on_K;
  if constexpr(is_solver_iterative<SolverK>::value){
    auto setup_solverk = [verbose](SolverK& solver){
                           solver.setup(parameters::max_iter{10},
                                        parameters::verbose{verbose});
                         };
    hybrid_solver.setup(parameters::setup_solver_K<std::function<void(SolverK&)>>{setup_solverk});
  }

  return hybrid_solver * B;
}

template<typename Scalar>
bool check_result(const PartVector<Vector<Scalar>>& X){
  using Real = typename arithmetic_real<Scalar>::type;

  X.display_centralized("X");

  std::shared_ptr<Process> p = X.get_proc();
  PartVector<Vector<Scalar>> X_expected = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
  PartVector<Vector<Scalar>> diff = X - X_expected;
  Real direct_diff_norm = diff.norm();

  std::cout << "||X_exp - X|| = " << direct_diff_norm << '\n';
  return (direct_diff_norm < Real{1e-6});
}

TEST_CASE("PartSchurSolver", "[CG][schur][distributed]"){

  using Scalar = double;
  using SpMat = PartMatrix<SparseMatrixCOO<Scalar>>;
  using DMat = PartMatrix<DenseMatrix<Scalar>>;
  using Vect = PartVector<Vector<Scalar>>;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();

  //const SpMat dA = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
  SpMat dA = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
  dA.apply_on_data([](SparseMatrixCOO<Scalar>& m){
                     m.to_triangular(MatrixStorage::lower);
                     m.set_spd(MatrixStorage::lower);
                     m.order_indices();
                   });

  Vect dX_expected = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
  const Vect dB = dA * dX_expected;

  dX_expected.display_centralized("dX_exp");
  dB.display_centralized("dB");

  SECTION("PartSchur( Pastix, CG ), explicit"){
    using Solver_K = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using Solver_S = ConjugateGradient<DMat, Vect>;
    using Solver = PartSchurSolver<SpMat, Vect, Solver_K, Solver_S>;

    Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
    REQUIRE(check_result(X));
  }

  SECTION("PartSchur( Pastix, CG + diag pcd ), explicit"){
    using Precond = DiagonalPrecond<DMat, Vect>;
    using Solver_K = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using Solver_S = ConjugateGradient<DMat, Vect, Precond>;
    using Solver = PartSchurSolver<SpMat, Vect, Solver_K, Solver_S>;

    Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
    REQUIRE(check_result(X));
  }

  SECTION("PartSchur( Pastix, CG + AS ), explicit"){
    using Precond = AdditiveSchwarz<DMat, Vect, Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>>;
    using Solver_K = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using Solver_S = ConjugateGradient<DMat, Vect, Precond>;
    using Solver = PartSchurSolver<SpMat, Vect, Solver_K, Solver_S>;

    Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
    REQUIRE(check_result(X));
  }

  SECTION("PartSchurSolver( Pastix, CG ), implicit"){
    using Solver_K = Pastix<SparseMatrixCOO<Scalar>,Vector<Scalar>>;
    using ImplSchur = ImplicitSchur<SparseMatrixCOO<Scalar>,Vector<Scalar>,Solver_K>;
    using Solver_S = ConjugateGradient<PartOperator<ImplSchur,SparseMatrixCOO<Scalar>,Vector<Scalar>>,Vect>;
    using Solver = PartSchurSolver<SpMat,Vect, Solver_K,Solver_S>;

    Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
    REQUIRE(check_result(X));
  }

  SECTION("PartSchurSolver( Pastix, CG + diag pcd ), implicit"){
    using Precond = DiagonalPrecond<PartMatrix<DenseMatrix<Scalar>>,Vect>;
    using Solver_K = Pastix<SparseMatrixCOO<Scalar>,Vector<Scalar>>;
    using ImplSchur = ImplicitSchur<SparseMatrixCOO<Scalar>,Vector<Scalar>,Solver_K>;
    using Solver_S = ConjugateGradient<PartOperator<ImplSchur,SparseMatrixCOO<Scalar>,Vector<Scalar>>,Vect, Precond>;
    using Solver = PartSchurSolver<SpMat,Vect, Solver_K,Solver_S>;

    Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
    REQUIRE(check_result(X));
  }

  SECTION("PartSchurSolver(CG, CG), implicit"){
    using Solver_K = ConjugateGradient<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
    using ImplSchurCG = ImplicitSchur<SparseMatrixCOO<Scalar>,Vector<Scalar>,Solver_K>;
    using Solver_S = ConjugateGradient<PartOperator<ImplSchurCG,SparseMatrixCOO<Scalar>,Vector<Scalar>>,Vect>;
    using Solver = PartSchurSolver<SpMat, Vect, Solver_K, Solver_S>;

    Vect X = hybrid_solve<SpMat, Vect, Solver_S, Solver>(dB, dA);
    REQUIRE(check_result(X));
  }
}
// Test:1 ends here
