// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Tests][Tests:1]]
#include <maphys.hpp>
#include <maphys/kernel/TlapackKernels.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace maphys;

TEMPLATE_TEST_CASE("TlapackKernels", "[dense][sequential][tlapack][kernel]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  //using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar, std::complex<Real>>::type;
// Tests:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Blas 1][Blas 1:1]]
SECTION("Blas 1 - SCAL"){
  const Vector<Scalar> x({1, -1, 3});
  const DenseMatrix<Scalar> mat({1, 2, 3, 4}, 2, 2);    

  // Scal (*2)
  const Vector<Scalar> x_scal({2, -2, 6});

  Vector<Scalar> x2(x);
  tlapack_kernels::scal(x2, Scalar{2});
  REQUIRE(x2 == x_scal);

  // Test with increment
  DenseMatrix<Scalar> mat2(mat);
  Vector<Scalar> y = mat2.get_row_view(0);

  const Vector<Scalar> y_scal({2, 6});
  tlapack_kernels::scal(y, Scalar{2});

  REQUIRE(y == y_scal);
}

SECTION("Blas 1 - AXPY"){
  const Vector<Scalar> x({1, -1, 3});

  // AXPY (*2 + (-1,-1,-1)T)
  Vector<Scalar> y({-1, -1, -1});
  const Vector<Scalar> x2py({1, -3, 5});
  tlapack_kernels::axpy(x, y, Scalar{2});
  REQUIRE(y == x2py);
}

// dot
SECTION("Blas 1 - DOT"){
  const Vector<Scalar> x({1, -1, 3});

  const Vector<Scalar> y({2, -4, -1});
  const Scalar xdoty{3};
  REQUIRE(tlapack_kernels::dot(x, y) == xdoty);
}

// norm2
SECTION("Blas 1 - NORM2"){
  const Vector<Scalar> x({1, -1, 3});

  const Real normx_sq = 11;
  const Real normx = std::sqrt(normx_sq);
  REQUIRE(tlapack_kernels::norm2_squared(x) == normx_sq);
  REQUIRE(tlapack_kernels::norm2(x) == normx);
}
// Blas 1:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Blas 2][Blas 2:1]]
SECTION("Blas 2 -GEMV"){
  //static void gemv(const Matrix& A, const Vector& X, Vector& Y, Scalar alpha = 1, Scalar beta = 0)

  // 1 4        3
  // 2 5 x -1 = 3
  // 3 6    1   3
  const DenseMatrix<Scalar> MA({1, 2, 3, 4, 5, 6}, 3, 2);
  const Vector<Scalar> x({-1, 1});
  const Vector<Scalar> sol_check({3, 3, 3});
  Vector<Scalar> y(maphys::n_rows(MA));

  tlapack_kernels::gemv(MA, x, y);

  REQUIRE(sol_check == y);
}

SECTION("TLAPACK 2 - Gemv sym/herm"){
  Vector<Scalar> X({3, 2, 1});
  DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
  L.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

  Vector<Scalar> LX({-2, 5, -2});
  if constexpr(maphys::is_complex<Scalar>::value){
    X += Scalar{0, 1} * Vector<Scalar>({1, 2, -1});
    L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
    L.set_property(maphys::MatrixSymmetry::hermitian);
    LX = Vector<Scalar>({{-5, 7}, {8, -2}, {1, -7}});
  }

  Vector<Scalar> my_LX(3);
  tlapack_kernels::gemv(L, X, my_LX);
  REQUIRE(my_LX == LX);
}
// Blas 2:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Blas 3][Blas 3:1]]
SECTION("Blas 3 - GEMM - general"){
  const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
  const DenseMatrix<Scalar> B({1.0,  2.0, 3.0,  4.0, 5.0,  6.0}, 3, 2);

  //            1 4
  //  1  2  3 x 2 5 =  14  32
  // -1 -2 -3   3 6   -14 -32

  const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
  DenseMatrix<Scalar> C(maphys::n_rows(A), maphys::n_cols(B));

  tlapack_kernels::gemm(A, B, C);
  REQUIRE(C == C_check);
}

SECTION("Blas 3 - GEMM - symmetric / hermitian"){
  DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
  DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
  L.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

  DenseMatrix<Scalar> ML({-14,-16,-18, 5, 7, 9, 36, 39, 42}, 3,3);
  DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
  if constexpr(maphys::is_complex<Scalar>::value){
    M += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 1,
	-1, 2, -2,
	1, -1, -0.5}, 3, 3);
    L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
    L.set_property(maphys::MatrixSymmetry::hermitian);
    ML = DenseMatrix<Scalar>({{-16, -3}, {-13, 11}, {-19.5, -0.5}, {6, -9}, {6, -19}, {7, -11.5}, {39, 8}, {34, 8}, {47, 1.5}}, 3, 3);
    LM = DenseMatrix<Scalar>({{-4, -7}, {2, 9}, {11, -2}, {-16, 10}, {12, 2}, {28, -11}, {-17.5, -2.5}, {17, 13.5}, {29, -15.5}}, 3, 3);
  }

  DenseMatrix<Scalar> my_ML(3, 3);
  tlapack_kernels::gemm(M, L, my_ML);
  REQUIRE(my_ML == ML);

  DenseMatrix<Scalar> my_LM(3, 3);
  tlapack_kernels::gemm(L, M, my_LM);

  REQUIRE(my_LM == LM);
}
// Blas 3:1 ends here

// [[file:../../../org/maphys/kernel/TlapackKernels.org::*Footer][Footer:1]]
}
// Footer:1 ends here
