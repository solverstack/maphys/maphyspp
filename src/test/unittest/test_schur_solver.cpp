// [[file:../../../org/maphys/solver/SchurSolver.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <maphys/solver/SchurSolver.hpp>
#include <maphys/solver/ImplicitSchur.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>

#include <catch2/catch_test_macros.hpp>

using namespace maphys;

template<typename Matrix, typename Vect, typename SolverS, typename HybSolver>
Vect hybrid_solve(const Vect& B, const Matrix& K, const bool stop_crit_b = true){
  const IndexArray<int> interface{0, 3};
  HybSolver hybrid_solver;

  SolverS& iter_solver = hybrid_solver.get_solver_S();
  if constexpr(is_solver_iterative<SolverS>::value){
      iter_solver.setup(parameters::max_iter{10}, 
                        parameters::verbose{true});

    }

  hybrid_solver.setup(parameters::A{K},
                      parameters::schurlist{interface},
                      parameters::verbose{true},
                      parameters::stop_crit_on_b<bool>{stop_crit_b});

  using SolverK = typename HybSolver::solver_on_K;
  if constexpr(is_solver_iterative<SolverK>::value){
    auto setup_solverk = [](SolverK& solver){
                           solver.setup(parameters::max_iter{10},
                                        parameters::verbose{true});
                         };

    hybrid_solver.setup(parameters::setup_solver_K<std::function<void(SolverK&)>>{setup_solverk});
  }

  return hybrid_solver * B;
}

template<typename Scalar>
bool check_result(const Vector<Scalar>& X){
  using Real = typename arithmetic_real<Scalar>::type;

  X.display("X");

  const Vector<Scalar> X_expected{3.0, 5.0, 7.0, 9.0, 11.0};
  Vector<Scalar> diff = X - X_expected;
  Real direct_diff_norm = diff.norm();

  std::cout << "||X_exp - X|| = " << direct_diff_norm << '\n';
  return (direct_diff_norm < Real{1e-6});
}

TEST_CASE("SchurSolver", "[CG][schur][implicit][explicit]"){

  using Scalar = double;
  using SpMat = SparseMatrixCOO<Scalar>;
  using DenseMat = DenseMatrix<Scalar>;
  using Vect = Vector<Scalar>;
  using Solver_K = Pastix<SpMat, Vect>;
  using ImplSchur = ImplicitSchur<SpMat, Vect, Solver_K>;

  // Solving a 5x5 1D-laplacan
  // Find X in AX = B system:
  //  2 -1  0  0  0    3     1
  // -1  2 -1  0  0    5     0
  //  0 -1  2 -1  0  x 7  =  0
  //  0  0 -1  2 -1    9     0
  //  0  0  0 -1  2   11    13

  const int m = 5;
  const Vect B{1.0, 0.0, 0.0, 0.0, 13.0};

  SpMat spA = maphys::test_matrix::laplacian_1D_matrix<Scalar, SpMat>(m).matrix;
  spA.to_triangular(MatrixStorage::lower);

  SECTION("SchurSolver(Pastix, CG), explicit"){
    using CGType = ConjugateGradient<DenseMat, Vect>;
    using HybSolver = SchurSolver<SpMat, Vect, Solver_K, CGType>;

    Vect X = hybrid_solve<SpMat, Vect, CGType, HybSolver>(B, spA);
    REQUIRE(check_result(X));
  }

  SECTION("SchurSolver(Pastix, CG), explicit, stop crit on b = false"){
    using CGType = ConjugateGradient<DenseMat, Vect>;
    using HybSolver = SchurSolver<SpMat, Vect, Solver_K, CGType>;

    Vect X = hybrid_solve<SpMat, Vect, CGType, HybSolver>(B, spA, false);
    REQUIRE(check_result(X));
  }

  SECTION("SchurSolver(Pastix, CG + diag_pcd), explicit"){
    using Precond = DiagonalPrecond<DenseMatrix<Scalar>, Vect>;
    using CGType = ConjugateGradient<DenseMatrix<Scalar>, Vect, Precond>;
    using HybSolver = SchurSolver<SpMat, Vect, Solver_K, CGType>;

    Vect X = hybrid_solve<SpMat, Vect, CGType, HybSolver>(B, spA);
    REQUIRE(check_result(X));
  }

  SECTION("SchurSolver(Pastix, Blas), explicit"){
    using BlasSolv = BlasSolver<DenseMat, Vect>;
    using HybSolver = SchurSolver<SpMat, Vect, Solver_K, BlasSolv>;

    Vect X = hybrid_solve<SpMat, Vect, BlasSolv, HybSolver>(B, spA);

    REQUIRE(check_result(X));
  }

  SECTION("SchurSolver(Pastix, CG), implicit"){
    using CGType = ConjugateGradient<ImplSchur, Vect>;
    using HybSolver = SchurSolver<SpMat, Vect, Solver_K, CGType>;

    Vect X = hybrid_solve<SpMat, Vect, CGType, HybSolver>(B, spA);
    REQUIRE(check_result(X));
  }

  SECTION("SchurSolver(Pastix, CG + diag pcd), implicit"){
    using Precond = DiagonalPrecond<DenseMatrix<Scalar>, Vect>;
    using CGType = ConjugateGradient<ImplSchur, Vect, Precond>;
    using HybSolver = SchurSolver<SpMat, Vect, Solver_K, CGType>;

    Vect X = hybrid_solve<SpMat, Vect, CGType, HybSolver>(B, spA);
    REQUIRE(check_result(X));
  }

  SECTION("SchurSolver(CG, CG), implicit"){
    using CG_K = ConjugateGradient<SpMat, Vect>;
    using ImplSchurCG = ImplicitSchur<SpMat, Vect, CG_K>;
    using CG_S = ConjugateGradient<ImplSchurCG, Vect>;
    using HybSolver = SchurSolver<SpMat, Vect, CG_K, CG_S>;

    Vect X = hybrid_solve<SpMat, Vect, CG_S, HybSolver>(B, spA);
    REQUIRE(check_result(X));
  }
}
// Tests:1 ends here
