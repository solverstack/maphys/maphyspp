// [[file:../../../org/maphys/kernel/BlasKernels.org::*Tests][Tests:1]]
#include <maphys.hpp>
#include <maphys/kernel/BlasKernels.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace maphys;

TEMPLATE_TEST_CASE("BlasKernels", "[dense][sequential][blas][kernel]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  using Complex = typename arithmetic_complex<Scalar>::type;

  constexpr const Real ev_tol = (is_precision_double<Scalar>::value) ? 1e-12 : 1e-5;
// Tests:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Blas 1][Blas 1:1]]
SECTION("Blas 1 - SCAL"){
  const Vector<Scalar> x({1, -1, 3});
  const DenseMatrix<Scalar> mat({1, 2, 3, 4}, 2, 2);

  // Scal (*2)
  const Vector<Scalar> x_scal({2, -2, 6});

  Vector<Scalar> x2(x);
  blas_kernels::scal(x2, Scalar{2});
  REQUIRE(x2 == x_scal);

  // Test with increment
  DenseMatrix<Scalar> mat2(mat);
  Vector<Scalar> y = mat2.get_row_view(0);

  const Vector<Scalar> y_scal({2, 6});
  blas_kernels::scal(y, Scalar{2});

  REQUIRE(y == y_scal);
}

SECTION("Blas 1 - AXPY"){
  const Vector<Scalar> x({1, -1, 3});

  // AXPY (*2 + (-1,-1,-1)T)
  Vector<Scalar> y({-1, -1, -1});
  const Vector<Scalar> x2py({1, -3, 5});
  blas_kernels::axpy(x, y, Scalar{2});
  REQUIRE(y == x2py);
}

// dot
SECTION("Blas 1 - DOT"){
  const Vector<Scalar> x({1, -1, 3});

  const Vector<Scalar> y({2, -4, -1});
  const Scalar xdoty{3};
  REQUIRE(blas_kernels::dot(x, y) == xdoty);
}

// norm2
SECTION("Blas 1 - NORM2"){
  const Vector<Scalar> x({1, -1, 3});

  const Real normx_sq = 11;
  const Real normx = std::sqrt(normx_sq);
  REQUIRE(blas_kernels::norm2_squared(x) == normx_sq);
  REQUIRE(blas_kernels::norm2(x) == normx);
}
// Blas 1:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Blas 2][Blas 2:1]]
SECTION("Blas 2 -GEMV"){
  // 1 4        3
  // 2 5 x -1 = 3
  // 3 6    1   3
  const DenseMatrix<Scalar> MA({1, 2, 3, 4, 5, 6}, 3, 2);
  const Vector<Scalar> x({-1, 1});
  const Vector<Scalar> sol_check({3, 3, 3});
  Vector<Scalar> y(maphys::n_rows(MA));

  blas_kernels::gemv(MA, x, y);

  REQUIRE(sol_check == y);
}

SECTION("BLAS 2 - Gemv sym/herm"){
  Vector<Scalar> X({3, 2, 1});
  DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
  L.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

  Vector<Scalar> LX({-2, 5, -2});
  if constexpr(maphys::is_complex<Scalar>::value){
    X += Scalar{0, 1} * Vector<Scalar>({1, 2, -1});
    L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
    L.set_property(maphys::MatrixSymmetry::hermitian);
    LX = Vector<Scalar>({{-5, 7}, {8, -2}, {1, -7}});
  }

  Vector<Scalar> my_LX(3);
  blas_kernels::gemv(L, X, my_LX);
  REQUIRE(my_LX == LX);
}
// Blas 2:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Blas 3][Blas 3:1]]
SECTION("Blas 3 - GEMM - general"){
  const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
  const DenseMatrix<Scalar> B({1.0,  2.0, 3.0,  4.0, 5.0,  6.0}, 3, 2);

  //            1 4
  //  1  2  3 x 2 5 =  14  32
  // -1 -2 -3   3 6   -14 -32

  const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
  DenseMatrix<Scalar> C(maphys::n_rows(A), maphys::n_cols(B));

  blas_kernels::gemm(A, B, C);
  REQUIRE(C == C_check);
}

SECTION("Blas 3 - GEMM - symmetric / hermitian"){
  DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
  DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
  L.set_property(maphys::MatrixStorage::lower, maphys::MatrixSymmetry::symmetric);

  DenseMatrix<Scalar> ML({-14,-16,-18, 5, 7, 9, 36, 39, 42}, 3,3);
  DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
  if constexpr(maphys::is_complex<Scalar>::value){
    M += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 1,
	-1, 2, -2,
	1, -1, -0.5}, 3, 3);
    L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
    L.set_property(maphys::MatrixSymmetry::hermitian);
    ML = DenseMatrix<Scalar>({{-16, -3}, {-13, 11}, {-19.5, -0.5}, {6, -9}, {6, -19}, {7, -11.5}, {39, 8}, {34, 8}, {47, 1.5}}, 3, 3);
    LM = DenseMatrix<Scalar>({{-4, -7}, {2, 9}, {11, -2}, {-16, 10}, {12, 2}, {28, -11}, {-17.5, -2.5}, {17, 13.5}, {29, -15.5}}, 3, 3);
  }

  DenseMatrix<Scalar> my_ML(3, 3);
  blas_kernels::gemm(M, L, my_ML);
  REQUIRE(my_ML == ML);

  DenseMatrix<Scalar> my_LM(3, 3);
  blas_kernels::gemm(L, M, my_LM);

  REQUIRE(my_LM == LM);
}
// Blas 3:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Eigenvalue decomposition][Eigenvalue decomposition:1]]
SECTION("Eigenvalue decomposition, general"){
  const DenseMatrix<Scalar> A({1, 2, 3, 4, -3, -13, 7, 8, 3.5}, 3, 3);
  // With this matrix: eigenvalues are
  //0.659471
  //0.420265 + i. 8.0168
  //0.420265 + i. -8.0168

  auto [lambdas, U] = blas_kernels::eig(A);

  auto A_cplx = A.template cast<Complex>();

  for(Size i = 0; i < n_rows(A); ++i){
    Vector<Complex> AU = A_cplx * U.get_vect_view(i);
    Vector<Complex> lamU = lambdas[i] * U.get_vect_view(i);
    REQUIRE((AU - lamU).norm() < (ev_tol * 10));
  }

  auto mus = blas_kernels::eigvals(A);

  for(Size i = 0; i < n_rows(A); ++i){
    REQUIRE(std::abs(lambdas[i] - mus[i]) < ev_tol);
  }
}

SECTION("Eigenvalue decomposition, symmetric/hermitian case"){
  DenseMatrix<Scalar> A({3, 6, 1, 6, 2, 2, 1, 2, 1}, 3, 3);
  // With this matrix: eigenvalues are
  //-3.667348
  //0.601550
  //9.065798

  if constexpr(is_complex<Scalar>::value){
    // Add some imaginary part for fun (consistent with hermitian format)
    A += Scalar{0, 1} * DenseMatrix<Scalar>(
					    { 0,   -1,   2,
					      1,    0, 0.5,
					     -2, -0.5,   0}, 3, 3);
  }

  auto [lambdas, U] = blas_kernels::eigh(A);

  for(Size i = 0; i < n_rows(A); ++i){
    Vector<Scalar> AU = A * U.get_vect_view(i);
    Vector<Scalar> lamU;
    if constexpr(is_complex<Scalar>::value){
      Scalar cplx_lam = Scalar{lambdas[i], 0};
      lamU = cplx_lam * U.get_vect_view(i);
    } else {
      lamU = lambdas[i] * U.get_vect_view(i);
    }

    REQUIRE((AU - lamU).norm() < (ev_tol * 10));
  }

  auto mus = blas_kernels::eigvalsh(A);

  for(Size i = 0; i < n_rows(A); ++i){
    REQUIRE(std::abs(lambdas[i] - mus[i]) < (10 * ev_tol));
  }
}
// Eigenvalue decomposition:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Eigenvalue decomposition for selected eigenvalues][Eigenvalue decomposition for selected eigenvalues:1]]
SECTION("EVD (he/sym) with selected eigenvalues"){

  auto test_matrix_he_sym = [](){
    DenseMatrix<Scalar> M(
			  {3, 6, 1, 5,
			   6, 2, 2, 4,
			   1, 2, 4, 3,
			   5, 4, 3, 2}, 4, 4);
    // With this matrix: eigenvalues are
    //-3.667348
    //0.601550
    //9.065798

    if constexpr(is_complex<Scalar>::value){
      // Add some imaginary part for fun (consistent with hermitian format)
      M += Scalar{0, 1} * DenseMatrix<Scalar>(
					      { 0,    1,  -2,   -1,
					       -1,    0,   2,  0.5,
						2,   -2,   0,   -1,
						1, -0.5,   1,    0}, 4, 4);
    }
    return M;
  };

  const DenseMatrix<Scalar> A(test_matrix_he_sym());

  auto [evref, Uref] = blas_kernels::eigh(A);

  // Check A * U_i = lambda_i * U_i
  auto check_ev = [&A](Vector<Scalar> U_i, Real lambda_i){
    const Vector<Scalar> AU_i = A * U_i;
    const Vector<Scalar> lam_U_i = Scalar{lambda_i} * U_i;
    return (AU_i - lam_U_i).norm() < ev_tol;
  };

  // Compute middle ev (idx 1 and 2)
  auto [evmid, Umid] = blas_kernels::eigh_select(A, 1, 2);
  REQUIRE(n_cols(Umid) == 2);
  REQUIRE(evmid.size() == 2);
  REQUIRE(std::abs(evmid[0] - evref[1]) < ev_tol);
  REQUIRE(std::abs(evmid[1] - evref[2]) < ev_tol);
  REQUIRE(check_ev(Umid.get_vect_view(0), evmid[0]));
  REQUIRE(check_ev(Umid.get_vect_view(1), evmid[1]));

  auto [evsmall, Usmall] = blas_kernels::eigh_smallest(A, 2);
  auto [evlarge, Ularge] = blas_kernels::eigh_largest(A, 2);

  REQUIRE(n_cols(Usmall) == 2);
  REQUIRE(n_cols(Ularge) == 2);
  REQUIRE(evsmall.size() == 2);
  REQUIRE(evlarge.size() == 2);
  REQUIRE(std::abs(evsmall[0] - evref[0]) < ev_tol);
  REQUIRE(std::abs(evsmall[1] - evref[1]) < ev_tol);
  REQUIRE(std::abs(evlarge[0] - evref[2]) < ev_tol);
  REQUIRE(std::abs(evlarge[1] - evref[3]) < ev_tol);
  REQUIRE(check_ev(Usmall.get_vect_view(0), evsmall[0]));
  REQUIRE(check_ev(Usmall.get_vect_view(1), evsmall[1]));
  REQUIRE(check_ev(Ularge.get_vect_view(0), evlarge[0]));
  REQUIRE(check_ev(Ularge.get_vect_view(1), evlarge[1]));
}
// Eigenvalue decomposition for selected eigenvalues:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Generalized eigenvalue decomposition][Generalized eigenvalue decomposition:1]]
SECTION("Generalized EVD (he/sym) with selected eigenvalues"){
  const int M = 10;
  const int n_v = 6;

  // We want to find U so that A U = B U LAM
  // With U the n_v eigenvectors associated to the smallest eigenvalues
  auto make_hpd_mat = [](){
    DenseMatrix<Scalar> mat = test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(M*M, Real{-2}, Real{2}, M, M).matrix;
    for(int k = 0; k < M; ++k){ mat(k, k) += 10; }
    mat *= mat.t();
    mat.set_property(MatrixSymmetry::symmetric);
    return mat;
  };

  const DenseMatrix<Scalar> A = make_hpd_mat();
  const DenseMatrix<Scalar> B = make_hpd_mat();

  // In case A or B is too big, we multiply tolerance by (norm of A + norm of B) (Frobenius norm)
  Real scal_norm = A.norm() + B.norm();
  //std::cerr << scal_norm << '\n';
  Real my_ev_tol = ev_tol * scal_norm;

  // Check A * U_i = B * U_i * lambda_i
  auto check_ev = [&A, &B, my_ev_tol](Vector<Scalar> U_i, Real lambda_i){
    const Vector<Scalar> AU_i = A * U_i;
    const Vector<Scalar> lam_B_U_i = Scalar{lambda_i} * B * U_i;
    return (AU_i - lam_B_U_i).norm() < my_ev_tol;
  };

  auto [evref  , Uref]   = blas_kernels::gen_eigh_select  (A, B, 0, M - 1);
  auto [evsmall, Usmall] = blas_kernels::gen_eigh_smallest(A, B, n_v);
  auto [evlarge, Ularge] = blas_kernels::gen_eigh_largest (A, B, n_v);

  for(int j = 0; j < n_v; ++j){
    REQUIRE(check_ev(Usmall.get_vect_view(j), evsmall[j]));
    REQUIRE(std::abs(evsmall[j] - evref[j]) < my_ev_tol);

    REQUIRE(check_ev(Ularge.get_vect_view(j), evlarge[j]));
    REQUIRE(std::abs(evlarge[j] - evref[j + M - n_v]) < my_ev_tol);
  }
}
// Generalized eigenvalue decomposition:1 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Generalized eigenvalue decomposition][Generalized eigenvalue decomposition:2]]
SECTION("Singular value decomposition"){
  const Size M = 3;
  const Size N = 2;
  const Size K = 2;

  DenseMatrix<Scalar> A({1, 2, 3, -4, -5, 6}, M, N);
  if constexpr(is_complex<Scalar>::value){
    // Add some imaginary part for fun
    A += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 2, 1, 0, 0.5}, M, N);
  }

  auto check_svd = [&A](const DenseMatrix<Scalar>& l_U,
			const IndexArray<Real>& l_sigmas,
			const DenseMatrix<Scalar>& l_Vt){
    // Construct the diagonal matrix Sigma for checking
    DenseMatrix<Scalar> Sigma(n_cols(l_U), n_rows(l_Vt));
    auto l_k = std::min(n_cols(l_U), n_rows(l_Vt));
    for(Size i = 0; i < l_k; ++i) Sigma(i, i) = Scalar{l_sigmas[i]};

    DenseMatrix<Scalar> Acheck = l_U * Sigma * l_Vt;
    return ((A - Acheck).norm() < ev_tol);
  };

  auto check_singvals = [](const IndexArray<Real>& s1,
			   const IndexArray<Real>& s2){
    if(s1.size() != s2.size()) return false;
    for(Size i = 0; i < s1.size(); ++i){
      if(std::abs(s1[i] - s2[i]) > ev_tol) return false;
    }
    return true;
  };

  // Reduced svd
  const bool reduced = true;
  const bool full = false;

  auto [U, sigmas, Vt] = blas_kernels::svd(A, reduced);
  REQUIRE(n_rows(U) == M);
  REQUIRE(n_cols(U) == K);
  REQUIRE(n_rows(Vt) == K);
  REQUIRE(n_cols(Vt) == N);
  REQUIRE(check_svd(U, sigmas, Vt));

  // Full svd
  auto [U2, sigmas2, Vt2] = blas_kernels::svd(A, full);
  REQUIRE(n_rows(U2) == M);
  REQUIRE(n_cols(U2) == M);
  REQUIRE(n_rows(Vt2) == N);
  REQUIRE(n_cols(Vt2) == N);
  REQUIRE(check_singvals(sigmas2, sigmas));
  REQUIRE(check_svd(U2, sigmas2, Vt2));

  // Full, only left singular vectors U
  auto [U3, sigmas3] = blas_kernels::svd_left_sv(A, full);
  REQUIRE(n_rows(U3) == M);
  REQUIRE(n_cols(U3) == M);
  REQUIRE(check_singvals(sigmas3, sigmas));

  // Full, only right singular vectors Vt
  auto [sigmas4, Vt4] = blas_kernels::svd_right_sv(A, full);
  REQUIRE(n_rows(Vt4) == N);
  REQUIRE(n_cols(Vt4) == N);
  REQUIRE(check_singvals(sigmas4, sigmas));

  // Only singular values
  auto sigmas5 = blas_kernels::svdvals(A);
  REQUIRE(check_singvals(sigmas5, sigmas));
}
// Generalized eigenvalue decomposition:2 ends here

// [[file:../../../org/maphys/kernel/BlasKernels.org::*Footer][Footer:1]]
}
// Footer:1 ends here
