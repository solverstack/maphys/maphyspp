// [[file:../../../org/maphys/solver/ConjugateGradient.org::*Sequential][Sequential:1]]
#include <iostream>
#include <vector>

#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen_header.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMA
#include <maphys/wrappers/armadillo/Armadillo_header.hpp>
#endif
#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMA
#include <maphys/wrappers/armadillo/Armadillo.hpp>
#endif

#include <maphys.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "test_iterative_solver.hpp"

using namespace maphys;

template<typename Scalar, typename Matrix, bool use_pcd = true>
void test(const std::string mat_type){
  using Vect = typename vector_type<Matrix>::type;

  const int m = 4;
  const int max_iter = 5;
  SECTION(std::string("ConjugateGradient, no preconditioner, matrix ") + mat_type){
    test_solver<Scalar, Matrix, Vect, ConjugateGradient<Matrix, Vect>>(
    test_matrix::laplacian_1D_matrix<Scalar, Matrix>(m).matrix,
    test_matrix::simple_vector<Scalar, Vect>().vector, max_iter);
  }

  if constexpr (use_pcd){
    SECTION(std::string("ConjugateGradient, diagonal preconditioner, matrix ") + mat_type){
      using PcdType = DiagonalPrecond<Matrix, Vect>;
      test_solver<Scalar, Matrix, Vect, ConjugateGradient<Matrix, Vect, PcdType>>(
      test_matrix::laplacian_1D_matrix<Scalar, Matrix>(m).matrix,
      test_matrix::simple_vector<Scalar, Vect>().vector, max_iter);
    }
  }
}

TEMPLATE_TEST_CASE("ConjugateGradient", "[ConjugateGradient][iterative][sequential]", float, double){
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;

  test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
  test<Scalar, SparseMatrixCOO<Scalar>>("SparseMatrixCOO");
  test<Scalar, SparseMatrixCSC<Scalar>>("SparseMatrixCSC");
  test<Scalar, Laplacian<Scalar>, false>("Laplacian");
#ifdef MAPHYSPP_USE_EIGEN
  using EigenDenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
  using EigenSparseMatrix = Eigen::SparseMatrix<Scalar>;

  test<Scalar, EigenDenseMatrix>("Eigen dense");
  test<Scalar, EigenSparseMatrix>("Eigen sparse");
#endif
#ifdef MAPHYSPP_USE_ARMA
  test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
  test<Scalar, arma::SpMat<Scalar>>("Armadillo sparse");
#endif

  SECTION("ConjugateGradient, with correct initial guess"){
    using Matrix = DenseMatrix<Scalar>;
    Vector<Scalar> B{0, 0, 0, 5};
    Vector<Scalar> Xguess{1, 2, 3, 4};
    Matrix A = test_matrix::laplacian_1D_matrix<Scalar, Matrix>(4).matrix;
    Real tol = arithmetic_tolerance<Scalar>::value;
    ConjugateGradient<Matrix, Vector<Scalar>> cg;
    cg.setup(parameters::A<Matrix>{A},
	     parameters::tolerance<Real>{tol},
	     parameters::max_iter<int>{1},
	     parameters::verbose{true});

    cg.solve(B, Xguess);

    REQUIRE( cg.get_n_iter() == 0 );
    REQUIRE( cg.get_residual() < tol );
  }
} // TEMPLATE_PRODUCT_TEST_CASE
// Sequential:1 ends here

// [[file:../../../org/maphys/solver/ConjugateGradient.org::*Test with views][Test with views:1]]
TEST_CASE("ConjugateGradient, with view", "[ConjugateGradient][iterative][sequential]"){
  std::vector<double> Ad{4., -1., 0, -1., 3., -0.5, 0., -0.5, 4.};
  std::vector<double> Bd{2., 3.5, 11.};
  std::vector<double> Xd{0, 0, 0};

  const size_t m = 3;

  const DenseMatrix<double> A(DenseData<double>(m, m, Ad.data()), true);
  const Vector<double> B(DenseData<double, 1>(m, Bd.data()), true);
  Vector<double> X(DenseData<double, 1>(m, Xd.data()), true);

  ConjugateGradient<DenseMatrix<double>, Vector<double>> cg;
  cg.setup(parameters::A{A},
           parameters::verbose{true},
           parameters::max_iter{10},
           parameters::tolerance{1e-8});

  X = cg * B;

  REQUIRE(cg.get_n_iter() >= 0);
  REQUIRE(cg.get_residual() < 1e-8);
} // TEST_CASE
// Test with views:1 ends here
