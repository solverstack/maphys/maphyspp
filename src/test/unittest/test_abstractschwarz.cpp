// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Test abstractschwarz][Test abstractschwarz:1]]
#include <maphys.hpp>
  #include <maphys/solver/ConjugateGradient.hpp>
  #include <maphys/solver/BlasSolver.hpp>
  #include <maphys/solver/PartSchurSolver.hpp>
  #include <maphys/precond/AbstractSchwarz.hpp>
  #include <maphys/testing/TestMatrix.hpp>
#if defined(MAPHYSPP_USE_MUMPS)
  #include <maphys/solver/Mumps.hpp>
#elif defined(MAPHYSPP_USE_PASTIX)
  #include <maphys/solver/Pastix.hpp>
#endif
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>

  using namespace maphys;

  TEMPLATE_TEST_CASE("AbstractSchwarz", "[AbstractSchwarz][preconditioner]", SparseMatrixCOO<double>, SparseMatrixCSC<double>){
    using SparseMat = TestType;
    using Scalar = double;
    using Real = double;

    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    auto A = test_matrix::dist_spd_matrix<Scalar, SparseMat>(p).matrix;
    auto Xt = PartVector<Vector<Scalar>>(p);
    Xt.set_to_ones();
    auto B = A * Xt;

    const int max_iter = std::max(1, n_rows(A));
    const bool verb = true;
    const Real tol = 1e-5;

    using DenseDirectSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
#if defined(MAPHYSPP_USE_MUMPS)
    using SparseDirectSolver = Mumps<SparseMat, Vector<Scalar>>;
    if(MMPI::rank() == 0) std::cout << "Direct solver used: Mumps\n";
#elif defined(MAPHYSPP_USE_PASTIX)
    using SparseDirectSolver = Pastix<SparseMat, Vector<Scalar>>;
    if(MMPI::rank() == 0) std::cout << "Direct solver used: Pastix\n";
#else
    static_assert(false, "AbstractSchwarz tests required Pastix or Mumps");
#endif

    SECTION("Vanilla CG"){
      ConjugateGradient<decltype(A), decltype(B)> cg_vanilla;
      cg_vanilla.setup(parameters::A{A},
		       parameters::tolerance{tol},
		       parameters::max_iter{max_iter},
		       parameters::verbose{verb});
      auto X = cg_vanilla * B;
      cg_vanilla.display("Vanilla CG");
      REQUIRE(cg_vanilla.get_n_iter() >= 0);
    }

    SECTION("CG with Additive Schwarz"){
      using Pcd = AdditiveSchwarz<decltype(A), decltype(B), SparseDirectSolver>;
      static_assert(Pcd::is_AS, "AdditiveSchwarz trait unrecognized");

      ConjugateGradient<decltype(A), decltype(B), Pcd> cg_as;
      cg_as.setup(parameters::A{A},
		  parameters::tolerance{tol},
		  parameters::max_iter{max_iter},
		  parameters::verbose{verb});
      auto X = cg_as * B;
      cg_as.display("CG with Additive Schwarz");
      REQUIRE(cg_as.get_n_iter() >= 0);
    }

    SECTION("CG with Neumann Neumann, partition of unity = matrix"){
      using Pcd = NeumannNeumann<decltype(A), decltype(B), SparseDirectSolver>;
      static_assert(Pcd::is_NN, "NeumannNeumann trait unrecognized");
      ConjugateGradient<decltype(A), decltype(B), Pcd> cg_nn;
      cg_nn.setup(parameters::A{A},
		  parameters::tolerance{tol},
		  parameters::max_iter{max_iter},
		  parameters::verbose{verb});
      auto X = cg_nn * B;
      cg_nn.display("CG with Neumann Neumann - matrix");
      REQUIRE(cg_nn.get_n_iter() >= 0);
    }

    SECTION("CG with Neumann Neumann, partition of unity = boolean"){
      using Pcd = NeumannNeumann<decltype(A), decltype(B), SparseDirectSolver, PartOfUnityMethod::boolean>;
      ConjugateGradient<decltype(A), decltype(B), Pcd> cg_nn;
      cg_nn.setup(parameters::A{A},
		  parameters::tolerance{tol},
		  parameters::max_iter{max_iter},
		  parameters::verbose{verb});
      auto X = cg_nn * B;
      cg_nn.display("CG with Neumann Neumann - boolean");
      // Test for compilation mostly
      //REQUIRE(cg_nn.get_n_iter() >= 0);
    }

    SECTION("CG with Neumann Neumann, partition of unity = multiplicity"){
      using Pcd = NeumannNeumann<decltype(A), decltype(B), SparseDirectSolver, PartOfUnityMethod::multiplicity>;
      ConjugateGradient<decltype(A), decltype(B), Pcd> cg_nn;
      cg_nn.setup(parameters::A{A},
		  parameters::tolerance{tol},
		  parameters::max_iter{max_iter},
		  parameters::verbose{verb});
      auto X = cg_nn * B;
      cg_nn.display("CG with Neumann Neumann - multiplicity");
      REQUIRE(cg_nn.get_n_iter() >= 0);
    }

    SECTION("CG with Robin Robin"){
      using Pcd = RobinRobin<decltype(A), decltype(B), SparseDirectSolver>;
      static_assert(Pcd::is_RR, "RobinRobin trait unrecognized");

      ConjugateGradient<decltype(A), decltype(B), Pcd> cg_rr;

      cg_rr.setup(parameters::A{A},
		  parameters::tolerance{tol},
		  parameters::max_iter{max_iter},
		  parameters::verbose{verb});

      auto X = cg_rr * B;
      cg_rr.display("CG with Robin Robin");
      REQUIRE(cg_rr.get_n_iter() >= 0);
    }

    SECTION("CG on S without precond"){
      using CGType = ConjugateGradient<PartMatrix<DenseMatrix<Scalar>>, decltype(B)>;
      PartSchurSolver<decltype(A), decltype(B), SparseDirectSolver, CGType> cg_s;
      cg_s.setup(parameters::A{A});

      CGType& cg_dense = cg_s.get_solver_S();
      cg_dense.setup(parameters::max_iter{max_iter},
		     parameters::tolerance{tol},
		     parameters::verbose{verb});

      auto X = cg_s * B;
      cg_dense.display("CG on S without precond");
      REQUIRE(cg_dense.get_n_iter() >= 0);
    }

    SECTION("CG on S with Additive Schwarz"){
      using Pcd = AdditiveSchwarz<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>, DenseDirectSolver>;
      using CGType = ConjugateGradient<PartMatrix<DenseMatrix<Scalar>>, decltype(B), Pcd>;

      PartSchurSolver<decltype(A), decltype(B), SparseDirectSolver, CGType> as_s;
      as_s.setup(parameters::A{A});

      CGType& cg_as_dense = as_s.get_solver_S();
      cg_as_dense.setup(parameters::max_iter{max_iter},
		       parameters::tolerance{tol},
		       parameters::verbose{verb});

      auto X = as_s * B;
      cg_as_dense.display("CG on S with Additive Schwarz");
      REQUIRE(cg_as_dense.get_n_iter() >= 0);
    }

    SECTION("CG on S with Neumann Neumann"){
      using Pcd = NeumannNeumann<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>, DenseDirectSolver>;
      using CGType = ConjugateGradient<PartMatrix<DenseMatrix<Scalar>>, decltype(B), Pcd>;

      PartSchurSolver<decltype(A), decltype(B), SparseDirectSolver, CGType> nn_s;
      nn_s.setup(parameters::A{A});

      CGType& cg_nn_dense = nn_s.get_solver_S();
      cg_nn_dense.setup(parameters::tolerance{tol},
			parameters::max_iter{max_iter},
			parameters::verbose{verb});

      auto X = nn_s * B;
      cg_nn_dense.display("CG on S with Neumann Neumann");

      REQUIRE(cg_nn_dense.get_n_iter() >= 0);
    }

    SECTION("CG on S with Robin Robin"){
      using Pcd = RobinRobin<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>, DenseDirectSolver>;
      using CGType = ConjugateGradient<PartMatrix<DenseMatrix<Scalar>>, decltype(B), Pcd>;

      PartSchurSolver<decltype(A), decltype(B), SparseDirectSolver, CGType> rr_s;
      rr_s.setup(parameters::A{A});

      CGType& cg_rr_dense = rr_s.get_solver_S();
      cg_rr_dense.setup(parameters::tolerance{tol},
			parameters::max_iter{max_iter},
			parameters::verbose{verb});

      auto X = rr_s * B;
      cg_rr_dense.display("CG on S with Robin Robin");
      REQUIRE(cg_rr_dense.get_n_iter() >= 0);
    }
  } // TEST_CASE
// Test abstractschwarz:1 ends here
