// [[file:../../../org/maphys/solver/CoarseSolve.org::*Tests][Tests:1]]
#include <iostream>

#include <maphys.hpp>
#include <maphys/solver/Mumps.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/solver/CentralizedSolver.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/solver/CoarseSolve.hpp>
#include <maphys/testing/TestMatrix.hpp>

#include <catch2/catch_test_macros.hpp>

using namespace maphys;

TEST_CASE("Coarse solve", "[coarsesolve]"){
  using Scalar = double;

  SECTION("Small test 3 SD - dense"){
    using DnDirSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
    using DnGlobSolver = CentralizedSolver<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>, DnDirSolver>;

    // Load matrix and RHS
    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    const PartMatrix<DenseMatrix<Scalar>> A = test_matrix::dist_spd_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
    PartVector<Vector<Scalar>> x_exp = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
    x_exp.apply_on_data([](Vector<Scalar>& v){
      for(Size k = 0; k < n_rows(v); ++k){
        v[k] = Scalar{1};
      }
    });

    const PartVector<Vector<Scalar>> b = A * x_exp;

    CoarseSolve<DenseMatrix<Scalar>, Vector<Scalar>, DnGlobSolver> cgc;
    cgc.setup(A);

    auto x = cgc * b;

    //x.display_centralized("x_centr");
    auto norm = (x_exp - x).norm();
    if(MMPI::rank() == 0) std::cout << "||x_exp - x|| = " << norm << '\n';
    REQUIRE((x_exp - x).norm() < 1e-10);
  }

  SECTION("Small test 3 SD - sparse COO"){
    using SparseMat = SparseMatrixCOO<Scalar>;
    using SpDirSolver = Mumps<SparseMat, Vector<Scalar>>;
    using SpGlobSolver = CentralizedSolver<PartMatrix<SparseMat>, PartVector<Vector<Scalar>>, SpDirSolver>;

    // Load matrix and RHS
    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    PartMatrix<SparseMat> A = test_matrix::dist_spd_matrix<Scalar, SparseMat>(p).matrix;
    PartVector<Vector<Scalar>> x_exp = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
    x_exp.apply_on_data([](Vector<Scalar>& v){
      for(Size k = 0; k < n_rows(v); ++k){
        v[k] = Scalar{1};
      }
    });

    const PartVector<Vector<Scalar>> b = A * x_exp;

    CoarseSolve<SparseMat, Vector<Scalar>, SpGlobSolver> cgc;
    cgc.setup(A);

    auto x = cgc * b;

    //x.display_centralized("x_centr");
    auto norm = (x_exp - x).norm();
    if(MMPI::rank() == 0) std::cout << "||x_exp - x|| = " << norm << '\n';
    REQUIRE((x_exp - x).norm() < 1e-10);
  }

  SECTION("Small test 3 SD - sparse CSC"){
    using SparseMat = SparseMatrixCSC<Scalar>;
    using SpDirSolver = Mumps<SparseMat, Vector<Scalar>>;
    using SpGlobSolver = CentralizedSolver<PartMatrix<SparseMat>, PartVector<Vector<Scalar>>, SpDirSolver>;
    // Load matrix and RHS
    std::shared_ptr<Process> p = test_matrix::get_distr_process();
    PartMatrix<SparseMat> A = test_matrix::dist_spd_matrix<Scalar, SparseMat>(p).matrix;
    PartVector<Vector<Scalar>> x_exp = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
    x_exp.apply_on_data([](Vector<Scalar>& v){
      for(Size k = 0; k < n_rows(v); ++k){
        v[k] = Scalar{1};
      }
    });

    const PartVector<Vector<Scalar>> b = A * x_exp;

    CoarseSolve<SparseMat, Vector<Scalar>, SpGlobSolver> cgc;
    cgc.setup(A);

    auto x = cgc * b;

    //x.display_centralized("x_centr");
    auto norm = (x_exp - x).norm();
    if(MMPI::rank() == 0) std::cout << "||x_exp - x|| = " << norm << '\n';
    REQUIRE((x_exp - x).norm() < 1e-10);
  }

  SECTION("Big test 8 SD dense"){
    using DnDirSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
    using DnGlobSolver = CentralizedSolver<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>, DnDirSolver>;
    const std::string path_to_part = "@MAPHYSPP_MATRIX_PARTITIONS_8@";
    const int n_subdomains = 8;

    // Subdomain topology
    std::shared_ptr<Process> p = bind_subdomains(n_subdomains);

    // Load matrix and RHS
    PartMatrix<SparseMatrixCOO<Scalar>> Asp(p);
    PartVector<Vector<Scalar>> x_exp(p);
    load_subdomains_and_data(path_to_part, n_subdomains, p, Asp, x_exp);
    x_exp.apply_on_data([](Vector<Scalar>& v){
      for(Size k = 0; k < n_rows(v); ++k){
        v[k] = Scalar{1};
      }
    });


    const PartVector<Vector<Scalar>> b = Asp * x_exp;
    PartMatrix<DenseMatrix<Scalar>> A = Asp.convert<DenseMatrix<Scalar>>([](const SparseMatrixCOO<Scalar>& m){ return m.to_dense(); });

    CoarseSolve<DenseMatrix<Scalar>, Vector<Scalar>, DnGlobSolver> cgc;
    cgc.setup(A);

    auto x = cgc * b;

    //x.display_centralized("x_centr");
    auto norm = (x_exp - x).norm();
    if(MMPI::rank() == 0) std::cout << "||x_exp - x|| = " << norm << '\n';
    REQUIRE((x_exp - x).norm() < 1e-10);
  }

  SECTION("Big test 8 SD sparse"){
    using SparseMat = SparseMatrixCOO<Scalar>;
    using SpDirSolver = Mumps<SparseMat, Vector<Scalar>>;
    using SpGlobSolver = CentralizedSolver<PartMatrix<SparseMat>, PartVector<Vector<Scalar>>, SpDirSolver>;
    const std::string path_to_part = "@MAPHYSPP_MATRIX_PARTITIONS_8@";
    const int n_subdomains = 8;

    // Subdomain topology
    std::shared_ptr<Process> p = bind_subdomains(n_subdomains);

    // Load matrix and RHS
    PartMatrix<SparseMat> A(p);
    PartVector<Vector<Scalar>> x_exp(p);
    load_subdomains_and_data(path_to_part, n_subdomains, p, A, x_exp);
    x_exp.apply_on_data([](Vector<Scalar>& v){
      for(Size k = 0; k < n_rows(v); ++k){
        v[k] = Scalar{1};
      }
    });

    const PartVector<Vector<Scalar>> b = A * x_exp;

    CoarseSolve<SparseMat, Vector<Scalar>, SpGlobSolver> cgc;
    cgc.setup(A);

    auto x = cgc * b;

    //x.display_centralized("x_centr");
    auto norm = (x_exp - x).norm();
    if(MMPI::rank() == 0) std::cout << "||x_exp - x|| = " << norm << '\n';
    REQUIRE((x_exp - x).norm() < 1e-10);
  }

} // TEST_CASE
// Tests:1 ends here
