// [[file:../../../org/maphys/solver/CentralizedSolver.org::*Tests][Tests:1]]
#include <iostream>
#include <maphys.hpp>
#include "maphys/part_data/PartMatrix.hpp"
#include "maphys/solver/Pastix.hpp"
#include "maphys/solver/CentralizedSolver.hpp"
#include "maphys/testing/TestMatrix.hpp"

#include <catch2/catch_test_macros.hpp>

using namespace maphys;

TEST_CASE("CentralizedSolver", "[centralized]"){

  using Scalar = double;
  using Real = double;

  const std::string path_to_part = "@MAPHYSPP_MATRIX_PARTITIONS_8@";
  const int n_subdomains = 8;

  // Subdomain topology
  std::shared_ptr<Process> p = bind_subdomains(n_subdomains);

  // Load matrix and RHS
  PartMatrix<SparseMatrixCOO<Scalar>> dm(p);
  PartVector<Vector<Scalar>> dX_expected(p);
  load_subdomains_and_data(path_to_part, n_subdomains, p, dm, dX_expected);
  dX_expected.apply_on_data([](Vector<Scalar>& v){
    for(Size k = 0; k < n_rows(v); ++k){
      v[k] = Scalar{1};
    }
  });

  PartVector<Vector<Scalar>> d_rhs = dm * dX_expected;

  using Solver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;

  CentralizedSolver<decltype(dm), decltype(d_rhs), Solver> ctr_solver;
  ctr_solver.setup(parameters::A{dm},
                   parameters::root{0});

  PartVector<Vector<Scalar>> dX = ctr_solver * d_rhs;

  Real be = (d_rhs - dm * dX).norm();
  be /= d_rhs.norm();

  std::cout << "Backward error: " << be << '\n';
  REQUIRE(be < 1e-14);
} //TEST_CASE
// Tests:1 ends here
