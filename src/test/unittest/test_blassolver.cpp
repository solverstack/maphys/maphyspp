// [[file:../../../org/maphys/solver/BlasSolver.org::*Tests][Tests:1]]
#include <maphys.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace maphys;

TEMPLATE_TEST_CASE("BlasSolver", "[dense][sequential][blas]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;

  DenseMatrix<Scalar> A = test_matrix::general_matrix<Scalar, DenseMatrix<Scalar>>().matrix;
  DenseMatrix<Scalar> A_spd({3, 2, 2, 1,
      0, 4, 1, 1,
      0, 0, 3, 2,
      0, 0, 0, 4}, 4, 4, true);
  A_spd *= A_spd.t();
  A_spd.set_spd(MatrixStorage::full);

  DenseMatrix<Scalar> A_sym({1, 2, 3, 4,
      2, 2, 3, 1,
      3, 3, 4, 3,
      4, 1, 3, 1}, 4, 4, true);
  A_sym.set_property(MatrixSymmetry::symmetric);

  const Real tol = arithmetic_tolerance<Scalar>::value;
// Tests:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Square systems][Square systems:1]]
SECTION("Single solve Ax=b, A full"){
  Vector<Scalar> b{1,2,3,4};
  BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>> s;
  s.setup(A);

  Vector<Scalar> x = s * b;

  Vector<Scalar> direct_err = A * x - b;
  REQUIRE(direct_err.norm() <= tol);
}

SECTION("Single solve Ax=b, A spd"){
  Vector<Scalar> b{1,2,3,4};
  BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>> s;
  s.setup(A_spd);

  Vector<Scalar> x = s * b;

  Vector<Scalar> direct_err = A_spd * x - b;
  REQUIRE(direct_err.norm() <= tol);
}

SECTION("Single solve Ax=b, A sym"){
  Vector<Scalar> b{1,2,3,4};
  BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>> s;
  s.setup(A_sym);

  Vector<Scalar> x = s * b;

  Vector<Scalar> direct_err = A_sym * x - b;
  REQUIRE(direct_err.norm() <= tol);
}

SECTION("Consecutive solve Ax=b with different b"){
  Vector<Scalar> b1{1,2,3,4};
  Vector<Scalar> b2{2,2,3,-7};
  Vector<Scalar> b3{1,-22,33,4};
  BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>> s;
  s.setup(A);

  Vector<Scalar> x1 = s * b1;
  Vector<Scalar> x2 = s * b2;
  Vector<Scalar> x3 = s * b3;

  Vector<Scalar> direct_err1 = A * x1 - b1;
  REQUIRE(direct_err1.norm() <= tol);
  Vector<Scalar> direct_err2 = A * x2 - b2;
  REQUIRE(direct_err2.norm() <= tol);
  Vector<Scalar> direct_err3 = A * x3 - b3;
  REQUIRE(direct_err3.norm() <= tol);
}

SECTION("Solve multirhs AX=B, A full, B is a matrix"){
  DenseMatrix<Scalar> B({1,  2, -10, 0,
      2,  3,  2, -1,
      4,  2,  3,  10}, 4, 3);

  BlasSolver<DenseMatrix<Scalar>, DenseMatrix<Scalar>> s;
  s.setup(A);
  DenseMatrix<Scalar> X = s * B;
  DenseMatrix<Scalar> direct_err = A * X - B;
  REQUIRE(direct_err.norm() <= tol);
}

SECTION("Triangular solve with SPD matrix"){
  BlasSolver<DenseMatrix<Scalar>, DenseMatrix<Scalar>> s;
  s.setup(A_spd);

  // Perform the solve in two steps
  // 1-  y <- solve L . y = b
  // 2-  x <- solve L^T x = y
  // And compare x with x_exp use to generate b = A * x_exp
  const Vector<Scalar> x_exp{1,2,3,4};
  const Vector<Scalar> b = A_spd * x_exp; // (80. 78. 89. 100.)

  const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-8 : 1e-4;
  const Vector<Scalar> y_exp{18.85618083, 8.66856112, 17.29826783, 13.15562323};
  auto y = s.triangular_solve(b, MatrixStorage::lower, false);
  Real direct_err = (y - y_exp).norm();
  REQUIRE(direct_err <= large_tol);
  auto x = s.triangular_solve(y, MatrixStorage::lower, true);

  direct_err = (x - x_exp).norm();
  REQUIRE(direct_err <= tol);
}
// Square systems:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Overdetermined system][Overdetermined system:1]]
SECTION("Least square solve (A m x n with m > n)"){
  const DenseMatrix<Scalar> Ar({1, 3, 5, 2, 4, 6}, 3, 2);
  const Vector<Scalar> b({-1, 0, 1});
  const Vector<Scalar> x_exp({2, -1.5});

  BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>> solver(Ar);
  Vector<Scalar> x = solver * b;

  const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-6 : 1e-2;
  REQUIRE((x - x_exp).norm() < large_tol);
}
// Overdetermined system:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Underdetermined system][Underdetermined system:1]]
SECTION("Minimal norm (A m x n with m < n)"){
  const DenseMatrix<Scalar> Ar({1, 2, 3, 4, 5, 6}, 2, 3);
  const Vector<Scalar> b({-1, 2});
  const Vector<Scalar> x_exp({3.5, 1, -1.5});

  BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>> solver(Ar);
  Vector<Scalar> x = solver * b;

  const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-6 : 1e-1;
  REQUIRE((x - x_exp).norm() < large_tol);
}
// Underdetermined system:1 ends here

// [[file:../../../org/maphys/solver/BlasSolver.org::*Footer][Footer:1]]
}
// Footer:1 ends here
