// [[file:../../../org/maphys/part_data/PartVector.org::*nrhs = 1][nrhs = 1:1]]
#include <iostream>
#include <vector>

#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen_header.hpp>
#include <maphys/wrappers/Eigen/Eigen.hpp>
#endif

#include <maphys.hpp>
#include "maphys/dist/MPI.hpp"
#include "maphys/part_data/PartVector.hpp"

#include <catch2/catch_test_macros.hpp>

// Non master processes do not own data.
// Therefore they are not concerned by the tests.
#define REQ_MASTER( cond_ ) do {                \
    if(p->is_master()) REQUIRE( cond_ );        \
  } while(0)

TEST_CASE("distvector", "[distvector][distributed]"){
  using namespace maphys;
  using Nei_map = std::map<int, IndexArray<int>>;

  const int n_dofs = 4;

  // SD 1
  Nei_map NM0 {{1, {2, 3}},
	       {2, {0, 3}}};

  // SD 2
  Nei_map NM1 {{0, {1, 0}},
	       {2, {0, 3}}};

  // SD 3
  Nei_map NM2 {{0, {2, 3}},
	       {1, {3, 0}}};


  std::vector<Subdomain> sd;
  sd.emplace_back(0, n_dofs, std::move(NM0), false);
  sd.emplace_back(1, n_dofs, std::move(NM1), false);
  sd.emplace_back(2, n_dofs, std::move(NM2), false);

  std::shared_ptr<Process> p = bind_subdomains(static_cast<int>(sd.size()));
  p->load_subdomains(sd);

  //V = (0, 1, 2, 3, 4, 5, 6)^T
  std::map<int, Vector<double>> m1;
  if(p->owns_subdomain(0)) m1.emplace(0, Vector<double>{0.0, 1.0, 2.0, 3.0});
  if(p->owns_subdomain(1)) m1.emplace(1, Vector<double>{3.0, 2.0, 4.0, 5.0});
  if(p->owns_subdomain(2)) m1.emplace(2, Vector<double>{5.0, 6.0, 0.0, 3.0});
  //W = (2, 2, 2, 2, 2, 2, 2)^T

  std::map<int, Vector<double>> m2;
  if(p->owns_subdomain(0)) m2.emplace(0, Vector<double>{2.0, 2.0, 2.0, 2.0});
  if(p->owns_subdomain(1)) m2.emplace(1, Vector<double>{2.0, 2.0, 2.0, 2.0});
  if(p->owns_subdomain(2)) m2.emplace(2, Vector<double>{2.0, 2.0, 2.0, 2.0});

  const PartVector<Vector<double>> dv(p, m1);
  const PartVector<Vector<double>> dw(p, m2);

  SECTION("Number of columns"){
    REQ_MASTER(dv.get_n_cols() == 1);
  }

  SECTION("Norms"){
    double nv = dv.norm2();
    double nv_exp = 9.5393920; // sqrt(91)
    double nw = dw.norm2();
    double nw_exp = 5.2915026; // sqrt(28)

    std::vector<double> nv_c = dv.cwise_norm();
    std::vector<double> nw_c = dw.cwise_norm();

    REQ_MASTER(nv_c.size() == 1);
    REQ_MASTER(nw_c.size() == 1);
    REQ_MASTER(nv_c[0] - nv_exp < 1e-6);
    REQ_MASTER(nw_c[0] - nw_exp < 1e-6);
    REQ_MASTER(nv - nv_exp < 1e-6);
    REQ_MASTER(nw - nw_exp < 1e-6);
  }

  SECTION("dot product"){
    //double vw = dv.dot(dw);
    double vw = dot(dv, dw);
    double vw_exp = 42;
    REQ_MASTER(vw == vw_exp);
  }

  const Vector<double> dv_centr_exp{0.0,1.0,2.0,3.0,4.0,5.0,6.0};
  const Vector<double> dw_centr_exp{2.0,2.0,2.0,2.0,2.0,2.0,2.0};
  SECTION("centralize"){
    const int root = 0;
    const int rank = MMPI::rank();
    Vector<double> dv_centr = dv.centralize(root);
    Vector<double> dw_centr = dw.centralize(root);
    if( rank == root ){
      REQ_MASTER( dv_centr == dv_centr_exp );
      REQ_MASTER( dw_centr == dw_centr_exp );
    }
  }

  SECTION("centralize on all proces"){
    Vector<double> dv_ag = dv.centralize();
    Vector<double> dw_ag = dw.centralize();
    REQ_MASTER(dv_ag == dv_centr_exp);
    REQ_MASTER(dw_ag == dw_centr_exp);
  }

  SECTION("centralize on interface"){
    const Vector<double> iv_exp({2.0,2.0,3.0,2.0});
    PartVector<Vector<double>> iv(p, true);
    for(int i = 0; i < 3; ++i) { if(p->owns_subdomain(i)) iv.add_subdomain(i, Vector<double>({1,1,1})); }
    iv.assemble();
    const int root = 0;
    const int rank = MMPI::rank();
    const Vector<double> iv_centr = iv.centralize(root);
    if( rank == root ){
      REQ_MASTER( iv_centr == iv_exp );
    }
  }

  //(0,1,2,3,4,5,6)^T + (2,2,2,2,2,2,2)^T = (2,3,4,5,6,7,8)^T
  SECTION("operator+"){
    PartVector<Vector<double>> dvplusdw = dv + dw;
    Vector<double> dvplusdw_centr = dvplusdw.centralize();
    Vector<double> dvplusdw_centr_exp{2.0,3.0,4.0,5.0,6.0,7.0,8.0};
    REQ_MASTER( dvplusdw_centr == dvplusdw_centr_exp);
  }

  SECTION("scalar multiplication"){
    //(2,2,2,2,2,2,2)^T * 3 = (6,6,6,6,6,6,6)^T
    PartVector<Vector<double>> dw_bis = dw;
    Vector<double> dw3{6.0,6.0,6.0,6.0,6.0,6.0,6.0};
    dw_bis *= 3;
    Vector<double> dw_centr = dw_bis.centralize();
    REQ_MASTER(dw_centr == dw3);
  }

  SECTION("scalar multiplication: V = a U"){
    //(2,2,2,2,2,2,2)^T * 3 = (6,6,6,6,6,6,6)^T
    PartVector<Vector<double>> U = dw;
    Vector<double> dw_centr = dw.centralize();

    const Vector<double> U3_ref{6.0,6.0,6.0,6.0,6.0,6.0,6.0};
    PartVector<Vector<double>> Utimes3 = double{3} * U;

    Vector<double> Utimes3_centr = Utimes3.centralize();
    Vector<double> U_centr = U.centralize();
    REQ_MASTER(Utimes3_centr == U3_ref);
    REQ_MASTER(U_centr == dw_centr);
  }

  SECTION("columnwise scalaing"){
    //(2,2,2,2,2,2,2)^T * 3 = (6,6,6,6,6,6,6)^T
    PartVector<Vector<double>> U = dw;

    const Vector<double> U3_ref{6.0,6.0,6.0,6.0,6.0,6.0,6.0};
    std::vector<double> scale = {3.0};
    U.cwise_scale(scale);
    Vector<double> U_centr = U.centralize();
    REQ_MASTER(U_centr == U3_ref);
  }

  SECTION("operator+="){
    //(2,2,2,2,2,2,2)^T + (0,1,2,3,4,5,6)^T = (2,3,4,5,6,7,8)^T
    PartVector<Vector<double>> dw_bis = dw;
    Vector<double> dwpluseqdv{2.0,3.0,4.0,5.0,6.0,7.0,8.0};
    dw_bis += dv;
    Vector<double> dw_centr = dw_bis.centralize();
    REQ_MASTER(dw_centr == dwpluseqdv);
  }

  SECTION("assembly / disassembly"){

    std::map<int, Vector<double>> m_non_assembled;
    if(p->owns_subdomain(0)) m_non_assembled.emplace(0, Vector<double>{0.0, 1.0, 2.0, 3.0});
    if(p->owns_subdomain(1)) m_non_assembled.emplace(1, Vector<double>{0.0, 0.0, 4.0, 5.0});
    if(p->owns_subdomain(2)) m_non_assembled.emplace(2, Vector<double>{0.0, 6.0, 0.0, 0.0});
    PartVector<Vector<double>> dvna(p, m_non_assembled);

    std::map<int, Vector<double>> m_assembled;
    if(p->owns_subdomain(0)) m_assembled.emplace(0, Vector<double>{0.0, 1.0, 2.0, 3.0});
    if(p->owns_subdomain(1)) m_assembled.emplace(1, Vector<double>{3.0, 2.0, 4.0, 5.0});
    if(p->owns_subdomain(2)) m_assembled.emplace(2, Vector<double>{5.0, 6.0, 0.0, 3.0});
    PartVector<Vector<double>> dva(p, m_assembled);

    PartVector<Vector<double>> dvna_cpy = dvna;
    dvna_cpy.assemble();
    REQ_MASTER(dvna_cpy == dva);

    PartVector<Vector<double>> dva_cpy = dva;
    dva_cpy.disassemble();
    REQ_MASTER(dva_cpy == dvna);
  }

  SECTION("Assembly - owner value"){
    std::map<int, Vector<double>> initval;
    if(p->owns_subdomain(0)) initval.emplace(0, Vector<double>{1.0, 2.0, 3.0, 4.0});
    if(p->owns_subdomain(1)) initval.emplace(1, Vector<double>{5.0, 6.0, 7.0, 8.0});
    if(p->owns_subdomain(2)) initval.emplace(2, Vector<double>{9.0, 10.0, 11.0, 12.0});
    PartVector<Vector<double>> v_init(p, initval);

    std::map<int, Vector<double>> expected;
    if(p->owns_subdomain(0)) expected.emplace(0, Vector<double>{1.0, 2.0, 3.0, 4.0});
    if(p->owns_subdomain(1)) expected.emplace(1, Vector<double>{4.0, 3.0, 7.0, 8.0});
    if(p->owns_subdomain(2)) expected.emplace(2, Vector<double>{8.0, 10.0, 1.0, 4.0});
    const PartVector<Vector<double>> v_exp(p, expected);

    v_init.assemble(Reduction::owner_value);
    REQUIRE(v_init == v_exp);
  }
} // TEST_CASE
// nrhs = 1:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Multiple columns][Multiple columns:1]]
TEST_CASE("distvector ncols > 1", "[distvector][distributed]"){
  using namespace maphys;
  using Nei_map = std::map<int, IndexArray<int>>;

  const int n_dofs = 4;

  // SD 1
  Nei_map NM0 {{1, {2, 3}},
	       {2, {0, 3}}};

  // SD 2
  Nei_map NM1 {{0, {1, 0}},
	       {2, {0, 3}}};

  // SD 3
  Nei_map NM2 {{0, {2, 3}},
	       {1, {3, 0}}};

  std::vector<Subdomain> sd;
  sd.emplace_back(0, n_dofs, std::move(NM0), false);
  sd.emplace_back(1, n_dofs, std::move(NM1), false);
  sd.emplace_back(2, n_dofs, std::move(NM2), false);

  std::shared_ptr<Process> p = bind_subdomains(static_cast<int>(sd.size()));
  p->load_subdomains(sd);

  //V = (0, 1, 2, 3, 4, 5, 6)^T
  //    (0, 2, 4, 6, 8, 10, 12)^T
  std::map<int, DenseMatrix<double, 2>> m1;
  if(p->owns_subdomain(0)) m1.emplace(0, DenseMatrix<double, 2>{{0.0, 1.0, 2.0, 3.0, 0.0, 2.0, 4.0, 6.0},4,2});
  if(p->owns_subdomain(1)) m1.emplace(1, DenseMatrix<double, 2>{{3.0, 2.0, 4.0, 5.0, 6.0, 4.0, 8.0, 10.0},4,2});
  if(p->owns_subdomain(2)) m1.emplace(2, DenseMatrix<double, 2>{{5.0, 6.0, 0.0, 3.0, 10.0, 12.0, 0.0, 6.0},4,2});
  //W = (2, 2, 2, 2, 2, 2, 2)^T

  std::map<int, DenseMatrix<double, 2>> m2;
  if(p->owns_subdomain(0)) m2.emplace(0, DenseMatrix<double, 2>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
  if(p->owns_subdomain(1)) m2.emplace(1, DenseMatrix<double, 2>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
  if(p->owns_subdomain(2)) m2.emplace(2, DenseMatrix<double, 2>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});

  const PartVector<DenseMatrix<double, 2>> dv(p, m1);
  const PartVector<DenseMatrix<double, 2>> dw(p, m2);

  SECTION("Number of columns"){
    REQ_MASTER(dv.get_n_cols() == 2);
    PartVector<DenseMatrix<double>> empty{};
    REQ_MASTER(empty.get_n_cols() == 0);
  }

  SECTION("Norms"){
    std::vector<double> nv = dv.cwise_norm();
    double nv_exp = 9.5393920; // sqrt(91)
    std::vector<double> nw = dw.cwise_norm();
    double nw_exp = 5.2915026; // sqrt(28)

    REQ_MASTER(nv.size() == 2);
    REQ_MASTER(nw.size() == 2);
    REQ_MASTER((nv[0] - nv_exp) < 1e-6);
    REQ_MASTER((nw[0] - nw_exp) < 1e-6);
    REQ_MASTER((nv[1] - 2*nv_exp) < 1e-6);
    REQ_MASTER((nw[1] - 2*nw_exp) < 1e-6);
  }

  SECTION("dot product"){
    DenseMatrix<double> vw = dot_block(dv, dw);
    double vw_exp = 42;
    REQ_MASTER(vw(0, 0) == vw_exp);
    REQ_MASTER(vw(0, 1) == 2*vw_exp);
    REQ_MASTER(vw(1, 0) == 2*vw_exp);
    REQ_MASTER(vw(1, 1) == 4*vw_exp);
  }

  const DenseMatrix<double, 2> dv_centr_exp{{0.0,1.0,2.0,3.0,4.0,5.0,6.0,0.0,2.0,4.0,6.0,8.0,10.0,12.0},7,2};
  const DenseMatrix<double, 2> dw_centr_exp{{2.0,2.0,2.0,2.0,2.0,2.0,2.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0},7,2};
  SECTION("centralize"){
    const int root = 0;
    const int rank = MMPI::rank();
    DenseMatrix<double, 2> dv_centr = dv.centralize(root);
    DenseMatrix<double, 2> dw_centr = dw.centralize(root);
    if( rank == root ){
      REQ_MASTER( dv_centr == dv_centr_exp );
      REQ_MASTER( dw_centr == dw_centr_exp );
    }
  }

  SECTION("centralize on all proces"){
    DenseMatrix<double, 2> dv_ag = dv.centralize();
    DenseMatrix<double, 2> dw_ag = dw.centralize();
    REQ_MASTER(dv_ag == dv_centr_exp);
    REQ_MASTER(dw_ag == dw_centr_exp);
  }

  //(0,1,2,3,4,5,6)^T + (2,2,2,2,2,2,2)^T = (2,3,4,5,6,7,8)^T
  SECTION("operator+"){
    PartVector<DenseMatrix<double, 2>> dvplusdw = dv + dw;
    DenseMatrix<double, 2> dvplusdw_centr = dvplusdw.centralize();
    DenseMatrix<double, 2> dvplusdw_centr_exp{{2.0,3.0,4.0,5.0,6.0,7.0,8.0,4.0,6.0,8.0,10.0,12.0,14.0,16.0},7,2};
    REQ_MASTER( dvplusdw_centr == dvplusdw_centr_exp);
  }

  SECTION("scalar multiplication"){
    //((2,2,2,2,2,2,2),       = ((6,6,6,6,6,6,6),
    // (4,4,4,4,4,4,4))^T * 3    (12,12,12,12,12,12,12))^T
    PartVector<DenseMatrix<double, 2>> dw_bis = dw;
    DenseMatrix<double, 2> dw3{{6.0,6.0,6.0,6.0,6.0,6.0,6.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0},7,2};
    dw_bis *= 3;
    DenseMatrix<double, 2> dw_centr = dw_bis.centralize();
    REQ_MASTER(dw_centr == dw3);
  }

  SECTION("scalar multiplication: V = a U"){
    //(2,2,2,2,2,2,2)^T * 3 = (6,6,6,6,6,6,6)^T
    PartVector<DenseMatrix<double, 2>> U = dw;
    DenseMatrix<double, 2> dw_centr = dw.centralize();

    const DenseMatrix<double, 2> U3_ref{{6.0,6.0,6.0,6.0,6.0,6.0,6.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0},7,2};
    PartVector<DenseMatrix<double, 2>> Utimes3 = double{3} * U;

    DenseMatrix<double, 2> Utimes3_centr = Utimes3.centralize();
    DenseMatrix<double, 2> U_centr = U.centralize();
    REQ_MASTER(Utimes3_centr == U3_ref);
    REQ_MASTER(U_centr == dw_centr);
  }

  SECTION("DenseMatrix multiplication: V = U A"){
    //((2,2,2,2,2,2,2),        = ((6,6,6,6,6,6,6),
    // (4,4,4,4,4,4,4))^T * A     (12,12,12,12,12,12,12))^T
    std::map<int, DenseMatrix<double>> m;
    if(p->owns_subdomain(0)) m.emplace(0, DenseMatrix<double>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
    if(p->owns_subdomain(1)) m.emplace(1, DenseMatrix<double>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});
    if(p->owns_subdomain(2)) m.emplace(2, DenseMatrix<double>{{2.0, 2.0, 2.0, 2.0, 4.0, 4.0, 4.0, 4.0},4,2});

    PartVector<DenseMatrix<double>> U(p, m);
    DenseMatrix<double> dw_centr = U.centralize();
    DenseMatrix<double> A22 = DenseMatrix<double, 2>{{1.0, 2.0, 3.0, 4.0},2,2};
    DenseMatrix<double> A21 = DenseMatrix<double>{{5.0, 6.0},2,1};

    const DenseMatrix<double, 2> UA22_ref{{10.0,10.0,10.0,10.0,10.0,10.0,10.0,22.0,22.0,22.0,22.0,22.0,22.0,22.0},7,2};
    const DenseMatrix<double, 1> UA21_ref{{34.0,34.0,34.0,34.0,34.0,34.0,34.0},7,1};
    PartVector<DenseMatrix<double>> UA22 = U * A22;
    PartVector<DenseMatrix<double>> UA21 = U * A21;

    DenseMatrix<double> UA22_centr = UA22.centralize();
    DenseMatrix<double> U_centr = U.centralize();
    DenseMatrix<double> UA21_centr = UA21.centralize();
    REQ_MASTER(UA22_centr == UA22_ref);
    REQ_MASTER(UA21_centr == UA21_ref);
    REQ_MASTER(U_centr == dw_centr);
  }

  SECTION("columnwise scalaing"){
    //(2,2,2,2,2,2,2)^T * 3  = (6,6,6,6,6,6,6)^T
    //(4,4,4,4,4,4,4)^T * 4    (16,16,16,16,16,16,16)^T
    PartVector<DenseMatrix<double, 2>> dw_bis = dw;
    DenseMatrix<double, 2> dw3{{6.0,6.0,6.0,6.0,6.0,6.0,6.0,16.0,16.0,16.0,16.0,16.0,16.0,16.0},7,2};
    std::vector<double> scale = {3.0, 4.0};
    dw_bis.cwise_scale(scale);
    DenseMatrix<double, 2> dw_centr = dw_bis.centralize();
    REQ_MASTER(dw_centr == dw3);
  }

  SECTION("sub_vector copy"){
    //(2,2,2,2,2,2,2)^T + (0,1,2,3,4,5,6)^T   = (2,3,4,5,6,7,8)^T
    //(4,4,4,4,4,4,4)^T   (0,2,4,6,8,10,12)^T   (4,6,8,10,12,14,16)^T
    PartVector<DenseMatrix<double, 2>> dv_bis = dv;
    const DenseMatrix<double, 1> dv1_centr_exp{{0.0,1.0,2.0,3.0,4.0,5.0,6.0},7,1};
    const DenseMatrix<double, 1> dv2_centr_exp{{0.0,2.0,4.0,6.0,8.0,10.0,12.0},7,1};
    PartVector<DenseMatrix<double>> dv1 = dv_bis.get_vect(0);
    PartVector<DenseMatrix<double>> dv2 = dv_bis.get_vect(1);
    PartVector<DenseMatrix<double>> dv12 = dv_bis.get_vect(0, 2);
    DenseMatrix<double> dv1c = dv1.centralize();
    DenseMatrix<double> dv2c = dv2.centralize();
    DenseMatrix<double> dv12c = dv12.centralize();
    REQ_MASTER(dv1_centr_exp == dv1c);
    REQ_MASTER(dv2_centr_exp == dv2c);
    REQ_MASTER(dv_centr_exp == dv12c);
    dv1 *= 2.0;
    DenseMatrix<double> dv_centr_bis = dv_bis.centralize();
    REQ_MASTER(dv_centr_exp == dv_centr_bis);
  }

  SECTION("sub_vector no copy"){
    //(2,2,2,2,2,2,2)^T + (0,1,2,3,4,5,6)^T = (2,3,4,5,6,7,8)^T
    PartVector<DenseMatrix<double, 2>> dv_bis = dv;
    const DenseMatrix<double, 1> dv1_centr_exp{{0.0,3.0,6.0,9.0,12.0,15.0,18.0},7,1};
    const DenseMatrix<double, 2> dv_centr_exp2{{0.0,3.0,6.0,9.0,12.0,15.0,18.0,0.0,2.0,4.0,6.0,8.0,10.0,12.0},7,2};
    PartVector<DenseMatrix<double, 1>> dv1 = dv_bis.get_vect_view(0);
    dv1 *= 3.0;
    DenseMatrix<double, 1> dv1c = dv1.centralize();
    DenseMatrix<double, 2> dv_centr_bis = dv_bis.centralize();
    REQ_MASTER(dv1_centr_exp == dv1c);
    REQ_MASTER(dv_centr_exp2 == dv_centr_bis);
  }


  SECTION("sub_vector_view assigment: [V_0, V_1] = [3*U_0, U_1]"){
    PartVector<DenseMatrix<double, 2>> U = dw;
    DenseMatrix<double, 2> dw_centr = dw.centralize();
    const DenseMatrix<double, 2> U3_ref{{6.0,6.0,6.0,6.0,6.0,6.0,6.0,4.0,4.0,4.0,4.0,4.0,4.0,4.0},7,2};
    PartVector<DenseMatrix<double, 2>> Utimes3 = U.get_vect(0, 2);
    Utimes3.get_vect_view(0, 1) = U.get_vect(0, 1) * 3.0;

    DenseMatrix<double, 2> Utimes3_centr = Utimes3.centralize();
    DenseMatrix<double, 2> U_centr = U.centralize();
    REQ_MASTER(Utimes3_centr == U3_ref);
    REQ_MASTER(U_centr == dw_centr);
  }

  SECTION("same distribution diffrent number of columns"){
    PartVector<DenseMatrix<double, 2>> dv_bis2 = dv;
    PartVector<DenseMatrix<double>> dv_bis(dv_bis2); //= *((PartVector<DenseMatrix<double>>*)&dv_bis2); // does not work for strict compilation options
    const DenseMatrix<double> dv0_centr_exp(7,0);
    const DenseMatrix<double> dv1_centr_exp(7,1);
    const DenseMatrix<double> dv3_centr_exp(7,3);
    PartVector<DenseMatrix<double>> dv0{dv_bis, 0};
    PartVector<DenseMatrix<double>> dv1{dv_bis, 1};
    PartVector<DenseMatrix<double>> dv3{dv_bis, 3};
    DenseMatrix<double> dv0c = dv0.centralize();
    DenseMatrix<double> dv1c = dv1.centralize();
    DenseMatrix<double> dv3c = dv3.centralize();
    DenseMatrix<double> dv_centr_bis = dv_bis.centralize();
    REQ_MASTER(dv0c.get_n_cols() == 0);
    REQ_MASTER(dv1c.get_n_cols() == 1);
    REQ_MASTER(dv3c.get_n_cols() == 3);
    REQ_MASTER(dv0_centr_exp == dv0c);
    REQ_MASTER(dv1_centr_exp == dv1c);
    REQ_MASTER(dv3_centr_exp == dv3c);
    REQ_MASTER(dv_centr_exp == dv_centr_bis);
  }

  SECTION("operator+="){
    //(2,2,2,2,2,2,2)^T + (0,1,2,3,4,5,6)^T = (2,3,4,5,6,7,8)^T
    PartVector<DenseMatrix<double, 2>> dw_bis = dw;
    DenseMatrix<double, 2> dwpluseqdv{{2.0,3.0,4.0,5.0,6.0,7.0,8.0, 4.0,6.0,8.0,10.0,12.0,14.0,16.0},7,2};
    dw_bis += dv;
    DenseMatrix<double, 2> dw_centr = dw_bis.centralize();
    REQ_MASTER(dw_centr == dwpluseqdv);
  }

  SECTION("assembly / disassembly"){

    std::map<int, DenseMatrix<double, 2>> m_non_assembled;
    if(p->owns_subdomain(0)) m_non_assembled.emplace(0, DenseMatrix<double, 2>{{0.0, 1.0, 2.0, 3.0, 0.0, 2.0, 4.0, 6.0},4,2});
    if(p->owns_subdomain(1)) m_non_assembled.emplace(1, DenseMatrix<double, 2>{{0.0, 0.0, 4.0, 5.0, 0.0, 0.0, 8.0, 10.0},4,2});
    if(p->owns_subdomain(2)) m_non_assembled.emplace(2, DenseMatrix<double, 2>{{0.0, 6.0, 0.0, 0.0, 0.0, 12.0, 0.0, 0.0},4,2});
    PartVector<DenseMatrix<double, 2>> dvna(p, m_non_assembled);

    std::map<int, DenseMatrix<double, 2>> m_assembled;
    if(p->owns_subdomain(0)) m_assembled.emplace(0, DenseMatrix<double, 2>{{0.0, 1.0, 2.0, 3.0, 0.0, 2.0, 4.0, 6.0},4,2});
    if(p->owns_subdomain(1)) m_assembled.emplace(1, DenseMatrix<double, 2>{{3.0, 2.0, 4.0, 5.0, 6.0, 4.0, 8.0, 10.0},4,2});
    if(p->owns_subdomain(2)) m_assembled.emplace(2, DenseMatrix<double, 2>{{5.0, 6.0, 0.0, 3.0, 10.0, 12.0, 0.0, 6.0},4,2});
    PartVector<DenseMatrix<double, 2>> dva(p, m_assembled);

    PartVector<DenseMatrix<double, 2>> dvna_cpy = dvna;
    dvna_cpy.assemble();
    REQ_MASTER(dvna_cpy == dva);

    PartVector<DenseMatrix<double, 2>> dva_cpy = dva;
    dva_cpy.disassemble();
    REQ_MASTER(dva_cpy == dvna);
  }
} // TEST_CASE
// Multiple columns:1 ends here

// [[file:../../../org/maphys/part_data/PartVector.org::*Views][Views:1]]
#if defined(MAPHYSPP_USE_EIGEN)
TEST_CASE("PartVector views Eigen", "[distvector][distributed][views]"){
  using namespace maphys;
  using Nei_map = std::map<int, IndexArray<int>>;

  const int n_dofs = 4;

  // SD 1
  Nei_map NM0 {{1, {2, 3}},
	       {2, {0, 3}}};

  // SD 2
  Nei_map NM1 {{0, {1, 0}},
	       {2, {0, 3}}};

  // SD 3
  Nei_map NM2 {{0, {2, 3}},
	       {1, {3, 0}}};


  std::vector<Subdomain> sd;
  sd.emplace_back(0, n_dofs, std::move(NM0), false);
  sd.emplace_back(1, n_dofs, std::move(NM1), false);
  sd.emplace_back(2, n_dofs, std::move(NM2), false);

  std::shared_ptr<Process> p = bind_subdomains(static_cast<int>(sd.size()));
  p->load_subdomains(sd);

  using EigenVector = Eigen::Matrix<double, Eigen::Dynamic, 1>;
  using ComposeVector = DenseMatrix<double, 1>;

  SECTION("Eigen vector"){
    PartVector<EigenVector> v(p);
    v.set_to_arange();

    const double v_sq = dot(v, v);

    PartVector<ComposeVector> v2 = PartVector<ComposeVector>::view(v);

    const double v2_sq = dot(v2, v2);

    REQUIRE(v_sq == v2_sq);
  }
} // TEST_CASE
#endif // MAPHYSPP_USE_EIGEN
// Views:1 ends here
