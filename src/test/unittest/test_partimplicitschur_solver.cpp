/* This file was tangled by tangleorg.sh using org-babel-tangle.
   Do not modify directly this file. Modify the original org file
   and launch the script again. The source org file is: include/maphys/solver/PartImplicitSchurSolver.org */

#include <iostream>
#include <vector>

#include <maphys.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <maphys/solver/PartImplicitSchurSolver.hpp>
#include <maphys/solver/Pastix.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>

#include <maphys/testing/catch.hpp>

TEST_CASE("PartSchurSolver", "[CG][schur][distributed]"){

  using namespace maphys;

  using Scalar = double;
  using SpMat = PartMatrix<SparseMatrixCOO<Scalar>>;
  using Vect = PartVector<Vector<Scalar>>;
  using Real = typename arithmetic_real<Scalar>::type;

  Process p = test_matrix::get_distr_process();

  const SpMat dA = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
  Vect dX_expected = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
  const Vect dB = dA * dX_expected;

  dX_expected.display_centralized("dX_exp");
  dB.display_centralized("dB");

  bool verbose = false;
  if(MMPI::rank() == 0) verbose = true;

  SECTION("PartImplicitSchurSolver( Pastix, CG )"){
    using DirSolver = Pastix<SparseMatrixCOO<Scalar>,Vector<Scalar>>;
    using ImplSchur = ImplicitSchur<SparseMatrixCOO<Scalar>,Vector<Scalar>,DirSolver>;
    using IterSolver = ConjugateGradient<PartOperator<ImplSchur,SparseMatrixCOO<Scalar>,Vector<Scalar>>,Vect>;
    using Solver = PartImplicitSchurSolver<SpMat,Vect, DirSolver,IterSolver>;

    Solver hybrid_solver;

    hybrid_solver.setup(parameters::A{dA},
                        parameters::verbose{verbose});

    IterSolver& iter_solver = hybrid_solver.get_solver_S();
    iter_solver.setup(parameters::max_iter{20},
                      parameters::tolerance{1e-8},
                      parameters::verbose{verbose});

    Vect X = hybrid_solver * dB;
    X.display_centralized("X");

    Vect diff = X - dX_expected;
    Real direct_diff_norm = diff.norm();
    REQUIRE( direct_diff_norm < Real{1e-6} );
  }

  SECTION("PartImplicitSchurSolver( Pastix, CG + diag pcd )"){
    using Precond = DiagonalPrecond<PartMatrix<DenseMatrix<Scalar>>,Vect>;
    using DirSolver = Pastix<SparseMatrixCOO<Scalar>,Vector<Scalar>>;
    using ImplSchur = ImplicitSchur<SparseMatrixCOO<Scalar>,Vector<Scalar>,DirSolver>;
    using IterSolver = ConjugateGradient<PartOperator<ImplSchur,SparseMatrixCOO<Scalar>,Vector<Scalar>>,Vect, Precond>;
    using Solver = PartImplicitSchurSolver<SpMat,Vect, DirSolver,IterSolver>;

    Solver hybrid_solver;

    hybrid_solver.setup(parameters::A{dA},
                        parameters::verbose{verbose});

    IterSolver& iter_solver = hybrid_solver.get_solver_S();
    iter_solver.setup(parameters::max_iter{20},
                      parameters::tolerance{1e-8},
                      parameters::verbose{verbose});

    Vect X = hybrid_solver * dB;
    X.display_centralized("X");

    Vect diff = X - dX_expected;
    Real direct_diff_norm = diff.norm();
    REQUIRE( direct_diff_norm < Real{1e-6} );
  }

}
