// [[file:../../../org/maphys/solver/CastOperator.org::*Tests][Tests:1]]
#include <maphys.hpp>
#include <maphys/solver/CastOperator.hpp>
#include <catch2/catch_test_macros.hpp>

using namespace maphys;

struct MatMultDouble: public LinearOperator<DenseMatrix<double>, Vector<double>>{
  DenseMatrix<double> m;
  void setup(const DenseMatrix<double>& M){ m = M; }
  Vector<double> apply(const Vector<double>& U){ return m * U; }
};

struct MatMultFloat: public LinearOperator<DenseMatrix<float>, Vector<float>>{
  DenseMatrix<float> m;
  void setup(const DenseMatrix<float>& M){ m = M; }
  Vector<float> apply(const Vector<float>& U){
    U.display("U");
    return m * U; 
  }
};

TEST_CASE("CastOperator", "[operator]"){
  const DenseMatrix<double> A({1, 1, 1, 1, 1, 1, 1, 1, 1}, 3, 3);
  const Vector<double> U{1e8, 1, 0}; // Because for float: 1e8 + 1 = 1e8

  MatMultDouble matmultd;
  matmultd.setup(A);
  Vector<double> outd = matmultd.apply(U);
  outd.display("outd");

  CastOperator<DenseMatrix<double>, Vector<double>, MatMultFloat> castop;
  castop.setup(A);
  Vector<double> outf = castop.apply(U);
  outf.display("outf");

  const Vector<double> diff_exp{1, 1, 1};
  Vector<double> diff = outd - outf;
  diff.display("diff");
  REQUIRE(diff == diff_exp);
}
// Tests:1 ends here
