// [[file:../../../org/maphys/linalg/SVD.org::*Tests][Tests:1]]
#include <iostream>
#include <maphys.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>
#include <maphys/linalg/SVD.hpp>
#include <maphys/testing/TestMatrix.hpp>

#include <maphys/testing/catch.hpp>

TEMPLATE_TEST_CASE("SVD", "[singular_value_decomposition]", float, double, std::complex<float>, std::complex<double>){
  using namespace maphys;

  using Scalar = TestType;
  using Real = typename maphys::arithmetic_real<Scalar>::type;

  const int M = 10;
  const int N = 8;
  const int min = std::min(M, N);

  // We want to find \Sigma, U, Vt so that A = U \Sigma Vt
  DenseMatrix<Scalar> A = test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(M*M, Real{-10}, Real{10}, M, N).matrix;

  std::vector<Real> Sigma{};
  singular_value_decomposition(A, Sigma);

  DenseMatrix<Scalar> U(M, min);
  DenseMatrix<Scalar> Vt(min, N);
  std::vector<Real> Sigma2{};
  singular_value_decomposition(A, Sigma2, U, Vt);

  // In case A is too big, we multiply tolerance by (norm of A) (Frobenius norm)
  Real tol = is_precision_double<Scalar>::value ? 1e-9 : 1e-5;
  Real scal_norm = A.norm();
  tol *= scal_norm;

  Real prev_lambda = Real{1e3};
  //Vt.transpose();
  for(int j = 0; j < min; ++j){
    // Check lambdas are in asending order
    std::cerr << Sigma[j] << '\n';
    REQUIRE(std::abs(Sigma[j] - Sigma2[j]) < Real{1e-4});
    // From
    // https://icl.bitbucket.io/lapackpp/group__gesvd.html#gaae07dbf50a7e0e1995e4dff0e99e7d21
    // "The vector S of length min(m,n). The singular values of A, sorted so that S(i) >= S(i+1)"
    REQUIRE(prev_lambda >= Sigma[j]);
    prev_lambda = Sigma[j];
    /*Vector<Scalar> Uj = U.get_vect(j);
    Vector<Scalar> Vtj = Vt.get_vect(j);
    Vector<Scalar> A_v = A * Vtj;
    Scalar ut_A_v = dot(Uj, A_v);
    REQUIRE(std::abs(ut_A_v - Sigma[j]) < tol);*/
  }
}
// Tests:1 ends here
