// [[file:../../../org/maphys/precond/AbstractSchwarz.org::*Test robin robin transfer matrices][Test robin robin transfer matrices:1]]
#include <maphys.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/solver/PartSchurSolver.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <maphys/solver/Pastix.hpp>

TEST_CASE("Transfer matrices", "[AbstractSchwarz][preconditioner][transfer]"){
  using namespace maphys;

  using SparseMat = SparseMatrixCOO<double>;
  using Scalar = double;
  using Real = double;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();
  auto A = test_matrix::dist_spd_matrix<Scalar, SparseMat>(p).matrix;
  auto Xt = PartVector<Vector<Scalar>>(p);
  Xt.set_to_ones();
  auto B = A * Xt;

  const int max_iter = std::max(1, n_rows(A));
  const bool verb = (MMPI::rank() == 0);
  const Real tol = 1e-5;

  using DenseDirectSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
  using SparseDirectSolver = Pastix<SparseMat, Vector<Scalar>>;

  SECTION("Transfer matrix - scalar matrix"){
    using Pcd = RobinRobin<decltype(A), decltype(B), SparseDirectSolver>;

    ConjugateGradient<decltype(A), decltype(B), Pcd> cg_rr;

    Scalar robin_w = 2.0;

    cg_rr.setup(parameters::A{A},
                parameters::tolerance{tol},
                parameters::max_iter{max_iter},
                parameters::verbose{verb});

    cg_rr.get_preconditioner().setup(parameters::Ti<Scalar>{robin_w});

    auto X = cg_rr * B;
    if(verb) cg_rr.display("CG with Robin Robin + transfer mat (scalar)");
    REQUIRE(cg_rr.get_n_iter() >= 0);
  }

  SECTION("Transfer matrix - diagonal matrix"){
    using Pcd = RobinRobin<decltype(A), decltype(B), SparseDirectSolver>;

    ConjugateGradient<decltype(A), decltype(B), Pcd> cg_rr;

    const bool on_interface = true;
    PartVector<Vector<Scalar>> robin_diag;
    robin_diag.initialize(p, on_interface);
    robin_diag.apply_on_data([](Vector<Scalar>& v){
                               for(Size k = 0; k < n_rows(v); ++k) v[k] = 2.0;
                               v[1] = 1.4;
                             });

    cg_rr.setup(parameters::A{A},
                parameters::tolerance{tol},
                parameters::max_iter{max_iter},
                parameters::verbose{verb});

    cg_rr.get_preconditioner().setup(parameters::Ti<PartVector<Vector<Scalar>>>{robin_diag});

    auto X = cg_rr * B;
    if(verb) cg_rr.display("CG with Robin Robin + transfer mat (diagonal)");
    REQUIRE(cg_rr.get_n_iter() >= 0);
  }

  SECTION("Transfer matrix - matrix"){
    using Pcd = RobinRobin<decltype(A), decltype(B), SparseDirectSolver>;

    ConjugateGradient<decltype(A), decltype(B), Pcd> cg_rr;

    const bool on_interface = true;
    PartMatrix<SparseMat> robin_mat(p, on_interface);
    robin_mat.identity(on_interface, true);
    robin_mat *= Scalar{2};
    robin_mat.apply_on_data([](SparseMat& m){m(1, 1) = 1.867;});

    cg_rr.setup(parameters::A{A},
                parameters::tolerance{tol},
                parameters::max_iter{max_iter},
                parameters::verbose{verb});

    cg_rr.get_preconditioner().setup(parameters::Ti<PartMatrix<SparseMat>>{robin_mat});

    auto X = cg_rr * B;
    if(verb) cg_rr.display("CG with Robin Robin + transfer matrix (fully given)");
    REQUIRE(cg_rr.get_n_iter() >= 0);
  }

  SECTION("Transfer matrix - matrix on Schur"){
    using Pcd = RobinRobin<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>, DenseDirectSolver>;
    using CGType = ConjugateGradient<PartMatrix<DenseMatrix<Scalar>>, decltype(B), Pcd>;

    PartSchurSolver<decltype(A), decltype(B), SparseDirectSolver, CGType> rr_s;
    rr_s.setup(parameters::A{A});

    CGType& cg_rr = rr_s.get_solver_S();

    const bool on_interface = true;
    PartMatrix<DenseMatrix<Scalar>> robin_mat(p, on_interface);
    robin_mat.identity(on_interface, true);
    robin_mat *= Scalar{2};
    robin_mat.apply_on_data([](DenseMatrix<Scalar>& m){m(1, 1) = 1.589;});

    cg_rr.setup(parameters::tolerance{tol},
                parameters::max_iter{max_iter},
                parameters::verbose{verb});

    cg_rr.get_preconditioner().setup(parameters::Ti<PartMatrix<DenseMatrix<Scalar>>>{robin_mat});

    auto X = rr_s * B;
    if(verb) cg_rr.display("CG with Robin Robin + transfer matrix (fully given)");
    REQUIRE(cg_rr.get_n_iter() >= 0);
  }

}
// Test robin robin transfer matrices:1 ends here
