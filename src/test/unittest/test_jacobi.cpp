// [[file:../../../org/maphys/solver/Jacobi.org::*Sequential][Sequential:1]]
#include <iostream>
#include <vector>

#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen_header.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMA
#include <maphys/wrappers/armadillo/Armadillo_header.hpp>
#endif
#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMA
#include <maphys/wrappers/armadillo/Armadillo.hpp>
#endif

#include <maphys.hpp>
#include <maphys/solver/Jacobi.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "test_iterative_solver.hpp"

using namespace maphys;

template<typename Scalar, typename Matrix>
void test(const std::string mat_type){
  using Vect = typename vector_type<Matrix>::type;

  // Make a diagonal dominant matrix
  Matrix A = test_matrix::general_matrix<Scalar, Matrix>().matrix;
  for(size_t i = 0; i < n_rows(A); ++i){ A(i, i) += 10; }

  SECTION(std::string("Jacobi, matrix ") + mat_type){
    test_solver<Scalar, Matrix, Vect, Jacobi<Matrix, Vect>>(A,
							    test_matrix::simple_vector<Scalar, Vect>().vector);
  }
}

TEMPLATE_TEST_CASE("Jacobi", "[Jacobi][iterative][sequential]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;

  test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
#ifdef MAPHYSPP_USE_EIGEN
  using EigenDenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
  test<Scalar, EigenDenseMatrix>("Eigen dense");
#endif
#ifdef MAPHYSPP_USE_ARMA
  test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
#endif
} // TEMPLATE_PRODUCT_TEST_CASE
// Sequential:1 ends here
