// [[file:../../../org/maphys/solver/LinearOperator.org::*Tests][Tests:1]]
#include <maphys.hpp>
#include <maphys/solver/LinearOperator.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>

using namespace maphys;

struct AddX: public LinearOperator<double, double>{
  double m_x;
  void setup(const double& x){ m_x = x; }
  double apply(const double& t){ return m_x + t; }
};

struct TimesX: public LinearOperator<double, double>{
  double m_x;
  void setup(const double& x){ m_x = x; }
  double apply(const double& t){ return m_x * t; }
};

TEST_CASE("LinearOperator", "[operator]"){

  AddX add10;
  TimesX times3, times10;

  add10.setup(10);
  times3.setup(3);

  auto subtract = [](const double& d1, const double& d2){ return d1 - d2; };

  CombinedLinearOperator<double, double> add10MinusTimes3(add10, times3, subtract);

  SECTION("Linear operator: add 10"){
    double result;
    result = add10 * double{1};
    REQUIRE(result == double{11});
    result = add10 * double{-13};
    REQUIRE(result == double{-3});
  }

  SECTION("Linear operator: times 3"){
    double result;
    result = times3 * double{1};
    REQUIRE(result == double{3});
    result = times3 * double{-10};
    REQUIRE(result == double{-30});
  }

  SECTION("Linear operator combined: add10MinusTimes3"){
    double result;
    result = add10MinusTimes3 * double{1};
    // (1 + 10) - 3 * 1 = 8
    REQUIRE(result == double{8});
    // (-10 + 10) - 3 * (-10) = 30
    result = add10MinusTimes3 * double{-10};
    REQUIRE(result == double{30});
  }

  SECTION("Operator + on two linear operator"){
    auto add10times10 = add10 + times10;
    add10times10.setup(double{10});
    // (10 + 3) + (10 * 3) = 43
    double result = add10times10 * double{3};
    REQUIRE(result == double{43});
  }
}
// Tests:1 ends here
