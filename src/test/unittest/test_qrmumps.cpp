// [[file:../../../org/maphys/solver/QrMumps.org::*Tests][Tests:1]]
#include <iostream>

#include <maphys.hpp>
#include <maphys/solver/QrMumps.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>

using namespace maphys;

TEST_CASE("Qrumps", "[qrmumps][direct][least_squares]"){
  using Scalar = double;
  using Real = double;
  using Vect = Vector<Scalar>;
  using MatCOO = SparseMatrixCOO<Scalar>;
  using DenseMat = DenseMatrix<Scalar>;

  const Real tol = arithmetic_tolerance<Scalar>::value;

  SECTION("Least squares (qr_mumps example) with A(7,5)"){
    const MatCOO A({0, 0, 0, 1, 2, 2, 3, 3, 4, 4, 5, 6, 6},
                   {0, 2, 4, 1, 2, 4, 0, 3, 3, 4, 1, 0, 2},
                   {1.0, 2.0, 3.0, 1.0, 1.0, 2.0, 4.0, 1.0, 5.0, 1.0, 3.0, 6.0, 1.0},
                   7, 5);

    const Vect B{22.0, 5.0, 13.0, 8.0, 25.0, 5.0, 9.0};
    const Vect Xe{1.0, 2.0, 3.0, 4.0, 5.0};
    QrMumps<MatCOO,Vect> qrm(A);
    Vect X = qrm * B;
    X.display("X");
    REQUIRE((Xe - X).norm() < tol);
  }

  SECTION("Least squares (qr_mumps example) with A(7,5) - 2 RHS"){
    const MatCOO A({0, 0, 0, 1, 2, 2, 3, 3, 4, 4, 5, 6, 6},
                   {0, 2, 4, 1, 2, 4, 0, 3, 3, 4, 1, 0, 2},
                   {1.0, 2.0, 3.0, 1.0, 1.0, 2.0, 4.0, 1.0, 5.0, 1.0, 3.0, 6.0, 1.0},
                   7, 5);

    const DenseMat B({22.0, 5.0, 13.0, 8.0, 25.0, 5.0, 9.0,
                      15.0, 3.0, 10.0, -4.0, 4.0, 9.0, -4.0}, 7, 2);
    const DenseMat Xe({1.0, 2.0, 3.0, 4.0, 5.0,
                       -1.0, 3.0, 2.0, 0.0, 4.0}, 5, 2);
    QrMumps<MatCOO,DenseMat> qrm(A);
    DenseMat X = qrm * B;
    X.display("X");
    REQUIRE((Xe - X).norm() < tol);
  }

  SECTION("Square solve with QR"){
    const MatCOO A = test_matrix::general_matrix<Scalar, MatCOO>().matrix;
    const Vect Xe = test_matrix::simple_vector<Scalar, Vect>().vector;
    const Vect B = A * Xe;
    QrMumps<MatCOO,Vect> qrm(A);
    Vect X = qrm * B;
    X.display("X");
    REQUIRE((Xe - X).norm() < tol);
  }

  SECTION("Square solve with Cholesky"){
    const MatCOO A = test_matrix::laplacian_1D_matrix<Scalar, MatCOO>(4).matrix;
    const Vect Xe = test_matrix::simple_vector<Scalar, Vect>().vector;
    const Vect B = A * Xe;

    QrMumps<MatCOO,Vect> qrm(A);
    Vect X = qrm * B;

    X.display("X");    
    REQUIRE((Xe - X).norm() < tol);
  }

  SECTION("Square solve with Cholesky - 3 RHS"){
    MatCOO A = test_matrix::laplacian_1D_matrix<Scalar, MatCOO>(4).matrix;

    const DenseMat Xe({1, 2, 3, 4, 
                       -1, -3, 0, 1,
                       10, -1, 4, 2}, 4, 3);
    const DenseMat B = A * Xe;
    QrMumps<MatCOO,DenseMat> qrm(A);
    Vect X = qrm * B;
    X.display("X");
    REQUIRE((Xe - X).norm() < tol);
  }

  SECTION("Triangular solves (SPD matrix)"){
    const MatCOO A = test_matrix::laplacian_1D_matrix<Scalar, MatCOO>(4).matrix;

    QrMumps<MatCOO, Vect> qrm;
    qrm.setup(A);
    A.display("A");

    // Perform the solve in two steps
    // 1-  y <- solve L . y = b
    // 2-  x <- solve L^T x = y
    // And compare x with x_exp use to generate b = A * x_exp
    const Vect x_exp{0,1,4,9};
    const Vect b = A * x_exp;
    b.display("b");

    auto y = qrm.triangular_solve(b, MatrixStorage::lower, false);
    y.display("y");

    auto x = qrm.triangular_solve(y, MatrixStorage::lower, true);
    x.display("x");
    double direct_err_x = (x - x_exp).norm();
    REQUIRE(direct_err_x < 1e-12);
  }
} // TEST_CASE
// Tests:1 ends here
