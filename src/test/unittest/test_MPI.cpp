// [[file:../../../org/maphys/dist/MPI.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#include <maphys.hpp>
#include "maphys/dist/MPI.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("MMPI", "[mpi]"){

  using C = std::complex<float>;
  using Z = std::complex<double>;

  using namespace maphys;

  int rank = MMPI::rank(MPI_COMM_WORLD);
  int size = MMPI::size(MPI_COMM_WORLD);

  int    sbuf_i[10], rbuf_i[10];
  float  sbuf_s[10], rbuf_s[10];
  double sbuf_d[10], rbuf_d[10];
  C      sbuf_c[10], rbuf_c[10];
  Z      sbuf_z[10], rbuf_z[10];

  for(auto i = 0; i < 10; ++i){
    float  sval = (float)  i * rank;
    double dval = (double) i * rank;
    sbuf_i[i] = i * rank;
    sbuf_s[i] = sval;
    sbuf_d[i] = dval;
    sbuf_c[i] = {sval, -sval};
    sbuf_z[i] = {dval, -dval};
  }

  int dest = (rank + 1) % size;

  MMPI::send<int>   (sbuf_i, 10, 11, dest, MPI_COMM_WORLD);
  MMPI::send<float> (sbuf_s, 10, 22, dest, MPI_COMM_WORLD);
  MMPI::send<double>(sbuf_d, 10, 33, dest, MPI_COMM_WORLD);
  MMPI::send<C>     (sbuf_c, 10, 44, dest, MPI_COMM_WORLD);
  MMPI::send<Z>     (sbuf_z, 10, 55, dest, MPI_COMM_WORLD);

  int src = (rank + size - 1) % size;

  MMPI::recv<int>   (rbuf_i, 10, 11, src, MPI_COMM_WORLD);
  MMPI::recv<float> (rbuf_s, 10, 22, src, MPI_COMM_WORLD);
  MMPI::recv<double>(rbuf_d, 10, 33, src, MPI_COMM_WORLD);
  MMPI::recv<C>     (rbuf_c, 10, 44, src, MPI_COMM_WORLD);
  MMPI::recv<Z>     (rbuf_z, 10, 55, src, MPI_COMM_WORLD);

  SECTION("Send -  recv"){
    for(auto i = 0; i < 10; ++i){
      float  sval = (float)  i * src;
      double dval = (double) i * src;
      REQUIRE( rbuf_i[i] == (src * i) );
      REQUIRE(std::abs(rbuf_s[i] - src * i ) < 1e-6);
      REQUIRE(std::abs(rbuf_d[i] - src * i ) < 1e-14);
      REQUIRE(std::abs(rbuf_c[i] - C{sval, -sval}) < 1e-6);
      REQUIRE(std::abs(rbuf_z[i] - Z{dval, -dval}) < 1e-14);
    }
  }

  std::vector<Message<double>> mess(5);
  // On double only
  double send_buffers[5][5];
  for(auto i = 0; i < 5; ++i){
    for(auto j = 0; j < 5; ++j){
      send_buffers[i][j] = (double) (5 * 5 * dest + 5 * rank + j);
      mess[i].buffer = send_buffers[i];
      mess[i].destination = dest;
      mess[i].size = 5;
    }
  }

  auto receptor = MMPI::isendMessages(mess, 5);

  SECTION("isendMessages"){
    int source, tag;
    while( receptor.get_n_recv() ){
      std::vector<double> received = receptor(source, tag);
      REQUIRE( source == src );
      for(auto j = 0; j < 5; ++j){
        REQUIRE( received[j] == (5 * 5 * rank + 5 * source + j));
      }
    }
    receptor.wait();
  }

  int sbuf1[] = {1  * (rank + 1), 2  * (rank + 1), 3  * (rank + 1)};
  int sbuf2[] = {10 * (rank + 1), 20 * (rank + 1), 30 * (rank + 1), 40 * (rank + 1)};
  int rbuf1[] = {0, 0, 0};
  int rbuf2[] = {0, 0, 0, 0};
  std::vector<Message<int>> smess(2);
  std::vector<Message<int>> rmess(2);

  smess[0] = Message<int>(sbuf1, 3, dest, 189);
  smess[1] = Message<int>(sbuf2, 4, dest, 2332);

  rmess[0] = Message<int>(rbuf1, 3, src, 189);
  rmess[1] = Message<int>(rbuf2, 4, src, 2332);

  auto receptor_SR = MMPI::isendRecvMessages(smess, rmess);
  receptor_SR.wait();
  MMPI::barrier();

  SECTION("isendRecvMessages"){
    for(auto i = 0; i < 3; ++i){
      //std::cout << rbuf1[i] << " / " << i * (src + 1) << '\n';
      REQUIRE(rbuf1[i] == (i+1) * (src + 1));
      REQUIRE(rbuf2[i] == 10 * (i+1) * (src + 1));
    }
    REQUIRE(rbuf2[3] == 10 * 4 * (src + 1));
  }

  SECTION("Serialization"){
    using Triplet = std::tuple<int, int, double>;
    const std::vector<Triplet> tosend{{3, 3, 12.44}, {4, 2, -55.3}, {1, 9, 4627.104}};
    std::vector<char> s = MMPI::serialize<Triplet>(tosend);
    MPI_Request req;
    MMPI::isend<char>(s.data(), static_cast<int>(s.size()), 11, dest, req, MPI_COMM_WORLD);

    MPI_Status stat = MMPI::probe(src, 11, MPI_COMM_WORLD);
    int count = MMPI::get_count<char>(stat);
    std::vector<char> r(count);
    MMPI::recv<char>(r.data(), count, 11, src, MPI_COMM_WORLD);

    std::vector<Triplet> received = MMPI::deserialize<Triplet>(r);
    REQUIRE(tosend == received);
  }

} //TEST_CASE
// Tests:1 ends here
