// [[file:../../../org/maphys/solver/GCR.org::*Sequential][Sequential:1]]
#include <iostream>
#include <vector>

#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen_header.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMA
#include <maphys/wrappers/armadillo/Armadillo_header.hpp>
#endif
#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMA
#include <maphys/wrappers/armadillo/Armadillo.hpp>
#endif

#include <maphys.hpp>
#include <maphys/solver/GCR.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "test_iterative_solver.hpp"

using namespace maphys;

template<typename Scalar, typename Matrix>
void test(const std::string mat_type){
  using Vect = typename vector_type<Matrix>::type;

  SECTION(std::string("GCR, no preconditioner, matrix ") + mat_type){
    test_solver<Scalar, Matrix, Vect, GCR<Matrix, Vect>>(
							 test_matrix::general_matrix<Scalar, Matrix>().matrix,
							 test_matrix::simple_vector<Scalar, Vect>().vector);
  }

  SECTION(std::string("GCR, diagonal preconditioner, matrix ") + mat_type){
    using PcdType = DiagonalPrecond<Matrix, Vect>;
    test_solver<Scalar, Matrix, Vect, GCR<Matrix, Vect, PcdType>>(
								  test_matrix::general_matrix<Scalar, Matrix>().matrix,
								  test_matrix::simple_vector<Scalar, Vect>().vector);
  }
}

TEMPLATE_TEST_CASE("GCR", "[GCR][iterative][sequential]", float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;

  test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
  test<Scalar, SparseMatrixCOO<Scalar>>("SparseMatrixCOO");
  test<Scalar, SparseMatrixCSC<Scalar>>("SparseMatrixCSC");

#ifdef MAPHYSPP_USE_EIGEN
  using EigenDenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
  using EigenSparseMatrix = Eigen::SparseMatrix<Scalar>;

  test<Scalar, EigenDenseMatrix>("Eigen dense");
  test<Scalar, EigenSparseMatrix>("Eigen sparse");
#endif // MAPHYSPP_USE_EIGEN

#ifdef MAPHYSPP_USE_ARMA
  test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
  test<Scalar, arma::SpMat<Scalar>>("Armadillo sparse");
#endif
} // TEMPLATE_TEST_CASE
// Sequential:1 ends here
