// [[file:../../../org/maphys/wrappers/Eigen/EigenDenseSolver.org::*Tests][Tests:1]]
#include <maphys/wrappers/Eigen/EigenDenseSolver.hpp>
#include <maphys.hpp>
#include <maphys/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace maphys;

TEMPLATE_TEST_CASE("EigenDenseSolver", "[dense][sequential][Eigen]", float, double/*, std::complex<float>, std::complex<double>*/){
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;

  using E_Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
  using E_DenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
  E_DenseMatrix A = test_matrix::general_matrix<Scalar, E_DenseMatrix>().matrix;

  E_DenseMatrix E_A_spd(4,4);
  E_A_spd << 3, 2, 2, 1,
    0, 4, 1, 1,
    0, 0, 3, 2,
    0, 0, 0, 4;
  E_A_spd *= E_A_spd.transpose();
  E_DenseMatrix A_spd(std::move(E_A_spd));
  MatrixProperties<Scalar> prop_spd;
  prop_spd.set_spd(MatrixStorage::full);

  E_DenseMatrix E_A_sym(4,4);
  E_A_sym << 1, 2, 3, 4,
    2, 2, 3, 1,
    3, 3, 4, 3,
    4, 1, 3, 1;
  E_DenseMatrix A_sym(std::move(E_A_sym));
  MatrixProperties<Scalar> prop_sym;
  prop_sym.set_property(MatrixSymmetry::symmetric);

  const Real tol = arithmetic_tolerance<Scalar>::value;

  SECTION("Single solve Ax=b, A general"){
    E_Vector b(4); b << 1,2,3,4;
    EigenDenseSolver<E_DenseMatrix, E_Vector> s;
    s.setup(A);

    E_Vector x = s * b;

    E_Vector direct_err = A * x - b;
    REQUIRE(direct_err.norm() <= tol);
  }

  SECTION("Single solve Ax=b, A spd"){
    E_Vector b(4); b << 1,2,3,4;
    EigenDenseSolver<E_DenseMatrix, E_Vector> s;
    s.setup(A_spd, prop_spd);

    E_Vector x = s * b;

    E_Vector direct_err = A_spd * x - b;
    REQUIRE(direct_err.norm() <= tol);
  }

  SECTION("Single solve Ax=b, A sym"){
    E_Vector b(4); b << 1,2,3,4;
    EigenDenseSolver<E_DenseMatrix, E_Vector> s;
    s.setup(A_sym, prop_sym);

    E_Vector x = s * b;

    E_Vector direct_err = A_sym * x - b;
    REQUIRE(direct_err.norm() <= tol);
  }

  SECTION("Consecutive solve Ax=b with different b"){
    E_Vector b1(4); b1 << 1,2,3,4;
    E_Vector b2(4); b2 << 2,2,3,-7;
    E_Vector b3(4); b3 << 1,-22,33,4;
    EigenDenseSolver<E_DenseMatrix, E_Vector> s;
    s.setup(A);

    E_Vector x1 = s * b1;
    E_Vector x2 = s * b2;
    E_Vector x3 = s * b3;

    E_Vector direct_err1 = A * x1 - b1;
    REQUIRE(direct_err1.norm() <= tol);
    E_Vector direct_err2 = A * x2 - b2;
    REQUIRE(direct_err2.norm() <= tol);
    E_Vector direct_err3 = A * x3 - b3;
    REQUIRE(direct_err3.norm() <= tol);
  }

  SECTION("Solve multirhs AX=B, A full, B is a matrix"){
    E_DenseMatrix E_B(4,3);
    E_B << 1,  2, -10, 0,
      2,  3,  2, -1,
      4,  2,  3,  10;
    E_DenseMatrix B(std::move(E_B));
    EigenDenseSolver<E_DenseMatrix, E_DenseMatrix> s;
    s.setup(A);
    E_DenseMatrix X = s * B;
    E_DenseMatrix direct_err = A * X - B;
    REQUIRE(direct_err.norm() <= tol);
  }
}
// Tests:1 ends here
