// [[file:../../../org/maphys/linalg/EigenSolver.org::*Tests][Tests:1]]
#include <iostream>
#include <maphys.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>
#include <maphys/linalg/EigenSolver.hpp>
#include <maphys/testing/TestMatrix.hpp>

#include <maphys/testing/catch.hpp>

TEMPLATE_TEST_CASE("EigenSolver", "[eigen_problem]", float, double, std::complex<float>, std::complex<double>){
  using namespace maphys;

  using Scalar = TestType;
  using Real = typename maphys::arithmetic_real<Scalar>::type;

  const int M = 10;
  const int n_v = 6;

  // We want to find U so that A U = LAM B U
  // With U the n_v eigenvectors associated to the smallest eigenvalues
  DenseMatrix<Scalar> A = test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(M*M, Real{-10}, Real{10}, M, M).matrix;
  DenseMatrix<Scalar> B = test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(M*M, Real{-10}, Real{10}, M, M).matrix;
  A *= A.t();
  B *= B.t();
  A.set_property(MatrixSymmetry::symmetric);
  B.set_property(MatrixSymmetry::symmetric);

  std::vector<Real> lambda;

  DenseMatrix<Scalar> U = generalized_eigen_smallest<DenseMatrix<Scalar>,Vector<Scalar>>(A, B, n_v, lambda);

  // In case A or B is too big, we multiply tolerance by (norm of A + norm of B) (Frobenius norm)
  Real tol = is_precision_double<Scalar>::value ? 1e-9 : 1e-5;
  Real scal_norm = A.norm() + B.norm();
  //std::cerr << scal_norm << '\n';
  tol *= scal_norm;

  Real prev_lambda = Real{-1};
  for(int j = 0; j < n_v; ++j){
    // Check lambdas are in asending order
    REQUIRE(lambda[j] > prev_lambda);
    prev_lambda = lambda[j];
    Vector<Scalar> Uj = U.get_vect_view(j);
    Vector<Scalar> A_u = A * Uj;
    Vector<Scalar> l_B_u = static_cast<Scalar>(lambda[j]) * (B * Uj);

    //std::cout << "Lambda : " << lambda[j] << '\n';
    Vector<Scalar> diff = A_u - l_B_u;
    //diff.display("Diff");
    REQUIRE(diff.norm() < tol);
  }
}
// Tests:1 ends here
