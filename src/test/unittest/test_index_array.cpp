// [[file:../../../org/maphys/utils/IndexArray.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <map>

#include <maphys.hpp>
#include "maphys/utils/IndexArray.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("IndexArray", "[index_array]"){
  using namespace maphys;

  const IndexArray<double> base = {1.0, 11.0, 3.0, 1000.0, 2.0, 4.0};
  const IndexArray<double> b{-1.0, -2.0, -3.0, -4.0};
  const IndexArray<int> filter{0, 2, 4, 5};

  SECTION("a[ IndexArray<int> ] = b"){
    IndexArray<double> a1 = base;
    const IndexArray<double> a1_exp{ -1.0, 11.0, -2.0, 1000.0, -3.0, -4.0};
    a1[filter] = b;
    REQUIRE( a1 == a1_exp );
  }

  const IndexArray<bool> bool_filter{true, false, true, false, true, true};
  const IndexArray<double> a2_exp{-1.0, 11.0, -2.0, 1000.0, -3.0, -4.0};
  SECTION("a[ IndexArray<bool> ] = b"){
    IndexArray<double> a2 = base;
    a2[bool_filter] = b;
    REQUIRE( a2 == a2_exp);
  }

  SECTION("a[ IndexArray<int> ] = 4.4"){
    IndexArray<double> a4 = base;
    const IndexArray<double> a4_exp{4.4, 11.0, 4.4, 1000.0, 4.4, 4.4};
    a4[filter] = 4.4;
    REQUIRE(a4 == a4_exp);
  }

  const IndexArray<double> a567_exp{1.0, 3.0, 2.0, 4.0};
  SECTION("a = base[ IndexArray<int> ]"){
    IndexArray<double> a5 = base[filter];
    IndexArray<double> a5_b;
    a5_b = base[filter];
    REQUIRE(a5 == a567_exp);
    REQUIRE(a5_b == a567_exp);
  }

  SECTION("a = a[ IndexArray<int> ]"){
    IndexArray<double> a{1.0, 2.0, 3.0, 4.0};
    const IndexArray<long int> reorder{3, 1, 0, 2};
    const IndexArray<double> a_reoredered{4.0, 2.0, 1.0, 3.0};
    a = a[reorder];
    REQUIRE(a == a_reoredered);
  }

  SECTION("a = base[ IndexArray<bool> ]"){
    IndexArray<double> a6 = base[bool_filter];
    IndexArray<double> a6_b;
    a6_b= base[bool_filter];
    REQUIRE( a6 == a567_exp );
    REQUIRE( a6_b == a567_exp );
  }

  SECTION("a[ IndexArray<int> ] += b"){
    IndexArray<double> a8 = base;
    const IndexArray<double> a8_exp{0.0, 11.0, 1.0, 1000.0, -1.0, 0.0};
    a8[filter] += b;
    REQUIRE( a8 == a8_exp );
  }

  SECTION("unique"){
    IndexArray<double> a9{1.0, 10.0, 100.0, 10.0, 2.0, 1.0, 3.0};
    const IndexArray<double> a9_exp{1.0, 2.0, 3.0, 10.0, 100.0};
    a9.unique();
    REQUIRE(a9 == a9_exp);
  }

  SECTION("insert"){
    IndexArray<double> a10{1.0, 2.0, -1.0};
    const IndexArray<double> b10{2.0, 3.0, -1.0, 0.0};
    const IndexArray<double> a10_exp{1.0, 2.0, -1.0, 2.0, 3.0, -1.0, 0.0};
    a10.insert(b10);
    REQUIRE(a10 == a10_exp);
  }

  SECTION("union_with"){
    IndexArray<double> a11{1.0, 2.0, -1.0};
    const IndexArray<double> b11{2.0, 3.0, -1.0, 0.0};
    const IndexArray<double> a11_exp{-1.0, 0.0, 1.0, 2.0, 3.0};
    a11.union_with(b11);
    REQUIRE(a11 == a11_exp);
  }

  SECTION("erase_after"){
    IndexArray<double> a12{1.0, 10.0, 100.0, 10.0, 2.0, 1.0, 3.0};
    const IndexArray<double> a12_exp{1.0, 10.0, 100.0};
    a12.erase_after(3);
    REQUIRE(a12 == a12_exp);
  }

  SECTION("where"){
    IndexArray<int> a13 = base.where( [](const double &d){ return d < 10.0; } );
    const IndexArray<int> a13_exp{0,2,4,5};
    REQUIRE(a13 == a13_exp);
  }

  SECTION("Sort"){
    IndexArray<int> a{1, 3, 4, 18, 2, 34, 1, 2};
    IndexArray<int> a_sorted{1, 1, 2, 2, 3, 4, 18, 34};
    a.sort();
    REQUIRE(a == a_sorted);
  }

  SECTION("Index sort"){
    IndexArray<int> a{4, 2, 1, 3, 6, 5};
    IndexArray<int> idx{2, 1, 3, 0, 5, 4};
    IndexArray<int> a_idxsort = a.argsort();
    REQUIRE(a_idxsort == idx);
  }

  SECTION("Multilevel index sorting"){
    IndexArray<int>   a1{ 0,  1,  2,  1,  0,  0,  3,  4,  0,  3};
    IndexArray<float> a2{12, 16, 10, 14, 10, 11, 12, 10, 11, 12};
    IndexArray<int>   a3{20, 21, 21, 20, 23, 23, 21, 20, 22, 20};
    const IndexArray<int> idx_exp{4, 8, 5, 0, 3, 1, 2, 9, 6, 7};
    IndexArray<int> idx = multilevel_argsort(a1, a2, a3);
    //IndexArray<int> aa1 = a1[idx]; aa1.display("a1");
    //IndexArray<float> aa2 = a2[idx]; aa2.display("a2");
    //IndexArray<int> aa3 = a3[idx]; aa3.display("a3");
    REQUIRE(idx == idx_exp);
  }

  SECTION("Search sorted"){
    IndexArray<double> a{4.0, 1.0, 2.0, 1.5, 3.0};
    // Sorted 1, 1.5, 2, 3, 4
    const IndexArray<double> values{1.5, 4.0, 2.0};
    const IndexArray<int> expected{1, 4, 2};
    IndexArray<int> found = sort_and_search(a, values);
    REQUIRE(found == expected);
  }

  SECTION("Find index"){
    const IndexArray<double> a{4.0, 1.0, 2.0, 1.5, 3.0};
    REQUIRE(a.find_idx(4.0) == 0);
    REQUIRE(a.find_idx(1.5) == 3);
    REQUIRE(a.find_idx(3.0) == 4);
    REQUIRE(a.find_idx(3.14) == 5); // Not found
  }

  SECTION("Term by term operations"){
    IndexArray<double> a{1.0, 2.0, 3.0};
    const IndexArray<double> z{1.0, 2.0, 3.0};

    a += z;
    const IndexArray<double> apz{2.0, 4.0, 6.0};
    REQUIRE(a == apz);

    a -= z;
    const IndexArray<double> amz{1.0, 2.0, 3.0};
    REQUIRE(a == amz);

    a *= z;
    const IndexArray<double> atz{1.0, 4.0, 9.0};
    REQUIRE(a == atz);

    a /= z;
    const IndexArray<double> adz{1.0, 2.0, 3.0};
    REQUIRE(a == adz);
  }

  SECTION("One value operations"){
    IndexArray<double> a{1.0, 2.0, 3.0};
    const double z = 10.0;

    a += z;
    const IndexArray<double> apz{11.0, 12.0, 13.0};
    REQUIRE(a == apz);

    a -= z;
    const IndexArray<double> amz{1.0, 2.0, 3.0};
    REQUIRE(a == amz);

    a *= z;
    const IndexArray<double> atz{10.0, 20.0, 30.0};
    REQUIRE(a == atz);

    a /= z;
    const IndexArray<double> adz{1.0, 2.0, 3.0};
    REQUIRE(a == adz);
  }

  SECTION("arange - int"){
    IndexArray<int> a1 = arange(3);
    IndexArray<int> a1_c{0, 1, 2};
    REQUIRE(a1 == a1_c);
    IndexArray<int> a2 = arange(3, 7);
    IndexArray<int> a2_c{3, 4, 5, 6};
    REQUIRE(a2 == a2_c);
    IndexArray<int> a3 = arange(7, 3, -2);
    IndexArray<int> a3_c{7, 5};
    REQUIRE(a3 == a3_c);
    IndexArray<int> a4 = arange(7, 2, -2);
    IndexArray<int> a4_c{7, 5, 3};
    REQUIRE(a4 == a4_c);
  }

  SECTION("arange - double"){
    IndexArray<double> a1 = arange(3.1);
    IndexArray<double> a1_c{0.0, 1.0, 2.0, 3.0};
    REQUIRE(a1 == a1_c);
    IndexArray<double> a2 = arange(3.1, 7.0);
    IndexArray<double> a2_c{3.1, 4.1, 5.1, 6.1};
    REQUIRE(a2 == a2_c);
    IndexArray<double> a3 = arange(7.1, 3.0, -2.0);
    IndexArray<double> a3_c{7.1, 5.1, 3.1};
    //REQUIRE(a3 == a3_c); // For some reason result is correct but check does not pass
    REQUIRE(a3.size() == a3_c.size());
    IndexArray<double> a4 = arange(7.1, 3.1, -2.0);
    IndexArray<double> a4_c{7.1, 5.1};
    REQUIRE(a4 == a4_c);
  }
} //TEST_CASE
// Tests:1 ends here
