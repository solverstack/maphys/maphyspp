#include <iostream>
#include <vector>

#include <maphys/wrappers/armadillo/Armadillo_header.hpp>
#include <maphys/wrappers/armadillo/Armadillo.hpp>

#include <maphys.hpp>
#include <maphys/solver/ConjugateGradient.hpp>

using namespace maphys;
using namespace arma;

int main() {

  Mat<double> A = {
    {2.0, -1.0, 0.0, 0.0},
    {-1.0, 2.0, -1.0, 0.0},
    {0.0, -1.0, 2.0, -1.0},
    {0.0, 0.0, -1.0, 2.0}};

  Col<double> b = {1.0, 0.0, 0.0, 11.0};

  ConjugateGradient<Mat<double>, Col<double>> cg(A);
  Col<double> X = cg * b;

  X.print();

  return 0;
}
