#include <iostream>
#include <vector>

#include <maphys/wrappers/Eigen/Eigen_header.hpp>
#include <maphys/wrappers/Eigen/Eigen.hpp>

#include <maphys.hpp>
#include <maphys/solver/ConjugateGradient.hpp>

int main() {

  const int m = 4;

  Eigen::MatrixXd e_mat(m, m);
  e_mat << 2.0, -1.0,  0.0,  0.0,
    -1.0,  2.0, -1.0,  0.0,
    0.0, -1.0,  2.0, -1.0,
    0.0,  0.0, -1.0,  2.0;

  Eigen::VectorXd e_B(m);
  e_B << 1.0, 0.0, 0.0, 11.0;

  maphys::ConjugateGradient<Eigen::MatrixXd, Eigen::VectorXd> cg(e_mat);
  Eigen::VectorXd e_X = cg * e_B;

  cg.display();
  std::cout << e_X.transpose() << '\n';

  return 0;
}
