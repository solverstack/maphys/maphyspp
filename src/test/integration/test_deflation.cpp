#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0

#include <maphys.hpp>
#include <maphys/IO/ReadParam.hpp>

#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/precond/TwoLevelAbstractSchwarz.hpp>
#include <maphys/precond/EigenDeflatedPcd.hpp>
#include <maphys/loc_data/DenseMatrix.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/testing/TestMatrix.hpp>

#include <maphys/solver/Pastix.hpp>


using namespace maphys;
using Scalar = double;

int main() {

  MMPI::init();
  //std::cout << std::setprecision(1);
  Timer<0> tt("Total time");
  const int rank = MMPI::rank();

  using Vect = DenseMatrix<Scalar,-1>;//typename vector_type<MatType>::type;//
  using MatType = SparseMatrixCOO<Scalar>;
  using M0_type = EigenDeflatedPcd<MatType, Vect>;
  //using M1_type = Pastix<MatType, Vect>;
  //using Precond = TwoLevelAbstractSchwarz<MatType, Vect, M0_type, M1_type, true>;
  using Precond = M0_type;

  MatType A;
  A.from_matrix_market_file("../../matrices/bcsstk09.mtx");
  A.set_spd(MatrixStorage::lower);
  A.order_indices();

  Vect b = test_matrix::random_matrix<Scalar, Vect>(n_cols(A), -1e3, 1e3, n_cols(A), 1).matrix;
  for(Size i = 0; i < n_rows(b); ++i) b(i, 0) = Scalar{1.0};
  //b = A * b;

  if (rank == 0){
    std::cout << " - Size of global K: " << n_rows(A) << '\n';
    std::cout << "------\n";
  }

  MAPHYSPP_ASSERT(A.is_spd(), "A is not SPD!!!");

  auto setup_geneo = [&](const MatType& mat, Precond& pcd){
    pcd.setup(mat, 50, 12, 3);
    /*pcd.set_n_vect(6);
    pcd.set_n_vect_solve(12);
    pcd.set_nev(3);
    pcd.setup(mat);*/
  };

  auto initguess = [](const MatType& mat, const Vect& rhs, Precond& pcd){
    (void) mat;
    return pcd.init_guess(rhs);
  };

  ConjugateGradient<MatType, Vect, Precond> cg;
  cg.setup(parameters::A{A},
           parameters::tolerance{1e-8},
           parameters::max_iter{500},
           parameters::verbose{false},
           parameters::setup_pcd<std::function<void(const MatType&, Precond&)>>{setup_geneo},
           parameters::setup_init_guess<std::function<Vect(const MatType&, const Vect&, Precond&)>>{initguess});


  if(rank == 0){
    std::cout << "CG solve with: simple deflation\n";
  }

  for(int i = 0; i < 5; i++){
    auto X = cg * b;
    std::cout << (b - A*X).norm() / b.norm() << '\n';
    //b += (X / X.norm());
    cg.display();
  }
  //Timer<1> t_maphys("solver");
  //t_maphys.stop();

  MMPI::finalize();

  return 0;
}
