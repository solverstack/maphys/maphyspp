#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1011
#define TIMER_LEVEL_MIN 0

#include <chrono>
#include <thread>

#include <maphys.hpp>
#include <maphys/solver/Fabulous.hpp>
#include <maphys/dist/MPI.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/precond/TruncateColumns.hpp>
#include <maphys/solver/Pastix.hpp>

#include <random>

using namespace maphys;

void usage(char ** argv){
  std::cerr << "Usage: " << std::string(argv[0]) << " s/d matrix_file";
}

template<typename Solver, typename Scalar, typename Matrix, typename Vector>
Vector solve(const Matrix& A, const Vector& b){
  using Real = typename arithmetic_real<Scalar>::type;

  const Real tol_0 = 1.0e-8;
  const std::vector<Real> tol{tol_0};


  Solver fab;
  fab.setup(fabulous_params::A<Matrix>{A},
	    fabulous_params::tolerances<std::vector<Real>>{tol},
	    fabulous_params::max_iter<int>{1800},
	    fabulous_params::max_krylov_space<int>{10},
	    fabulous_params::ortho_type{fabulous::OrthoType::BLOCK},
	    fabulous_params::ortho_scheme{fabulous::OrthoScheme::MGS});
  //fab.setup(fabulous_params::verbose<bool>{false},fabulous_params::deflated_restart{fabulous::deflated_restart<Scalar>(5, Scalar{0})});

  Vector X = fab * b;

  //fab.display();
  const int rank = MMPI::rank();
  fabulous::Logger log = fab.get_last_log();
  const double* data = log.write_down_array();
  double time_from_start = 0.0;
  Vector R = b - A * X;
  auto Rn = R.cwise_norm();
  auto bn = b.cwise_norm();
  const double backward_error = Rn[0] / bn[0];
  std::this_thread::sleep_for(std::chrono::milliseconds(500 * rank));
  auto iter = log.get_iterations();
  for (int i = 0; i < log.get_nb_iterations(); ++i) {
    time_from_start += data[3*i + 2];
    std::cout << rank << ',' << iter[i].nb_mvp << "," << data[3*i + 0] << "," << data[3*i + 1] << "," << time_from_start << "\n";
  }

  std::cerr << "Rank: " << rank << ", Cumulated iterations: " << fab.get_n_iter() << ", Backward Error: " << backward_error << '\n';
  return X;
}

template<typename Scalar>
void test(char ** argv){
  int n_subdomains = MMPI::size();
  std::string mat_filename = std::string(argv[2]);

  int seed = 1337;
  std::mt19937 gen(seed);
  std::uniform_real_distribution<> dis(double{-1}, double{1});

  auto p = bind_subdomains(n_subdomains);

  using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
  using loc_data = DenseMatrix<Scalar>;
  using PVect = PartVector<loc_data>;

  PMat A(p);
  PVect b(p);
  PVect bt(p);
  {
    PartMatrix<SparseMatrixCOO<Scalar>> A_coo(p);
    load_subdomains_and_data(mat_filename, n_subdomains, p, A_coo, b);
    for(int sd_id : A_coo.get_sd_ids()){
      auto& loc_A_coo = A_coo.get_local_matrix(sd_id);
      loc_A_coo.fill_tri_to_full();
      loc_A_coo.set_property(MatrixSymmetry::general);
      A.add_subdomain(sd_id, loc_A_coo.to_csc());
    }
  }
  {
    PartMatrix<SparseMatrixCOO<Scalar>> A_coo(p);
    load_subdomains_and_data(mat_filename, n_subdomains, p, A_coo, bt);
  }

  //const bool is_root = (MMPI::rank() == 0);
  //if(is_root) b.display();
  PVect x_exp = b;
  for(int sd_id : x_exp.get_sd_ids()){
    auto& x_loc = x_exp.get_local_vector(sd_id);
    auto M = x_loc.get_n_rows();
    for(size_t k = 0; k < M; ++k) x_loc(k, Size{0}) = Scalar{1};
  }

  x_exp.disassemble();
  x_exp.assemble();

  b = A * x_exp;

  /*using DirectSolver = TruncateColumns<SparseMatrixCSC<Scalar>, DenseMatrix<Scalar, 1>,
				      Pastix<SparseMatrixCSC<Scalar>, DenseMatrix<Scalar,1>>,
				      7, 10>;*/
  using DirectSolver = Pastix<SparseMatrixCSC<Scalar>, loc_data>;
  //using DirectSolver = Mumps<SparseMatrixCSC<Scalar>, Vector<Scalar>>;
  using PCD = AdditiveSchwarz<decltype(A), decltype(b), DirectSolver>;
  //using PCD = ColumSelection<decltype(A), decltype(b)>;
  using Solver = Fabulous<PMat, PVect, PCD, fabulous::bgmres::ARNOLDI_IB>;
  //using Solver = Fabulous<PMat, PVect, PCD, fabulous::bgmres::ARNOLDI_IBDR, fabulous::DeflatedRestart<Scalar>>;
  //using Solver = Fabulous<PMat, PVect, PCD, fabulous::bgcro::ARNOLDI, fabulous::DeflatedRestart<Scalar>>;
  auto x_res = solve<Solver, Scalar>(A, b);

  auto xn = x_res.cwise_norm();
  auto xen = x_exp.cwise_norm();
  const double relative_error = (xn[0] - xen[0]) / xn[0];
  std::this_thread::sleep_for(std::chrono::milliseconds(500 * MMPI::rank()));
  std::cerr << "Rank: " << MMPI::rank() << ", Relative Error: " << relative_error << '\n';
  }

int main(int argc, char ** argv){

  if(argc != 3){
    usage(argv);
    return 1;
  }

  MMPI::init();

  if(std::string(argv[1]) == std::string("s")){
    test<float>(argv);
  }
  else if(std::string(argv[1]) == std::string("d")){
    test<double>(argv);
  }
  else{
    usage(argv);
    MMPI::finalize();
    return 1;
  }

  MMPI::finalize();
  return 0;
}
