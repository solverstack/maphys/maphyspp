macro(add_inte_test_exe _exe_name)
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${_exe_name}.cpp ${CMAKE_CURRENT_BINARY_DIR}/${_exe_name}.cpp COPYONLY)
  add_executable(${_exe_name} ${CMAKE_CURRENT_BINARY_DIR}/${_exe_name}.cpp)
  target_compile_options(${_exe_name} PRIVATE "$<$<CONFIG:Debug>:-Wall>")
  target_link_libraries(${_exe_name} PRIVATE ${CMAKE_PROJECT_NAME})
endmacro()

if(MAPHYSPP_USE_ARMADILLO)
  add_inte_test_exe(test_armadillo)
  target_compile_definitions(test_armadillo PRIVATE MAPHYSPP_NO_MPI)
endif()

# Eigen
if(MAPHYSPP_USE_EIGEN AND EIGEN3_FOUND)
  add_inte_test_exe(test_eigen)
  target_compile_definitions(test_eigen PRIVATE MAPHYSPP_NO_MPI)
endif()

# Fabulous
if(MAPHYSPP_USE_FABULOUS)
  add_inte_test_exe(test_deflation)
  if(MAPHYSPP_USE_PASTIX)
    add_inte_test_exe(test_fabulous_parallel)
  endif()
endif()

add_inte_test_exe(test_jacobi_as_pcd)
target_compile_definitions(test_jacobi_as_pcd PRIVATE MAPHYSPP_NO_MPI)

# Librsb
if(MAPHYSPP_USE_RSB OR MAPHYSPP_USE_RSB_SPBLAS)
  add_inte_test_exe(test_librsb)  
endif()

add_inte_test_exe(test_no_interior)

if(MAPHYSPP_USE_PASTIX AND MAPHYSPP_USE_MUMPS)
  add_inte_test_exe(test_pastix_vs_mumps)
endif()
