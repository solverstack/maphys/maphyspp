#include <iostream>
#include <fstream>
#include <vector>

#include <random>

#include <maphys.hpp>
#include <maphys/solver/BiCGSTAB.hpp>
#include <maphys/solver/Jacobi.hpp>

using namespace maphys;
using Scalar = double;

int main() {

  const int N = 1000;

  // Create dense matrix
  // With bigger coeffs on the diagonal but not diagonal dominant

  std::random_device rd;  //To get seed
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> random_extra(0.1, 1.0);
  std::uniform_real_distribution<> random_diag(10.0, 100.0);

  DenseMatrix<Scalar> A(N, N);
  for(int i = 0; i < N; ++i){
    for(int j = 0; j < N; ++j){
      if(i == j){
        A(i, j) = random_diag(gen);
      }
      else{
        A(i, j) = random_extra(gen);
      }
    }
  }

  // Create random RHS
  Vector<Scalar> b(N);
  for(int i = 0; i < N; ++i){
    b(i) = random_extra(gen) * 10;
  }

  // Use BiCGSTAB for solving
  BiCGSTAB<DenseMatrix<Scalar>, Vector<Scalar>> solver(A);
  solver.setup(parameters::max_iter{N},
               parameters::tolerance{1e-8});

  auto X = solver * b;

  solver.display("Without precond");

  BiCGSTAB<DenseMatrix<Scalar>, Vector<Scalar>, Jacobi<DenseMatrix<Scalar>, Vector<Scalar>>> solver_J3(A);
  solver_J3.setup(parameters::max_iter<int>{N},
                  parameters::tolerance<double>{1e-8});

  Jacobi<DenseMatrix<Scalar>, Vector<Scalar>>& J3 = solver_J3.get_preconditioner();
  J3.setup(parameters::fixed_iter<int>{3});

  auto X2 = solver_J3 * b;

  solver_J3.display("With J3 as pcd");

  return 0;
}
