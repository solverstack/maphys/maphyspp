program test_driver

  use maphyspp_driver_d
  use iso_c_binding

  implicit none

  include 'mpif.h'

  ! MPI
  integer :: ierr
  integer :: myid
  integer :: comm
  ! Variables
  integer, parameter :: M = 4
  integer, parameter :: NNZ  = 7
  integer, parameter :: NRHS = 1

  integer(c_int), dimension(:), pointer :: a_i(:), a_j(:)
  real(c_double), dimension(:), pointer :: a_v(:), b_v(:), x_v(:)

  type(c_ptr) :: mpp_handle
  type(maphyspp_coo_matrix_t), target     :: A
  type(maphyspp_dense_matrix_t), target     :: B, X
  type(maphyspp_parameters_t), target :: params
  type(maphyspp_output_t), target     :: mpp_output

  integer          :: n_schur = 2
  integer(c_int), dimension(:), pointer :: schur_list
  real(c_double), parameter     :: tol = 0.00000001d0

  integer :: i

  ! Init MPI
  Call MPI_INIT(ierr)
  comm = MPI_COMM_WORLD
  if (ierr /= MPI_SUCCESS) ierr = -1
  if (ierr < 0) goto 9999

  Call MPI_COMM_RANK(comm, myid, ierr)
  if (ierr /= MPI_SUCCESS) ierr = -1
  if (ierr < 0) goto 9999

  ! Init A
  call initmpp_coomatrix(A)
  A%m = M
  A%nnz = NNZ
  A%storage = 1
  A%symmetry= 1

  allocate(a_i(NNZ))
  allocate(a_j(NNZ))
  allocate(a_v(NNZ))

  A%i_ptr = c_loc(a_i)
  A%j_ptr = c_loc(a_j)
  A%v_ptr = c_loc(a_v)
  !Since the solve step is made using c/c++ code via fortran interface,
  !index values for COO format stored in a_i and a_j begin at 0
  a_i(1) = 0; a_j(1) = 0; a_v(1) = 2
  a_i(2) = 0; a_j(2) = 1; a_v(2) = -1
  a_i(3) = 1; a_j(3) = 1; a_v(3) = 2
  a_i(4) = 1; a_j(4) = 2; a_v(4) = -1
  a_i(5) = 2; a_j(5) = 2; a_v(5) = 2
  a_i(6) = 2; a_j(6) = 3; a_v(6) = -1
  a_i(7) = 3; a_j(7) = 3; a_v(7) = 2

  ! Init B
  call initmpp_densematrix(B)
  B%m = M
  B%ld = M
  B%n = NRHS

  allocate(b_v(M*NRHS))
  B%v_ptr = c_loc(b_v)

  b_v         = 0.0d0
  b_v(1)      = -1.0d0
  b_v(M*NRHS) = 1.0d0*M*NRHS

  ! Init X
  call initmpp_densematrix(X)
  X%m = M
  X%ld = M
  X%n = NRHS

  allocate(x_v(M*NRHS))
  X%v_ptr = c_loc(x_v)
  x_v(:) = 0

  ! Init params
  call initmpp_parameters(params)
  params%solver        = 1 ! 1: CG
  params%precond       = 0 ! 0: no pcd, 1: diag_pcd, 2: Additive Schwarz
  params%tolerance     = tol
  params%max_iter      = 10
  params%direct_solver = 1 ! 1: Mumps, 2: Pastix
  params%use_schur     = 1
  params%schur_size    = n_schur
  params%verbose       = 1

  allocate(schur_list(n_schur))
  params%schur_list = c_loc(schur_list)
  schur_list(1) = 1
  schur_list(2) = 2

  ! Call driver
  Call driver_maphyspp_init(mpp_handle, A, params, mpp_output)

  deallocate(a_i)
  deallocate(a_j)
  deallocate(a_v)

  Call driver_maphyspp_solve(mpp_handle, B, X)
  Call driver_maphyspp_finalize(mpp_handle)

  print*, "Computed solution x :"
  do i=1, X%m
     print*,x_v(i)
  end do

  ! FINALIZE MPI
  Call MPI_FINALIZE(ierr)
  if (ierr /= 0) then
     print*, "Problem with mpi finalize"
  end if

  print *, "Number of iterations:", mpp_output%n_iter
  print *, "Residual:", mpp_output%residual

9999 Continue

  deallocate(b_v)
  deallocate(x_v)
  deallocate(schur_list)

  if (ierr /= 0) then
     print*, "error"
  end if

end program test_driver
