use_schur = [0, 1]
solver = [1, 2]
sparse_solver = [1, 2]
sparse_solver_def = {1: "MAPHYSPP_USE_MUMPS", 2: "MAPHYSPP_USE_PASTIX"}
precond_seq = [0, 1]
precond_part = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
precond = precond_part
using_matrix_type = False
using_iter_type = False

def if_loop_options(param_ite, param_name, f):
    if len(param_ite) == 0:
        yield None
    f.write("if(inParams->" + param_name + " == " + str(param_ite[0]) + "){\n")
    yield param_ite[0]
    f.write("} //if(inParams->" + param_name + " == " + str(param_ite[0]) + ")\n")
    for i in range(1, len(param_ite)):
        f.write("else if(inParams->" + param_name + " == " + str(param_ite[i]) + "){\n")
        yield param_ite[i]
        f.write("} //else if(inParams->" + param_name + " == " + str(param_ite[i]) + ")\n")

def write_cpp_fct(f, prefix, core, suffix, core_schuronly=""):
    f.write(prefix)

    # Hybrid solve with Schur complement
    if 1 in use_schur:
        f.write("if(inParams->use_schur == 1){\n")
        f.write("using IteMatrix = typename dense_type<PMat>::type;\n")
        for i_sparse_solver in if_loop_options(sparse_solver, "direct_solver", f):
            f.write("#ifdef " + sparse_solver_def[i_sparse_solver] + "\n")
            f.write("using SparseSolver = typename sparse_solver_type<" + str(i_sparse_solver) + ">::type;\n")
        
            for i_precond in if_loop_options(precond, "precond", f):
                f.write("using Precond = typename precond_type<IteMatrix, PVec, SparseSolver, " + str(i_precond) + ">::type;\n")
        
                for i_solver in if_loop_options(solver, "solver", f):
                    f.write("using Solver = typename solver_type<PMat, PVec, SparseSolver, Precond, true, " + str(i_solver) + ">::type;\n")
                    if using_iter_type:
                        f.write("using IteSolver = typename solver_type<PMat, PVec, SparseSolver, Precond, true, " + str(i_solver) + ">::solver_iter_type;\n")
                    if core_schuronly:
                        f.write(core_schuronly)
                    else:
                        f.write(core)

            f.write("#endif //" + sparse_solver_def[i_sparse_solver] + "\n")
        f.write("} // if(inParams->use_schur == 1)\n")

    # Pure iterative solve (no Schur complement)
    if 0 in use_schur:
        f.write("if(inParams->use_schur == 0){\n")
        f.write("using SparseSolver = typename sparse_solver_type<1>::type;\n")
        for i_precond in if_loop_options(precond, "precond", f):
            f.write("using Precond = typename precond_type<PMat, PVec, SparseSolver, " + str(i_precond) + ">::type;\n")
            for i_solver in if_loop_options(solver, "solver", f):
                f.write("using Solver = typename solver_type<PMat, PVec, SparseSolver, Precond, false, " + str(i_solver) + ">::type;\n")
                f.write(core)
        f.write("} // if(inParams->use_schur == 0)\n")

    f.write(suffix + "\n")

with open("generated_interface_x.hpp", 'w') as f:

    header = """
#include <maphys.hpp>
#include <maphys/IO/ReadParam.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/solver/GMRES.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>
#include <maphys/precond/TwoLevelAbstractSchwarz.hpp>
#include <maphys/solver/SchurSolver.hpp>
#include <maphys/solver/PartSchurSolver.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/drivers/preconditioners.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#if defined(MAPHYSPP_USE_MUMPS)
#include <maphys/solver/Mumps.hpp>
#endif
#if defined(MAPHYSPP_USE_PASTIX)
#include <maphys/solver/Pastix.hpp>
#endif
#if defined(MAPHYSPP_USE_FABULOUS)
#include <maphys/precond/EigenDeflatedPcd.hpp>
#endif
#include <maphys/IO/ReadParam.hpp>

#include "driver_mpp_@SCAL@.h"

struct mpp_handle{
  void * solver;
  void * process;
  maphyspp_@SCAL@_parameters * inParams;
  maphyspp_@SCAL@_output * output;
  bool partitioned;
};

using namespace maphys;

SparseMatrixCSC<Scalar, int> csc_from_input(const maphyspp_@SCAL@_coo_matrix *);
DenseMatrix<Scalar> dense_mat_from_input(const maphyspp_@SCAL@_dense_matrix *);

template<int sparse_solver_id>
#ifdef MAPHYSPP_USE_MUMPS
struct sparse_solver_type{ using type = Mumps<SparseMatrixCSC<Scalar>, Vector<Scalar>>; };
#else
#ifdef MAPHYSPP_USE_PASTIX
struct sparse_solver_type{ using type = Pastix<SparseMatrixCSC<Scalar>, Vector<Scalar>>; };
#else
struct sparse_solver_type{ using type = Identity<SparseMatrixCSC<Scalar>, Vector<Scalar>>; };
#endif // MAPHYSPP_USE_PASTIX
#endif // MAPHYSPP_USE_MUMPS

#ifdef MAPHYSPP_USE_MUMPS
template<> struct sparse_solver_type<1>{ using type = Mumps<SparseMatrixCSC<Scalar>, Vector<Scalar>>; };
#endif
#ifdef MAPHYSPP_USE_PASTIX
template<> struct sparse_solver_type<2>{ using type = Pastix<SparseMatrixCSC<Scalar>, Vector<Scalar>>; };
#endif

template<typename Matrix, typename Vector, typename SparseSolver, int precond_id>
struct precond_type : std::false_type {};
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 0>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 1>{ using type = DiagonalPrecond<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 2>{ using type = decltype(precond_additive_schwarz<Matrix, Vector, SparseSolver>()); };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 3>{ using type = decltype(precond_neumann_neumann<Matrix, Vector, SparseSolver>()); };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 4>{ using type = decltype(precond_robin_robin<Matrix, Vector, SparseSolver>()); };

#if defined(MPP_SCALAR_TYPE_s) || defined(MPP_SCALAR_TYPE_d)

#if defined(MAPHYSPP_USE_ARPACK) && (defined(MAPHYSPP_USE_PASTIX) || defined(MAPHYSPP_USE_MUMPS))

template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 5>{ using type = decltype(precond_geneo_AS<Matrix, Vector, SparseSolver, false>()); };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 6>{ using type = decltype(precond_geneo_AS<Matrix, Vector, SparseSolver, true>()); };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 7>{ using type = decltype(precond_geneo_NN<Matrix, Vector, SparseSolver, true>()); };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 8>{ using type = decltype(precond_geneo_RR<Matrix, Vector, SparseSolver, true>()); };

#else // if arpack non avaialble geneo solver cannot be used on K

template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 5>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 6>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 7>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 8>{ using type = Identity<Matrix, Vector>; };

#endif

#if defined(MAPHYSPP_USE_FABULOUS)

template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 9>{ using type = decltype(precond_defl_AS<Matrix, Vector, SparseSolver>()); };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 10>{ using type = decltype(precond_defl_NN<Matrix, Vector, SparseSolver>()); };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 11>{ using type = decltype(precond_defl_RR<Matrix, Vector, SparseSolver>()); };

#else

template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 9>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 10>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 11>{ using type = Identity<Matrix, Vector>; };

#endif

#else
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 5>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 6>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 7>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 8>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 9>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 10>{ using type = Identity<Matrix, Vector>; };
template<typename Matrix, typename Vector, typename SparseSolver>
struct precond_type<Matrix, Vector, SparseSolver, 11>{ using type = Identity<Matrix, Vector>; };
#endif

template<typename Matrix, typename Vector, typename SparseSolver, typename Precond, bool use_schur, int solver_id>
struct solver_type : std::false_type {};

template<typename Matrix, typename Vector, typename SparseSolver, typename Precond>
struct solver_type<Matrix, Vector, SparseSolver, Precond, false, 1>{ using type = ConjugateGradient<Matrix, Vector, Precond>;
    using mat_iter_type = Matrix;
    using solver_iter_type = ConjugateGradient<Matrix, Vector, Precond>;
    };
template<typename Matrix, typename Vector, typename SparseSolver, typename Precond>
struct solver_type<Matrix, Vector, SparseSolver, Precond, false, 2>{ using type = GMRES<Matrix, Vector, Precond>;
    using mat_iter_type = Matrix;
    using solver_iter_type = GMRES<Matrix, Vector, Precond>;
    };

template<typename Matrix, typename Vector, typename SparseSolver, typename Precond>
struct solver_type<Matrix, Vector, SparseSolver, Precond, true, 1>{
  using mat_iter_type = typename dense_type<Matrix>::type;
  using solver_iter_type = ConjugateGradient<mat_iter_type, Vector, Precond>;
  using type = typename std::conditional_t<is_distributed<Matrix>::value, PartSchurSolver<Matrix, Vector, SparseSolver, solver_iter_type>, SchurSolver<Matrix, Vector, SparseSolver, solver_iter_type>>; };
template<typename Matrix, typename Vector, typename SparseSolver, typename Precond>
struct solver_type<Matrix, Vector, SparseSolver, Precond, true, 2>{
  using mat_iter_type = typename dense_type<Matrix>::type;
  using solver_iter_type = GMRES<mat_iter_type, Vector, Precond>;
  using type = typename std::conditional_t<is_distributed<Matrix>::value, PartSchurSolver<Matrix, Vector, SparseSolver, solver_iter_type>, SchurSolver<Matrix, Vector, SparseSolver, solver_iter_type>>; };

    template<typename Precond, typename TrMatType>
    typename std::enable_if<is_abstractschwarz<Precond>::value>::type
    set_transmission_matrix(Precond& pcd, TrMatType& tr_mat){
      pcd.setup(parameters::Ti<TrMatType>{tr_mat});
    }

    template<typename Precond, typename TrMatType>
    typename std::enable_if<!is_abstractschwarz<Precond>::value>::type
    set_transmission_matrix(Precond&, TrMatType&){}

    template<typename Precond, typename Mat, typename Vect, typename Solver>
    void setup_precond_geneo(Solver& solver, maphyspp_@SCAL@_parameters * inParams){
      auto setup_geneo = [inParams](const Mat& mat, Precond& pcd){
                           pcd.set_n_vect(inParams->geneo_nv); pcd.set_alpha(0); pcd.set_beta(0); pcd.setup(mat); pcd.set_tolerance(inParams->tolerance); };
      auto geneo_initguess = [](const Mat& mat, const Vect& rhs, Precond& pcd){ (void) mat; return pcd.init_guess(rhs); };
      solver.setup(parameters::setup_pcd<std::function<void(const Mat&, Precond&)>>{setup_geneo},
                   parameters::setup_init_guess<std::function<Vect(const Mat&, const Vect&, Precond&)>>{geneo_initguess});
    }

    template<typename Precond, typename Mat, typename Vect, typename Solver>
    void setup_precond_deflated(Solver& solver, maphyspp_@SCAL@_parameters * inParams){
      auto setup_defl = [inParams](const Mat& mat, Precond& pcd){
                             if(inParams->defl_max_size == -1) inParams->defl_max_size = std::min(std::max(1, n_rows(mat) / 10), 100);
                             if(inParams->defl_solve_size == -1) inParams->defl_solve_size = 2 * inParams->defl_max_size + 1;
                             if(inParams->defl_increment_size == -1) inParams->defl_increment_size = std::max(1, inParams->defl_max_size / 4);
                             pcd.set_n_vect(inParams->defl_max_size); pcd.set_n_vect_solve(inParams->defl_solve_size);
                             pcd.set_nev(inParams->defl_increment_size); pcd.setup(mat);
                           };
      auto defl_initguess = [](const Mat& mat, const Vect& rhs, Precond& pcd){ (void) mat; return pcd.init_guess(rhs); };
      solver.setup(parameters::setup_pcd<std::function<void(const Mat&, Precond&)>>{setup_defl},
                   parameters::setup_init_guess<std::function<Vect(const Mat&, const Vect&, Precond&)>>{defl_initguess});
    }
"""
    f.write(header)

    # SETUP SEQ
    using_matrix_type = False
    using_iter_type = True
    precond = precond_seq
    prefix = """mpp_handle * setup_seq(const SparseMatrixCSC<Scalar>& A, maphyspp_@SCAL@_parameters * inParams, maphyspp_@SCAL@_output * output){
    using PMat = SparseMatrixCSC<Scalar>;
    using PVec = Vector<Scalar>;
    void * solver_void_ptr = nullptr;
    """

    core_schuronly = """Solver * solver_ptr = new Solver();
    IteSolver& ite = solver_ptr->get_solver_S();
    bool verbose = (inParams->verbose != 0);
    IndexArray<int> schurlist(inParams->schur_list, inParams->schur_size);

    Solver& solver = *solver_ptr;
    solver.setup(parameters::copy_A{true},
                 parameters::A{A},
                 parameters::schurlist{schurlist},
                 parameters::verbose{verbose});
    solver.get_solver_K().setup(A);

    ite.setup(parameters::max_iter{inParams->max_iter},
              parameters::tolerance{inParams->tolerance},
              parameters::restart{inParams->restart},
              parameters::verbose{verbose});

    solver_void_ptr = (void *) solver_ptr;
"""

    core = """Solver * solver_ptr = new Solver();
    bool verbose = (inParams->verbose != 0);

    Solver& solver = *solver_ptr;
    solver.setup(parameters::copy_A{true},
                 parameters::A{A},
                 parameters::max_iter{inParams->max_iter},
                 parameters::tolerance{inParams->tolerance},
                 parameters::restart{inParams->restart},
                 parameters::verbose{verbose});

    solver_void_ptr = (void *) solver_ptr;
"""

    suffix = """
    mpp_handle * handle = new mpp_handle();
    handle->solver = solver_void_ptr;
    handle->inParams = inParams;
    handle->output = output;
    handle->partitioned = 0;
    return handle;
    }
    """

    write_cpp_fct(f, prefix, core, suffix, core_schuronly)

    # SETUP PART
    precond = precond_part
    using_matrix_type = True
    using_iter_type = True
    prefix = """
    mpp_handle * setup_part(const PartMatrix<SparseMatrixCSC<Scalar>>& A, maphyspp_@SCAL@_parameters * inParams, maphyspp_@SCAL@_output * output){
    using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
    using PVec = PartVector<Vector<Scalar>>;
    void * solver_void_ptr = NULL;
    """

    core_schuronly = """Solver * solver_ptr = new Solver();
    IteSolver& ite = solver_ptr->get_solver_S();
    bool verbose = (MMPI::rank(inParams->comm) == 0 && inParams->verbose != 0);

    if constexpr(is_pcd_geneo<Precond>::value) { setup_precond_geneo<Precond, IteMatrix, PVec, IteSolver>(ite, inParams); }
    if constexpr(has_spectral_extraction<Precond>::value) { setup_precond_deflated<Precond, IteMatrix, PVec, IteSolver>(ite, inParams); }

    Solver& solver = *solver_ptr;
    solver.setup(parameters::copy_A{true},
                 parameters::A{A},
                 parameters::verbose{verbose});
    solver.get_solver_K().setup(A);

    ite.setup(parameters::max_iter{inParams->max_iter},
              parameters::tolerance{inParams->tolerance},
              parameters::verbose{verbose});

    solver_void_ptr = (void *) solver_ptr;
"""

    core = """Solver * ite_ptr = new Solver();
    Solver& ite = *ite_ptr;
    bool verbose = (MMPI::rank(inParams->comm) == 0 && inParams->verbose != 0);

    if constexpr(is_pcd_geneo<Precond>::value) { setup_precond_geneo<Precond, PMat, PVec, Solver>(ite, inParams); }
    if constexpr(has_spectral_extraction<Precond>::value) { setup_precond_deflated<Precond, PMat, PVec, Solver>(ite, inParams); }

    ite.setup(parameters::copy_A{true},
             parameters::A{A},
             parameters::max_iter{inParams->max_iter},
             parameters::tolerance{inParams->tolerance},
             parameters::verbose{verbose});
    solver_void_ptr = (void *) ite_ptr;
"""

    suffix = """
    mpp_handle * handle = new mpp_handle();
    handle->solver = solver_void_ptr;
    handle->inParams = inParams;
    handle->output = output;
    handle->partitioned = 1;
    return handle;
    }
    """

    write_cpp_fct(f, prefix, core, suffix, core_schuronly)

    using_matrix_type = False
    using_iter_type = False

    # SET TRANSFER MATRICES

    # Dense transfer matrix on Schur complement
    use_schur = [1]

    # Backup list of pcd
    precond_part_back = list(precond_part)
    # Now work only on RobinRobin precond
    precond_part = [4, 8, 11]
    
    prefix = """void driver_maphyspp_@SCAL@_set_transfer_mat_dense(void * handle, maphyspp_@SCAL@_dense_matrix * tr_mat){
      mpp_handle * xhandle = (mpp_handle *) handle;
      maphyspp_@SCAL@_parameters * inParams = xhandle->inParams;
      using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
      using PVec = PartVector<Vector<Scalar>>;
      std::shared_ptr<Process> * p_ptr = (std::shared_ptr<Process> *) xhandle->process;
      PartMatrix<DenseMatrix<Scalar>> robin_mat(*p_ptr, MMPI::rank(), dense_mat_from_input(tr_mat), true);
    """

    core = """Solver * solver = (Solver *) xhandle->solver;
    set_transmission_matrix(solver->get_solver_S().get_preconditioner(), robin_mat);
    """

    suffix = "}"
    write_cpp_fct(f, prefix, core, suffix)

    # Sparse transfer matrix on sparse input matrix
    use_schur = [0]

    prefix = """void driver_maphyspp_@SCAL@_set_transfer_mat_sparse(void * handle, maphyspp_@SCAL@_coo_matrix * tr_mat){
      mpp_handle * xhandle = (mpp_handle *) handle;
      maphyspp_@SCAL@_parameters * inParams = xhandle->inParams;
      using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
      using PVec = PartVector<Vector<Scalar>>;
      std::shared_ptr<Process> * p_ptr = (std::shared_ptr<Process> *) xhandle->process;
      PMat robin_mat(*p_ptr, MMPI::rank(), csc_from_input(tr_mat), true);
    """

    core = """Solver * solver = (Solver *) xhandle->solver;
    set_transmission_matrix(solver->get_preconditioner(), robin_mat);
    """

    suffix = "}"
    write_cpp_fct(f, prefix, core, suffix)

    # Put variables back
    use_schur = [0, 1]
    precond_part = precond_part_back

    # SOLVE PART

    prefix = """void driver_maphyspp_@SCAL@_solve(void * handle, maphyspp_@SCAL@_dense_matrix * inB, maphyspp_@SCAL@_dense_matrix * outX){
    mpp_handle * xhandle = (mpp_handle *) handle;

    maphyspp_@SCAL@_parameters * inParams = xhandle->inParams;
    if(xhandle->partitioned){
      std::shared_ptr<Process> * p_ptr = (std::shared_ptr<Process> *) xhandle->process;
      std::shared_ptr<Process>& p = *p_ptr;
      auto make_vect = [&](int m, int ld, Scalar * v_ptr){
                         Vector<Scalar> U(DenseData<Scalar, 1>(m, v_ptr, ld), /*fixed_ptr*/ true);
                         return PartVector<Vector<Scalar>>(p, MMPI::rank(inParams->comm), std::move(U));
                       };
      using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
      using PVec = PartVector<Vector<Scalar>>;
      const PVec B(make_vect(inB->m, inB->ld, inB->v_ptr));
      PVec X(make_vect(outX->m, outX->ld, outX->v_ptr));
    """

    core = """Solver * solver = (Solver *) xhandle->solver;
solver->apply(B, X);
xhandle->output->n_iter = solver->get_n_iter();
xhandle->output->residual = solver->get_residual();"""

    suffix = "}"

    write_cpp_fct(f, prefix, core, suffix)

    # SOLVE SEQ
    precond = precond_seq

    prefix ="""else { // Here
    auto make_vect = [&](int m, int ld, Scalar * v_ptr){
return Vector<Scalar>(DenseData<Scalar, 1>(m, v_ptr, ld), /*fixed_ptr*/ true);
};
using PMat = SparseMatrixCSC<Scalar>;
using PVec = Vector<Scalar>;
const PVec B(make_vect(inB->m, inB->ld, inB->v_ptr));
PVec X(make_vect(outX->m, outX->ld, outX->v_ptr));
"""

    core = """Solver * solver = (Solver *) xhandle->solver;
solver->apply(B, X);
xhandle->output->n_iter = solver->get_n_iter();
xhandle->output->residual = solver->get_residual();"""

    suffix = """} // if(xhandle->partitioned)
} // void driver_maphyspp_@SCAL@_solve
"""

    write_cpp_fct(f, prefix, core, suffix)

    # FINALIZE PART
    precond = precond_part

    prefix = """void driver_maphyspp_@SCAL@_finalize(void * handle){
    mpp_handle * xhandle = (mpp_handle *) handle;
    maphyspp_@SCAL@_parameters * inParams = xhandle->inParams;
    if(xhandle->partitioned){
    using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
    using PVec = PartVector<Vector<Scalar>>;
    """

    core = """delete (Solver *) xhandle->solver;
    MPI_Comm_free(&xhandle->inParams->comm);
    """

    suffix = """std::shared_ptr<Process> * p_ptr = (std::shared_ptr<Process> *) xhandle->process;
    delete p_ptr;
    }
"""

    write_cpp_fct(f, prefix, core, suffix)

    # FINALIZE SEQ
    precond = precond_seq

    prefix = """else{
    using PMat = SparseMatrixCSC<Scalar>;
    using PVec = Vector<Scalar>;
"""

    core = """delete (Solver *) xhandle->solver;
"""

    suffix = """}
    delete xhandle;
}
"""

    write_cpp_fct(f, prefix, core, suffix)

    precond = precond_part

    # Display centralized input matrix for debugging
    prefix = """
    void maphyspp_@SCAL@_display_input_matrix(void * handle, int root){
      mpp_handle * xhandle = (mpp_handle *) handle;
      maphyspp_@SCAL@_parameters * inParams = xhandle->inParams;
      using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
      using PVec = PartVector<Vector<Scalar>>;
    """

    core = """
      Solver * solver = (Solver *) xhandle->solver;
      const auto * K_mat = solver->get_input_matrix();
      if(K_mat == nullptr) { std::cerr << "maphyspp_X_display_input_matrix: input matrix unset"; return; }
      if(xhandle->partitioned){
        K_mat->display_centralized("Input matrix", std::cout, root);
      }
    """

    suffix = """    }
    """

    write_cpp_fct(f, prefix, core, suffix)

    # Display input matrix as a dense matrix

    prefix = """
    void maphyspp_@SCAL@_display_input_matrix_as_dense(void * handle, int root){
      mpp_handle * xhandle = (mpp_handle *) handle;
      maphyspp_@SCAL@_parameters * inParams = xhandle->inParams;
      using PMat = PartMatrix<SparseMatrixCSC<Scalar>>;
      using PVec = PartVector<Vector<Scalar>>;
    """

    core = """
      Solver * solver = (Solver *) xhandle->solver;
      const auto * K_mat = solver->get_input_matrix();
      if(K_mat == nullptr) { std::cerr << "maphyspp_X_display_input_matrix: input matrix unset"; return; }
      if(xhandle->partitioned){
        auto K_mat_centr = K_mat->centralize(root);
        if(MMPI::rank(inParams->comm) == root){
          K_mat_centr.to_dense().display("Input matrix");
        }
      }
    """

    suffix = """    }
    """

    write_cpp_fct(f, prefix, core, suffix)
