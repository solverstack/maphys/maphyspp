#include <iostream>
#include <fstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <stdlib.h>

#define TIMER_LEVEL_MAX 0
#define TIMER_LEVEL_MIN 0

#include "generated_interface_@SCAL@.hpp"

using namespace maphys;

template<typename Matrix>
void verbose_parameters(const Matrix& A, const maphyspp_@SCAL@_parameters& inParams){
  std::cout << "----- On rank " << MMPI::rank(inParams.comm) << '\n';
  std::cout << "A: " << n_rows(A) << " x " << n_cols(A) << '\n';
  std::cout << A.properties_str() << "\n\n";

  std::cout << "Parameters:\n";

  std::cout << " - solver: " << inParams.solver << " (";
  switch(inParams.solver){
  case 0:
    std::cout << "automatic";
    break;
  case 1:
    std::cout << "conjugate gradient";
    break;
  case 2:
    std::cout << "GMRES";
    break;
  default:
    std::cout << "unknown value";
    break;
  }
  std::cout << ")\n";

  std::cout << " - precond: " << inParams.precond << " (";
  switch(inParams.precond){
  case 0:
    std::cout << "no preconditioner";
    break;
  case 1:
    std::cout << "jacobi preconditioner";
    break;
  case 2:
    std::cout << "additive Schwarz";
    break;
  case 3:
    std::cout << "Neumann - Neumann";
    break;
  case 4:
    std::cout << "Robin - Robin";
    break;
#if defined(MPP_SCALAR_TYPE_s) || defined(MPP_SCALAR_TYPE_d)
  case 5:
    std::cout << "geneo with additive Schwarz, additive";
    break;
  case 6:
    std::cout << "geneo with additive Schwarz, deflated";
    break;
  case 7:
    std::cout << "geneo with Neumann - Neumann, deflated";
    break;
  case 8:
    std::cout << "geneo with Robin - Robin, deflated";
    break;

  case 9:
    std::cout << "eigen-deflated with additive Schwarz";
    break;
  case 10:
    std::cout << "eigen-deflated with Neumann - Neumann";
    break;
  case 11:
    std::cout << "eigen-deflated with Robin - Robin";
    break;

#endif
  default:
    std::cout << "unknown value";
    break;
  }
  std::cout << ")\n";

  std::cout << " - geneo_nv: " << inParams.geneo_nv << '\n';
  std::cout << " - defl_max_size: " << inParams.defl_max_size << '\n';
  std::cout << " - defl_solve_size: " << inParams.defl_solve_size << '\n';
  std::cout << " - defl_increment_size: " << inParams.defl_increment_size << '\n';
  std::cout << " - tolerance: " << inParams.tolerance << '\n';
  std::cout << " - max_iter: " << inParams.max_iter << '\n';
  std::cout << " - restart: " << inParams.restart << '\n';
  std::cout << " - direct_solver: " << inParams.direct_solver << " (";

  switch(inParams.direct_solver){
  case 1:
    std::cout << "MUMPS";
    break;
  case 2:
    std::cout << "Pastix";
    break;
  default:
    std::cout << "Unavailable";
    break;
  }
  std::cout << ")\n";

  std::cout << " - use_schur: " << inParams.use_schur << '\n';
  std::cout << " - schur_size: " << inParams.schur_size << '\n';
  std::cout << "-----\n";
}

// Input matrix
SparseMatrixCSC<Scalar, int> csc_from_input(const maphyspp_@SCAL@_coo_matrix * inA){
  SparseMatrixCOO<Scalar, int> At(inA->m, inA->m, inA->nnz, inA->i_ptr, inA->j_ptr, inA->v_ptr);
  if(inA->symmetry == 2) At.set_property(MatrixSymmetry::symmetric);
  if(inA->symmetry == 1) At.set_spd(MatrixStorage::full);
  if(inA->storage > 0) At.set_property(MatrixStorage::upper);
  if(inA->storage < 0) At.set_property(MatrixStorage::lower);
  return At.to_csc();
}

DenseMatrix<Scalar> dense_mat_from_input(const maphyspp_@SCAL@_dense_matrix * inDMat){
  return DenseMatrix<Scalar>::view(static_cast<Size>(inDMat->m), static_cast<Size>(inDMat->n), inDMat->v_ptr, static_cast<Size>(inDMat->ld));
}

extern "C"
{
  using namespace maphys;

  void maphyspp_@SCAL@_read_parameters(const char * filename, maphyspp_@SCAL@_parameters * inParams){
    std::map<std::string, int> int_values;
    std::map<std::string, double> real_values;
    const std::string fname(filename);
    std::map<std::string, std::string> param_map = read_param_file(fname);

    try{ inParams->solver = std::stoi(param_map.at("solver")); } catch(...){}
    try{ inParams->precond = std::stoi(param_map.at("precond")); } catch(...){}
    try{ inParams->restart = std::stoi(param_map.at("restart")); } catch(...){}
    try{ inParams->geneo_nv = std::stoi(param_map.at("geneo_nv")); } catch(...){}
    try{ inParams->defl_max_size = std::stoi(param_map.at("defl_max_size")); } catch(...){}
    try{ inParams->defl_solve_size = std::stoi(param_map.at("defl_solve_size")); } catch(...){}
    try{ inParams->defl_increment_size = std::stoi(param_map.at("defl_increment_size")); } catch(...){}
    try{ inParams->max_iter = std::stoi(param_map.at("max_iter")); } catch(...){}
    try{ inParams->direct_solver = std::stoi(param_map.at("direct_solver")); } catch(...){}
    try{ inParams->use_schur = std::stoi(param_map.at("use_schur")); } catch(...){}
    try{ inParams->verbose = std::stoi(param_map.at("verbose")); } catch(...){}
    try{ inParams->tolerance = std::stod(param_map.at("tolerance")); } catch(...){}
  }

  //initialisation style based on spm.c (SPM: SParse Matrix package))
  void maphyspp_@SCAL@_init_coo_matrix(maphyspp_@SCAL@_coo_matrix * mppm){
    mppm->i_ptr = NULL;
    mppm->j_ptr = NULL;
    mppm->v_ptr = NULL;
    mppm->m = 0;
    mppm->nnz = 0;
    mppm->storage = 0; // 0: full default
    mppm->symmetry = 0; // 0: general default
  }

  void maphyspp_@SCAL@_init_dense_matrix(maphyspp_@SCAL@_dense_matrix * mppv){
    mppv->v_ptr = NULL;
    mppv->m = 0;
    mppv->n = 1;
    mppv->ld = 0;
  }

  void maphyspp_@SCAL@_init_parameters(maphyspp_@SCAL@_parameters * mppp){
    mppp->solver = 0;
    mppp->precond = 0;
    mppp->restart = 500;
    mppp->geneo_nv = 5;
    mppp->defl_max_size = -1;
    mppp->defl_increment_size = -1;
    mppp->defl_solve_size = -1;
    mppp->tolerance = 1e-5;
    mppp->max_iter = 100;
    mppp->direct_solver = 1;
    mppp->use_schur = 0;
    mppp->schur_size = 0;
    mppp->schur_list = NULL;
    mppp->verbose = 1;
    mppp->comm = MPI_COMM_NULL;
    mppp->fcomm = 0;
  }

  void maphyspp_@SCAL@_init_neighbors(maphyspp_@SCAL@_neighbors * mppn){
    mppn->n_neigh = 0;
    mppn->neighbors = NULL;
    mppn->idx_ptr = NULL;
    mppn->indices = NULL;
  }

  void * driver_maphyspp_@SCAL@_init(maphyspp_@SCAL@_coo_matrix * inA, maphyspp_@SCAL@_parameters * inParams, maphyspp_@SCAL@_output * output){
    mpp_handle * handle = NULL;

    const SparseMatrixCSC<Scalar, int> A(csc_from_input(inA));
    inParams->comm = MPI_COMM_SELF;

#if !defined(MAPHYSPP_USE_PASTIX)
    inParams->direct_solver = 1;
#endif
#if !defined(MAPHYSPP_USE_MUMPS)
    inParams->direct_solver = 2;
#endif
#if !defined(MAPHYSPP_USE_PASTIX) && !defined(MAPHYSPP_USE_MUMPS)
    inParams->direct_solver = -1;
    inParams->use_schur = 0;
#endif

    if(inParams->verbose) verbose_parameters(A, *inParams);

    if(inParams->solver == 0){
      if(A.is_spd_or_hpd()) { inParams->solver = 1; } //CG
      else { inParams->solver = 2; } //GMRES
    }

    handle = setup_seq(A, inParams, output);

    if(handle == NULL){
      std::cerr << "Error: handle is NULL, parameters must be wrong\n";
    }

    return (void *) handle;
  }

  void * driver_maphyspp_@SCAL@_part_init(maphyspp_@SCAL@_coo_matrix * inA, maphyspp_@SCAL@_neighbors * inNeigh, maphyspp_@SCAL@_parameters * inParams, maphyspp_@SCAL@_output * output){
    mpp_handle * handle = NULL;
    SparseMatrixCSC<Scalar> loc_mat(csc_from_input(inA));
    
    if(inParams->fcomm != -1){
      inParams->comm = MPI_Comm_f2c(inParams->fcomm);
    }

    int old_rank = MMPI::rank(inParams->comm);

    const int has_subdomain = (inNeigh->n_neigh > 0) ? 1 : 0;
    int n_sd = 0;

    MMPI::allreduce(&has_subdomain, &n_sd, 1, MPI_SUM, inParams->comm);
    if(!has_subdomain) n_sd = 0;

    MPI_Comm new_comm;
    MPI_Comm_split(inParams->comm, has_subdomain, old_rank, &new_comm);
    int new_rank = MMPI::rank(new_comm);

    std::shared_ptr<Process> * p_ptr = new std::shared_ptr<Process>(bind_subdomains(n_sd, 1, new_comm));
    std::shared_ptr<Process>& p = *p_ptr;

    std::map<int, IndexArray<int>> sd_map;
    IndexArray<int> interface_dofs;
    for(int k = 0; k < inNeigh->n_neigh; ++k){
      const int nei = inNeigh->neighbors[k];
      const int beg_idx = inNeigh->idx_ptr[k];
      const int end_idx = inNeigh->idx_ptr[k+1];
      const int size_intrf = end_idx - beg_idx;
      interface_dofs.union_with(IndexArray<int>(inNeigh->indices + beg_idx, size_intrf));
      sd_map.emplace(nei, IndexArray<int>(inNeigh->indices + beg_idx, size_intrf));
    }

    interface_dofs = IndexArray<int>();
    const int n_dofs = static_cast<int>(n_rows(loc_mat));
    std::vector<Subdomain> sds{Subdomain(new_rank, n_dofs, sd_map, false)};
    if(!has_subdomain) sds.clear();
    p->load_subdomains(sds);

    using PMat = PartMatrix<SparseMatrixCSC<Scalar, int>>;
    auto create_part_mat = [&](){
      if(!has_subdomain) return PMat();
      return PMat(p, new_rank, std::move(loc_mat));
    };
    const PMat A(create_part_mat());

#if !defined(MAPHYSPP_USE_PASTIX)
    inParams->direct_solver = 1;
#endif
#if !defined(MAPHYSPP_USE_MUMPS)
    inParams->direct_solver = 2;
#endif
#if !defined(MAPHYSPP_USE_PASTIX) && !defined(MAPHYSPP_USE_MUMPS)
    inParams->direct_solver = -1;
    inParams->use_schur = 0;
#endif

    if(has_subdomain){
      if(inParams->verbose && MMPI::rank(new_comm) == 0) verbose_parameters(A.get_local_matrix(0), *inParams);

      if(inParams->solver == 0){
	if(A.get_local_matrix(MMPI::rank(new_comm)).is_spd_or_hpd()) { inParams->solver = 1; } //CG
	else { inParams->solver = 2; } //GMRES
      }
    }

    inParams->comm = new_comm;
    handle = setup_part(A, inParams, output);

    if(handle == NULL){
      std::cerr << "Error: handle is NULL, parameters must be wrong\n";
      return NULL;
    }
    handle->process = p_ptr;
    return (void *) handle;
  }
} // extern "C"
