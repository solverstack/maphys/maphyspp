#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <complex.h>
#include <mpi.h>
#include "driver_mpp_z.h"

int main(int argc, char** argv){

  MPI_Init(NULL, NULL);

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if(size != 3){
    fprintf(stderr, "Error: this exemple should be launched with 3 processes\n");
    return 1;
  }

  const int M = 4;
  const int NNZ = 14;
  const int NRHS = 1;

  int * a_i = (int *) malloc(NNZ * sizeof(int));
  int * a_j = (int *) malloc(NNZ * sizeof(int));

  double _Complex * a_v = (double _Complex *) malloc(NNZ * sizeof(double _Complex));
  double _Complex * b = (double _Complex *) malloc(M * NRHS * sizeof(double _Complex));
  double _Complex * x = (double _Complex *) malloc(M * NRHS * sizeof(double _Complex));

  for(int k = 0; k < M; ++k){
    b[k] = 0;
    x[k] = 0;
  }

  if(rank == 0){
    b[0] = -13 + 1 * I; b[1] = -1 + 0 * I; b[2] = -2 + 0 * I; b[3] = 12 + 0 * I;
  }
  else if(rank == 1){
    b[0] = 12 + 0 * I; b[1] = -2 + 0 * I; b[2] = 5 + 0 * I; b[3] = 17 + 0 * I;
  }
  else{
    b[0] = 17 + 0 * I; b[1] = 16 + 0 * I; b[2] = -13 + 1 * I; b[3] = 12 + 0 * I;
  }

  a_i[0] = 0; a_j[0] = 0; a_v[0] = 3;
  a_i[1] = 0; a_j[1] = 1; a_v[1] = -1;
  a_i[2] = 0; a_j[2] = 3; a_v[2] = -1;
  a_i[3] = 1; a_j[3] = 1; a_v[3] = 4;
  a_i[4] = 1; a_j[4] = 2; a_v[4] = -1;
  a_i[5] = 1; a_j[5] = 3; a_v[5] = -1;
  a_i[6] = 2; a_j[6] = 2; a_v[6] = 3;
  a_i[7] = 2; a_j[7] = 3; a_v[7] = -1;
  a_i[8] = 3; a_j[8] = 3; a_v[8] = 4;

  a_i[9]  = 1; a_j[9]  = 0; a_v[9]  = -1 + 1 * I;
  a_i[10] = 3; a_j[10] = 0; a_v[10] = -1 - 1 * I;
  a_i[11] = 2; a_j[11] = 1; a_v[11] = -1 + 1 * I;
  a_i[12] = 3; a_j[12] = 1; a_v[12] = -1 - 1 * I;
  a_i[13] = 3; a_j[13] = 2; a_v[13] = -1 + 1 * I;

  maphyspp_z_coo_matrix A = {
                             .i_ptr = a_i,
                             .j_ptr = a_j,
                             .v_ptr = a_v,
                             .m = M,
                             .nnz = NNZ,
                             .storage = 0,
                             .symmetry = 0
  };
  maphyspp_z_dense_matrix B = {
                               .v_ptr = b,
                               .m = M,
                               .n = NRHS,
                               .ld = M
  };
  maphyspp_z_dense_matrix X = {
                               .v_ptr = x,
                               .m = M,
                               .n = NRHS,
                               .ld = M
  };

  maphyspp_z_neighbors topo;

  topo.n_neigh = 2;
  topo.neighbors = (int *) malloc(2 * sizeof(int));
  topo.idx_ptr = (int *) malloc(3 * sizeof(int));
  topo.indices = (int *) malloc(4 * sizeof(int));

  int * neighbors = topo.neighbors;
  int * idx_ptr = topo.idx_ptr;
  int * indices = topo.indices;
  if(rank == 0) { neighbors[0] = 1; neighbors[1] = 2; }
  if(rank == 1) { neighbors[0] = 0; neighbors[1] = 2; }
  if(rank == 2) { neighbors[0] = 0; neighbors[1] = 1; }

  idx_ptr[0] = 0; idx_ptr[1] = 2; idx_ptr[2] = 4;

  if(rank == 0) { indices[0] = 2; indices[1] = 3; indices[2] = 0; indices[3] = 3; }
  if(rank == 1) { indices[0] = 1; indices[1] = 0; indices[2] = 0; indices[3] = 3; }
  if(rank == 2) { indices[0] = 2; indices[1] = 3; indices[2] = 3; indices[3] = 0; }

  maphyspp_z_parameters params = {
                                  .solver = 2, // GMRES
                                  .precond = 0, //0: no pcd, 1: diag_pcd, 2: Additive Schwarz, 3: NeumannNeumann, 4: RobinRobin
                                  .geneo_nv = 2,
                                  .tolerance = 1e-8,
                                  .max_iter = 10,
                                  .restart = 4,
                                  .direct_solver = 2,
                                  .use_schur = 1,
                                  .schur_size = 0,
                                  .schur_list = NULL,
                                  .verbose = 1,
                                  .comm = MPI_COMM_WORLD,
                                  .fcomm = -1
  };

  maphyspp_z_output output;

  void * handle = driver_maphyspp_z_part_init(&A, &topo, &params, &output);

  free(a_i);
  free(a_j);
  free(a_v);
  free(topo.neighbors);
  free(topo.idx_ptr);
  free(topo.indices);

  driver_maphyspp_z_solve(handle, &B, &X);
  driver_maphyspp_z_finalize(handle);

  for(int r = 0; r < 3; ++r){
    MPI_Barrier(MPI_COMM_WORLD);
    if(r == rank){
      for(int k = 0; k < 4; ++k) printf("%lf + %lf i\n", creal(x[k]), cimag(x[k]));
      printf("\n");
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  if(rank == 0){
    printf("Number of iterations: %d\n", output.n_iter);
    printf("Residual: %lf\n", output.residual);
  }

  free(b);
  free(x);

  MPI_Finalize();

  return 0;
}
