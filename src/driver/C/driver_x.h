#if defined __cplusplus
using Scalar = @CPP_SCALAR_TYPE@;
using Real = @CPP_REAL_TYPE@;
#else
typedef @C_SCALAR_TYPE@ Scalar;
typedef @C_REAL_TYPE@ Real;
#endif

#define MPP_SCALAR_TYPE_@SCAL@ 1

// Sparse square matrix
typedef struct{
  int m;
  int nnz;
  int * i_ptr;
  int * j_ptr;
  Scalar * v_ptr;
  int symmetry; // 0: general, 1: spd, 2: symmetric
  int storage; // 0: full; >0: upper; <0: lower
} maphyspp_@SCAL@_coo_matrix;

/* Vector structure for RHS and solution vector format: dense column-major (only
   single rhs n = 1 is supported for now)
*/

// Dense rhs and solution vector
typedef struct{
  Scalar * v_ptr;
  int m;
  int n;
  int ld;
} maphyspp_@SCAL@_dense_matrix;

/* Solver parameters */

typedef struct{
  int solver; //0: automatic, 1: CG, 2: GMRES
  int precond; //0: no pcd, 1: diag_pcd, 2: Additive Schwarz, 3: Neumann-Neumann, 4: Robin-Robin
  //2-level preconditioner (geneo) 5: Additive Schwarz, additive, 6: Additive Schwarz, deflated, 7: Neumann-Neumann, deflated, 8: Robin-Robin, deflated
  //2-level preconditioner Eigen-Deflated: 9: Additive Schwarz, 10: Neumann-Neumann, 11: Robin-Robin
  int geneo_nv; // Number of eigenvectors per subdomain to build geneo coarse space (default: 5)

  // For Eigen-deflation (precond 9 - 10 - 11)
  int defl_max_size; // Maximal number of vectors to keep in the deflation space
  int defl_increment_size; // Number of vectors to be added to the deflation space at each solve. Muse be < defl_max_size.
  int defl_solve_size; // Number of vectors used for deflation during the solve. Muse be > (2 * defl_increment_size)

  Real tolerance;
  int max_iter;
  int restart;
  int direct_solver; //1: Mumps, 2: Pastix
  int use_schur; // 0 to solve on input matrix directly, 1 use schur complement on interface
  int schur_size;
  int * schur_list; // Optional if use_schur = 0 or if using driver_maphyspp_@SCAL@_part
  int verbose;
  MPI_Comm comm;
  MPI_Fint fcomm;
} maphyspp_@SCAL@_parameters;

typedef struct{
  int n_iter;
  Real residual;
} maphyspp_@SCAL@_output;

/* Subdomain topology parameter
    1 subdomain per MPI rank
    Local indexing only

    Example: if the subdomain 0 shares doffs [2, 3] with 1 and [0, 3] with 2

    n_neigh = 2 

    neighbors = [1, 2] (size n_neigh): indices of the neighbor subdomains

    idx_ptr = [0, 2, 4] (size n_neigh+1): position in the array 'indices' of the
    elements shared with the neighbor 'nei' must be between idx_ptr[nei] and
    idx_ptr[nei+1] (respecting the order of neighbors)

    indices = [2, 3, 0, 3] (size idx_ptr[n_neigh]): list of the elements shared
    with each neighbor
*/
 typedef struct{
   int n_neigh;
   int * neighbors;
   int * idx_ptr;
   int * indices;
 } maphyspp_@SCAL@_neighbors;

#if defined __cplusplus
extern "C" {
#endif

void maphyspp_@SCAL@_init_coo_matrix(maphyspp_@SCAL@_coo_matrix * mppm);
void maphyspp_@SCAL@_init_dense_matrix(maphyspp_@SCAL@_dense_matrix * mppv);
void maphyspp_@SCAL@_init_parameters(maphyspp_@SCAL@_parameters * mppp);
void maphyspp_@SCAL@_init_neighbors(maphyspp_@SCAL@_neighbors * mppn);

void maphyspp_@SCAL@_read_parameters(const char * filename,
                                     maphyspp_@SCAL@_parameters * inParams);

void * driver_maphyspp_@SCAL@_init(maphyspp_@SCAL@_coo_matrix * inA,
                                   maphyspp_@SCAL@_parameters * inParams,
                                   maphyspp_@SCAL@_output * output);

void * driver_maphyspp_@SCAL@_part_init(maphyspp_@SCAL@_coo_matrix * inA,
                                        maphyspp_@SCAL@_neighbors * inNeigh,
                                        maphyspp_@SCAL@_parameters * inParams,
                                        maphyspp_@SCAL@_output * output);

void driver_maphyspp_@SCAL@_set_transfer_mat_dense(void * handle,
                                                   maphyspp_@SCAL@_dense_matrix * tr_mat);
void driver_maphyspp_@SCAL@_set_transfer_mat_sparse(void * handle,
                                                    maphyspp_@SCAL@_coo_matrix * tr_mat);

void driver_maphyspp_@SCAL@_solve(void * handle,
                                  maphyspp_@SCAL@_dense_matrix * inB,
                                  maphyspp_@SCAL@_dense_matrix * outX);

void driver_maphyspp_@SCAL@_finalize(void * handle);

void maphyspp_@SCAL@_display_input_matrix(void * handle, int root);
void maphyspp_@SCAL@_display_input_matrix_as_dense(void * handle, int root);

#if defined __cplusplus
}
#endif
