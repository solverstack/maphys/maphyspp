#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <mpi.h>
#include "driver_mpp_d.h"

int main(int argc, char** argv){

  MPI_Init(NULL, NULL);

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if(size != 3){
    fprintf(stderr, "Error: this exemple should be launched with 3 processes\n");
    return 1;
  }

  const int M = 4;
  const int NNZ = 9;
  const int NRHS = 1;

  int * a_i = (int *) malloc(NNZ * sizeof(int));
  int * a_j = (int *) malloc(NNZ * sizeof(int));
  double * a_v = (double *) malloc(NNZ * sizeof(double));

  double * b = (double *) malloc(M * NRHS * sizeof(double));
  double * x = (double *) malloc(M * NRHS * sizeof(double));

  for(int k = 0; k < M; ++k){
    b[k] = 0;
    x[k] = 0;
  }

  if(rank == 0){
    b[0] = -13; b[1] = -1; b[2] = -2; b[3] = 12;
  }
  else if(rank == 1){
    b[0] = 12; b[1] = -2; b[2] = 5; b[3] = 17;
  }
  else{
    b[0] = 17; b[1] = 16; b[2] = -13; b[3] = 12;
  }

  a_i[0] = 0; a_j[0] = 0; a_v[0] = 3;
  a_i[1] = 0; a_j[1] = 1; a_v[1] = -1;
  a_i[2] = 0; a_j[2] = 3; a_v[2] = -1;
  a_i[3] = 1; a_j[3] = 1; a_v[3] = 4;
  a_i[4] = 1; a_j[4] = 2; a_v[4] = -1;
  a_i[5] = 1; a_j[5] = 3; a_v[5] = -1;
  a_i[6] = 2; a_j[6] = 2; a_v[6] = 3;
  a_i[7] = 2; a_j[7] = 3; a_v[7] = -1;
  a_i[8] = 3; a_j[8] = 3; a_v[8] = 4;

  maphyspp_d_coo_matrix A = {
                             .i_ptr = a_i,
                             .j_ptr = a_j,
                             .v_ptr = a_v,
                             .m = M,
                             .nnz = NNZ,
                             .storage = 1,
                             .symmetry = 1
  };
  maphyspp_d_dense_matrix B = {
                               .v_ptr = b,
                               .m = M,
                               .ld = M,
                               .n = NRHS
  };
  maphyspp_d_dense_matrix X = {
                               .v_ptr = x,
                               .m = M,
                               .ld = M,
                               .n = NRHS
  };

  maphyspp_d_neighbors topo;

  topo.n_neigh = 2;
  topo.neighbors = (int *) malloc(2 * sizeof(int));
  topo.idx_ptr = (int *) malloc(3 * sizeof(int));
  topo.indices = (int *) malloc(4 * sizeof(int));

  int * neighbors = topo.neighbors;
  int * idx_ptr = topo.idx_ptr;
  int * indices = topo.indices;
  if(rank == 0) { neighbors[0] = 1; neighbors[1] = 2; }
  if(rank == 1) { neighbors[0] = 0; neighbors[1] = 2; }
  if(rank == 2) { neighbors[0] = 0; neighbors[1] = 1; }

  idx_ptr[0] = 0; idx_ptr[1] = 2; idx_ptr[2] = 4;

  if(rank == 0) { indices[0] = 2; indices[1] = 3; indices[2] = 0; indices[3] = 3; }
  if(rank == 1) { indices[0] = 1; indices[1] = 0; indices[2] = 0; indices[3] = 3; }
  if(rank == 2) { indices[0] = 2; indices[1] = 3; indices[2] = 3; indices[3] = 0; }

  maphyspp_d_parameters params = {
                                  .solver = 1, //1: CG
                                  .precond = 2, //0: no pcd, 1: diag_pcd, 2: Additive Schwarz, 3: NeumannNeumann, 4: RobinRobin
                                  .geneo_nv = 2,
                                  .tolerance = 1e-8,
                                  .max_iter = 10,
                                  .direct_solver = 1, //1: Mumps, (2: Pastix, not available for now)
                                  .use_schur = 1,
                                  .schur_size = 0,
                                  .schur_list = NULL,
                                  .verbose = 1,
                                  .comm = MPI_COMM_WORLD,
                                  .fcomm = -1
  };

  maphyspp_d_output output;

  void * handle = driver_maphyspp_d_part_init(&A, &topo, &params, &output);

  free(a_i);
  free(a_j);
  free(a_v);
  free(topo.neighbors);
  free(topo.idx_ptr);
  free(topo.indices);

  maphyspp_d_display_input_matrix(handle, 0);
  maphyspp_d_display_input_matrix_as_dense(handle, 0);

  driver_maphyspp_d_solve(handle, &B, &X);
  driver_maphyspp_d_finalize(handle);

  for(int r = 0; r < 3; ++r){
    MPI_Barrier(MPI_COMM_WORLD);
    if(r == rank){
      for(int k = 0; k < 4; ++k) printf("%lf\n", x[k]);
      printf("\n");
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  if(rank == 0){
    printf("Number of iterations: %d\n", output.n_iter);
    printf("Residual: %lf\n", output.residual);
  }

  free(b);
  free(x);

  MPI_Finalize();

  return 0;
}
