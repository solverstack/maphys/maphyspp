#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <mpi.h>
#include "driver_mpp_d.h"

int main(int argc, char** argv){

  MPI_Init(NULL, NULL);

  const int M = 4;
  const int NNZ = 7;
  const int NRHS = 1;

  int * a_i = (int *) malloc(NNZ * sizeof(int));
  int * a_j = (int *) malloc(NNZ * sizeof(int));
  double * a_v = (double *) malloc(NNZ * sizeof(double));

  double * b = (double *) malloc(M * NRHS * sizeof(double));
  double * x = (double *) malloc(M * NRHS * sizeof(double));

  for(int k = 0; k < M; ++k){
    b[k] = 0;
    x[k] = 0;
  }
  b[0] = -1;
  b[M-1] = M;

  a_i[0] = 0; a_j[0] = 0; a_v[0] = 2;
  a_i[1] = 0; a_j[1] = 1; a_v[1] = -1;
  a_i[2] = 1; a_j[2] = 1; a_v[2] = 2;
  a_i[3] = 1; a_j[3] = 2; a_v[3] = -1;
  a_i[4] = 2; a_j[4] = 2; a_v[4] = 2;
  a_i[5] = 2; a_j[5] = 3; a_v[5] = -1;
  a_i[6] = 3; a_j[6] = 3; a_v[6] = 2;

  int n_schur = 2;
  int schur_list[2] = {1, 3};

  maphyspp_d_coo_matrix A;
  maphyspp_d_dense_matrix B;
  maphyspp_d_dense_matrix X;
  maphyspp_d_parameters params;
  maphyspp_d_output output;

  maphyspp_d_init_coo_matrix(&A);
  maphyspp_d_init_dense_matrix(&B);
  maphyspp_d_init_dense_matrix(&X);
  maphyspp_d_init_parameters(&params);

  A.m = M;
  A.nnz = NNZ;
  B.m = M;
  X.m = M;

  A.i_ptr = a_i;
  A.j_ptr = a_j;
  A.v_ptr = a_v;
  A.storage = 1;
  A.symmetry = 1;

  B.v_ptr = b;
  B.ld = M;
  B.n = NRHS;

  X.v_ptr = x;
  X.ld = M;
  X.n = NRHS;

  params.solver = 1; //1: CG
  params.precond = 1; //0: no pcd, 1: diag_pcd
  params.tolerance = 1e-8;
  params.max_iter = 10;
  params.direct_solver = 1; //1: Mumps, 2: Pastix
  params.use_schur = 1;
  params.schur_size = n_schur;
  params.verbose = 1;
  params.schur_list = &schur_list[0];

  //printf("B before:\n");
  //for(int k = 0; k < M; ++k){ printf("%lf ", b[k]); }
  //printf("\n");
  //
  //printf("X before:\n");
  //for(int k = 0; k < M; ++k){ printf("%lf ", x[k]); }
  //printf("\n");

  void * handle = driver_maphyspp_d_init(&A, &params, &output);

  free(a_i);
  free(a_j);
  free(a_v);

  driver_maphyspp_d_solve(handle, &B, &X);
  driver_maphyspp_d_finalize(handle);

  //printf("B after:\n");
  //for(int k = 0; k < M; ++k){ printf("%lf ", b[k]); }
  //printf("\n");

  printf("X:\n");
  for(int k = 0; k < M; ++k){ printf("%lf ", x[k]); }
  printf("\n");

  printf("Number of iterations: %d\n", output.n_iter);
  printf("Residual: %lf\n", output.residual);

  free(b);
  free(x);

  MPI_Finalize();

  return 0;
}
