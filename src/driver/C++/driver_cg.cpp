#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#define TIMER_RECORD_CUMULATIVE
#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0
#include <maphys.hpp>
#include <maphys/IO/ReadParam.hpp>
//#if defined(MAPHYSPP_USE_EIGEN)
//#include <maphys/wrappers/Eigen/Eigen.hpp>
//#include <maphys/wrappers/Eigen/EigenDenseSolver.hpp>
//#include <maphys/wrappers/Eigen/EigenSparseSolver.hpp>
//#endif
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/precond/AbstractSchwarz.hpp>
#include <maphys/precond/TwoLevelAbstractSchwarz.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/solver/PartSchurSolver.hpp>
#include <maphys/solver/BlasSolver.hpp>
#include <maphys/drivers/preconditioners.hpp>
#if defined(MAPHYSPP_USE_MUMPS)
#include <maphys/solver/Mumps.hpp>
#endif
#if defined(MAPHYSPP_USE_PASTIX)
#include <maphys/solver/Pastix.hpp>
#endif

#if !defined(MAPHYSPP_USE_PASTIX)
#if !defined(MAPHYSPP_USE_MUMPS)
#define MAPHYSPP_NO_DIRECT_SOLVER
#endif
#endif

#include <maphys/IO/ArgumentParser.hpp>

#include <random>

using namespace maphys;
using Scalar = double;

std::vector<int> n_iter_solves;

template<typename CgType, typename Vect>
void display_info(const CgType& cg, const PartVector<Vect>& X){
  if (MMPI::rank() == 0){
    const Vect& X_loc0 = X.get_local_vector(0);
    std::cout << "First local vector elts:\n";
    for(int k = 0; k < 15; ++k){
      std::cout << k << "\t| " << X_loc0[k] << '\n';
    }
    cg.display();
  }
}

const int seed = 1337;
std::mt19937 gen(seed);

//std::random_device rd;
//const int seed = rd() // for true random

// Randomize the vector, but keeps the same norm
template<typename LocVect>
void randomize_vector(PartVector<LocVect>& pvect){
  std::uniform_real_distribution<> dis(double{0.0}, double{1.0});
  auto norm_before = pvect.norm();
  auto randomize = [&dis](LocVect& vec){
                     for(Size k = 0; k < n_rows(vec); ++k){ vec[k] = static_cast<double>(dis(gen)); }
                   };
  pvect.apply_on_data(randomize);
  pvect.assemble(Reduction::owner_value);
  pvect *= (norm_before / pvect.norm());
}

struct solver_param{
  // Mandatory
  std::string partitions = "";
  int n_subdomains = 0;
  std::string precond = "0";
  //Optional
  bool use_schur = true;
  double tolerance = 1e-8;
  int max_iter = 1000;
  double robin_weight = 1.0;
  std::string matrix_type = "mpp";
  std::string direct_solver = "Pastix";
  bool implicit_schur = false;
  bool verb = true;
  int geneo_nv = 3;
  int defl_max_size = -1;
  int defl_increment_size = -1;
  int defl_solve_size = -1;
  int n_solves = 1;
  int n_threads = 1;
  std::string partition_type = "";

  ArgumentList arglist;

  solver_param(){
    arglist.add_argument_int("n_subdomains", "number of subdomains to load", 0, 'N');
    arglist.add_argument_str("partitions", "path to the directory containing the subdomains", std::string(), 'P');
    arglist.add_argument_real("tolerance", "stopping criterion tolerance", 1e-8, 't');
    arglist.add_argument_real("robin_weight", "weight for the Robin Robin preconditioner", 1.0);
    arglist.add_argument_bool("use_schur", "if not set, CG is applied directly on the input (sparse) matrix, otherwise on the Schur complement", 'S');
    arglist.add_argument_bool("verbose", "if set, process 0 gives additional output", 'v');
    arglist.add_argument_int("max_iter", "maximal number of iteration", 1000, 'i');
    arglist.add_argument_bool("implicit_schur", "if set, the schur complement is computed implicitly");
    arglist.add_argument_int("geneo_nv", "number of eigen value per subdomain for geneo coarse space", 3);
    arglist.add_argument_int("defl_max_size", "maximal size of deflation for eigen extraction, -1 for automatic", -1);
    arglist.add_argument_int("defl_increment_size", "number of new eigenvectors per CG solve for eigen extraction, -1 for automatic", -1);
    arglist.add_argument_int("defl_solve_size", "size of the evd problem to be solved for eigen extraction, -1 for automatic", -1);
    arglist.add_argument_int("n_solves", "number of solves to perform", 1, 's');
    arglist.add_argument_str("matrix_type", "\"mpp\" or \"Eigen\"", std::string("mpp"));
    arglist.add_argument_str("direct_solver", "\"Pastix\" or \"Mumps\"", std::string("Pastix"));
    arglist.add_argument_str("partition_type", "if \"baton\", special format for the partition can be used", std::string(""));
    arglist.add_argument_int("n_threads", "if not set, default number of threads used in each process is 1", 1, 'T');
    
    std::string pcd_descr("type of preconditioner. Possible values are:\n");
    pcd_descr += "    0 : no preconditioner (default)\n";
    pcd_descr += "    diag : diagonal (a.k.a. Jacobi) preconditioner\n";
    pcd_descr += "    AS : Additive Schwarz\n";
    pcd_descr += "    NN : Neumann Neumann\n";
    pcd_descr += "    RR : Robin Robin\n";
    pcd_descr += "    ASG+ : Additive Schwarz with coarse space correction (geneo, additive)\n";
    pcd_descr += "    ASGd : Additive Schwarz with coarse space correction (geneo, deflated)\n";
    pcd_descr += "    NNGd : Neumann Neumann with coarse space correction (geneo, deflated)\n";
    pcd_descr += "    RRGd : Robin Robin with coarse space correction (geneo, deflated)\n";
    pcd_descr += "    ASed : Additive Schwarz deflated with eigen extraction (for consecutive rhs)\n";
    pcd_descr += "    RRed : Robin Robin  deflated with eigen extraction (for consecutive rhs)\n";
    arglist.add_argument_str("precond", pcd_descr.c_str(), std::string("0"), 'p');
  }

  void display(){
    std::cout << "------\n"
              << " - Partitions: " << partitions << '\n'
              << " - Number of subdomains: " << n_subdomains << '\n'
              << " - Preconditioner: " << precond << '\n'
              << " - Use schur: " << std::boolalpha << use_schur << '\n'
              << " - Tolerance: " << tolerance << '\n'
              << " - Max iterations: " << max_iter << '\n'
              << " - Robin-Robin weight: " << robin_weight << '\n'
              << " - Matrix type: " << matrix_type << '\n'
              << " - Direct solver: " << direct_solver << '\n'
              << " - Implicit Schur: " << std::boolalpha << implicit_schur << '\n'
              << " - geneo_nv: " << geneo_nv << '\n'
              << " - defl_max_size: " << defl_max_size << '\n'
              << " - defl_increment_size: " << defl_increment_size << '\n'
              << " - defl_solve_size: " << defl_solve_size << '\n'
              << " - partition_type: " << partition_type << '\n'
              << " - n_solves: " << n_solves << '\n'
              << " - n_threads: " << n_threads << '\n'
              << " - Verbose: "  << verb << '\n';
  }

  void set_parameters(const std::map<std::string, std::string>& keymap){
    arglist.fill_value(keymap, "n_subdomains", n_subdomains);
    arglist.fill_value(keymap, "partitions", partitions);
    arglist.fill_value(keymap, "precond", precond);
    arglist.fill_value(keymap, "use_schur", use_schur);
    arglist.fill_value(keymap, "tolerance", tolerance);
    arglist.fill_value(keymap, "robin_weight", robin_weight);
    arglist.fill_value(keymap, "verbose", verb);
    arglist.fill_value(keymap, "max_iter", max_iter);
    arglist.fill_value(keymap, "implicit_schur", implicit_schur);
    arglist.fill_value(keymap, "geneo_nv", geneo_nv);
    arglist.fill_value(keymap, "defl_max_size", defl_max_size);
    arglist.fill_value(keymap, "defl_increment_size", defl_increment_size);
    arglist.fill_value(keymap, "defl_solve_size", defl_solve_size);
    arglist.fill_value(keymap, "n_solves", n_solves);
    arglist.fill_value(keymap, "matrix_type", matrix_type);
    arglist.fill_value(keymap, "direct_solver", direct_solver);
    arglist.fill_value(keymap, "partition_type", partition_type);
    arglist.fill_value(keymap, "n_threads", n_threads);
  }
  void set_from_line_arguments(int argc, char ** argv){
    set_parameters(parse_arguments(argc, argv));
  }

  void set_from_parsed_file(const std::string& filename){
    set_parameters(read_param_file(filename));
  }
};

void usage(char ** argv, const ArgumentList& arglist){
  if(MMPI::rank() == 0){
    std::cerr << "Usage: " << std::string(argv[0]) << " input_file\n"
              << "   or: " << std::string(argv[0]) << " [parameters]\n"
              << "   REQUIRED keys: partitions, n_subdomains\n\n"
              << arglist.usage() << '\n'
              << "  Ex: " << std::string(argv[0]) << " -P ./partitions -N 4 -p AS\n\n";
  }
}

template<typename MatType, typename Vect, typename SparseDirectSolver, typename Precond = Identity<MatType, Vect>>
void test_cg(const PartMatrix<MatType>& A, PartVector<Vect>& b, const std::string& test_name, solver_param& params){

  ConjugateGradient<PartMatrix<MatType>, PartVector<Vect>, Precond> cg;
  cg.setup(parameters::A{A},
           parameters::tolerance{params.tolerance},
           parameters::max_iter{params.max_iter},
           parameters::verbose{params.verb});

  [[maybe_unused]] bool is_setup = false;

  if constexpr(std::is_same<Precond, RobinRobin<PartMatrix<MatType>, PartVector<Vect>, SparseDirectSolver>>::value){
    auto setup_robin = [&](const PartMatrix<MatType>& pA, Precond& pM){
                         pM.setup(pA);
                         pM.setup(parameters::Ti<Scalar>{Scalar{params.robin_weight}});
                       };
    cg.setup(parameters::setup_pcd<std::function<void(const PartMatrix<MatType>&, Precond&)>>{setup_robin});
  }

  else if constexpr(is_pcd_geneo<Precond>::value) {
    auto setup_geneo = [&](const PartMatrix<MatType>& pA, Precond& pM){
                         if(!is_setup){
                           pM.set_n_vect(params.geneo_nv); pM.set_alpha(0); pM.set_beta(0); pM.setup(pA);
                         }
                         is_setup = true;
                       };
    auto geneo_initguess = [](const PartMatrix<MatType>& mat, const PartVector<Vect>& rhs, Precond& pcd){ (void) mat; return pcd.init_guess(rhs); };
    cg.setup(parameters::setup_pcd<std::function<void(const PartMatrix<MatType>&, Precond&)>>{setup_geneo},
             parameters::setup_init_guess<std::function<PartVector<Vect>(const PartMatrix<MatType>&, const PartVector<Vect>&, Precond&)>>{geneo_initguess});
  }

  else if constexpr(has_spectral_extraction<Precond>::value) {
    auto setup_defl = [&](const PartMatrix<MatType>& pA, Precond& pM){
                        if(!is_setup){
                          if(params.defl_max_size == -1) params.defl_max_size = std::min(std::max(1, n_rows(pA) / 10), 50);
                          if(params.defl_solve_size == -1) params.defl_solve_size = 2 * params.defl_max_size + 1;
                          if(params.defl_increment_size == -1) params.defl_increment_size = std::max(1, params.defl_max_size / 4);
                          pM.set_n_vect(params.defl_max_size); pM.set_n_vect_solve(params.defl_solve_size);
                          pM.set_nev(params.defl_increment_size); pM.setup(pA);
                        }
                        is_setup = true;
                      };
    auto defl_initguess = [](const PartMatrix<MatType>& mat, const PartVector<Vect>& rhs, Precond& pcd){ (void) mat; return pcd.init_guess(rhs); };
    cg.setup(parameters::setup_pcd<std::function<void(const PartMatrix<MatType>&, Precond&)>>{setup_defl},
             parameters::setup_init_guess<std::function<PartVector<Vect>(const PartMatrix<MatType>&, const PartVector<Vect>&, Precond&)>>{defl_initguess});
  }

  if(MMPI::rank() == 0){
    std::cout << "CG solve with: " << test_name << '\n';
  }

  Timer<1> t_maphys("Solver");

  for(int k = 0; k < params.n_solves; ++k){
    randomize_vector<Vect>(b);
    auto X = cg * b;
    display_info(cg, X);
    n_iter_solves.push_back(cg.get_n_iter());
  }
}

template<typename MatType, typename Vect, typename SparseDirectSolver, typename Precond = Identity<MatType, Vect>, bool Implicit = false>
void test_cg_schur(const PartMatrix<MatType>& A, PartVector<Vect>& b, const std::string& test_name, solver_param& params){
#if defined(MAPHYSPP_NO_DIRECT_SOLVER)
  (void) A; (void) b; (void) test_name; (void) params;
#else
  using DMat = typename dense_type<MatType>::type;
  using ConjGrad = typename std::conditional<Implicit,
                                             ConjugateGradient<PartOperator<ImplicitSchur<MatType, Vect,SparseDirectSolver>, MatType, Vect>,PartVector<Vect>, Precond>,
                                             ConjugateGradient<PartMatrix<DMat>, PartVector<Vect>, Precond>>::type;

  PartSchurSolver<PartMatrix<MatType>, PartVector<Vect>, SparseDirectSolver, ConjGrad> schur_solver;
  schur_solver.setup(parameters::A{A},
                     parameters::verbose{params.verb});

  ConjGrad& cg = schur_solver.get_solver_S();
  cg.setup(parameters::tolerance{params.tolerance},
           parameters::max_iter{params.max_iter},
           parameters::verbose{params.verb});

  [[maybe_unused]] bool is_setup = false;

  if constexpr(std::is_same<Precond, RobinRobin<PartMatrix<DMat>, PartVector<Vect>, SparseDirectSolver>>::value){
    auto setup_robin = [&](const PartMatrix<DMat>& pA, Precond& pM){
                         pM.setup(pA);
                         pM.setup(parameters::Ti<Scalar>{Scalar{params.robin_weight}});
                       };
    cg.setup(parameters::setup_pcd<std::function<void(const PartMatrix<DMat>&, Precond&)>>{setup_robin});
  }

  else if constexpr(is_pcd_geneo<Precond>::value) {
    auto setup_geneo = [&](const PartMatrix<DMat>& pA, Precond& pM){
                         if(!is_setup){
                           pM.set_n_vect(params.geneo_nv); pM.set_alpha(0); pM.set_beta(0); pM.setup(pA);
                           is_setup = true;
                         }
                       };
    auto geneo_initguess = [](const PartMatrix<DMat>& mat, const PartVector<Vect>& rhs, Precond& pcd){ (void) mat; return pcd.init_guess(rhs); };
    cg.setup(parameters::setup_pcd<std::function<void(const PartMatrix<DMat>&, Precond&)>>{setup_geneo},
             parameters::setup_init_guess<std::function<PartVector<Vect>(const PartMatrix<DMat>&, const PartVector<Vect>&, Precond&)>>{geneo_initguess});
  }

  else if constexpr(has_spectral_extraction<Precond>::value) {
    auto setup_defl = [&](const PartMatrix<DMat>& pA, Precond& pM){
                        if(!is_setup){
                          if(params.defl_max_size == -1) params.defl_max_size = std::min(std::max(1, n_rows(pA) / 10), 50);
                          if(params.defl_solve_size == -1) params.defl_solve_size = 2 * params.defl_max_size + 1;
                          if(params.defl_increment_size == -1) params.defl_increment_size = std::max(1, params.defl_max_size / 4);
                          pM.set_n_vect(params.defl_max_size); pM.set_n_vect_solve(params.defl_solve_size);
                          pM.set_nev(params.defl_increment_size); pM.setup(pA);
                          is_setup = true;
                        }
                      };
    auto defl_initguess = [](const PartMatrix<DMat>& mat, const PartVector<Vect>& rhs, Precond& pcd){ (void) mat; return pcd.init_guess(rhs); };
    cg.setup(parameters::setup_pcd<std::function<void(const PartMatrix<DMat>&, Precond&)>>{setup_defl},
             parameters::setup_init_guess<std::function<PartVector<Vect>(const PartMatrix<DMat>&, const PartVector<Vect>&, Precond&)>>{defl_initguess});
  }

  if(MMPI::rank() == 0){
    std::cout << "CG solve with: " << test_name << '\n';
  }

  Timer<1> t_maphys("Solver");

  for(int k = 0; k < params.n_solves; ++k){
    randomize_vector<Vect>(b);
    auto X = schur_solver * b;
    display_info(cg, X);
    n_iter_solves.push_back(cg.get_n_iter());
  }
#endif
}

template<typename Scalar, typename MatType, typename VectType>
struct dense_solver { using type = BlasSolver<MatType, VectType>; };
//#if defined(MAPHYSPP_USE_EIGEN)
//template<typename Scalar>
//struct dense_solver<Scalar, EigenDenseMatrix<Scalar>, Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> {
//  using type = EigenDenseSolver<EigenDenseMatrix<Scalar>, Eigen::Matrix<Scalar, Eigen::Dynamic, 1>>;
//};
//#endif

template<typename MatType, typename SparseDirectSolver>
void bench(std::shared_ptr<Process> p, solver_param& params, char ** argv){

  // Load matrix and RHS
  PartMatrix<SparseMatrixCOO<Scalar>> Acoo(p);
  PartVector<Vector<Scalar>> bmph(p);
  if(params.partition_type == "baton") { 
    load_baton_subdomains(params.partitions, params.n_subdomains, p, Acoo, bmph); 
  }
  else { 
    load_subdomains_and_data(params.partitions, params.n_subdomains, p, Acoo, bmph); 
  }
  // b must be assembled!
  bmph.assemble();
  // Set to symmetric positive definite, lower storage
  auto set_spd_lower = [](SparseMatrixCOO<Scalar>& loc_mat){
                         loc_mat.set_spd(MatrixStorage::lower);
                         loc_mat.order_indices();
                       };               
  Acoo.apply_on_data(set_spd_lower);

  if (MMPI::rank() == 0){
    std::cout << " - Size of global K: " << p->get_n_glob() << '\n';
    std::cout << "------\n";
  }

  using Vect = typename vector_type<MatType>::type;
  PartMatrix<MatType> A;
  PartVector<Vect> b;
  if constexpr(std::is_same_v<SparseMatrixCOO<Scalar>, MatType>){
    A = std::move(Acoo);
    b = std::move(bmph);
  }
  else if constexpr(std::is_same_v<SparseMatrixCSC<Scalar>, MatType>){
    auto coo_to_csc = [](const SparseMatrixCOO<Scalar>& coo) { return coo.to_csc(); };
    A = Acoo.convert<MatType>(coo_to_csc);
    b = std::move(bmph);
    Acoo = PartMatrix<SparseMatrixCOO<Scalar>>(); // Free memory
  }
//#if defined(MAPHYSPP_USE_EIGEN)
//  else{
//    auto coo_to_eigen = [](const SparseMatrixCOO<Scalar>& coo) {
//                          EigenSparseMatrix<Scalar> A_eigen;
//                          build_matrix<Scalar>(A_eigen, static_cast<int>(n_rows(coo)), static_cast<int>(n_cols(coo)), static_cast<int>(n_nonzero(coo)),
//                                               const_cast<int *>(coo.get_i_ptr()), const_cast<int *>(coo.get_j_ptr()), const_cast<Scalar *>(coo.get_v_ptr()), /*fill_symmetry*/ false);
//                          A_eigen.set_spd(MatrixStorage::lower);
//                          return A_eigen;
//                        };
//    A = Acoo.convert<MatType>(coo_to_eigen);
//    Acoo = PartMatrix<SparseMatrixCOO<Scalar>>(); // Free memory
//
//    auto vmph_to_veigen = [](const Vector<Scalar>& vmph) {
//                            Vect veigen(n_rows(vmph));
//                            std::memcpy(get_ptr(veigen), get_ptr(vmph), sizeof(Scalar) * n_rows(vmph));
//                            return veigen;
//                          };
//    b = bmph.convert<Vect>(vmph_to_veigen);
//    bmph = PartVector<Vector<Scalar>>();
//  }
//#endif

  if(p->owns_subdomain(0)){
    MAPHYSPP_ASSERT(A.get_local_matrix(0).is_spd(), "A is not SPD!!!");
  }

  using DMat = typename dense_type<MatType>::type;
  using DenseDirectSolver = typename dense_solver<Scalar, DMat, Vect>::type;

  //(0/K,0/S,diag/K,diag/S,AS/K,AS/S,NN/K,NN/S,RR/K,RR/S,J/S)
  if(params.precond == "0"){
    if(params.use_schur){
      using PCD = Identity<PartMatrix<DMat>, decltype(b)>;
      if(params.implicit_schur) { test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, true>(A, b, "no precond. on S, implicit S", params); }
      else{ test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "no precond. on S", params); }
    }
    else{
      using PCD = Identity<decltype(A), decltype(b)>;
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "no precond. on K", params);
    }
  }

  else if(params.precond == "diag"){
    if(params.use_schur){
      using PCD = decltype(precond_diagonal<PartMatrix<DMat>, decltype(b)>());
      if(params.implicit_schur) { test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, true>(A, b, "diagonal precond on S, implicit S", params); }
      else{ test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "diagonal precond on S", params); }
    }
    else{
      using PCD = decltype(precond_diagonal<decltype(A), decltype(b)>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "diagonal precond on K", params);
    }
  }

  else if(params.precond == "AS"){
    if(params.use_schur){
      using PCD = decltype(precond_additive_schwarz<PartMatrix<DMat>, decltype(b), DenseDirectSolver>());
      if(params.implicit_schur) { test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, true>(A, b, "Additive Schwarz on S, implicit S", params); }
      else{ test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Additive Schwarz on S", params); }
    }
    else{
      using PCD = decltype(precond_additive_schwarz<decltype(A), decltype(b), SparseDirectSolver>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Additive Schwarz on K", params);
    }
  }

  else if(params.precond == "NN"){
    if(params.use_schur){
      using PCD = decltype(precond_neumann_neumann<PartMatrix<DMat>, decltype(b), DenseDirectSolver>());
      if(params.implicit_schur) { test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, true>(A, b, "Neumann Neumann on S, implicit S", params); }
      else{ test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Neumann Neumann on S", params); }
    }
    else{
      using PCD = decltype(precond_neumann_neumann<decltype(A), decltype(b), SparseDirectSolver>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Neumann Neumann on K", params);
    }
  }

  else if(params.precond == "RR"){
    if(params.use_schur){
      using PCD = decltype(precond_robin_robin<PartMatrix<DMat>, decltype(b), DenseDirectSolver>());
      if(params.implicit_schur) { test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, true>(A, b, "Robin Robin on S, implicit S", params); }
      else{ test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Robin Robin on S", params); }
    }
    else{
      using PCD = decltype(precond_robin_robin<decltype(A), decltype(b), SparseDirectSolver>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Robin Robin on K", params);
    }
  }

  else if(params.precond == "ASG+"){
#if defined(MAPHYSPP_NO_DIRECT_SOLVER)
    if(MMPI::rank() == 0) std::cerr << "A direct solver is needed for precond ASG+\n";
#else
    if(params.use_schur){
      using PCD = decltype(precond_geneo_AS<PartMatrix<DMat>, decltype(b), DenseDirectSolver, true>());
      test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Additive Schwarz + GenEO CSC on S", params);
    }
    else{
#if defined(MAPHYSPP_USE_ARPACK)
      using PCD = decltype(precond_geneo_AS<decltype(A), decltype(b), SparseDirectSolver, true>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Additive Schwarz + GenEO on K", params);
#else
      if(MMPI::rank() == 0) std::cerr << "Arpack is required to compute eigen problem on a sparse matrix\n";
#endif
    }
#endif
  }

  else if(params.precond == "ASGd"){
#if defined(MAPHYSPP_NO_DIRECT_SOLVER)
    if(MMPI::rank() == 0) std::cerr << "A direct solver is needed for precond ASGd\n";
#else
    if(params.use_schur){
      using PCD = decltype(precond_geneo_AS<PartMatrix<DMat>, decltype(b), DenseDirectSolver, false>());
      test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Additive Schwarz + deflated GenEO CSC on S", params);
    }
    else{
#if defined(MAPHYSPP_USE_ARPACK)
      using PCD = decltype(precond_geneo_AS<decltype(A), decltype(b), SparseDirectSolver, false>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Additive Schwarz + deflated GenEO on K", params);
#else
      if(MMPI::rank() == 0) std::cerr << "Arpack is required to compute eigen problem on a sparse matrix\n";
#endif
    }
#endif
  }

  else if(params.precond == "NNGd"){
#if defined(MAPHYSPP_NO_DIRECT_SOLVER)
    if(MMPI::rank() == 0) std::cerr << "A direct solver is needed for precond NNGd\n";
#else
    if(params.use_schur){
      using PCD = decltype(precond_geneo_NN<PartMatrix<DMat>, decltype(b), DenseDirectSolver>());
      test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Neumann Neumann + deflated GenEO CSC on S", params);
    }
    else{
#if defined(MAPHYSPP_USE_ARPACK)
      using PCD = decltype(precond_geneo_NN<decltype(A), decltype(b), SparseDirectSolver>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Neumann Neumann + deflated GenEO on K", params);
#else
      if(MMPI::rank() == 0) std::cerr << "Arpack is required to compute eigen problem on a sparse matrix\n";
#endif
    }
#endif
  }

  else if(params.precond == "RRGd"){
#if defined(MAPHYSPP_NO_DIRECT_SOLVER)
    if(MMPI::rank() == 0) std::cerr << "A direct solver is needed for precond RRGd\n";
#else
    if(params.use_schur){
      using PCD = decltype(precond_geneo_RR<PartMatrix<DMat>, decltype(b), DenseDirectSolver>());
      test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Robin Robin + deflated GenEO CSC on S", params);
    }
    else{
#if defined(MAPHYSPP_USE_ARPACK)
      using PCD = decltype(precond_geneo_RR<decltype(A), decltype(b), SparseDirectSolver>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Robin Robin + deflated GenEO on K", params);
#else
      if(MMPI::rank() == 0) std::cerr << "Arpack is required to compute eigen problem on a sparse matrix\n";
#endif
    }
#endif
  }

  else if(params.precond == "ASed"){
#if defined(MAPHYSPP_USE_FABULOUS)
    if(params.use_schur){
      using PCD = decltype(precond_defl_AS<PartMatrix<DMat>, decltype(b), DenseDirectSolver>());
      test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Additive Schwarz + eigen deflation on S", params);
    }
    else{
      using PCD = decltype(precond_defl_AS<decltype(A), decltype(b), SparseDirectSolver>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Additive Schwarz + eigen deflation on K", params);
    }
#else
    if(MMPI::rank() == 0) std::cerr << "Fabulous is required to use eigen deflation\n";
#endif
  }

  else if(params.precond == "RRed"){
#if defined(MAPHYSPP_USE_FABULOUS)
    if(params.use_schur){
      using PCD = decltype(precond_defl_RR<PartMatrix<DMat>, decltype(b), DenseDirectSolver>());
      test_cg_schur<MatType, Vect, SparseDirectSolver, PCD, false>(A, b, "Robin Robin + eigen deflation on S", params);
    }
    else{
      using PCD = decltype(precond_defl_RR<decltype(A), decltype(b), SparseDirectSolver>());
      test_cg<MatType, Vect, SparseDirectSolver, PCD>(A, b, "Robin Robin + eigen deflation on K", params);
    }
#else
    if(MMPI::rank() == 0) std::cerr << "Fabulous is required to use eigen deflation\n";
#endif
  }

  else{
    if(MMPI::rank() == 0) std::cerr << "Precond type '" << params.precond << "' not recognized\n";
    usage(argv, params.arglist);
  }
}

int main(int argc, char ** argv) {

  MMPI::init(MPI_THREAD_MULTIPLE);
  {
    Timer<0> tt("Total time");
    const int rank = MMPI::rank();

    solver_param params;

    // Load parameters from file
    if(argc == 2){
      std::string filename = std::string(argv[1]);
      auto check_file_exists = [](const std::string& fname){
                                 std::ifstream f(fname.c_str());
                                 return f.good();
                               };
      if(check_file_exists(filename)){ params.set_from_parsed_file(filename); }
    }
    // Load parameters command line
    else{
      params.set_from_line_arguments(argc, argv);
    }

  // Check required keys
  {
    bool error = false;
    if(params.partitions.empty()){
      if(rank == 0) std::cerr << "Required key not found: partitions\n";
      error = true;
    }
    if(params.n_subdomains < 1){
      if(rank == 0) std::cerr << "Required key not found: n_subdomains\n";
      error = true;
    }
    if(error){
      usage(argv, params.arglist);
      MMPI::finalize();
      return 1;
    }
  }

#if !defined(MAPHYSPP_USE_PASTIX)
  params.direct_solver = "Mumps";
#endif

#if !defined(MAPHYSPP_USE_MUMPS)
  params.direct_solver = "Pastix";
#endif

#if defined(MAPHYSPP_NO_DIRECT_SOLVER)
    params.direct_solver = "Unavailable";
    params.use_schur = false;
#endif

  if(rank == 0){
    params.display();
  }
  if(MMPI::rank() != 0) params.verb = false;

  // Subdomain topology
  auto p = bind_subdomains(params.n_subdomains, params.n_threads);

  if(params.matrix_type == "Eigen"){
//#if defined(MAPHYSPP_USE_EIGEN)
//      using E_Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
//#if defined(MAPHYSPP_USE_PASTIX)
//      if(params.direct_solver == "Pastix"){
//        bench<EigenSparseMatrix<Scalar>, Pastix<EigenSparseMatrix<Scalar>, E_Vector>>(p, params, argv);
//      }
//#endif
//#if defined(MAPHYSPP_USE_MUMPS)
//      if(params.direct_solver == "Mumps"){
//        bench<EigenSparseMatrix<Scalar>, Mumps<EigenSparseMatrix<Scalar>, E_Vector>>(p, params, argv);
//      }
//#endif

//#else
//      usage(argv, params.arglist);
//      if (rank == 0) std::cerr << "matrix_type is Eigen but eigen is not available !\n";
//#endif
      if (rank == 0) std::cerr << "matrix_type eigen not supported !\n";
    }
    else{
#if defined(MAPHYSPP_USE_PASTIX)
      if(params.direct_solver == "Pastix"){
        bench<SparseMatrixCSC<Scalar>, Pastix<SparseMatrixCSC<Scalar>, Vector<Scalar>>>(p, params, argv);
      }
#endif
#if defined(MAPHYSPP_USE_MUMPS)
      if(params.direct_solver == "Mumps"){
        bench<SparseMatrixCSC<Scalar>, Mumps<SparseMatrixCSC<Scalar>, Vector<Scalar>>>(p, params, argv);
      }
#endif
#if defined(MAPHYSPP_NO_DIRECT_SOLVER)
      bench<SparseMatrixCSC<Scalar>, Identity<SparseMatrixCSC<Scalar>, Vector<Scalar>>>(p, params, argv);
#endif
    }

    tt.stop();

    if(rank == 0){
      for(size_t k = 0; k < n_iter_solves.size(); ++k){
        std::cout << "n_iter_solve: " << k << ", " << n_iter_solves[k] << '\n';
      }
    }

    std::map<std::string, double> output_times;
    std::map<std::string, std::vector<std::string>> capture_timers;
    if(rank == 0){
      capture_timers["Schur Computation"] = std::vector<std::string>{"Compute Schur"};
      capture_timers["Local Pcd Setup"] = std::vector<std::string>{"Precond setup aS", "Precond setup diagonal"};
      capture_timers["Coarse Eigen Solve"] = std::vector<std::string>{"lapack_EVD_pb", "arpack_GEVD_pb", "lapack_GEVD_pb"};
      capture_timers["Coarse Pcd Setup"] = std::vector<std::string>{"CoarseSolve_setup"};
      capture_timers["Direct Solve"] = std::vector<std::string>{"Pastix b2f", "Pastix y2x", "Mumps b2f", "Mumps y2x", "EigenSparseSolver b2f", "EigenSparseSolver y2x"};
      capture_timers["Iterative Solve"] = std::vector<std::string>{"Iterative solve"};
      capture_timers["Total time"] = std::vector<std::string>{"Solver"};
      capture_timers["Runtime"] = std::vector<std::string>{"Total time"};
      for(const auto& [step, measures] : capture_timers){
	      (void) measures;
	      output_times[step] = double{0};
      }
      std::cout << "+---------+-----+-----+-----+\n";
      std::cout << "| Measure | max | avg | min |\n";
    }

    std::vector<std::string> keys = Timer<1>::get_event_keys();
    std::sort(keys.begin(), keys.end());
    //std::cout << rank << " has " << keys.size() << '\n';
    for(std::string& key : keys){
      double time = Timer<1>::get_event_cumul_time(key);
      double max_time, min_time, sum_time;
      MMPI::reduce(&time, &max_time, 1, MPI_MAX, 0, MPI_COMM_WORLD);
      MMPI::reduce(&time, &min_time, 1, MPI_MIN, 0, MPI_COMM_WORLD);
      MMPI::reduce(&time, &sum_time, 1, MPI_SUM, 0, MPI_COMM_WORLD);
      if(rank == 0){
        double avg_time = (sum_time / MMPI::size());
        std::cout << "| " << key << " | " << max_time << " | " << avg_time << " | " << min_time << "|\n";

        for(const auto& [step, measures] : capture_timers){
          for(const std::string& capt_step : measures){
            if(key == capt_step){
              output_times[step] += max_time;
            }
          }
        }

        // Hack
        // For aS/K, the factorization time should be taken from the iterative time
        // because it is done on the first iteration
        if(key == "Pastix facto" || key == "Mumps facto"){
          if(params.precond == "AS/K" || params.precond == "NN/K" || params.precond == "RR/K"){
            output_times["Iterative Solve"] -= max_time;
            output_times["Local Pcd Setup"] += max_time;
          }
        }
      }
    }

    if(rank == 0){
      std::cout << "+---------+-----+-----+-----+\n\n";
      for(const auto& [step, time] : output_times){
        std::cout << step << ": " << time << '\n';
      }
    }
  }
  MMPI::finalize();

  return 0;
}
