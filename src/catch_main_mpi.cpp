#include <catch2/catch_session.hpp>
#include <maphys/dist/MPI.hpp>

int main( int argc, char* argv[] ) {
  maphys::MMPI::init(MPI_THREAD_SERIALIZED);
  int result = Catch::Session().run( argc, argv );
  maphys::MMPI::finalize();
  return result;
}
