#include <iostream>
#include <vector>
#include <mpi.h>
#include <maphys.hpp>
#include <maphys/part_data/PartMatrix.hpp>
#include <maphys/solver/ConjugateGradient.hpp>

int main(){

  using namespace maphys;
  using Scalar = double;

  MMPI::init();
  {
    const int rank = MMPI::rank();

    const int M = 4;
    const int NNZ = 9;
    const int N_DOFS = 4;
    const int N_SUBDOMAINS = 3;

    // Bind subdomains to MPI process
    std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);

    // Define topology for each subdomain
    std::vector<Subdomain> sd;
    if(p->owns_subdomain(0)){
      std::map<int, IndexArray<int>> nei_map_0 {{1, {2, 3}},
                                                {2, {0, 3}}};
      sd.emplace_back(0, N_DOFS, std::move(nei_map_0));
    }
    if(p->owns_subdomain(1)){
      std::map<int, IndexArray<int>> nei_map_1 {{0, {1, 0}},
                                                {2, {0, 3}}};
      sd.emplace_back(1, N_DOFS, std::move(nei_map_1));
    }
    if(p->owns_subdomain(2)){
      std::map<int, IndexArray<int>> nei_map_2 {{0, {2, 3}},
                                                {1, {3, 0}}};
      sd.emplace_back(2, N_DOFS, std::move(nei_map_2));
    }
    p->load_subdomains(sd);

    // Local matrices (here the same matrix on every subdomain)
    std::vector<int> a_i{0, 0, 0, 1, 1, 1, 2, 2, 3};
    std::vector<int> a_j{0, 1, 3, 1, 2, 3, 2, 3, 3};
    std::vector<Scalar> a_v{3, -1, -1, 4, -1, -1, 3, -1, 4};
    SparseMatrixCOO<Scalar, int> A_loc(M, M, NNZ, a_i.data(), a_j.data(), a_v.data());
    A_loc.set_spd(MatrixStorage::upper);

    // Create the partitioned matrix
    std::map<int, SparseMatrixCOO<Scalar, int>> A_map;
    for(int k = 0; k < 3; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
    const PartMatrix<SparseMatrixCOO<Scalar>> A(p, std::move(A_map));

    // Local rhs vectors
    std::map<int, Vector<Scalar>> b_map;
    if(p->owns_subdomain(0)) b_map[0] = Vector<Scalar>{-13, -1, -2, 12};
    if(p->owns_subdomain(1)) b_map[1] = Vector<Scalar>{12, -2, 5, 17};
    if(p->owns_subdomain(2)) b_map[2] = Vector<Scalar>{17, 16, -13, 12};

    // Create the partitioned vector
    const PartVector<Vector<Scalar>> b(p, std::move(b_map));

    // Solver definition
    using Solver = ConjugateGradient<PartMatrix<SparseMatrixCOO<Scalar>>, PartVector<Vector<Scalar>>>;

    // Setup
    Solver iter_solver;
    iter_solver.setup(A);

    // Set iterative solver parameters
    iter_solver.setup(parameters::max_iter{50},
                      parameters::tolerance{1e-8},
                      parameters::verbose{(rank == 0)});

    // Solve
    auto X = iter_solver * b;

    X.display_centralized("X found");
    if(rank == 0) std::cout << "Niter: " << iter_solver.get_n_iter() << '\n';
  }
  MMPI::finalize();

  return 0;
}
