/*
 * In this example we use the ConjugateGradient with different operators and vector types
 * - maphys internal dense and sparse matrices, with maphys vectors
 * - maphys 1D-laplacian operator, with maphys vectors
 * - eigen dense and sparse matrices, with eigen vectors
 * - armadillo dense and sparse matrices, with armadillo vectors
 * The system solved is :
 * ( 2 -1  0  0 )   ( 3 )   (  1 )
 * (-1  2 -1  0 )   ( 5 )   (  0 )
 * ( 0 -1  2 -1 ) x ( 7 ) = (  0 )
 * ( 0  0 -1  2 )   ( 9 )   ( 11 )
 */

#include <iostream>
#include <vector>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <armadillo>

namespace maphys{
  double dot(const Eigen::VectorXd&, const Eigen::VectorXd&);
  auto size(const Eigen::Matrix<double, -1, 1, 0>&);
  auto n_rows(const Eigen::Matrix<double, -1, 1, 0>&);
  auto n_rows(const Eigen::SparseMatrix<double>&);
  auto size(const arma::Col<double>&);
  auto n_rows(const arma::Mat<double>&);
  auto n_rows(const arma::SpMat<double>&);
}

#include <maphys.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/loc_data/Laplacian.hpp>

namespace maphys{
  template<> struct scalar_type<Eigen::VectorXd>{ using type = double; };
  double dot(const Eigen::VectorXd& v1, const Eigen::VectorXd& v2){ return v1.dot(v2); }

  auto n_rows(const Eigen::Matrix<double, -1, 1, 0>& m){ return m.size(); }
  auto size(const Eigen::Matrix<double, -1, 1, 0>& m){ return m.size(); }
  template<> struct vector_type<Eigen::MatrixXd>{ using type = Eigen::VectorXd; };
  template<> struct scalar_type<Eigen::MatrixXd>{ using type = double; };

  auto n_rows(const Eigen::SparseMatrix<double>& m){ return m.size(); }
  template<> struct vector_type<Eigen::SparseMatrix<double>>{ using type = Eigen::VectorXd; };
  template<> struct scalar_type<Eigen::SparseMatrix<double>>{ using type = double; };

  template<> struct scalar_type<arma::Col<double>>{ using type = double; };

  auto n_rows(const arma::Mat<double>& m){ return m.size(); }
  auto size(const arma::Col<double>& m){ return m.size(); }
  template<> struct vector_type<arma::Mat<double>>{ using type = arma::Col<double>; };
  template<> struct scalar_type<arma::Mat<double>>{ using type = double; };

  auto n_rows(const arma::SpMat<double>& m){ return m.size(); }
  template<> struct vector_type<arma::SpMat<double>>{ using type = arma::Col<double>; };
  template<> struct scalar_type<arma::SpMat<double>>{ using type = double; };
}

int main() {

  using Scalar = double;
  using Real = double;

  // Maphys classes
  using MPH_D_mat = maphys::DenseMatrix<Scalar>;
  using MPH_SP_mat = maphys::SparseMatrixCOO<Scalar>;
  using MPH_Laplacian = maphys::Laplacian<Scalar>;
  using MPH_vect = maphys::Vector<Scalar>;

  // CG parameters
  const int max_iter = 10; // 4 should be enough
  const Real tolerance = Real{1e-5};
  const bool verbose = true;

  const int m = 4;
  const MPH_vect X_expected{3.0, 5.0, 7.0, 9.0};

#if defined(MAPHYSPP_USE_RSB) || defined(MAPHYSPP_USE_RSB_SPBLAS)
  if(rsb_lib_init(RSB_NULL_INIT_OPTIONS) != RSB_ERR_NO_ERROR) return -1;
#endif

  /************************************************************************/
  /************************************************************************/
  std::cout << "Solving with maphys dense matrix" << std::endl << std::endl;

  MPH_D_mat A1({2.0,  -1.0,  0.0,  0.0,
               -1.0,   2.0, -1.0,  0.0,
               0.0,  -1.0,  2.0, -1.0,
               0.0,   0.0, -1.0,  2.0}, m, m);
  A1.set_spd(maphys::MatrixStorage::full);

  const MPH_vect B{1.0, 0.0, 0.0, 11.0};

  maphys::ConjugateGradient<MPH_D_mat, MPH_vect> cg_mph_dense;
  cg_mph_dense.setup(maphys::parameters::A<MPH_D_mat>{A1},
                     maphys::parameters::max_iter<int>{max_iter},
                     maphys::parameters::tolerance<Real>{tolerance},
                     maphys::parameters::verbose<bool>{verbose});

  MPH_vect X1 = cg_mph_dense * B;
  X1.display("X1");

  cg_mph_dense.display("CG maphys dense");

  /************************************************************************/
  /************************************************************************/
  std::cout << "Solving with maphys sparse matrix" << std::endl << std::endl;

  MPH_SP_mat A2({  0,    0,   1,    1,   2,    2,   3},
                {  0,    1,   1,    2,   2,    3,   3},
                {2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0},
                4, 4);
  A2.set_spd(maphys::MatrixStorage::lower);

  maphys::ConjugateGradient<MPH_SP_mat, MPH_vect> cg_mph_sparse;
  cg_mph_sparse.setup( maphys::parameters::A<MPH_SP_mat>{A2},
                      maphys::parameters::max_iter<int>{max_iter},
                      maphys::parameters::tolerance<Real>{tolerance},
                      maphys::parameters::verbose<bool>{verbose});

  MPH_vect X2 = cg_mph_sparse * B;
  X1.display("X2");

  cg_mph_sparse.display("CG maphys sparse");

  /************************************************************************/
  /************************************************************************/
  std::cout << "Solving with maphys 1D-laplacian function" << std::endl << std::endl;
  MPH_Laplacian lap;
  maphys::ConjugateGradient<MPH_Laplacian, MPH_vect> cg_mph_lap;
  cg_mph_lap.setup( maphys::parameters::A<MPH_Laplacian>{lap},
                   maphys::parameters::max_iter<int>{max_iter},
                   maphys::parameters::tolerance<Real>{tolerance},
                   maphys::parameters::verbose<bool>{verbose});

  MPH_vect X3 = cg_mph_lap * B;
  X3.display("X3");

  cg_mph_lap.display("CG maphys laplacian");

  /************************************************************************/
  /************************************************************************/
  // Eigen classes
  using E_mat = Eigen::MatrixXd;
  using E_vect = Eigen::VectorXd;

  std::cout << "Solving with eigen matrix" << std::endl << std::endl;

  E_mat A3(m, m);
  A3 << 2.0, -1.0,  0.0,  0.0,
			  -1.0,  2.0, -1.0,  0.0,
					     0.0, -1.0,  2.0, -1.0,
							      0.0,  0.0, -1.0,  2.0;

  E_vect B2(m);
  B2<< 1.0, 0.0, 0.0, 11.0;

  maphys::ConjugateGradient<E_mat, E_vect> cg_eigen;
  cg_eigen.setup( maphys::parameters::A<E_mat>{A3},
                 maphys::parameters::max_iter<int>{max_iter},
                 maphys::parameters::tolerance<Real>{tolerance},
                 maphys::parameters::verbose<bool>{verbose});

  E_vect X4 = cg_eigen * B2;
  std::cout << "X4\t" << X4.transpose() << std::endl;

  cg_eigen.display("CG with eigen matrix");

  // Eigen sparse class
  using E_SP_mat = Eigen::SparseMatrix<double>;
  using E_tri = Eigen::Triplet<double>;

  E_SP_mat A3_sp(4, 4);

  std::vector<E_tri> A3_sp_data {E_tri(0,0, 2.0),
      E_tri(0,1,-1.0),
      E_tri(1,1, 2.0),
      E_tri(1,2,-1.0),
      E_tri(2,2, 2.0),
      E_tri(2,3,-1.0),
      E_tri(3,3, 2.0)};

  A3_sp.setFromTriplets(A3_sp_data.begin(), A3_sp_data.end());

  maphys::ConjugateGradient<decltype(A3_sp), E_vect> cg_sp_eigen;
  cg_sp_eigen.setup( maphys::parameters::A<decltype(A3_sp)>{A3_sp},
                    maphys::parameters::max_iter<int>{max_iter},
                    maphys::parameters::tolerance<Real>{tolerance},
                    maphys::parameters::verbose<bool>{verbose});

  E_vect X4_bis = cg_sp_eigen * B2;
  std::cout << "X4_bis\t" << X4_bis.transpose() << std::endl;

  /************************************************************************/
  /************************************************************************/
  // Armadillo classes
  using ARMA_mat = arma::Mat<Scalar>;
  using ARMA_vect = arma::Col<Scalar>;

  std::cout << "Solving with armadillo matrix" << std::endl << std::endl;

  const ARMA_mat A4 = {
    {2.0, -1.0, 0.0, 0.0},
    {-1.0, 2.0, -1.0, 0.0},
    {0.0, -1.0, 2.0, -1.0},
    {0.0, 0.0, -1.0, 2.0}};

  const ARMA_vect B3 = {1.0, 0.0, 0.0, 11.0};

  maphys::ConjugateGradient<ARMA_mat, ARMA_vect> cg_arma;
  cg_arma.setup( maphys::parameters::A<ARMA_mat>{A4},
                maphys::parameters::max_iter<int>{max_iter},
                maphys::parameters::tolerance<Real>{tolerance},
                maphys::parameters::verbose<bool>{verbose});

  ARMA_vect X5 = cg_arma * B3;
  X5.print("X5");

  cg_arma.display("CG with armadillo matrix");

  using ARMA_SPmat = arma::SpMat<Scalar>;

  const ARMA_SPmat A5(A4);

  maphys::ConjugateGradient<ARMA_SPmat, ARMA_vect> cg_arma_sp;
  cg_arma_sp.setup( maphys::parameters::A<ARMA_SPmat>{A5},
                   maphys::parameters::max_iter<int>{max_iter},
                   maphys::parameters::tolerance<Real>{tolerance},
                   maphys::parameters::verbose<bool>{verbose});

  ARMA_vect X6 = cg_arma_sp * B3;
  X6.print("X6");

  cg_arma_sp.display("CG with armadillo sparse matrix");

#if defined(MAPHYSPP_USE_RSB) || defined(MAPHYSPP_USE_RSB_SPBLAS)
  if(rsb_lib_exit(RSB_NULL_EXIT_OPTIONS) != RSB_ERR_NO_ERROR) return -1;
#endif

  return 0;
}
