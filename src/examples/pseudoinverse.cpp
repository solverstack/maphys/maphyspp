#include <iostream>
#include <vector>
#include <maphys.hpp>

int main() {
  using namespace maphys;
  using Scalar = double;

  const DenseMatrix<Scalar> A_square(
                               {3., -1,  0,  0,
                                -1,  2, -1,  0,
                                0 , -1,  3, -1,
                                0 ,  0, -1,  2}, 4, 4);

  const Vector<Scalar> x_exp1({1, 2, 3, 4});
  x_exp1.display("Expected solution 1");
  const Vector<Scalar> b1 = A_square * x_exp1;

  // Solve the system once with operator%

  const Vector<Scalar> x1 = A_square % b1;
  x1.display("A % b1");

  // Write ~A as the inverse of A
  auto inv_A = ~A_square;
  // And solve the system with a multiplication by b
  const Vector<Scalar> x2 = inv_A * b1;
  x2.display("~A * b1");

  // ~A can be used again with another right-hand side
  const Vector<Scalar> x_exp2({-1, 2, 1, -7});
  x_exp2.display("Expected solution 2");
  const Vector<Scalar> b2 = A_square * x_exp2;

  const Vector<Scalar> x3 = inv_A * b2;
  x3.display("~A * b2");

  // -----------------------
  // Now with a rectangular A, more rows than columns
  const DenseMatrix<Scalar> A_2(
                                {3., -1,  0,
                                 -1,  2, -1,
                                 0 , -1,  3,
                                 0 ,  2, -1}, 4, 3, true);

  A_2.display("A rectangular M > N");

  // A % b or ~A * b now solves the least squares problem
  const Vector<Scalar> x4 = A_2 % b1;
  x4.display("Least squares x");

  // With a rectangular A, more columns than rows
  const DenseMatrix<Scalar> A_3(A_2.t());
  A_3.display("A rectangular M < N");

  const Vector<Scalar> b3{1, 2, 3};

  const Vector<Scalar> x5 = ~A_3 * b3;
  x5.display("Minimal norm x");

  // -------------------------
  // Solve sparse linear system

#if defined(MAPHYSPP_USE_PASTIX) || defined(MAPHYSPP_USE_MUMPS)
  MMPI::init();

  const SparseMatrixCOO<Scalar> A_sp(A_square);
  A_sp.display("A sparse");

  const Vector<Scalar> x6 = A_sp % b1;
  x6.display("A sparse % b1");

  MMPI::finalize();
#endif

  return 0;
}
