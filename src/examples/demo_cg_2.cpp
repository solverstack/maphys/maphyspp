#include <iostream>

#include <maphys.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>

int main() {

  const std::string matrix_file_real = "../../../matrices/bcsstk17.mtx";
  const std::string matrix_file_complex = "../../../matrices/young1c.mtx";

  using Mat_c = maphys::SparseMatrixCOO<std::complex<float>>;

  using Mat_d = maphys::SparseMatrixCOO<double>;
  using Vect_d = maphys::Vector<double>;
  using Pcd_d = maphys::DiagonalPrecond<Mat_d, Vect_d>;

  Mat_d A_d;
  A_d.from_matrix_market_file(matrix_file_real);
  A_d.set_spd( maphys::MatrixStorage::lower );

  Mat_c A_c;
  A_c.from_matrix_market_file(matrix_file_complex);

  std::cout << "bcsstk17: m = " << A_d.get_n_rows()
            << ", n = " << A_d.get_n_cols()
            << ", nnz = " << A_d.get_nnz()
            << std::endl;

  std::cout << "young1c: m = " << A_c.get_n_rows()
            << ", n = " << A_c.get_n_cols()
            << ", nnz = " << A_c.get_nnz()
            << std::endl;

  int m = A_d.get_n_rows();
  Vect_d solution(m);
  double * x_data = solution.get_ptr();
  for(auto k = 0; k < m; ++k){
    x_data[k] = (double) k;
  }

  Vect_d rhs = A_d * solution;

  Pcd_d diag_pcd(A_d);

  maphys::ConjugateGradient<Mat_d, Vect_d, Pcd_d> cg;
  cg.setup(maphys::parameters::A<Mat_d>{A_d},
           maphys::parameters::max_iter<int>{20000},
           maphys::parameters::tolerance<double>{1e-8},
           maphys::parameters::verbose<bool>{true});

  Vect_d X = cg * rhs;

  cg.display("cg");

  double * X_ptr = X.get_ptr();
  std::cout << "X: " << std::endl;
  for(auto k = 0; k < 10; ++k){
    std::cout << X_ptr[k] << " ";
  }
  std::cout << std::endl;

  return 0;
}
