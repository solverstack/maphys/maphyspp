#include <iostream>
#include <fstream>

#include <maphys.hpp>
#include <maphys/dist/Process.hpp>

using namespace maphys;

void usage(char ** argv){
  std::cout << "Usage:\n";
  std::cout << argv[0] << " path_to_partitions n_subdomains\n";
}

int main(int argc, char ** argv) {

  MMPI::init();
  {
    if(argc != 3){
      usage(argv);
      return 1;
    }
    const std::string path_to_part(argv[1]);
    const int n_subdomains = std::stoi(std::string(argv[2]));

    std::shared_ptr<Process> p = bind_subdomains(n_subdomains);

    p->load_subdomains(path_to_part);
    p->dump_subdomains(path_to_part, true);
  }
  MMPI::finalize();

  return 0;
}
