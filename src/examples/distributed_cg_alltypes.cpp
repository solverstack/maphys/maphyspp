#include <iostream>
#include <fstream>
#include <vector>

#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen_header.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMADILLO
#include <maphys/wrappers/armadillo/Armadillo_header.hpp>
#endif
#ifdef MAPHYSPP_USE_EIGEN
#include <maphys/wrappers/Eigen/Eigen.hpp>
#endif
#ifdef MAPHYSPP_USE_ARMADILLO
#include <maphys/wrappers/armadillo/Armadillo.hpp>
#endif

#define TIMER_RECORD_CUMULATIVE

#include <maphys.hpp>
#include <maphys/IO/ArgumentParser.hpp>
#include <maphys/solver/ConjugateGradient.hpp>
#include <maphys/precond/DiagonalPrecond.hpp>
#include <maphys/part_data/PartMatrix.hpp>

using namespace maphys;
using Scalar = double;

template<class Vector, class Matrix>
void test_cg(const Matrix& A, const Vector& b, const std::string& test_name,
             const int max_iter, const double tol, const bool verbose){
  using Precond = DiagonalPrecond<Matrix, Vector>;
  Precond pcd(A);

  if(MMPI::rank() == 0) std::cout << "=========\n" << test_name << "=========\n";

  ConjugateGradient<Matrix, Vector, Precond> cg;
  cg.setup(parameters::A<Matrix>{A},
           parameters::max_iter<int>{max_iter},
           parameters::verbose<bool>{(MMPI::rank() == 0) && verbose},
           parameters::tolerance<double>{tol});

  {
    Timer<1> t_maphys(test_name);
    auto X = cg * b;
  }

  if(MMPI::rank() == 0) cg.display();
}

void usage(char ** argv){
  if(MMPI::rank() == 0){
    std::cout << "Usage: " << std::string(argv[0]) << " path_to_partitions n_procs\n";
    std::cout << "    Optional keys:\n";
    std::cout << "    --max_iter n : maximum number of iteration to n\n";
    std::cout << "    --tolerance t : set iterative solver tolerance to t\n";
    std::cout << "    --solution_one : set rhs so that solution is a vector of ones\n";
    std::cout << "    --verbose : process 0 displays additional output\n";
  }
}

int main(int argc, char ** argv) {

  MMPI::init();
  {
#if defined(MAPHYSPP_USE_RSB) || defined(MAPHYSPP_USE_RSB_SPBLAS)
    if(rsb_lib_init(RSB_NULL_INIT_OPTIONS) != RSB_ERR_NO_ERROR) return -1;
#endif
    auto args_dict = parse_arguments(argc, argv);

    for(auto& [k, v] : args_dict){
      std::cout << k << ": " << v << '\n';
    }

    if((argc < 3) || args_dict.count(std::string("help"))){
      usage(argv);
      MMPI::finalize();
      return 1;
    }

    std::string path_to_part = std::string(argv[1]);
    int n_subdomains = std::stoi(std::string(argv[2]));

    int max_iter = 500;
    if(args_dict.count(std::string("max_iter"))){
      max_iter = std::stoi(args_dict[std::string("max_iter")]);
    }

    double tolerance = 1e-8;
    if(args_dict.count(std::string("tolerance"))){
      tolerance = std::stod(args_dict[std::string("tolerance")]);
    }

    bool solution_one = (args_dict.count(std::string("solution_one")) == 1);
    bool verbose = (args_dict.count(std::string("verbose")) == 1);

    // Subdomain topology
    std::shared_ptr<Process> p = bind_subdomains(n_subdomains);

    // Load matrix and RHS
    PartMatrix<SparseMatrixCOO<Scalar>> coo_matrix(p);
    PartVector<Vector<Scalar>> rhs(p);
    load_subdomains_and_data(path_to_part, n_subdomains, p, coo_matrix, rhs);
    rhs.assemble();

    // Set to symmetric positive definite, lower storage
    auto set_spd_full = [](SparseMatrixCOO<Scalar>& loc_mat){
                          loc_mat.fill_tri_to_full();
                          loc_mat.set_spd(MatrixStorage::full);
                          loc_mat.order_indices();
                        };
    coo_matrix.apply_on_data(set_spd_full);

    if(solution_one){
      PartVector<Vector<Scalar>> ones = rhs;
      auto set_to_ones = [](Vector<Scalar>& v){ for(size_t i = 0; i < n_rows(v); ++i) v[i] = double{1}; };
      ones.apply_on_data(set_to_ones);
      rhs = coo_matrix * ones;
    }

    // COO matrix
    test_cg(coo_matrix, rhs, "MaPHyS COO matrix", max_iter, tolerance, verbose);

    auto coo_to_csc = [](const SparseMatrixCOO<Scalar>& coo) { return coo.to_csc(); };
    PartMatrix<SparseMatrixCSC<Scalar>> csc_matrix = coo_matrix.convert<SparseMatrixCSC<Scalar>>(coo_to_csc);

    // COO matrix
    test_cg(csc_matrix, rhs, "MaPHyS CSC matrix", max_iter, tolerance, verbose);

#ifdef MAPHYSPP_USE_EIGEN
    // Eigen matrix
    using EigenMat = Eigen::SparseMatrix<Scalar>;
    using EigenVect = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;

    auto coo_to_eigen = [](const SparseMatrixCOO<Scalar>& coo) {
      EigenMat out_eigen;
      build_matrix(out_eigen, n_rows(coo), n_cols(coo), n_nonzero(coo),
                   coo.get_i_ptr(), coo.get_j_ptr(), coo.get_v_ptr());
      return out_eigen;
    };
    PartMatrix<EigenMat> eigen_matrix = coo_matrix.convert<EigenMat>(coo_to_eigen);

    auto vect_to_eigenvect = [](const Vector<Scalar>& v) {
                               EigenVect ev(size(v));
                               for(Size i = 0; i < size(v); ++i) ev[i] = v[i];
                               return ev;
                             };
    PartVector<EigenVect> eigen_rhs = rhs.convert<EigenVect>(vect_to_eigenvect);

    test_cg(eigen_matrix, eigen_rhs, "Eigen matrix", max_iter, tolerance, verbose);
#endif

#ifdef MAPHYSPP_USE_ARMADILLO
    // Armadillo matrix
    auto coo_to_arma = [](const SparseMatrixCOO<Scalar>& coo) {
                         arma::SpMat<Scalar> out_arma;
                         build_matrix(out_arma, n_rows(coo), n_cols(coo), n_nonzero(coo),
                                      coo.get_i_ptr(), coo.get_j_ptr(), coo.get_v_ptr());
                         return out_arma;
                       };
    PartMatrix<arma::SpMat<Scalar>> arma_matrix = coo_matrix.convert<arma::SpMat<Scalar>>(coo_to_arma);

    auto vect_to_armavect = [](const Vector<Scalar>& v) {
                              arma::Col<Scalar> av(size(v));
                              for(Size i = 0; i < size(v); ++i) av[i] = v[i];
                              return av;
                            };
    PartVector<arma::Col<Scalar>> arma_rhs = rhs.convert<arma::Col<Scalar>>(vect_to_armavect);

    test_cg(arma_matrix, arma_rhs, "Armadillo matrix", max_iter, tolerance, verbose);
#endif

    if (MMPI::rank() == 0){
      Timer<1>::results();
    }
#if defined(MAPHYSPP_USE_RSB) || defined(MAPHYSPP_USE_RSB_SPBLAS)
    if(rsb_lib_exit(RSB_NULL_EXIT_OPTIONS) != RSB_ERR_NO_ERROR) return -1;
#endif
  }
  MMPI::finalize();

  return 0;
}
