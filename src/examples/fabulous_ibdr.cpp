/*!
 * Example strongly inspired from fabulous' example1.cpp
 * \brief Example for fabulous use; dense matrix, not distributed
 */

#include <maphys.hpp>
#include <maphys/solver/Fabulous.hpp>
#include <utility>
#include <string>

using Scalar = std::complex<double>;
using DM = maphys::DenseMatrix<Scalar>;//fa::Block<Scalar>;
using SparseMat = maphys::SparseMatrixCOO<Scalar>;

/*! \brief example1 matrix */

int main(int argc, char** argv){

  std::string mat_path("../matrices/young1c.mtx");
  if(argc == 2){
    mat_path = std::string(argv[1]);
  }

  std::cout << "Search for matrix (complex) in: " << mat_path << '\n';

  SparseMat A;
  A.from_matrix_market_file(mat_path);
  std::vector<double> epsilon{1e-6};
  const int dim = A.get_n_cols();
  const int max_mvp = 1000;
  const int nrhs = 6;

  DM X(dim, nrhs);
  for(auto i = 0; i < dim; ++i)
    for(auto j = 0; j < nrhs; ++j)
      X(i,j) = Scalar{(double) i+1, ((double) j + 1)/2.0};

  // Choose the fabulous algorithm
  auto algo = fabulous::bgmres::qribdr();

  maphys::Fabulous<SparseMat, DM, maphys::Identity<SparseMat, DM>, decltype(algo), fabulous::DeflatedRestart<Scalar>> fabulous;
  fabulous.setup(maphys::fabulous_params::A{A},
                 maphys::fabulous_params::max_iter{max_mvp},
                 maphys::fabulous_params::tolerances{epsilon},
                 //maphys::fabulous_params::preconditioner{M},
                 maphys::fabulous_params::verbose{true},
                 maphys::fabulous_params::algo{algo},
                 maphys::fabulous_params::deflated_restart{fabulous::deflated_restart<Scalar>(20, Scalar{0.0})}
                 );

  DM B = A * X;
  DM sol = fabulous * B;

  std::cout << "Found\tExpected\n";
  for(auto i = 0; i < 10; ++i){
    std::cout << sol.at(i, 0) << "\t\t" << X.at(i, 0) << std::endl;
  }

  fabulous.display("Fabulous");

  return 0;
}
