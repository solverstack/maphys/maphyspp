/*
 * In this example we call dense matrices operations (product and linear solver)
 */

#include <iostream>
#include <string>
#include <chameleon.h>
#include <maphys.hpp>

using namespace maphys;

int main(int argc, char *argv[]) {

  using Scalar = double;

  /* Choose between Blas and Chameleon kernels/solver */
  int BlasOrChameleon = 0;
  if( argc > 1 ) {
    std::cout << "BlasOrChameleon = " << argv[1] << std::endl;
    BlasOrChameleon = std::stoi(argv[1]);
  }

  /* Set size M of the problem (number of rows in the matrices) */
  const int Defaultsize = 10;
  int M = Defaultsize;
  if( argc > 2 ) {
    std::cout << "M = " << argv[2] << std::endl;
    M = std::stoi(argv[2]);
  }

  /* Set size N of the problem (number of columns in the rectangular matrices) */
  int N = Defaultsize;
  if( argc > 3 ) {
    std::cout << "N = " << argv[3] << std::endl;
    N = std::stoi(argv[3]);
  }
  if ( N > M ){
    std::cout << "N cannot be larger than M, exit." << std::endl;
    return -1;
  }

  /* Chameleon parameters: tile size, number of threads, gpus */
  int NB = std::min(320, N);
  if( argc > 4 ) {
    std::cout << "NB = " << argv[4] << std::endl;
    NB = std::stoi(argv[4]);
  }
  Chameleon::tile_size = NB;

  int NT = 4;
  if( argc > 5 ) {
    std::cout << "NT = " << argv[5] << std::endl;
    NT = std::stoi(argv[5]);
  }
  Chameleon::ncpus = NT;

  int NG = 0;
  if( argc > 6 ) {
    std::cout << "NG = " << argv[6] << std::endl;
    NG = std::stoi(argv[6]);
  }
  Chameleon::ngpus = NG;

  /* Initialize Chameleon context */
  Chameleon chameleon;

  /* GEMM */

  /* Fill the matrices A and B (for GEMM) with random values */
  DenseMatrix<Scalar> A(M, M);
  DenseMatrix<Scalar> B(M, M);
  DenseMatrix<Scalar> C(M, M);
  int seedA = 10;
  CHAMELEON_dplrnt( M, M, get_ptr(A), M, seedA );
  int seedB = 20;
  CHAMELEON_dplrnt( M, M, get_ptr(B), M, seedB );

  Timer<0> tgemm("Gemm time");
  if ( BlasOrChameleon == 0 ) {
    blas_kernels::gemm(A, B, C);
    if ( M <= Defaultsize ) C.display("C blas");
  } else {
    chameleon_kernels::gemm(A, B, C);
    if ( M <= Defaultsize ) C.display("C chameleon");
  }
  tgemm.stop();
  tgemm.summary_results();
  // C = A * B; may be called instead of the call to function gemm
  // if compose is configured with chameleon this is chameleon that is used underneath else blas

  /* Solver SPD Ax=b */

  /* Fill the matrix A with random values so that it is square and SPD */
  CHAMELEON_dplgsy( (Scalar)M, ChamLower, M, get_ptr(A), M, seedA );
  A.set_spd(MatrixStorage::full);

  /* Fill the vector b with random values */
  Vector<Scalar> b(M);
  CHAMELEON_dplrnt( M, 1, get_ptr(b), M, seedB );

  /* Solve the linear system */
  Timer<0> tlinsys("Linear system (Cholesky) time");
  if ( BlasOrChameleon == 0 ) {
    BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>> SCblas;
    // if A is given it is copied in the solver class
    // giving A as rvalue allows to avoid copy
    SCblas.setup(std::move(A));
    // Can call SCblas.factorize(); to perform factorization before solving
    Vector<Scalar> x = SCblas * b;
    if ( M <= Defaultsize ) x.display("x blas");
  } else {
    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> SCcham;
    SCcham.setup(std::move(A));
    Vector<Scalar> x = SCcham * b;
    if ( M <= Defaultsize ) x.display("x chameleon");
  }
  tlinsys.stop();
  tlinsys.summary_results();

  /* Solver least squares -> A=QR */

  /* Fill the rectangular matrix A with random values */
  A = DenseMatrix<Scalar>(M, N);
  CHAMELEON_dplrnt( M, N, get_ptr(A), M, seedA );

  Timer<0> tlssys("Least squares system time");
  if ( BlasOrChameleon == 0 ) {
    BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>> SLblas;
    SLblas.setup(std::move(A));
    Vector<Scalar> x = SLblas * b;
    if ( M <= Defaultsize ) x.display("x blas");
  } else {
    ChameleonSolver<DenseMatrix<Scalar>, Vector<Scalar>> SLcham;
    SLcham.setup(std::move(A));
    Vector<Scalar> x = SLcham * b;
    if ( M <= Defaultsize ) x.display("x chameleon");
  }
  tlssys.stop();
  tlssys.summary_results();

  return 0;
}
