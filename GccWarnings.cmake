set(_maphys_gcc_warnings "-Wall -Wextra") # Basics

set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wshadow") #warn the user if a variable declaration shadows one from a parent context
set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wnon-virtual-dtor") # warn the user if a class with virtual functions has a non-virtual destructor. This helps catch hard to track down memory errors
set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wpedantic") # (all versions of GCC, Clang >= 3.2) warn if non-standard C++ is used

#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wold-style-cast") # warn for c-style casts
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wcast-align") # warn for potential performance problem casts
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wunused") #warn on anything being unused
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Woverloaded-virtual") # warn if you overload (not override) a virtual function
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wconversion") # warn on type conversions that may lose data
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wsign-conversion") # (Clang all versions, GCC >= 4.3) warn on sign conversions
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wmisleading-indentation") # (only in GCC >= 6.0) warn if indentation implies blocks where blocks do not exist
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wduplicated-cond") # (only in GCC >= 6.0) warn if if / else chain has duplicated conditions
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wduplicated-branches") # (only in GCC >= 7.0) warn if if / else branches have duplicated code
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wlogical-op") # (only in GCC) warn about logical operations being used where bitwise were probably wanted
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wnull-dereference") # (only in GCC >= 6.0) warn if a null dereference is detected
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wuseless-cast") # (only in GCC >= 4.8) warn if you perform a cast to the same type
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wdouble-promotion") # (GCC >= 4.6, Clang >= 3.8) warn if float is implicit promoted to double
#set(_maphys_gcc_warnings "${_maphys_gcc_warnings} -Wformat=2") # warn on security issues around functions that format output (ie printf)
