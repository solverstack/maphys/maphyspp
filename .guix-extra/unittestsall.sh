#!/usr/bin/env bash

set -ex

guix time-machine -C .guix-extra/channels-fixed.scm -- describe
git submodule update --init --recursive
guix time-machine -C .guix-extra/channels-fixed.scm -- shell --pure -D maphys++ armadillo eigen emacs emacs-org python-gcovr sz-compressor -- .guix-extra/unittestssuball.sh
