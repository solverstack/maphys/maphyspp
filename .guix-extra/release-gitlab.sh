#!/usr/bin/env bash
###
#
#  @file release-gitlab.sh
#  @copyright 2013-2021 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
#                       Univ. Bordeaux. All rights reserved.
#
#  @brief Script to generate the release when pushing a branch and tag of the same name
#
#  @version 1.1.0
#  @author Florent Pruvost
#  @author Mathieu Faverge
#  @author Matthieu Simonin (minor)
#  @author Gilles Marait (minor)
#  @date 2021-04-20
#
###

#
# Steps to do the release:
#    - Update the ChangeLog
#    - Create a tag named vx.x.x and push it on gitlab (will trigger the CI to generate the release)
#

# Get the release name through the branch name, and through the ChangeLog file.
# Both have to match to be correct
changelog=""
function gen_changelog()
{
    local firstline=$( grep -n "^maphyspp-" ChangeLog | head -n 1 | cut -d ':' -f 1 )
    firstline=$(( firstline + 2 ))
    #echo $firstline
    local lastline=$( grep -n "^maphyspp-" ChangeLog | head -n 2 | tail -n 1 | cut -d ':' -f 1 )
    lastline=$(( lastline - 1 ))
    #echo $lastline

    changelog="Changes:\n"
    for i in `seq $firstline $lastline`
    do
        local line=$( head -n $i ChangeLog | tail -n 1 )
        changelog="$changelog$line\\n"
        #echo $line
    done

    changelog="$changelog\nWARNING: Download the source archive by clicking on the link __Download release__ above, please do not consider the automatic Source code links as they are missing the submodules.\n"
}

release=""
function get_release()
{
    local firstline=$( grep -n "^maphyspp-" ChangeLog | head -n 1 | cut -d ':' -f 1 )
    release=$( head -n $firstline ChangeLog | tail -n 1 | sed 's/maphyspp\-//' )
}

set -x

guix describe

# Get the release name through the git ref (tag), and through the ChangeLog file.
# Both have to match to be correct
RELEASE_NAME=`echo $CI_COMMIT_TAG | cut -d v -f 2`
get_release

if [ -z "$RELEASE_NAME" -o -z "$release" -o "$RELEASE_NAME" != "$release" ]
then
    echo "Commit name $RELEASE_NAME is different from ChangeLog name $release"
    if [ -z "$RELEASE_NAME" -a -z "$release" ]
        then
            echo 'Exiting, no release version defined'
            exit 1
        fi
    if [ -z "$RELEASE_NAME" ]
    then
        echo 'Release version of branch missing, using ChangeLog version'
        RELEASE_NAME=$release
    fi
    if [ -z "$release" ]
    then
        echo 'Release version in ChangeLog missing, using branch version'
        release=$RELEASE_NAME
    fi
    if [ "$RELEASE_NAME" != "$release" ]
    then
        echo 'Exiting, conficting release version'
        exit 1
    fi
fi

# generate the archive
wget https://raw.githubusercontent.com/Kentzo/git-archive-all/master/git_archive_all.py
python3 git_archive_all.py --force-submodules maphyspp-$RELEASE_NAME.tar.gz

# upload the source archive to the Gitlab's Package registry
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./maphyspp-$RELEASE_NAME.tar.gz "https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/source/$CI_COMMIT_TAG/maphyspp-$RELEASE_NAME.tar.gz"

# extract the change log from ChangeLog
gen_changelog
echo $changelog

# Try to remove the release if it already exists
curl --request DELETE --header "JOB-TOKEN: $CI_JOB_TOKEN" https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases/v$RELEASE_NAME

# create the release associated to the tag
COMMAND=`echo curl --header \"Content-Type: application/json\" --header \"JOB-TOKEN: $CI_JOB_TOKEN\" \
  --data \'{ \"name\": \"v$RELEASE_NAME\", \
            \"tag_name\": \"v$RELEASE_NAME\", \
            \"ref\": \"$CI_COMMIT_REF_NAME\", \
            \"description\": \"$changelog\", \
            \"assets\": { \"links\": [{ \"name\": \"Download release maphyspp-$RELEASE_NAME.tar.gz\", \"url\": \"https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/packages/generic/source/$CI_COMMIT_TAG/maphyspp-$RELEASE_NAME.tar.gz\" }] } }\' \
  --request POST https://gitlab.inria.fr/api/v4/projects/$CI_PROJECT_ID/releases`
eval $COMMAND
