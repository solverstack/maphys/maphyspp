#!/usr/bin/env bash

set -ex

./tangleorg.sh

export OMPI_MCA_rmaps_base_oversubscribe=1
#NB: qrmumps off because sources are not free (and also wrapper TOFIX for new version)
cmake -B build -DCMAKE_CXX_FLAGS="--coverage" -DMAPHYSPP_USE_ARMADILLO=ON -DMAPHYSPP_USE_EIGEN=ON -DMAPHYSPP_USE_PASTIX=ON -DMAPHYSPP_USE_MUMPS=ON -DMAPHYSPP_USE_QRMUMPS=OFF -DMAPHYSPP_USE_FABULOUS=ON -DMAPHYSPP_USE_SZ_COMPRESSOR=ON -DMAPHYSPP_USE_ARPACK=ON -DMAPHYSPP_C_DRIVER=ON -DMAPHYSPP_Fortran_DRIVER=ON -DMAPHYSPP_COMPILE_EXAMPLES=ON -DMAPHYSPP_COMPILE_TESTS=ON -DMAPHYSPP_COMPILE_BENCH=ON -DMAPHYSPP_USE_TLAPACK=ON -DMAPHYSPP_USE_CHAMELEON=ON -DCMAKE_INSTALL_PREFIX=$PWD/install -DCMAKE_VERBOSE_MAKEFILE=ON
cmake --build build --verbose -j4
ctest --test-dir build --no-compress-output -T Test --output-junit junit.xml
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root .
mv build/junit.xml .
