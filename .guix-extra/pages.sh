#!/usr/bin/env bash

set -ex

export PUBLISHDIR="$CI_COMMIT_BRANCH"
guix describe
export ENVGUIX="emacs emacs-org emacs-org-ref emacs-htmlize git texlive python-pygments imagemagick"
export GUIXCMD="guix time-machine -C ./.guix-extra/channels-fixed.scm -- shell --pure"
# Tangle source code
${GUIXCMD} ${ENVGUIX} -- emacs --batch --no-init-file --load publish.el --eval '(org-publish "generate-source-code")'
# Generate pdf
${GUIXCMD} ${ENVGUIX} -- emacs --batch --no-init-file --load publish.el --eval '(org-publish "generate-pdf")'
# Pre-treatment for htmls
cd ./doc/html/ && ./make_doc.sh && cd ../..
# HTML export
${GUIXCMD} --preserve=PUBLISHDIR ${ENVGUIX} -- emacs --batch --no-init-file --load publish.el --eval '(org-publish "site")'
# Set README.org as homepage
if [[ -f ./public/README.html ]]; then mv ./public/README.html ./public/index.html; fi
# Add an index to link to the different branch pages available
cp ./doc/html/index_to_branches.org ./public/index_to_branches.org
cd ./public && guix time-machine -C ../.guix-extra/channels-fixed.scm -- shell --pure ${ENVGUIX} -- emacs --batch -l org index_to_branches.org -f org-html-export-to-html && cd ..
