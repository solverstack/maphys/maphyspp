#!/usr/bin/env bash

set -ex

./tangleorg.sh

export OMPI_MCA_rmaps_base_oversubscribe=1
cmake -B build -DCMAKE_CXX_FLAGS="--coverage" -DMAPHYSPP_GCC_WARNINGS=ON -DMAPHYSPP_USE_FABULOUS=ON -DMAPHYSPP_USE_CHAMELEON=ON -DCMAKE_INSTALL_PREFIX=$PWD/install -DCMAKE_VERBOSE_MAKEFILE=ON
cmake --build build --verbose -j4
ctest --test-dir build --no-compress-output -T Test --output-junit junit.xml
NFAILURES=$(cat build/junit.xml | grep failures | awk -F '"' '{print $2}')
if [[ "$NFAILURES" -gt 0 ]]; then
    exit 1
fi
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root .
mv build/junit.xml .
