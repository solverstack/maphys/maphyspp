#!/usr/bin/env bash

set -ex

guix time-machine -C .guix-extra/channels-fixed.scm -- describe
git submodule update --init --recursive
guix time-machine -C .guix-extra/channels-fixed.scm -- shell --pure -D maphys++ emacs emacs-org python-gcovr -- .guix-extra/unittestssub.sh
