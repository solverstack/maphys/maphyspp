set(maphyspp_VERSION @maphyspp_VERSION@)

# relocatable package
@PACKAGE_INIT@

set_and_check(maphyspp_BIN_DIR "@PACKAGE_BIN_INSTALL_DIR@")
set_and_check(maphyspp_INC_DIR "@PACKAGE_INC_INSTALL_DIR@")
set_and_check(maphyspp_LIB_DIR "@PACKAGE_LIB_INSTALL_DIR@")

check_required_components(maphyspp)

# need MORSE Find modules: necessary files must be distributed in the install path
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/find")

include(CMakeFindDependencyMacro)

find_dependency(blaspp REQUIRED)
find_dependency(lapackpp REQUIRED)
find_dependency(MPI)

# Propagate c++ standard required
if(NOT DEFINED CMAKE_CXX_STANDARD)
  if(@MAPHYSPP_CXX_CONCEPTS@)
    set(CMAKE_CXX_STANDARD 20)
  else()
    set(CMAKE_CXX_STANDARD 17)
  endif()
endif()
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# blaspp bug?
if(@OpenMP_FOUND@)
  find_dependency(OpenMP REQUIRED)
endif()

if(@CHAMELEON_FOUND@)
  find_dependency(CHAMELEON REQUIRED)
endif()

if(@PASTIX_FOUND@)
  find_dependency(PASTIX REQUIRED)
endif()

if(@MUMPS_FOUND@)
  find_dependency(MUMPS REQUIRED)
endif()

if(@Fabulous_FOUND@)
  find_dependency(Fabulous REQUIRED)
endif()

if(@QRM_FOUND@)
  find_dependency(QRM REQUIRED)
endif()

if(@SZ_FOUND@)
  find_dependency(SZ REQUIRED)
endif()

if(@Eigen3_FOUND@)
  find_dependency(Eigen3 REQUIRED)
endif()

if(@ARMADILLO_FOUND@)
  find_dependency(ARMADILLO REQUIRED)
endif()

if(@arpack-ng_FOUND@)
  find_dependency(arpack-ng REQUIRED NAMES arpackng arpack-ng)
endif()

include("${CMAKE_CURRENT_LIST_DIR}/maphysppTargets.cmake")

if(@MAPHYSPP_C_DRIVER@)
  include("${CMAKE_CURRENT_LIST_DIR}/mpp_driver_cTargets.cmake")
endif()

if(@MAPHYSPP_Fortran_DRIVER@)
  include("${CMAKE_CURRENT_LIST_DIR}/mpp_driver_fTargets.cmake")
endif()
